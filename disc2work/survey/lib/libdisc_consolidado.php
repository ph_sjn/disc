<?php

/******
 * 
 * libdefweb_consolidado.php
 * 
 * Classe para obtencao das informacoes consolidadas
 * 
 ******/

// Configuracoes de acesso ao banco de dados ///////////////////////////////////

include_once "../config.inc.php";

$DEFWEBcs_db_type = $db_type;
$DEFWEBcs_db_host = $db_host;
$DEFWEBcs_db_user = $db_user;
$DEFWEBcs_db_pass = $db_pass;
$DEFWEBcs_db_name = $db_name;

define("DEFWEBcs_debug", 0);
define("DEFWEBcs_db_type",$DEFWEBcs_db_type);
define("DEFWEBcs_db_host",$DEFWEBcs_db_host);
define("DEFWEBcs_db_user",$DEFWEBcs_db_user);
define("DEFWEBcs_db_pass",$DEFWEBcs_db_pass);
define("DEFWEBcs_db_name",$DEFWEBcs_db_name);

// Classes auxiliares //////////////////////////////////////////////////////////

class DEFWEBconsolidadoItem {
    //
    public $CodigoTema = null; // cd_tema
    public $DescricaoTema = null; // ds_tema
    public $CodigoItem = null; // cd_item
    public $DescricaoItem = null; // ds_item
    public $IdItemFormulario = null; // id_item_formulario
    public $IdFormulario = null; // id_formulario
    public $CodigoAgrupamento = null; // cd_agrupamento
    public $DescricaoAgrupamento = null; // ds_agrupamento
    public $TipoOpcoes = null; // dv_tipo_opcoes
    // array DEFWEBconsolidadoItemOpcao()
    public $colOpcoes = null;
    //
    public $QuantidadeTotal = null; // << CALCULAR >>
}

class DEFWEBconsolidadoItemOpcao {
    //
    public $Resposta = null; // ds_resposta
    public $DescricaoOpcaoItem; // ds_opcao_item
    public $Quantidade = null; // nr_qtde (GROUP BY)
    //
    public $CodigoOpcao = null; // cd_opcao_item
    public $CodigoItem = null; // cd_item
    public $CodigoAgrupamento = null; // cd_agrupamento
    public $CodigoTema = null; // cd_tema
    public $Versao = null; // id_versao
    public $ValorItem = null; // vl_opcao_item
    public $ValorOpcao = null; // vl_opcao_valor
    public $TipoCampo = null; // dv_tipo_campo
    //
    public $Percentual = null; // << CALCULAR >>
}

// Classe principal do consolidado /////////////////////////////////////////////

class DEFWEBmasterConsolidado {
    
    // Atributos para checagem de erros nas chamadas e no acesso ao
    // banco de dados
    
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";

    function __construct() {
        //
    }

    function obterOpcoesDeRespostas($PARM_cd_agrupamento, $PARM_cd_tema, $PARM_cd_item) {
        //
        $RET_array = array();
        //
        if(DEFWEBcs_debug) {
            $this->DBG_db_type = DEFWEBcs_db_type;
            $this->DBG_db_host = DEFWEBcs_db_host;
            $this->DBG_db_name = DEFWEBcs_db_name;
            $this->DBG_db_user = DEFWEBcs_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBcs_db_host, DEFWEBcs_db_user, DEFWEBcs_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBcs_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " cd_opcao_item, ds_opcao_item";
                $sSQL .= ", cd_agrupamento";
                $sSQL .= ", cd_tema, cd_item ";
                $sSQL .= ", id_versao ";
                $sSQL .= ", vl_opcao_item ";
                $sSQL .= ", vl_opcao_valor ";
                $sSQL .= ", dv_tipo_campo ";
                $sSQL .= " FROM tbdw_opcao_item";
                $sSQL .= " WHERE cd_agrupamento = " . mysqli_escape_string($objLink, $PARM_cd_agrupamento);
                $sSQL .= " AND cd_tema = " . mysqli_escape_string($objLink, $PARM_cd_tema);
                $sSQL .= " AND cd_item = " . mysqli_escape_string($objLink, $PARM_cd_item);
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objItem = new DEFWEBconsolidadoItemOpcao();
                        //
                        $objItem->CodigoOpcao = $rsDados["cd_opcao_item"];
                        $objItem->DescricaoOpcaoItem = $rsDados["ds_opcao_item"];
                        $objItem->CodigoAgrupamento = $rsDados["cd_agrupamento"];
                        $objItem->CodigoTema = $rsDados["cd_tema"];
                        $objItem->CodigoItem = $rsDados["cd_item"];
                        $objItem->Versao = $rsDados["id_versao"];
                        $objItem->ValorItem = $rsDados["vl_opcao_item"];
                        $objItem->ValorOpcao = $rsDados["vl_opcao_valor"];
                        $objItem->TipoCampo = $rsDados["dv_tipo_campo"];
                        //
                        $RET_array[$objItem->CodigoOpcao] = $objItem;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }
    
    function obterConsolidado($PARM_id_avaliacao) {
        //
        $RET_array = array();
        //
        if(DEFWEBcs_debug) {
            
            $this->DBG_db_type = DEFWEBcs_db_type;
            $this->DBG_db_host = DEFWEBcs_db_host;
            $this->DBG_db_name = DEFWEBcs_db_name;
            $this->DBG_db_user = DEFWEBcs_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBcs_db_host, DEFWEBcs_db_user, DEFWEBcs_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBcs_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " itemform.cd_tema AS cd_tema";
                $sSQL .= ", tema.ds_tema AS ds_tema";
                $sSQL .= ", item.cd_item AS cd_item";
                $sSQL .= ", item.ds_item AS ds_item";
                $sSQL .= ", item.dv_tipo_opcoes AS dv_tipo_opcoes";
                $sSQL .= ", itemform.id_item_formulario AS id_item_formulario";
                $sSQL .= ", itemform.id_formulario AS id_formulario";
                $sSQL .= ", itemform.cd_agrupamento AS cd_agrupamento";
                $sSQL .= ", agrup.ds_agrupamento";
                $sSQL .= ", resp.ds_resposta AS ds_resposta";
                $sSQL .= ", opcao.ds_opcao_item AS ds_opcao_item";
                $sSQL .= ", opcao.cd_opcao_item AS cd_opcao_item";
                $sSQL .= ", count(resp.ds_resposta) AS nr_qtde";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_avaliacao_formulario AS af";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_item_formulario AS itemform ON itemform.id_formulario = af.id_formulario";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_item AS item ON item.cd_item = itemform.cd_item AND item.cd_agrupamento = itemform.cd_agrupamento AND item.cd_tema = itemform.cd_tema";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_agrupamento AS agrup ON agrup.cd_agrupamento = item.cd_agrupamento";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_tema AS tema ON tema.cd_tema = itemform.cd_tema AND tema.cd_agrupamento = item.cd_agrupamento";
                $sSQL .= " LEFT JOIN ";
                $sSQL .= " tbdw_aval_token AS token ON token.id_avaliacao = af.id_avaliacao AND token.cd_publico_alvo = af.cd_publico_alvo";
                $sSQL .= " LEFT JOIN ";
                $sSQL .= " tbdw_af_token_item AS resp ON resp.id_item_formulario = itemform.id_item_formulario AND resp.id_token = token.id_token";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_opcao_item AS opcao ON opcao.cd_opcao_item = resp.cd_opcao_item AND opcao.cd_agrupamento = item.cd_agrupamento AND opcao.cd_tema = item.cd_tema";
                $sSQL .= " WHERE af.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                $sSQL .= " AND af.cd_publico_alvo = 2";
                $sSQL .= " GROUP";
                $sSQL .= " BY";
                $sSQL .= " itemform.cd_tema";
                $sSQL .= ", tema.ds_tema";
                $sSQL .= ", item.cd_item";
                $sSQL .= ", item.ds_item";
                $sSQL .= ", item.dv_tipo_opcoes";
                $sSQL .= ", itemform.id_item_formulario";
                $sSQL .= ", itemform.id_formulario";
                $sSQL .= ", itemform.cd_agrupamento";
                $sSQL .= ", agrup.ds_agrupamento";
                $sSQL .= ", opcao.cd_opcao_item";
                $sSQL .= ", resp.ds_resposta";
                $sSQL .= ", opcao.ds_opcao_item";
                $sSQL .= " ORDER";
                $sSQL .= " BY";
                $sSQL .= " af.id_avaliacao_formulario";
                $sSQL .= ", itemform.cd_tema";
                $sSQL .= ", tema.ds_tema";
                $sSQL .= ", item.cd_item";
                $sSQL .= ", item.ds_item";
                $sSQL .= ", item.dv_tipo_opcoes";
                $sSQL .= ", itemform.nr_posicao";
                $sSQL .= ", itemform.id_item_formulario";
                $sSQL .= ", itemform.cd_agrupamento";
                $sSQL .= ", agrup.ds_agrupamento";
                $sSQL .= ", opcao.cd_opcao_item";
                $sSQL .= ", resp.ds_resposta";
                $sSQL .= ", opcao.ds_opcao_item";
                //
                //$this->ERRO_mensagem = $sSQL;
                ////
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
					// Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    $iPos = 0;
					//
                    while($rsDados) {
                        //
                        unset($objItem);
                        $objItem = new DEFWEBconsolidadoItem();
                        //
                        $qtdeTotal = 0;
                        //
                        $objItem->CodigoItem = $rsDados["cd_item"];
                        $objItem->CodigoTema = $rsDados["cd_tema"];
                        $objItem->DescricaoTema = $rsDados["ds_tema"];
                        $objItem->DescricaoItem = $rsDados["ds_item"];
                        $objItem->IdItemFormulario = $rsDados["id_item_formulario"];
                        $objItem->IdFormulario = $rsDados["id_formulario"];
                        $objItem->CodigoAgrupamento = $rsDados["cd_agrupamento"];
                        $objItem->DescricaoAgrupamento = $rsDados["ds_agrupamento"];
                        $objItem->TipoOpcoes = $rsDados["dv_tipo_opcoes"];
                        //
                        unset($arrayTemp_colOpcoes);
                        $arrayTemp_colOpcoes = $this->obterOpcoesDeRespostas($objItem->CodigoAgrupamento, $objItem->CodigoTema, $objItem->CodigoItem);
                        //
                        unset($arrayTemp_colRespostas);
                        $arrayTemp_colRespostas = array();
                        //
                        // Se for somatorio de percentuais...
                        if($objItem->TipoOpcoes == "S") {
                            //
                            while( 
                                        ($rsDados) 
                                    &&  $rsDados["cd_tema"] == $objItem->CodigoTema
                                    &&  $rsDados["id_item_formulario"] == $objItem->IdItemFormulario
                                    &&  $rsDados["id_formulario"] == $objItem->IdFormulario
                                    &&  $rsDados["cd_agrupamento"] == $objItem->CodigoAgrupamento
                                    ) {
                                //
                                unset($objItemOpcao);
                                $objItemOpcao = new DEFWEBconsolidadoItemOpcao();
                                //
                                if($rsDados["ds_opcao_item"]) {
                                    $objItemOpcao->DescricaoOpcaoItem = $rsDados["ds_opcao_item"];
                                } else {
                                    $objItemOpcao->DescricaoOpcaoItem = $rsDados["ds_resposta"];
                                }
                                //
                                $qtRespostas = 0;
                                $pcTotal = 0;
                                //
                                if($objItemOpcao->DescricaoOpcaoItem) {
                                    //
                                    $objItemOpcao->CodigoOpcao = $rsDados["cd_opcao_item"];
                                    $objItemOpcao->CodigoAgrupamento = $rsDados["cd_agrupamento"];
                                    $objItemOpcao->CodigoTema = $rsDados["cd_tema"];
                                    $objItemOpcao->CodigoItem = $rsDados["cd_item"];
                                    $objItemOpcao->Versao = $rsDados["id_versao"];
                                    //
                                    while( 
                                                ($rsDados) 
                                            &&  $rsDados["cd_tema"] == $objItem->CodigoTema
                                            &&  $rsDados["id_item_formulario"] == $objItem->IdItemFormulario
                                            &&  $rsDados["id_formulario"] == $objItem->IdFormulario
                                            &&  $rsDados["cd_agrupamento"] == $objItem->CodigoAgrupamento
                                            &&  $rsDados["cd_opcao_item"] == $objItemOpcao->CodigoOpcao
                                            ) {
                                        //
                                        $qtRespostas = $qtRespostas + $rsDados["nr_qtde"];
                                        $pcTotal = $pcTotal + (floatval($rsDados["nr_qtde"]) * floatval($rsDados["ds_resposta"]));
                                        //
                                        $rsDados = mysqli_fetch_assoc($objResult);
                                    }
                                    //
                                    $objItemOpcao->Quantidade = $qtRespostas;
                                    $objItemOpcao->Resposta = $pcTotal;
                                    //
                                    if($qtRespostas != 0) {
                                        $objItemOpcao->Percentual = $pcTotal / $qtRespostas;
                                    }
                                    //
                                    $arrayTemp_colRespostas[$objItemOpcao->CodigoOpcao] = $objItemOpcao;
                                } else {
                                    $rsDados = mysqli_fetch_assoc($objResult);
                                }
                            }
                            //
                            foreach ($arrayTemp_colRespostas AS $objResposta) {
                                //
                                $arrayTemp_colOpcoes[$objResposta->CodigoOpcao]->Quantidade = $objResposta->Quantidade;
                                $arrayTemp_colOpcoes[$objResposta->CodigoOpcao]->Percentual = $objResposta->Percentual;
                                //
                                $qtdeTotal = $qtdeTotal + $objResposta->Quantidade;
                            }
                            //
                            foreach ($arrayTemp_colOpcoes AS $objOpcao) {
                                //
                                if((!is_null($objOpcao->CodigoItem)&&($objOpcao->CodigoItem != ""))&&(is_null($objOpcao->Quantidade)||$objOpcao->Quantidade=="")) {
                                    $arrayTemp_colOpcoes[$objOpcao->CodigoOpcao]->Quantidade = 0;
                                    $arrayTemp_colOpcoes[$objOpcao->CodigoOpcao]->Percentual = 0;
                                }
                            }
                            //
                            $objItem->colOpcoes = $arrayTemp_colOpcoes;
                            $objItem->QuantidadeTotal = $qtdeTotal;
                            //
                        } else {
                            //
                            while( 
                                        ($rsDados) 
                                    &&  $rsDados["cd_tema"] == $objItem->CodigoTema
                                    &&  $rsDados["id_item_formulario"] == $objItem->IdItemFormulario
                                    &&  $rsDados["id_formulario"] == $objItem->IdFormulario
                                    &&  $rsDados["cd_agrupamento"] == $objItem->CodigoAgrupamento
                                    ) {
                                //
                                unset($objItemOpcao);
                                $objItemOpcao = new DEFWEBconsolidadoItemOpcao();
                                //
                                $objItemOpcao->Resposta = $rsDados["ds_resposta"];
                                if($rsDados["ds_opcao_item"]) {
                                    $objItemOpcao->DescricaoOpcaoItem = $rsDados["ds_opcao_item"];
                                } else {
                                    $objItemOpcao->DescricaoOpcaoItem = $rsDados["ds_resposta"];
                                }
                                $objItemOpcao->Quantidade = $rsDados["nr_qtde"];
                                //
                                if($objItemOpcao->DescricaoOpcaoItem) {
                                    //
                                    $objItemOpcao->CodigoOpcao = $rsDados["cd_opcao_item"];
                                    $objItemOpcao->CodigoAgrupamento = $rsDados["cd_agrupamento"];
                                    $objItemOpcao->CodigoTema = $rsDados["cd_tema"];
                                    $objItemOpcao->CodigoItem = $rsDados["cd_item"];
                                    $objItemOpcao->Versao = $rsDados["id_versao"];
                                    //
                                    $arrayTemp_colRespostas[$objItemOpcao->CodigoOpcao] = $objItemOpcao;
                                }
                                //
                                $rsDados = mysqli_fetch_assoc($objResult);
                            }
                            //
                            foreach ($arrayTemp_colRespostas AS $objResposta) {
                                //
                                $arrayTemp_colOpcoes[$objResposta->CodigoOpcao]->Quantidade = $objResposta->Quantidade;
                                //
                                $qtdeTotal = $qtdeTotal + $objResposta->Quantidade;
                            }
                            //
                            foreach ($arrayTemp_colOpcoes AS $objOpcao) {
                                //
                                if((!is_null($objOpcao->CodigoItem)&&($objOpcao->CodigoItem != ""))&&(is_null($objOpcao->Quantidade)||$objOpcao->Quantidade=="")) {
                                    $arrayTemp_colOpcoes[$objOpcao->CodigoOpcao]->Quantidade = 0;
                                }
                                //
                                if($qtdeTotal!=0) {
                                    $arrayTemp_colOpcoes[$objOpcao->CodigoOpcao]->Percentual = 100*$arrayTemp_colOpcoes[$objOpcao->CodigoOpcao]->Quantidade/$qtdeTotal;
                                }
                            }
                            //
                            $objItem->colOpcoes = $arrayTemp_colOpcoes;
                            $objItem->QuantidadeTotal = $qtdeTotal;
                        }
                        //
                        $RET_array[$iPos] = $objItem;
                        $iPos++;
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }
    
}
