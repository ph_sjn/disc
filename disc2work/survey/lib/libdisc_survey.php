<?php
/******
 * 
 * libdefweb_survey.php
 * 
 * Classe utilizada na aplicacao dos questionarios
 * 
 ******/

// Configuracoes de acesso ao banco de dados ///////////////////////////////////

include_once "../config.inc.php";

$DEFWEBsv_db_type = $db_type;
$DEFWEBsv_db_host = $db_host;
$DEFWEBsv_db_user = $db_user;
$DEFWEBsv_db_pass = $db_pass;
$DEFWEBsv_db_name = $db_name;

define("DEFWEBsv_debug", 0);
define("DEFWEBsv_db_type",$DEFWEBsv_db_type);
define("DEFWEBsv_db_host",$DEFWEBsv_db_host);
define("DEFWEBsv_db_user",$DEFWEBsv_db_user);
define("DEFWEBsv_db_pass",$DEFWEBsv_db_pass);
define("DEFWEBsv_db_name",$DEFWEBsv_db_name);

// Classes auxiliares //////////////////////////////////////////////////////////

class DEFWEBsvPergunta {
    
    // Dados da pergunta
    public $id_item_formulario = null;
    public $cd_agrupamento = null;
    public $ds_agrupamento = null;
    public $cd_tema = null;
    public $ds_tema = null;
    public $cd_item = null;
    public $ds_item = null;
    public $dv_tipo_opcoes = null;
    public $qt_limite_resp = null;
    public $id_versao = null;
    public $id_aval_form_item = null;
    public $flag_respondida = null;
    public $nr_pergunta = null;
    
    // Identifica��o da pergunta anterior
    public $id_anterior = null;
    
    // Identificacao da pr�xima pergunta
    public $id_proxima = null;
    
}

// AJRB - 03/04/2017 - inicio
class DEFWEBdisc_normalizado {
	//
	public $colDISC_convertido = null;
	//
	public $serie       = "[0,0,0,0]";
	public $resultado   = "";
	public $complemento = "";
	//
    function __construct() {
    	//
    	unset($this->colDISC_convertido);
    	//
		$this->colDISC_convertido = array(
			 "D" => 0
			,"I" => 0
			,"S" => 0
			,"C" => 0
		);
		$this->serie 		= "[0,0,0,0]";
		$this->resultado 	= "";
		$this->complemento 	= "";
	}
	//
	public function normalizar_disc($disc, $sexo){
		if ( !is_array($disc) ) return;
		//
		// Normalizar resultado da analise
		$this->colDISC_convertido["D"] = $this->conversao_disc("D", $disc["D"]["A"], $sexo);
		$this->colDISC_convertido["I"] = $this->conversao_disc("I", $disc["I"]["A"], $sexo);
		$this->colDISC_convertido["S"] = $this->conversao_disc("S", $disc["S"]["A"], $sexo);
		$this->colDISC_convertido["C"] = $this->conversao_disc("C", $disc["C"]["A"], $sexo);
		//
		// Montar a serie
		$serie  = "[" . $this->colDISC_convertido["D"];
		$serie .= ',' . $this->colDISC_convertido["I"];
		$serie .= ',' . $this->colDISC_convertido["S"];
		$serie .= ',' . $this->colDISC_convertido["C"];
		$serie .= ']';
		$this->serie = $serie;
		//
		// Separa apenas o resultado
		$resultados = array();
		foreach($this->colDISC_convertido as $key=>$value) {
			if ( preg_match("/(D|I|S|C)/", $key) ){
				array_push($resultados, array($key => $value));
			}
		}
		// Ordena o resultado
		usort ( $resultados , function ($a, $b) {
				$keyA = key($a);
				$keyB = key($b);
				if($a[$keyA] == $b[$keyB]) {
					if ($keyA == "C" && preg_match("/(D|I|S)/", $keyB)){
						return 1;
					} elseif ($keyA == "S" && preg_match("/(D|I)/", $keyB)){
						return 1;
					} elseif ($keyA == "I" && preg_match("/D/", $keyB)){
						return 1;
					} else {
						return 0;
					}
				}
				return ($a[$keyA] > $b[$keyB]) ? -1 : 1;
			}
		);
		//
		// Resultado e Complemento
		$this->resultado 	= "";
		$this->complemento 	= "";
		$nr_equilibrio 		= 0;
		foreach($resultados as $disc) {
			foreach($disc as $key=>$value) {
				if ( $value > 50 ) {
					$this->resultado .= $key;
				}
				// ALTOS OU BAIXOS
				if ( ($value > 85) || ($value < 16) ) {
					//
					$this->complemento = "RD";
				}
				// EQUILIBRI0
				elseif ( $value < 59 && $value > 42 ) {
					//
					$nr_equilibrio++;
				}
			}
		}
		$letterDisc = $this->resultado;
		if ( strlen($letterDisc) == 3 ) {
			switch(substr($letterDisc,1,2)) {
				case "SI":
					$letterDisc = substr($letterDisc,0,1) . "IS";
					break;
				case "CS":
					$letterDisc = substr($letterDisc,0,1) . "SC";
					break;
				case "CI":
					$letterDisc = substr($letterDisc,0,1) . "IC";
					break;
				case "SD":
					$letterDisc = substr($letterDisc,0,1) . "DS";
					break;
				case "CD":
					$letterDisc = substr($letterDisc,0,1) . "DC";
					break;
				case "ID":
					$letterDisc = substr($letterDisc,0,1) . "DI";
					break;
			}
			$this->resultado = $letterDisc;
		}
		if ( $this->complemento == "" && $nr_equilibrio == 4){
			$this->complemento = "EQ";
		}
	}
	//
    private function conversao_disc($disc, $valor, $sexo) {
		//
		$retorno = "";
		//
		if ( preg_match("/(D|I|S|C)/", $disc ) ){
			// tratamento valor
			if ( !preg_match("/(M|F)/", $sexo ) ){
				// Valor padrao, caso o mesmo nao seja informado
				$sexo = "M";
			}
			//
			// Dados de conversao
			$cnvDados = array("M", "F");
			//
			$cnvDados["M"]["D"] = array(18 => 100
									  , 17 => 99
									  , 16 => 96
									  , 15 => 93
									  , 14 => 84
									  , 13 => 81
									  , 12 => 80
									  , 11 => 79
									  , 10 => 77
									  , 9 => 74
									  , 8 => 71
									  , 7 => 68
									  , 6 => 65
									  , 5 => 62
									  , 4 => 59
									  , 3 => 55
									  , 2 => 53
									  , 1 => 51
									  , 0 => 49
									  , -1 => 46
									  , -2 => 43
									  , -3 => 40
									  , -4 => 37
									  , -5 => 33
									  , -6 => 31
									  , -7 => 28
									  , -8 => 26
									  , -9 => 25
									  , -10 => 21
									  , -11 => 20
									  , -12 => 19
									  , -13 => 18
									  , -14 => 15
									  , -15 => 11
									  , -16 => 7
									  , -17 => 5
									  , -18 => 3
									  , -19 => 1);
			//
			$cnvDados["M"]["I"] = array(17 => 100
									  , 16 => 99
									  , 15 => 98
									  , 14 => 97
									  , 13 => 96
									  , 12 => 87
									  , 11 => 83
									  , 10 => 81
									  , 9 => 79
									  , 8 => 77
									  , 7 => 75
									  , 6 => 70
									  , 5 => 65
									  , 4 => 61
									  , 3 => 56
									  , 2 => 54
									  , 1 => 51
									  , 0 => 48
									  , -1 => 43
									  , -2 => 40
									  , -3 => 36
									  , -4 => 33
									  , -5 => 27
									  , -6 => 25
									  , -7 => 22
									  , -8 => 20
									  , -9 => 18
									  , -10 => 16
									  , -11 => 11
									  , -12 => 7
									  , -13 => 6
									  , -14 => 5
									  , -15 => 3
									  , -16 => 1);
			//
			$cnvDados["M"]["S"] = array(16 => 100
									  , 15 => 99
									  , 14 => 96
									  , 13 => 93
									  , 12 => 84
									  , 11 => 83
									  , 10 => 80
									  , 9 => 78
									  , 8 => 76
									  , 7 => 74
									  , 6 => 70
									  , 5 => 67
									  , 4 => 64
									  , 3 => 61
									  , 2 => 58
									  , 1 => 56
									  , 0 => 52
									  , -1 => 50
									  , -2 => 46
									  , -3 => 43
									  , -4 => 39
									  , -5 => 35
									  , -6 => 32
									  , -7 => 26
									  , -8 => 24
									  , -9 => 22
									  , -10 => 19
									  , -11 => 17
									  , -12 => 15
									  , -13 => 12
									  , -14 => 9
									  , -15 => 5
									  , -16 => 1);
			//
			$cnvDados["M"]["C"] = array(13 => 100
									  , 12 => 99
									  , 11 => 95
									  , 10 => 86
									  , 9 => 84
									  , 8 => 83
									  , 7 => 81
									  , 6 => 79
									  , 5 => 76
									  , 4 => 74
									  , 3 => 71
									  , 2 => 67
									  , 1 => 64
									  , 0 => 58
									  , -1 => 57
									  , -2 => 54
									  , -3 => 51
									  , -4 => 48
									  , -5 => 44
									  , -6 => 40
									  , -7 => 36
									  , -8 => 33
									  , -9 => 25
									  , -10 => 24
									  , -11 => 22
									  , -12 => 21
									  , -13 => 19
									  , -14 => 12
									  , -15 => 7
									  , -16 => 5
									  , -17 => 3
									  , -18 => 1);
			//
			$cnvDados["F"]["D"] = array(17 => 100
									  , 16 => 99
									  , 15 => 95
									  , 14 => 91
									  , 13 => 88
									  , 12 => 85
									  , 11 => 83
									  , 10 => 82
									  , 9 => 81
									  , 8 => 80
									  , 7 => 77
									  , 6 => 75
									  , 5 => 70
									  , 4 => 66
									  , 3 => 63
									  , 2 => 60
									  , 1 => 58
									  , 0 => 57
									  , -1 => 54
									  , -2 => 52
									  , -3 => 51
									  , -4 => 49
									  , -5 => 46
									  , -6 => 44
									  , -7 => 40
									  , -8 => 36
									  , -9 => 33
									  , -10 => 30
									  , -11 => 27
									  , -12 => 24
									  , -13 => 20
									  , -14 => 18
									  , -15 => 17
									  , -16 => 16
									  , -17 => 9
									  , -18 => 6
									  , -19 => 2
									  , -20 => 1);
			//
			$cnvDados["F"]["I"] = array(16 => 100
									  , 15 => 99
									  , 14 => 95
									  , 13 => 91
									  , 12 => 88
									  , 11 => 81
									  , 10 => 79
									  , 9 => 78
									  , 8 => 77
									  , 7 => 74
									  , 6 => 70
									  , 5 => 64
									  , 4 => 60
									  , 3 => 56
									  , 2 => 53
									  , 1 => 50
									  , 0 => 47
									  , -1 => 43
									  , -2 => 40
									  , -3 => 36
									  , -4 => 33
									  , -5 => 29
									  , -6 => 27
									  , -7 => 22
									  , -8 => 20
									  , -9 => 19
									  , -10 => 18
									  , -11 => 9
									  , -12 => 6
									  , -13 => 2
									  , -14 => 1);
			//
			$cnvDados["F"]["S"] = array(17 => 100
									  , 16 => 99
									  , 15 => 97
									  , 14 => 88
									  , 13 => 83
									  , 12 => 82
									  , 11 => 81
									  , 10 => 80
									  , 9 => 76
									  , 8 => 74
									  , 7 => 66
									  , 6 => 64
									  , 5 => 61
									  , 4 => 59
									  , 3 => 54
									  , 2 => 52
									  , 1 => 50
									  , 0 => 46
									  , -1 => 42
									  , -2 => 39
									  , -3 => 37
									  , -4 => 35
									  , -5 => 30
									  , -6 => 26
									  , -7 => 22
									  , -8 => 21
									  , -9 => 20
									  , -10 => 18
									  , -11 => 9
									  , -12 => 2
									  , -13 => 1);
			//
			$cnvDados["F"]["C"] = array(14 => 100
									  , 13 => 99
									  , 12 => 95
									  , 11 => 91
									  , 10 => 88
									  , 9 => 83
									  , 8 => 81
									  , 7 => 79
									  , 6 => 76
									  , 5 => 74
									  , 4 => 70
									  , 3 => 65
									  , 2 => 62
									  , 1 => 59
									  , 0 => 54
									  , -1 => 53
									  , -2 => 51
									  , -3 => 48
									  , -4 => 42
									  , -5 => 37
									  , -6 => 34
									  , -7 => 31
									  , -8 => 27
									  , -9 => 25
									  , -10 => 21
									  , -11 => 19
									  , -12 => 18
									  , -13 => 16
									  , -14 => 12
									  , -15 => 6
									  , -16 => 4
									  , -17 => 2
									  , -18 => 1);
			//
			$cnv = $cnvDados[$sexo][$disc];
			//
			if ( array_key_exists($valor, $cnv) ){
				$retorno = $cnv[$valor];
			} else {
				//
				//$maximo = reset($cnv);
				$maximo = array_search(100, $cnv);
				//$minimo = end($cnv);
				$minimo = array_search(1, $cnv);
				if ( $valor > $maximo ) {
					$retorno = 100;
				} elseif ( $valor < $minimo ) {
					$retorno = 1;
				}
			}
		}
		//
		return $retorno;
	}	
}
// AJRB - 03/04/2017 - fim


// Classe principal ////////////////////////////////////////////////////////////

class DEFWEBsurvey {

    // Dados da avalia��o
    public $id_avaliacao = null;
    public $id_cliente = null;
    public $ds_titulo_avaliacao = null;
    public $cd_sit_avaliacao = null;
    public $cd_publico_alvo = null;
    public $dt_inicio_avaliacao = null;
    public $dt_inicio_avaliacao_fmt = null;
    public $dt_fim_avaliacao = null;
    public $dt_fim_avaliacao_fmt = null;
    public $dt_inicio_token = null;
    public $dt_termino_token = null;
    public $dt_ult_acesso_token = null;
	public $fl_concluido = null;
	public $IMGLogoID = null;
	public $IMGLogoName = null;
    //
    public $texto_introducao = null;
    public $periodo_aplicacao = null;
    
        // Email de conclusao
    public $fl_enviar_email_conclusao = 0;
    public $ds_email_subj_conclusao = null;
    public $ds_email_body_conclusao = null;
	
	// Paginas
    public $pg_cad_ds		= "templates/1/basico/dssurvey_cad.php";
    public $pg_cad_dv		= 1;
    public $pg_instr_ds		= "templates/2/basico/dssurvey_instr.php";
    public $pg_instr_dv		= 1;
    public $pg_qst_ds		= "templates/3/basico/dssurvey_qst.php";
    public $pg_qst_dv		= 1;
    public $pg_concl_ds		= "templates/4/basico/dssurvey_concl.php";
    public $pg_concl_dv		= 1;
    public $pg_rpt_ds		= "templates/5/basico/dssurvey_sem_rpt.php";
    public $pg_rpt_dv		= 1;
    public $pg_rpt_adm_ds	= "templates/5/basico/dssurvey_com_rpt_bas.php";
    public $pg_rpt_adm_dv	= 1;
    
    // Eu mantive os atributos acima para que sejam usados como padrao
    // caso o tipo da pagina seja 0 (ND)
    
    // Configuracao das paginas
    // 
    // Confirmacao de cadastro
    public $pgcad_cd_pagina = null;
    public $pgcad_ds_pagina = null;
    public $pgcad_cd_tipo = null;
    public $pgcad_dv_include = 0;
    // Instrucoes
    public $pgins_cd_pagina = null;
    public $pgins_ds_pagina = null;
    public $pgins_cd_tipo = null;
    public $pgins_dv_include = 0;
    // Questionario
    public $pgqst_cd_pagina = null;
    public $pgqst_ds_pagina = null;
    public $pgqst_cd_tipo = null;
    public $pgqst_dv_include = 0;
    // Conclusao
    public $pgcon_cd_pagina = null;
    public $pgcon_ds_pagina = null;
    public $pgcon_cd_tipo = null;
    public $pgcon_dv_include = 0;
    // Relatorio para o entrevistado
    public $pgrel_cd_pagina = null;
    public $pgrel_ds_pagina = null;
    public $pgrel_cd_tipo = null;
    public $pgrel_dv_include = 0;
    // Relatorio para a administracao
    public $pgadm_cd_pagina = null;
    public $pgadm_ds_pagina = null;
    public $pgadm_cd_tipo = null;
    public $pgadm_dv_include = 0;
    
    
    // Identifica��o do entrevistado
    public $id_token = null;
    public $ds_nome_token = null;
    public $ds_unidade = null;
	// AJRB - 31/03/2017 - inicio
	public $ds_email_token 		= null;
	public $dv_sexo_token 		= null;
	public $dt_nascimento_token = null;
	public $nr_cep_token 		= null;
	public $nr_telefone_token 	= null;
	// AJRB - 31/03/2017 - fim

    // Resumo
    public $qt_respondidas = null;
    public $qt_nao_respondidas = null;
    public $qt_total = null;
    public $pc_respondidas = null;
    public $id_proxima_pergunta = null;
    public $id_primeira_pergunta = null;
    
    // Matriz de perguntas
    public $colPerguntas = null;

    // Reposit�rio de mensagens
    public $colMensagens = null;
    
    // Reposit�rio de respostas
    public $colRespostas = null;
    
    // Matriz DISC
    public $colDISC = null;
    
    // Atributos que reportam os erros
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    function __construct($PARM_id_avaliacao, $PARM_id_token) {
        //
        $this->carregarMensagens();
        //
        if(($PARM_id_avaliacao)&&($PARM_id_token)) {
            //
            $this->carregarDadosDaAvaliacao($PARM_id_avaliacao, $PARM_id_token);
        }
    }
    
    public function carregarMensagens() {
        //
        unset($this->colMensagens);
        //
        $DW_MSG_ERRO_HEADER = "<b>ATEN&Ccedil;&Atilde;O:</b><br />";
        //
        $this->colMensagens["MSG_ERRO_HEADER"] = $DW_MSG_ERRO_HEADER;
        //
        $DW_MSG_ERRO_FOOTER = "<br /><br />";
        //
        $this->colMensagens["MSG_ERRO_FOOTER"] = $DW_MSG_ERRO_FOOTER;
        //
        $DW_MSG_ERRO_100PC = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_100PC .= "A soma dos valores dos campos ";
        $DW_MSG_ERRO_100PC .= "deveria resultar em 100%!<br />";
        $DW_MSG_ERRO_100PC .= "Reveja os valores antes de enviar a resposta.";
        $DW_MSG_ERRO_100PC .= $DW_MSG_ERRO_FOOTER;
        //
        $this->colMensagens["MSG_ERRO_100PC"] = $DW_MSG_ERRO_100PC;
        //
        $DW_MSG_ERRO_SEMRESPOSTA = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_SEMRESPOSTA .= "Voc&ecirc; deve responder esta pergunta para ";
        $DW_MSG_ERRO_SEMRESPOSTA .= "avan&ccedil;ar no question&aacute;rio!<br />";
        $DW_MSG_ERRO_SEMRESPOSTA .= $DW_MSG_ERRO_FOOTER;
        //
        $this->colMensagens["MSG_ERRO_SEMRESPOSTA"] = $DW_MSG_ERRO_SEMRESPOSTA;
        //
        $DW_MSG_ERRO_CAMPOVAZIO = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_CAMPOVAZIO .= "Voc&ecirc; deve preencher todos os campos para ";
        $DW_MSG_ERRO_CAMPOVAZIO .= "avan&ccedil;ar no question&aacute;rio!<br />";
        $DW_MSG_ERRO_CAMPOVAZIO .= $DW_MSG_ERRO_FOOTER;
        //
        $this->colMensagens["MSG_ERRO_CAMPOVAZIO"] = $DW_MSG_ERRO_CAMPOVAZIO;
        //
        $DW_MSG_HEADER = "<br /><br /><b>INFORMA&Ccedil;&Atilde;O:</b><br />";
        $DW_MSG_HEADER .= "<br />";
        //
        $this->colMensagens["MSG_HEADER"] = $DW_MSG_HEADER;
        //
        $DW_MSG_ERRO_HEADER = "<br /><br /><b>ATEN&Ccedil;&Atilde;O:</b><br />";
        $DW_MSG_ERRO_HEADER .= "<br />";
        //
        $this->colMensagens["MSG_ERRO_HEADER"] = $DW_MSG_ERRO_HEADER;
        //
        $DW_MSG_ERRO_FOOTER = "<br />";
        $DW_MSG_ERRO_FOOTER .= "Tente novamente mais tarde, caso o problema ";
        $DW_MSG_ERRO_FOOTER .= "persista entre em contato com o suporte.";
        //
        $this->colMensagens["MSG_ERRO_FOOTER"] = $DW_MSG_ERRO_FOOTER;
        //
        $DW_MSG_ERRO_FALHACONEXAOBD = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_FALHACONEXAOBD .= "Falhou na conex&atilde;o com o banco de dados!<br />";
        $DW_MSG_ERRO_FALHACONEXAOBD .= $DW_MSG_ERRO_FOOTER;
        //
        $this->colMensagens["MSG_ERRO_FALHACONEXAOBD"] = $DW_MSG_ERRO_FALHACONEXAOBD;
        //
        $DW_MSG_ERRO_FALHACONSULTABD = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_FALHACONSULTABD .= "Falhou na consulta ao banco de dados!<br />";
        $DW_MSG_ERRO_FALHACONSULTABD .= $DW_MSG_ERRO_FOOTER;
        //
        $this->colMensagens["MSG_ERRO_FALHACONSULTABD"] = $DW_MSG_ERRO_FALHACONSULTABD;
        //
        $DW_MSG_ERRO_FALHADADOSNAOENCONTRADOS = $DW_MSG_ERRO_HEADER;
        $DW_MSG_ERRO_FALHADADOSNAOENCONTRADOS .= "Dados n&atilde;o encontrados no banco de dados!<br />";
        $DW_MSG_ERRO_FALHADADOSNAOENCONTRADOS .= $DW_MSG_ERRO_FOOTER;//
        //
        $this->colMensagens["MSG_ERRO_FALHADADOSNAOENCONTRADOS"] = $DW_MSG_ERRO_FALHADADOSNAOENCONTRADOS;
        //
        $DW_MSG_QUESTIONARIO_CONCLUIDO = $DW_MSG_HEADER;
        $DW_MSG_QUESTIONARIO_CONCLUIDO .= "O question&aacute;rio j&aacute; foi respondido!<br />";
        $DW_MSG_QUESTIONARIO_CONCLUIDO .= "<br />";
        $DW_MSG_QUESTIONARIO_CONCLUIDO .= "Voc� poder� revisar suas respostas at� o fechamento da pesquisa.<br />";
        //$DW_MSG_QUESTIONARIO_CONCLUIDO .= "<br />";
        //$DW_MSG_QUESTIONARIO_CONCLUIDO .= "Agradecemos a sua colabora&ccedil;&atilde;o.";
        //
        $this->colMensagens["MSG_QUESTIONARIO_CONCLUIDO"] = $DW_MSG_QUESTIONARIO_CONCLUIDO;
        //
        $DW_MSG_PESQUISA_ENCERRADA = $DW_MSG_HEADER;
        $DW_MSG_PESQUISA_ENCERRADA .= "A pesquisa j� foi encerrada!";
        //
        $this->colMensagens["MSG_PESQUISA_ENCERRADA"] = $DW_MSG_PESQUISA_ENCERRADA;
        //
        $DW_MSG_PESQUISA_NAOLIBERADA = $DW_MSG_HEADER;
        $DW_MSG_PESQUISA_NAOLIBERADA .= "A pesquisa ainda n�o est� dispon�vel!";
        //
        $this->colMensagens["MSG_PESQUISA_NAOLIBERADA"] = $DW_MSG_PESQUISA_NAOLIBERADA;
    }
    
    public function obterMensagem($PARM_codigo) {
        // Converte tudo para letras mai�sculas e
        // Obt�m o texto da mensagem
        $RET_mensagem = $this->colMensagens[strtoupper($PARM_codigo)];
        // Retorno da fun��o
        return($RET_mensagem);
    }
    
    /*
     *  criarAvaliacaoToken( <Id da Avaliacao>, <Id do Token> )
     * 
     *  Cria o registro que faz a liga��o entre os formul�rios da avalia��o
     *  e o token (identifica��o) do entrevistado.
     * 
     *  INSERT na tbdw_aval_form_token, deve ser feito para que o banco de dados
     *  permita a grava��o das respostas do usu�rio.
     * 
     *  Retorna zero se obteve sucesso.
     */
    
    public function criarAvaliacaoToken($PARM_id_avaliacao, $PARM_id_token) {
        //
        if(DEFWEBsv_debug) {
            $this->DBG_db_type = DEFWEBsv_db_type;
            $this->DBG_db_host = DEFWEBsv_db_host;
            $this->DBG_db_name = DEFWEBsv_db_name;
            $this->DBG_db_user = DEFWEBsv_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBsv_db_host, DEFWEBsv_db_user, DEFWEBsv_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBsv_db_name)) {
                //
                // Monta o INSERT
                $sSQL = "INSERT tbdw_aval_form_token";
                $sSQL .= " (id_avaliacao_formulario, id_avaliacao, id_cliente, id_formulario, id_token, fl_concluido, dt_inicio)";
                $sSQL .= " SELECT";
                $sSQL .= " af.id_avaliacao_formulario, af.id_avaliacao, af.id_cliente, af.id_formulario,";
                $sSQL .= " tk.id_token, 0, NOW()";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_aval_token AS tk";
                $sSQL .= " LEFT";
                $sSQL .= " JOIN tbdw_avaliacao_formulario AS af";
                $sSQL .= " ON af.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                $sSQL .= " AND af.cd_publico_alvo = tk.cd_publico_alvo";
                $sSQL .= " WHERE";
                $sSQL .= " tk.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                $sSQL .= " AND tk.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " AND af.id_avaliacao_formulario NOT IN (";
                $sSQL .= " SELECT af2.id_avaliacao_formulario FROM tbdw_aval_form_token AS af2";
                $sSQL .= " WHERE af2.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                $sSQL .= " AND af2.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " )";  
                //
                // Executa o comando
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        // Retorno da fun��o
        return($this->ERRO);
    }
    
    /*
     * carregarDadosDaAvaliacao( <Id da Avaliacao>, <Id do Token> )
     * 
     * Obt�m todas as informa��es necess�rias para a aplica��o do question�rio:
     * 
     *  1. Dados da avalia��o
     *  2. Todas as perguntas
     *  3. Quantidades
     * 
     */
    
    public function carregarDadosDaAvaliacao( $PARM_id_avaliacao, $PARM_id_token ) {
        //
        $this->id_avaliacao = $PARM_id_avaliacao;
        $this->id_token = $PARM_id_token;
        //
        if(DEFWEBsv_debug) {
            $this->DBG_db_type = DEFWEBsv_db_type;
            $this->DBG_db_host = DEFWEBsv_db_host;
            $this->DBG_db_name = DEFWEBsv_db_name;
            $this->DBG_db_user = DEFWEBsv_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBsv_db_host, DEFWEBsv_db_user, DEFWEBsv_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBsv_db_name)) {
                //
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " af.id_avaliacao AS id_avaliacao, ";
                $sSQL .= " av.ds_titulo AS ds_titulo_avaliacao, ";
                $sSQL .= " av.dt_inicio AS dt_inicio_avaliacao, ";
                $sSQL .= " av.dt_fim AS dt_fim_avaliacao, ";
                $sSQL .= " av.cd_sit_avaliacao AS cd_sit_avaliacao, ";
                $sSQL .= " av.ds_franqueador_intro AS ds_franqueador_intro, ";
                $sSQL .= " av.ds_franqueado_intro AS ds_franqueado_intro, ";
				$sSQL .= " av.nmarq_topo_interno AS IMGLogoID, ";
				$sSQL .= " av.nmarq_topo_original AS IMGLogoName, ";
                $sSQL .= " af.id_avaliacao_formulario AS id_avaliacao_formulario, ";
                $sSQL .= " af.id_formulario AS id_formulario, ";
                $sSQL .= " form.ds_formulario AS ds_formulario, ";
                $sSQL .= " form.cd_sit_formulario AS cd_sit_formulario, ";
                $sSQL .= " sit.ds_sit_formulario AS ds_sit_formulario, ";
                $sSQL .= " af.id_cliente AS id_cliente, ";
                $sSQL .= " cl.cd_cliente AS cd_cliente, ";
                $sSQL .= " cl.ds_cliente AS ds_cliente, ";
                $sSQL .= " af.dt_inicio AS dt_inicio, ";
                $sSQL .= " af.dt_fim AS dt_fim, ";
                $sSQL .= " af.cd_publico_alvo AS cd_publico_alvo, ";
                $sSQL .= " pa.ds_publico_alvo AS ds_publico_alvo, ";
                $sSQL .= " itemf.id_item_formulario AS id_item_formulario, ";
                $sSQL .= " itemf.id_formulario AS id_formulario, ";
                $sSQL .= " itemf.cd_agrupamento AS cd_agrupamento, ";
                $sSQL .= " ag.ds_agrupamento AS ds_agrupamento, ";
                $sSQL .= " itemf.cd_tema AS cd_tema, ";
                $sSQL .= " tema.ds_tema AS ds_tema, ";
                $sSQL .= " itemf.cd_item AS cd_item, ";
                $sSQL .= " item.ds_item AS ds_item, ";
                $sSQL .= " item.dv_tipo_opcoes AS dv_tipo_opcoes, ";
                $sSQL .= " item.qt_limite_resp AS qt_limite_resp, ";
                $sSQL .= " itemf.id_versao AS id_versao, ";
                $sSQL .= " itemf.nr_posicao AS nr_posicao, ";
                $sSQL .= " token.id_token AS id_token, ";
                $sSQL .= " token.ds_codigo AS ds_codigo_token, ";
                $sSQL .= " token.ds_nome AS ds_nome_token, ";
                $sSQL .= " token.ds_unidade AS ds_unidade, ";
                $sSQL .= " token.ds_email AS ds_email_token, ";
				$sSQL .= " token.dv_sexo AS dv_sexo_token, ";
				$sSQL .= " DATE_FORMAT(token.dt_nascimento, '%d/%m/%Y') AS dt_nascimento_token, ";
				$sSQL .= " token.nr_cep AS nr_cep_token, ";
				$sSQL .= " nr_telefone AS nr_telefone_token, ";
                $sSQL .= " token.ds_observacao AS ds_observacao_token, ";
                $sSQL .= " resp.id_aval_form_item AS id_aval_form_item, ";
                $sSQL .= " resp.ds_resposta AS ds_resposta, ";
                $sSQL .= " resp.dt_resposta AS dt_resposta ";
                $sSQL .= ", avtk.dt_inicio AS dt_inicio_token ";
                $sSQL .= ", avtk.dt_termino AS dt_termino_token ";
                $sSQL .= ", avtk.dt_ult_acesso AS dt_ult_acesso_token ";
				$sSQL .= ", avtk.fl_concluido AS fl_concluido ";
                //
                // MPM - 13/04/2017 - inicio
                $sSQL .= ", pgcad.cd_pagina AS pgcad_cd_pagina";
                $sSQL .= ", pgcad.ds_pagina AS pgcad_ds_pagina";
                $sSQL .= ", pgcad.cd_tipo_pagina AS pgcad_cd_tipo";
                $sSQL .= ", pgcad.dv_include AS pgcad_dv_include";
                $sSQL .= ", pgins.cd_pagina AS pgins_cd_pagina";
                $sSQL .= ", pgins.ds_pagina AS pgins_ds_pagina";
                $sSQL .= ", pgins.cd_tipo_pagina AS pgins_cd_tipo";
                $sSQL .= ", pgins.dv_include AS pgins_dv_include";
                $sSQL .= ", pgqst.cd_pagina AS pgqst_cd_pagina";
                $sSQL .= ", pgqst.ds_pagina AS pgqst_ds_pagina";
                $sSQL .= ", pgqst.cd_tipo_pagina AS pgqst_cd_tipo";
                $sSQL .= ", pgqst.dv_include AS pgqst_dv_include";
                $sSQL .= ", pgcon.cd_pagina AS pgcon_cd_pagina";
                $sSQL .= ", pgcon.ds_pagina AS pgcon_ds_pagina";
                $sSQL .= ", pgcon.cd_tipo_pagina AS pgcon_cd_tipo";
                $sSQL .= ", pgcon.dv_include AS pgcon_dv_include";
                $sSQL .= ", pgrel.cd_pagina AS pgrel_cd_pagina";
                $sSQL .= ", pgrel.ds_pagina AS pgrel_ds_pagina";
                $sSQL .= ", pgrel.cd_tipo_pagina AS pgrel_cd_tipo";
                $sSQL .= ", pgrel.dv_include AS pgrel_dv_include";
                $sSQL .= ", pgadm.cd_pagina AS pgadm_cd_pagina";
                $sSQL .= ", pgadm.ds_pagina AS pgadm_ds_pagina";
                $sSQL .= ", pgadm.cd_tipo_pagina AS pgadm_cd_tipo";
                $sSQL .= ", pgadm.dv_include AS pgadm_dv_include";
                // MPM - 13/04/2017 - fim
                // 
                // MPM - 25/06/2018 - inicio {suporte para e-mail de conclusao automatico}
                // 
                $sSQL .= ", av.fl_enviar_email_conclusao AS fl_enviar_email_conclusao";
                $sSQL .= ", av.ds_email_subj_conclusao AS ds_email_subj_conclusao";
                $sSQL .= ", av.ds_email_body_conclusao AS ds_email_body_conclusao";
                // 
                // MPM - 25/06/2018 - fim
                //
                $sSQL .= " FROM tbdw_avaliacao_formulario AS af ";
                $sSQL .= " LEFT JOIN tbdw_avaliacao AS av ON av.id_avaliacao = af.id_avaliacao ";
                $sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON itemf.id_formulario = af.id_formulario ";
                $sSQL .= " LEFT JOIN tbdw_aval_token AS token ON token.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON resp.id_item_formulario = itemf.id_item_formulario ";
                $sSQL .= " AND resp.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = af.id_cliente ";
                $sSQL .= " LEFT JOIN tbdw_item as item ON item.cd_item = itemf.cd_item AND item.cd_agrupamento = itemf.cd_agrupamento AND item.cd_tema = itemf.cd_tema AND item.id_versao = itemf.id_versao ";
                $sSQL .= " LEFT JOIN tbdw_agrupamento AS ag ON ag.cd_agrupamento = itemf.cd_agrupamento ";
                $sSQL .= " LEFT JOIN tbdw_tema AS tema ON tema.cd_tema = itemf.cd_tema AND tema.cd_agrupamento = itemf.cd_agrupamento ";
                $sSQL .= " LEFT JOIN tbdw_formulario as form ON form.id_formulario = af.id_formulario ";
                $sSQL .= " LEFT JOIN tbdw_sit_formulario AS sit ON sit.cd_sit_formulario = form.cd_sit_formulario ";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = af.cd_publico_alvo ";
                $sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON avtk.id_avaliacao = af.id_avaliacao ";
                $sSQL .= " AND avtk.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                //
                // MPM - 13/04/2017 - inicio
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgcad ON pgcad.id_pagina = av.id_pagina_confcad";
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgins ON pgins.id_pagina = av.id_pagina_instrucoes";
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgqst ON pgqst.id_pagina = av.id_pagina_questionario";
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgcon ON pgcon.id_pagina = av.id_pagina_conclusao";
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgrel ON pgrel.id_pagina = av.id_pagina_relatorio";
                $sSQL .= " LEFT JOIN tbdw_pagina AS pgadm ON pgadm.id_pagina = av.id_pagina_reladmin";
                // MPM - 13/04/2017 - fim
                //
                $sSQL .= " WHERE af.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                $sSQL .= " AND af.cd_publico_alvo = token.cd_publico_alvo";
                $sSQL .= " ORDER BY af.id_avaliacao_formulario, itemf.cd_tema, itemf.cd_agrupamento, itemf.nr_posicao, itemf.id_item_formulario ";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    // Ajusta a mensagem
                    $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONSULTABD");
                    $this->ERRO = 1;
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if(!$rsDados) {
                        // Ajusta a mensagem
                        $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHADADOSNAOENCONTRADOS"); ;
                        $this->ERRO = 1;
//                        //
//                        $this->ERRO_mensagem .= "<br>carregarDadosAvaliacao()<hr>" . $sSQL;
//                        //
                    // Senao, obteve a primeira linha do result set...
                    } else {
                        //
                        // Extrai os dados da avalia��o
                        $this->id_cliente = $rsDados["id_cliente"];
                        $this->ds_titulo_avaliacao = $rsDados["ds_titulo_avaliacao"];
                        $this->cd_sit_avaliacao = $rsDados["cd_sit_avaliacao"];
                        $this->cd_publico_alvo = $rsDados["cd_publico_alvo"];
                        $this->dt_inicio_avaliacao = $rsDados["dt_inicio_avaliacao"];
                        $this->dt_fim_avaliacao = $rsDados["dt_fim_avaliacao"];
                        $this->dt_inicio_token = $rsDados["dt_inicio_token"];
                        $this->dt_termino_token = $rsDados["dt_termino_token"];
                        $this->dt_ult_acesso_token = $rsDados["dt_ult_acesso_token"];
						$this->fl_concluido = $rsDados["fl_concluido"];
						$this->IMGLogoID = $rsDados["IMGLogoID"];
						$this->IMGLogoName = $rsDados["IMGLogoName"];
                        // MPM - 25/06/2018 - inicio
                        $this->fl_enviar_email_conclusao = $rsDados["fl_enviar_email_conclusao"];
                        $this->ds_email_subj_conclusao = $rsDados["ds_email_subj_conclusao"];
                        $this->ds_email_body_conclusao = $rsDados["ds_email_body_conclusao"];
                        // MPM - 25/06/2018 - fim
                        //
                        // Monta o texto do per�odo de aplica��o
                        $this->periodo_aplicacao = "";
                        //
                        $this->dt_inicio_avaliacao_fmt = "";
                        if((!is_null($this->dt_inicio_avaliacao))&&($this->dt_inicio_avaliacao!="")) {
                            $this->dt_inicio_avaliacao_fmt = date("d/m/Y", strtotime($this->dt_inicio_avaliacao));
                        }
                        $this->dt_fim_avaliacao_fmt = "";
                        if((!is_null($this->dt_fim_avaliacao))&&($this->dt_fim_avaliacao!="")) {
                            $this->dt_fim_avaliacao_fmt = date("d/m/Y", strtotime($this->dt_fim_avaliacao));
                        }
                        //
                        if(($this->dt_inicio_avaliacao_fmt!="")||($this->dt_fim_avaliacao_fmt!="")) {
                            // Esta pesquisa podera ser respondida
                            $this->periodo_aplicacao = "Esta pesquisa poder&aacute; ser respondida ";
                            //
                            if(($this->dt_inicio_avaliacao_fmt!="")&&($this->dt_fim_avaliacao_fmt!="")) {
                                //  no periodo de XX ate XX.
                                $this->periodo_aplicacao .= "no per&iacute;odo";
                                $this->periodo_aplicacao .= " de " . $this->dt_inicio_avaliacao_fmt;
                                $this->periodo_aplicacao .= " at&eacute; " . $this->dt_fim_avaliacao_fmt;
                                //
                            } elseif($this->dt_inicio_avaliacao_fmt!="") {
                                // a partir de XX.
                                $this->periodo_aplicacao .= "a partir ";
                                $this->periodo_aplicacao .= "de " . $this->dt_inicio_avaliacao_fmt;
                                //
                            } elseif($this->dt_fim_avaliacao_fmt) {
                                //ate XX.
                                $this->periodo_aplicacao .= "at&eacute; " . $this->dt_fim_avaliacao_fmt;
                            }
                            //
                            $this->periodo_aplicacao .= ".";
                        }
                        //
                        // Extrai os dados do entrevistado
                        $this->ds_nome_token = $rsDados["ds_nome_token"];
                        $this->ds_unidade = $rsDados["ds_unidade"];
						// AJRB - 31/03/2017 - inicio
						$this->ds_email_token 		= $rsDados["ds_email_token"];
						$this->dv_sexo_token 		= $rsDados["dv_sexo_token"];
						$this->dt_nascimento_token 	= $rsDados["dt_nascimento_token"];
						$this->nr_cep_token 		= $rsDados["nr_cep_token"];
						$this->nr_telefone_token 	= $rsDados["nr_telefone_token"];
						// AJRB - 31/03/2017 - fim

                        // MPM - 13/04/2017 - inicio

                        // Configuracao das paginas
                        //
                        // Confirmacao de cadastro
                        //
                        $this->pgcad_cd_pagina = $rsDados["pgcad_cd_pagina"];
                        $this->pgcad_ds_pagina = $rsDados["pgcad_ds_pagina"];;
                        $this->pgcad_cd_tipo = $rsDados["pgcad_cd_tipo"];;
                        $this->pgcad_dv_include = $rsDados["pgcad_dv_include"];;
                        // Se nao for include...
                        if($this->pgcad_dv_include == 0) {
                            // Substitui as macros
                            $this->pgcad_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgcad_ds_pagina);
                            $this->pgcad_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgcad_ds_pagina);
                            //$this->pgcad_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgcad_ds_pagina);
                            //$this->pgcad_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgcad_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgcad_ds_pagina = str_replace("\n", "<br />", $this->pgcad_ds_pagina);
                        }
                        //
                        // Instrucoes
                        //
                        $this->pgins_cd_pagina = $rsDados["pgins_cd_pagina"];
                        $this->pgins_ds_pagina = $rsDados["pgins_ds_pagina"];;
                        $this->pgins_cd_tipo = $rsDados["pgins_cd_tipo"];;
                        $this->pgins_dv_include = $rsDados["pgins_dv_include"];;
                        // Se nao for include...
                        if($this->pgins_dv_include == 0) {
                            // Substitui as macros
                            $this->pgins_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgins_ds_pagina);
                            $this->pgins_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgins_ds_pagina);
                            //$this->pgins_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgins_ds_pagina);
                            //$this->pgins_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgins_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgins_ds_pagina = str_replace("\n", "<br />", $this->pgins_ds_pagina);
                        }
                        //
                        // Questionario
                        //
                        $this->pgqst_cd_pagina = $rsDados["pgqst_cd_pagina"];
                        $this->pgqst_ds_pagina = $rsDados["pgqst_ds_pagina"];;
                        $this->pgqst_cd_tipo = $rsDados["pgqst_cd_tipo"];;
                        $this->pgqst_dv_include = $rsDados["pgqst_dv_include"];;
                        // Se nao for include...
                        if($this->pgqst_dv_include == 0) {
                            // Substitui as macros
                            $this->pgqst_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgqst_ds_pagina);
                            $this->pgqst_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgqst_ds_pagina);
                            //$this->pgqst_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgqst_ds_pagina);
                            //$this->pgqst_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgqst_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgqst_ds_pagina = str_replace("\n", "<br />", $this->pgqst_ds_pagina);
                        }
                        //
                        // Conclusao
                        //
                        $this->pgcon_cd_pagina = $rsDados["pgcon_cd_pagina"];
                        $this->pgcon_ds_pagina = $rsDados["pgcon_ds_pagina"];;
                        $this->pgcon_cd_tipo = $rsDados["pgcon_cd_tipo"];;
                        $this->pgcon_dv_include = $rsDados["pgcon_dv_include"];;
                        // Se nao for include...
                        if($this->pgcon_dv_include == 0) {
                            // Substitui as macros
                            $this->pgcon_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgcon_ds_pagina);
                            $this->pgcon_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgcon_ds_pagina);
                            //$this->pgcon_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgcon_ds_pagina);
                            //$this->pgcon_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgcon_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgcon_ds_pagina = str_replace("\n", "<br />", $this->pgcon_ds_pagina);
                        }
                        //
                        // Relatorio para o entrevistado
                        //
                        $this->pgrel_cd_pagina = $rsDados["pgrel_cd_pagina"];
                        $this->pgrel_ds_pagina = $rsDados["pgrel_ds_pagina"];;
                        $this->pgrel_cd_tipo = $rsDados["pgrel_cd_tipo"];;
                        $this->pgrel_dv_include = $rsDados["pgrel_dv_include"];;
                        // Se nao for include...
                        if($this->pgrel_dv_include == 0) {
                            // Substitui as macros
                            $this->pgrel_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgrel_ds_pagina);
                            $this->pgrel_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgrel_ds_pagina);
                            //$this->pgrel_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgrel_ds_pagina);
                            //$this->pgrel_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgrel_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgrel_ds_pagina = str_replace("\n", "<br />", $this->pgrel_ds_pagina);
                        }
                        //
                        // Relatorio para a administracao
                        //
                        $this->pgadm_cd_pagina = $rsDados["pgadm_cd_pagina"];
                        $this->pgadm_ds_pagina = $rsDados["pgadm_ds_pagina"];;
                        $this->pgadm_cd_tipo = $rsDados["pgadm_cd_tipo"];;
                        $this->pgadm_dv_include = $rsDados["pgadm_dv_include"];;
                        // Se nao for include...
                        if($this->pgadm_dv_include == 0) {
                            // Substitui as macros
                            $this->pgadm_ds_pagina = str_replace("{NOME}", $this->ds_nome_token, $this->pgadm_ds_pagina);
                            $this->pgadm_ds_pagina = str_replace("{UNIDADE}", $this->ds_unidade, $this->pgadm_ds_pagina);
                            //$this->pgadm_ds_pagina = str_replace("{DT-INICIO}", $sLink, $this->pgadm_ds_pagina);
                            //$this->pgadm_ds_pagina = str_replace("{DT-FIM}", $sLink, $this->pgadm_ds_pagina);
                            //
                            // Substitui as quebras de linha do php pelas do html
                            $this->pgadm_ds_pagina = str_replace("\n", "<br />", $this->pgadm_ds_pagina);
                        }
                        // MPM - 13/04/2017 - fim
                        
                        // 
                        // Zera os contadores
                        $this->qt_respondidas = 0;
                        $this->qt_nao_respondidas = 0;
                        $this->qt_total = 0;
                        $this->pc_respondidas = 0;
                        //
                        // Limpa a matriz de perguntas
                        unset($this->colPerguntas);
                        //
                        $ID_pergunta_anterior = null;
                        //
                        // Enquanto houver linhas no result set
                        while ($rsDados) {
                            //
                            unset($objPergunta);
                            $objPergunta = new DEFWEBsvPergunta();
                            // Salva os dados da pergunta
                            $objPergunta->id_item_formulario = $rsDados["id_item_formulario"];
                            $objPergunta->cd_agrupamento = $rsDados["cd_agrupamento"];
                            $objPergunta->ds_agrupamento = $rsDados["ds_agrupamento"];
                            $objPergunta->cd_tema = $rsDados["cd_tema"];
                            $objPergunta->ds_tema = $rsDados["ds_tema"];
                            $objPergunta->cd_item = $rsDados["cd_item"];
                            $objPergunta->ds_item = $rsDados["ds_item"];
                            $objPergunta->dv_tipo_opcoes = $rsDados["dv_tipo_opcoes"];
                            $objPergunta->qt_limite_resp = $rsDados["qt_limite_resp"];
                            $objPergunta->id_versao = $rsDados["id_versao"];
                            $objPergunta->id_aval_form_item = $rsDados["id_aval_form_item"];

                            // Identifica��o da pergunta anterior
                            $objPergunta->id_anterior = $ID_pergunta_anterior;
                            
                            // Se houver pergunta anterior...
                            if(!is_null($objPergunta->id_anterior)) {
                                //
                                // Marca qual ser� a pr�xima pergunta
                                $this->colPerguntas[$objPergunta->id_anterior]->id_proxima = $objPergunta->id_item_formulario;
                            }
                            //
                            $ID_pergunta_anterior = $objPergunta->id_item_formulario;
                            //
                            // Salva a pergunta atual
                            $this->colPerguntas[$objPergunta->id_item_formulario] = $objPergunta;
                            
                            // Incrementa o contador
                            $this->qt_total++;
                            // Armazena a posi��o da pergunta
                            $objPergunta->nr_pergunta = $this->qt_total;
                            //
                            if(is_null($this->id_primeira_pergunta)) {
                                //
                                $this->id_primeira_pergunta = $objPergunta->id_item_formulario;
                            }
                            // Se a pergunta n�o foi respondida...
                            if(is_null($rsDados["dt_resposta"])) {
                                // Marca como n�o respondida
                                $objPergunta->flag_respondida = FALSE;
                                // Incrementa o contador de n�o respondidas
                                $this->qt_nao_respondidas++;
                                // Se o contador for 1, significa que � a
                                // pr�xima a ser respondida
                                if($this->qt_nao_respondidas == 1) {
                                    // Salva a identificacao desta pergunta
                                    // na avalia��o
                                    $this->id_proxima_pergunta = $objPergunta->id_item_formulario;
                                }
                            // Sen�o
                            } else {
                                $objPergunta->flag_respondida = TRUE;
                                // Incrementa o contador de respondidas
                                $this->qt_respondidas++;
                            }
                            // Enquanto for o mesmo ID...
                            while(($rsDados)&&($rsDados["id_item_formulario"] == $objPergunta->id_item_formulario)) {
                                // L� a pr�xima linha do result set
                                $rsDados = mysqli_fetch_assoc($objResult);
                            }
                        }
                        //
                        // Calcula o percentual respondido
                        //
                        // Se houverem perguntas...
                        if($this->qt_total > 0) {
                            // Calcula o % de perguntas respondidas
                            $this->pc_respondidas = 100 * $this->qt_respondidas / $this->qt_total;
                        // Senao..
                        } else {
                            // Todas ja foram respondidas (100%)
                            $this->pc_respondidas = 100;
                        }
                        //
                        // Carrega todas as respostas, se houver
                        unset($this->colRespostas);
                        $this->colRespostas = $this->obterTodasRespostas();
                        
                        // Montar a matriz DISC
                        unset($this->colDISC);
                        unset($this->colDISC_Normalizado);
						
                        // Se a pesquisa j� estiver completa...
                        if ($this->qt_nao_respondidas == 0) {
                            //
                            $this->colDISC = array(
                              "D" => array(
                                  "M" => 0,
                                  "L" => 0,
                                  "A" => 0
                              ),
                              "I" => array(
                                  "M" => 0,
                                  "L" => 0,
                                  "A" => 0
                              ),
                              "S" => array(
                                  "M" => 0,
                                  "L" => 0,
                                  "A" => 0
                              ),
                              "C" => array(
                                  "M" => 0,
                                  "L" => 0,
                                  "A" => 0
                              )
							);
                            //
                            foreach($this->colRespostas as $objResposta) {
                                //
                                $aRespostaDISC = explode("|", $objResposta["ds_resposta"]);
                                //
                                if(count($aRespostaDISC) == 2) {
                                    //
                                    $tempResposta_p = $aRespostaDISC[0];
                                    $tempResposta_m = $aRespostaDISC[1];
                                } else {
                                    $tempResposta_p = "";
                                    $tempResposta_m = "";
                                }
                                //
                                $this->colDISC[$tempResposta_p]["M"]++;
                                $this->colDISC[$tempResposta_p]["A"] = $this->colDISC[$tempResposta_p]["M"] - $this->colDISC[$tempResposta_p]["L"];
                                //
                                $this->colDISC[$tempResposta_m]["L"]++;
                                $this->colDISC[$tempResposta_m]["A"] = $this->colDISC[$tempResposta_m]["M"] - $this->colDISC[$tempResposta_m]["L"];
                            }
                        }
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        // Retorno da fun��o
        return($this->ERRO);
        
    }
    
    public function obterTodasRespostas() {
         //
        $RET_array = array();
        //
         //
        if(DEFWEBsv_debug) {
            $this->DBG_db_type = DEFWEBsv_db_type;
            $this->DBG_db_host = DEFWEBsv_db_host;
            $this->DBG_db_name = DEFWEBsv_db_name;
            $this->DBG_db_user = DEFWEBsv_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBsv_db_host, DEFWEBsv_db_user, DEFWEBsv_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBsv_db_name)) {
                //
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_aval_form_item, id_item_formulario, ";
                $sSQL .= " ds_resposta, id_token, ";
                $sSQL .= " dt_resposta, nr_posicao, ";
                $sSQL .= " cd_item, id_versao, cd_opcao_item  ";
                $sSQL .= " FROM tbdw_af_token_item AS af ";
                $sSQL .= " WHERE id_token = " . mysqli_escape_string($objLink, $this->id_token);
                $sSQL .= " ORDER BY nr_posicao, cd_item";
                //
                //$this->ERRO_mensagem = $sSQL;
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if($objResult) {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if($rsDados) {
                        // Enquanto houver linhas no result set
                        while ($rsDados) {
                            //
                            $RET_array[$rsDados["cd_item"]] = $rsDados;
                            // L� a pr�xima linha do result set
                            $rsDados = mysqli_fetch_assoc($objResult);
                        }
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        return($RET_array);
    }
    
    public function obterRespostas($PARM_id_token, $PARM_cd_item) {
         //
        $RET_array = array();
        //
         //
        if(DEFWEBsv_debug) {
            $this->DBG_db_type = DEFWEBsv_db_type;
            $this->DBG_db_host = DEFWEBsv_db_host;
            $this->DBG_db_name = DEFWEBsv_db_name;
            $this->DBG_db_user = DEFWEBsv_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBsv_db_host, DEFWEBsv_db_user, DEFWEBsv_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBsv_db_name)) {
                //
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_aval_form_item, id_item_formulario, ";
                $sSQL .= " ds_resposta, id_token, ";
                $sSQL .= " dt_resposta, nr_posicao, ";
                $sSQL .= " cd_item, id_versao, cd_opcao_item  ";
                $sSQL .= " FROM tbdw_af_token_item AS af ";
                $sSQL .= " WHERE id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " AND cd_item = " . mysqli_escape_string($objLink, $PARM_cd_item);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    // Ajusta a mensagem
                    $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHACONSULTABD");
                    $this->ERRO = 1;
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if(!$rsDados) {
                        // Ajusta a mensagem
                        $this->ERRO_mensagem = $this->obterMensagem("MSG_ERRO_FALHADADOSNAOENCONTRADOS"); ;
                        $this->ERRO = 1;
                        //
                    // Senao, obteve a primeira linha do result set...
                    } else {
                        //
                        // Enquanto houver linhas no result set
                        while ($rsDados) {
                            //
                            $RET_array[$rsDados["cd_opcao_item"]] = $rsDados;
                            // L� a pr�xima linha do result set
                            $rsDados = mysqli_fetch_assoc($objResult);
                        }
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        return($RET_array);
   }
}