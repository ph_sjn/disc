<?php

// Configuracoes de acesso ao banco de dados
include_once "../config.inc.php";
include_once "libdisc_survey.php";

$DEFWEB_db_type = $db_type;
$DEFWEB_db_host = $db_host;
$DEFWEB_db_user = $db_user;
$DEFWEB_db_pass = $db_pass;
$DEFWEB_db_name = $db_name;

define("DEFWEB_debug", 0);
define("DEFWEB_db_type",$DEFWEB_db_type);
define("DEFWEB_db_host",$DEFWEB_db_host);
define("DEFWEB_db_user",$DEFWEB_db_user);
define("DEFWEB_db_pass",$DEFWEB_db_pass);
define("DEFWEB_db_name",$DEFWEB_db_name);

/******
 * 
 * libdefweb.php
 * 
 ******/

class DEFWEBcliente {
    // id_cliente
    public $Id = null;
    // cd_cliente
    public $Codigo = null;
    // ds_cliente
    public $Nome = null;
}

class DEFWEBprojeto {
    // id_proejto
    public $Id = null;
    // nm_projeto
    public $Nome = null;
    // cd_projeto
    public $Codigo = null;
    // dt_inicio
    public $DataInicio = null;
    // dt_fim
    public $DataTermino = null;
    // Cliente
    public $Cliente = null;
}

class DEFWEBformulario {
    // id_formulario
    public $Id = null;
    // ds_formulario
    public $Nome = null;
    // cd_sit_formulario
    public $CodigoSituacao = null;
    // ds_sit_formulario
    public $Situacao = null;
}

class DEFWEBformularioAvaliacao {
    // id_avaliacao_formulario
    public $Id = null;
    // id_formulario
    public $IdFormulario = null;
    // id_avaliacao
    public $IdAvaliacao;
    // id_cliente
    public $IdCliente;
    // dt_inicio
    public $DataInicio = null;
    // dt_fim
    public $DataTermino = null;
    // cd_publico_alvo
    public $CodigoPublicoAlvo = null;
    // ds_publico_alvo
    public $PublicoAlvo = null;
    // ds_formulario
    public $Nome = null;
    // cd_sit_formulario
    public $CodigoSituacao = null;
    // ds_sit_formulario
    public $Situacao = null;
}

class DEFWEBtokenItemFormulario {
    
}

class DEFWEBtokenFormulario {
    
}

class DEFWEBtoken {
    // id_token
    public $Id = null;
    // dt_criacao
    public $DataCriacao = null;
    // ds_codigo
    public $Codigo = null;
    // ds_nome
    public $Nome = null;
    // ds_email
    public $Email = null;
    // ds_observacao
    public $Observacoes = null;
    // ds_unidade
    public $Unidade = null;
    // cd_publico_alvo
    public $CodigoPublicoAlvo = null;
    // ds_publico_alvo
    public $PublicoAlvo = null;
    // id_avaliacao
    public $IdAvaliacao = null;
    // id_cliente
    public $IdCliente = null;
    //
    public $QtdeTotalPerguntas = null;
    public $QtdePerguntasFaltam = null;
    public $QtdePerguntasRespondidas = null;
}

class DEFWEBsituacaoAvaliacao {
    // cd_sit_avaliacao
    public $Codigo = null;
    // ds_sit_avaliacao
    public $Situacao = null;
}

class DEFWEBpergunta {
    //
    
}

class DEFWEBopcaoResposta {
    //
    
}

class DEFWEBavaliacao {
    // id_avaliacao
    public $Id = null;
    // dt_inicio
    public $DataInicio = null;
    // dt_fim
    public $DataFim = null;
    // objeto DEFWEBsituacaoAvaliacao(cd_sit_avaliacao)
    //public $Situacao = null;
    // ds_titulo
    public $Titulo = null;
    
    // ds_franqueador_intro
    public $TextoIntroducaoFranqueador = null;
    // ds_franqueado_intro
    public $TextoIntroducaoFranqueado = null;

    // id_token_franqueador
    public $IdFranqueador = null;
    // ds_nome
    public $NomeFranqueador = null;
    // ds_email
    public $EmailFranqueador = null;
    // ds_email_assunto
    public $EmailModeloAssunto = null;
    // ds_email_txt_link
    public $EmailModeloCorpo = null;
    // ds_email_subj_conclusao
    public $EmailConclusaoAssunto = null;
    // ds_email_body_conclusao
    public $EmailConclusaoCorpo = null;
    // fl_enviar_email_conclusao
    public $EnviarEmailConclusao = 0;
        
    // cd_sit_avaliacao
    public $CodigoSituacao = null;
    // ds_sit_avaliacao
    public $Situacao = null;

    // id_cliente
    public $IdCliente = null;
    // ds_cliente
    public $NomeCliente = null;
    
    // id_projeto
    public $IdProjeto = null;
    // ds_projeto
    public $NomeProjeto = null;

    // objeto DEFWEBcliente(id_cliente)
    public $objCliente = null;
    // objeto DEFWEBprojeto(id_projeto)
    public $objProjeto = null;
    // array DEFWEBtoken()
    public $colTokens = null;
    // array DEFWEBformularioAvaliacao()
    public $colFormularios = null;
    
    // array DEFWEBperguntas
    public $colPerguntasFranqueado = null;
    
    public $IdPaginaConfCad = null;
    public $IdPaginaInstrucoes = null;
    public $IdPaginaQuestionario = null;
    public $IdPaginaConclusao = null;
    public $IdPaginaRelEntrevistado = null;
    public $IdPaginaRelAdmin = null;
    
    public $IMGLogoID = null;
    public $IMGLogoName = null;

    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_complemento = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function __construct($PARM_id_avaliacao) {
        //
        if($PARM_id_avaliacao) {
            //
            $this->obterAvaliacao($PARM_id_avaliacao);
        }
    }
    
    public function limparErro() {
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = 0;
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "SEM ERRO";
        $this->ERRO_complemento = "";
    }
    
    public function obterProximoIdProjeto() {
        //
        $RET_id = 0;
        //
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " MAX(id_avaliacao)+1 AS proximo_id FROM tbdw_avaliacao;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if(!$rsDados) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Dados n�o encontrados no banco de dados!";
                    // Senao, obteve o result set...
                    } else {
                        $RET_id = $rsDados["proximo_id"];
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_id);
    }

    // F = Franqueador
    // * = Franqueados
    public function pesquisaEmAndamento($PARM_tipo) {
        //
        $RET_pesquisaEmAndamento = FALSE;
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT DISTINCT id_token FROM tbdw_aval_token";
                $sSQL .= " WHERE id_avaliacao = " . mysqli_escape_string($objLink, $this->Id);
                //
                if($PARM_id_token != "") {
                    // Franqueados
                    if($PARM_id_token == "*") {
                        //
                        $sSQL .= " AND id_token <> " . mysqli_escape_string($objLink, $this->IdFranqueador);
                        //
                    } else {
                        //
                        $sSQL .= " AND id_token = " . mysqli_escape_string($objLink, $this->IdFranqueador);
                    }
                }
                //
                if(strtoupper($PARM_tipo) == "F") {
                    //
                    $sSQL .= " AND";   
                    $sSQL .= " cd_publico_alvo = 1";
                } elseif($PARM_tipo == "*") {
                    //
                    $sSQL .= " AND";   
                    $sSQL .= " cd_publico_alvo = 2";
                }
                //
                $sSQL .= " AND";   
                $sSQL .= " id_token IN (SELECT DISTINCT id_token FROM tbdw_af_token_item)";
                $sSQL .= ";";
                
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if($rsDados) {
                        //
                        $RET_pesquisaEmAndamento = TRUE;
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_pesquisaEmAndamento);
    }
    
    function incluirProjeto($PARM_avaliacao) {
        //
        $PARM_avaliacao->IdAvaliacao = $this->obterProximoIdProjeto();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_avaliacao ";
                $sSQL .= " (";
                $sSQL .= " id_avaliacao";
                $sSQL .= ", id_cliente";
                $sSQL .= ", cd_sit_avaliacao";
                $sSQL .= ", ds_titulo";
                $sSQL .= ", id_projeto";
                $sSQL .= ", dt_inicio";
                $sSQL .= ", dt_fim";
                $sSQL .= ", id_pagina_confcad";
                $sSQL .= ", id_pagina_instrucoes";
                $sSQL .= ", id_pagina_questionario";
                $sSQL .= ", id_pagina_conclusao";
                $sSQL .= ", id_pagina_relatorio";
                $sSQL .= ", id_pagina_reladmin";
                //
                $sSQL .= ", ds_email_assunto";
                $sSQL .= ", ds_email_txt_link";
                $sSQL .= ", ds_email_subj_conclusao";
                $sSQL .= ", ds_email_body_conclusao";
                $sSQL .= ", fl_enviar_email_conclusao";
                //
                $sSQL .= " )";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdAvaliacao);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_avaliacao->IdCliente);
                //
                //$sSQL .= ", ". mysqli_escape_string($objLink, 2);
                //
                if(is_null($PARM_avaliacao->CodigoSituacao)||$PARM_avaliacao->CodigoSituacao=="") {
                    //
                    $PARM_avaliacao->CodigoSituacao = 2;
                }
                //
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_avaliacao->CodigoSituacao);
                //
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->Titulo) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_avaliacao->IdProjeto);
                //
                if((!is_null($PARM_avaliacao->DataInicio))&&($PARM_avaliacao->DataInicio!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->DataInicio) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if((!is_null($PARM_avaliacao->DataFim))&&($PARM_avaliacao->DataFim!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->DataFim) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaConfCad) && $PARM_avaliacao->IdPaginaConfCad != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaConfCad);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaInstrucoes) && $PARM_avaliacao->IdPaginaInstrucoes != "") {
                $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaInstrucoes);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaQuestionario) && $PARM_avaliacao->IdPaginaQuestionario != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaQuestionario);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaConclusao) && $PARM_avaliacao->IdPaginaConclusao != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaConclusao);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaRelEntrevistado) && $PARM_avaliacao->IdPaginaRelEntrevistado != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaRelEntrevistado);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->IdPaginaRelAdmin) && $PARM_avaliacao->IdPaginaRelAdmin != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->IdPaginaRelAdmin);
                } else {
                    $sSQL .= "0";
                }
                //
                ////// MODELO DE E-MAIL
                //
                if((!is_null($PARM_avaliacao->EmailModeloAssunto))&&($PARM_avaliacao->EmailModeloAssunto!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->EmailModeloAssunto) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if((!is_null($PARM_avaliacao->EmailModeloCorpo))&&($PARM_avaliacao->EmailModeloCorpo!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->EmailModeloCorpo) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                ////// MODELO DE E-MAIL DE CONCLUSAO
                //
                if((!is_null($PARM_avaliacao->EmailConclusaoAssunto))&&($PARM_avaliacao->EmailConclusaoAssunto!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->EmailConclusaoAssunto) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if((!is_null($PARM_avaliacao->EmailConclusaoCorpo))&&($PARM_avaliacao->EmailConclusaoCorpo!="")) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_avaliacao->EmailConclusaoCorpo) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ", ";
                if(!is_null($PARM_avaliacao->EnviarEmailConclusao) && $PARM_avaliacao->EnviarEmailConclusao != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_avaliacao->EnviarEmailConclusao);
                } else {
                    $sSQL .= "0";
                }
                //
                //////
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id = mysqli_insert_id($objLink);
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($PARM_avaliacao->IdAvaliacao);
        //
    }
    
    function atribuirIDtokenFranqueador($PARM_id_avaliacao, $PARM_id_token) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET id_token_franqueador = ". mysqli_escape_string($objLink, $PARM_id_token);
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $PARM_id_avaliacao);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    function removerAvaliacao($PARM_id_avaliacao) {
        //
        if($PARM_id_avaliacao == $this->Id) {
            //
            if(DEFWEB_debug) {
                $this->DBG_db_type = DEFWEB_db_type;
                $this->DBG_db_host = DEFWEB_db_host;
                $this->DBG_db_name = DEFWEB_db_name;
                $this->DBG_db_user = DEFWEB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
            //
            if(!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_avaliacao ";
                    $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $PARM_id_avaliacao);
                    $sSQL .= " AND id_cliente = " . $this->IdCliente;
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);

                    // Se falhou...
                    if(mysqli_errno($objLink) != 0) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                        //
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        } else {
            //
            $this->ERRO = -3;
            $this->ERRO_bd_cod_erro = "";
            $this->ERRO_bd_msg_erro = "";
            $this->ERRO_mensagem = "Par�metros inv�lidos!";
            //
        }
    }
    
    function removerFormularios($PARM_cd_publico_alvo) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o DELETE
                $sSQL  = " DELETE FROM tbdw_avaliacao_formulario ";
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                $sSQL .= " AND id_cliente = ". mysqli_escape_string($objLink, $this->IdCliente);
                $sSQL .= " AND cd_publico_alvo = ". mysqli_escape_string($objLink, $PARM_cd_publico_alvo);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    function atualizarFormularios($PARM_cd_publico_alvo, $PARM_array_forms) {
        // Remove todos os formul�rios
        $this->removerFormularios($PARM_cd_publico_alvo);
        //
        // Se n�o deu erro...
        if(!($this->ERRO == 1)) {
            // Para cada ID do array de forms...
            foreach ($PARM_array_forms AS $objItem) {
                if($objItem) {
                    // Inclui o formul�rio
                    $this->incluirFormularios($objItem, $PARM_cd_publico_alvo);
                    //
                    if($this->ERRO == 1) {
                        break;
                    }
                }
            }           
        }
    }

    function atualizarDatas($PARM_dt_inicio, $PARM_dt_fim) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                if((!is_null($PARM_dt_inicio))&&($PARM_dt_inicio)) {
                    $PARM_dt_inicio = "'". mysqli_escape_string($objLink, $PARM_dt_inicio) . "'";
                } else {
                    $PARM_dt_inicio = "NULL";
                }
                //
                if((!is_null($PARM_dt_fim))&&($PARM_dt_fim)) {
                    $PARM_dt_fim = "'". mysqli_escape_string($objLink, $PARM_dt_fim) . "'";
                } else {
                    $PARM_dt_fim = "NULL";
                }
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ";
                $sSQL .= " dt_inicio = ". $PARM_dt_inicio;
                $sSQL .= ", dt_fim = ". $PARM_dt_fim;
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function atualizarImagemTopo($PARM_nmarq_topo_original, $PARM_nmarq_topo_interno) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ";
                $sSQL .= " nmarq_topo_original = ";
                if(!is_null($PARM_nmarq_topo_original) && $PARM_nmarq_topo_original != "") {
                    $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_nmarq_topo_original) . "'";
                } else {
                    $sSQL .= "NULL";
                }
                $sSQL .= ", nmarq_topo_interno = ";
                if(!is_null($PARM_nmarq_topo_interno) && $PARM_nmarq_topo_interno != "") {
                    $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_nmarq_topo_interno) . "'";
                } else {
                    $sSQL .= "NULL";
                }
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                //$this->ERRO_complemento = $sSQL;
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }

    public function atualizarPaginas($PARM_objAval) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ";
                $sSQL .= " id_pagina_confcad = ";
                if(!is_null($PARM_objAval->IdPaginaConfCad) && $PARM_objAval->IdPaginaConfCad != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaConfCad);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", id_pagina_instrucoes = ";
                if(!is_null($PARM_objAval->IdPaginaInstrucoes) && $PARM_objAval->IdPaginaInstrucoes != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaInstrucoes);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", id_pagina_questionario = ";
                if(!is_null($PARM_objAval->IdPaginaQuestionario) && $PARM_objAval->IdPaginaQuestionario != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaQuestionario);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", id_pagina_conclusao = ";
                if(!is_null($PARM_objAval->IdPaginaConclusao) && $PARM_objAval->IdPaginaConclusao != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaConclusao);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", id_pagina_relatorio = ";
                if(!is_null($PARM_objAval->IdPaginaRelEntrevistado) && $PARM_objAval->IdPaginaRelEntrevistado != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaRelEntrevistado);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", id_pagina_reladmin = ";
                if(!is_null($PARM_objAval->IdPaginaRelAdmin) && $PARM_objAval->IdPaginaRelAdmin != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->IdPaginaRelAdmin);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= ", fl_enviar_email_conclusao = ";
                if(!is_null($PARM_objAval->EnviarEmailConclusao) && $PARM_objAval->EnviarEmailConclusao != "") {
                    $sSQL .= mysqli_escape_string($objLink, $PARM_objAval->EnviarEmailConclusao);
                } else {
                    $sSQL .= "0";
                }
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                //$this->ERRO_complemento = $sSQL;
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    function atualizarTextosDeIntroducao($PARM_ds_franqueador_intro, $PARM_ds_franqueado_intro) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ";
                $sSQL .= " ds_franqueador_intro = '". mysqli_escape_string($objLink, $PARM_ds_franqueador_intro) . "'";
                $sSQL .= ", ds_franqueado_intro = '". mysqli_escape_string($objLink, $PARM_ds_franqueado_intro) . "'";
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    function atualizarTitulo($PARM_ds_titulo) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ds_titulo = '". mysqli_escape_string($objLink, $PARM_ds_titulo) . "'";
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }

    public function atualizarSituacao($PARM_cd_sit_avaliacao) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET cd_sit_avaliacao = ". mysqli_escape_string($objLink, $PARM_cd_sit_avaliacao);
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function incluirFormularios($PARM_id_formulario, $PARM_cd_publico_alvo) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT tbdw_avaliacao_formulario ";
                $sSQL .= " (id_formulario, id_avaliacao, id_cliente, cd_publico_alvo)";
                $sSQL .= " VALUES";
                $sSQL .= " (";
                $sSQL .= " " . mysqli_escape_string($objLink, $PARM_id_formulario) . ",";
                $sSQL .= " " . mysqli_escape_string($objLink, $this->Id) . ",";
                $sSQL .= " " . mysqli_escape_string($objLink, $this->IdCliente) . ",";
                $sSQL .= " " . mysqli_escape_string($objLink, $PARM_cd_publico_alvo);
                $sSQL .= " )";
                //
//                echo($sSQL);
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }

    /*
     *  criarProjeto()
     * 
     *  Cria o PxP (Projeto/Pesquisa) de acordo com os atributos do objeto
     *  instanciado ($this).
     * 
     *  Retorna o ID gerado para o PxP (tbdw_avaliacao)
     */
    
    public function criarProjeto() {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        // Cria o projeto no BD e obtem o ID
        $RET_id_avaliacao = $this->incluirProjeto($this);
        //
        // Se obteve o ID
        if($RET_id_avaliacao > 0) {
            //
            $this->Id = $RET_id_avaliacao;
            //
        } else {
            // Falhou na cria��o do PxP
            $this->ERRO = 1;
            $this->ERRO_mensagem = "N�o foi poss�vel criar o Projeto/Pesquisa!";
        }
        //
        return ($RET_id_avaliacao);
        //
    }
    
    public function obterAvaliacao($PARM_id_avaliacao) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " av.id_avaliacao AS id_avaliacao";
                $sSQL .= ", av.id_cliente AS id_cliente";
                $sSQL .= ", cl.ds_cliente AS ds_cliente";
                $sSQL .= ", av.ds_titulo AS ds_titulo";
                $sSQL .= ", av.cd_sit_avaliacao AS cd_sit_avaliacao";
                $sSQL .= ", sit.ds_sit_avaliacao AS ds_sit_avaliacao";
                $sSQL .= ", av.id_projeto AS id_projeto";
                $sSQL .= ", pj.nm_projeto AS nm_projeto";
                $sSQL .= ", av.dt_inicio AS dt_inicio, av.dt_fim AS dt_fim";
                $sSQL .= ", av.id_token_franqueador AS id_token_franqueador";
                $sSQL .= ", tk.ds_nome AS ds_nome_franqueador";
                $sSQL .= ", tk.ds_email AS ds_email_franqueador";
                $sSQL .= ", av.ds_email_assunto AS ds_email_assunto";
                $sSQL .= ", av.ds_email_txt_link AS ds_email_txt_link";
                $sSQL .= ", av.ds_email_subj_conclusao AS ds_email_subj_conclusao";
                $sSQL .= ", av.ds_email_body_conclusao AS ds_email_body_conclusao";
                $sSQL .= ", av.fl_enviar_email_conclusao AS fl_enviar_email_conclusao";
                $sSQL .= ", av.ds_franqueador_intro AS ds_franqueador_intro";
                $sSQL .= ", av.ds_franqueado_intro AS ds_franqueado_intro";
                $sSQL .= ", av.dt_inicio AS dt_inicio";
                $sSQL .= ", av.dt_fim AS dt_fim";
                $sSQL .= ", av.id_pagina_confcad AS id_pagina_confcad";
                $sSQL .= ", av.id_pagina_instrucoes AS id_pagina_instrucoes";
                $sSQL .= ", av.id_pagina_questionario AS id_pagina_questionario";
                $sSQL .= ", av.id_pagina_conclusao AS id_pagina_conclusao";
                $sSQL .= ", av.id_pagina_relatorio AS id_pagina_relatorio";
                $sSQL .= ", av.id_pagina_reladmin AS id_pagina_reladmin";
                $sSQL .= ", av.nmarq_topo_original AS nmarq_topo_original";
                $sSQL .= ", av.nmarq_topo_interno AS nmarq_topo_interno";
                $sSQL .= " FROM tbdw_avaliacao AS av";
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente";
                $sSQL .= " LEFT JOIN tbdw_sit_avaliacao AS sit ON sit.cd_sit_avaliacao = av.cd_sit_avaliacao";
                $sSQL .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
                $sSQL .= " LEFT JOIN tbdw_aval_token AS tk ON tk.id_token = av.id_token_franqueador";
                $sSQL .= " WHERE av.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_avaliacao);
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if(!$rsDados) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Dados n�o encontrados no banco de dados!";
                    // Senao, obteve o result set...
                    } else {
                        $this->Id = $PARM_id_avaliacao;
                        $this->IdCliente = $rsDados["id_cliente"];
                        $this->NomeCliente = $rsDados["ds_cliente"];
                        $this->Titulo = $rsDados["ds_titulo"];
                        $this->CodigoSituacao = $rsDados["cd_sit_avaliacao"];
                        $this->Situacao = $rsDados["ds_sit_avaliacao"];
                        $this->IdProjeto = $rsDados["id_projeto"];
                        $this->NomeProjeto = $rsDados["nm_projeto"];
                        $this->IdFranqueador = $rsDados["id_token_franqueador"];
                        $this->NomeFranqueador = $rsDados["ds_nome_franqueador"];
                        $this->EmailFranqueador = $rsDados["ds_email_franqueador"];
                        $this->EmailModeloAssunto = $rsDados["ds_email_assunto"];
                        $this->EmailModeloCorpo = $rsDados["ds_email_txt_link"];
                        $this->EmailConclusaoAssunto = $rsDados["ds_email_subj_conclusao"];
                        $this->EmailConclusaoCorpo = $rsDados["ds_email_body_conclusao"];
                        $this->EnviarEmailConclusao = $rsDados["fl_enviar_email_conclusao"];
                        $this->TextoIntroducaoFranqueador = $rsDados["ds_franqueador_intro"];
                        $this->TextoIntroducaoFranqueado = $rsDados["ds_franqueado_intro"];
                        $this->DataInicio = $rsDados["dt_inicio"];
                        $this->DataFim = $rsDados["dt_fim"];
                        $this->IdPaginaConfCad = $rsDados["id_pagina_confcad"];
                        $this->IdPaginaInstrucoes = $rsDados["id_pagina_instrucoes"];
                        $this->IdPaginaQuestionario = $rsDados["id_pagina_questionario"];
                        $this->IdPaginaConclusao = $rsDados["id_pagina_conclusao"];
                        $this->IdPaginaRelEntrevistado = $rsDados["id_pagina_relatorio"];
                        $this->IdPaginaRelAdmin = $rsDados["id_pagina_reladmin"];
                        $this->IMGLogoID = $rsDados["nmarq_topo_interno"];
                        $this->IMGLogoName = $rsDados["nmarq_topo_original"];
                        //
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function obterTokens($PARM_completo) {
        //
        unset($this->colTokens);
        $this->colTokens = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.dt_criacao AS dt_criacao,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.cd_publico_alvo AS cd_publico_alvo,";
                $sSQL .= " pa.ds_publico_alvo AS ds_publico_alvo,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.id_avaliacao AS id_avaliacao";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = tk.cd_publico_alvo";
                if($this->Id > 0) {
                    $sSQL .= " WHERE tk.id_avaliacao = " . mysqli_escape_string($objLink, $this->Id);
                }
                if($this->IdCliente > 0) {
                    if($this->Id > 0) {
                        $sSQL .= " AND";
                    } else {
                        $sSQL .= " WHERE";
                    }
                    //
                    $sSQL .= " tk.id_cliente = " . mysqli_escape_string($objLink, $this->IdCliente);
                }
                $sSQL .= " ORDER BY ds_nome";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    while($rsDados) {
                        //
                        $objToken = new DEFWEBtoken();
                        //
                        $objToken->Id = $rsDados["id_token"];
                        $objToken->DataCriacao = $rsDados["dt_criacao"];
                        $objToken->Codigo = $rsDados["ds_codigo"];
                        $objToken->Nome = $rsDados["ds_nome"];
                        $objToken->Email = $rsDados["ds_email"];
                        $objToken->Observacoes = $rsDados["ds_observacao"];
                        $objToken->Unidade = $rsDados["ds_unidade"];
                        $objToken->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objToken->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objToken->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objToken->IdCliente = $rsDados["id_cliente"];
                        //
                        if($PARM_completo === TRUE) {
                            //
                            $objToken->QtdeTotalPerguntas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 0, DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass, DEFWEB_db_name);
                            $objToken->QtdePerguntasRespondidas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 1, DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass, DEFWEB_db_name);
                            $objToken->QtdePerguntasFaltam = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 2, DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass, DEFWEB_db_name);
                        }
                        //
                        $this->colTokens[strtolower($objToken->Id)] = $objToken;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }

    //  obterTokenPeloID(<ID-Token>)
    //  
    //  Retorna o objeto token pelo ID fornecido
    //  For�a a carga da cole��o de tokens, caso ela n�o esteja carregada
    //
    public function obterTokenPeloID($PARM_id_token) {
        //
        // Se a cole��o de tokens n�o estiver carregada...
        if(!($this->colTokens)) {
            // For�a a carga
            $this->obterTokens(TRUE);
        }
        //
        // Inicializa o retorno
        $RET_objToken = null;
        // Percorre cada token da cole��o
        foreach ($this->colTokens AS $objItem) {
            // Se encontrou o ID desejado...
            if($objItem->Id == $PARM_id_token) {
                // Atribui para a vari�vel de retorno
                $RET_objToken = $objItem;
                // Quebra o la�o "foreach"
                break;
            }
        }
        //
        return($RET_objToken);
    }
    
    public function incluirToken($PARM_objToken, $PARM_atualizarTokens) {
        //
        $RET_id_token = 0;
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_aval_token ";
                $sSQL .= " (id_avaliacao, id_cliente, dt_criacao, cd_publico_alvo, ";
                $sSQL .= " ds_observacao, ds_email, ds_nome, ds_codigo, ds_unidade ";
                $sSQL .= " ) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->DataCriacao) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->CodigoPublicoAlvo);
                //
                if($PARM_objToken->Observacoes) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Observacoes) . "'";
                } else {
                    $sSQL .= ", null";
                };
                //
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Email) . "'";
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Nome) . "'";
                //
                if($PARM_objToken->Codigo) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Codigo) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->Unidade) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Unidade) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id_token = mysqli_insert_id($objLink);
                    //
                    if($RET_id_token > 0) {
                        //
                        if($PARM_atualizarTokens) {
                            //
                            $this->obterTokens();
                        } else {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                        //
                        }
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_id_token);
    }
     
    public function atualizarToken($PARM_objToken, $PARM_atualizarTokens) {
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
		
                // Monta o UPDATE
                $sSQL = "UPDATE tbdw_aval_token  ";
		$sSQL .= " SET ";
                $sSQL .= " ds_nome  = '". mysqli_escape_string($objLink, $PARM_objToken->Nome) . "'";
		$sSQL .= ", ds_email  = '". mysqli_escape_string($objLink, $PARM_objToken->Email) . "'";
                if($PARM_objToken->Codigo) {
                    $sSQL .= ", ds_codigo = '". mysqli_escape_string($objLink, $PARM_objToken->Codigo) . "'";
                }
                if($PARM_objToken->Observacoes) {
                    $sSQL .= ", ds_observacao  = '". mysqli_escape_string($objLink, $PARM_objToken->Observacoes) . "'";
                }
                if($PARM_objToken->Unidade) {
                    $sSQL .= ", ds_unidade  = '". mysqli_escape_string($objLink, $PARM_objToken->Unidade) . "'";
                }
		$sSQL .= " WHERE id_token = " . mysqli_escape_string($objLink, $PARM_objToken->Id);
                $sSQL .= " AND id_cliente = " . mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                $sSQL .= " AND id_avaliacao = " . mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                
                //$sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->DataCriacao) . "'";
                //$sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->CodigoPublicoAlvo);
                
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id_token = mysqli_insert_id($objLink);
                    //
                    if($RET_id_token > 0) {
                        //
                        if($PARM_atualizarTokens) {
                            //
                            $this->obterTokens();
                        } else {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                        //
                        }
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function reiniciarPesquisa($PARM_id_token) {
        //
        $RET_OK = FALSE;
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                //
                // Monta o DELETE
                $sSQL  = " DELETE FROM tbdw_af_token_item ";
                $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_id_token);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                    //
                } else {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_aval_form_token ";
                    $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                    $sSQL .= " AND id_cliente = ". mysqli_escape_string($objLink, $this->IdCliente);
                    $sSQL .= " AND id_token = ". mysqli_escape_string($objLink, $PARM_id_token);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    //
                    // Se falhou...
                    if(mysqli_errno($objLink) != 0) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                        //
                    } else {
                        //
                        $RET_OK = TRUE;
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_OK);
    }
    
    //
    //  removerToken( <Id do Token>, <Atualizar colecao de tokens> )
    //
    public function removerToken($PARM_id_token, $PARM_atualizarTokens) {
        //
        // Inicializa o retorno da funcao
        $RET_OK = FALSE;
        //
        // Se obteve sucesso na limpeza da pesquisa...
        if($this->reiniciarPesquisa($PARM_id_token)) {
            //
            if(DEFWEB_debug) {
                $this->DBG_db_type = DEFWEB_db_type;
                $this->DBG_db_host = DEFWEB_db_host;
                $this->DBG_db_name = DEFWEB_db_name;
                $this->DBG_db_user = DEFWEB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
            //
            if(!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_aval_token ";
                    $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $this->Id);
                    $sSQL .= " AND id_cliente = ". mysqli_escape_string($objLink, $this->IdCliente);
                    $sSQL .= " AND id_token = ". mysqli_escape_string($objLink, $PARM_id_token);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if(mysqli_errno($objLink) != 0) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                        //
                    } else {
                        //
                        if($PARM_atualizarTokens) {
                            //
                            $this->obterTokens();
                        }
                        //
                        $RET_OK = TRUE;
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_OK);
    }
    
    public function obterFormularios() {
         //
        unset($this->colFormularios);
        $this->colFormularios = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " af.id_avaliacao_formulario,";
                $sSQL .= " af.id_formulario,";
                $sSQL .= " af.id_avaliacao,";
                $sSQL .= " af.id_cliente,";
                $sSQL .= " af.dt_inicio,";
                $sSQL .= " af.dt_fim,";
                $sSQL .= " af.cd_publico_alvo,";
                $sSQL .= " pa.ds_publico_alvo,";
                $sSQL .= " form.ds_formulario,";
                $sSQL .= " form.cd_sit_formulario,";
                $sSQL .= " sf.ds_sit_formulario";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_avaliacao_formulario AS af";
                $sSQL .= " LEFT JOIN tbdw_formulario AS form ON form.id_formulario = af.id_formulario";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = af.cd_publico_alvo";
                $sSQL .= " LEFT JOIN tbdw_sit_formulario AS sf ON sf.cd_sit_formulario = form.cd_sit_formulario";
                $sSQL .= " WHERE af.id_avaliacao = " . mysqli_escape_string($objLink, $this->Id);
                $sSQL .= " ORDER BY af.id_avaliacao_formulario";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    while($rsDados) {
                        //
                        $objFormulario = new DEFWEBformularioAvaliacao();
                        //
                        $objFormulario->Id = $rsDados["id_avaliacao_formulario"];
                        $objFormulario->IdFormulario = $rsDados["id_formulario"];
                        $objFormulario->IdAvaliacao =  $rsDados["id_avaliacao"];
                        $objFormulario->IdCliente = $rsDados["id_cliente"];
                        $objFormulario->DataInicio = $rsDados["dt_inicio"];
                        $objFormulario->DataTermino = $rsDados["dt_fim"];
                        $objFormulario->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objFormulario->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objFormulario->Nome = $rsDados["ds_formulario"];
                        $objFormulario->CodigoSituacao = $rsDados["cd_sit_formulario"];
                        $objFormulario->Situacao = $rsDados["ds_sit_formulario"];
                        //
                        $this->colFormularios[$objFormulario->CodigoPublicoAlvo][$objFormulario->IdFormulario] = $objFormulario;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function obterPerguntasFranqueado() {
         //
        unset($this->colFormularios);
        $this->colFormularios = array();
        //
    }
    
    public function obterPerguntas($PARM_cd_publico_alvo) {
        
    }
}

class DEFWEBrespostaFranqueador {
    public $CodigoTema = null; // cd_tema
    public $DescricaoTema = null; // ds_tema
    public $DescricaoItem = null; // ds_item
    public $IdItemFormulario = null; // id_item_formulario
    public $IdFormulario = null; // id_formulario
    public $CodigoAgrupamento = null; // cd_agrupamento
    public $DescricaoAgrupamento = null; // ds_agrupamento
    // array DEFWEBitemRespostaFranqueador()
    public $colCampos = null;
}
//
class DEFWEBitemRespostaFranqueador {
    public $Resposta = null; // ds_resposta
    public $DescricaoOpcaoItem; // ds_opcao_item
    //
    public $CodigoOpcao = null; // cd_opcao_item
    public $CodigoItem = null; // cd_item
    public $Versao = null; // id_versao
}

class DEFWEBmaster {
    
    public $colAvaliacoes = null;
    public $colClientes = null;
    public $colProjetos = null;
    public $colFormularios = null;
    
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function obterToken($PARM_id_token) {
        
    }
    
    public function obterSituacoesPesquisa() {
        //
        $RET_array = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " cd_sit_avaliacao, ds_sit_avaliacao";
                $sSQL .= " FROM tbdw_sit_avaliacao";
                $sSQL .= " ORDER BY ds_sit_avaliacao";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objItem = new DEFWEBsituacaoAvaliacao();
                        //
                        $objItem->Codigo = $rsDados["cd_sit_avaliacao"];
                        $objItem->Situacao = $rsDados["ds_sit_avaliacao"];
                        //
                        $RET_array[$rsDados["cd_sit_avaliacao"]] = $objItem;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }
    
    public function obterProjetos($PARM_id_cliente) {
        //
        unset($this->colProjetos);
        $this->colProjetos = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " pj.id_projeto, pj.nm_projeto, pj.cd_projeto,";
                $sSQL .= " pj.dt_inicio, pj.dt_fim,";
                $sSQL .= " pj.id_cliente, cl.ds_cliente, cl.cd_cliente";
                $sSQL .= " FROM tbdw_projeto AS pj";
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = pj.id_cliente";
                if($PARM_id_cliente != "") {
                    $sSQL .= " WHERE pj.id_cliente = " . mysqli_escape_string($objLink, $PARM_id_cliente);
                }
                $sSQL .= " ORDER BY pj.nm_projeto";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objCliente = new DEFWEBcliente();
                        //
                        $objCliente->Id = $rsDados["id_cliente"];
                        $objCliente->Nome = $rsDados["ds_cliente"];
                        $objCliente->Codigo = $rsDados["cd_cliente"];
                        //
                        $objProjeto = new DEFWEBprojeto();
                        //
                        $objProjeto->Id = $rsDados["id_projeto"];
                        $objProjeto->Codigo = $rsDados["cd_projeto"];
                        $objProjeto->Nome = $rsDados["nm_projeto"];
                        $objProjeto->DataInicio = $rsDados["dt_inicio"];
                        $objProjeto->DataTermino = $rsDados["dt_fim"];
                        $objProjeto->Cliente = $objCliente;
                        //
                        $this->colProjetos[$rsDados["id_projeto"]] = $objProjeto;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->colProjetos);
    }
        
    public function obterClientes() {
        //
        unset($this->colClientes);
        $this->colClientes = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_cliente, cd_cliente, ds_cliente ";
                $sSQL .= " FROM tbdw_cliente";
                $sSQL .= " ORDER BY ds_cliente";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objCliente = new DEFWEBcliente();
                        //
                        $objCliente->Id = $rsDados["id_cliente"];
                        $objCliente->Nome = $rsDados["ds_cliente"];
                        $objCliente->Codigo = $rsDados["cd_cliente"];
                        //
                        $this->colClientes[$rsDados["id_cliente"]] = $objCliente;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function obterAvaliacoes($PARM_id_cliente, $PARM_completo) {
        //
        unset($this->colAvaliacoes);
        $this->colAvaliacoes = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " av.id_avaliacao AS id_avaliacao, av.ds_titulo AS ds_titulo";
                if($PARM_completo == TRUE) {
                    $sSQL .= ", av.id_projeto AS id_projeto, pj.nm_projeto AS nm_projeto";
                }
                $sSQL .= " FROM tbdw_avaliacao AS av";
                if($PARM_completo == TRUE) {
                    $ssQL .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
                }
                $sSQL .= " WHERE av.id_cliente = " . mysqli_escape_string($objLink, $PARM_id_cliente);
                $sSQL .= " ORDER BY av.ds_titulo";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        if($PARM_completo == TRUE) {
                            //
                            $objAvaliacao = new DEFWEBavaliacao($rsDados["id_avaliacao"]);
                            //
                        } else {
                            //
                            $objAvaliacao = new DEFWEBavaliacao();
                            //
                            $objAvaliacao->Id = $rsDados["id_avaliacao"];
                            $objAvaliacao->Titulo = $rsDados["ds_titulo"];
                        }
                        //
                        $this->colAvaliacoes[$rsDados["id_avaliacao"]] = $objAvaliacao;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function obterFormularios($PARM_cd_sit_formulario) {
       //
        unset($this->colFormularios);
        $this->colFormularios = array();
        //
        if(DEFWEB_debug) {
            $this->DBG_db_type = DEFWEB_db_type;
            $this->DBG_db_host = DEFWEB_db_host;
            $this->DBG_db_name = DEFWEB_db_name;
            $this->DBG_db_user = DEFWEB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEB_db_host, DEFWEB_db_user, DEFWEB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " f.id_formulario AS id_formulario, f.ds_formulario AS ds_formulario, ";
                $sSQL .= " f.cd_sit_formulario AS cd_sit_formulario, ";
                $sSQL .= " s.ds_sit_formulario AS ds_sit_formulario ";
                $sSQL .= " FROM tbdw_formulario AS f";
                $sSQL .= " LEFT JOIN tbdw_sit_formulario AS s ON s.cd_sit_formulario = f.cd_sit_formulario";
                //
                if($PARM_situacao != "") {
                    $sSQL .= " WHERE f.cd_sit_formulario = " . mysqli_escape_string($objLink, $PARM_cd_sit_formulario);
                }
                //
                $sSQL .= " ORDER BY ds_formulario";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objFormulario = new DEFWEBformulario();
                        //
                        $objFormulario->Id = $rsDados["id_formulario"];
                        $objFormulario->Nome = $rsDados["ds_formulario"];
                        $objFormulario->CodigoSituacao = $rsDados["cd_sit_formulario"];
                        $objFormulario->Situacao = $rsDados["ds_sit_formulario"];
                        //
                        $this->colFormularios[$rsDados["id_formulario"]] = $objFormulario;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
}

// 0 = Qtde. total de perguntas
// 1 = Qtde. de perguntas respondidas
// 2 = Qtde. de perguntas que faltam ser respondidas
function DWS_obterQtde($PARM_id_avaliacao, $PARM_id_token, $PARM_tipo, $db_host, $db_user, $db_pass, $db_name) {
    //
    $RET_qtde = 0;
    //
    $objDWsurvey = new DEFWEBsurvey($PARM_id_avaliacao, $PARM_id_token);
    //
    if($objDWsurvey) {
        //
        switch($PARM_tipo) {
            case 1:
                $RET_qtde = $objDWsurvey->qt_respondidas;
                break;
            case 2:
                $RET_qtde = $objDWsurvey->qt_nao_respondidas;
                break;
            default:
                $RET_qtde = $objDWsurvey->qt_total;
        }
    }
    //
    return($RET_qtde);
}
