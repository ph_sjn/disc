<?php

/******
 * 
 * libdefweb_analise.php
 * 
 * Classe para obtencao das informacoes de analise
 * 
 ******/

// Configuracoes de acesso ao banco de dados ///////////////////////////////////

include_once "../config.inc.php";

// Biblioteca de classes e fun��es do DefWebExpress
//include_once("../discx/libdisc.php");
include_once("libdisc.php");

// Biblioteca de classes e fun��es para aplica��o do question�rio
//include_once("../discx/libdisc_survey.php");
include_once("libdisc_survey.php");

$DEFWEBan_db_type = $db_type;
$DEFWEBan_db_host = $db_host;
$DEFWEBan_db_user = $db_user;
$DEFWEBan_db_pass = $db_pass;
$DEFWEBan_db_name = $db_name;

define("DEFWEBan_debug", 0);
define("DEFWEBan_db_type",$DEFWEBan_db_type);
define("DEFWEBan_db_host",$DEFWEBan_db_host);
define("DEFWEBan_db_user",$DEFWEBan_db_user);
define("DEFWEBan_db_pass",$DEFWEBan_db_pass);
define("DEFWEBan_db_name",$DEFWEBan_db_name);

// Classes auxiliares //////////////////////////////////////////////////////////

// ENGAJAMENTO DOS FRANQUEADOS /////////////////////////////////////////////////

/*
 * Engajamento: soma dos produtos entre a nota das perguntas e o valor das
 *              respostas com posterior enquadramento do resultado em uma
 *              das faixas distribu�das entre os scores m�ximos e m�nimos
 *              poss�veis.
 * 
 * Com a configura��o abaixo s�o poss�veis:
 * 
 * Score m�nimo = 21            Score m�ximo = 84
 * 
 * Faixas:
 * 
 * -------------------------- ------- ------------------------------------------
 * Descri��o da faixa         Valores Como � calculada
 * -------------------------- ------- ------------------------------------------
 * 01-Ativamente n�o engajado 21 a 45 A partir do score m�nimo at� o limite
 * -------------------------- ------- ------------------------------------------
 * 02-N�o engajado            46 a 58 A partir de 30% do score m�x. + score m�n.
 *                                    at� o limite.
 * -------------------------- ------- ------------------------------------------
 * 03-Satisfa��o b�sica       59 a 66 A partir de 70% do score m�x. at� o limite
 * -------------------------- ------- ------------------------------------------
 * 04-Confiante               67 a 75 A partir de 80% do score m�x. at� o limite
 * -------------------------- ------- ------------------------------------------
 * 05-Defensor                76 a 84 A partir de 90% do score m�x. at� o limite
 * -------------------------- ------- ------------------------------------------
 */

/*
 * Tabela de perguntas utilizadas para medir o engajamento do franqueado:
 * 
 * Id   Valor Pergunta
 * ---- ----- ------------------------------------------------------------------
 * 36   2,00  Voltando no tempo, compraria novamente a franquia?
 * 37   2,00  Indicaria a franquia para parentes ou conhecidos?
 * 44   2,00  Voc� e sua equipe participam e aplicam os conte�dos aprendidos nas
 *            capacita��es da rede?
 * 45   1,00  Promove treinamentos internos com frequ�ncia para a sua equipe?
 * 46   2,00  Participa ativamente de todas as visitas de consultoria de campo, 
 *            executando o plano de a��o proposto?
 * 47   1,00  Mant�m e envia para a franqueadora informa��es de faturamento,
 *            custos e despesas do seu neg�cio?
 * 48   1,00  Reinveste no neg�cio, mantendo as instala��es e identidade visual 
 *            em ordem e atualizados?
 * 49   1,00  Mant�m um quadro de funcion�rios adequados para atender os seus 
 *            clientes com qualidade?
 * 50   2,00  Esteve presente em encontros/eventos promovidos pela franqueadora?
 * 51   2,00  D� sugest�o ou faz cr�ticas para a melhoria da opera��o e gest�o 
 *            do neg�cio?
 * 52   2,00  Realiza a��es de divulga��o local em sua unidade?
 * 53   2,00  Participa ativamente das promo��es da rede?
 * 54   1,00  � atualizado em rela��o �s comunica��es da rede? Est� por dentro 
 *            do que est� acontecendo com a rede?
 */

/* Tabela de valores das op��es (respostas):
 * 
 * Resposta                          Valor Resposta                        Valor
 * --------------------------------- ----- ------------------------------- -----
 * N�O                               1     MUITO SATISFEITO                4
 * SIM                               4     DISCORDO COMPLETAMENTE          1
 * IRRELEVANTE                       1     DISCORDO MAIS DO QUE CONCORDO   2
 * POUCO IMPORTANTE                  2     CONCORDO MAIS DO QUE DISCORDO   3
 * IMPORTANTE                        3     CONCORDO COMPLETAMENTE          4
 * MUITO IMPORTANTE                  4     NUNCA                           1
 * MUITO INSATISFEITO                1     RARAMENTE                       2
 * + INSATISFEITO DO QUE SATISFEITO  2     FREQUENTEMENTE                  3
 * + SATISFEITO DO QUE INSATISFEITO  3     SEMPRE                          4
 */

class DEFWEBengajamentoFaixa {
    //
    public $cd_engajamento = null;
    public $ds_engajamento = null;
    public $vl_faixa_inicial = null;
    public $vl_faixa_final = null;
}

class DEFWEBengajamento {
    //
    public $colFaixasEngajamento = null;
    //
    public $cd_engajamento = null;
    public $ds_engajamento = null;
    public $vl_faixa_inicial = null;
    public $vl_faixa_final = null;
    //
    public $vl_score = null;
    //
    function __construct($PARM_score) {
        //
        $this->carregarFaixasEngajamento();
        //
        if($PARM_score) {
            //
            $this->obterEngajamento($PARM_score);
        }
    }
    
    /*
     * carregarFaixasEngajamento()
     * 
     * Carrega as faixas de classifica��o de engajamento.
     * 
     */
    function carregarFaixasEngajamento() {
        //
        unset($this->colFaixasEngajamento);
        //
        $this->colFaixasEngajamento = array();
        //
        $objFaixa = new DEFWEBengajamentoFaixa();
        $objFaixa->cd_engajamento = 1;
        $objFaixa->vl_faixa_inicial = 21;
        $objFaixa->vl_faixa_final = 45;
        $objFaixa->ds_engajamento = "Ativamente n�o engajado";
        $this->colFaixasEngajamento[$objFaixa->cd_engajamento] = $objFaixa;
        //
        unset($objFaixa);
        $objFaixa = new DEFWEBengajamentoFaixa();
        $objFaixa->cd_engajamento = 2;
        $objFaixa->vl_faixa_inicial = 46;
        $objFaixa->vl_faixa_final = 58;
        $objFaixa->ds_engajamento = "N�o engajado";
        $this->colFaixasEngajamento[$objFaixa->cd_engajamento] = $objFaixa;
        //
        unset($objFaixa);
        $objFaixa = new DEFWEBengajamentoFaixa();
        $objFaixa->cd_engajamento = 3;
        $objFaixa->vl_faixa_inicial = 59;
        $objFaixa->vl_faixa_final = 66;
        $objFaixa->ds_engajamento = "Satisfa��o b�sica";
        $this->colFaixasEngajamento[$objFaixa->cd_engajamento] = $objFaixa;
        //
        unset($objFaixa);
        $objFaixa = new DEFWEBengajamentoFaixa();
        $objFaixa->cd_engajamento = 4;
        $objFaixa->vl_faixa_inicial = 67;
        $objFaixa->vl_faixa_final = 75;
        $objFaixa->ds_engajamento = "Confiante";
        $this->colFaixasEngajamento[$objFaixa->cd_engajamento] = $objFaixa;
        //
        unset($objFaixa);
        $objFaixa = new DEFWEBengajamentoFaixa();
        $objFaixa->cd_engajamento = 5;
        $objFaixa->vl_faixa_inicial = 76;
        $objFaixa->vl_faixa_final = 84;
        $objFaixa->ds_engajamento = "Defensor";
        $this->colFaixasEngajamento[$objFaixa->cd_engajamento] = $objFaixa;
    }
    
    /*
     * obterEngajamento( <score> ) : objEngajamento
     * 
     * Retorna um objeto de engajamento de acordo com a classifica��o
     * do score recebido como par�metro.
     */
    public function obterEngajamento($PARM_score) {
        //
        $this->vl_score = $PARM_score;
        //
        // Para cada faixa...
        foreach($this->colFaixasEngajamento AS $objFaixa) {
            // Se o score estiver na faixa...
            if(($PARM_score >= $objFaixa->vl_faixa_inicial) && ($PARM_score <= $objFaixa->vl_faixa_final)) {
                // Atribui os valores da faixa aos atributos do objeto
                $this->cd_engajamento = $objFaixa->cd_engajamento;
                $this->ds_engajamento = $objFaixa->ds_engajamento;
                $this->vl_faixa_inicial = $objFaixa->vl_faixa_inicial;
                $this->vl_faixa_final = $objFaixa->vl_faixa_final;
                // Abandona o la�o
                break;
            }
        }
        //
        return($this);
    }
}

// FRANQUEADO //////////////////////////////////////////////////////////////////

class DEFWEBfranqueadoAnalise {
    // id_token
    public $Id = null;
    // ds_codigo
    public $Codigo = null;
    // cd_hash
    public $Hash = null;
    // ds_nome
    public $Nome = null;
    // ds_email
    public $Email = null;
    // ds_observacao
    public $Observacoes = null;
    // ds_unidade
    public $Unidade = null;
    // id_cliente
    public $IdCliente = null;
    //
    public $Engajamento = null;
}

// Classe principal das analises ///////////////////////////////////////////////

class DEFWEBmasterAnalise {
    
    // Id da avalia��o
    public $id_avaliacao = null;
    // Cole��o de franqueados
    public $colFranqueados = null;
    
    // Atributos para checagem de erros nas chamadas e no acesso ao
    // banco de dados
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";

    function __construct($PARM_id_avaliacao) {
        //
        $this->id_avaliacao = $PARM_id_avaliacao;
    }

    /**
     *
     * obterScoreEngajamento( <Id do franqueado> ) : score 
     * 
     * Calcula o score de engajamento para o id do token informado (franqueado).
     * 
     */
    public function obterScoreEngajamento($PARM_id_franqueado) {
        //
        $RET_vl_score = null;
        //
        if(DEFWEBan_debug) {
            $this->DBG_db_type = DEFWEBan_db_type;
            $this->DBG_db_host = DEFWEBan_db_host;
            $this->DBG_db_name = DEFWEBan_db_name;
            $this->DBG_db_user = DEFWEBan_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBan_db_host, DEFWEBan_db_user, DEFWEBan_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBan_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " SUM(oitem.vl_opcao_valor * item.vl_item_peso) AS vl_score";
                $sSQL .= " FROM tbdw_af_token_item AS afitem";
                $sSQL .= " LEFT JOIN tbdw_opcao_item AS oitem ON oitem.cd_opcao_item = afitem.cd_opcao_item";
                $sSQL .= " LEFT JOIN tbdw_item AS item ON item.cd_item = oitem.cd_item";
                $sSQL .= " WHERE id_token = " . mysqli_escape_string($objLink, $PARM_id_franqueado);
                $sSQL .= " AND afitem.cd_item IN (36, 37, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54)";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    if($rsDados) {
                        //
                        $RET_vl_score = $rsDados["vl_score"];
                    }
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_vl_score);
    }

    /***
     * 
     * obterFranqueados( <Id da Avalia��o> ) : cole��o de franqueados
     * 
     * Obt�m todos os franqueados da avalia��o, carrega na cole��o do objeto
     * e devolve a cole��o.
     * 
     */
    function obterFranqueados($PARM_id_avaliacao) {
        //
        unset($this->colFranqueados);
        $this->colFranqueados = array();
        //
        if(DEFWEBan_debug) {
            $this->DBG_db_type = DEFWEBan_db_type;
            $this->DBG_db_host = DEFWEBan_db_host;
            $this->DBG_db_name = DEFWEBan_db_name;
            $this->DBG_db_user = DEFWEBan_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(DEFWEBan_db_host, DEFWEBan_db_user, DEFWEBan_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, DEFWEBan_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.cd_hash AS cd_hash";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = tk.cd_publico_alvo";
                $sSQL .= " WHERE tk.id_avaliacao = " . mysqli_escape_string($objLink, $this->id_avaliacao);
                $sSQL .= " AND tk.cd_publico_alvo = 2";
                $sSQL .= " ORDER BY ds_nome";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while($rsDados) {
                        //
                        $objFranqueado = new DEFWEBfranqueadoAnalise();
                        //
                        $objFranqueado->Id = $rsDados["id_token"];
                        $objFranqueado->Codigo = $rsDados["ds_codigo"];
                        $objFranqueado->Nome = $rsDados["ds_nome"];
                        $objFranqueado->Hash = $rsDados["cd_hash"];
                        $objFranqueado->Email = $rsDados["ds_email"];
                        $objFranqueado->Observacoes = $rsDados["ds_observacao"];
                        $objFranqueado->Unidade = $rsDados["ds_unidade"];
                        $objFranqueado->IdCliente = $rsDados["id_cliente"];
                        //
                        $this->colFranqueados[$objFranqueado->Id] = $objFranqueado;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->colFranqueados);
    }
    
    /***
     * 
     * carregarEngajamentoFranqueados()
     * 
     * Obt�m o score e faz a classifica��o de engajamento para cada um dos
     * franqueados da cole��o.
     */
    function carregarEngajamentoFranqueados() {
        // Inicializa o objeto do engajamento
        $objEngajamento = null;
        // Para cada franqueado da lista...
        foreach ($this->colFranqueados AS $objFranqueado) {
            // Calcula o score
            $vlScore = $this->obterScoreEngajamento($objFranqueado->Id);
            // Se obteve o score...
            if(!is_null($vlScore)) {
                // Faz a classifica��o do engajamento
                unset($objEngajamento);
                $objEngajamento = new DEFWEBengajamento($vlScore);
                // Atribui o engajamento ao franqueador
                $this->colFranqueados[$objFranqueado->Id]->Engajamento = $objEngajamento;
            }
        }
    }

}
