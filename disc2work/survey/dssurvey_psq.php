<?php
// Redireciona caso nao receba os parametros obrigatorios
if (!isset($_POST["id_avaliacao"]) && !isset($_POST["id_cliente"])){
	header("Location: ../site");
	die();
}
// Parametros obrigatorios informados
$REQ_DW_id_avaliacao = $_POST["id_avaliacao"];
$REQ_DW_id_cliente	 = $_POST["id_cliente"];
//
// Se vieram os parametros obrigatorios...
if(!$REQ_DW_id_avaliacao && !$REQ_DW_id_cliente) {
	header("Location: ../site");
	die();
}
//
//
// Biblioteca de classes e funcoes do DefWebExpress
include_once("lib/libdisc_analise.php");
//
//
$REQ_DW_cd_situacao  	 = 0;
$REQ_DW_flag_include	 = 0;
$REQ_DW_ds_titulo 	 	 = "";
$REQ_DW_rpt_pesquisa 	 = "";
$REQ_DW_msg_erro 	 	 = "";
//
// SAO OS TEMPLATES COM OS TEXTOS DA ANALISE DO RESULTADO APRESENTADOS NO RELATORIO
$DIR_rpt_template_basico = "templates/5/basico/";
//
//
$objLink = mysqli_connect($db_host, $db_user, $db_pass);
if(!$objLink) {
	$total_pesquisas = 0;
} else {	
	//
	if(mysqli_select_db($objLink, $db_name)) {
		//
		$sql = "SELECT";
		$sql .= " pj.nm_projeto AS nm_projeto";
		$sql .= ", av.ds_titulo AS ds_titulo";
		$sql .= ", av.cd_sit_avaliacao AS cd_sit_avaliacao";
		$sql .= ", av.dt_inicio AS dt_inicio_avaliacao";
		$sql .= ", av.dt_fim AS dt_fim_avaliacao";
		$sql .= ", av.nmarq_topo_interno AS IMGLogoID";
		$sql .= ", av.nmarq_topo_original AS IMGLogoName";
		$sql .= ", pgadm.cd_pagina AS pgadm_cd_pagina";
		$sql .= ", pgadm.ds_pagina AS pgadm_ds_pagina";
		$sql .= ", pgadm.cd_tipo_pagina AS pgadm_cd_tipo";
		$sql .= ", pgadm.dv_include AS pgadm_dv_include";
		$sql .= " FROM tbdw_avaliacao AS av";
		$sql .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
		$sql .= " LEFT JOIN tbdw_pagina AS pgadm ON pgadm.id_pagina = av.id_pagina_reladmin";
		$sql .= sprintf(" WHERE av.id_cliente = %d", $REQ_DW_id_cliente);
		$sql .= sprintf(" AND av.id_avaliacao = %d", $REQ_DW_id_avaliacao);
		//
		$objResult = mysqli_query($objLink, $sql);
		if(!$objResult) {
			$total_pesquisas = 0;
		} else {
			//
			$total_pesquisas = 1;
			//
			$rsDados = mysqli_fetch_assoc($objResult);
			//
			/*
			echo "<pre>";
			print_r($rsDados);
			echo "</pre>";
			*/
			//
		}
	}
}
//
//echo "total_pesquisas:[$total_pesquisas]<br>";
//
if ( $total_pesquisas < 1 ){
	//
	header("Location: ../site");
	die();
	//
} else {
	//
	//$REQ_DW_ds_titulo = $rsDados["nm_projeto"] . " : " . $rsDados["ds_titulo"];
	$REQ_DW_ds_titulo = $rsDados["ds_titulo"];
	//
	if ( $rsDados["cd_sit_avaliacao"] != 2 ) {
		// Se for diferente de 2=Nao liberada
		$REQ_DW_cd_situacao = 1;
	} else {
		$REQ_DW_cd_situacao = 0;
	}
	//
	$REQ_DW_flag_include = $rsDados["pgadm_dv_include"];
	if ( $rsDados["pgadm_dv_include"] == 1 ) {
		//
		$REQ_DW_rpt_pesquisa = substr($rsDados["pgadm_ds_pagina"], 0, count($rsDados["pgadm_ds_pagina"])-5) . "_psq.php";
		//
	} else {
		//
		$REQ_DW_rpt_pesquisa = $rsDados["pgadm_ds_pagina"];
		//
	}
	//echo "REQ_DW_rpt_pesquisa:[$REQ_DW_rpt_pesquisa]<br>";
	//
	//-----------------------------------------------------------------------------------------------------
	// IMAGEM DO LOGO
	//-----------------------------------------------------------------------------------------------------
	if ( !empty($rsDados["IMGLogoID"]) ){
		//
		$type = "png";
		if ( !empty($rsDados["IMGLogoName"]) ){
			if ( strpos($rsDados["IMGLogoName"], ".") !== false ){
				$type = substr($rsDados["IMGLogoName"], strpos($objDWsurvey->IMGLogoName, ".")+1);
			}
		}
		//
		//$IMGLogo = docGet($upload_dir.$rsDados["IMGLogoID"], "");
		$IMGLogo = docGetThumb($upload_dir.$rsDados["IMGLogoID"], 0, 0, $type, 100);
		/*
		//
		if ( $REQ_DW_id_avaliacao == 7 ) {
			$imagedata = file_get_contents("img/logo.png");
		} else {
			$imagedata = file_get_contents("img/Topo_Basico.png");
		}
		$base64 = base64_encode($imagedata);
		$IMGLogo = "data:image/jpeg;base64,$base64";
		*/
		//
	} else {
		//
		if ( $REQ_DW_id_avaliacao == 7 ) {
			$imagedata = file_get_contents("img/logo.png");
		} else {
			$imagedata = file_get_contents("img/Topo_Basico.png");
		}
		$base64 = base64_encode($imagedata);
		$IMGLogo = "data:image/jpeg;base64,$base64";
	}
	//
	//
	$objDEFWEBmasterAnalise = new DEFWEBmasterAnalise($REQ_DW_id_avaliacao);
	//
	$objDEFWEBmasterAnalise->obterFranqueados($REQ_DW_id_avaliacao);
	if ( $objDEFWEBmasterAnalise->ERRO != 0 ){
		$REQ_DW_msg_erro = $objDEFWEBmasterAnalise->ERRO_mensagem;
		$REQ_DW_cd_situacao = 0;
	} else {
		//
		// Cria matriz da pesquisa
		$discPesquisa = array(
			"D" => array(
				"M" => 0,
				"L" => 0,
				"A" => 0
			),
			"I" => array(
				"M" => 0,
				"L" => 0,
				"A" => 0
			),
			"S" => array(
				"M" => 0,
				"L" => 0,
				"A" => 0
			),
			"C" => array(
				"M" => 0,
				"L" => 0,
				"A" => 0
			)
		);
		//
		$REQ_DW_nr_entrevistados = 0;
		//
		foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
			//
			unset($objDWsurvey);
			$objDWsurvey = new DEFWEBsurvey($REQ_DW_id_avaliacao, $objFranqueado->Id);
			if ($objDWsurvey->qt_nao_respondidas == 0) {
				//
				// Contar os entrevistados
				$REQ_DW_nr_entrevistados++;
				//
				// Acumular os pontos M e L do disc dos entrevistados
				$discPesquisa["D"]["M"] += $objDWsurvey->colDISC["D"]["M"];
				$discPesquisa["D"]["L"] += $objDWsurvey->colDISC["D"]["L"];
				$discPesquisa["I"]["M"] += $objDWsurvey->colDISC["I"]["M"];
				$discPesquisa["I"]["L"] += $objDWsurvey->colDISC["I"]["L"];
				$discPesquisa["S"]["M"] += $objDWsurvey->colDISC["S"]["M"];
				$discPesquisa["S"]["L"] += $objDWsurvey->colDISC["S"]["L"];
				$discPesquisa["C"]["M"] += $objDWsurvey->colDISC["C"]["M"];
				$discPesquisa["C"]["L"] += $objDWsurvey->colDISC["C"]["L"];
				//
				$objDiscNormalizado = new DEFWEBdisc_normalizado();
				$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
				//
				// Acumular os pontos A normalizados dos entrevistados
				$discPesquisa["D"]["A"] += $objDiscNormalizado->colDISC_convertido["D"];
				$discPesquisa["I"]["A"] += $objDiscNormalizado->colDISC_convertido["I"];
				$discPesquisa["S"]["A"] += $objDiscNormalizado->colDISC_convertido["S"];
				$discPesquisa["C"]["A"] += $objDiscNormalizado->colDISC_convertido["C"];
			}
		}
		if ( $REQ_DW_nr_entrevistados > 0 ){
			//
			// Transformando o acumulado em media da pesquisa
			$discPesquisa["D"]["M"] = intval(round(($discPesquisa["D"]["M"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["L"] = intval(round(($discPesquisa["D"]["L"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["M"] = intval(round(($discPesquisa["I"]["M"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["L"] = intval(round(($discPesquisa["I"]["L"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["M"] = intval(round(($discPesquisa["S"]["M"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["L"] = intval(round(($discPesquisa["S"]["L"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["M"] = intval(round(($discPesquisa["C"]["M"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["L"] = intval(round(($discPesquisa["C"]["L"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["A"] = intval(round(($discPesquisa["D"]["A"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["A"] = intval(round(($discPesquisa["I"]["A"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["A"] = intval(round(($discPesquisa["S"]["A"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["A"] = intval(round(($discPesquisa["C"]["A"]/$REQ_DW_nr_entrevistados),0,PHP_ROUND_HALF_UP));
			// Monta a serie do grafico normalizado
			$discPesqSerieNormalizada = "[" . $discPesquisa["D"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["I"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["S"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["C"]["A"];
			$discPesqSerieNormalizada .= "]";
			//
			// Calculando o A da pesquisa para obter o resultado
			$discPesquisa["D"]["A"] = ($discPesquisa["D"]["M"]-$discPesquisa["D"]["L"]);
			$discPesquisa["I"]["A"] = ($discPesquisa["I"]["M"]-$discPesquisa["I"]["L"]);
			$discPesquisa["S"]["A"] = ($discPesquisa["S"]["M"]-$discPesquisa["S"]["L"]);
			$discPesquisa["C"]["A"] = ($discPesquisa["C"]["M"]-$discPesquisa["C"]["L"]);
			// Cria o objeto Normalizador
			$discPesqNormalizado = new DEFWEBdisc_normalizado();
			$discPesqNormalizado->normalizar_disc($discPesquisa, "");
			// Obtem o resultado da pesquisa
			$sLetters   = $discPesqNormalizado->resultado;
			$result_fmt = "";
			for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
				if ( $nLetter == 0 ) {
					$result_fmt = "<span class=\"disc_letter_main\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				} else {
					$result_fmt .= "<span class=\"disc_letter_norm\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				}
			}
		} else {
			//
			$REQ_DW_msg_erro = "NENHUM ENTREVISTADO RESPONDEU AO QUESTIONÁRIO!";
			$REQ_DW_cd_situacao = 0;
			//
		}
		//
	}
	//
	//echo "REQ_DW_cd_situacao:[$REQ_DW_cd_situacao]<br>";
	//echo "REQ_DW_nr_entrevistados:[$REQ_DW_nr_entrevistados]<br>";
	//
}
//
?>
<!DOCTYPE html>
<html>
    <head>
		<? // EXIBE O NOME DA EXTRANET ESPECIFICADO NO ARQUIVO DE IDIOMAS, NA PASTA /LANG ?>
		<title>disc2work - <?= $REQ_DW_ds_titulo ?></title>

		<?// FAVICON PARA PC ?>
		<link rel="shortcut icon" href="img-icons/favicon.ico" />

		<?//FAVICON PARA DISPOSITIVOS MOVEIS?>
		<link rel="apple-touch-icon" sizes="144x144" href="img-icons/touch-icon-ipad-retina.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="img-icons/touch-icon-iphone-retina.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="img-icons/touch-icon-ipad.png" />
		<link rel="apple-touch-icon" href="img-icons/touch-icon-iphone.png" />

		<?// Estilos: Bootstrap e outros ?>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/normalize.css" type="text/css" rel="stylesheet">
		<link href="css/padrao.css" type="text/css" rel="stylesheet">
		<link href="css/dvtable.css" type="text/css" rel="stylesheet">
		<link href="css/survey.css" type="text/css" rel="stylesheet">

		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
		<meta http-equiv="Cache-Control" content="no-store" />
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?// JQuery ?>
		<script src="js/jquery-3.2.0.min.js"></script>
		
		<?// JQuery UI ?>
		<link rel="stylesheet" href="css/jquery-ui.min.css">
		<script src="js/external/jquery/jquery.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		
		<?// Bootstrap - dependencia ?>
		<script src="js/bootstrap.min.js"></script>
    </head>

    <body>

        <!-- Google Analytics inicio -->
		<!--
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-38250366-1']);
            _gaq.push(['_setDomainName', '<?=$web_host?>.franquiaextranet.com.br']);
            _gaq.push(['_trackPageview']);
            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
		-->
        <!-- Google Analytics Fim -->
		<?php
		//---------------------------------------------------------------------
		// CONTAINER
		//---------------------------------------------------------------------
		?>
		<div class="container-fluid">
			<?php 
			//---------------------------------------------------------------------
			// LOGO 
			//---------------------------------------------------------------------
			?>
			<div id="dsTopImg" class="row">
				<img src="<?php echo($IMGLogo); ?>" class="img-responsive center-block"/>
			</div>
			<?php
			//---------------------------------------------------------------------
			// CONTEUDO
			//---------------------------------------------------------------------
			?>
			<div id="dsContent" class="row">
				<?php
				// A situacao deve ser 1 (Liberada/Em andamento)
				if ( $REQ_DW_cd_situacao == 1 ){
				?>
				<form action="dssurvey_psq.php" method="post" name="frmSurveyPsq" class="form-horizontal">
					<?php
						//---------------------------------------------------------------------
						// TEMPLATE
						//---------------------------------------------------------------------
						// RELATORIO PARA A PESQUISA
						if ( $REQ_DW_flag_include == 1 ) {
							include($REQ_DW_rpt_pesquisa);
						} else {
							echo $REQ_DW_rpt_pesquisa;
						}
						//---------------------------------------------------------------------
						// TEMPLATES
						//---------------------------------------------------------------------
						mysqli_close($objLink);
					?>
					<input type="hidden" id="id_avaliacao" name="id_avaliacao" value="<?php echo($REQ_DW_id_avaliacao); ?>" />
					<input type="hidden" id="id_cliente"   name="id_cliente"   value="<?php echo($REQ_DW_id_cliente); ?>"   />
				</form>
				<?php
					//
				} else {
				?>
					<div class="panel panel-default center-block" style="max-width:600px;">
						<div class="panel-heading">
							<h3>
								<strong>
									<?php echo($REQ_DW_ds_titulo); ?>
								</strong>
							</h3>
						</div>
						<div class="panel-body">
						<?php
							//
							echo("<pre>" . $REQ_DW_msg_erro . "</pre>");
							//
						?>
						</div>
					</div>
				<?php
				}
				?>
			</div>
			<?php
			//---------------------------------------------------------------------
			// CONTEUDO
			//---------------------------------------------------------------------
			?>
		</div>
		<?php
		//---------------------------------------------------------------------
		// CONTAINER
		//---------------------------------------------------------------------
		?>
    </body>
</html>