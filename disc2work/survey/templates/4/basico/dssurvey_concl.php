<?php 
$REQ_DW_id_item_formulario = $_POST["id_item_formulario"];
$REQ_DW_id_item_anterior   = $_POST["id_item_anterior"];
?>
				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
						<h4>
								<!--Entrevistado:-->
								<?php 
									echo($objDWsurvey->ds_nome_token);
									if($objDWsurvey->ds_unidade != "") {
										//
										echo(" (" . $objDWsurvey->ds_unidade . ")");
									}
								?>
						</h4>
					</div>
					<div class="panel-body">
						<h3>Parab�ns!</h3>
						<p><strong>Voc� acabou de finalizar o question�rio.</strong></p>
						<p><strong>Agora voc� pode clicar no bot�o CONCLUIR para enviar o question�rio ou, se achar que precisa mudar alguma das respostas, clicar no bot�o ANTERIOR.</strong></p>
						<nav aria-label="...">
							<ul id="cmdConclusao" class="pager">
								<li class="previous"><a href="#" onclick="q_anterior()"><span aria-hidden="true">&larr;</span> Anterior</a></li>
								<li class="next"><a href="#" onclick="q_concluir()">Concluir</a></li>
							</ul>
						</nav>
						<input type='hidden' name="navegarAnterior" value=""/>
						<input type='hidden' name="navegarProxima" value=""/>
						<input type="hidden" name="hdnIdAnterior" id="hdnIdAnterior" value="<?php echo($REQ_DW_id_item_anterior); ?>" />
						<!-- value N indica que a questao esta intacta, se for S, foi alterada pelo usuario -->
						<input type='hidden' name="questaoAlterada" value="N"/>
						<input type="hidden" id="id_item_formulario" name="id_item_formulario" value="<?php echo($REQ_DW_id_item_formulario); ?>" />
						<input type="hidden" id="id_item_anterior" name="id_item_anterior" value="<?php echo($REQ_DW_id_item_anterior); ?>" />
						<p id="qstMsg" class="text-danger"><?php echo($DW_mensagem_erro); ?></p>
					</div>
				</div>
				<script>
					//
					// BOTAO ANTERIOR
					function q_anterior(){
						var etapa = $('input[name=etapa]');
						if ( etapa !== undefined ){
							// VOLTA PARA A ETAPA 3: QUESTIONARIO - SEGUE PARA A QUESTAO ANTERIOR
							etapa.val('3');
						}
						var btnAnterior = $('input[name=navegarAnterior]');
						if ( btnAnterior !== undefined ){
							btnAnterior.val('Anterior');
							$('form[name="frmSurvey"]').submit();
						}
					}
					// BOTAO PROXIMA QUESTAO
					function q_concluir(){
						var etapa = $('input[name=etapa]');
						if ( etapa !== undefined ){
							// SEGUE PARA A ETAPA 5: RELATORIO
							etapa.val('5');
						}
						var btnProxima = $('input[name=navegarProxima]');
						if ( btnProxima !== undefined ){
							btnProxima.val('Concluir');
							$('form[name="frmSurvey"]').submit();
						}
					}
				</script>