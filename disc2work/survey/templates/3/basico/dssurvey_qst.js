//
var doc_init = false;
//
$( document ).ready(function() {
	var inradio = $('input[type=radio]');
	if ( inradio !== undefined ){
		doc_init = true;
	}
	var chk_respostaP = $('input[id=ds_resposta_p]:checked').val();
	var chk_respostaM = $('input[id=ds_resposta_m]:checked').val();
	if ( chk_respostaP !== undefined || chk_respostaM !== undefined ){
		if ( chk_respostaP != chk_respostaM ){
			var cmdQuestionNext = $("#cmdQuestion li.next");
			if ( cmdQuestionNext !== undefined ){
				if ( cmdQuestionNext.hasClass("disabled") ){
					cmdQuestionNext.removeClass("disabled");
				}
			}
		}
	}
});
//
// VERIFICA SELECAO/ALTERACAO NAS RESPOSTAS
$('input[type=radio]').on('change', function() {
	$('#qstMsg').empty();
	var chk_respostaP = $('input[id=ds_resposta_p]:checked').val();
	var chk_respostaM = $('input[id=ds_resposta_m]:checked').val();
	//
	var statusSelecao;
	if ( chk_respostaP === undefined || chk_respostaM === undefined ){
		statusSelecao = false;
	} else if ( chk_respostaP == chk_respostaM ){
		statusSelecao = false;
		$('#qstMsg').html("N�o � permitido duas respostas na mesma pergunta.");
	} else {
		statusSelecao = true;
	}
	//
	// obtem o botao de comando: 'next'
	var cmdQuestionNext 	= $("#cmdQuestion li.next");
	//
	var questaoAlterada = $('input[name=questaoAlterada]');
	if ( questaoAlterada !== undefined && doc_init ){
		questaoAlterada.val('S');
	}
	//
	// configura o objeto comando: 'next'
	if ( statusSelecao ) {
		if ( cmdQuestionNext !== undefined ){
			if ( cmdQuestionNext.hasClass("disabled") ){
				cmdQuestionNext.removeClass("disabled");
			}
		}
	} else {
		if ( cmdQuestionNext !== undefined ){
			if ( !cmdQuestionNext.hasClass("disabled") ){
				cmdQuestionNext.addClass("disabled");
			}
		}
	}
});
//
// EXIBIR|OCULTAR INSTRUCOES
function instrucoes(){
	$("#pnlInstrucoes").fadeToggle("slide");
	$("#pnlInstrucoes").removeClass("hidden");
}
//
// BOTAO QUESTAO ANTERIOR
function q_anterior(){
	var etapa = $('input[name=etapa]');
	if ( etapa !== undefined ){
		// PERMANECE NA ETAPA 3: QUESTIONARIO - SEGUE PARA A QUESTAO ANTERIOR
		etapa.val('3');
	}
	var btnAnterior = $('input[name=navegarAnterior]');
	if ( btnAnterior !== undefined ){
		btnAnterior.val('Anterior');
		$('form[name="frmSurvey"]').submit();
	}
}
//
// BOTAO PROXIMA QUESTAO
function q_proxima(){
	var chk_respostaP = $('input[id=ds_resposta_p]:checked').val();
	var chk_respostaM = $('input[id=ds_resposta_m]:checked').val();

	if ( chk_respostaP === undefined || chk_respostaM === undefined ){
		return;
	} else if ( chk_respostaP == chk_respostaM ){
		return;
	} else {
		var etapa = $('input[name=etapa]');
		if ( etapa !== undefined ){
			// PERMANECE NA ETAPA 3: QUESTIONARIO - SEGUE PARA A PROXIMA QUESTAO
			etapa.val('3');
		}
		var btnProxima = $('input[name=navegarProxima]');
		if ( btnProxima !== undefined ){
			btnProxima.val('Proxima');
			var cmdQuestionNext = $("#cmdQuestion li.next");
			if ( cmdQuestionNext !== undefined ){
				cmdQuestionNext.addClass("disabled");
			}
			$('form[name="frmSurvey"]').submit();
		}
	}
}
//
function q_ultima(){
	var chk_respostaP = $('input[id=ds_resposta_p]:checked').val();
	var chk_respostaM = $('input[id=ds_resposta_m]:checked').val();
	//
	if ( chk_respostaP === undefined || chk_respostaM === undefined ){
		return;
	} else if ( chk_respostaP == chk_respostaM ){
		return;
	} else {
		var etapa = $('input[name=etapa]');
		if ( etapa !== undefined ){
			// SEGUE PARA A ETAPA 4: CONCLUSAO
			etapa.val('4');
		}
		var btnProxima = $('input[name=navegarProxima]');
		if ( btnProxima !== undefined ){
			btnProxima.val('Concluir');
			var cmdQuestionNext = $("#cmdQuestion li.next");
			if ( cmdQuestionNext !== undefined ){
				cmdQuestionNext.addClass("disabled");
			}
			$('form[name="frmSurvey"]').submit();
		}
	}
}
//