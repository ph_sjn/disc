<?php
$REQ_DW_id_item_formulario 	= $_POST["id_item_formulario"];
$REQ_DW_id_item_anterior 	= $_POST["id_item_anterior"];
$REQ_DW_ds_resposta 		= $_POST["ds_resposta"];
//
$REQ_DW_ds_resposta_p 	 	= $_POST["ds_resposta_p"];
$REQ_DW_ds_resposta_m 	 	= $_POST["ds_resposta_m"];
//
$REQ_DW_nr_posicao 		 	= $_POST["nr_posicao"];
//
$REQ_DW_navegar_primeira 	= $_POST["navegarPrimeira"];
$REQ_DW_id_primeira 	 	= $_POST["hdnIdPrimeira"];
$REQ_DW_navegar_anterior 	= $_POST["navegarAnterior"];
$REQ_DW_id_anterior 	 	= $_POST["hdnIdAnterior"];
$REQ_DW_navegar_proxima  	= $_POST["navegarProxima"];
$REQ_DW_id_proxima 		 	= $_POST["hdnIdProxima"];
$REQ_DW_questao_alterada 	= $_POST["questaoAlterada"];
//
$REQ_DW_tipo = $_POST["hdnTipo"];
if($REQ_DW_tipo = "X") {
    //
    $REQ_DW_ds_resposta = $REQ_DW_ds_resposta_p . "|" . $REQ_DW_ds_resposta_m;
    //
    if($REQ_DW_ds_resposta == "|") {
        $REQ_DW_ds_resposta = "";
    }
}
// Flag indicadora para zerar o questionario antes de inicia-lo
$REQ_DW_zerar_qst = false;
//
if (!$REQ_DW_id_item_formulario){
	$REQ_DW_id_item_formulario = $objDWsurvey->id_primeira_pergunta;
	// Primeira pergunta ... indicar na flag para zerar o questionario
	$REQ_DW_zerar_qst = true;
}
//
// TEMPO LIMITE PARA RESPONDER O QUESTIONARIO (EM SEGUNDOS)
$REQ_DW_limit_time = ( 12 * 60 );
//
// Tempo restante para a finalizacao da avaliacao - Formato: hh:mm:ss
$REQ_DW_remainder_time = "";
if ( isset($REQ_DW_id_item_formulario) ){
	//
	// Calcular o tempo percorrido desde o inicio da avaliacao ate o momento
	//
	$flgDate1 = false;
	if ( $REQ_DW_id_item_formulario == $objDWsurvey->id_primeira_pergunta ){
		$date1 = new DateTime();
		$REQ_DW_questao_alterada = "S";
		$flgDate1 = true;
	} else {
		if ( preg_match ("/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/", $objDWsurvey->dt_inicio_token) ){
			$date1 = new DateTime($objDWsurvey->dt_inicio_token);
			$flgDate1 = true;
		}
	}
	//
	if ( $flgDate1 ){
		$date2 = new DateTime();
		$interval = $date2->diff($date1);
		//
		// Convertendo em segundos
		$seconds_diff = ($interval->format('%i')*60) + $interval->format('%s');
		// Se nao houver diferenca na data e hora e nos segundos eh inferior ao REQ_DW_limit_time ( em segundos)
		// Calcular o tempo que resta
		if ( $interval->format('%d') == 0 && $interval->format('%m') == 0 && $interval->format('%y') == 0 &&
			$interval->format('%h') == 0 && ( $seconds_diff < $REQ_DW_limit_time ) ) {
			//
			$m_remainder = $REQ_DW_limit_time - $seconds_diff;
			$REQ_DW_remainder_time = gmdate('H:i:s', $m_remainder);
		} else {
			$REQ_DW_remainder_time = "00:00:00";
		}
	}
}
//
// Limpa os indicadores de erro
$DW_veio_resposta = false;
$DW_mensagem_erro = "";
$bFalhou 		  = false;
//
//
//----------------------------------------------------------------------------------------
// AVALIACAO DO TEMPO DISPONIVEL DO QUESTIONARIO
if ( $REQ_DW_id_item_formulario != $objDWsurvey->id_primeira_pergunta ){
	// Nao eh a primeira pergunta
	if ( ($REQ_DW_remainder_time == "00:00:00") && 
		 ($objDWsurvey->qt_nao_respondidas > 0) ){
		// Tempo expirou e ainda tem perguntas nao respondidas
		//
		if ( ($REQ_DW_navegar_proxima == "Proxima") ||
			 ($REQ_DW_navegar_anterior == "Anterior") ){
			//
			//Esta navegando...Encerra o questionario e redireciona para o inicio do processo
			$REQ_DW_questao_alterada = "N";
			$REQ_DW_ds_resposta = "";
			// VOLTAR PARA A ETAPA INICIAL DO PROCESSO
			$REQ_DW_etapa = 1;
			$bFalhou = true;
			$REQ_DW_time_qst_expirou = true;
			//
		}
	} else if ( ($REQ_DW_remainder_time == "00:00:00") && 
				($objDWsurvey->qt_nao_respondidas == 0) ){
		// Tempo expirou e o questionario foi respondido
		// Redireciona para o relatorio
		//
		// SEGUE PARA A ETAPA 5: RELATORIO
		$REQ_DW_questao_alterada = "N";
		$REQ_DW_ds_resposta = "";
		$REQ_DW_etapa = 5;
		$REQ_DW_time_qst_expirou = true;
		$bFalhou = true;
	}
} else {
	//Primeira pergunta ... forcar a gravacao mesmo que venha indicado que
	// nao houve alteracao, pois existem casos onde o tempo de resposta expirou
	// e o questionario nao foi respondido totalmente. 
	// E necessario registrar a nova data de inicio no token.
	if ( ($REQ_DW_questao_alterada == "N") && ($REQ_DW_time_qst_expirou == true) ){
		$REQ_DW_time_qst_expirou = false;
		$REQ_DW_questao_alterada = "S";
	}
}
// ZERAR O QUESTIONARIO.
if ( $REQ_DW_zerar_qst ){
	//
	// Cria um novo objeto token
	$objToken = new clsDWCtoken();
	//
	// Preenche os parametros
	$objToken->Id = $REQ_DW_id_token;
	$objToken->IdCliente = $objDWsurvey->id_cliente;
	$objToken->IdAvaliacao = $REQ_DW_id_avaliacao;
	//
	// Instancia objeto da classe Geofusion
	$objDWC = new clsDWCmaster();
	//
	$objDWC->reiniciarPesquisaToken($objToken);
	//
}
//
//----------------------------------------------------------------------------------------
// GRAVACAO DA RESPOSTA ENVIADA
//
// Se o request veio de uma pergunta ja exibida ao usuario e nao for navegacao...
if(($REQ_DW_id_item_formulario) && 
   ($REQ_DW_questao_alterada == "S") &&
   (($REQ_DW_navegar_anterior == "Anterior") || ($REQ_DW_navegar_proxima == "Proxima") || ($REQ_DW_navegar_proxima == "Concluir")) &&
   ($REQ_DW_ds_resposta)) {
	//
	$DW_veio_resposta = TRUE;
	// Se nao tiver posicao definida para a questao...
	if(!($REQ_DW_nr_posicao) || ($REQ_DW_nr_posicao == "")) {
		// Atribui NULL para a gravacao da resposta
		$REQ_DW_nr_posicao = "NULL";
	}
	// Se veio resposta em branco...
	if((!$REQ_DW_ds_resposta) && 
	   ($REQ_DW_id_item_anterior != "") && 
	   ($REQ_DW_id_item_anterior == $REQ_DW_id_item_formulario)) {
		//
		// Ajusta a mensagem para exibicao ao usuario
		$DW_mensagem_erro = $objDWsurvey->obterMensagem("MSG_ERRO_SEMRESPOSTA");
	}
	//
	// Cria as entradas na avaliacao para o token do usuario e,
	// se deu erro...
	if($objDWsurvey->criarAvaliacaoToken($REQ_DW_id_avaliacao, $REQ_DW_id_token) != 0) {
		// Monta a mensagem para o usuario
		$DW_mensagem_erro = $objDWsurvey->ERRO_mensagem;
		$bFalhou = TRUE;
	}
	//
	// Se nao deu nenhum erro ate gora...
	if($DW_mensagem_erro == "") {
		//
        ////////////////////////////////////////////////////////////////////////
        //
        // GRAVAR AS RESPOSTAS NA TABELA
        // 
        ////////////////////////////////////////////////////////////////////////
		//
		$objLink = mysqli_connect($db_host, $db_user, $db_pass);
		//
		if(!$objLink) {
			//
			$DW_mensagem_erro = $objDWsurvey->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
			$bFalhou = TRUE;
			//
		} else {
			// Se obteve sucesso na selecao do banco de dados...
			if(mysqli_select_db($objLink, $db_name)) {
				//
				// Remove as respostas existentes
				//
				// Monta o DELETE
				$sSQL  = " DELETE FROM tbdw_af_token_item WHERE";
				$sSQL .= sprintf(" id_token = %d", 	mysqli_escape_string($objLink, $REQ_DW_id_token));
				$sSQL .= sprintf(" AND id_item_formulario = %d", mysqli_escape_string($objLink, $REQ_DW_id_item_formulario));
				//
				// Executa a consulta ao banco de dados
				$objResult = mysqli_query($objLink, $sSQL);
				//
				// Monta o request para o codigo do item
				$sReqOpcaoItem = "hdnOpcaoItem";
				// Extrai o valor
				$sOpcaoItem = $_POST[$sReqOpcaoItem];
				// Monta o request para o item
				$sReqItem = "hdnItem";
				// Extrai o valor
				$sItem = $_POST[$sReqItem];
				// Monta o request para a versao do item
				$sReqVersao = "hdnVersao";
				// Extrai o valor
				$sVersao = $_POST[$sReqVersao];
				// Monta o comando SQL
				$sSQL = "INSERT";
				$sSQL .= " tbdw_af_token_item";
				$sSQL .= " (";
				$sSQL .= " id_item_formulario, ds_resposta, id_token,";
				$sSQL .= " dt_resposta, nr_posicao,";
				$sSQL .= " cd_opcao_item, cd_item, id_versao";
				$sSQL .= " )";
				$sSQL .= " VALUES";
				$sSQL .= " (";
				$sSQL .= sprintf("%d", mysqli_escape_string($objLink, $REQ_DW_id_item_formulario));
				$sSQL .= sprintf(", '%s'", mysqli_escape_string($objLink, $REQ_DW_ds_resposta));
				$sSQL .= sprintf(", %d", mysqli_escape_string($objLink, $REQ_DW_id_token));
				$sSQL .= ", NOW()";
				$sSQL .= sprintf(", %d", mysqli_escape_string($objLink, $REQ_DW_nr_posicao));
				//
				if($sOpcaoItem == "") {
					$sSQL .= ", NULL";
				} else {
					$sSQL .= sprintf(", %d", mysqli_escape_string($objLink, $sOpcaoItem));
				}
				$sSQL .= sprintf(", %d", mysqli_escape_string($objLink, $sItem));
				$sSQL .= sprintf(", 's'", mysqli_escape_string($objLink, $sVersao));
				$sSQL .= " )";
				//
				// Executa a consulta ao banco de dados
				$objResult = mysqli_query($objLink, $sSQL);
				//
				// Atualiza o ultimo acesso do usuario ao questionario
				$sSQL = "UPDATE tbdw_aval_form_token";
				$sSQL .= " SET dt_ult_acesso = NOW()";
				// Se eh a resposta da primeira pergunta
				if ( $REQ_DW_id_item_formulario == $objDWsurvey->id_primeira_pergunta ){
					$sSQL .= ", dt_inicio = NOW()";
				}
				$sSQL .= " WHERE";
				$sSQL .= sprintf(" id_avaliacao = %d", mysqli_escape_string($objLink, $REQ_DW_id_avaliacao));
				$sSQL .= sprintf(" AND id_token = %d", mysqli_escape_string($objLink, $REQ_DW_id_token));
				// Executa a consulta ao banco de dados
				$objResult = mysqli_query($objLink, $sSQL);
				// Fecha a conexao com o banco de dados
				mysqli_close($objLink);
				//
				// Atualiza o objeto: objDWsurvey
				unset($objDWsurvey);
				$objDWsurvey = new DEFWEBsurvey($REQ_DW_id_avaliacao, $REQ_DW_id_token);
				//
			}
		}
	}
}
//
if (!$bFalhou) {
	if(($REQ_DW_navegar_anterior)&&($REQ_DW_id_anterior)) {
		//
		$REQ_DW_id_item_formulario = $REQ_DW_id_anterior;
	}
	if(($REQ_DW_navegar_proxima)&&($REQ_DW_id_proxima)) {
		//
		$REQ_DW_id_item_formulario = $REQ_DW_id_proxima;
	}
	if(($REQ_DW_navegar_primeira)&&($REQ_DW_id_primeira)) {
		//
		$REQ_DW_id_item_formulario = $REQ_DW_id_primeira;
	}
}
//
//
// Se veio o id da pergunta...
if($REQ_DW_id_item_formulario) {
	// Obter a pergunta informada
	$objPergunta = $objDWsurvey->colPerguntas[$REQ_DW_id_item_formulario];
	//
} else {
	// Obter a proxima pergunta nao respondida
	$objPergunta = $objDWsurvey->colPerguntas[$objDWsurvey->id_proxima_pergunta];
	//
}
?>