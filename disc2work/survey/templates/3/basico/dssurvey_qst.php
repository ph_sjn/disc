				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
                		<?php 
                		if ( ($REQ_DW_etapa==3)&&($REQ_DW_remainder_time != "") ) {
                		?>
                		<div class="pull-right">
                			<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                			<?php echo("Tempo restante " . $REQ_DW_remainder_time); ?>
                		</div>
                		<?php 
                		}
                		?>						
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
						<h4>
								<!--Entrevistado:-->
								<?php 
									echo($objDWsurvey->ds_nome_token);
									if($objDWsurvey->ds_unidade != "") {
										//
										echo(" (" . $objDWsurvey->ds_unidade . ")");
									}
								?>
						</h4>
					</div>
					<div class="panel-body">
						<div class="content_form">

							<p class="">
								<?php 
								//echo("Pergunta " . $objPergunta->nr_pergunta . " de " . $objDWsurvey->qt_total . " ");
								//
								if($objDWsurvey->qt_nao_respondidas > 0) {
									if($objDWsurvey->qt_nao_respondidas > 1) {
										$sPlural = "s";
									} else {
										$sPlural = "";
									}
									//echo("<small> (Falta responder ".$objDWsurvey->qt_nao_respondidas." pergunta" . $sPlural . ", realizado " . number_format($objDWsurvey->pc_respondidas, 0, ",", ".") . "%)</small>");
								} else {
									//echo("<small> (Todas as perguntas j� foram respondidas)</small>");
								}
								?>
							</p>
							<p>
								Escolha o adjetivo que <strong>MAIS</strong> e o que  <strong>MENOS</strong> tem a ver com voc�.
								<strong>
									<?php
									//echo($objPergunta->ds_agrupamento);
									// Se o nome do agrupamento for diferente do nome do tema...
									//if(strtolower($objPergunta->ds_agrupamento) != strtolower($objPergunta->ds_tema)) {
										// Exibe o tema junto do agrupamento
										//echo(" - " . $objPergunta->ds_tema);
									//}
									?>
								</strong>
							</p>
                    <?php
                    //
                    $DW_id_item_formulario = $objPergunta->id_item_formulario;
                    //
                    if($objPergunta->dv_tipo_opcoes != "C") {
                        
                        $colRespostas = null;
                        
                        // Se a pergunta ja foi respondida...
                        if($objPergunta->flag_respondida) {
                            // Obter as respostas 
                            $colRespostas = $objDWsurvey->obterRespostas($objDWsurvey->id_token, $objPergunta->cd_item);
                        }
                   
                        // Obter as opcoes do item
                        //
                        $objLink = mysqli_connect($db_host, $db_user, $db_pass);
                        //
                        if(!$objLink) {
                            //
                            //echo("Falhou na conexao com o banco de dados!");
                            $bFalhou_opcoes = TRUE;
                            //
                        } else {
                            // Se obteve sucesso na selecao do banco de dados...
                            if(mysqli_select_db($objLink, $db_name)) {

                                // Obtem a proxima opcao da resposta...

                                $sSQL = "SELECT";
                                $sSQL .= " cd_opcao_item, ds_opcao_item, vl_opcao_item";
                                $sSQL .= " FROM tbdw_opcao_item ";
                                $sSQL .= " WHERE cd_agrupamento = ".$objPergunta->cd_agrupamento;
                                $sSQL .= " AND cd_tema = ".$objPergunta->cd_tema;
                                $sSQL .= " AND cd_item = ".$objPergunta->cd_item;

                                // Executa a consulta ao banco de dados
                                $objResult = mysqli_query($objLink, $sSQL);
                                // Se falhou...
                                if(!$objResult) {
                                    //
                                    //echo("Falhou na consulta ao banco de dados!");
                                    $bFalhou_opcoes = TRUE;
                                    $objPergunta->dv_tipo_opcoes = "C";
                                    //
                                // Senao...
                                } else {
                                    // Obtem o resultado da consulta
                                    $rsDados = mysqli_fetch_assoc($objResult);
                                    
                                    // Se o result set estiver vazio...
                                    if(!$rsDados) {
                                        //
                                        //echo("Dados n?encontrados no banco de dados!");
                                        $bFalhou_opcoes = TRUE;
                                        $objPergunta->dv_tipo_opcoes = "C";
                                        
                                    // Senao, obteve o result set...
                                    } else {
                                        
                                        if($objPergunta->dv_tipo_opcoes == "X") {
                                        	//
                                        ?>
                                        <div>
                                        	<table class="table table-condensed table-bordered">
                                        		<tr class="info">
                                        			<th><strong><?php echo($objPergunta->ds_item . " / ". $objDWsurvey->qt_total); ?></strong></th>
                                        			<th class="text-center" style="width:14%;"><strong>Mais</strong></th>
                                        			<th class="text-center" style="width:14%;"><strong>Menos</strong></th>
                                        		</tr>
                                        <?php
                                        }
                                        ?>
                                        
                                        <?php
                                        while ($rsDados) {
                                            $DW_OPCAO_cd_opcao_item = $rsDados["cd_opcao_item"];
                                            $DW_OPCAO_ds_opcao_item = $rsDados["ds_opcao_item"];
                                            $DW_OPCAO_vl_opcao_item = $rsDados["vl_opcao_item"];
                                            //
                                            if ($objPergunta->dv_tipo_opcoes == "X") {
                                                // DISC
                                                    //
                                                    $tempChaves = array_keys($colRespostas);
                                                    $tempChave = $tempChaves[0];
                                                    $tempRespostaDISC = $colRespostas[$tempChave]["ds_resposta"];
                                                    //
                                                    $aRespostaDISC = explode("|", $tempRespostaDISC);
                                                    //
                                                    if(count($aRespostaDISC) == 2) {
                                                        //
                                                        $tempResposta_p = $aRespostaDISC[0];
                                                        $tempResposta_m = $aRespostaDISC[1];
                                                    } else {
                                                        $tempResposta_p = "";
                                                        $tempResposta_m = "";
                                                    }
                                                    //
                                                    $tempChecked_p = "";
                                                    //
                                                    if($tempResposta_p == $DW_OPCAO_vl_opcao_item) {
                                                        $tempChecked_p = " CHECKED";
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo($DW_OPCAO_ds_opcao_item);?>
                                                        </td>
                                                        <td class="text-center">
                                                           <input 
                                                                style="text-align: left;"
                                                                type="radio" 
                                                                id="ds_resposta_p" name="ds_resposta_p" 
                                                                value="<?php echo($DW_OPCAO_vl_opcao_item);?>" 
                                                                <?php echo($tempChecked_p); ?>
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnOpcaoItem_p"
                                                                name="hdnOpcaoItem_p"
                                                                value="<?php echo($DW_OPCAO_cd_opcao_item);?>"
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnItem_p"
                                                                name="hdnItem_p"
                                                                value="<?php echo($objPergunta->cd_item);?>"
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnVersao_p"
                                                                name="hdnVersao_p"
                                                                value="<?php echo($objPergunta->id_versao);?>"
                                                                />
                                                        </td>
                                                        <?php
                                                        //
                                                        $tempChecked_m = "";
                                                        //
                                                        if($tempResposta_m == $DW_OPCAO_vl_opcao_item) {
                                                            $tempChecked_m = " CHECKED";
                                                        }
                                                        ?>
                                                        <td class="text-center">
                                                           <input 
                                                                style="text-align: left;"
                                                                type="radio" 
                                                                id="ds_resposta_m" name="ds_resposta_m" 
                                                                value="<?php echo($DW_OPCAO_vl_opcao_item);?>" 
                                                                <?php echo($tempChecked_m); ?>
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnOpcaoItem_m"
                                                                name="hdnOpcaoItem_m"
                                                                value="<?php echo($DW_OPCAO_cd_opcao_item);?>"
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnItem_m"
                                                                name="hdnItem_m"
                                                                value="<?php echo($objPergunta->cd_item);?>"
                                                                />
                                                            <input 
                                                                type="hidden" 
                                                                id="hdnVersao_m"
                                                                name="hdnVersao_m"
                                                                value="<?php echo($objPergunta->id_versao);?>"
                                                                />
                                                        </td>
                                                    </tr>
                                                    <?php
                                            }
                                            $rsDados = mysqli_fetch_assoc($objResult);
                                        }
                                        ?>
                                                <input type="hidden"
                                                id="hdnTipo" name="hdnTipo"
                                                value="<?php echo($objPergunta->dv_tipo_opcoes); ?>"
                                                />
                                                <input type="hidden"
                                                id="hdnQtdeOpcoes" name="hdnQtdeOpcoes"
                                                value="<?php echo($iOpcaoPergunta); ?>"
                                                />
                                                <input 
                                                    type="hidden" 
                                                    id="hdnOpcaoItem"
                                                    name="hdnOpcaoItem"
                                                    value="<?php echo($DW_OPCAO_cd_opcao_item);?>"
                                                    />
                                                <input 
                                                    type="hidden" 
                                                    id="hdnItem"
                                                    name="hdnItem"
                                                    value="<?php echo($objPergunta->cd_item);?>"
                                                    />
                                                <input 
                                                    type="hidden" 
                                                    id="hdnVersao"
                                                    name="hdnVersao"
                                                    value="<?php echo($objPergunta->id_versao);?>"
                                                    />
                                    	</table>
                                        <?php
                                        //
                                        if($objPergunta->dv_tipo_opcoes == "X") {
                                            //
                                            ?>
                                    	</div>
                                        <?php
                                        }
                                    }
                                    // 
                                    // Libera a memoria alocada
                                    mysqli_free_result($objResult);
                                }
                                // Fecha a conexao com o banco de dados
                                mysqli_close($objLink);
                            }
                        }
                    }
                    ?>
						</div>
					<p id="qstMsg" class="text-danger"><?php echo($DW_mensagem_erro); ?></p>						
                    <nav aria-label="...">
                        <ul id="cmdQuestion" class="pager">
                            <?php
                            if(($objPergunta)&&(($objPergunta->id_anterior)||($objPergunta->id_proxima))) {
                            ?>
                            <?php
                                // Se tiver pergunta anterior
                                if($objPergunta->id_anterior) {
                            ?>
                                <li class="previous"><a href="#" onclick="q_anterior()"><span aria-hidden="true">&larr;</span> Anterior</a>
                                <input type='hidden' name="navegarAnterior" value=""/>
                                <input type="hidden" name="hdnIdAnterior" id="hdnIdAnterior" value="<?php echo($objPergunta->id_anterior); ?>" />
                                </li>
                            <?php
                                }
                                // Se tiver proxima pergunta e ...
                                // for franqueador ou ...
                                // for franqueado e a pergunta ja foi respondida
                                if( ($objPergunta->id_proxima) && 
                                    (
                                        ($objDWsurvey->cd_publico_alvo == 1) ||
                                        ($objDWsurvey->cd_publico_alvo == 2)
                                    )
                                ) {
                            ?>
                                <li class="next disabled"><a href="#" onclick="q_proxima()">Pr�xima <span aria-hidden="true">&rarr;</span></a>
                                <input type='hidden' name="navegarProxima" value=""/>
                                <input type="hidden" name="hdnIdProxima" id="hdnIdProxima" value="<?php echo($objPergunta->id_proxima); ?>" />
                                </li>
                            <?php
                                } elseif ( (!$objPergunta->id_proxima) && 
                                    (
                                        ($objDWsurvey->cd_publico_alvo == 1) ||
                                        ($objDWsurvey->cd_publico_alvo == 2)
                                    )
								) {
                            ?>
                                <li class="next disabled"><a href="#" onclick="q_ultima()">Pr�xima <span aria-hidden="true">&rarr;</span></a>
                                <input type='hidden' name="navegarProxima" value=""/>
                                <input type="hidden" name="hdnIdProxima" id="hdnIdProxima" value="<?php echo($objPergunta->id_proxima); ?>" />
                                </li>
                            <?php
                                }
                            ?>
                            <?php
                            }
                            ?>
                        </ul>
                    </nav>
<?php 
    // Salva o item atual como item anterior
    $DW_id_item_anterior = $DW_id_item_formulario;
?>
					
						<!-- value N indica que a questao esta intacta, se for S, foi alterada pelo usuario -->
						<input type='hidden' name="questaoAlterada" value="N"/>
						<input type="hidden" id="id_item_formulario" name="id_item_formulario" value="<?php echo($DW_id_item_formulario); ?>" />
						<input type="hidden" id="id_item_anterior" name="id_item_anterior" value="<?php echo($DW_id_item_anterior); ?>" />
						<input type="hidden" id="nr_posicao" name="nr_posicao" value="<?php echo($DW_nr_posicao); ?>" />												
						
						<p>
							<a href="#" onclick="instrucoes()">
								<span class="caret"></span>
								<strong> Instru��es:</strong>
							</a>
						</p>
						<div class="panel panel-default hidden" id="pnlInstrucoes">
							<div class="panel-body">
								<ol>
									<li>Voc� responder� 24 perguntas.</li>
									<li>Cada pergunta apresentar� 4 adjetivos.</li>
									<li>Escolha dois adjetivos: um que descreva o que MAIS tem a ver com voc� e outro que MENOS tem a ver com voc�.</li>
									<li>O adjetivo escolhido como MAIS n�o pode ser igual ao escolhido como MENOS.</li>
									<li>Depois de selecionar os dois adjetivos, clique no bot�o PR�XIMA para pular para a pr�xima pergunta.</li>
									<li>Sempre que quiser mudar alguma resposta anterior, pode clicar no bot�o ANTERIOR.</li>
									<li>Depois da pergunta 24 voc� ir� finalizar o question�rio.</li>
									<li>Voc� ter� no m�ximo 10 minutos para responder todas as perguntas.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
				<script src="templates/3/basico/dssurvey_qst.js"></script>