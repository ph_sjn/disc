<?php
//
$DIR_rpt_template_basico = "templates/5/basico/";
//
function configResultDisc($disc){
	$html = "";
	for ($letter = 0; $letter < strlen($disc); $letter++) {
		if ( $letter > 0 ){
			if ( $letter == ( strlen($disc) - 1 ) ){
				$html .= " e ";
			} else {
				$html .= ", ";
			}
		}
		switch (strtoupper(substr($disc, $letter, 1))) {
			case "D":
				$html .= "Dominante";
				break;
			case "I":
				$html .= "Influente";
				break;
			case "S":
				$html .= "Est�vel";
				break;
			case "C":
				if ( $letter == 0 ){
					$html .= "Em ";
				} else {
					$html .= "em ";
				}
				$html .= "Conformidade";
				break;
		}
	}
	return $html;
}
//
//
if ($objDWsurvey->qt_nao_respondidas == 0){
	//
	$objDiscNormalizado = new DEFWEBdisc_normalizado();
	$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
	//
}
?>
				<script src="js/dssurvey_graph_min_v1.js"></script>
				<div class="panel panel-default center-block dv_rpt_abf_container" style="max-width:600px;">
					<div class="panel-heading dv_rpt_abf_panel_head">
						<div class="pull-left dv_rpt_abf_graph_disc">
            				<input type="hidden" id="grfAn<?php echo($objDWsurvey->id_avaliacao); ?>" value="<?php echo($objDiscNormalizado->serie); ?>">
            				<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="1" />
            				<div id="dvCanv">
            					<canvas id="canvAn<?php echo($objDWsurvey->id_avaliacao); ?>" height="100"></canvas>
            				</div>
						</div>
						<div class="clearfix dv_rpt_abf_panel_title">
							<h3 class="panel-title rpt-title">
								<?php //echo($objDWsurvey->ds_titulo_avaliacao); ?>
								Avalia��o da franquia ideal para investir
							</h3>
							<h2 class="panel-title rpt-user">
								<!--Entrevistado:-->
								<?php 
								echo($objDWsurvey->ds_nome_token);
								if($objDWsurvey->ds_unidade != "") {
									//
									echo(" (" . $objDWsurvey->ds_unidade . ")");
								}
								?>
							</h2>
						</div>
					</div>
					<div class="panel-body dv_rpt_abf_panel_body clearfix">
                		<?php 
						//
                    	if ($objDWsurvey->qt_nao_respondidas == 0 && $objDiscNormalizado->resultado != "") {
						?>
                        	<div class="dv_rpt_palavra">
								<h1>NATURALMENTE SOU</h1>
								<ul>
									<li>
										<?php echo(configResultDisc($objDiscNormalizado->resultado));?>
									</li>
								</ul>
							</div>
                        <?php 
							//
							// COMO VOCE SE COMPORTA
							include($DIR_rpt_template_basico . "inc_rpt_comporta_" . $objDiscNormalizado->resultado . ".html");
							//
							// FRANQUIA MAIS ADEQUADA
							include($DIR_rpt_template_basico . "inc_rpt_franquia_" . $objDiscNormalizado->resultado . ".html");
							//
                    	}
                    	?>
					</div>
					<div class="panel-footer dv_rpt_abf_panel_footer">
						<p><b>disc2work</b> � a �nica avalia��o personalizada para franquias</p>
					</div>
				</div>
            <script language='JavaScript'>
            	//
            	$( document ).ready(function() {
					montarGraficosAnalise();
            	});
				//
            </script>
<?php
//------------------------------------------------------------
// REGIAO DO INCLUDE -  FIM
//------------------------------------------------------------
?>