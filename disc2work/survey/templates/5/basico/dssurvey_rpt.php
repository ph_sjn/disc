				<script src="js/dssurvey_graph.js"></script>
				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
						<h4>
								<!--Entrevistado:-->
								<?php 
									echo($objDWsurvey->ds_nome_token);
									if($objDWsurvey->ds_unidade != "") {
										//
										echo(" (" . $objDWsurvey->ds_unidade . ")");
									}
								?>
						</h4>
					</div>
					<div class="panel-body">
                		<?php
						//
						if ($objDWsurvey->qt_nao_respondidas == 0){
							//
							$objDiscNormalizado = new DEFWEBdisc_normalizado();
							$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
							//
						}
						//
                    	if ($objDWsurvey->qt_nao_respondidas == 0 && $objDiscNormalizado->resultado != "") {
                        ?>
                        	<table class="table table-condensed">
                        	    <thead>
                        	        <tr>
                        	            <td class="text-info">
                        	                <strong>Resultado</strong><br>
                        	                <h1 class="text-info"><strong><?php echo($objDiscNormalizado->resultado);?></strong></h1>
                        	            </td>
                        	        </tr>
                        	    </thead>
                        	    <tbody>
                            		<tr>
            							<td class="text-left" style='vertical-align: middle;'>
            								<input type="hidden" id="grfAn<?php echo($objDWsurvey->id_avaliacao); ?>" value="<?php echo($objDiscNormalizado->serie); ?>">
            								<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="1" />
            								<div id="dvCanv">
            									<canvas id="canvAn<?php echo($objDWsurvey->id_avaliacao); ?>" height="300"></canvas>
            								</div>
            							</td>
                            		</tr>
                        		</tbody>
                    		</table>
							<br>
							<?php 
							// INCLUSAO DOS DADOS ESPECIFICOS
							include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $objDiscNormalizado->resultado . ".html");
							// OPCIONAL - INCLUSAO DOS DADOS COMPLEMENTARES
							if ( preg_match ("/(EQ|RD)/", $objDiscNormalizado->complemento) ){
								echo("<br>");
								include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $objDiscNormalizado->complemento . ".html");
							}
							?>
							<br>
							<?php
							// INCLUSAO DOS DADOS GERAIS
							if ( preg_match ("/(D|I|S|C)/", substr($objDiscNormalizado->resultado, 0, 1)) ){
								include($DIR_rpt_template_basico . "inc_rpt_geral_" . substr($objDiscNormalizado->resultado, 0, 1) . ".html");
							}
                    		?>
                    	<?php 
                    	}
                    	?>
					</div>
				</div>
            <script language='JavaScript'>
            	//
            	$( document ).ready(function() {
					montarGraficosAnalise();
            	});
				//
            </script>
<?php
//------------------------------------------------------------
// REGIAO DO INCLUDE -  FIM
//------------------------------------------------------------
?>