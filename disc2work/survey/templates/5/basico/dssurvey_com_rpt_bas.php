<?php
//
function configResultDisc($disc){
	$html = "";
	for ($letter = 0; $letter < strlen($disc); $letter++) {
		if ( $letter > 0 ){
			if ( $letter == ( strlen($disc) - 1 ) ){
				$html .= " e ";
			} else {
				$html .= ", ";
			}
		}
		switch (strtoupper(substr($disc, $letter, 1))) {
			case "D":
				$html .= "Dominante";
				break;
			case "I":
				$html .= "Influente";
				break;
			case "S":
				$html .= "Est�vel";
				break;
			case "C":
				if ( $letter == 0 ){
					$html .= "Em ";
				} else {
					$html .= "em ";
				}
				$html .= "Conformidade";
				break;
		}
	}
	return $html;
}
//
?>
				<script src="js/dssurvey_graph_min_v1.js"></script>
				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
						<h4>
								<!--Entrevistado:-->
								<?php 
									echo($objDWsurvey->ds_nome_token);
									if($objDWsurvey->ds_unidade != "") {
										//
										echo(" (" . $objDWsurvey->ds_unidade . ")");
									}
								?>
						</h4>
					</div>
					<div class="panel-body">
                		<?php
						//
						if ($objDWsurvey->qt_nao_respondidas == 0){
							//
							$objDiscNormalizado = new DEFWEBdisc_normalizado();
							$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
							//
						}
						//
                    	if ($objDWsurvey->qt_nao_respondidas == 0 && $objDiscNormalizado->resultado != "") {
                        ?>
                        	<?php // RESULTADO DISC: Letra(s) ?>
							<div class="dv_rpt_resultato">
								<h1>
									<?php echo(configResultDisc($objDiscNormalizado->resultado));?>
								</h1>
							</div>
							<?php // GRAFICO DISC ?>
							<div class="dv_rpt_grafico">
            					<input type="hidden" id="grfAn<?php echo($objDWsurvey->id_avaliacao); ?>" value="<?php echo($objDiscNormalizado->serie); ?>">
            					<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="1" />
            					<div id="dvCanv">
            						<canvas id="canvAn<?php echo($objDWsurvey->id_avaliacao); ?>" height="300"></canvas>
            					</div>
							</div>
							<div class="rpt_basico">
							<?php 
							// INCLUSAO DOS TEXTOS
							//include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $objDiscNormalizado->resultado . ".html");
							//
							// PALAVRA CHAVE
							include($DIR_rpt_template_basico . "inc_rpt_palavra_" . $objDiscNormalizado->resultado . ".html");
							//
							// PRINCIPAIS CARACTERISTICAS
							include($DIR_rpt_template_basico . "inc_rpt_caracteristica_" . $objDiscNormalizado->resultado . ".html");
							//
							// COMO VOCE SE COMPORTA
							include($DIR_rpt_template_basico . "inc_rpt_comporta_" . $objDiscNormalizado->resultado . ".html");
							//
							// ONDE VOC� BRILHA MAIS + ONDE VOC� PODE TROPE�AR
							include($DIR_rpt_template_basico . "inc_rpt_brilatropeca_" . $objDiscNormalizado->resultado . ".html");
							?>
							</div>
						<?php
						}
                    	?>
					</div>
				</div>
            <script language='JavaScript'>
            	//
            	$( document ).ready(function() {
					montarGraficosAnalise();
            	});
				//
            </script>
<?php
//------------------------------------------------------------
// REGIAO DO INCLUDE -  FIM
//------------------------------------------------------------
?>