<?php
//
$DIR_rpt_template_basico = "templates/5/bradesco_abf/";
//
function configResultDisc($disc){
	$html = "";
	for ($letter = 0; $letter < strlen($disc); $letter++) {
		if ( $letter > 0 ){
			if ( $letter == ( strlen($disc) - 1 ) ){
				$html .= " e ";
			} else {
				$html .= ", ";
			}
		}
		switch (strtoupper(substr($disc, $letter, 1))) {
			case "D":
				$html .= "Dominante";
				break;
			case "I":
				$html .= "Influente";
				break;
			case "S":
				$html .= "Est�vel";
				break;
			case "C":
				if ( $letter == 0 ){
					$html .= "Em ";
				} else {
					$html .= "em ";
				}
				$html .= "Conformidade";
				break;
		}
	}
	return $html;
}
//
?>
				<script src="js/dssurvey_graph.js"></script>
				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($REQ_DW_ds_titulo); ?>
							</strong>
						</h3>
					</div>
					<div class="panel-body">
                		<?php
						//
                    	if ($discPesqNormalizado->resultado != "") {
                        ?>
                        	<div class="dv_rpt_resultato">
								<h1>
									<?php echo(configResultDisc($discPesqNormalizado->resultado));?>
								</h1>
							</div>
							<div class="dv_rpt_grafico">
            					<input type="hidden" id="grfAn<?php echo($REQ_DW_id_avaliacao); ?>" value="<?php echo($discPesqSerieNormalizada); ?>">
            					<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="1" />
            					<div id="dvCanv">
            						<canvas id="canvAn<?php echo($REQ_DW_id_avaliacao); ?>" height="300"></canvas>
            					</div>
							</div>
							<?php 
							// INCLUSAO DOS DADOS ESPECIFICOS
							include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $discPesqNormalizado->resultado . ".html");
							?>
                    	<?php 
                    	}
                    	?>
					</div>
				</div>
				<script language='JavaScript'>
					//
					$( document ).ready(function() {
						montarGraficosAnalise();
					});
					//
				</script>
<?php
//------------------------------------------------------------
// REGIAO DO INCLUDE -  FIM
//------------------------------------------------------------
?>