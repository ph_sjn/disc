<?php
//
$DIR_rpt_template_basico = "templates/5/bradesco_abf/";
//
function configResultDisc($disc){
	$html = "";
	for ($letter = 0; $letter < strlen($disc); $letter++) {
		if ( $letter > 0 ){
			if ( $letter == ( strlen($disc) - 1 ) ){
				$html .= " e ";
			} else {
				$html .= ", ";
			}
		}
		switch (strtoupper(substr($disc, $letter, 1))) {
			case "D":
				$html .= "Dominante";
				break;
			case "I":
				$html .= "Influente";
				break;
			case "S":
				$html .= "Est�vel";
				break;
			case "C":
				if ( $letter == 0 ){
					$html .= "Em ";
				} else {
					$html .= "em ";
				}
				$html .= "Conformidade";
				break;
		}
	}
	return $html;
}
//
//
if ($objDWsurvey->qt_nao_respondidas == 0){
	//
	$objDiscNormalizado = new DEFWEBdisc_normalizado();
	$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
	//
}
?>
				<script src="js/dssurvey_graph_v1.js"></script>
				<div class="panel panel-default center-block dv_rpt_abf_container" style="max-width:600px;">
					<div class="panel-heading dv_rpt_abf_panel_head">
						<div class="pull-left dv_rpt_abf_graph_disc">
            				<input type="hidden" id="grfAn<?php echo($objDWsurvey->id_avaliacao); ?>" value="<?php echo($objDiscNormalizado->serie); ?>">
            				<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="1" />
            				<div id="dvCanv">
            					<canvas id="canvAn<?php echo($objDWsurvey->id_avaliacao); ?>" height="100"></canvas>
            				</div>
						</div>
						<div class="clearfix dv_rpt_abf_panel_title">
							<h3 class="panel-title">
								<strong>
								<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
								</strong>
							</h3>
							<h4 class="panel-title">
								<!--Entrevistado:-->
								<?php 
								echo($objDWsurvey->ds_nome_token);
								if($objDWsurvey->ds_unidade != "") {
									//
									echo(" (" . $objDWsurvey->ds_unidade . ")");
								}
								?>
							</h4>
						</div>
					</div>
					<div class="panel-body dv_rpt_abf_panel_body clearfix">
                		<?php
						//
                    	if ($objDWsurvey->qt_nao_respondidas == 0 && $objDiscNormalizado->resultado != "") {
                        ?>
							<?php 
							// INCLUSAO DOS DADOS ESPECIFICOS
							include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $objDiscNormalizado->resultado . ".html");
							// OPCIONAL - INCLUSAO DOS DADOS COMPLEMENTARES
							/*
							if ( preg_match ("/(EQ|RD)/", $objDiscNormalizado->complemento) ){
								echo("<br>");
								include($DIR_rpt_template_basico . "inc_rpt_especifico_" . $objDiscNormalizado->complemento . ".html");
							}
							*/
							?>
							<!--
							<br>
							<hr>
							-->
							<?php
							// INCLUSAO DOS DADOS GERAIS
							//if ( preg_match ("/(D|I|S|C)/", substr($objDiscNormalizado->resultado, 0, 1)) ){
							//	include($DIR_rpt_template_basico . "inc_rpt_geral_" . substr($objDiscNormalizado->resultado, 0, 1) . ".html");
							//}
                    		?>
                    	<?php 
                    	}
                    	?>
					</div>
				</div>
            <script language='JavaScript'>
            	//
            	$( document ).ready(function() {
					montarGraficosAnalise();
            	});
				//
            </script>
<?php
//------------------------------------------------------------
// REGIAO DO INCLUDE -  FIM
//------------------------------------------------------------
?>