//
// VALIDA NOME
function validaNome(){
	var dsNome = $('input[id=dsNome]');
	if (!dsNome[0].checkValidity()) {
		if (dsNome[0].validity.valueMissing) {
			$('#msg_dsNome').html("O Nome deve ser informado!");
			return false;
		}
	} else {
		$('#msg_dsNome').empty();
		$('#msg_form').empty();
		return true;
	}	
}
//
$('input[id=dsNome]').blur(function() {
	validaNome();
});
//
//
// VALIDA EMAIL
function validaEmail(){
	var dsEmail	= $('input[id=dsEmail]');
	if (!dsEmail[0].checkValidity()){
		if (dsEmail[0].validity.valueMissing) {
			$('#msg_dsEmail').html("O Email deve ser informado!");
			return false;
		} else if (!dsEmail[0].validity.valid){
			$('#msg_dsEmail').html("Email inv�lido!");
			return false;
		}
	} else {
		$('#msg_dsEmail').empty();
		$('#msg_form').empty();
		return true;
	}	
}
//
$('input[id=dsEmail]').blur(function() {
	validaEmail();
});
//
//
// CHECK SELECT OPTION SEXO
function validaSexo(){
	var dvSexo = $('input[name=dvSexo]');
	if (dvSexo.filter(':checked').val() == undefined){
		$('#msg_dvSexo').html("O Sexo deve ser informado!");
		return false;
	} else {
		$('#msg_dvSexo').empty();
		$('#msg_form').empty();
		return true;
	}
}
//
$('input[id=dvSexo1]').click(function() {
	validaSexo();
});
$('input[id=dvSexo2]').click(function() {
	validaSexo();
});
//
//
// CHECK SELECT DATA DE NASCIMENTO
function validaDtNascimento(){
	var dtNasc_dia = $('select[id=dt_dia]');
	var dtNasc_mes = $('select[id=dt_mes]');
	var dtNasc_ano = $('select[id=dt_ano]');
	//
	if (dtNasc_dia.val() == null && 
		dtNasc_mes.val() == null && 
		dtNasc_ano.val() == null){
		//
		$('#msg_dtNascimento').html("A Data de Nascimento deve ser informada!");
		return false;
		//
	} else if (dtNasc_dia.val() != null && 
			   dtNasc_mes.val() != null && 
	           dtNasc_ano.val() != null){
		//
		var d = parseInt(dtNasc_dia.val(), 10);
		var m = parseInt(dtNasc_mes.val(), 10);
		var y = parseInt(dtNasc_ano.val(), 10);
		//
		var date = new Date(y,m-1,d);
		if (date.getFullYear() != y || date.getMonth() + 1 != m || date.getDate() != d) {
			$('#msg_dtNascimento').html("Data de Nascimento inv�lida!");
			return false;
		} else {
			//
			var currentDate = new Date();
			var age = currentDate.getFullYear() - date.getFullYear();
			if ( currentDate.getMonth() < date.getMonth() ){
				age--;
			} else if ( currentDate.getMonth() == date.getMonth() &&
 			            currentDate.getDate() < date.getDate() ){
				age--;
			}
			/*
			if ( age < 10 ){
				dtNasc_dia.focus();
				$('#msg_dtNascimento').html("Idade inferior ao permitido!");
				return false;
			} else {
			*/
			$('#msg_dtNascimento').empty();
			$('#msg_form').empty();
			return true;
			//}
		}
	}
}
//
$('select[id=dt_dia]').change(function() {
	validaDtNascimento();
});
$('select[id=dt_mes]').change(function() {
	validaDtNascimento();
});
$('select[id=dt_ano]').change(function() {
	validaDtNascimento();
});
//
//
// VALIDA CEP
function validaCEP(){
	var nrCep = $('input[id=nrCep]');
	if ( /^[0-9]{8}$/.test(nrCep.val()) ){
		$('#msg_nrCep').empty();
		$('#msg_form').empty();
		return true;		
	} else {
		$('#msg_nrCep').html("CEP inv�lido!");
		return false;
	}
}
//
$('input[id=nrCep]').blur(function() {
	validaCEP();
});
//
//
$( document ).ready(function() {
	//
	var btnEnviar = $("#btnEnviar");
	//
	btnEnviar.bind('click', function (event) {
		//
		// Valida o campo Nome
		if (!validaNome()){
			$('input[id=dsNome]').focus();
			event.originalEvent.currentTarget.href = "#dsNome";
			return;
		}
		//
		// Valida o campo Email
		else if (!validaEmail()){
			$('input[id=dsEmail]').focus();
			event.originalEvent.currentTarget.href = "#dsEmail";
			return;
		}
		//
		// Valida o campo Sexo
		else if (!validaSexo()){
			$('input[id=dvSexo1]').focus();
			event.originalEvent.currentTarget.href = "#dvSexo1";
			return;
		}
		//
		// Valida data de nascimento
		else if (!validaDtNascimento()){
			$('select[id=dt_dia]').focus();
			event.originalEvent.currentTarget.href = "#dt_dia";
			return;
		/*}
		//
		// Valida o campo CEP
		
		else if (!validaCEP()){
			$('input[id=nrCep]').focus();
			return; */
		} else {
			//
			var etapa = $('input[name=etapa]');
			if ( etapa !== undefined ){
				// SEGUE PARA A ETAPA 2: INSTRUCOES
				etapa.val('2');
			}
			$('form[name="frmSurvey"]').submit();
		}
	});
});
//