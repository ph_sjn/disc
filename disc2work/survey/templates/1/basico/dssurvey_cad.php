				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
					</div>
					<div class="panel-body">
						<h3>S� precisamos de mais algumas informa��es sobre voc� antes de come�armos...</h3>
						<div class="content_form">
							<div class="form-group form-group-sm">
								<label for="dsNome" class="col-xs-12 col-sm-12 col-md-12">Nome:<span class="text-danger"> *<span></label>
								<div class="col-xs-12 col-sm-8 col-md-8">
									<input type="text" class="form-control" name="dsNome" id="dsNome" maxlength="60" placeholder="Nome" value="<?php echo($dsNome); ?>" autofocus required autocomplete="name">
								</div>
								<p id="msg_dsNome" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
							<div class="form-group form-group-sm">
								<label for="dsEmail" class="col-xs-12 col-sm-12 col-md-12">Email:<span class="text-danger"> *<span></label>
								<div class="col-xs-12 col-sm-8 col-md-8">
									<input type="email" class="form-control" name="dsEmail" id="dsEmail" maxlength="50" placeholder="Email" value="<?php echo($dsEmail); ?>" required autocomplete="email">
								</div>
								<p id="msg_dsEmail" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
							<div class="form-group form-group-sm">
								<label for="dvSexo" class="col-xs-12 col-sm-12 col-md-12">Sexo:<span class="text-danger"> *<span></label>
								<div class="col-xs-12 col-sm-8 col-md-8">
									<label class="radio-inline">
										<input type="radio" name="dvSexo" id="dvSexo1" value="M" <?php if ($dv_sexo == "M") echo("checked");?>> M
									</label>
									<label class="radio-inline">
										<input type="radio" name="dvSexo" id="dvSexo2" value="F" <?php if ($dv_sexo == "F") echo("checked");?>> F
									</label>
								</div>
								<p id="msg_dvSexo" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
							<div class="form-group form-group-sm">
								<label for="dtNascimento" class="col-xs-12 col-sm-12 col-md-12">Data de Nascimento:<span class="text-danger"> *<span></label>
								<div class="col-xs-8 col-sm-2 col-md-2">									
									<select id="dt_dia" name="dt_dia" class="form-control">
										<option value="" selected disabled>Dia</option>
										<?php  
											for ($i = 1; $i <= 31; $i++) {
												$dia = $i;
												if (strlen($dia) == 1) $dia = "0" . $dia;
												if ( $dia == $dtNasDia ) {
													echo "<option value=\"$dia\" selected>$dia</option>";
												} else {
													echo "<option value=\"$dia\">$dia</option>";
												}
											}
										?>
									</select>									
								</div>
								<div class="col-xs-8 col-sm-2 col-md-2">									
									<select id="dt_mes" name="dt_mes" class="form-control">
										<option value="" selected disabled>M�s</option>
										<?php  
											for ($i = 1; $i <= 12; $i++) {
												$mes = $i;
												if (strlen($mes) == 1) $mes = "0" . $mes;
												if ( $mes == $dtNasMes ) {
													echo "<option value=\"$mes\" selected>$mes</option>";
												} else {
													echo "<option value=\"$mes\">$mes</option>";
												}
											}
										?>
									</select>
								</div>
								<div class="col-xs-8 col-sm-2 col-md-2">
									<select id="dt_ano" name="dt_ano" class="form-control">
										<option value="" selected disabled>Ano</option>
										<?php  
											$ano_corrente = date('Y');
											for ($i = $ano_corrente; $i >= ($ano_corrente - 110); $i--) {
												if ( $i == $dtNasAno ) {
													echo "<option value=\"$i\" selected>$i</option>";
												} else {
													echo "<option value=\"$i\">$i</option>";
												}
											}
										?>
									</select>
								</div>
								<p id="msg_dtNascimento" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
							<div class="form-group form-group-sm">
								<label for="nrCep" class="col-xs-12 col-sm-12 col-md-12">CEP Residencial (s� n�meros):</label>
								<div class="col-xs-4 col-sm-3 col-md-3">
									<input type="tel" class="form-control" name="nrCep" id="nrCep" maxlength="8" placeholder="CEP" value="<?php echo($nrCep); ?>" autocomplete="postal-code">
								</div>
								<p id="msg_nrCep" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
							<div class="form-group form-group-sm">
								<label for="nrTelefone" class="col-xs-12 col-sm-12 col-md-12">Telefone:</label>
								<div class="col-xs-3 col-sm-2 col-md-2">
									<input type="tel" class="form-control" name="nrCodTel" id="nrCodTel" maxlength="2" placeholder="Cod" value="<?php echo($nrCodTel); ?>">
								</div>
								<div class="col-xs-8 col-sm-3 col-md-3">
									<input type="tel" class="form-control" name="nrTelefone" id="nrTelefone" maxlength="9" placeholder="Telefone" value="<?php echo($nrTelefone); ?>" autocomplete="phone">
								</div>
								<p id="msg_nrTelefone" class="text-danger col-xs-12 col-sm-12 col-md-12"></p>
							</div>
						</div>
						<nav aria-label="...">
							<ul class="pager">
								<li class="previous"><a id="btnEnviar" href="#">Enviar</a></li>
							</ul>
						</nav>
						<p class="text-danger">* Campos obrigat�rios.</p>
						<p id="msg_form" class="text-danger"><?php echo($DW_mensagem_erro); ?></p>
					</div>
				</div>
				<script src="templates/1/basico/dssurvey_cad.js"></script>