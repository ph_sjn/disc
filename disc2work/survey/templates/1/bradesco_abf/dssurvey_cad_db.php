<?php
//
function is_valid_DateTime($date){
    $f = DateTime::createFromFormat('Y-m-d H:i:s', $date);
    $valid = DateTime::getLastErrors();
    return ($valid['warning_count']==0 and $valid['error_count']==0);
}
//
//
//-----------------------------------------------------------
// DADOS DO CADASTRO - INICIO - ETAPA: 1
$dsNome			= "";
$dsEmail		= "";
$dv_sexo		= "";
$dtNascimento 	= "";
$dtNasDia 		= "";
$dtNasMes 		= "";
$dtNasAno 		= "";
$nrCodTel		= "";
$nrTelefone		= "";
$nrCep			= "";
//
if(isset($_POST['dsNome'])){
	$dsNome = $_POST['dsNome'];
}
if(isset($_POST['dsEmail'])){
	$dsEmail = $_POST['dsEmail'];
}
if(isset($_POST['dvSexo'])){
	$dv_sexo = $_POST['dvSexo'];
}
if(isset($_POST['dt_dia']) && isset($_POST['dt_mes']) && isset($_POST['dt_ano'])){
	$dtNascimento = $_POST['dt_ano'] . "-" . $_POST['dt_mes'] . "-" . $_POST['dt_dia'] . " 00:00:00";
}
if(isset($_POST['nrTelefone'])){
	if (isset($_POST['nrCodTel'])){
		$nrTelefone = $_POST['nrCodTel'] . $_POST['nrTelefone'];
	} else {
		$nrTelefone = $_POST['nrTelefone'];
	}
}
if(isset($_POST['nrCep'])){
	$nrCep = $_POST['nrCep'];
}
//
//
// Dados do cadastro enviado para salvar na base
if ($REQ_DW_etapa == 2) {
	//
	// Verifica os dados requeridos
	$DW_mensagem_erro = "";
	if ( empty($dsNome) ){
		$DW_mensagem_erro = "O campo nome deve ser informado.";
	} elseif ( !filter_var($dsEmail, FILTER_VALIDATE_EMAIL) ) {
		$DW_mensagem_erro = "O email informado � inv�lido.";
	} elseif ( empty($dv_sexo) ){
		$DW_mensagem_erro = "O campo sexo deve ser informado.";
	} elseif ( !is_valid_DateTime($dtNascimento) ){
		$DW_mensagem_erro = "O data de anivers�rio informada � inv�lida.";
	} elseif ( $nrCep !="" ) {
		if ( !preg_match("/[0-9]{8}/", $nrCep) ) {
			$DW_mensagem_erro = "O CEP informado � inv�lido.";
		}
	}
	//
	if ( $DW_mensagem_erro != "" ){
		//
		// Dados inconsistentes...
		// VOLTAR PARA A ETAPA INICIAL DO PROCESSO
		$REQ_DW_etapa = 1;
		//
	} else {
		//
		// Gravar na Base
		$objLink = mysqli_connect($db_host, $db_user, $db_pass);
		if(!$objLink) {
			$DW_mensagem_erro = $objDWsurvey->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
			// VOLTAR PARA A ETAPA INICIAL DO PROCESSO
			$REQ_DW_etapa = 1;
		} else {
			if(mysqli_select_db($objLink, $db_name)) {
				//
				$sSQL = "UPDATE tbdw_aval_token SET";
				$sSQL .= sprintf(" ds_nome = '%s'", 	mysqli_escape_string($objLink, $dsNome));
				$sSQL .= sprintf(", ds_email = '%s'", 	mysqli_escape_string($objLink, $dsEmail));
				$sSQL .= sprintf(", dv_sexo = '%s'", 	mysqli_escape_string($objLink, $dv_sexo));
				$sSQL .= sprintf(", dt_nascimento = '%s'", 	mysqli_escape_string($objLink, $dtNascimento));
				$sSQL .= sprintf(", nr_cep = '%s'", 	mysqli_escape_string($objLink, $nrCep));
				if ( !empty($nrTelefone) ) {
					$sSQL .= sprintf(", nr_telefone = '%s'", 	mysqli_escape_string($objLink, $nrTelefone));
				}
                $sSQL .= " WHERE";
				$sSQL .= sprintf(" id_token = %d", 	mysqli_escape_string($objLink, $REQ_DW_id_token));
                 // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
				//
				unset($objDWsurvey);
				$objDWsurvey = new DEFWEBsurvey($REQ_DW_id_avaliacao, $REQ_DW_id_token);
				//
			}
		}
		//
	}
}
//
$dsNome 	 	= $objDWsurvey->ds_nome_token;
$dsEmail 		= $objDWsurvey->ds_email_token;
$dv_sexo 	 	= $objDWsurvey->dv_sexo_token;
$dtNascimento 	= $objDWsurvey->dt_nascimento_token;
$nrTelefone 	= $objDWsurvey->nr_telefone_token;
$nrCep 			= $objDWsurvey->nr_cep_token;
//
if ( strpos($dtNascimento, "/") !== false ){
	$dtSplit	= explode("/", $dtNascimento);
	$dtNasDia	= $dtSplit[0];
	$dtNasMes	= $dtSplit[1];
	$dtNasAno	= $dtSplit[2];
}
//
if ( strlen($nrTelefone) > 9 ){
		$nrCodTel   = substr($nrTelefone, 0, 2);
		$nrTelefone = substr($nrTelefone, 2);
}
//
// DADOS DO CADASTRO - FIM - ETAPA: 1
//----------------------------------------------------------- 
?>