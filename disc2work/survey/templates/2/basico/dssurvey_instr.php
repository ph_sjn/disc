				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
					</div>
					<div class="panel-body">
						<div id="InstrPg1">
							<h3>E ai <?php echo($objDWsurvey->ds_nome_token);?>, pronto para come�ar?</h3>
							<h4>Ent�o leia esses 5 pontos important�ssimos:</h4>
							<ul>
								<li class="top_bot_pad"><strong>RESPONDA SOZINHO:</strong> esse question�rio deve ser respondido apenas por voc�, sem a influ�ncia de mais ningu�m.</li>
								<li class="top_bot_pad"><strong>RESPONDA SEM INTERRUP��ES:</strong> eventuais interrup��es podem tirar sua aten��o e impactar a qualidade da avalia��o.</li>
								<li class="top_bot_pad"><strong>N�O EXISTE MELHOR OU PIOR:</strong> o objetivo principal dessa avalia��o � descobrir onde voc� brilha para que possa brilhar ainda mais no que faz bem. Ou seja, todos os perfis tem pontos muito positivos a serem trabalhados.</li>
								<!--<li class="top_bot_pad"><strong>PENSE EM VOC� TRABALHANDO:</strong> escolha os adjetivos que melhor te descrevem no dia a dia do seu trabalho, n�o de seus relacionamentos pessoais. Se voc� n�o estiver trabalhando nesse momento, imagine como se comportaria se estivesse. Se voc� for estudante, imagine como se comportaria em um trabalho em grupo.</li>-->
								<li class="top_bot_pad"><strong>DESLIGUE OS PROBLEMAS:</strong> precisamos de voc� completamente conectado com voc� mesmo. S�o apenas 12 minutinhos, ent�o deixe todos seus problemas, liga��es, reuni�es ou conversas para depois desse precioso tempo.</li>
							</ul>
							<nav aria-label="...">
								<ul id="cmdIniciar" class="pager">
									<li class="previous"><a href="#" onclick="next_instr()">Continuar</a></li>
								</ul>
							</nav>
						</div>
						<div id="InstrPg2" class="hidden">
							<h3>Como vai ser?</h3>
							<h4><strong>Leia as instru��es abaixo, respire fundo e clique em INICIAR.</strong></h4>
							<!--
							<div class="center-block">
								<h4 class="text-center top_bot_pad"><u>INSTRU��ES DE V�O</u></h4>
							</div>
							-->
							<ol>
								<li class="top_bot_pad">Voc� responder� 24 perguntas.</li>
								<li class="top_bot_pad">Cada pergunta apresentar� 4 adjetivos.</li>
								<li class="top_bot_pad">Escolha dois adjetivos: um que descreva o que MAIS tem a ver com voc� e outro que MENOS tem a ver com voc�.</li>
								<li class="top_bot_pad">O adjetivo escolhido como MAIS n�o pode ser igual ao escolhido como MENOS.</li>
								<li class="top_bot_pad">Depois de selecionar os dois adjetivos, clique no bot�o PR�XIMA para pular para a pr�xima pergunta.</li>
								<li class="top_bot_pad">Sempre que quiser mudar alguma resposta anterior, pode clicar no bot�o ANTERIOR.</li>
								<li class="top_bot_pad">Depois da pergunta 24 voc� ir� finalizar o question�rio.</li>
								<li class="top_bot_pad">Voc� ter� no m�ximo 12 minutos para responder todas as perguntas.</li>
							</ol>
							<nav aria-label="...">
								<ul id="cmdIniciar" class="pager">
									<li class="previous"><a href="#" onclick="ini_quest()">Iniciar question�rio</a></li>
								</ul>
							</nav>
						</div>
						<p><small><?php echo($objDWsurvey->periodo_aplicacao); ?></small></p>
					</div>
				</div>
			<script>
        //
				function ini_quest(){
					var etapa = $('input[name=etapa]');
					if ( etapa !== undefined ){
						// SEGUE PARA A ETAPA 3: QUESTIONARIO
						etapa.val('3');
					}
					$('form[name="frmSurvey"]').submit();
				}
				// EXIBIR PROXIMA INSTRUCAO
				function next_instr(){
					$("#InstrPg1").fadeToggle(function() {
    					$("#InstrPg2").removeClass("hidden");
  				});
				}
			</script>