<?php
// Redireciona caso nao receba os parametros obrigatorios
if (!isset($_POST["id_h"])){
	header("Location: ../site");
	die();
}
// Parametros obrigatorios informados
$REQ_DW_id_h = $_POST["id_h"];
//
// Se vieram os parametros obrigatorios...
if(!$REQ_DW_id_h) {
	header("Location: ../site");
	die();
}
//
$REQ_DW_req_print = $_REQUEST["rpt"];
//
//$CONFIGSURVEY_DISCX = "/home/users/__discx/";
$CONFIGSURVEY_DISCX = "D:/Projetos/Source/disc/__discx/";
//$CONFIGSURVEY_DISC2WORKCLIENT = "/home/users/disc2work/client/";
$CONFIGSURVEY_DISC2WORKCLIENT = "D:/Projetos/Source/disc/disc2work/client";
//
include_once($CONFIGSURVEY_DISCX . "inc/libDiscX.php");
include_once($CONFIGSURVEY_DISC2WORKCLIENT . "lib/mail/rp_mail_disc.inc.php");
//
//
$objDWC = new clsDWCmaster();
$objTokenTemp = $objDWC->obterTokenPeloHash($REQ_DW_id_h);
//
// VALIDACAO DO TOKEN
if(!is_null($objTokenTemp)) {
    //
    // Se for ortoestica com avaliacao que era DISC, ignorar a checagem do hash...
    if(!($objTokenTemp->IdCliente == 11 && $objTokenTemp->IdAvaliacao == 18)) {
        //
        $sHashTemp = $objDWC->gerarHashToken($objTokenTemp->IdCliente, $objTokenTemp->IdAvaliacao, $objTokenTemp->Id);
        //
        if($sHashTemp != $REQ_DW_id_h) {
            
            // TO DO: HASH INVALIDO
            header("Location: ../site");
            die();
        }
    }
    //
}
//
$REQ_DW_id_avaliacao = $objTokenTemp->IdAvaliacao;
$REQ_DW_id_token 	 = $objTokenTemp->Id;
//
$objDWsurvey = null;
$objDWsurvey = new DEFWEBsurvey($REQ_DW_id_avaliacao, $REQ_DW_id_token);
//
if($objDWsurvey->ERRO != 0) {
	//
	$DW_mensagem_erro = $objDWsurvey->ERRO_mensagem;
	echo("<pre>" . $DW_mensagem_erro . "</pre>");
	die();
	//
} else {
	//
	// Indicador para exibir a janela modal
	$REQ_DW_showModal = false;
	//
	// Conteudo da janela modal
	$REQ_DW_msg_modal = "";
	//
	// Alterar o campo fl_concluido para 1 (respondido)
	$REQ_DW_concluir  = false;
	//
	// Indicador, quando igual a true, de que o tempo de resposta do questionaro expirou
	$REQ_DW_time_qst_expirou = $_POST["timeQst"];
	if (!isset($REQ_DW_time_qst_expirou)){
		$REQ_DW_time_qst_expirou = false;
	}
	//
	//
	$REQ_DW_etapa = $_POST["etapa"];
	if (!isset($REQ_DW_etapa) || empty($REQ_DW_etapa)){
		// ETAPA INICIAL DO PROCESSO
		$REQ_DW_etapa = 1;
		//
	}
	//
	if ( $REQ_DW_etapa == 1 && $objDWsurvey->pgcad_cd_tipo == 0 ) {
			// Se a pagina do Cadastro for tipo 0 (ND) ... pular esta etapa e ir para a seguinte
			$REQ_DW_etapa = 2;
	}
	//
	$REQ_DW_navegar_proxima = $_POST["navegarProxima"];
	//
	// SAO OS TEMPLATES COM OS TEXTOS DA ANALISE DO RESULTADO APRESENTADOS NO RELATORIO
	$DIR_rpt_template_basico = "templates/5/basico/";
	//
	//
	//-----------------------------------------------------------------------------------------------------
	// IMAGEM DO LOGO
	//-----------------------------------------------------------------------------------------------------
	if ( !empty($objDWsurvey->IMGLogoID) ){
		//
		$type = "png";
		if ( !empty($objDWsurvey->IMGLogoName) ){
			if ( strpos($objDWsurvey->IMGLogoName, ".") !== false ){
				$type = substr($objDWsurvey->IMGLogoName, strpos($objDWsurvey->IMGLogoName, ".")+1);
			}
		}
		//
		$IMGLogo = docGetThumb($upload_dir.$objDWsurvey->IMGLogoID, 0, 0, $type, 80);
		//
	} else {
		//
		if ( $REQ_DW_id_avaliacao == 7 ) {
			$imagedata = file_get_contents("img/logo.png");
		} else {
			$imagedata = file_get_contents("img/Topo_Basico.png");
		}
		$base64 = base64_encode($imagedata);
		$IMGLogo = "data:image/jpeg;base64,$base64";
	}
	//-----------------------------------------------------------------------------------------------------
	//
	// Questionario ja respondido
	if ( ($objDWsurvey->qt_nao_respondidas == 0) && 
		 ($REQ_DW_etapa == 1 || $REQ_DW_etapa == 2) ){
		//
		if ( (!$REQ_DW_concluir) && 
			 ($objDWsurvey->fl_concluido == 0) ){
			// LIGAR A FLAG PARA CONCLUSAO DO QUESTIONARIO
			$REQ_DW_concluir = true;
		}
		// ETAPA FINAL - RELATORIO
		$REQ_DW_etapa = 5;
	}
	//
	// LIGAR A FLAG PARA CONCLUSAO DO QUESTIONARIO
	if ( ($REQ_DW_navegar_proxima == "Concluir") && 
	     ($REQ_DW_etapa == 5) ){
		$REQ_DW_concluir = true;
	}
	//
	// CONCLUIR O QUESTIONARIO
	if ( $REQ_DW_concluir ) {
		//
		$objDiscNorm = new DEFWEBdisc_normalizado();
		$objDiscNorm->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
		//
		// Mudar o campo fl_concluido para 1
		$objLink = mysqli_connect($db_host, $db_user, $db_pass);
		if(!$objLink) {
			//
			$DW_mensagem_erro = $objDWsurvey->obterMensagem("MSG_ERRO_FALHACONEXAOBD");
			//
		} else {
			// Se obteve sucesso na selecao do banco de dados...
			if(mysqli_select_db($objLink, $db_name)) {
				//
				$sSQL  = " UPDATE tbdw_aval_form_token SET";
				$sSQL .= " fl_concluido = 1";
				$sSQL .= ", dt_termino = NOW()";
				$sSQL .= sprintf(", ds_result_fator_disc = '%s'", mysqli_escape_string($objLink, $objDiscNorm->resultado));
				$sSQL .= " WHERE";
				$sSQL .= sprintf(" id_token = %d", 	mysqli_escape_string($objLink, $REQ_DW_id_token));
				$sSQL .= sprintf(" AND id_avaliacao = %d", 	mysqli_escape_string($objLink, $REQ_DW_id_avaliacao));
				//
				// Executa a consulta ao banco de dados
				$objResult = mysqli_query($objLink, $sSQL);
				// Fecha a conexao com o banco de dados
				mysqli_close($objLink);
				//
			}
                        //////////
                        //
                        // E-MAIL AUTOMATICO DE CONCLUSAO
                        //
                        //////////

//                        if ($REQ_DW_id_token == 6806) {
//                            //
//                            echo("<hr>");
//                            echo("<pre>");
//                            print_r($objDWsurvey);
//                            echo("</pre>");
//                            echo("<hr>");
//                        }

                        // Se o indicador de envio automatico de e-mail na conclusao estiver ligado...
                        if($objDWsurvey->fl_enviar_email_conclusao == 1) {

                            // Atribuir modelo de conclusao de questionario
                            $sEmailAssunto = html_entity_decode($objDWsurvey->ds_email_subj_conclusao);
                            $sEmailModeloCorpo = $objDWsurvey->ds_email_body_conclusao;
                            //
                            if ( (!is_null($sEmailAssunto)) && $sEmailAssunto != "" && (!is_null($sEmailModeloCorpo)) && $sEmailModeloCorpo != "") {
                                
//                        if ($REQ_DW_id_token == 6806) {
//                            //
//                            echo("<hr>");
//                            echo("objTokenTemp<pre>");
//                            print_r($objTokenTemp);
//                            echo("</pre>");
//                            echo("<hr>");
//                        }
                                // Monta o link para a pesquisa
                                //$sLink = "http://aws.franquiaextranet.com.br/discmaster/discx/";
								//$CONFIGSURVEY_DISC2WORK = "http://www.disc2work.com.br/";
								$CONFIGSURVEY_DISC2WORK = "http://localhost/disc/disc2work/";                                
								//$sLink = "http://www.disc2work.com.br/survey/";
								$sLink = $CONFIGSURVEY_DISC2WORK . "survey/";
                                $sLink .= "dssurvey_redir.php";
                                $sLink .= "?id_h=" . $objTokenTemp->Hash;
                                //$sLink .= "?id_avaliacao=" . $REQ_filtroAv;
                                //$sLink .= "&id_token=" . $sToken;

                                //
                                // Monta o corpo do e-mail
                                //
                                // Substitui as macros
                                $sEmailCorpo = str_replace("{NOME}", $objTokenTemp->Nome, $sEmailModeloCorpo);
                                $sEmailCorpo = str_replace("{UNIDADE}", $objTokenTemp->Unidade, $sEmailCorpo);
                                $sEmailCorpo = str_replace("{LINK}", $sLink, $sEmailCorpo);
                                // Substitui as quebras de linha do php pelas do html
                                $sEmailCorpo = str_replace("\n", "<br />", $sEmailCorpo);
                                //
                                // Envia o e-mail
                                $RET_email = rp_mail($objTokenTemp->Email, $sEmailAssunto, $sEmailCorpo);
                                //
//                        if ($REQ_DW_id_token == 6806) {
//                            //
//                            echo("<hr>");
//                            echo("RET_email:<pre>");
//                            print_r($RET_email);
//                            echo("</pre>");
//                            echo("<hr>");
//                        }                //                echo("<pre>RET_email<br>");
                //                print_r($RET_email);
                //                echo("</pre>");
                                //
                                if(!($RET_email[0] == 1)) {
                                    //
                                    $bFalhou = TRUE;
                                    //
                                    //echo("<pre>"); print_r($RET_email); echo("</pre>");
                                }
                            }

                        }

		}
	}
	//
	//-----------------------------------------------------------------------------------------------------
	// INCLUSAO DOS ARQUIVOS DE REGRAS DE NEGOCIO
	//
	$REQ_DW_cd_sit_avaliacao = $objDWsurvey->cd_sit_avaliacao;
	//
	$dt_fim_avaliacao = $objDWsurvey->dt_fim_avaliacao;
	$date1 = date("Y-m-d",strtotime($dt_fim_avaliacao));
	$date2 = date("Y-m-d");
	if($date2>$date1){
		// Prazo para responder o questionario terminou
		$REQ_DW_cd_sit_avaliacao = 0;
	}
	//
	if ( $REQ_DW_cd_sit_avaliacao != 2 ){
		// Se for diferente de 2=Nao liberada
		if ( ( $REQ_DW_etapa == 5 ) || ( $REQ_DW_etapa == 6 ) ){
			// pode direcionar para o RELATORIO se a etapa for 5 ou 6
			$REQ_DW_cd_sit_avaliacao = 1;
		}
	}
	// A situacao deve ser 1 (Liberada/Em andamento)
	if ( $REQ_DW_cd_sit_avaliacao == 1 ){
		// QUESTIONARIO || CONCLUSAO
		if ( $REQ_DW_etapa == 3 || $REQ_DW_etapa == 4) {
			if ( $objDWsurvey->pg_qst_dv == 1 ) {
				$pg_qst_dv_db = substr($objDWsurvey->pg_qst_ds, 0, count($objDWsurvey->pg_qst_ds)-5) . "_db.php";
				include($pg_qst_dv_db);
			}
		}
		//
		if ( ($REQ_DW_time_qst_expirou == true) && ($REQ_DW_etapa == 1 || $REQ_DW_etapa == 5) ) {
			// Habilita a janela modal
			$REQ_DW_showModal = true;
			//
			// Configura a mensagem
			$REQ_DW_msg_modal = "<p>Tempo esgotado!</p>";
			if ( $REQ_DW_etapa == 1 || $REQ_DW_etapa == 2 ){
				$REQ_DW_msg_modal .= "<p>Reiniciando o question�rio...</p>";
                                //$REQ_DW_msg_modal .= "(" . $REQ_DW_remainder_time . "|" . $REQ_DW_limit_time . "|" . $seconds_diff . ")";
			} else if ( $REQ_DW_etapa == 5 ){
				$REQ_DW_msg_modal .= "<p>Concluindo o question�rio...</p>";
			}
		}
		//
		// CADASTRO || INSTRUCOES
		if (( $REQ_DW_etapa == 1 || $REQ_DW_etapa == 2) && $objDWsurvey->pgcad_cd_tipo != 0 ){
			if ( $objDWsurvey->pg_cad_dv == 1 ) {
				$pg_cad_ds_db = substr($objDWsurvey->pg_cad_ds, 0, count($objDWsurvey->pg_cad_ds)-5) . "_db.php";
				include($pg_cad_ds_db);
			}
		}
	}
	//-----------------------------------------------------------------------------------------------------
}
//
?>
<!DOCTYPE html>
<html>
    <head>
		<? // EXIBE O NOME DA EXTRANET ESPECIFICADO NO ARQUIVO DE IDIOMAS, NA PASTA /LANG ?>
		<title>disc2work - <?= $objDWsurvey->ds_titulo_avaliacao ?></title>

		<?// FAVICON PARA PC ?>
		<link rel="shortcut icon" href="img-icons/favicon.ico" />

		<?//FAVICON PARA DISPOSITIVOS MOVEIS?>
		<link rel="apple-touch-icon" sizes="144x144" href="img-icons/touch-icon-ipad-retina.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="img-icons/touch-icon-iphone-retina.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="img-icons/touch-icon-ipad.png" />
		<link rel="apple-touch-icon" href="img-icons/touch-icon-iphone.png" />

		<?// Estilos: Bootstrap e outros ?>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/normalize.css" type="text/css" rel="stylesheet">
		<link href="css/padrao.css" type="text/css" rel="stylesheet">
		<link href="css/dvtable.css" type="text/css" rel="stylesheet">
		<link href="css/survey.css" type="text/css" rel="stylesheet">

		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
		<meta http-equiv="Cache-Control" content="no-store" />
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?// JQuery ?>
		<script src="js/jquery-3.2.0.min.js"></script>
		
		<?// JQuery UI ?>
		<link rel="stylesheet" href="css/jquery-ui.min.css">
		<script src="js/external/jquery/jquery.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		
		<?// Bootstrap - dependencia ?>
		<script src="js/bootstrap.min.js"></script>
    </head>

<?php

$HTML_COMPL_PRINT = "";
if($REQ_DW_req_print === "1") {
    $HTML_COMPL_PRINT .= "onload=\"javascript:window.print();setTimeout('window.close()', 100);\"";
}
?>

    <body <?php echo($HTML_COMPL_PRINT); ?>>

        <!-- Google Analytics inicio -->
		<!--
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-38250366-1']);
            _gaq.push(['_setDomainName', '<?=$web_host?>.franquiaextranet.com.br']);
            _gaq.push(['_trackPageview']);
            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
		-->
        <!-- Google Analytics Fim -->
		<?php
		//---------------------------------------------------------------------
		// CONTAINER
		//---------------------------------------------------------------------
		?>
		<div class="container-fluid">
			<?php 
			//---------------------------------------------------------------------
			// LOGO 
			//---------------------------------------------------------------------
			?>
			<div id="dsTopImg" class="row">
				<img src="<?php echo($IMGLogo); ?>" class="img-responsive center-block"/>
			</div>
			<?php
			//---------------------------------------------------------------------
			// CONTEUDO
			//---------------------------------------------------------------------
			?>
			<div id="dsContent" class="row">
				<?php
				// A situacao deve ser 1 (Liberada/Em andamento)
				if ( $REQ_DW_cd_sit_avaliacao == 1 ){
				?>
				<form action="dssurvey.php" method="post" name="frmSurvey" class="form-horizontal">
					<?php
						//---------------------------------------------------------------------
						// TEMPLATES
						//---------------------------------------------------------------------
						// CADASTRO
						if ( $REQ_DW_etapa == 1 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgcad_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_cad_ds);
								//
							} else {
								if ( $objDWsurvey->pgcad_dv_include == 1 ) {
									include($objDWsurvey->pgcad_ds_pagina);
								} else {
									echo $objDWsurvey->pgcad_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						// INSTRUCOES
						elseif ( $REQ_DW_etapa == 2 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgins_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_instr_ds);
								//
							} else {
								if ( $objDWsurvey->pgins_dv_include == 1 ) {
									include($objDWsurvey->pgins_ds_pagina);
								} else {
									echo $objDWsurvey->pgins_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						// QUESTIONARIO
						elseif ( $REQ_DW_etapa == 3 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgqst_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_qst_ds);
								//
							} else {
								if ( $objDWsurvey->pgqst_dv_include == 1 ) {
									include($objDWsurvey->pgqst_ds_pagina);
								} else {
									echo $objDWsurvey->pgqst_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						// CONCLUSAO
						elseif ( $REQ_DW_etapa == 4 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgcon_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_concl_ds);
								//
							} else {
								if ( $objDWsurvey->pgcon_dv_include == 1 ) {
									include($objDWsurvey->pgcon_ds_pagina);
								} else {
									echo $objDWsurvey->pgcon_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						// RELATORIO PARA O ENTREVISTADO
						elseif ( $REQ_DW_etapa == 5 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgrel_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_rpt_ds);
								//
							} else {
								if ( $objDWsurvey->pgrel_dv_include == 1 ) {
									include($objDWsurvey->pgrel_ds_pagina);
								} else {
									echo $objDWsurvey->pgrel_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						// RELATORIO PARA A ADMINSTRACAO
						elseif ( $REQ_DW_etapa == 6 ) {
                        	// MPM - 13/04/2017 - inicio
							if ( $objDWsurvey->pgadm_cd_tipo == 0 ) {
								//
								include($objDWsurvey->pg_rpt_adm_ds);
								//
							} else {
								if ( $objDWsurvey->pgadm_dv_include == 1 ) {
									include($objDWsurvey->pgadm_ds_pagina);
								} else {
									echo $objDWsurvey->pgadm_ds_pagina;
								}
							}
                        	// MPM - 13/04/2017 - fim
						}
						//---------------------------------------------------------------------
						// TEMPLATES
						//---------------------------------------------------------------------
					?>
					<?php // CAMPOS USADOS PELO DSSURVEY - GERENCIADOR DA PESQUISA ?>
					<input type="hidden" id="etapa" name="etapa" value="<?php echo($REQ_DW_etapa); ?>" />
					<input type="hidden" id="id_avaliacao" name="id_avaliacao" value="<?php echo($REQ_DW_id_avaliacao); ?>" />
					<input type="hidden" id="id_token" name="id_token" value="<?php echo($REQ_DW_id_token); ?>" />
					<input type="hidden" id="id_h" name="id_h" value="<?php echo($REQ_DW_id_h); ?>" />
					<input type="hidden" id="dw_sufixo" name="dw_sufixo" value="<?php echo($REQ_DW_sufixo); ?>" />
					<input type="hidden" id="showModal" name="showModal" value="<?php echo($REQ_DW_showModal);?>";>
					<input type="hidden" id="timeQst" name="timeQst" value="<?php echo($REQ_DW_time_qst_expirou);?>";>
					<?php // CAMPOS USADOS PELO DSSURVEY - GERENCIADOR DA PESQUISA ?>
				</form>
				<?php
					//
				} else {
				?>
				<div class="panel panel-default center-block" style="max-width:600px;">
					<div class="panel-heading">
						<h3>
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h3>
					</div>
					<div class="panel-body">
					<?php
					//
					// Situacao diferente de 1 (Nao liberada OU Concluida)
					//
					if ( $REQ_DW_cd_sit_avaliacao == 2 ) {
						//
						// QUESTIONARIO NAO LIBERADO
						$DW_mensagem_erro =  $objDWsurvey->obterMensagem("MSG_PESQUISA_NAOLIBERADA");
						$REQ_DW_etapa = 1;
						echo("<pre>" . $DW_mensagem_erro . "</pre>");
						//
					} else {
						// PESQUISA ENCERRADA
						$DW_mensagem_erro =  $objDWsurvey->obterMensagem("MSG_PESQUISA_ENCERRADA");
						$REQ_DW_etapa = 1;
						echo("<pre>" . $DW_mensagem_erro . "</pre>");
						//
					}
					//
					?>
					</div>
				</div>
				<?php
				}
				?>
			</div>
			<?php
			//---------------------------------------------------------------------
			// CONTEUDO
			//---------------------------------------------------------------------
			?>
		</div>
		<?php
		//---------------------------------------------------------------------
		// CONTAINER
		//---------------------------------------------------------------------
		?>
		<?php
		//---------------------------------------------------------------------
		// MODAL
		//---------------------------------------------------------------------
		?>
		<div id="dsModal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<strong>
							<?php echo($objDWsurvey->ds_titulo_avaliacao); ?>
							</strong>
						</h4>
					</div>
					<div class="modal-body">
						<?php echo($REQ_DW_msg_modal); ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>
		<script language="JavaScript"  type="text/javascript">
			//
			var showModal = $('input[id="showModal"]');
			$( document ).ready(function() {
				//
				if ( showModal.val() ){
					$('#dsModal').modal('show');
				}
				//
			});
		</script>		
    </body>
</html>