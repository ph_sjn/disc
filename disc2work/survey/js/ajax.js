function Ajax() {
    try {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (ex) {
            try {
                return new XMLHttpRequest();
            } catch (exc) {
                return false;
            }
        }
    }
}

function carregarProjetos() {
    //
    ajax2 = Ajax();
    var objDropDownCliente = document.getElementById("id_cliente");
    //
    url_pagina = "dsm_projetos/ajax_obterprojetos.php?idc=" + objDropDownCliente.value;
    //
    ajax2.open("GET", url_pagina, true);
    ajax2.onreadystatechange = function() {
        if (ajax2.readyState == 4) {
            if (ajax2.status == 200) {
                objDropDownProjeto = document.getElementById("id_projeto");
                objDropDownProjeto.innerHTML = ajax2.responseText;
            }
        }
    }
    ajax2.send(null);
}

function montarDashboard(modulo, id, div, img, tr, mode, sub_pasta, i) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);

    if (element.style.display == 'none') {

        if (browser.indexOf("MSIE") > 0) element.style.display = "block"; 
        else element.style.display = 'table-row';
        ico.setAttribute('src','img/open.gif');
        var sLoading = "<div style=\"text-align: center; margin: 10px;\"><img src=\"disc_config/img/dwloading.gif\"></div>";
        document.getElementById(div).innerHTML = sLoading;

        pagina = "dsm_projetos/ajax_dashboard.php?module=" + modulo + "&mode=" + mode + "&id=" + id + "&id_pasta=" + sub_pasta + "&idx=" + i;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {
                    document.getElementById(div).innerHTML = ajax2.responseText;
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}

function montarGraficosConsolidado() {
    //console.log("montarGraficosConsolidado()\n");
    //
    var vlGraficoCfg = jQuery("#hdnGraficoCfg").val();
    //
    if(vlGraficoCfg == 1) {
        //
        jQuery("input[id^='graf']").each( function() {
            var sChartId  = jQuery(this).attr("id").replace("graf","canv");
            var sLabelsId = jQuery(this).attr("id").replace("graf","grfLabels");
            var sColorsId = jQuery(this).attr("id").replace("graf","grfColors");
            //console.log("sChartId: " + sChartId + " | sLabelsId: " + sLabelsId + " | sColorsId: " + sColorsId + "\n");
            //
            var sChartData   = jQuery(this).val();
            if ( sChartData ) {
            	sChartData  = JSON.parse(sChartData);
        	}
            var sChartLabels = jQuery('#' + sLabelsId).val();
            if ( sChartLabels ) {
            	sChartLabels = JSON.parse(sChartLabels);
        	}
            var sChartColors = jQuery('#' + sColorsId).val();
            if ( sChartColors ) {
            	sChartColors = JSON.parse(sChartColors);
        	}
            //console.log("sChartData:   " + JSON.stringify(sChartData)   + "\n");
            //console.log("sChartLabels: " + JSON.stringify(sChartLabels) + "\n");
            //console.log("sChartColors: " + JSON.stringify(sChartColors) + "\n");
            //
            var ctx = document.getElementById(sChartId).getContext("2d");
            //
            var myPieChart = new Chart(ctx,{
            	type: 'pie',
				data: {
					labels: sChartLabels,
					datasets: [{
						data: sChartData, 
						backgroundColor: sChartColors,
						hoverBackgroundColor: sChartColors
					}]
				},
				options: {
					responsive: true,
					legend: {
						display: false
					}
				}
            });
        });
    }
}

function montarConsolidado(modulo, id, div, img, tr, mode) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);

    if (element.style.display == 'none') {

        pagina = "dsm_projetos/ajax_consolidado.php?module=" + modulo + "&mode=" + mode + "&id=" + id;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {
                    if (browser.indexOf("MSIE") > 0)
                        element.style.display = "block";
                    else
                        element.style.display = 'table-row';
                    ico.setAttribute('src', 'img/open.gif');
                    document.getElementById(div).innerHTML = ajax2.responseText;
                    // Chama a fun��o que desenha os gr�ficos
                    montarGraficosConsolidado();
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}

// A funcao comentada abaixo foi inserida no arquivo: dssurvey_graph.js
/*
function montarGraficosAnalise() {
    //console.log("montarGraficosAnalise()\n");
    //
    var vlGraficoCfg = jQuery("#hdnGraficoCfg").val();
    //
    if(vlGraficoCfg == 1) {
    	//
    	jQuery("input[id^='grfAn']").each( function() {
        	// Identica o canvas
        	var sChartId   = jQuery(this).attr("id").replace("grfAn","canvAn");
        	//
        	// Dados do usuario
        	var sChartData = JSON.parse(jQuery(this).val());
        	//
        	var ctx = document.getElementById(sChartId).getContext("2d");
        	//
			// Altura definida para o canvas (px)
			var hCanvas  = ctx.canvas.clientHeight;
			//
			// Margens (px)
			var mgLeft   	= 10;
			var mgTop   	= 0;
			var mgBottom 	= 0;
			//
			// Quantidade de celulas
			// S�o 5 celulas na horizontal ( D I S C )
			var nCellsX		= 5;
			//
			// Sao 13 celulas na vertical contando com a linha dos labels
			var nCellsY		= 13;
			//
			// Tamanho da celula (px^2)
			var szCell    	= (hCanvas-mgTop-mgBottom)/nCellsY;
			//
			// Calculo da largura do grafico (px)
			var wGraf    	= szCell*nCellsX;
			//
			// Calculo da altura do grafico (px) - remover a linha do label (1 celula)
			var hGraf    	= szCell*(nCellsY-1);
			//
			// Pontos iniciais
			var xIni     	= mgLeft;
			var yIni     	= mgTop;
			//
			// Largura do canvas proporcional
			ctx.canvas.width = parseInt(wGraf) + mgLeft + 2;
			//
			ctx.strokeStyle = "rgb(0,0,0)";
			//
			//---------------------------------------------------------------------
			// Valores Maximo e Minimo da escala 
			var scaleY_max = 100;
			var scaleY_min = 0;
			//
			// Calcula qtde de itens na vertical que comporta o range
			var yItens = (scaleY_max - scaleY_min);
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Labels
			ctx.fillStyle 	 = "rgb(0,0,0)";
			ctx.font 		 = 'bold 18px Arial';
			ctx.textAlign 	 = 'center';
			ctx.textBaseline = 'top';
			//
			ctx.fillText('D', xIni+(szCell*1), yIni);
			ctx.fillText('I', xIni+(szCell*2), yIni);
			ctx.fillText('S', xIni+(szCell*3), yIni);
			ctx.fillText('C', xIni+(szCell*4), yIni);
			//---------------------------------------------------------------------
			//
			// Desenhando a area do grafico
			// Ajuste do Y ( pular um espaco de uma celula )
			yIni = yIni+szCell;
			//
			//---------------------------------------------------------------------
			// Contorno do grafico
			ctx.fillStyle = "rgb(0,0,0)";
			ctx.lineWidth = 1;
			ctx.strokeRect (xIni, yIni, wGraf, hGraf);
			ctx.strokeStyle = "rgb(0,0,0)";
			//---------------------------------------------------------------------
			//
			// Controle das coordenadas
			var x, y;
			//
			//---------------------------------------------------------------------
			// Zonas extremas (ALTOS e BAIXOS)
			// Cor da area
			ctx.fillStyle = "rgba(192,192,192, 0.5)";
			//
			// Faixa ALTOS
			var ptoTop     = (((scaleY_max - 100)/yItens)*(hGraf+szCell))+(((100 - scaleY_min)/yItens)*szCell);
			// Limite Superior
			var ptoLimiteS = (((scaleY_max - 86)/yItens)*(hGraf+szCell))+(((86 - scaleY_min)/yItens)*szCell);
			//
			ctx.fillRect(xIni, yIni, wGraf, ptoLimiteS - ptoTop);
			//
			// Faixa BAIXOS
			// Limite Inferior
			var ptoLimiteI = (((scaleY_max - 15)/yItens)*(hGraf+szCell))+(((15 - scaleY_min)/yItens)*szCell);
			//
			ctx.fillRect(xIni, ptoLimiteI, wGraf, hGraf);
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Faixa EQUILIBRIO
			ctx.fillStyle = "rgba(208,208,208, 0.5)";
			// Limite Superior
			ptoLimiteS = (((scaleY_max - 58)/yItens)*(hGraf+szCell))+(((58 - scaleY_min)/yItens)*szCell);
			// Limite Inferior
			ptoLimiteI = (((scaleY_max - 43)/yItens)*(hGraf+szCell))+(((43 - scaleY_min)/yItens)*szCell);
			ctx.fillRect(xIni, ptoLimiteS, wGraf, ptoLimiteI - ptoLimiteS);
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Linhas Verticais (col)
			x = xIni;
			y = yIni;
			for(var col=1;col<nCellsX;col++){
				x += szCell;
				ctx.beginPath();
				ctx.moveTo(x,y);
				ctx.lineTo(x,hGraf+szCell);
				ctx.lineWidth = 1;
				ctx.closePath();
				ctx.stroke();
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Linhas Horizontais (row)
			x = xIni;
			y = yIni;
			for(var row=1;row<(nCellsY-1);row++){
				//
				// Calcula a posicao Y
				//
				// Linha limite valores ALTOS
				if ( row == 1 ) {
					y = (((scaleY_max - 86)/yItens)*(hGraf+szCell))+(((86 - scaleY_min)/yItens)*szCell);
				} 
				// Linha limite superior valores EQUILIBRIO
				else if ( row == 5 ){
					y = (((scaleY_max - 58)/yItens)*(hGraf+szCell))+(((58 - scaleY_min)/yItens)*szCell);
				}
				// Linha ZERO
				else if ( row == 6 ){
					y = (((scaleY_max - 51)/yItens)*(hGraf+szCell))+(((51 - scaleY_min)/yItens)*szCell);
				}
				// Linha limite inferior valores EQUILIBRIO
				else if ( row == 7 ){
					y = (((scaleY_max - 43)/yItens)*(hGraf+szCell))+(((43 - scaleY_min)/yItens)*szCell);
				}
				// Linha limite valores BAIXOS
				else if ( row == 11 ){
					y = (((scaleY_max - 15)/yItens)*(hGraf+szCell))+(((15 - scaleY_min)/yItens)*szCell);
				}
				else if ( row == 12 ){
					y = (((scaleY_max - 1)/yItens)*(hGraf+szCell))+(((1 - scaleY_min)/yItens)*szCell);
				}
				//
				// Desenha somente nas linhas abaixo
				if (row == 1 || 
				    row == 5 || 
					row == 6 || 
					row == 7 || 
					row == 11 || 
					row == 12 ){
					ctx.beginPath();
					ctx.moveTo(x,y);
					ctx.lineTo(wGraf+mgLeft,y);
					ctx.lineWidth = 1;
					ctx.closePath();
					ctx.stroke();
				}
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// dados informados do usuario
			var userData = sChartData;
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// x Axes
			var xCoor = [
				xIni+(szCell*1), 
				xIni+(szCell*2), 
				xIni+(szCell*3), 
				xIni+(szCell*4)
			];
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// y Axes
			var yCoor = [
				(((scaleY_max - userData[0])/yItens)*(hGraf+szCell))+(((userData[0] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[1])/yItens)*(hGraf+szCell))+(((userData[1] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[2])/yItens)*(hGraf+szCell))+(((userData[2] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[3])/yItens)*(hGraf+szCell))+(((userData[3] - scaleY_min)/yItens)*szCell)
			];
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Set pointRadius
			var radius = 6;
			var startAngle = 0;
			var endAngle = 2*Math.PI;
			var anticlockwise = true;
			ctx.fillStyle = "rgb(0,0,255)";
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Desenha os dados no grafico
			for(var idx=0;idx<nCellsX-1;idx++){
				// pointRadius
				ctx.beginPath();
				ctx.arc(xCoor[idx], yCoor[idx], radius, startAngle, endAngle, anticlockwise);
				ctx.fill();
				ctx.closePath();
				// line
				if ( (idx+1) < (nCellsX-1) ) {
					ctx.beginPath();
					ctx.moveTo(xCoor[idx], yCoor[idx]);
					ctx.lineTo(xCoor[idx+1], yCoor[idx+1]);
					ctx.lineWidth = 2;
					ctx.strokeStyle = "rgb(0,0,255)";
					ctx.stroke();
				}
			}
			//---------------------------------------------------------------------
    	});
	}
}
*/

function montarAnalise(modulo, id, div, img, tr, mode) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);

    if (element.style.display == 'none') {

        pagina = "dsm_projetos/ajax_matriz.php?module=" + modulo + "&mode=" + mode + "&id=" + id;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {
                    if (browser.indexOf("MSIE") > 0)
                        element.style.display = "block";
                    else
                        element.style.display = 'table-row';
                    ico.setAttribute('src', 'img/open.gif');
                    document.getElementById(div).innerHTML = ajax2.responseText;
                    // Chama a fun��o que desenha os gr�ficos
                    montarGraficosAnalise();
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}

function exibirRespostas(modulo, id, idfq, div, img, tr, mode) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);

    if (element.style.display == 'none') {

        pagina = "dsm_projetos/ajax_franqueador.php?module=" + modulo + "&mode=" + mode + "&id=" + id + "&idfq=" + idfq;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {
                    if (browser.indexOf("MSIE") > 0)
                        element.style.display = "block";
                    else
                        element.style.display = 'table-row';
                    ico.setAttribute('src', 'img/open.gif');
                    document.getElementById(div).innerHTML = ajax2.responseText;
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}

function incluirForm(PARM_id_form_franqueador, PARM_id_forms_franqueador) {
    //
    var objDropDown = document.getElementById(PARM_id_form_franqueador);
    //
    if(objDropDown.value == "") {
        alert("Selecione um formul�rio para inclus�o na lista.");
        objDropDown.focus();
    } else {
        var objLista = document.getElementById(PARM_id_forms_franqueador);
        //
        var objOption = document.createElement("option");
        var iPos = objDropDown.selectedIndex;
        objOption.text = objDropDown.options[iPos].text;
        objOption.value = objDropDown.options[iPos].value;
        objLista.add(objOption);
        objDropDown.remove(iPos);
        objDropDown.selectedIndex = 0;
    }
    //
}


function excluirForm(PARM_id_form_franqueador, PARM_id_forms_franqueador) {
    //
    var objLista = document.getElementById(PARM_id_forms_franqueador);
    //
    var iPos = objLista.selectedIndex;
    //
    if(iPos >= 0) {
        //
        var objOption = document.createElement("option");
        var objDropDown = document.getElementById(PARM_id_form_franqueador);
        //
        objOption.text = objLista.options[iPos].text;
        objOption.value = objLista.options[iPos].value;
        //
        objDropDown.add(objOption);
        objLista.remove(iPos);
        objLista.selectedIndex = -1;
    }
}

function incluirFormFranqueador() {
    //
    incluirForm("id_form_franqueador", "id_forms_franqueador");
    //
    atualizarIDs("id_forms_franqueador","ids_forms_franqueador");
}

function excluirFormFranqueador() {
    //
    excluirForm("id_form_franqueador", "id_forms_franqueador");
    //
    atualizarIDs("id_forms_franqueador","ids_forms_franqueador");
}

function incluirFormFranqueado() {
    //
    incluirForm("id_form_franqueado", "id_forms_franqueado");
    //
    atualizarIDs("id_forms_franqueado","ids_forms_franqueado");
}

function excluirFormFranqueado() {
    //
    excluirForm("id_form_franqueado", "id_forms_franqueado");
    //
    atualizarIDs("id_forms_franqueado","ids_forms_franqueado");
}

function atualizarIDs(PARM_id_forms, PARM_ids) {
    //
    var objIDsForms = document.getElementById(PARM_id_forms);
    var sIDs = '';
    var iPos = 0;
    for(iPos=0;iPos<objIDsForms.options.length;iPos++) {
        if(sIDs != "") {
            sIDs += ',';
        }
        sIDs += objIDsForms.options[iPos].value;
    }
    //
    var objHiddenIDsForms = document.getElementById(PARM_ids);
    objHiddenIDsForms.value = sIDs;
}
