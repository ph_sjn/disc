/*
	Grafico DISC - Survey
*/
function montarGraficosAnalise() {
    //console.log("montarGraficosAnalise()\n");
    //
    var vlGraficoCfg = jQuery("#hdnGraficoCfg").val();
    //
    if(vlGraficoCfg == 1) {
    	//
    	jQuery("input[id^='grfAn']").each( function() {
        	// Identica o canvas
        	var sChartId   = jQuery(this).attr("id").replace("grfAn","canvAn");
        	//
        	// Dados do usuario
        	var sChartData = JSON.parse(jQuery(this).val());
        	//
        	var ctx = document.getElementById(sChartId).getContext("2d");
        	//
			// Altura definida para o canvas (px)
			var hCanvas  = ctx.canvas.clientHeight;
			//
			// Margens (px)
			var mgLeft   	= 10;
			var mgTop   	= 0;
			var mgBottom 	= 0;
			//
			// Quantidade de celulas
			// S�o 5 celulas na horizontal ( D I S C )
			var nCellsX		= 5;
			//
			// Sao 13 celulas na vertical contando com a linha dos labels
			var nCellsY		= 13;
			//
			// Tamanho da celula (px^2)
			var szCell    	= (hCanvas-mgTop-mgBottom)/nCellsY;
			//
			// Calculo da largura do grafico (px)
			var wGraf    	= szCell*nCellsX;
			//
			// Calculo da altura do grafico (px) - remover a linha do label (1 celula)
			var hGraf    	= szCell*(nCellsY-1);
			//
			// Pontos iniciais
			var xIni     	= mgLeft;
			var yIni     	= mgTop;
			//
			// Largura do canvas proporcional
			ctx.canvas.width = parseInt(wGraf) + mgLeft + 2;
			//
			ctx.strokeStyle = "rgb(0,0,0)";
			//
			//---------------------------------------------------------------------
			// Valores Maximo e Minimo da escala 
			var scaleY_max = 100;
			var scaleY_min = 0;
			//
			// Calcula qtde de itens na vertical que comporta o range
			var yItens = (scaleY_max - scaleY_min);
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Labels
			var fontSize     = (18 * (hCanvas/300));
			if ( fontSize < 8 ) fontSize = 8;
			if ( fontSize > 22 ) fontSize = 22;
			ctx.fillStyle 	 = "rgb(0,0,0)";
			ctx.font 		 = 'bold ' + fontSize + 'px Arial';
			ctx.textAlign 	 = 'center';
			ctx.textBaseline = 'top';
			//
			yIni = yIni - 1;
			ctx.fillText('D', xIni+(szCell*1), yIni);
			ctx.fillText('I', xIni+(szCell*2), yIni);
			ctx.fillText('S', xIni+(szCell*3), yIni);
			ctx.fillText('C', xIni+(szCell*4), yIni);
			//---------------------------------------------------------------------
			//
			// Desenhando a area do grafico
			// Ajuste do Y ( pular um espaco de uma celula )
			yIni = mgTop+szCell;
			//
			//---------------------------------------------------------------------
			// Contorno do grafico
			ctx.fillStyle = "rgb(0,0,0)";
			ctx.lineWidth = 1;
			ctx.strokeRect (xIni, yIni, wGraf, hGraf);
			ctx.strokeStyle = "rgb(0,0,0)";
			//---------------------------------------------------------------------
			//
			// Controle das coordenadas
			var x, y;
			//
			//---------------------------------------------------------------------
			// Zonas extremas (ALTOS e BAIXOS)
			// Cor da area
			ctx.fillStyle = "rgba(192,192,192, 0.5)";
			//
			// Faixa ALTOS
			var ptoTop     = (((scaleY_max - 100)/yItens)*(hGraf+szCell))+(((100 - scaleY_min)/yItens)*szCell);
			// Limite Superior
			var ptoLimiteS = (((scaleY_max - 86)/yItens)*(hGraf+szCell))+(((86 - scaleY_min)/yItens)*szCell);
			//
			ctx.fillRect(xIni, yIni, wGraf, ptoLimiteS - ptoTop);
			//
			// Faixa BAIXOS
			// Limite Inferior
			var ptoLimiteI = (((scaleY_max - 15)/yItens)*(hGraf+szCell))+(((15 - scaleY_min)/yItens)*szCell);
			//
			ctx.fillRect(xIni, ptoLimiteI, wGraf, hGraf);
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Faixa EQUILIBRIO
			ctx.fillStyle = "rgba(208,208,208, 0.5)";
			// Limite Superior
			ptoLimiteS = (((scaleY_max - 58)/yItens)*(hGraf+szCell))+(((58 - scaleY_min)/yItens)*szCell);
			// Limite Inferior
			ptoLimiteI = (((scaleY_max - 43)/yItens)*(hGraf+szCell))+(((43 - scaleY_min)/yItens)*szCell);
			ctx.fillRect(xIni, ptoLimiteS, wGraf, ptoLimiteI - ptoLimiteS);
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Linhas Verticais (col)
			x = xIni;
			y = yIni;
			for(var col=1;col<nCellsX;col++){
				x += szCell;
				ctx.beginPath();
				ctx.moveTo(x,y);
				ctx.lineTo(x,hGraf+szCell);
				ctx.lineWidth = 1;
				ctx.closePath();
				ctx.stroke();
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Linhas Horizontais (row)
			x = xIni;
			y = yIni;
			for(var row=1;row<(nCellsY-1);row++){
				//
				// Calcula a posicao Y
				//
				// Linha limite valores ALTOS
				if ( row == 1 ) {
					y = (((scaleY_max - 86)/yItens)*(hGraf+szCell))+(((86 - scaleY_min)/yItens)*szCell);
				} 
				// Linha limite superior valores EQUILIBRIO
				else if ( row == 5 ){
					y = (((scaleY_max - 58)/yItens)*(hGraf+szCell))+(((58 - scaleY_min)/yItens)*szCell);
				}
				// Linha ZERO
				else if ( row == 6 ){
					y = (((scaleY_max - 51)/yItens)*(hGraf+szCell))+(((51 - scaleY_min)/yItens)*szCell);
				}
				// Linha limite inferior valores EQUILIBRIO
				else if ( row == 7 ){
					y = (((scaleY_max - 43)/yItens)*(hGraf+szCell))+(((43 - scaleY_min)/yItens)*szCell);
				}
				// Linha limite valores BAIXOS
				else if ( row == 11 ){
					y = (((scaleY_max - 15)/yItens)*(hGraf+szCell))+(((15 - scaleY_min)/yItens)*szCell);
				}
				else if ( row == 12 ){
					y = (((scaleY_max - 1)/yItens)*(hGraf+szCell))+(((1 - scaleY_min)/yItens)*szCell);
				}
				//
				// Desenha somente nas linhas abaixo
				if (row == 1 || 
				    row == 5 || 
					row == 6 || 
					row == 7 || 
					row == 11 || 
					row == 12 ){
					ctx.beginPath();
					ctx.moveTo(x,y);
					ctx.lineTo(wGraf+mgLeft,y);
					ctx.lineWidth = 1;
					ctx.closePath();
					ctx.stroke();
				}
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// dados informados do usuario
			var userData = sChartData;
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// x Axes
			var xCoor = [
				xIni+(szCell*1), 
				xIni+(szCell*2), 
				xIni+(szCell*3), 
				xIni+(szCell*4)
			];
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// y Axes
			var yCoor = [
				(((scaleY_max - userData[0])/yItens)*(hGraf+szCell))+(((userData[0] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[1])/yItens)*(hGraf+szCell))+(((userData[1] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[2])/yItens)*(hGraf+szCell))+(((userData[2] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - userData[3])/yItens)*(hGraf+szCell))+(((userData[3] - scaleY_min)/yItens)*szCell)
			];
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Set pointRadius
			var radius = (6 * (hCanvas/300));
			if ( radius < 3 ) radius = 3;
			if ( radius > 9 ) radius = 9;
			var startAngle = 0;
			var endAngle = 2*Math.PI;
			var anticlockwise = true;
			ctx.fillStyle = "rgb(0,0,255)";
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Desenha os dados no grafico
			for(var idx=0;idx<nCellsX-1;idx++){
				// pointRadius
				ctx.beginPath();
				ctx.arc(xCoor[idx], yCoor[idx], radius, startAngle, endAngle, anticlockwise);
				ctx.fill();
				ctx.closePath();
				// line
				if ( (idx+1) < (nCellsX-1) ) {
					ctx.beginPath();
					ctx.moveTo(xCoor[idx], yCoor[idx]);
					ctx.lineTo(xCoor[idx+1], yCoor[idx+1]);
					ctx.lineWidth = 2;
					ctx.strokeStyle = "rgb(0,0,255)";
					ctx.stroke();
				}
			}
			//---------------------------------------------------------------------
    	});
	}
}
//
function montarGraficosComparacao() {
    //console.log("montarGraficosComparacao()\n");
    //
    var vlGraficoCfg = jQuery("#hdnGraficoCfg").val();
    //
    if(vlGraficoCfg == 1) {
    	//
		jQuery("input[id^='grfCompar']").each( function() {
        	// Identica o canvas
        	var sChartId   = jQuery(this).attr("id").replace("grfCompar","canvCompar");
        	//
        	// Dados do usuario
        	var sChartData = JSON.parse(jQuery(this).val());
        	//
        	var ctx = document.getElementById(sChartId).getContext("2d");
        	//
			// Altura definida para o canvas (px)
			var hCanvas  = ctx.canvas.clientHeight;
			//
			// Margens (px)
			var mgLeft   	= 10;
			var mgTop   	= 0;
			var mgBottom 	= 0;
			//
			// Quantidade de celulas
			// S�o 5 celulas na horizontal ( D I S C )
			var nCellsX		= 5;
			//
			// Sao 13 celulas na vertical contando com a linha dos labels
			var nCellsY		= 13;
			//
			// Tamanho da celula (px^2)
			var szCell    	= (hCanvas-mgTop-mgBottom)/nCellsY;
			//
			// Calculo da largura do grafico (px)
			var wGraf    	= szCell*nCellsX;
			//
			// Calculo da altura do grafico (px) - remover a linha do label (1 celula)
			var hGraf    	= szCell*(nCellsY-1);
			//
			// Pontos iniciais
			var xIni     	= mgLeft;
			var yIni     	= mgTop;
			//
			// Largura do canvas proporcional
			ctx.canvas.width = parseInt(wGraf) + mgLeft + 2;
			//
			ctx.strokeStyle = "rgb(0,0,0)";
			//
			//---------------------------------------------------------------------
			// Valores Maximo e Minimo da escala 
			var scaleY_max = 100;
			var scaleY_min = 0;
			//
			// Calcula qtde de itens na vertical que comporta o range
			var yItens = (scaleY_max - scaleY_min);
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Labels
			var fontSize     = (18 * (hCanvas/300));
			if ( fontSize < 8 ) fontSize = 8;
			if ( fontSize > 22 ) fontSize = 22;
			ctx.fillStyle 	 = "rgb(0,0,0)";
			ctx.font 		 = 'bold ' + fontSize + 'px Arial';
			ctx.textAlign 	 = 'center';
			ctx.textBaseline = 'top';
			//
			yIni = yIni - 1;
			ctx.fillText('D', xIni+(szCell*1), yIni);
			ctx.fillText('I', xIni+(szCell*2), yIni);
			ctx.fillText('S', xIni+(szCell*3), yIni);
			ctx.fillText('C', xIni+(szCell*4), yIni);
			//---------------------------------------------------------------------
			//
			// Desenhando a area do grafico
			// Ajuste do Y ( pular um espaco de uma celula )
			yIni = mgTop+szCell;
			//
			//
			//---------------------------------------------------------------------
			// Contorno do grafico
			ctx.fillStyle = "rgb(0,0,0)";
			ctx.lineWidth = 1;
			ctx.strokeRect (xIni, yIni, wGraf, hGraf);
			ctx.strokeStyle = "rgb(0,0,0)";
			//---------------------------------------------------------------------
			//
			// Controle das coordenadas
			var x, y;
			//
			//---------------------------------------------------------------------
			// Linhas Verticais (col)
			x = xIni;
			y = yIni;
			for(var col=1;col<nCellsX;col++){
				x += szCell;
				ctx.beginPath();
				ctx.moveTo(x,y);
				ctx.lineTo(x,hGraf+szCell);
				ctx.lineWidth = 1;
				ctx.closePath();
				ctx.stroke();
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// Linhas Horizontais (row)
			x = xIni;
			y = yIni;
			var nY = 0;
			for(var row=1;row<(nCellsY-1);row++){
				//
				// Calcula a posicao Y
				nY += 10;
				y = (((scaleY_max - (100 - nY))/yItens)*(hGraf+szCell))+((((100 - nY) - scaleY_min)/yItens)*szCell);
				//
				ctx.beginPath();
				ctx.moveTo(x,y);
				ctx.lineTo(wGraf+mgLeft,y);
				ctx.lineWidth = 1;
				ctx.closePath();
				ctx.stroke();
			}
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// dados informados do usuario
			var userData = sChartData;
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// x Axes
			var xCoor = [
				xIni+(szCell*1), 
				xIni+(szCell*2), 
				xIni+(szCell*3), 
				xIni+(szCell*4)
			];
			//---------------------------------------------------------------------
			
			//---------------------------------------------------------------------
			// y Axes
			var yCoor = [
				(((scaleY_max - (50+userData[0]))/yItens)*(hGraf+szCell))+((((50+userData[0]) - scaleY_min)/yItens)*szCell),
				(((scaleY_max - (50+userData[1]))/yItens)*(hGraf+szCell))+((((50+userData[1]) - scaleY_min)/yItens)*szCell),
				(((scaleY_max - (50+userData[2]))/yItens)*(hGraf+szCell))+((((50+userData[2]) - scaleY_min)/yItens)*szCell),
				(((scaleY_max - (50+userData[3]))/yItens)*(hGraf+szCell))+((((50+userData[3]) - scaleY_min)/yItens)*szCell)
			];
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			//
			//---------------------------------------------------------------------
			// Desenha os dados no grafico
			var hY = 0;
			var y50 = (((scaleY_max - 50)/yItens)*(hGraf+szCell))+(((50 - scaleY_min)/yItens)*szCell);
			//
			for(var col=0;col<nCellsX-1;col++){
				// cor da barra
				if ( col == 0 ){
					ctx.fillStyle = 'rgba(255, 99, 132, 0.5)'
				}
				if ( col == 1 ){
					ctx.fillStyle = 'rgba(255, 159, 64, 0.5)'
				}
				if ( col == 2 ){
					ctx.fillStyle = 'rgba(75, 192, 192, 0.5)'
				}
				if ( col == 3 ){
					ctx.fillStyle = 'rgba(54, 162, 235, 0.5)'
				}
				// coordenada
				y  = y50;
				hY = yCoor[col] - y50;
				//
				ctx.fillRect(xCoor[col]-10, y, 20, hY);
			}
			//---------------------------------------------------------------------
    	});
	}
}