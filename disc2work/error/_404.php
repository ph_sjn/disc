<?php
$url = $_SERVER['SERVER_NAME'];
$pos = strpos($url,'.');
$nome_extranet =  substr($url,0,$pos);
?>

<html>
<head>
<style type="text/css">

body{ font-size: 10pt; font-family: Arial; color: #000000; text-decoration:none; }
a { font-size: 10pt; font-family: Arial; color: #000000; text-decoration:none; }
a:hover { color: <?=$cor1?>; text-decoration:none;}


#logo_rodape {
    font-family: "Century Schoolbook", Century,Times, serif;
    font-style: italic;
    font-weight: bold;
    color: #000099;
    font-size: 11px;
    margin-right: 5px;
}

.div_rodape_copyright {
    position: fixed; 
	bottom: 0px; 
	text-align:center; 
	vertical-align:center; 
	width:100%; 
	background:#f0f0f0;
}

 .div_rodape_copyright.sobre{
	bottom: 15px;  
	background: transparent;
}

.div_rodape_copyright a{
	color:  #000099;
	font-size:11px;

}

.div_rodape_copyright a:hover{
	color:  <?=$cor1?>;
}

#copyright{
    color: #888888;
    margin-right: 15px;
    font-size: 10px;
    padding-top: 3px;
}


input[type="button"],
input[type="submit"]{
	 background-color: red;
	 border: 0;
	 color: #ffffff;
	 height: 22px; 
}

</style>
	<? // EXIBE O NOME DA EXTRANET ESPECIFICADO NO ARQUIVO DE IDIOMAS, NA PASTA /LANG ?>
	<title>Extranet - <?= ucfirst($nome_extranet) ?></title>		
	<?//FAVICON PARA PC?>
    <link rel="shortcut icon" href="../../img-icons/favicon.ico" />
	<?//FAVICON PARA DISPOSITIVOS M�VEIS?>
	<link rel="apple-touch-icon" sizes="144x144" href="../../img-icons/touch-icon-ipad-retina.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="../../img-icons/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="../../img-icons/touch-icon-ipad.png" />
	<link rel="apple-touch-icon" href="../../img-icons/touch-icon-iphone.png" />		
	<? // INICIALIZA O CSS ESPECIFICADO NO ARQUIVO CONFIG.PHP ?>
	
	<!-- <link href="../../lib/css/padrao.php" type="text/css" rel="stylesheet" > -->
	
	<? // CHARSET PADR�O DA EXTRANET: ISO 8859-1 (LATIN1) ?>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
</head>
<body>
	<div id="div_conteiner">	
		<div >	
			<img src="http://aws.franquiaextranet.com.br/extranet/PainelControle/img/logos_clientes/logo_<?=$nome_extranet?>.png" style="padding-left:10px"><br>
		</div>
		<div style="text-align:center; padding-top:20%; font-weight:bold">
			Oops!<br><br>
			P�gina n�o encontrada! <br><br>
			<input type="button" onclick="history.back()" value="Voltar">		
		</div>		
		<div class="div_rodape_copyright" >
			<a href="http://www.rpconsultoria.com.br" id="logo_rodape" target="_blank">RPconsultoria</a>
			<b id="copyright">&copy;&nbsp;Copyright 2004-<?= date('Y') ?>. Todos os direitos reservados.</b>
		</div>
	</div>
</body>
</html>
