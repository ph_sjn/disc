<?
$acesso_administrativo = checaPermissao($module,"A",$user_access); 

$objDSXadmin = new clsDSXmasterAdmin();
?>

<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<tr>
	<td style="vertical-align:middle;">
		<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms"><span style="font-weight:bold;">CADASTRAR P�GINA</span></a>
	</td>
</tr>
<tr>
	<td style="text-align:right">
	
		<form action='index.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
		  
				Tipo de p�gina: 
				<select name='filtroTipo' class="combo">
				<option value=''>Todos</option>
                                <?php
                                //
                                $colTiposDePagina = $objDSXadmin->listarTiposDePagina();
                                //
                                foreach($colTiposDePagina AS $objItem) {
                                    //
                                    if($_POST['filtroTipo'] === $objItem->CodigoTipoPagina) {
                                        $sAttrSelected = " selected";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<option value='";
                                    $sHTML .= $objItem->CodigoTipoPagina;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objItem->NomeTipoPagina . "</option>";
                                    //
                                    echo($sHTML);
                                }
                                ?>
				</select>
			</td>
			<td style="vertical-align:middle;">
				<input type='Submit' name="filtrarCliente" value='Filtrar' class="botao">
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>

<br>

<table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
<tr align='center' bgcolor="<?= $bgcolor4 ?>">
	<td>

		<table border=0 cellpadding=4 cellspacing=0 width='100%' bgcolor="<?= $bgcolor5 ?>" align='center'>
		<tr bgcolor="<?= $bgcolor4 ?>">
			<td style="font-weight: bold;">C�DIGO</td>
			<td style="font-weight: bold;">P�GINA</td>
			<td style="font-weight: bold;">TIPO</td>
		</tr>
                <?php
                $qtLinhas = 0;
                //
		if ($_POST['filtroTipo']) {
                    $parmFiltro = Array("cd_tipo_pagina" => $_POST['filtroTipo']);
		} else {
                    $parmFiltro = null;
                }
                //
                $colPaginas = $objDSXadmin->listarPaginas($parmFiltro);
		//
                foreach ($colPaginas AS $objItem) {
                    $qtLinhas++;
                    $tr_hover = " onmouseover=\"this.style.cursor='pointer'; this.style.backgroundColor='$bgcolor4'\"";
                    $tr_mouseout = " onmouseout=\"this.style.backgroundColor='" . $bgcolor6 . "'\"";
                    $onclick = " onclick=\"document.location.href='index.php?module=" . $module . "&mode=forms&id_pagina=" . $objItem->IdPagina . "'\" ";
                    ?>
    			<tr <?= $tr_hover . $tr_mouseout . $onclick ?> bgcolor="<?= $bgcolor6 ?>">
    				<td><?php echo($objItem->CodigoPagina); ?></td>
    				<td>
                                    <?php 
                                    if($objItem->Include == 1 || $objItem->TipoPagina->CodigoTipoPagina == 0) {
                                        echo($objItem->Pagina); 
                                    } else {
                                        echo("[HTML]=>".substr($objItem->Pagina,0,15));
                                    }
                                    ?>
                                </td>
    				<td><?php echo($objItem->TipoPagina->NomeTipoPagina); ?></td>
    			</tr><?
                }
                ?>
			
		</table>
		
	</td>
</tr>
</table>

<br><br>

	<div style="text-align: center; font-weight: bold;">Total de p�ginas encontradas: <span style="color:#FF0000"><?= $qtLinhas ?></span> </div>

<br><br>
