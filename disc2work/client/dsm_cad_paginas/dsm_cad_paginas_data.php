<?

if (strtolower($dv_include) == "on") {
    $dv_include = 1;
} else {
    $dv_include = 0;
}

if ($incluir) {

    if ($cd_pagina == "") {
        $msg_erro = "Preencha o c�digo da p�gina.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if ($cd_tipo_pagina == "") {
        $msg_erro = "Selecione o tipo da p�gina.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if (!$msg_erro) {

        $objPagina = new clsDSXpagina();
        //
        $objPagina->CodigoPagina = $cd_pagina;
        $objPagina->Pagina = $ds_pagina;
        $objPagina->TipoPagina->CodigoTipoPagina = $cd_tipo_pagina;
        $objPagina->Include = $dv_include;
        $objPagina->Descricao = $ds_descricao;
        //
        $objDSXadmin = new clsDSXmasterAdmin();
        //
        $RET_id = $objDSXadmin->incluirPagina($objPagina);
        //
        if (!is_null($RET_id) && ($RET_id > 0)) {
            echo retornaMensagem("P�gina inclu�da com sucesso.");
        } else {
            echo retornaMensagem("Falhou na inclus�o da p�gina.");
        }

        include_once("dsm_cad_paginas/dsm_cad_paginas_view.php");
    } else {
        include_once("dsm_cad_paginas/dsm_cad_paginas_forms.php");
    }
}

if ($salvar) {

    if ($cd_pagina == "") {
        $msg_erro = "Preencha o c�digo da p�gina.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if ($cd_tipo_pagina == "") {
        $msg_erro = "Selecione o tipo da p�gina.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if (!$msg_erro) {

        $objPagina = new clsDSXpagina();
        //
        $objPagina->IdPagina = $id_pagina;
        $objPagina->CodigoPagina = $cd_pagina;
        $objPagina->Pagina = $ds_pagina;
        $objPagina->TipoPagina->CodigoTipoPagina = $cd_tipo_pagina;
        $objPagina->Include = $dv_include;
        $objPagina->Descricao = $ds_descricao;
        //
        $objDSXadmin = new clsDSXmasterAdmin();
        //
        $objDSXadmin->atualizarPagina($objPagina);

        if ($objDSXadmin->ERRO == 1) {
            echo retornaMensagem("Ocorreu um erro ao atualizar os dados da p�gina. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        } else {
            echo retornaMensagem("Dados da p�gina atualizados com sucesso.");
        }
    }

    include_once("dsm_cad_paginas/dsm_cad_paginas_forms.php");
}

if ($excluir) {
    //
    $objDSXadmin = new clsDSXmasterAdmin();
    //
    $objDSXadmin->excluirPagina($id_pagina);

    if ($objDSXadmin->ERRO == 1) {
        echo retornaMensagem("Ocorreu um erro ao excluir a p�gina. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
    } else {
        echo retornaMensagem("P�gina exclu�da com sucesso.");
    }

    include_once("dsm_cad_paginas/dsm_cad_paginas_view.php");
}
?>