<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

$DW_id_pagina = $_REQUEST['id_pagina'];

$objDSXadmin = new clsDSXmasterAdmin();
//
$objDSXpagina = new clsDSXpagina();

// Se veio o id do pagina
if ($DW_id_pagina != "") {
    //
    $objDSXpagina = $objDSXadmin->consultarPagina($DW_id_pagina, "");
    //
    if(!is_null($objDSXpagina)) {
        $DW_cd_tipo_pagina = $objDSXpagina->TipoPagina->CodigoTipoPagina;
    }
}
?>

<form method=post enctype='multipart/form-data' action='index.php' name='frm'>
    <input type=hidden name='module' value='<?= $_REQUEST['module'] ?>'>
    <input type=hidden name='mode' value='data'>


    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">CADASTRO DE P�GINA</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">C�digo da p�gina:</td>
                        <td style="font-weight: bold;"><input type="text" name="cd_pagina" value="<?php echo($objDSXpagina->CodigoPagina);  ?>" class="texto" size="48" maxlength="45"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Tipo da p�gina:</td>
                        <td style="font-weight: bold;">
                            <select name="cd_tipo_pagina" class="combo">
                                <option value="">(Selecione o tipo da p�gina)</option>
                                <?
                                $colTiposDePagina = $objDSXadmin->listarTiposDePagina();
                                //
                                foreach($colTiposDePagina AS $objItem) {
                                    //
                                    if($DW_cd_tipo_pagina === $objItem->CodigoTipoPagina) {
                                        $sAttrSelected = " selected";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<option value='";
                                    $sHTML .= $objItem->CodigoTipoPagina;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objItem->NomeTipoPagina . "</option>";
                                    //
                                    echo($sHTML);
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Conte�do do p�gina:</td>
                        <td style="font-weight: bold;">
                            <textarea 
                                class="texto" cols=45 maxlength="1024" name="ds_pagina"
                                style="width:450px; height:120px"
                            ><?php echo($objDSXpagina->Pagina); ?></textarea>&nbsp;
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Include?</td>
                        <td style="font-weight: bold;">
                            <input type="checkbox" name="dv_include" class="checkbox"
                            <?php
                            if($objDSXpagina->Include == 1) {
                                echo(" checked");
                            }
                            ?>
                            >
                        </td>
                    </tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Descri��o:</td>
                        <td style="font-weight: bold;">
                            <textarea 
                                class="texto" cols=45 maxlength="1024" name="ds_descricao"
                                style="width:450px; height:120px"
                            ><?php echo($objDSXpagina->Descricao); ?></textarea>&nbsp;
                        </td>
                    </tr>

                </table>
                <input type=hidden name='id_pagina' value="<?php echo($objDSXpagina->IdPagina); ?>">

            </td>
        </tr>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td>
                            <input type="button" onclick="document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <? if ($DW_id_pagina != "") { ?>
                                <input type="submit" name="salvar" value="Salvar"  class="botao">
                                <input type="submit" name="excluir" value="Excluir"  class="botao" onclick="return confirm('Tem certeza que deseja excluir permanentemente este registro?')">
                            <? } else { ?>
                                <input type="submit" name="incluir" value="Incluir" class="botao">
                            <? } ?>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>
