<?
include_once "disc_config/dw_dlg_aguarde_msg.php";

$acesso_administrativo = checaPermissao($module, "A", $user_access);

$REQ_idc = $_REQUEST["idc"];

// Se veio o id do perfil de funcao
if ($_REQUEST['id_perfil_funcao']) {
    //
    $objPerfilDeFuncao_temp = DISCX_consultarPerfilDeFuncao(1, $_REQUEST["id_perfil_funcao"]);
    $objPerfilDeFuncao = json_decode($objPerfilDeFuncao_temp);
    //
    $DW_id_cliente = $objPerfilDeFuncao->IdCliente;
    $REQ_idc = $objPerfilDeFuncao->CodigoCliente;
}

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if (($DW_id_cliente != "" && $_REQUEST['id_perfil_funcao'] && $objCliente->Id == $DW_id_cliente)) {
            //
            $REQ_idc = $objCliente->Codigo;
        }
        if ($REQ_idc == $objCliente->Codigo || $REQ_idc == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            $DEFWEB_cd_cliente = $REQ_id_cliente;
            $DW_id_cliente = $objCliente->Id;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
?>

<?php
}
?>

<script language="javascript">
	//
	jQuery(document).ready(function() {
		jQuery('input[name^=pc_disc_]').on('blur', function() {
			var pc = jQuery(this).val();
			if ( /\,/.test(pc) ){
				var pc = jQuery(this).val().replace(',', '.');
			}
			jQuery(this).val(parseFloat(pc).toFixed(3));
		});
	});
	//
	function enviarDados(){
		if ( validacaoDeCampos() ){
			return true;
		} else {
			return false;
		}
	};
	//
	function validacaoDeCampos(){
		jQuery('#tdMsg').empty();
		//
		var nmFuncao = jQuery('input[name=nm_perfil_funcao]');
		var dsFuncao = jQuery('textarea[name=ds_perfil_funcao]');
		var pcD      = jQuery('input[name=pc_disc_d]');
		var pcDMax   = jQuery('input[name=pc_disc_d_max]');
		var pcDMin   = jQuery('input[name=pc_disc_d_min]');
		var pcI      = jQuery('input[name=pc_disc_i]');
		var pcIMax   = jQuery('input[name=pc_disc_i_max]');
		var pcIMin   = jQuery('input[name=pc_disc_i_min]');
		var pcS      = jQuery('input[name=pc_disc_s]');
		var pcSMax   = jQuery('input[name=pc_disc_s_max]');
		var pcSMin   = jQuery('input[name=pc_disc_s_min]');
		var pcC      = jQuery('input[name=pc_disc_c]');
		var pcCMax   = jQuery('input[name=pc_disc_c_max]');
		var pcCMin   = jQuery('input[name=pc_disc_c_min]');
		//
		// Valida o Nome da Funcao
		if ( nmFuncao.val() == "" ){
			jQuery('#tdMsg').html("Nome da fun��o n�o informado!");
			nmFuncao.focus();
			return false;
		}
		// Valida a Descricao da Funcao
		else if ( dsFuncao.val() == "" ){
			jQuery('#tdMsg').html("Descri��o da fun��o n�o informado!");
			dsFuncao.focus();
			return false;
		}
		// Valida o valor % para o fator D
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcD.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator D inv�lido!");
			pcD.focus();
			return false;
		}
		// Valida o valor % para o fator D Maximo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcDMax.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator D M�ximo inv�lido!");
			pcDMax.focus();
			return false;
		}
		// Valida o valor % para o fator D Minimo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcDMin.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator D M�nimo inv�lido!");
			pcDMin.focus();
			return false;
		}
		// Valida o valor % para o fator I
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcI.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator I inv�lido!");
			pcI.focus();
			return false;
		}
		// Valida o valor % para o fator I Maximo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcIMax.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator I M�ximo inv�lido!");
			pcIMax.focus();
			return false;
		}
		// Valida o valor % para o fator I Minimo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcIMin.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator I M�nimo inv�lido!");
			pcIMin.focus();
			return false;
		}
		// Valida o valor % para o fator S
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcS.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator S inv�lido!");
			pcS.focus();
			return false;
		}
		// Valida o valor % para o fator S Maximo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcSMax.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator S M�ximo inv�lido!");
			pcSMax.focus();
			return false;
		}
		// Valida o valor % para o fator S Minimo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcSMin.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator S M�nimo inv�lido!");
			pcSMin.focus();
			return false;
		}
		// Valida o valor % para o fator C
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcC.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator C inv�lido!");
			pcC.focus();
			return false;
		}
		// Valida o valor % para o fator C Maximo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcCMax.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator C M�ximo inv�lido!");
			pcCMax.focus();
			return false;
		}
		// Valida o valor % para o fator C Minimo
		else if ( !/(\d+|\d+\.|\d+\.\d+)/.test(pcCMin.val()) ){
			jQuery('#tdMsg').html("Porcentual do fator C M�nimo inv�lido!");
			pcCMin.focus();
			return false;
		} else {
			return true;
		}
	}
	//
</script>

<form method=post enctype='multipart/form-data' action='index.php' id="frm" name='frm' onsubmit="return enviarDados()">
    <input type=hidden name='id_cliente' value="<?= $DW_id_cliente ?>">
    <input type=hidden name='id_perfil_funcao' value="<?= $_REQUEST['id_perfil_funcao'] ?>">
    <input type=hidden name='module' value="<?= $_REQUEST['module'] ?>">
    <input type=hidden name='mode' value='data'>


    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<? echo($bgcolor5); ?>" align='center'>
        <tr align='center' bgcolor="<? echo($bgcolor4); ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">CADASTRO DO PERFIL DE FUN��O</td>
        </tr>
        <tr>
            <td bgcolor="<? echo($bgcolor6); ?>">

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr>
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Cliente:</td>
                        <td style="font-weight: bold; vertical-align:middle;"> 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select id="id_cliente" name='id_cliente' class="combo">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr bgcolor="<? echo($bgcolor6); ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Nome da fun��o:</td>
                        <td style="font-weight: bold;">
                            <input type="text" name="nm_perfil_funcao" value="<?php echo($objPerfilDeFuncao->Nome); ?>" class="texto" size="45" maxlength="45">
                        </td>
                    </tr>

                    <tr bgcolor="<? echo($bgcolor6); ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Descri��o:</td>
                        <td style="font-weight: bold;">
                            <textarea 
                                class="texto" cols=45 maxlength="1024" name="ds_perfil_funcao"
                                style="width:450px; height:120px"
                            ><?php echo($objPerfilDeFuncao->Descricao); ?></textarea>&nbsp;
                        </td>
                    </tr>
		
                    <tr bgcolor="<? echo($bgcolor6); ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Perfil:</td>
                        <td style="text-align: left;">
                            <table border=0 cellpadding=4 cellspacing=0 align='left'>
                                <tr>
                                    <td></td>
                                    <td style="text-align: center;">%</td>
                                    <td style="text-align: center;">% M�ximo</td>
                                    <td style="text-align: center;">% M�nimo</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; vertical-align: middle; text-align: center; ">D</td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_d" value="<?php echo($objPerfilDeFuncao->DISC_D); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_d_max" value="<?php echo($objPerfilDeFuncao->DISC_D_Max); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_d_min" value="<?php echo($objPerfilDeFuncao->DISC_D_Min); ?>" class="texto" size="8" maxlength="11"></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; vertical-align: middle; text-align: center; ">I</td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_i" value="<?php echo($objPerfilDeFuncao->DISC_I); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_i_max" value="<?php echo($objPerfilDeFuncao->DISC_I_Max); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_i_min" value="<?php echo($objPerfilDeFuncao->DISC_I_Min); ?>" class="texto" size="8" maxlength="11"></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; vertical-align: middle; text-align: center; ">S</td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_s" value="<?php echo($objPerfilDeFuncao->DISC_S); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_s_max" value="<?php echo($objPerfilDeFuncao->DISC_S_Max); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_s_min" value="<?php echo($objPerfilDeFuncao->DISC_S_Min); ?>" class="texto" size="8" maxlength="11"></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold; vertical-align: middle; text-align: center; ">C</td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_c" value="<?php echo($objPerfilDeFuncao->DISC_C); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_c_max" value="<?php echo($objPerfilDeFuncao->DISC_C_Max); ?>" class="texto" size="8" maxlength="11"></td>
                                    <td style="text-align: right;"><input type="text" name="pc_disc_c_min" value="<?php echo($objPerfilDeFuncao->DISC_C_Min); ?>" class="texto" size="8" maxlength="11"></td>
                                </tr>								
                            </table>
                        </td>
                    </tr>
					<tr bgcolor="<? echo($bgcolor6); ?>">
						<td id="tdMsg" style="font-weight: bold; color: red; vertical-align: middle;">
						</td>
					</tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<? echo($bgcolor6); ?>">
                        <td>
                            <input type="button" onclick="document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <? if ($_REQUEST['id_perfil_funcao']) { ?>
                                <input type="submit" name="salvar" value="Salvar"  class="botao">
                                <input type="submit" name="excluir" value="Excluir"  class="botao" onclick="return confirm('Tem certeza que deseja excluir permanentemente este registro?')">
                            <? } else { ?>
                                <input type="submit" name="incluir" value="Incluir" class="botao">
                            <? } ?>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>