//
jQuery('document').ready( function(){
	configBtnPerfis();
	configRadioOrder();
});
//
//
// Abre/Fecha o entrevistado selecionado
function perfil(img, tr){
    browser = "NAVEGADOR" + navigator.userAgent;
	var element = document.getElementById(tr);
    var ico = document.getElementById(img);
    if (element.style.display == 'none') {
        if (browser.indexOf("MSIE") > 0)
            element.style.display = "block";
        else
            element.style.display = 'table-row';
		ico.setAttribute('src','img/open.gif');
		montarGraficosAnalise();
		plotarPerfilMaxMinGraf();
    } else {
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}
//
//
function configBtnPerfis(){
	if ( jQuery("tr[id^='trPerfil_']").length > 0 ) {
		var btnOpen  = jQuery('input[name=cmdOpenAll]');
		var btnClose = jQuery('input[name=cmdCloseAll]');
		if ( btnOpen !== undefined ){
			btnOpen.bind( "click", function() {
				// Abre a analise de todos os entrevistados
				jQuery("tr[id^='trPerfil_']").show();
				jQuery("img[id^='imgPerfil_']").attr("src","img/open.gif");
				montarGraficosAnalise();
				plotarPerfilMaxMinGraf();
			});
			if ( btnOpen.css('display') == 'none' ){
				btnOpen.show();
			}
		}
		if ( btnClose !== undefined ){
			btnClose.bind( "click", function() {
				// Fecha a analise de todos os entrevistados
				jQuery("tr[id^='trPerfil_']").hide();
				jQuery("img[id^='imgPerfil_']").attr("src","img/close.gif");
			});
			if ( btnClose.css('display') == 'none' ){
				btnClose.show();
			}
		}
	}
}
//
//
function configRadioOrder(){
	jQuery('input[type=radio]').click(function(){
		var optOrder = jQuery('input[name=optOrder]:checked').val();
		if (/(0|1)/.test(optOrder)){
			sortTable(optOrder);
		}
	});
}
//
function sortTable(n) {
	var rows = jQuery('#tblDadosPerfil > tbody > tr').get();
	//
	if ( n == 1 ){
		n = 2;
	}
	//
	rows.sort(function(a, b) {
		var A = getVal(a);
		var B = getVal(b);
		if(A < B) {
			return -1;
		}
		if(A > B) {
			return 1;
		}
		return 0;
	});
	//
	function getVal(tr){
		var tr2 = jQuery('table > tbody > tr:eq(0)', tr);
		var tr3 = jQuery('table > tbody > tr:eq(0)', tr2);
		var td  = jQuery('td:eq(' + n + ')', tr3);
		var spn = jQuery('span', td);
		var tx  = jQuery.trim(spn.text().toUpperCase());
		if ( n != 0 ){
			// Se for o resultado
			tx = tx.replace("C", "X");
		}
		return tx;
	}
	//
	jQuery.each(rows, function(index, row) {
		jQuery('#tblDadosPerfil').children('tbody').append(row);
	});
	//
}

function filtrar() {
	// Valor do filtro
	var filter = jQuery('#entrFiltro').val().toUpperCase();
	//
	// Dados dos entrevistados
	var rows = jQuery('#tblDadosPerfil > tbody > tr').get();
	//
	//
	rows.filter(function(row) {
		var rFilter = valFilter(row);
		if(rFilter) {
			row.style.display = "none";
		} else {
			row.style.display = "";
		}
	});
	//
	function valFilter(tr){
		var tr2 = jQuery('table > tbody > tr:eq(0)', tr);
		var tr3 = jQuery('table > tbody > tr:eq(0)', tr2);
		var td1  = jQuery('td:eq(0)', tr3);
		var td2  = jQuery('td:eq(2)', tr3);
		var spn1 = jQuery('span', td1);
		var spn2 = jQuery('span', td2);
		var tx1  = jQuery.trim(spn1.text().toUpperCase());
		var tx2  = jQuery.trim(spn2.text().toUpperCase());
		//console.log("tx1: [" + tx1 + "] - txt2: [" + tx2 + "]\n");
		//
		// caracter \ nao eh permitido
		filter = filter.replace(/\\/g, "");
		//
		// i: case-insensitive
		var pattern = new RegExp(filter, "i");
		// O filter devera ser feito apenas na coluna 2 (resultados) e nao nos perfis (coluna 1)
		//if ((pattern.test(tx1)) || (pattern.test(tx2))){
		if ( pattern.test(tx2) ){
			// valor correspondeu ... nao filtrar
			return false;
		} else {
			// nao correspondeu ao valor procurado ... filtrar
			return true;
		}
	}
	//
	jQuery.each(rows, function(index, row) {
		jQuery('#tblDadosPerfil').children('tbody').append(row);
	});
}
//
function plotarPerfilMaxMinGraf(){
    var vlGraficoCfg = jQuery("#hdnGraficoCfg").val();
    //
    if(vlGraficoCfg == 1) {
    	//
    	jQuery("input[id^='grfAn']").each( function() {
        	// Identica o canvas
        	var sChartId   = jQuery(this).attr("id").replace("grfAn","canvAn");
        	//
        	// Dados do perfil Max e Min
			var sInpMaxId = jQuery(this).attr("id").replace("grfAn","grfMax");
			var sInpMinId = jQuery(this).attr("id").replace("grfAn","grfMin");
        	var sDatMax = JSON.parse(jQuery("input[id='" + sInpMaxId + "']").val());
			var sDatMin = JSON.parse(jQuery("input[id='" + sInpMinId + "']").val());
        	//
			var c   = document.getElementById(sChartId);
        	var ctx = c.getContext("2d");
        	//
			// Altura definida para o canvas (px)
			var hCanvas  = ctx.canvas.clientHeight;
			//
			// Margens (px)
			var mgLeft   	= 10;
			var mgTop   	= 0;
			var mgBottom 	= 0;
			//
			// Quantidade de celulas
			// S�o 5 celulas na horizontal ( D I S C )
			var nCellsX		= 5;
			//
			// Sao 13 celulas na vertical contando com a linha dos labels
			var nCellsY		= 13;
			//
			// Tamanho da celula (px^2)
			var szCell    	= (hCanvas-mgTop-mgBottom)/nCellsY;
			//
			// Calculo da largura do grafico (px)
			var wGraf    	= szCell*nCellsX;
			//
			// Calculo da altura do grafico (px) - remover a linha do label (1 celula)
			var hGraf    	= szCell*(nCellsY-1);
			//
			// Pontos iniciais
			var xIni     	= mgLeft;
			var yIni     	= mgTop;
			//
			// Largura do canvas proporcional
			//ctx.canvas.width = parseInt(wGraf) + mgLeft + 2;
			//
			//---------------------------------------------------------------------
			// Valores Maximo e Minimo da escala 
			var scaleY_max = 100;
			var scaleY_min = 0;
			//
			// Calcula qtde de itens na vertical que comporta o range
			var yItens = (scaleY_max - scaleY_min);
			//---------------------------------------------------------------------
			// Maximo e Minimo DISC
			//
			// x Axes
			var xCoor = [
				xIni+(szCell*1), 
				xIni+(szCell*2), 
				xIni+(szCell*3), 
				xIni+(szCell*4)
			];
			//
			// y Axes
			var yCoor = [
				(((scaleY_max - sDatMax[0])/yItens)*(hGraf+szCell))+(((sDatMax[0] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - sDatMax[1])/yItens)*(hGraf+szCell))+(((sDatMax[1] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - sDatMax[2])/yItens)*(hGraf+szCell))+(((sDatMax[2] - scaleY_min)/yItens)*szCell),
				(((scaleY_max - sDatMax[3])/yItens)*(hGraf+szCell))+(((sDatMax[3] - scaleY_min)/yItens)*szCell)
			];
			//
			var imgData = ctx.getImageData(0, 0, c.width, c.height);
			ctx.putImageData(imgData, 0, 0);
			//
			// Cor da area
			ctx.fillStyle = "rgba(255,153,0, 0.5)";
			//
			for(var idx=0;idx<nCellsX-1;idx++){
				// context.fillRect(x,y,width,height)
				ctx.fillRect(xCoor[idx]-10, yCoor[idx], 20,(scaleY_max - sDatMin[idx])*(hGraf/scaleY_max));
			}
    	});
	}
}
//
function inverterSelecao() {
    var objchkSelecao = document.getElementById('chkSelecao');
    if (objchkSelecao.checked) {
        selecionarTodos();
    } else {
        limparSelecao();
    }
}
//
function selecionarTodos() {
	var objlblSelecao = document.getElementById('lblSelecao');
	objlblSelecao.innerHTML = "Limpar sele��o";
	var objchkPerfil = document.getElementsByName('chkPerfil[]');
	for (iPos = 0; iPos < objchkPerfil.length; iPos++) {
		objchkPerfil[iPos].checked = true;
	}
}
//
function limparSelecao() {
	var objlblSelecao = document.getElementById('lblSelecao');
	objlblSelecao.innerHTML = "Selecionar todos";
	var objchkPerfil = document.getElementsByName('chkPerfil[]');
	for (iPos = 0; iPos < objchkPerfil.length; iPos++) {
		objchkPerfil[iPos].checked = false;
	}
}
//
function confirmarExclusaoDeItens() {
	if(confirm('Tem certeza que deseja excluir os registros selecionados?')) {
		dw_dlg_exibirAguarde();
		return(true);
	} else {
		return(false);
	}
}
//
function executarSubmit() {
	var objForm = document.getElementById("frm");
	dw_dlg_exibirAguarde();
	objForm.submit();
}
//