<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

$REQ_id_cliente = $_REQUEST["idc"];

$GRAFICO_EXIBIR = 1;

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
}

include_once "disc_config/dw_dlg_aguarde_msg.php";

?>

<script src='disc_pfuncao/js/perfis_de_funcao.js' type='text/javascript'></script>
<script src='../../survey/js/dssurvey_graph.js' type='text/javascript'></script>

<form action='index.php' method='post' id="frm" name='frm'>
    <input type="hidden" name="module" value="<?= $module ?>">
    <input type="hidden" name="mode" value="view">
	<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />

	<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
		<tr>
			<td style="vertical-align:middle;">
				<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms&idc=<?=$REQ_id_cliente ?>"><span style="font-weight:bold;">CADASTRAR PERFIL DE FUN��O</span></a>
                &nbsp;&nbsp;
				<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=viewflat"><span style="font-weight:bold;">LISTA SIMPLES</span></a>
			</td>
		</tr>
		<tr>
			<td style="text-align:right">
			
				<table align="right">
					<tr>
						<td style="text-align: right; font-weight: bold; vertical-align:middle;">

							Cliente: 
							<?php
							if($DEFWEB_master_user) {
							?>
								<select name='idc' class="combo"  onchange="javascript:executarSubmit();">
									<?php
									echo($sHTMLidc);
									?>
								</select>
							<?php
							} else {
								echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
							}
							?>

						</td>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
                            <input type='Submit' name="acaoAtualizar" value='Atualizar a lista' 
                                   title="Atualizar a listagem" 
                                   class="botao"
                                   onclick="javascript:dw_dlg_exibirAguarde()"
                                   >
                        </td>
					</tr>
				</table>
	
			</td>
		</tr>
	</table>
    <br>
    <table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr>
            <td>
                <div style="width:140px; display: inline-block;">
                    <input type="checkbox" name="chkSelecao" id="chkSelecao" onchange="javascript:inverterSelecao();">
                    &nbsp;<span id="lblSelecao">Selecionar todos</span>
                </div>    
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                <input type='Submit' name="acaoExcluir" value='Excluir' 
                       title="Excluir registros selecionados." 
                       class="botao"
                       onclick="return confirmarExclusaoDeItens();"
                       >
            </td>
        </tr>
    </table>
    <br>
    <table border=0 cellpadding=0 cellspacing=1 width='98%' align='center'>
        <tr align='center'>
            <td>
			
				<table width="100%">
					<tr>
						<td width="50%">
							ordenar por: 
							<input type="radio" name="optOrder" value="0" checked><b>Perfil</b>
							ou por: 
							<input type="radio" name="optOrder" value="1"><b>Resultado</b>
						</td>
						<td>
							<input type="text" id="entrFiltro" onkeyup="filtrar()" class="dsFiltro" placeholder="Filtrar resultado...">
						</td>
						<td>
							<input type='button' name="cmdOpenAll" value='Abrir todos' title="Abrir todos." class="botao" onmouseover="this.style.cursor='pointer';">
						</td>
						<td>
							<input type='button' name="cmdCloseAll" value='Fechar todos' title="Fechar todos." class="botao" onmouseover="this.style.cursor='pointer';">
						</td>
					</tr>
					<tr>
						<td colspan="4">
                <?php
				//
				$total_linhas = 0;
				//
				$colItens_temp = DISCX_listarPerfisDeFuncao(1, "", $REQ_id_cliente, "");
				$colItens = json_decode($colItens_temp, false);
				//
				include_once("/home/users/__discx/inc/libDiscX.php");
				$objDWCPerfil = new clsDWCPerfilDisc();
				//
                $bPrimeiraLinha = TRUE;
				foreach ($colItens As $objItem) {
					//
					$tr_hover = " onmouseover=\"this.style.cursor='pointer';\"";
					$sLinkModificar = "onclick=\"javascript:document.location.href='index.php?module=" . $module . "&mode=forms&idc=" . $REQ_id_cliente . "&id_perfil_funcao=" . $objItem->IdPerfilFuncao . "'\" ";
					$total_linhas++;
					//
					$objDWCPerfil->setPerfilX("", $objItem->DISC_D, $objItem->DISC_I,  $objItem->DISC_S, $objItem->DISC_C);
					$sSerie   = $objDWCPerfil->serieX;
					$sLetters = $objDWCPerfil->resultadoX;
					$result_fmt = $objDWCPerfil->resulXHtml;
					$objDWCPerfil->clear();
					//
					$sSerieMax  = "[" . intval($objItem->DISC_D_Max);
					$sSerieMax .= "," . intval($objItem->DISC_I_Max);
					$sSerieMax .= "," . intval($objItem->DISC_S_Max);
					$sSerieMax .= "," . intval($objItem->DISC_C_Max) . "]";
					//
					$sSerieMin  = "[" . intval($objItem->DISC_D_Min);
					$sSerieMin .= "," . intval($objItem->DISC_I_Min);
					$sSerieMin .= "," . intval($objItem->DISC_S_Min);
					$sSerieMin .= "," . intval($objItem->DISC_C_Min) . "]";
					//
					if ($bPrimeiraLinha) {
                        $bPrimeiraLinha = FALSE;
						echo "<table id=\"tblDadosPerfil\" width='100%'>";
					}
                    ?>
					<?php // table tblDadosPerfil ordenacao ?>
							<tr>
								<td>
					<?php // table tblDadosPerfil ordenacao ?>
	
			<table width='100%' border=0 cellpadding=5 cellspacing=1 bgcolor='<?php echo($bgcolor5); ?>' align='center'>
                <tr width="100%">
                    <td bgcolor="<?php echo($bgcolor4); ?>">
							
						<table width="100%" cellpadding="0" cellspacing="0" border=0>
							<tr <?= $tr_hover ?>>
                                <td onclick="javascript:perfil('imgPerfil_<?= $objItem->IdPerfilFuncao ?>', 'trPerfil_<?= $objItem->IdPerfilFuncao ?>');">
									<img id="imgPerfil_<?php echo($objItem->IdPerfilFuncao);?>" src="img/close.gif" border="0">&nbsp;
                                    <input type="checkbox" name="chkPerfil[]" id="chkPerfil[]" value="<?php echo($objItem->IdPerfilFuncao); ?>">
									<span class="td_perfil_nome">
                                    <?php
										echo($objItem->Nome);
                                    ?>
									</span>
                                </td>
								<td>
									<span class="td_perfil_descricao">
                                    <?php
										echo($objItem->Descricao);
                                    ?>
									</span>
								</td>
								<td>
									<?php
									/*
										$result_fmt = "";
										for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
											if ( $nLetter == 0 ) {
												$result_fmt = "<span class=\"disc_letter_main\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											} else {
												$result_fmt .= "<span class=\"disc_letter_norm\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											}
										}
										if ( strlen($sLetters) > 0 ) {
											*/
											echo($result_fmt);											
										//}
									?>
								</td>
								<td>
                                    <input type='button' name="cmdModificar" value='Modificar' 
                                        title="Alterar dados." 
                                        class="botao" 
										<?= $sLinkModificar ?>
                                    >
								</td>
                            </tr>
                        </table>
						
                    </td>
                </tr>
                <tr style="display:none; padding: 0px;" id="trPerfil_<?php echo($objItem->IdPerfilFuncao);?>" bgcolor="<?php echo($bgcolor6); ?>">
                    <td style="padding: 10px;">						
						
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <td>Resultado</td>
                                    <?php
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%Max</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%Min</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_D)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_D_Max)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_D_Min)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_I)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_I_Max)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_I_Min)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_S)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_S_Max)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_S_Min)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_C)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_C_Max)); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo(intval($objItem->DISC_C_Min)); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAn" . $objItem->IdPerfilFuncao . "' value='". $sSerie ."' >");
										echo("<input type='hidden' id='grfMax" . $objItem->IdPerfilFuncao . "' value='". $sSerieMax ."' >");
										echo("<input type='hidden' id='grfMin" . $objItem->IdPerfilFuncao . "' value='". $sSerieMin ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAn" . $objItem->IdPerfilFuncao . "' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
                                </tr>
							</tbody>
						</table>
						
					</td>
				</tr>
			</table>
			
					<?php // table tblDadosPerfil ordenacao ?>
								</td>
							</tr>
					<?php // table tblDadosPerfil ordenacao ?>
			
			<?  } // end for?>
			
            <?php
            if ( $total_linhas == 0 ) {
                echo("N�O FOI ENCONTRADO NENHUM PERFIL!");
            } else {
				echo "</table>";
			?>
				<br><br>

				<div style="text-align: center; font-weight: bold;">Total de registros encontrados: <span style="color:#FF0000"><?= $total_linhas ?></span> </div>

				<br><br>
			<?php
			}
            ?>

						</td>
					</tr>
				</table>
            </td>
        </tr>
    </table>
</form>