<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

$REQ_id_cliente = $_REQUEST["idc"];

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
}

if (($acaoExcluir)) {

    $arrayPerfis = $_REQUEST['chkPerfil'];
    $sPerfis = "";
    $sMensagemAdicional = "";
    
    $sCor = "navy";

    for ($i = 0; $i < count($arrayPerfis); $i++) {
        if ($sPerfis != "") {
            $sPerfis .= ",";
        }
        $sPerfis .= $arrayPerfis[$i];
    }

    // Se algum perfil foi selecionado...
    if (count($arrayPerfis) > 0) {
        //
        $colPerfis_temp = DISCX_listarPerfisDeFuncao(1, "", $REQ_id_cliente, "");
        $colPerfis = json_decode($colPerfis_temp, false);
        
        // Zera a quantidade de perfis processados
        $iQtdePerfis = 0;
        //
        
        // Percorre o array de Perfis
        for($iPos = 0; $iPos < count($arrayPerfis); $iPos++) {

            // Indicador de falha no processamento
            $bFalhou = FALSE;
            
            // Extrai o perfil atual
            $sPerfil = $arrayPerfis[$iPos];
            // Le os dados do perfil
            $objPerfilDeFuncao = DISCX_obterPerfilDeFuncaoPeloID($colPerfis, $sPerfil);
            
            if ($acaoExcluir) {
                //
                $objRetorno_temp = DISCX_excluirPerfilDeFuncao(1, $objPerfilDeFuncao);
                $objRetorno = json_decode($objRetorno_temp, false);

                //
                if ($objRetorno->ERRO != 0) {
                    //
                    $bFalhou = TRUE;
                }
                //
                unset($objPerfil);
            }

            if($bFalhou) {
                //
                if($sMensagemAdicional == "") {
                    //
                    $sMensagemAdicional = "Ocorreram problemas com os seguintes perfis de fun��o:<br />";
                    $sMensagemAdicional .= "<br />";
                }
                //
                $sMensagemAdicional .= $objPerfil->Nome;
                $sMensagemAdicional .= " (" . $objPerfil->IdPerfilFuncao . ")";
                $sMensagemAdicional .= "<br>";
                //
            } else {
                // Incrementa o contador de perfis
                $iQtdePerfis++;
            }
        }
        
        if($acaoExcluir) {
            $sMensagem = "Nenhum perfil de fun��o foi exclu�do!";
            //
        }
        
        // Se algum perfil foi processado...
        if($iQtdePerfis > 0) {
            // Se foi enviado mais de um e-mail
            if($iQtdePerfis > 1) {
                $sVarios = " perfis de fun��o";
            } else {
                $sVarios = " perfil de fun��o";
            }
            // Monta a mensagem de retorno
            $sMensagem = sprintf("%d", $iQtdePerfis);
            //
            if($acaoExcluir) {
                //
                $sMensagem .= $sVarios;
                $sMensagem .= " exclu�do";
                if($iQtdePerfis > 1) {
                    $sMensagem .= "s";
                }
                $sMensagem .= "!";
            }
        }
    } else {
        //
        $sMensagem = "Selecione pelo menos um perfil de fun��o para executar a a��o!";
        $sCor = "red";
        
    }

    // Mensagem de retorno
    echo retornaMensagem("<font color='" . $sCor . "'>" . $sMensagem . "</font>");
    
    // Se tiver mensagem adicional
    if($sMensagemAdicional != "") {
        //
        // Mensagem adicional de retorno
        echo retornaMensagem("<font color='red'>" . $sMensagemAdicional . "</font>");
    }

}



include_once "disc_config/dw_dlg_aguarde_msg.php";

?>

<script language="javascript">
    function inverterSelecao() {
        var objchkSelecao = document.getElementById('chkSelecao');
        if (objchkSelecao.checked) {
            selecionarTodos();
        } else {
            limparSelecao();
        }
    }

    function selecionarTodos() {
        var objlblSelecao = document.getElementById('lblSelecao');
        objlblSelecao.innerHTML = "Limpar sele��o";
        var objchkPerfil = document.getElementsByName('chkPerfil[]');
        for (iPos = 0; iPos < objchkPerfil.length; iPos++) {
            objchkPerfil[iPos].checked = true;
        }
    }

    function limparSelecao() {
        var objlblSelecao = document.getElementById('lblSelecao');
        objlblSelecao.innerHTML = "Selecionar todos";
        var objchkPerfil = document.getElementsByName('chkPerfil[]');
        for (iPos = 0; iPos < objchkPerfil.length; iPos++) {
            objchkPerfil[iPos].checked = false;
        }
    }
    
    function confirmarExclusaoDeItens() {
        if(confirm('Tem certeza que deseja excluir os registros selecionados?')) {
            dw_dlg_exibirAguarde();
            return(true);
        } else {
            return(false);
        }
    }
    
    function executarSubmit() {
        var objForm = document.getElementById("frm");
        dw_dlg_exibirAguarde();
        objForm.submit();
    }
    
</script>

<form action='index.php' method='post' id="frm" name='frm'>
    <input type="hidden" name="module" value="<?= $module ?>">
    <input type="hidden" name="mode" value="viewflat">

    <table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
        <tr>
            <td style="vertical-align:middle;">
                <a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms&idc=<?=$REQ_id_cliente ?>"><span style="font-weight:bold;">CADASTRAR PERFIL DE FUN��O</span></a>
            </td>
        </tr>
        <tr>
            <td style="text-align:right">

                <table align="right">
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 

                            Cliente: 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select name='idc' class="combo"  onchange="javascript:executarSubmit();">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
                            <input type='Submit' name="acaoAtualizar" value='Atualizar a lista' 
                                   title="Atualizar a listagem" 
                                   class="botao"
                                   onclick="javascript:dw_dlg_exibirAguarde()"
                                   >
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br>
    <table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr>
            <td>
                <div style="width:140px; display: inline-block;">
                    <input type="checkbox" name="chkSelecao" id="chkSelecao" onchange="javascript:inverterSelecao();">
                    &nbsp;<span id="lblSelecao">Selecionar todos</span>
                </div>    
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                <input type='Submit' name="acaoExcluir" value='Excluir' 
                       title="Excluir registros selecionados." 
                       class="botao"
                       onclick="return confirmarExclusaoDeItens();"
                       >
            </td>
        </tr>
    </table>
    <br>

    <table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td>
                <table border=0 cellpadding=4 cellspacing=0 width='100%' bgcolor="<?= $bgcolor5 ?>" align='center'>
                    <tr bgcolor="<?= $bgcolor4 ?>">
                        <td style="font-weight: bold;">&nbsp;</td>
                        <td style="font-weight: bold;">ID</td>
                        <td style="font-weight: bold;">NOME</td>
                        <td style="font-weight: bold;">DESCRICAO</td>
                        <td style="font-weight: bold;">OP��ES</td>
                    </tr>
<?
//
$total_linhas = 0;
//
$colItens_temp = DISCX_listarPerfisDeFuncao(1, "", $REQ_id_cliente, "");
$colItens = json_decode($colItens_temp, false);
//
foreach ($colItens As $objItem) {
    //
    $tr_hover = " onmouseover=\"this.style.cursor='pointer'; this.style.backgroundColor='$bgcolor4'\"";
    $tr_mouseout = " onmouseout=\"this.style.backgroundColor='" . $bgcolor6 . "'\"";
    $sLinkModificar = "onclick=\"javascript:document.location.href='index.php?module=" . $module . "&mode=forms&idc=" . $REQ_id_cliente . "&id_perfil_funcao=" . $objItem->IdPerfilFuncao . "'\" ";
    $total_linhas++;
        //
        ?>
                            <tr <?= $tr_hover . $tr_mouseout . $onclick ?> bgcolor="<?= $bgcolor6 ?>">
                                <td><input type="checkbox" name="chkPerfil[]" id="chkPerfil[]" value="<?php echo($objItem->IdPerfilFuncao); ?>"></td>
                                <td><?php echo($objItem->IdPerfilFuncao); ?></td>
                                <td><?php echo($objItem->Nome); ?></td>
                                <td><?php echo($objItem->Descricao); ?></td>
                                <td>
                                    <input type='button' name="cmdModificar" value='Modificar' 
                                           title="Alterar dados." 
                                           class="botao"
        <?= $sLinkModificar ?>
                                           >

                                </td>
                            </tr>
        <?php
}
?>

                </table>

            </td>
        </tr>
    </table>

    <br><br>

    <div style="text-align: center; font-weight: bold;">Total de registros encontrados: <span style="color:#FF0000"><?= $total_linhas ?></span> </div>

    <br><br>

</form>
