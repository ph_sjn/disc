<?

$REQ_id_perfil_funcao = $_POST["id_perfil_funcao"];
$REQ_cd_cliente = $_REQUEST["id_cliente"];

$msg_erro = "";

//
if($DEFWEB_master_user) {
    //
    $DISC_objCliente = DISCX_consultarCliente("", $REQ_cd_cliente);
    $DISC_objClienteDecoded = json_decode($DISC_objCliente);

    // Atribui o c�digo e o ID do cliente
    $DEFWEB_cd_cliente = $DISC_objClienteDecoded->Codigo;
    $DEFWEB_id_cliente = $DISC_objClienteDecoded->Id;
}

if ($incluir) {

    if ($_POST["nm_perfil_funcao"] == "") {
        $msg_erro = "Preencha o nome do perfil da fun��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    
    if (!$msg_erro) {

        $objPerfil = new clsDSXlocalPerfilDeFuncao();
        
        $objPerfil->IdCliente = $DEFWEB_id_cliente;
        $objPerfil->Nome = $_POST["nm_perfil_funcao"];
        $objPerfil->Descricao = $_POST["ds_perfil_funcao"];
        $objPerfil->DISC_D = $_POST["pc_disc_d"];
        $objPerfil->DISC_D_Min = $_POST["pc_disc_d_min"];
        $objPerfil->DISC_D_Max = $_POST["pc_disc_d_max"];
        $objPerfil->DISC_I = $_POST["pc_disc_i"];
        $objPerfil->DISC_I_Min = $_POST["pc_disc_i_min"];
        $objPerfil->DISC_I_Max = $_POST["pc_disc_i_max"];
        $objPerfil->DISC_S = $_POST["pc_disc_s"];
        $objPerfil->DISC_S_Min = $_POST["pc_disc_s_min"];
        $objPerfil->DISC_S_Max = $_POST["pc_disc_s_max"];
        $objPerfil->DISC_C = $_POST["pc_disc_c"];
        $objPerfil->DISC_C_Min = $_POST["pc_disc_c_min"];
        $objPerfil->DISC_C_Max = $_POST["pc_disc_c_max"];
        
        $objRetorno_temp = DISCX_criarPerfilDeFuncao(1, $objPerfil);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Registro inclu�do com sucesso.");
        } else {
            echo retornaMensagem("Falhou na inclus�o do registro.");
        }

        include_once("disc_pfuncao/disc_pfuncao_view.php");
    } else {
        include_once("disc_pfuncao/disc_pfuncao_forms.php");
    }
}

if ($salvar) {

    if ($_POST["nm_perfil_funcao"] == "") {
        $msg_erro = "Preencha o nome do perfil da fun��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if (!$msg_erro) {

        $objPerfil = new clsDSXlocalPerfilDeFuncao();
        
        $objPerfil->IdPerfilFuncao = $_POST["id_perfil_funcao"];
        $objPerfil->IdCliente = $DEFWEB_id_cliente;
        $objPerfil->Nome = $_POST["nm_perfil_funcao"];
        $objPerfil->Descricao = $_POST["ds_perfil_funcao"];
        $objPerfil->DISC_D = $_POST["pc_disc_d"];
        $objPerfil->DISC_D_Min = $_POST["pc_disc_d_min"];
        $objPerfil->DISC_D_Max = $_POST["pc_disc_d_max"];
        $objPerfil->DISC_I = $_POST["pc_disc_i"];
        $objPerfil->DISC_I_Min = $_POST["pc_disc_i_min"];
        $objPerfil->DISC_I_Max = $_POST["pc_disc_i_max"];
        $objPerfil->DISC_S = $_POST["pc_disc_s"];
        $objPerfil->DISC_S_Min = $_POST["pc_disc_s_min"];
        $objPerfil->DISC_S_Max = $_POST["pc_disc_s_max"];
        $objPerfil->DISC_C = $_POST["pc_disc_c"];
        $objPerfil->DISC_C_Min = $_POST["pc_disc_c_min"];
        $objPerfil->DISC_C_Max = $_POST["pc_disc_c_max"];
        
        $objRetorno_temp = DISCX_gravarPerfilDeFuncao(1, $objPerfil);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Dados atualizados com sucesso.");
        } else {
            echo retornaMensagem("Ocorreu um erro ao atualizar o registro. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        }

    }

    include_once("disc_pfuncao/disc_pfuncao_forms.php");

}

if ($excluir) {

    if (!$msg_erro) {

        $objPerfil = new clsDSXlocalPerfilDeFuncao();
        
        $objPerfil->IdPerfilFuncao = $_POST["id_perfil_funcao"];
        $objPerfil->IdCliente = $DEFWEB_id_cliente;

        //
        $objRetorno_temp = DISCX_excluirPerfilDeFuncao(1, $objPerfil);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Registro exclu�do com sucesso.");
        } else {
            echo("<pre>");
            print_r($objRetorno);
            echo("</pre>");
            echo("<hr>");
            echo retornaMensagem("Ocorreu um erro ao excluir o registro. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        }

    }

    include_once("disc_pfuncao/disc_pfuncao_view.php");
}
