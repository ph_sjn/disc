// IN�CIO C�DIGO RICARDO
var chatTimerCount = 0;
var chatTimerMin   = 2000; // 2 SEGUNDOS M�NIMOS DE ESPERA
var chatTimerMax   = 60000; // 60 SEGUNDOS M�XIMOS DE ESPERA
var chatTimer      = chatTimerMax;
// FIM C�DIGO RICARDO

function Ajax() {
  try {
    return new ActiveXObject("Microsoft.XMLHTTP");
  } catch(e) {
    try {
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch(ex) {
      try {
        return new XMLHttpRequest();
      } catch(exc) {
        return false;
      }
    }
  }
}

function enviarMensagem(id_remetente, id_destinatario, mensagem) {

	ajax_mensagem = Ajax();
	if(ajax_mensagem){
		pagina = "chat/chat_envia_mensagem.php";
		var params = "id_remetente="+id_remetente+"&id_destinatario="+id_destinatario+"&mensagem="+mensagem;
		ajax_mensagem.open("POST", pagina, true);
		ajax_mensagem.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajax_mensagem.send(params);
	}
	document.frmChat.mensagem.value = "";
	document.frmChat.mensagem.focus();
	return false;
	
}

function atualizarMensagens() {

	ajax_mensagens = Ajax();
	if(ajax_mensagens){
		pagina = "chat/chat_mensagens.php";
		ajax_mensagens.open("GET", pagina, true);
		ajax_mensagens.onreadystatechange = function() {
			if(ajax_mensagens.readyState == 4){
				if(ajax_mensagens.status == 200){
					if (document.getElementById('div_chat').innerHTML != "") {
						//alert('atualizarMensagens() : '+ ajax_verifica_mensagens.responseText);
						document.getElementById('div_mensagens').innerHTML = ajax_mensagens.responseText;
					}
					var objDiv = document.getElementById('div_mensagens');
					if (objDiv != null)
						objDiv.scrollTop = objDiv.scrollHeight;
				}
			}
		}
		ajax_mensagens.send(null);
	}
	
	if (document.getElementById('div_mensagens') != null && document.getElementById('div_mensagens').innerHTML != "") {
		document.getElementById('div_chat').style.display = "block";
	}
	
}

function abrirChat(id_user) {

	if (document.getElementById('div_chat').style.display == "none" || document.getElementById('div_chat').style.display == "") {
	
		ajax_chat = Ajax();
		if(ajax_chat){
			pagina = "chat/chat_corpo.php";
			if(id_user.length>0) pagina += "?id_user_target="+id_user;
			ajax_chat.open("GET", pagina, true);
			ajax_chat.onreadystatechange = function() {
				if(ajax_chat.readyState == 4){
					if(ajax_chat.status == 200){
						document.getElementById('div_chat').innerHTML = ajax_chat.responseText;
					}
				}
			}
			ajax_chat.send(null);
		}
	}
}

function verificaMensagensChat() {

	ajax_verifica_mensagens = Ajax();
	if(ajax_verifica_mensagens){
		pagina = "chat/chat_mensagens.php";
		ajax_verifica_mensagens.open("GET", pagina, true);
		ajax_verifica_mensagens.onreadystatechange = function() {
			if(ajax_verifica_mensagens.readyState == 4){
				if(ajax_verifica_mensagens.status == 200){
					if (ajax_verifica_mensagens.responseText != "") {	
					
						// IN�CIO C�DIGO RICARDO
						chatTimer = chatTimerMin;   
						chatTimerCount = 1;  
						// FIM C�DIGO RICARDO
					
						//alert(ajax_verifica_mensagens.responseText);
						abrirChat('0');
						
						
						// atualizarMensagens();

						// INI - Extra�do da fun��o atualizarMensagens() para evitar redund�ncia de requis�es de p�gina
						// Popula a div com as mensagens mais recentes
						if (document.getElementById('div_chat').innerHTML != "") {
							document.getElementById('div_mensagens').innerHTML = ajax_verifica_mensagens.responseText;
						}
						// Rolar a barra quando houverem novas mensagens que estourem o limite de altura da div
						var objDiv = document.getElementById('div_mensagens');
						if (objDiv != null)
							objDiv.scrollTop = objDiv.scrollHeight;
						
						// Exibir a div caso existam mensagens novas ou que ainda n�o foram lidas
						if (document.getElementById('div_mensagens') != null && document.getElementById('div_mensagens').innerHTML != "") {
							document.getElementById('div_chat').style.display = "block";
						}
						// FIM - Extra�do da fun��o atualizarMensagens()
							
						
					}
					else {
						//document.getElementById('div_chat').style.display='none';
						
						// IN�CIO C�DIGO RICARDO
						chatTimerCount++;
						// FIM C�DIGO RICARDO
						
					}
				}
			}
		}
		ajax_verifica_mensagens.send(null);
	}
	
	if (chatTimerCount >= 10) {
		chatTimer *= 2;
		chatTimerCount = 1;
		if (chatTimer > chatTimerMax) {
			chatTimer = chatTimerMax;
		}
	}
	
	//alert("Timer:"+ chatTimer);
	
	// Chama novamente depois de N segundos
	setTimeout('verificaMensagensChat()', chatTimer);

}


function fecharChat(id_remetente, id_destinatario, mensagem) {

	ajax_fechamento = Ajax();
	if(ajax_fechamento){
		pagina = "chat/chat_fechamento.php";
		var params = "id_remetente="+id_remetente+"&id_destinatario="+id_destinatario+"&mensagem="+mensagem;
		ajax_fechamento.open("POST", pagina, true);
		ajax_fechamento.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajax_fechamento.send(params);
	}

	chatTimer = chatTimerMax;
	document.getElementById('div_chat').style.display='none';
}

function naoDisponivel(flag) {
	ajax_disponivel = Ajax();
	if(ajax_disponivel){
		pagina = "chat/chat_disponivel.php";
		var params = "flag="+flag;
		ajax_disponivel.open("POST", pagina, true);
		ajax_disponivel.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		ajax_disponivel.send(params);
	}
	//document.location.href="index.php";

}