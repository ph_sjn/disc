<?php 
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);


// Carregar cabeçalho do sistema
include_once "lib/cabecalho.php"; 

// Verifica a sessão
if ( isset($_SESSION) && session_name() != "ExtraNet_disc2work_client" ) { 
	session_destroy();
 	header("Location:".$link_extranet ); 
}

	// Se a sessão do usuário estiver ativa, permitir acesso interno do sistema
	// Senão, pedir para fazer login
	if ($user_kurz) {	

		// Carregar o menu
		//include_once "lib/menu.php";
		
		// Verificar tipo de módulo a ser carregado
		// @pasta: módulos genéricos
		// @module: módulos específicos com pastas de nomes próprios
		if ($pasta) {
			$inc_modulo = $pasta;
		}
		else {
			if (!$module) {
				$module = "summary";
			}
			$inc_modulo = $module;
		}
		
		// Início da div de delimitação do miolo
		//include_once "lib/inc/topo_conteudo.php";
		
		// Bloqueia o acesso as páginas do tipo "sem acesso" para o usuário - função se encontra na lib.inc.php
		if ((checaAcessoRestrito($module, $user_access, $inc_modulo) == true) || ($user_kurz == "suporte")) { 	
		
			// Carregar módulo
			//include_once $inc_modulo ."/" .$inc_modulo .".php";
			
		} else { ?>

			<br><br><br><br>
			<div style="text-align:center; font-size: 14px; color: blue">P�gina indispon�vel para seu usu�rio.
				<br><br>
				Favor entrar em contato com o administrador caso precise de acesso no endere�o indicado.
				<br><br>
				Suporte Extra|Net 
			</div>
			<br><br><br><br> <?
		}
?>	



<!-- Chart.bundle.js versao 2.5.0-->
<script src="lib/js/chart/Chart.bundle.js"></script>
<link rel="stylesheet" href="lib/js/jquery/themes/extranet/jquery.ui.all.css">

<script src='dsm_projetos/js/ajax.js' type='text/javascript'></script>
<script src='dsm_projetos/js/projetos_analise.js' type='text/javascript'></script>
<script src='../../survey/js/dssurvey_graph.js' type='text/javascript'></script>

<?php
$acesso_administrativo = checaPermissao($module,"A",$user_access); 
?>

<?php
//$DEFWEB_cliente = $_POST['filtroCliente'];
$DEFWEB_cliente = 9;
?>

<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<tr>
	<td style="vertical-align:middle;">
		<!--<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms"><span style="font-weight:bold;">NOVA PESQUISA</span></a>-->
	</td>
</tr>
<tr>
	<td style="text-align:right">
	
		<form action='index.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
		  <!--
				Cliente: 
				<select name='filtroCliente' class="combo">
				<option value=''>Todos</option>
				<?
				$sql = "SELECT id_cliente, ds_cliente FROM tbdw_cliente ORDER BY ds_cliente ASC";
				$resultCliente = db_query($sql) or db_die();
				while ($rowCliente = db_fetch_array($resultCliente) ){
					if ($DEFWEB_cliente == $rowCliente['id_cliente'])
						$selected = "selected";
					else
						$selected = "";
					echo "<option value='". $rowCliente['id_cliente'] ."' $selected>". $rowCliente['ds_cliente'] ."</option> " ;
				}
				?>
				</select>
			-->
			</td>
			<td style="vertical-align:middle;">
				<!--<input type='Submit' name="filtrarCliente" value='Filtrar' class="botao">-->
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>

<br>

<table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
    <tr >
        <td>


            <?
            $sql = "SELECT";
            $sql .= " av.id_avaliacao AS id_avaliacao";
            $sql .= ", pj.nm_projeto AS nm_projeto";
            $sql .= ", cl.ds_cliente AS ds_cliente";
            $sql .= ", av.ds_titulo AS ds_titulo";
            $sql .= ", av.cd_sit_avaliacao AS cd_sit_avaliacao";
			$sql .= ", sit.ds_sit_avaliacao AS ds_sit_avaliacao";
			$sql .= ", av.dt_inicio AS dt_inicio_avaliacao";
			$sql .= ", av.dt_fim AS dt_fim_avaliacao";
			$sql .= ", av.nmarq_topo_interno AS IMGLogoID";
			$sql .= ", av.nmarq_topo_original AS IMGLogoName";
            $sql .= ", pgadm.cd_pagina AS pgadm_cd_pagina";
            $sql .= ", pgadm.ds_pagina AS pgadm_ds_pagina";
            $sql .= ", pgadm.cd_tipo_pagina AS pgadm_cd_tipo";
            $sql .= ", pgadm.dv_include AS pgadm_dv_include";
            $sql .= " FROM tbdw_avaliacao AS av";
            $sql .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente";
            $sql .= " LEFT JOIN tbdw_sit_avaliacao AS sit ON sit.cd_sit_avaliacao = av.cd_sit_avaliacao";
            $sql .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
			$sql .= " LEFT JOIN tbdw_pagina AS pgadm ON pgadm.id_pagina = av.id_pagina_reladmin";
            if ($DEFWEB_cliente != "") {
                $sql .= " WHERE av.id_cliente = " . $DEFWEB_cliente;
				//$sql .= " AND av.id_avaliacao = 15";
				$sql .= " AND av.id_avaliacao = 8";
            }
            $sql .= " ORDER BY ds_cliente, nm_projeto, ds_titulo";
            $resultSet = db_query($sql);
            $total_pesquisas = db_num_rows($resultSet);
			//
			//echo "total_pesquisas:[$total_pesquisas]<br>";
			//
            $i = 1;
            while ($rsDados = db_fetch_array($resultSet)) {
				//echo "cd_sit_avaliacao:[" . $rsDados["cd_sit_avaliacao"] . "]<br>";
				//echo "IMGLogoID:[" . $rsDados["IMGLogoID"] . "]<br>";
				//echo "IMGLogoName:[" . $rsDados["IMGLogoName"] . "]<br>";
				/*
				echo "<pre>";
				print_r($rsDados);
				echo "</pre>";
				*/
                ?>
                <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
					<tr width="100%">
                        <td bgcolor="<?= $bgcolor4 ?>">
                            <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                <tr>
                                    <td><img id="imagem_<?= $i ?>" src="img/open.gif" border="0">&nbsp;
                                        <b><?= $rsDados["nm_projeto"] . " : " . $rsDados["ds_titulo"] ?></b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding: 0px;" id="tr_<?= $i ?>" bgcolor="<?= $bgcolor6 ?>">
                        <td style="padding: 0px;">
							<div id="div_<?= $module ?>_<?= $i ?>">
<?php 
$DW_id_cliente = $DEFWEB_cliente;
//$DW_id_avaliacao = 15;
$DW_id_avaliacao = 8;

$GRAFICO_EXIBIR = 1;

include_once("../survey/lib/libdisc.php");

// Biblioteca de classes e funções para aplicação do questionário
include_once("../survey/lib/libdisc_survey.php");

// Biblioteca de classes e funções do DefWebExpress
include_once("../survey/lib/libdisc_analise.php");

$objDEFWEBmasterAnalise = new DEFWEBmasterAnalise($DW_id_avaliacao);

$objDEFWEBmasterAnalise->obterFranqueados($DW_id_avaliacao);
/*
echo("<pre>");
print_r($objDEFWEBmasterAnalise);
echo("</pre>");
*/
?>							
<center>
    <table width="80%">
		<tr>
            <td colspan="3">
                <br>
					<b>MATRIZ DISC DOS ENTREVISTADOS</b><br>
                <br>
            </td>
		</tr>
		<tr>
			<td width="50%">
				ordenar por: 
				<input type="radio" name="optOrder" value="0" checked> <b>Entrevistado</b>
				 ou por: 
				<input type="radio" name="optOrder" value="1"> <b>Resultado</b>
			</td>
			<td width="15%">
				<input type='button' name="cmdOpenRptAll" value='Abrir todos' title="Abrir todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
			<td width="15%">
				<input type='button' name="cmdCloseRptAll" value='Fechar todos' title="Fechar todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
        </tr>
        <tr>
            <td colspan="3">
						
                <?php
				//
				// Cria matriz da pesquisa
				$discPesquisa = array(
						"D" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"I" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"S" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"C" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					)
				);
				$nEntrevistados  = 0;
				//
                $bPrimeiraLinha = TRUE;
                foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
                    //
                    unset($objDWsurvey);
                    $objDWsurvey = new DEFWEBsurvey($DW_id_avaliacao, $objFranqueado->Id);
                    if ($objDWsurvey->qt_nao_respondidas == 0) {
						//
						// Contar os entrevistados
						$nEntrevistados++;
						//
						// Acumular os pontos M e L do disc dos entrevistados
						$discPesquisa["D"]["M"] += $objDWsurvey->colDISC["D"]["M"];
						$discPesquisa["D"]["L"] += $objDWsurvey->colDISC["D"]["L"];
						$discPesquisa["I"]["M"] += $objDWsurvey->colDISC["I"]["M"];
						$discPesquisa["I"]["L"] += $objDWsurvey->colDISC["I"]["L"];
						$discPesquisa["S"]["M"] += $objDWsurvey->colDISC["S"]["M"];
						$discPesquisa["S"]["L"] += $objDWsurvey->colDISC["S"]["L"];
						$discPesquisa["C"]["M"] += $objDWsurvey->colDISC["C"]["M"];
						$discPesquisa["C"]["L"] += $objDWsurvey->colDISC["C"]["L"];
						//
						$objDiscNormalizado = new DEFWEBdisc_normalizado();
						$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
						//
						// Acumular os pontos A normalizados dos entrevistados
						$discPesquisa["D"]["A"] += $objDiscNormalizado->colDISC_convertido["D"];
						$discPesquisa["I"]["A"] += $objDiscNormalizado->colDISC_convertido["I"];
						$discPesquisa["S"]["A"] += $objDiscNormalizado->colDISC_convertido["S"];
						$discPesquisa["C"]["A"] += $objDiscNormalizado->colDISC_convertido["C"];
						//
						if ($bPrimeiraLinha) {
                            $bPrimeiraLinha = FALSE;
							echo "<table id=\"tblDadosEntrevistados\" width='100%'>";
						}
                        ?>
			<tr>
				<td>
            <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?php echo($bgcolor5); ?>' align='center'>
			<!--<table class="tlb_pesquisa">-->
                <tr width="100%">
                    <td bgcolor="<?php echo($bgcolor4); ?>">
							
						<table width="100%" cellpadding="0" cellspacing="0" border=0>
						<!--<table class="tlb_entrevistado">-->
                            <tr>
                                <td width="50%" onclick="javascript:entrevistado('imgEntrv_<?= $objFranqueado->Id ?>', 'trEntrv_<?= $objFranqueado->Id ?>');" onmouseover="this.style.cursor = 'pointer'">
									<img id="imgEntrv_<?php echo($objFranqueado->Id);?>" src="img/close.gif" border="0">&nbsp;
                                    <span class="td_entrv_nome">
                                    <?php
                                        echo($objFranqueado->Nome);
                                        if ($objFranqueado->Unidade != "") {
                                            echo("<br>(" . $objFranqueado->Unidade . ")");
                                        }
                                    ?>
									</span>
                                </td>
								<td width="20%">
									<?php
										$sLetters   = $objDiscNormalizado->resultado;
										$result_fmt = "";
										for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
											if ( $nLetter == 0 ) {
												$result_fmt = "<span class=\"disc_letter_main\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											} else {
												$result_fmt .= "<span class=\"disc_letter_norm\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											}
										}
										if ( strlen($sLetters) > 0 ) {
											echo($result_fmt);
										}
									?>
								</td>
								<td width="10%">
									<?php
										// BOTAO DO RELATORIO DO ENTREVISTADO
										$sLink = " onclick=\"javascript:window.open('http://www.disc2work.com.br/survey/dssurvey_redir.php?etapa=6&id_h=" . $objFranqueado->Hash . "');\" ";
									?>
									<input type='button' name="cmdOpenRpt_<?php echo($objFranqueado->Id);?>" value='Relat�rio' title="Abrir relat�rio." class="botao" onmouseover="this.style.cursor='pointer';" <?php echo($sLink);?>>
								</td>
								<td width="20%" class="td_entrv_dt_termino">
									<?php
										$dt_termino_token = new DateTime($objDWsurvey->dt_termino_token);
										echo($dt_termino_token->format('d/m/Y H:i:s'));
									?>
								</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display:none; padding: 0px;" id="trEntrv_<?php echo($objFranqueado->Id);?>" bgcolor="<?php echo($bgcolor6); ?>">
                    <td style="padding: 0px;">						
						
						
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <td>Resultado</td>
                                    <?php
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">M</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">L</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">A</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["A"]); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAn" . $objFranqueado->Id . "' value='". $objDiscNormalizado->serie ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAn" . $objFranqueado->Id . "' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
                                </tr>
							</tbody>
						</table>
						
					</td>
				</tr>
			</table>
					</td>
				</tr>
                <?php
                    }
                }
                if ($bPrimeiraLinha) {
                    echo("NENHUM ENTREVISTADO RESPONDEU AO QUESTIONÁRIO!");
                } else {
					echo "</table>";
				}
                ?>
            </td>
        </tr>
		<?php 
		if ( $nEntrevistados > 0 ){
			//
			// Transformando o acumulado em media da pesquisa
			$discPesquisa["D"]["M"] = intval(round(($discPesquisa["D"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["L"] = intval(round(($discPesquisa["D"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["M"] = intval(round(($discPesquisa["I"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["L"] = intval(round(($discPesquisa["I"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["M"] = intval(round(($discPesquisa["S"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["L"] = intval(round(($discPesquisa["S"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["M"] = intval(round(($discPesquisa["C"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["L"] = intval(round(($discPesquisa["C"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["A"] = intval(round(($discPesquisa["D"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["A"] = intval(round(($discPesquisa["I"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["A"] = intval(round(($discPesquisa["S"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["A"] = intval(round(($discPesquisa["C"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			// Monta a serie do grafico normalizado
			$discPesqSerieNormalizada = "[" . $discPesquisa["D"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["I"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["S"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["C"]["A"];
			$discPesqSerieNormalizada .= "]";
			//
			// Calculando o A da pesquisa para obter o resultado
			$discPesquisa["D"]["A"] = ($discPesquisa["D"]["M"]-$discPesquisa["D"]["L"]);
			$discPesquisa["I"]["A"] = ($discPesquisa["I"]["M"]-$discPesquisa["I"]["L"]);
			$discPesquisa["S"]["A"] = ($discPesquisa["S"]["M"]-$discPesquisa["S"]["L"]);
			$discPesquisa["C"]["A"] = ($discPesquisa["C"]["M"]-$discPesquisa["C"]["L"]);
			// Cria o objeto Normalizador
			$discPesqNormalizado = new DEFWEBdisc_normalizado();
			$discPesqNormalizado->normalizar_disc($discPesquisa, "");
			// Obtem o resultado da pesquisa
			$sLetters   = $discPesqNormalizado->resultado;
			$result_fmt = "";
			for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
				if ( $nLetter == 0 ) {
					$result_fmt = "<span class=\"disc_letter_main\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				} else {
					$result_fmt .= "<span class=\"disc_letter_norm\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				}
			}
			?>
			<tr>
				<td>
					<br>
						<b>MATRIZ DISC DA PESQUISA</b><br>
					<br>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">
			
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
									<td>
									<?php
									if ( $result_fmt != "" ) {
										echo($result_fmt);
                                    } else {
										echo("Resultado");
									}
									?>
									</td>
									<?php
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
									<td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">M</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">L</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">A</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["A"]); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAnPesq_$DW_id_avaliacao' value='". $discPesqSerieNormalizada ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAnPesq_$DW_id_avaliacao' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
									<td>
									<?php
										// BOTAO DO RELATORIO DO ENTREVISTADO
										$sLink = " onclick=\"javascript:window.open('http://www.disc2work.com.br/survey/dssurvey_redir.php?etapa=7&id_cliente=" . $DEFWEB_cliente . "&id_avaliacao=" . $DW_id_avaliacao . "');\" ";
									?>
										<input type='button' name="cmdOpenRptPsq_<?php echo($objFranqueado->Id);?>" value='Relat�rio' title="Abrir relat�rio." class="botao" onmouseover="this.style.cursor='pointer';" <?php echo($sLink);?>>
									</td>
                                </tr>
							</tbody>
						</table>
			
			</td>
		</tr>
		<script>
			jQuery('document').ready( function(){
				configBtnEntrevistas();
				montarGraficosAnalise();
			});
			//
			jQuery('input[type=radio]').on('change', function() {
				var optOrder = jQuery('input[name=optOrder]:checked').val();
				if (/(0|1)/.test(optOrder)){
					sortTable(optOrder);
				}
			});
			//
			function sortTable(n) {
				var rows = jQuery('#tblDadosEntrevistados > tbody > tr').get();
				//
				rows.sort(function(a, b) {
					var A = getVal(a);
					var B = getVal(b);
					if(A < B) {
						return -1;
					}
					if(A > B) {
						return 1;
					}
					return 0;
				});
				//
				function getVal(tr){
					var tr2 = jQuery('table > tbody > tr:eq(0)', tr);
					var tr3 = jQuery('table > tbody > tr:eq(0)', tr2);
					var td  = jQuery('td:eq(' + n + ')', tr3);
					var spn = jQuery('span', td);
					var tx  = jQuery.trim(spn.text().toUpperCase());
					if ( n == 1 ){
						// Se for o resultado
						tx = tx.replace("C", "X");
					}
					return tx;
				}
				//
				jQuery.each(rows, function(index, row) {
					jQuery('#tblDadosEntrevistados').children('tbody').append(row);
				});
				//
			}
		</script>
		<?php 
		}
		?>
    </table>
    <br>
    <table width="80%">
        <tr>
            <td>
                <br>
                <b>ENTREVISTADOS QUE N�O RESPONDERAM AO QUESTION�RIO</b><br>
                <br>
            </td>   
        </tr>
        <tr>
            <td>
                <?php
                $bPrimeiraLinha = TRUE;
                //
                foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
                    //
                    unset($objDWsurvey);
                    $objDWsurvey = new DEFWEBsurvey($DW_id_avaliacao, $objFranqueado->Id);
                    //
                    // Se não respondeu nenhuma pergunta, ou,
                    // se ainda não respondeu todas as perguntas (em andamento)...
                    if(($objDWsurvey->qt_respondidas == 0)||($objDWsurvey->qt_respondidas > 0 && $objDWsurvey->qt_nao_respondidas > 0)) {
                        // Se for a primeira linha da tabela...
                        if ($bPrimeiraLinha) {
                            // Desliga o flag e monta
                            $bPrimeiraLinha = FALSE;
                            // Abre a tabela e monta o cabeçalho
                            ?>
				<table style="line-height: 25px;" cellspacing="0">
					<thead style="font-weight: bold;">
						<tr>
							<td>
								Entrevistado
							</td>
                            <td>
                                Situa��o
							</td>
                        </tr>
                    </thead>
                    <tbody>
						<?php 
						}
						?>
                        <tr>
                            <td bgcolor='#EEEEEE' style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                <?php
                                echo($objFranqueado->Nome);
                                if ($objFranqueado->Unidade != "") {
                                    echo("<br>(" . $objFranqueado->Unidade . ")");
                                }
                                ?>
                            </td>
                    <?php
                        // Se não respondeu nenhuma pergunta...
                        if ($objDWsurvey->qt_respondidas == 0) {
                    ?>
                            <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                Ainda n�o respondeu ao question�rio.
                            </td>
                    <?php
                    } else {
                    ?>
                            <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                Entrevista em andamento.
                            </td>
                    <?php
                    }
                    ?>
                        </tr>
                <?php
					}
                }
                // Se já desligou o indicador de primeira linha...
                if (!$bPrimeiraLinha) {
                    // Fecha a tabela
                ?>
                    </tbody>
                </table>
                    <?php
                } else {
                    echo("TODOS OS ENTREVISTADOS RESPONDERAM AO QUESTION�RIO!<br>");
                }
                ?>
                <br>
            </td>
        </tr>
    </table>

</center>
<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />
							
							
							</div>
						</td>
                    </tr>
                </table>
                <br>
                <? $i++; ?>	<? }
            ?>


        </td>
    </tr>
</table>
<br><br>

	<div style="text-align: center; font-weight: bold;">Total de pesquisas encontradas: <span style="color:#FF0000"><?= $total_pesquisas ?></span> </div>
	<?php 
				echo "Query:<br>";
                $sSQL = "SELECT DISTINCT";
                $sSQL .= " av.id_cliente";
				//$sSQL .= ", av.id_avaliacao";
                //$sSQL .= ", av.ds_titulo";
                $sSQL .= ", cl.cd_cliente";
                $sSQL .= ", cl.ds_cliente";
                $sSQL .= ", token.ds_nome AS Nome_Entrev";
                $sSQL .= ", token.ds_unidade AS Unidade_Entrev";
                $sSQL .= ", token.ds_email AS Email_Entrev";
				$sSQL .= ", token.dv_sexo AS Sexo_Entrev";
				$sSQL .= ", DATE_FORMAT(token.dt_nascimento, '%d/%m/%Y') AS Dt_Nasc_Entrev";
				$sSQL .= ", token.nr_cep AS CEP_Entrev";
				$sSQL .= ", token.nr_telefone AS Fone_Entrev";
                $sSQL .= " FROM tbdw_avaliacao AS av";
                $sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = av.id_avaliacao AND token.id_cliente = av.id_cliente)";
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente ";
                $sSQL .= " WHERE av.id_cliente = 9";
                //$sSQL .= " ORDER BY av.id_avaliacao, token.ds_nome";
				$sSQL .= " ORDER BY token.ds_nome";
				echo "$sSQL<br>";
				$resultSet = db_query($sSQL);
				$total_pesquisas = db_num_rows($resultSet);
				echo "total_pesquisas: $total_pesquisas<br>";
				$count = 0;
				while ($rsQuery = db_fetch_array($resultSet) ){
					echo "<pre>";
					print_r($rsQuery);
					echo "</pre>";	
					$count++;
					if ( $count >= 3 ){
						break;
					}
				}
	?>

<br><br>

<?php 		
		// Fim da delimitação do miolo
		include_once "lib/inc/rodape_conteudo.php";
		
	} else {

		// Setar módulo de login
		$module = "login";
		
		// Carregar página de login
		include_once "login/login.php";
		
	}

// Carregar rodapé do sistema
include_once "lib/rodape.php";

?>