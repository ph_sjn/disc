<?
include_once("/home/include_dados/session.inc.php");


header("Content-Type: text/html; charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php");

if ( $_SESSION['user_kurz'] != "" ) { 

	if ( isset($_POST['id_unidade']) ) { 
		$sql = "SELECT * FROM firm WHERE id = ". $_POST['id_unidade'];
		$consultaDadosUnidade = db_query($sql);
		$row = db_fetch_array($consultaDadosUnidade);
	}

	?>
	<table>
	<input type="hidden" name="id_unidade" id="id_unidade" value="<?= $_POST['id_unidade'] ?>">

	<tr id="tr_cnpj">
		<td style="text-align:right; vertical-align:middle; font-weight: bold; width: 120px;">CNPJ: </td>
		<td><input type='text' class='texto' name="cnpj" size=20 maxlength=30 value="<?= $row['cnpj'] ?>"></td>
	</tr>

	<tr id="tr_tipo_logradouro">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Tipo logradouro: </td>
		<td>
				<?
				$sql = "SELECT id, logradouro FROM tipo_logradouros ORDER BY logradouro";
				$consultaLogradouros = db_query($sql);
				?>
				<select name="tipo_logradouro" id="tipo_logradouro" class="combo">
					<option value="">---- Selecione o logradouro ----</option>
					<? while ($rowLogradouro = db_fetch_array($consultaLogradouros) ) { ?>
						<option value="<?= $rowLogradouro['logradouro'] ?>" <? if ($rowLogradouro['logradouro'] == $row['tipo_logradouro'] ) { ?> selected<? } ?>><?= $rowLogradouro['logradouro'] ?></option>
					<? } ?>
				</select>
		</td>
	</tr>
	<tr id="tr_logradouro">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Logradouro: </td>
		<td><input type='text' class='texto' name="logradouro" size=40 maxlength=100 value="<?= $row['logradouro'] ?>"></td>
	</tr>
	<tr id="tr_numero">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">N�mero: </td>
		<td><input type='text' class='texto' name="numero" size=15 maxlength=100 value="<?= $row['numero'] ?>"></td>
	</tr>
	<tr id="tr_complemento">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Complemento: </td>
		<td><input type='text' class='texto' name="complemento" size=20 maxlength=100 value="<?= $row['complemento'] ?>"></td>
	</tr>
	<tr id="tr_bairro">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Bairro: </td>
		<td><input type='text' class='texto' name="bairro" size=40 maxlength=100 value="<?= $row['bairro'] ?>"></td>
	</tr>
	<tr id="tr_cep">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">CEP: </td>
		<td><input type='text' class='texto' name="cep" size=10 maxlength=10 value="<?= $row['cep'] ?>"></td>
	</tr>

	<tr id="tr_uf">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">UF: </td>
		<td>						
			
			<? 
			$sql = "SELECT id, nome, uf FROM estado";
			$consulta_estados = db_query($sql);
			?>
			<select name="uf" id="uf" onchange="atualizaCidades(this.value)" class="combo">
				<option value="">---- Selecione a UF ----</option>
				<? while ($tupla_estados = db_fetch_array($consulta_estados) ) { ?>
					<option value="<?= $tupla_estados[2] ?>" <? if ($tupla_estados[2] == $row['uf'] ) { ?> selected<? } ?>><?= $tupla_estados[2] ?></option>
				<? } ?>
			</select>
			
		
		</td>
	</tr>

	<tr id="tr_cidade">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Cidade: </td>
		<td>					
			<div id="div_cidades">
			<select name="cidade" id="cidade" class="combo">
				<option value="">---- Selecione a UF ----</option>
				<?
				$sql_cidades = "SELECT c.id, c.nome FROM cidade c, estado e WHERE c.estado = e.id AND e.uf = '". $row['uf'] ."' ";
				$consulta_cidades = db_query($sql_cidades);
				while ($item_opcao2 = db_fetch_row($consulta_cidades)) {?>
					<option value="<?= $item_opcao2[1] ?>" <? if ($item_opcao2[1] == $row['cidade']) { ?> selected <? } ?>><?= $item_opcao2[1] ?></option><? 
				}
				?>
				</select>
			</div>
			<? if ($pers_ID) { ?>
				<script language="javascript">atualizaCidades(document.getElementById("radio").value, "<?= $row['cidade'] ?>");</script>
			<? } ?>
		
		</td>
	</tr>
	<tr id="tr_latitude">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Latitude: </td>
		<td><input type='text' class='texto' name="latitude" size=20 maxlength=50 value="<?= $row['latitude'] ?>"></td>
	</tr>
	<tr id="tr_longitude">
		<td style="text-align:right; vertical-align:middle; font-weight: bold;">Longitude: </td>
		<td><input type='text' class='texto' name="longitude" size=20 maxlength=50 value="<?= $row['longitude'] ?>"></td>
	</tr>
	<tr bgcolor="<?= $bgcolor6 ?>">
		<td></td>
		<td  style="text-align:right;" ><input type='submit' name='salvar_unidade' id="salvar_unidade" value='Salvar' class="botao"></td>
	</tr>
	</table>

<? } ?>