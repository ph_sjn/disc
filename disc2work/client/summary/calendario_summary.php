<?
session_name("ExtraNet_disc2work_client");
session_start();
$path_pre = "../";

header("Content-Type: text/html;  charset=ISO-8859-1",true);
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php");
?>

<script type='text/javascript' src="summary/js/calendar.js"></script>

<script type='text/javascript'>
function checkTag(TAG,ID,OBG) {		
	var x = document.getElementsByTagName(TAG);

	if(OBG != "") {			
		document.getElementById(OBG).className = "HIDDEN";			
	} else {
		for (var i = 0;i < x.length; i++) {						
			if((x[i].id == ID) && (x[i].className == "HIDDEN")) {
				document.getElementById(ID).className = "VISIBLE";
			}									
			if((x[i].id != ID) && (x[i].className == "VISIBLE")) {
				document.getElementById(x[i].id).className = "HIDDEN";
			}								
		}							
	}		
}
</script>

<div id="calback">
	<div id="calendar" style="margin-top:20px;margin-bottom:20px">
		<? 
		
		$output = '';
		$month = $_GET['month'];
		$year = $_GET['year'];
			
		if($month == '' && $year == '') { 
			$time = time();
			$month = date('n',$time);
		    $year = date('Y',$time);
		}
		
		$date = getdate(mktime(0,0,0,$month,1,$year));
		$today = getdate();
		$hours = $today['hours'];
		$mins = $today['minutes'];
		$secs = $today['seconds'];
		
		if(strlen($hours)<2) $hours="0".$hours;
		if(strlen($mins)<2) $mins="0".$mins;
		if(strlen($secs)<2) $secs="0".$secs;
		
		$days=date("t",mktime(0,0,0,$month,1,$year));
		$start = $date['wday']+1;
		
		switch($month) {
			case 1: case 01: $mes = "Janeiro";
			break;
			case 2: case 02: $mes = "Fevereiro";
			break;
			case 3: case 03: $mes = "Mar�o";
			break;
			case 4: case 04: $mes = "Abril";
			break;
			case 5: case 05: $mes = "Maio"; 
			break;
			case 6: case 06: $mes = "Junho"; 
			break;
			case 7: case 07: $mes = "Julho";  
			break;
			case 8: case 08: $mes = "Agosto"; 
			break;
			case 9: case 09: $mes = "Setembro";    
			break;
			case 10: $mes = "Outubro";    
			break;
			case 11: $mes = "Novembro";    
			break;
			case 12: $mes = "Dezembro";    
			break;    
		}
		
		$name = $date['month'];		
		$year2 = $date['year'];
		$offset = $days + $start - 1;
		 
		if($month==12) { 
			$next=1; 
			$nexty=$year + 1; 
		} else { 
			$next=$month + 1; 
			$nexty=$year; 
		}
		
		if($month==1) { 
			$prev=12; 
			$prevy=$year - 1; 
		} else { 
			$prev=$month - 1; 
			$prevy=$year; 
		}
		
		if($offset <= 28) $weeks=28; 
		elseif($offset > 35) $weeks = 42; 
		else $weeks = 35; 
		
		$output .= "
		<table class='cal' cellspacing='1'>
		<tr>
			<td colspan='7'>
				<table class='calhead'>
				<tr>
					<td>
						<a href='javascript:navigate($prev,$prevy)'><img src='summary/img/calLeft.gif'></a> <a href='javascript:navigate(\"\",\"\")'><img src='summary/img/calCenter.gif'></a> <a href='javascript:navigate($next,$nexty)'><img src='summary/img/calRight.gif'></a>
					</td>
					<td align='right'>
						<div>$mes $year2</div>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr class='dayhead'>
			<td>Dom</td>
			<td>Seg</td>
			<td>Ter</td>
			<td>Qua</td>
			<td>Qui</td>
			<td>Sex</td>
			<td>Sab</td>
		</tr>";
		
		$col=1;
		$cur=1;
		$next=0;
		
		$total = 0;
		
		for($i=1;$i<=$weeks;$i++) { 
			if($next==3) $next=0;
			if($col==1) $output.="<tr class='dayrow'>";
			
			$sqlData   = "SELECT titulo FROM calendario WHERE DAYOFMONTH(data) = $cur AND MONTH(data) = $month AND YEAR(data) = $year";
			$queryData = db_query($sqlData);
			$total     = db_num_rows($queryData);
			$rowsData  = db_fetch_row($queryData);
			if ($total > 0) {
				$style = "style='background: #999999;'";
				$onMouseOver = "onMouseOver = \"this.style.cursor='pointer';checkTag('DIV','".$cur."','');\"";
				$onMouseOut = "onMouseOut = \"checkTag('DIV','".$cur."','".$cur."');\"";
				$onclick = "onclick=\"document.location.href='index.php?module=calendario&mode=detalhes&data=$year-$month-$cur&mes=$month&dia=$cur&ano=$year'\"";
			}
			else {
				$style = "";
				$onMouseOver = "";
				$onMouseOut = "";

			}
		  	
			$output.="<td valign='top'  $onMouseOver $onMouseOut $style $onclick>";
		
			if($i <= ($days+($start-1)) && $i >= $start) {
				
				$output.="<div class=\"HIDDEN\" id=".$cur.">".$rowsData[0]."</div>";
				$output.="<div class='day'><a href='index.php?module=calendario&mode=detalhes&data=$year-$month-$cur&mes=$month&dia=$cur&ano=$year'><b";
				
				if(($cur==$today[mday]) && ($name==$today[month])) $output.=" style='color:#C00'";
		
				$output.=">$cur</b></a></div></td>";
		
				$cur++; 
				$col++; 
				
			} else { 
				$output.="&nbsp;</td>"; 
				$col++; 
			}  
			    
		    if($col==8) { 
			    $output.="</tr>"; 
			    $col=1; 
		    }
		}
		
		$output.="</table>";
		  
		echo $output;
		
		?>
	</div>
</div>