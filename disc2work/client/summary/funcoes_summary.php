<script language="javascript">
function visibilidadeLinha(id) {
	linha = document.getElementById('linha_'+id);
	var browser = "NAVEGADOR" + navigator.userAgent;
	if (browser.indexOf("Firefox") > 0) {
		if (linha.style.display == "none") {
			linha.style.display = "table-row";
		}
		else {
			linha.style.display = "none";
		}
	}
	else {
		if (linha.style.display == "none") {
			linha.style.display = "block";
		}
		else {
			linha.style.display = "none";
		}
	}
}
</script>

<?
function listaItensArquivos($modulo,$user_kurz,$user_ID, $bgcolor4, $bgcolor6, $tdelements) {
	$nr = 0;


	$sql = " SELECT * FROM ";
	$sql .= " ( ";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ." m, ". $modulo ."_item_acesso r WHERE ";
	$sql .= " m.id = r.id_file AND r.tipo_acesso = 'U' AND r.id_users = ". $user_ID ." AND r.acesso IS NULL";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " )";
	$sql .= " AS m ";
	$sql .= " GROUP BY m.ID ORDER BY m.ID desc LIMIT ". $tdelements;
	//echo $sql . "<br><br>";

	$result = db_query($sql) or db_die();
	$out = "<script src='summary/js/ajax.js' type='text/javascript'></script>";
	$out .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
	if (db_num_rows($result) > 0) {
		while ($row = db_fetch_row($result)) {
	        $mouseover = "onmouseover=\"this.style.backgroundColor='". $bgcolor4 ."';\"";
	        $mouseout = "onmouseout=\"this.style.backgroundColor='#fafafa';\"";
			$onclick = "onclick=\"document.location.href='arquivos/arquivos_down.php?module=". $modulo ."&nr=". $row[0] ."'\"";
			$out .= "<tr><td>";
			$out .= "<table width='100%' cellspacing='0' cellpadding='3' border='0'>";
			$out .= "<tr $mouseover $mouseout nowrap valign='middle'><td valign='middle' nowrap>";

			if ($row[10] == "t") {
				$out .= "<a href='index.php?mode=detalhes&module=". $modulo ."&ID=". $row[0] . "'>";
			}
			else {
				$out .= "<a href='arquivos/arquivos_down.php?module=". $modulo ."&nr=". $row[0] . "'>";
			}
			$out .= "". $row[2] ."</a>";
			$out .= "</td>";
			
			$out .= "<td align='right' nowrap>";
			
			//Conta o numero de mensagens no item
			$sql = "Select id, id_usuario from ". $modulo ."_mensagens where id_item = ". $row[0] ."";
			$msg = db_query($sql) or db_die();
			$row_mensagem = db_fetch_row($msg);
			$mensagem = db_num_rows($msg);
			
			$sql_nao_lidas = "SELECT id_users FROM ". $modulo ."_item_acesso WHERE id_file = ". $row[0];
			$sql_nao_lidas .= " AND id_users = ". $user_ID . " AND mensagem_pendente = 'S'";
			$result_nao_lidas = db_query($sql_nao_lidas) or db_die();
			$row_teste = db_fetch_row($result_nao_lidas);
			$row_nao_lidas = db_num_rows($result_nao_lidas);
			
			//Verifica se tem alguma mensagem,caso tenha mostra a imagem estatica, caso tenha nova mensagem mostra msg animada
			if ($row_nao_lidas) {
				$out .= "<a href='index.php?module=". $modulo . "&mode=detalhes&ID=". $row[0] ."'>";
				$out .= "<img src='img/msg_animado.gif' alt='Este item tem mensagens novas' border='0' title='Este item tem ". $row_confere[0] ." mensagens novas'/>";
				$out .= "</a>";
			}elseif ($mensagem > 0) {
				$out .= "<img src='img/msg.gif' alt='Este item tem mensagens' title='Este item tem mensagens'/>";
			}else {
				$out .= "<img src='img/t.gif'/>";
			}
			$out .= "</td>";
			
			$out .= "<td width='140' nowrap>";
	        $mouseover = "onmouseover=\"this.style.cursor='pointer'\"";
			$out .= "<img src='img/detalhe.gif' border=0 align='left' $mouseover  onclick=\"visibilidadeLinha('". $modulo ."_". $row[0] ."')\"> � ";
			$out .= dataMesPortugues($row[6]);
			$out .= "</td>";
			$out .= "</tr>";
			$out .= "</table>";

			$out .= "</td>";
			$out .= "</tr>";


			$out .= "<tr  id=\"linha_". $modulo ."_". $row[0] ."\" name=\"linha_". $modulo ."_". $row[0] ."\" style=\"display:none;\">";
			$out .= "<td>";

			$sql = " SELECT m2.filename, m.filesize, m.von, m.remark, DATE_FORMAT(m.datum,'%d/%m/%Y'), u.nachname, m.id, m.typ ";
			$sql .= "FROM ". $modulo . " m, ". $modulo . " m2, users u WHERE m.id = " . $row[0] . " AND u.id = m.von";
			$sql .= " AND m2.id = m.div1";
			$consulta_item = db_query($sql) or db_die();
			if (db_num_rows($consulta_item) > 0) {
				$item = db_fetch_row($consulta_item);

				$out .= "<table border='0' bgcolor='". $bgcolor5 ."' cellpadding='0' cellspacing='1' width='100%'>";
				$out .= "<tr><td>";

				$out .= "<table border='0' bgcolor='". $bgcolor4 ."' cellpadding='2' cellspacing='0' width='100%'>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "<td align='right' width='100'><b>Diret�rio:</b></td>";
				$out .= "<td width='130'>";
						if ($item[0] == "") 	$out .= "N�o especificada";
						else $out .= $item[0];
				$out .= "</td>";
				$out .= "<td align='right' width='100'><b>Data envio:</b></td>";
				$out .= "<td align='right'>";
				$out .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'><tr><td>";
				$out .=  $item[4];
				$out .=  "</td><td align='right'>";
				
				if ($item[7] != "t" && $item[7] != "l") {
					$out .= "<a href='arquivos/arquivos_down.php?module=". $modulo ." &nr=". $item[6] ."&alt_down=inline'>";
					$out .= "<img src='img/icone_download_alternativo.gif' border='0'>";
					$out .= "</a> ";
				}
			
				//Esta linha serve para contar o numero de mensagens e armazenar em uma variavel
				$sql = "Select id, id_usuario from ". $modulo ."_mensagens where id_item = ". $item[6];
				$msg = db_query($sql) or db_die();
				$mensagem = db_num_rows($msg);
				
				$out .= "<a href='index.php?module=". $modulo . "&mode=detalhes&ID=". $item[6] ."&msg=". $mensagem ."'>";
				$out .= "<img src='img/icone_detalhes.gif' border='0'>";
				$out .= "</a> ";
			
				if ($item[2] == $user_ID) {
					$out .= "<a href='index.php?module=". $modulo ."&mode=data&action=delete&ID=". $item[6] ."'>";
					$out .= "<img src='img/icone_excluir.gif' border='0'>";
					$out .= "</a> ";
				}
			
				$out .=  "</td></tr></table>";
				$out .= "</td>";
				$out .= "</tr>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "<td align='right'><b>Tamanho:</b></td>";
				$out .= "<td>". $item[1] ." bytes</td>";
				$out .= "<td align='right'><b>Respons�vel:</b></td>";
				$out .= "<td>". $item[5] ."</td>";
				$out .= "</tr>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "	<td align='right'><b>Coment�rios:</b></td>";
				$out .= "	<td colspan='3' align='left'>";
								if ($item[3] == "") 	$out .= "Sem coment�rios";
								else $out .= strip_tags(str_replace("\n","<br>",$item[3]), '<br><b></b><i></i><font></font>');
				$out .= "	</td>";
				$out .= "</tr>";
				$out .= "</table>";
				
				$out .= "</td></tr>";
				$out .= "</table>";
			}
			$out .=  "</td></tr>";

		}
	}
	else {
		$out .= "<tr><td width=100% colspan=2 align='center'>N�o existem novos itens a exibir.<br><br></td></tr>";
	}
	$out .= "</table>";
	return $out;
}


function listaItensArquivos3($modulo,$user_kurz,$user_ID, $bgcolor4, $bgcolor6, $tdelements) {





































































































	$nr = 0;


	$sql = " SELECT * FROM ";
	$sql .= " ( ";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ." m, ". $modulo ."_item_acesso r WHERE ";
	$sql .= " m.id = r.id_file AND r.tipo_acesso = 'U' AND r.id_users = ". $user_ID ." AND r.acesso IS NULL";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " )";
	$sql .= " AS m ";
	$sql .= " GROUP BY m.ID ORDER BY m.ID desc LIMIT ". $tdelements;
	//echo $sql . "<br><br>";

	$result = db_query($sql) or db_die();
	$out = "<script src='summary/js/ajax.js' type='text/javascript'></script>";
	$out .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
	if (db_num_rows($result) > 0) {
		while ($row = db_fetch_row($result)) {
	        $mouseover = "onmouseover=\"this.style.backgroundColor='". $bgcolor4 ."';\"";
	        $mouseout = "onmouseout=\"this.style.backgroundColor='#fafafa';\"";
			$onclick = "onclick=\"document.location.href='arquivos3/arquivos3_down.php?module=". $modulo ."&nr=". $row[0] ."'\"";
			$out .= "<tr><td>";
			$out .= "<table width='100%' cellspacing='0' cellpadding='3' border='0'>";
			$out .= "<tr $mouseover $mouseout nowrap valign='middle'><td valign='middle' nowrap>";

			if ($row[10] == "t") {
				$out .= "<a href='index.php?mode=detalhes&module=". $modulo ."&ID=". $row[0] . "'>";
			}
			else {
				$out .= "<a href='arquivos3/arquivos3_down.php?module=". $modulo ."&nr=". $row[0] . "'>";
			}
			$out .= "". $row[2] ."</a>";
			$out .= "</td>";
			
			$out .= "<td align='right' nowrap>";
			
			//Conta o numero de mensagens no item
			$sql = "Select id, id_usuario from ". $modulo ."_mensagens where id_item = ". $row[0] ."";
			$msg = db_query($sql) or db_die();
			$row_mensagem = db_fetch_row($msg);
			$mensagem = db_num_rows($msg);
			
			$sql_nao_lidas = "SELECT id_users FROM ". $modulo ."_item_acesso WHERE id_file = ". $row[0];
			$sql_nao_lidas .= " AND id_users = ". $user_ID . " AND mensagem_pendente = 'S'";
			$result_nao_lidas = db_query($sql_nao_lidas) or db_die();
			$row_teste = db_fetch_row($result_nao_lidas);
			$row_nao_lidas = db_num_rows($result_nao_lidas);
			
			//Verifica se tem alguma mensagem,caso tenha mostra a imagem estatica, caso tenha nova mensagem mostra msg animada
			if ($row_nao_lidas) {
				$out .= "<a href='index.php?module=". $modulo . "&mode=detalhes&ID=". $row[0] ."'>";
				$out .= "<img src='img/msg_animado.gif' alt='Este item tem mensagens novas' border='0' title='Este item tem ". $row_confere[0] ." mensagens novas'/>";
				$out .= "</a>";
			}elseif ($mensagem > 0) {
				$out .= "<img src='img/msg.gif' alt='Este item tem mensagens' title='Este item tem mensagens'/>";
			}else {
				$out .= "<img src='img/t.gif'/>";
			}
			$out .= "</td>";
			
			$out .= "<td width='140' nowrap>";
	        $mouseover = "onmouseover=\"this.style.cursor='pointer'\"";
			$out .= "<img src='img/detalhe.gif' border=0 align='left' $mouseover  onclick=\"visibilidadeLinha('". $modulo ."_". $row[0] ."')\"> � ";
			$out .= dataMesPortugues($row[6]);
			$out .= "</td>";
			$out .= "</tr>";
			$out .= "</table>";

			$out .= "</td>";
			$out .= "</tr>";


			$out .= "<tr  id=\"linha_". $modulo ."_". $row[0] ."\" name=\"linha_". $modulo ."_". $row[0] ."\" style=\"display:none;\">";
			$out .= "<td>";

			$sql = " SELECT m2.filename, m.filesize, m.von, m.remark, DATE_FORMAT(m.datum,'%d/%m/%Y'), u.nachname, m.id, m.typ ";
			$sql .= "FROM ". $modulo . " m, ". $modulo . " m2, users u WHERE m.id = " . $row[0] . " AND u.id = m.von";
			$sql .= " AND m2.id = m.div1";
			$consulta_item = db_query($sql) or db_die();
			if (db_num_rows($consulta_item) > 0) {
				$item = db_fetch_row($consulta_item);

				$out .= "<table border='0' bgcolor='". $bgcolor5 ."' cellpadding='0' cellspacing='1' width='100%'>";
				$out .= "<tr><td>";

				$out .= "<table border='0' bgcolor='". $bgcolor4 ."' cellpadding='2' cellspacing='0' width='100%'>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "<td align='right' width='100'><b>Diret�rio:</b></td>";
				$out .= "<td width='130'>";
						if ($item[0] == "") 	$out .= "N�o especificada";
						else $out .= $item[0];
				$out .= "</td>";
				$out .= "<td align='right' width='100'><b>Data envio:</b></td>";
				$out .= "<td align='right'>";
				$out .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'><tr><td>";
				$out .=  $item[4];
				$out .=  "</td><td align='right'>";
				
				if ($item[7] != "t" && $item[7] != "l") {
					$out .= "<a href='arquivos3/arquivos3_down.php?module=". $modulo ." &nr=". $item[6] ."&alt_down=inline'>";
					$out .= "<img src='img/icone_download_alternativo.gif' border='0'>";
					$out .= "</a> ";
				}
			
				//Esta linha serve para contar o numero de mensagens e armazenar em uma variavel
				$sql = "Select id, id_usuario from ". $modulo ."_mensagens where id_item = ". $item[6];
				$msg = db_query($sql) or db_die();
				$mensagem = db_num_rows($msg);
				
				$out .= "<a href='index.php?module=". $modulo . "&mode=detalhes&ID=". $item[6] ."&msg=". $mensagem ."'>";
				$out .= "<img src='img/icone_detalhes.gif' border='0'>";
				$out .= "</a> ";
			
				if ($item[2] == $user_ID) {
					$out .= "<a href='index.php?module=". $modulo ."&mode=data&action=delete&ID=". $item[6] ."'>";
					$out .= "<img src='img/icone_excluir.gif' border='0'>";
					$out .= "</a> ";
				}
			
				$out .=  "</td></tr></table>";
				$out .= "</td>";
				$out .= "</tr>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "<td align='right'><b>Tamanho:</b></td>";
				$out .= "<td>". $item[1] ." bytes</td>";
				$out .= "<td align='right'><b>Respons�vel:</b></td>";
				$out .= "<td>". $item[5] ."</td>";
				$out .= "</tr>";
				$out .= "<tr bgcolor='". $bgcolor4 ."'>";
				$out .= "	<td align='right'><b>Coment�rios:</b></td>";
				$out .= "	<td colspan='3' align='left'>";
								if ($item[3] == "") 	$out .= "Sem coment�rios";
								else $out .= strip_tags(str_replace("\n","<br>",$item[3]), '<br><b></b><i></i><font></font>');
				$out .= "	</td>";
				$out .= "</tr>";
				$out .= "</table>";
				
				$out .= "</td></tr>";
				$out .= "</table>";
			}
			$out .=  "</td></tr>";

		}
	}
	else {
		$out .= "<tr><td colspan=2 style=\"text-align:center; width: 100%;\">N�o existem novos itens a exibir.<br><br></td></tr>";
	}
	$out .= "</table>";
	return $out;
}



function listaComunicados($modulo,$user_kurz,$user_ID, $bgcolor4, $bgcolor6, $tdelements) {

	$nr = 0;


	$sql = " SELECT * FROM ";
	$sql .= " ( ";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";

	$sql .= " FROM ". $modulo ." m, ". $modulo ."_item_acesso r WHERE ";
	$sql .= " m.id = r.id_file AND r.tipo_acesso = 'U' AND r.id_users = ". $user_ID ." AND r.acesso IS NULL";


	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " )";
	$sql .= " AS m ";
	$sql .= " GROUP BY m.ID ORDER BY m.ID desc LIMIT ". $tdelements;
	//echo $sql . "<br><br>";

	$result = db_query($sql) or db_die();
	$out = "<script src='summary/js/ajax.js' type='text/javascript'></script>";
	$out .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";

	if (db_num_rows($result) > 0) {
		while ($row = db_fetch_row($result)) {
	        $mouseover = "onmouseover=\"this.style.cursor='pointer';\"";
	        //$mouseout = "onmouseout=\"this.style.backgroundColor='$bgcolor4';\"";
			if ($row[10] == "t") {
				$onclick = "onclick=\"document.location.href='index.php?mode=detalhes&module=". $modulo ."&ID=". $row[0] . "'\"";
			}
			else {
				$onclick = "onclick=\"document.location.href='arquivos/arquivos_down.php?module=". $modulo ."&nr=". $row[0] . "'\"";
			}

			$out .= "<tr><td style=\"padding-top: 8px;\">";
			$out .= "<table width='100%' cellspacing='0' cellpadding='3' border='0'>";
			$out .= "<tr $onclick $mouseover $mouseout nowrap valign='middle'>";
			$out .= "<td valign='middle' style=\"vertical-align:middle; text-transform:uppercase\" height='30'>";

			$out .= "<a href='index.php?mode=detalhes&module=". $modulo ."&ID=". $row[0] . "'>";
			$out .= "<span style=\"font-weight:bold; font-size: 16px\">". $row[2] ."</span></a><br>";
			$out .= "<span style=\"font-weight:bold; color: #888888; font-size: 10px\">". dataMesAnoPortugues($row[6]) ."</span><br><br>";
								
			$out .= "<span style=\"font-size: 12px; \">"; 
			$max_chars = 400;
			if ( strlen($row[5]) > $max_chars ) {
				$out .= strip_tags(str_replace("\n","<br>",substr($row[5],0,$max_chars)), '<br>') ."...";
				$out .= "<div style=\"text-align: right\">";
				$out .= "<a href='index.php?mode=detalhes&module=". $modulo ."&ID=". $row[0] . "' style=\"font-size:10px; \">";
				$out .= "Leia mais...</a>";
				$out .= "</div>";
			} else {
				$out .= strip_tags(str_replace("\n","<br>",$row[5]), '<br>');
			}
			$out .= "</span><br>";
			
			$out .= "</td>";
			$out .= "</tr>";
			$out .= "</table>";
			
			$out .= "</td>";
			$out .= "</tr>";

		}
	}
	else {
		$out .= "<tr><td style=\"width: 100%; text-align:center\"><br><br>N�o existem novos comunicados.<br><br><br></td></tr>";
	}
	$out .= "</table>";
	return $out;
}

function listaItensMarketing($modulo,$user_kurz,$user_ID, $bgcolor4, $bgcolor6, $tdelements) {
	global $upload_dir;
	$nr = 0;

	
		$sql = " SELECT * FROM ( ";
		$sql .= " (SELECT g.id, g.nome_arquivo, g.nome_temporario, g.descricao, g.id_categoria, h.ordem  ";
		$sql .= " FROM galerias_imagens g, galerias_imagens_home h ";
		$sql .= " WHERE h.id_imagem = g.id  ";
		$sql .= " ORDER BY h.ordem LIMIT 4) ";
		$sql .= " UNION ";
		$sql .= " (SELECT g.id, g.nome_arquivo, g.nome_temporario, g.descricao, g.id_categoria, g.id as ordem ";
		$sql .= " FROM galerias_imagens g ";
		$sql .= " ORDER BY id desc LIMIT 4) ";
		$sql .= " ) as tabela GROUP BY id ORDER BY ordem LIMIT 4";

	$result = db_query($sql) or db_die();
	$out = "<script src='summary/js/ajax.js' type='text/javascript'></script>";
	$out .= "<table width='100%' cellspacing='0' cellpadding='0' height=380>";
	$i=1;
	$total=db_num_rows($result) + 1;	
	if (db_num_rows($result) > 0) {
		while ($row = db_fetch_row($result)) {


			$link .= "<input type=button class='bt' name='botao_". $i ."' id='botao_". $i ."' style=\"font-weight:bold;cursor:pointer;backgroundColor:#FFFFFF\" onclick=\"exibirMarketing($i,$total)\"  onmouseover=\"habilitado=false;exibirMarketing($i,$total)\" value=' $i '> ";
			if ($i > 1) {
				$out .= "<tr style='display:none' id='tr_". $i ."'>";
			}
			else {
				$out .= "<tr id='tr_". $i ."'>";
			}
			$i++;
			
			$out .= "<td height=400 style='vertical-align:middle'>";
			$out .= "<table cellspacing='0' cellpadding='5' align=center>";
			
			
			$out .= "<tr $mouseover $mouseout $onclick valign='middle'><td valign='middle' align='center'>";

			$out .= "<a href='index.php?mode=detalhes&module=". $modulo ."&id=". $row[0] . "&id_album=". $row[4] ."'>";
			if (strstr($row[1],".jpg") || strstr($row[1],".gif") || strstr($row[1],".png") || strstr($row[1],".GIF") || strstr($row[1],".JPG") ) {
				if (strstr($row[1],".gif") || strstr($row[1],".GIF") ) {
					$out .= "<img width='400px' height='300px' src=\"". docGetThumb($upload_dir . $row[2],400,300,"png", 90) ."\" border=\"0\" onmouseover=\"this.style.cursor='pointer'\" align=\"bottom\">";

				}
				else {
					$out .= "<img width='400px' height='350px' src=\"". docGetThumb($upload_dir . $row[2],400,350,"png", 90) ."\" border=\"0\" onmouseover=\"this.style.cursor='pointer'\" align=\"bottom\">";

				}
			}
			else if (strstr($row[1],".swf")) {
				$out .= "<div align=\"center\">";
				$out .= "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" width=\"400\" height=\"350\">";
				$out .= "<param name=\"movie\" value=\"upload/". $row[2] ."\">";
				$out .= "<param name=\"quality\" value=\"high\">";
				$out .= "<embed src=\"upload/". $row[2] ."\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"400\" height=\"350\"></embed>";
				$out .= "</object>";
				$out .= "</div>";
			}
			$out .= "</a>";
			$out .= "<br>";
			$out .= "<span style=\"font-weight: bold; font-size: 16px;\">". $row[3] ."</span>";
			$out .= "</td>";
			
			
			$out .= "</tr>";
			$out .= "</table>";

			$out .= "</td>";
			$out .= "</tr>";

			
		}
		$out .= "<tr><td align='right'><table>";
		$out .= "<tr><td align='right' style='vertical-align:bottom'></td><td>". $link ."<td></tr>";
		$out .= "</td></table>";
	}
	else {
		$out .= "<tr><td width=100% colspan=2 style='vertical-align:middle;text-align:center'>";
		$out .= "<br><br>N�o existem novos itens.</font><br><br><br><Br></td></tr>";
	}
	$out .= "</table>";
	return $out;
}


function calculaQtdeItens($modulo) {
GLOBAL $user_kurz, $user_ID;

	$sql = " SELECT * FROM ";
	$sql .= " ( ";

	//QUERY PARA ARQUIVOS PRIVADOS OU P�BLICOS
	$sql .= " (";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ." m WHERE (m.von = ". $user_ID ." OR m.firms = 'todos') ";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " AND m.datum = '". date('d/m/Y') ."'";
	$sql .= " AND m.von <> '". $user_ID ."'";
	$sql .= " )";
	$sql .= " UNION ";

	//QUERY PARA ARQUIVOS POR GRUPO
	$sql .= " (";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ."_item_acesso f, ". $modulo ." m, user_firm u ";
	$sql .= " WHERE u.id_user = '". $user_kurz ."' ";
	$sql .= " AND f.id_file = m.id ";
	$sql .= " AND f.id_users = u.id_firm ";
	$sql .= " AND f.tipo_acesso = 'G' ";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " AND m.datum = '". date('d/m/Y') ."'";
	$sql .= " AND m.von <> '". $user_ID ."'";
	$sql .= " )";
	$sql .= " UNION ";

	//QUERY PARA ARQUIVOS POR USU�RIO
	$sql .= " (";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ."_item_acesso f, ". $modulo ." m ";
	$sql .= " WHERE f.id_file = m.id ";
	$sql .= " AND f.id_users = '". $user_ID ."' ";
	$sql .= " AND f.tipo_acesso = 'U' ";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'  ";
	$sql .= " AND m.datum = '". date('d/m/Y') ."'";
	$sql .= " AND m.von <> '". $user_ID ."'";
	$sql .= " )";
	$sql .= " UNION ";

	$sql .= " (";
	$sql .= " SELECT  m.ID, m.von, m.filename, m.remark, m.kat, m.acc, date_format(m.datum,'%d/%m/%Y') as datum, m.filesize, m.gruppe, m.tempname, ";
	$sql .= " m.typ, m.div1, m.div2, m.pw, m.acc_write, m.version, m.lock_user, m.contact, m.firms ";
	$sql .= " FROM ". $modulo ." m, ". $modulo ."_item_acesso r WHERE ";
	$sql .= " m.id = r.id_file AND r.tipo_acesso = 'U' AND r.id_users = ". $user_ID ." AND r.acesso IS NULL";
	$sql .= " AND m.div1 <> '0' ";
	$sql .= " AND m.typ <> 'd'";
	$sql .= " AND m.von <> '". $user_ID ."'";
	$sql .= " )  ";

	$sql .= " )";
	$sql .= " AS m ";
	$sql .= " WHERE m.von <> '". $user_ID ."'";

	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}



function calculaQtdeSolicitacoes() {
GLOBAL $user_kurz, $user_ID;
	$sql = " SELECT * FROM (";
	$sql .= "SELECT DISTINCT s.id ";
	$sql .= " FROM tb_solicitacoes s, users u WHERE s.solicitante = u.id AND s.situacao <> 'R' AND s.id IN  ";
	$sql .= " (SELECT b.id FROM tb_solicitacoes b, tb_solicitacoes_destinatarios d, user_firm f  ";
	$sql .= " WHERE b.id = d.id_solicitacao AND b.solicitante <> $user_ID AND ";
	$sql .= " (d.id_destinatario = $user_ID OR (f.id_firm = d.id_destinatario AND f.id_user = '$user_kurz')) ";
	$sql .= " AND DATE_FORMAT(b.data_envio,'%Y%m%d') = '". date('Ymd') ."'";
	$sql .= " GROUP BY id) ";
	$sql .= " UNION ";
	$sql .= " SELECT DISTINCT s.id ";
	$sql .= " FROM tb_solicitacoes s, users u WHERE s.solicitante = u.id AND s.situacao <> 'R' AND s.id IN ";
	$sql .= " (SELECT b.id FROM tb_solicitacoes b, tb_solicitacoes_permissoes d, user_firm f ";
	$sql .= " WHERE b.id = d.id_solicitacao AND b.solicitante <> $user_ID AND (d.id_acesso = $user_ID OR  ";
	$sql .= " (f.id_firm = d.id_acesso AND f.id_user = '$user_kurz')) ";
	$sql .= " AND DATE_FORMAT(b.data_envio,'%Y%m%d') = '". date('Ymd') ."'";
	$sql .= "GROUP BY id) ";
 	$sql .= " ) AS SOLICITACOES GROUP BY ID ORDER BY ID DESC";
	//$sql = "select count(id) from tb_solicitacoes where DATE_FORMAT(data_envio,'%Y%m%d') = '". date('Ymd') ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}


function calculaQtdeProjetos() {
GLOBAL $user_kurz, $user_ID;

	$sql = " SELECT id FROM (";
	$sql .= " SELECT p.id, p.NAME, DATE_FORMAT(p.ende,'%d/%m/%Y'), p.kategorie ";
	$sql .= " FROM projekte p, projekte_members m, user_firm f ";
	$sql .= " WHERE f.id_firm = m.id_member AND p.id = m.id_projekte AND f.id_user = '". $user_kurz ."' AND p.id_criador <> ". $user_ID ;
 	$sql .= " AND DATE_FORMAT(p.data_criacao,'%Y%m%d') = '". date('Ymd') ."'  AND tipo_acesso = 'G'";
	$sql .= " UNION ";
	$sql .= " SELECT p.id, p.NAME, DATE_FORMAT(p.ende,'%d/%m/%Y'), p.kategorie ";
	$sql .= " FROM projekte p, projekte_members m ";
	$sql .= " WHERE p.id = m.id_projekte AND m.id_member = '". $user_ID ."' AND p.id_criador <> ". $user_ID ;
 	$sql .= " AND DATE_FORMAT(p.data_criacao,'%Y%m%d') = '". date('Ymd') ."'  AND tipo_acesso = 'U' ";
	$sql .= " ) AS projetos";

	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeRecados() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM tb_recados WHERE datum";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeTopicosForum1() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM forum1 WHERE DATE_FORMAT(datum,'%Y%m%d') = '". date('Ymd') ."' and von <> '". $user_ID ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeTopicosForum2() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM forum2 WHERE DATE_FORMAT(datum,'%Y%m%d') = '". date('Ymd') ."' and von <> '". $user_ID ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeFaq() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM faq WHERE DATE_FORMAT(data_envio,'%Y%m%d') = '". date('Ymd') ."' and von <> '". $user_ID ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeContatos() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM contacts WHERE DATE_FORMAT(data_criacao,'%Y%m%d') = '". date('Ymd') ."' and von <> '". $user_ID ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeEnquetes() {
GLOBAL $user_kurz, $user_ID;
	$sql = "SELECT * FROM votum WHERE DATE_FORMAT(datum,'%Y%m%d') = '". date('Ymd') ."' and von <> '". $user_ID ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function calculaQtdeQuestionarios() {
	$sql = "SELECT * FROM questionario WHERE DATE_FORMAT(data_criacao,'%Y%m%d') = '". date('Ymd') ."'";
	$result = db_query($sql) or db_die();
	$row_count = db_num_rows($result);
	return $row_count;
}

function avisoTabelaVazia() {
	echo "<table border=0 width='100%' cellpadding='5' cellspacing='0'>";
	echo "<tr>";
	echo "		<td style=\"text-align:center\">N�o existem novos itens a exibir.<br><br></td>";
	echo "</tr>";
	echo "</table>";
}


function calculaQtdeCompromissos() {
	GLOBAL $user_kurz, $user_ID;
		$sql = "SELECT c.id, c.titulo, c.data, ";
		$sql.= "c.descricao, c.tipo, c.local, c.situacao, c.id_responsavel, c.hora ";
		$sql.="FROM calendario c WHERE c.id_responsavel = ".$user_ID." AND DATE_FORMAT(c.data,'%Y%m%d') = '". date('Ymd') ."' ";
		$sql.="UNION ";
		$sql.="SELECT c.id, c.titulo, c.data, ";
		$sql.="c.descricao, c.tipo, c.local, c.situacao, c.id_responsavel, c.hora "; 
		$sql.="FROM calendario c, calendario_item_acesso p ";
		$sql.="WHERE c.id = p.id_file AND p.tipo_acesso = 'T' AND DATE_FORMAT(c.data,'%Y%m%d') = '". date('Ymd') ."'	";
		$sql.="UNION ";
		$sql.="SELECT c.id, c.titulo, c.data, ";
		$sql.="c.descricao, c.tipo, c.local, c.situacao, c.id_responsavel, c.hora ";
		$sql.="FROM calendario c, calendario_item_acesso p ";
		$sql.="WHERE c.id = p.id_file AND p.id_users =  ".$user_ID."  ";
		$sql.="AND p.tipo_acesso = 'U' AND DATE_FORMAT(c.data,'%Y%m%d') = '". date('Ymd') ."' ";
		$sql.="UNION ";
		$sql.="SELECT c.id, c.titulo, c.data, ";
		$sql.="c.descricao, c.tipo, c.local, c.situacao, c.id_responsavel, c.hora ";
		$sql.="FROM calendario c, calendario_item_acesso p, user_firm f ";
		$sql.="WHERE c.id = p.id_file AND p.id_users = f.id_firm AND p.tipo_acesso = 'G' and f.id_user = '". $user_kurz."' ";
		$sql.="AND DATE_FORMAT(c.data,'%Y%m%d') = '". date('Ymd') ."' ";
		
		//echo $sql;
		$result = db_query($sql) or db_die();
		$row_count = db_num_rows($result);
		return $row_count;
}

?>