function Ajax() {
  try {
    return new ActiveXObject("Microsoft.XMLHTTP");
  } catch(e) {
    try {
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch(ex) {
      try {
        return new XMLHttpRequest();
      } catch(exc) {
        return false;
      }
    }
  }
}


function carregaDetalheArquivo(id,modulo,user_ID) {
	
	ajax = Ajax();
	var blank = '<table border="0" cellspacing="0" cellpadding="0"><tr><td height="1"></td></tr></table>';
	blank = '';
	if (document.getElementById('div_' + modulo + '_' + id).innerHTML != blank) {
		document.getElementById('div_' + modulo + '_' + id).innerHTML = '';
	}
	else {
		if(ajax){
			pagina = "summary/detalhe_arquivo.php?id="+id+"&modulo="+modulo+"&user_ID="+ user_ID;
			ajax.open("GET", pagina, true);
			ajax.onreadystatechange = function() {
				if(ajax.readyState == 4){
					if(ajax.status == 200){

						document.getElementById('div_' + modulo + '_' + id).innerHTML = ajax.responseText;
					}
				}
			}
			ajax.send(null);
		}
	}
}

function ChamadaListaMarketing(){
  ajax = Ajax();
  if(ajax){
    pagina = "summary/ajax_marketing.php";
    ajax.open("GET", pagina, true);
    ajax.onreadystatechange = function() {
      if(ajax.readyState == 4){
        if(ajax.status == 200){
			document.getElementById('barra_carregamento').innerHTML ='';
			document.getElementById('div_itens').innerHTML = ajax.responseText;
			barra(function(){
				barra('#barra_carregamento').slideToggle(2000);
				barra('#div_itens').slideToggle(2000);
			});
        }
      }
    }
    ajax.send(null);
  }
}