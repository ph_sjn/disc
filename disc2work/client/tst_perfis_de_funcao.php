<?php
// PERFIS DE FUNCAO
//-----------------------------------------------------------------------------------------------------------
// INDEX - INICIO - HEADER
//-----------------------------------------------------------------------------------------------------------
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);


// Carregar cabe�alho do sistema
include_once "lib/cabecalho.php"; 

// Verifica a sess�o
if ( isset($_SESSION) && session_name() != "ExtraNet_disc2work_client" ) { 
	session_destroy();
 	header("Location:".$link_extranet ); 
}

// Se a sess�o do usu�rio estiver ativa, permitir acesso interno do sistema
// Sen�o, pedir para fazer login
if ($user_kurz) {

		// Carregar o menu
		//include_once "lib/menu.php";
		
		// Verificar tipo de m�dulo a ser carregado
		// @pasta: m�dulos gen�ricos
		// @module: m�dulos espec�ficos com pastas de nomes pr�prios
		if ($pasta) {
			$inc_modulo = $pasta;
		}
		else {
			if (!$module) {
				$module = "summary";
			}
			$inc_modulo = $module;
		}
		
		// In�cio da div de delimita��o do miolo
		//include_once "lib/inc/topo_conteudo.php";
		
		// Bloqueia o acesso as p�ginas do tipo "sem acesso" para o usu�rio - fun��o se encontra na lib.inc.php
		if ((checaAcessoRestrito($module, $user_access, $inc_modulo) == true) || ($user_kurz == "suporte")) { 	
		
			// Carregar m�dulo
			//include_once $inc_modulo ."/" .$inc_modulo .".php";
			
		} else { ?>

			<br><br><br><br>
			<div style="text-align:center; font-size: 14px; color: blue">P�gina indispon�vel para seu usu�rio.
				<br><br>
				Favor entrar em contato com o administrador caso precise de acesso no endere�o indicado.
				<br><br>
				Suporte Extra|Net 
			</div>
			<br><br><br><br> <?
		}
?>
<?php 
//-----------------------------------------------------------------------------------------------------------
// INDEX - FINAL - HEADER 
//-----------------------------------------------------------------------------------------------------------
?>





<?php 
//-----------------------------------------------------------------------------------------------------------
// DSM_PERFIS_VIEW - INICIO - HEADER  
//-----------------------------------------------------------------------------------------------------------
?>

<!-- Chart.bundle.js versao 2.5.0-->
<script src="lib/js/chart/Chart.bundle.js"></script>
<link rel="stylesheet" href="lib/js/jquery/themes/extranet/jquery.ui.all.css">

<script src='dsm_projetos/js/ajax.js' type='text/javascript'></script>
<script src='dsm_projetos/js/perfis_de_funcao.js' type='text/javascript'></script>
<script src='../../survey/js/dssurvey_graph.js' type='text/javascript'></script>

<?php
$acesso_administrativo = checaPermissao($module,"A",$user_access); 
?>

<?php
$DEFWEB_cliente = $_POST['filtroCliente'];
//$DEFWEB_cliente = 9;
?>

<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<tr>
	<td style="vertical-align:middle;">
		<a href="tst_perfis_de_funcao.php?module=<?= $_REQUEST['module'] ?>&mode=forms"><span style="font-weight:bold;">NOVO PERFIL</span></a>
	</td>
</tr>
<tr>
	<td style="text-align:right">
	
		<!-- <form action='index.php' method='post' name='frm'> -->
		<form action='tst_perfis_de_funcao.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
				Cliente: 
				<select name='filtroCliente' class="combo">
				<option value=''>Todos</option>
				<?
				$sql = "SELECT id_cliente, ds_cliente FROM tbdw_cliente ORDER BY ds_cliente ASC";
				$resultCliente = db_query($sql) or db_die();
				while ($rowCliente = db_fetch_array($resultCliente) ){
					if ($DEFWEB_cliente == $rowCliente['id_cliente'])
						$selected = "selected";
					else
						$selected = "";
					echo "<option value='". $rowCliente['id_cliente'] ."' $selected>". $rowCliente['ds_cliente'] ."</option> " ;
				}
				?>
				</select>
			</td>
			<td style="vertical-align:middle;">
				<input type='Submit' name="filtrarCliente" value='Filtrar' class="botao">
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>
<br>
<?php
	$rsDados = array(
					  array("id_perfil" => "1", "nm_perfil" => "COORDENADOR", "ds_perfil" => "Coordena as rotinas administrativas e o planejamento estrat�gico")
					, array("id_perfil" => "2", "nm_perfil" => "COMPRADOR", "ds_perfil" => "Cota, negocia e compra materiais, equipamentos e servi�os")
					, array("id_perfil" => "3", "nm_perfil" => "GERENTE", "ds_perfil" => "Tra�a estrat�gia, planeja, organiza, controla e assessora a �rea de recursos humanos da empresa")
					, array("id_perfil" => "4", "nm_perfil" => "VENDEDOR", "ds_perfil" => "Executa atividades relacionadas � venda de produtos ou servi�os")
					, array("id_perfil" => "5", "nm_perfil" => "SUPERVISOR", "ds_perfil" => "Supervisiona as tarefas e os processos")
				);
?>
<?php 
//-----------------------------------------------------------------------------------------------------------
// DSM_PERFIS_VIEW - FINAL - HEADER  
//-----------------------------------------------------------------------------------------------------------
?>




<?php
//-----------------------------------------------------------------------------------------------------------
// AJAX_PERFIL - INICIO 
//-----------------------------------------------------------------------------------------------------------
$DW_id_cliente = $DEFWEB_cliente;
$GRAFICO_EXIBIR = 1;

//include_once("../survey/lib/libdisc.php");

// Biblioteca de classes e fun��es para aplica��o do question�rio
include_once("../survey/lib/libdisc_survey.php");

// Biblioteca de classes e fun��es do DefWebExpress
//include_once("../survey/lib/libdisc_analise.php");
?>							
<center>
    <table width="80%">
		<tr>
            <td colspan="4">
                <br>
				<b>PERFIS DE FUN��O</b><br>
                <br>
            </td>
		</tr>
		<tr>
			<td width="50%">
				ordenar por: 
				<input type="radio" name="optOrder" value="0" checked> <b>Perfil</b>
				 ou por: 
				<input type="radio" name="optOrder" value="1"> <b>Resultado</b>
			</td>
			<td>
				<input type="text" id="entrFiltro" onkeyup="filtrar()" class="dsFiltro" placeholder="Filtrar resultado...">
			</td>
			<td>
				<input type='button' name="cmdOpenAll" value='Abrir todos' title="Abrir todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
			<td>
				<input type='button' name="cmdCloseAll" value='Fechar todos' title="Fechar todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
        </tr>
        <tr>
            <td colspan="4">
						
                <?php
				//
                $bPrimeiraLinha = TRUE;
				for($i=0; $i < count($rsDados); $i++) {
                    //
					$discPerfil = array(
						"D" => array(
							"M" => 0,
							"L" => 0,
							"A" => 0
						),
						"I" => array(
							"M" => 0,
							"L" => 0,
							"A" => 0
						),
						"S" => array(
							"M" => 0,
							"L" => 0,
							"A" => 0
						),
						"C" => array(
							"M" => 0,
							"L" => 0,
							"A" => 0
						)
					);
					//
					switch ($rsDados[$i]["id_perfil"]) {
						case 1:
							// Coordenador - SC : (37, 36, 80, 76)
							//
							$sSerie = "[37, 36, 80, 76]";
							//
							$sSerieMax = "[100, 100, 100, 100]";
							$sSerieMin = "[20, 0, 70, 60]";
							//
							$sLetters = "SC";
							//
							$discPerfil["D"]["M"] = "100";
							$discPerfil["D"]["L"] = "20";
							$discPerfil["D"]["A"] = "37";
							$discPerfil["I"]["M"] = "100";
							$discPerfil["I"]["L"] = "0";
							$discPerfil["I"]["A"] = "36";
							$discPerfil["S"]["M"] = "100";
							$discPerfil["S"]["L"] = "70";
							$discPerfil["S"]["A"] = "80";
							$discPerfil["C"]["M"] = "100";
							$discPerfil["C"]["L"] = "60";
							$discPerfil["C"]["A"] = "76";
							break;
						case 2:
							// Comprador - IS : (37, 81, 76, 36)
							//
							$sSerie = "[37, 81, 76, 36]";
							//
							$sSerieMax = "[100, 100, 100, 100]";
							$sSerieMin = "[20, 70, 60, 0]";
							//
							$sLetters = "IS";
							//
							$discPerfil["D"]["M"] = "100";
							$discPerfil["D"]["L"] = "20";
							$discPerfil["D"]["A"] = "37";
							$discPerfil["I"]["M"] = "100";
							$discPerfil["I"]["L"] = "70";
							$discPerfil["I"]["A"] = "81";
							$discPerfil["S"]["M"] = "100";
							$discPerfil["S"]["L"] = "60";
							$discPerfil["S"]["A"] = "76";
							$discPerfil["C"]["M"] = "100";
							$discPerfil["C"]["L"] = "0";
							$discPerfil["C"]["A"] = "36";
							break;
						case 3:
							// Gerente - DI : (81, 77, 35, 36)
							//
							$sSerie = "[81, 77, 35, 36]";
							//
							$sSerieMax = "[100, 100, 100, 100]";
							$sSerieMin = "[70, 60, 20, 0]";
							//
							$sLetters = "DI";
							//
							$discPerfil["D"]["M"] = "100";
							$discPerfil["D"]["L"] = "70";
							$discPerfil["D"]["A"] = "81";
							$discPerfil["I"]["M"] = "100";
							$discPerfil["I"]["L"] = "60";
							$discPerfil["I"]["A"] = "77";
							$discPerfil["S"]["M"] = "100";
							$discPerfil["S"]["L"] = "20";
							$discPerfil["S"]["A"] = "35";
							$discPerfil["C"]["M"] = "100";
							$discPerfil["C"]["L"] = "0";
							$discPerfil["C"]["A"] = "36";
							break;
						case 4:
							// Vendedor - ID : (77, 81, 35, 36)
							//
							$sSerie = "[77, 81, 35, 36]";
							//
							$sSerieMax = "[100, 100, 100, 100]";
							$sSerieMin = "[60, 70, 20, 0]";
							//
							$sLetters = "ID";
							//
							$discPerfil["D"]["M"] = "100";
							$discPerfil["D"]["L"] = "60";
							$discPerfil["D"]["A"] = "77";
							$discPerfil["I"]["M"] = "100";
							$discPerfil["I"]["L"] = "70";
							$discPerfil["I"]["A"] = "81";
							$discPerfil["S"]["M"] = "100";
							$discPerfil["S"]["L"] = "20";
							$discPerfil["S"]["A"] = "35";
							$discPerfil["C"]["M"] = "100";
							$discPerfil["C"]["L"] = "0";
							$discPerfil["C"]["A"] = "36";
							break;
						case 5:
							// Supervisor - SD : (60, 40, 70, 40)
							//
							$sSerie = "[60, 40, 70, 40]";
							//
							$sSerieMax = "[100, 100, 100, 100]";
							$sSerieMin = "[51, 20, 60, 0]";
							//
							$sLetters = "SD";
							//
							$discPerfil["D"]["M"] = "100";
							$discPerfil["D"]["L"] = "51";
							$discPerfil["D"]["A"] = "60";
							$discPerfil["I"]["M"] = "100";
							$discPerfil["I"]["L"] = "20";
							$discPerfil["I"]["A"] = "40";
							$discPerfil["S"]["M"] = "100";
							$discPerfil["S"]["L"] = "60";
							$discPerfil["S"]["A"] = "70";
							$discPerfil["C"]["M"] = "100";
							$discPerfil["C"]["L"] = "0";
							$discPerfil["C"]["A"] = "40";
							break;
						default:
							break;
					}
					//
					if ($bPrimeiraLinha) {
                        $bPrimeiraLinha = FALSE;
						echo "<table id=\"tblDadosPerfil\" width='100%'>";
					}
                    ?>
					
	<?php // table tblDadosPerfil ordenacao ?>
	<tr>
		<td>
	<?php // table tblDadosPerfil ordenacao ?>
	
            <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?php echo($bgcolor5); ?>' align='center'>
                <tr width="100%">
                    <td bgcolor="<?php echo($bgcolor4); ?>">
							
						<table width="100%" cellpadding="0" cellspacing="0" border=0>
                            <tr>
                                <td width="50%" onclick="javascript:perfil('imgPerfil_<?= $rsDados[$i]["id_perfil"] ?>', 'trPerfil_<?= $rsDados[$i]["id_perfil"] ?>');" onmouseover="this.style.cursor = 'pointer'">
									<img id="imgPerfil_<?php echo($rsDados[$i]["id_perfil"]);?>" src="img/close.gif" border="0">&nbsp;
                                    <span class="td_perfil_nome">
                                    <?php
										echo($rsDados[$i]["nm_perfil"] . " : " . $rsDados[$i]["ds_perfil"]);
                                    ?>
									</span>
                                </td>
								<td width="20%">
									<?php
										$result_fmt = "";
										for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
											if ( $nLetter == 0 ) {
												$result_fmt = "<span class=\"disc_letter_main\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											} else {
												$result_fmt .= "<span class=\"disc_letter_norm\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											}
										}
										if ( strlen($sLetters) > 0 ) {
											echo($result_fmt);
										}
									?>
								</td>
                            </tr>
                        </table>
						
                    </td>
                </tr>
                <tr style="display:none; padding: 0px;" id="trPerfil_<?php echo($rsDados[$i]["id_perfil"]);?>" bgcolor="<?php echo($bgcolor6); ?>">
                    <td style="padding: 0px;">						
						
						
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <td>Resultado</td>
                                    <?php
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%Max</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">%Min</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["D"]["A"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["D"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["D"]["L"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["I"]["A"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["I"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["I"]["L"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["S"]["A"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["S"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["S"]["L"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["C"]["A"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["C"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPerfil["C"]["L"]); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAn" . $rsDados[$i]["id_perfil"] . "' value='". $sSerie ."' >");
										echo("<input type='hidden' id='grfMax" . $rsDados[$i]["id_perfil"] . "' value='". $sSerieMax ."' >");
										echo("<input type='hidden' id='grfMin" . $rsDados[$i]["id_perfil"] . "' value='". $sSerieMin ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAn" . $rsDados[$i]["id_perfil"] . "' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
                                </tr>
							</tbody>
						</table>
						
					</td>
				</tr>
			</table>
			
	<?php // table tblDadosPerfil ordenacao ?>
		</td>
	</tr>
	<?php // table tblDadosPerfil ordenacao ?>
			
			<?  } // end for?>
            <?php
                if ($bPrimeiraLinha) {
                    echo("N�O FOI ENCONTRADO NENHUM PERFIL!");
                } else {
					echo "</table>";
				}
            ?>

			  
            </td>
        </tr>
    </table>
</center>
<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />
<?php
//-----------------------------------------------------------------------------------------------------------
// AJAX_PERFIL - FINAL   
//-----------------------------------------------------------------------------------------------------------
?>







<?php 
//-----------------------------------------------------------------------------------------------------------
// DSM_PERFIS_VIEW - INICIO/FINAL - FOOTER
//-----------------------------------------------------------------------------------------------------------
?>


<?php 
//-----------------------------------------------------------------------------------------------------------
// INDEX - INICIO - FOOTER  
//-----------------------------------------------------------------------------------------------------------
?>
<?php 
	// Fim da delimita��o do miolo
	include_once "lib/inc/rodape_conteudo.php";
		
} else {

	// Setar m�dulo de login
	$module = "login";
		
	// Carregar p�gina de login
	include_once "login/login.php";
		
}

// Carregar rodap� do sistema
include_once "lib/rodape.php";
?>
<?php 
//-----------------------------------------------------------------------------------------------------------
// INDEX - FINAL - FOOTER  
//-----------------------------------------------------------------------------------------------------------
?>