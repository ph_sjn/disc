<?php
/*
 *  Parametros:
 *    remotedoc: nome do arquivo incluindo o path completo 
 */

//Definicao do nome do arquivo e diretorio (nome da extranet)
if(isset($_POST['remotedoc'])) {
    
    $doc = $_POST['remotedoc'];

	// Obter as dimensões da imagem, no formato 9999x9999
	$dimensoes = getimagesize( $doc );
	
	// LARGURA X ALTURA X FORMATO RGB/CMYK
	echo $dimensoes[0] ."x". $dimensoes[1] ."x". $dimensoes[2];
}
else {
	echo 0;
}
?>
