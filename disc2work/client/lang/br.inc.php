<?
//DECLARANDO VARI�VEIS
if ( !isset($kurz) ) $kurz = (string) "";
if ( !isset($user_name) ) $user_name = (string) "";
if ( !isset($titel) ) $titel = (string) "";
if ( !isset($module) ) $module = (string) "";
if ( !isset($kurz_len) ) $kurz_len = (string) "";


$nome_franquia = $nome_extranet;

$endereco_host = $link_extranet;

$error_upload = "Erro ao gravar arquivo";

$submit = "Submeter";
$back = "Voltar";
$print = "Imprimir";
$exp1 = "Exportar";
$exp2 = "| (Ajuda)";
$confirm = "Voc� tem certeza?";
$confirm_delete = "Confirmar exclus�o de usu�rio?";
$items = "Itens";
$items2 = "Registros";
$previous = " << anterior ";
$next = " pr�ximo >> ";
$move_it  = "Mover";
$copy_it = "Copiar";
$delete_it = "Excluir";
$save_it = "Salvar";
$directory = "Diret�rio";
$del_contents = "Apaga tamb�m o conte�do";
$sum = "Total";
$filter_it = "Busca";
$form_error = "Preencha o campo seguinte";
$approve = "Autorizar";
$undo = "Desfazer";
$choose_one="Selecione!";
$new = "Novo";
$select_all = "Selecione todos";
$printable_view = "Visualizar impress�o";
$notify_title = "[EXTRANET] Novo item no m�dulo ";
$notify_body = "Prezado usu�rio,<br><br>


Informamos que um novo item foi adicionado no m�dulo ";
$notify_body2 = ". <br><br> Item: <b>";
$notify_body21 = "</b>. <br>Coment�rio: <b>";
$notify_body3 = "</b><br><br>

Para conferir, acesse o link <br>
<a href='". $link_extranet ."'>". $link_extranet ."</a><br><br> 

Atenciosamente, <br><br> ". $nome_extranet;
$notify_text = "Notificar os usu�rios selecionados";
$yes = "Sim";
$no = "N�o";
$chars = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

$more_info = "Mais informa��es";

// admin.php
$admin_texta = "Senha";
$admin_textb = "Acesso";
$admin_textb1 = "Grupo";
$admin_text1 = "Administra��o";
$admin_text2 = "Sua senha";
$admin_text3 = "N�o est� autorizado a entrar. ";
$admin_text4 = "Ajuda";
$admin_text5 = "Gerenciamento de Usu�rios";
$admin_text6 = "Incluir";
$admin_text6a = "Nova enquete";
$admin_text7 = "Modificar";
$admin_text7b = "Detalhes do item";
$admin_text8 = "Excluir";
$admin_text9 = "Projetos";
$admin_text10 = "Recursos";
$admin_text11 = "Favoritos";
$admin_text12 = "Verificar a validade dos links";
$admin_text13 = "Verificar";
$admin_text14 = "Excluir favorito";
$admin_text16 = "(Pressione CTRL+Click para selecionar mais de uma op��o)";
$admin_text16b = "Pressione CTRL+Click para selecionar mais de uma op��o";
$admin_text171 = "Li��es Aprendidas";
$admin_text172 = "F�rum Discuss�es";
$admin_text173 = "An�ncios";
$admin_text18 = "Temas com mais de";
$admin_text19 = "dia(s) ";
$admin_text20 = "<i>Chat</i>";
$admin_text21 = "Salvar texto do corrente <i>Chat</i>";
$admin_text22 = "<i>Chat</i> Atual";
$admin_text23 = "Senha";
$admin_text23a = "Nova senha";
$admin_text23b = "(Senha antiga: Deixe vazio)";
$admin_text24 = "Grupo padr�o<br> (Somente o Root pode modificar)";
$admin_text25 = "Tipo de acesso";
$admin_text26 = "CEP";
$admin_text27 = "Idioma";
$admin_text28a = "Agenda vis�vel a outros utilizadores";
$admin_text28b = "Agenda invis�vel a outros utilizadores";
$admin_text28c = "Compromisso vis�vel, mas n�o leg�vel";
$admin_text30 = "Tem que preencher estes campos.";
$admin_text31 = "Preencha o nome do grupo.";
$admin_text32 = "J� existe este nome!";
$admin_text33 = "J� existe este Login!";
$admin_text33a = "Por favor insira exatamente $kurz_len letras para o Login.";
$admin_text33b = "O login <i>$kurz</i> j� pertence a outro usu�rio.";
$admin_text33c = "Selecione pelo menos um grupo do usu�rio.";
$admin_text34 = "J� existe esta senha!";
$admin_text34a = "Este nome j� existe !.";
$admin_text35 = "Novo usu�rio inclu�do.";
$admin_text36 = "Dados alterados com sucesso.";
$admin_text36b = "Dados do grupo atualizados.";
$admin_text36c = "Dados do usu�rio atualizados.";
$admin_text37 = "Escolha um membro";
$admin_text38 = "Continua listado em v�rios projetos. Por favor elimine.";
$admin_text39 = "Foram eliminados todos os perfis";
$admin_text40 = "Foi retirado de todos os perfis";
$admin_text41 = "Foram eliminados todos os �tens a fazer do membro";
$admin_text42 = "Foi retirado dessa vota��o onde ele/ela ainda n�o participaram";
$admin_text43 = "Foram eliminados todos os eventos";
$admin_text44 = "Arquivo do membro eliminado";
$admin_text45 = "Conta banc�ria eliminada";
$admin_text46 = "Terminado";
$admin_text46b = "Usu�rio exclu�do.";
$admin_text46c = "Grupo exclu�do.";
$admin_text47 = "Escolha um projeto";
$admin_text48 = "O projeto ";
$admin_text49 = " e os respectivos subprojetos foram removidos";
$admin_text50 = "Est� errada a dura��o do projeto.";
$admin_text51 = "Projeto inclu�do.";
$admin_text52 = "O projeto foi alterado com sucesso";
$admin_text53 = "Escolha um recurso";
$admin_text54 = "Recurso eliminado";
$admin_text55 = "Todos os links e eventos deste recurso foram eliminados";
$admin_text56 = "O recurso est� agora listado.";
$admin_text57 = "Recurso alterado com sucesso.";
$admin_text58 = "O servidor enviou a seguinte mensagem de erro";
$admin_text59 = "Todos os links s�o v�lidos.";
$admin_text60 = "Selecione no minimo um link";
$admin_text61 = "Favorito eliminado";
$admin_text62 = "tema(s) foi(foram) eliminado(s).";
$admin_text63 = "Todos os chats atuais foram eliminados";
$admin_text64 = "ou";
$admin_text65 = "Ponto eletr�nico";
$admin_text66 = "Ver";
$admin_text67 = "Escolher Grupo";
$admin_text68 = "Nome do Grupo";
$admin_text69 = "Formul�rio pequeno";
$admin_text70 = "Categoria";
$admin_text71 = "Anota��es";
$admin_text72 = "Gerenciamento do Grupo";
$admin_text73 = "Preencha a descri��o do projeto";
$admin_text74 = "O nome ou formul�rio pequeno j� existe";
$admin_text75 = "Assinala automaticamente para o grupo:";
$admin_text76 = "Assinala automaticamente para o usu�rio:";
$admin_text77 = "Gerenciamento do Suporte";
$admin_text78 = "Categoria apagada";
$admin_text79 = "A categoria foi criada";
$admin_text80 = "A categoria foi modificada";
$admin_text81 = "Membro dos seguintes grupos<br> (Somente o Root pode modificar)";
$admin_text82 = "O Grupo padr�o n�o est� na lista";
$admin_text83 = "Nome de Acesso";
$admin_text84 = "Voc� n�o pode excluir o grupo padr�o";
$admin_text85 = "Apaga o grupo e mescla o conte�do com o grupo";
$admin_text86 = "Por favor escolha uma op��o";
$admin_text86a = "Selecione um usu�rio";
$admin_text86b = "Selecione um grupo";
$admin_text86c = "Selecione a categoria";
$admin_text87 = "Novo grupo inclu�do.";
$admin_text88 = "Gerenciador de arquivo";
$admin_text88m = "Gerenciador de manuais";
$admin_text88mk = "Gerenciador de marketing";
$admin_text88f = "Gerenciador de formul�rios";
$admin_text88a = "Gerenciador de apresenta��es";
$admin_text89 = "Arquivos �rf�os";
$admin_text90 = "Remo��o da conta n�o foi poss�vel";
$admin_text91 = "Nome ldap";
$admin_text92 = "Celular";
$admin_text93a = "Usu�rio normal";
$admin_text93b = "Privilegiado";
$admin_text93c = "Administrador";
$admin_text93d = "Usu�rio RP";
$admin_text94 = "Fazendo Login";
$admin_text94a = "Login";
$admin_text94b = "Logout";
$admin_text95 = "Postando (com todos os coment�rios) com uma identifica��o";
$admin_text96a = "Papel apagado, atribui��o aos usu�rios para este papel removido";
$admin_text96b = "O papel foi criado";
$admin_text96c = "O papel foi modificado";
$admin_text96 = "Franquia";
$admin_text97 = "Gerenciamento de Grupos";
$admin_text98 = "Grupo";
$admin_text99 = "Descri��o";
$admin_text100 = "UF";
$admin_text101 = "Autoriza��o de t�picos";
$admin_text102a = "Autorizar";
$admin_text102b = "Negar";
$admin_text103a = "O t�pico foi autorizado.";
$admin_text103b = "O coment�rio foi negado e exclu�do!";
$admin_text104 = "Nenhuma autoriza��o pendente";
$admin_text105 = "Visualizar";
$admin_text107 = "Preencha o login com letras alfa-num�ricas.";

$admin_text108 = "Preencha o nome de usu�rio";
$admin_text109 = "Preencha o login";
$admin_text110 = "Preencha a senha";
$admin_text111 = "O email � obrigat�rio";


$admin_text112 = "Novo grupo criado.";
$admin_text113 = "Grupo exclu�do com sucesso.";
$admin_text114 = "O Projeto foi modificado com sucesso.";


$admin_text113 = "Pergunta modificada.";
$admin_text114 = "Question�rio modificado.";
$admin_text115 = "Novo question�rio criado.";
$admin_text116 = "Nova pergunta criada.";
$admin_text117 = "Question�rio exclu�do.";
$admin_text118 = "Pergunta exclu�da.";


//contacts.php
$con_text0 = "Gerenciador de contatos:";
$con_text1 = "Novo contato";
$con_text2 = "Membros do grupo";
$con_text2f = "Franqueado";
$con_text2fd = "Participante";
$con_text3 = "Fornecedores";
$con_text3at = "Franqueador";
$con_text4 = "Novo";
$con_text4c = "Inserir nova FAQ";
$con_text4b = "Nova categoria";
$con_text5 = "Importar";
$con_text6 = "Modificar";
$con_text7 = "Excluir";
$con_text8 = "Contato adicionado com sucesso.";
$con_text9 = "Os dados do contato foram modificados com sucesso.";
$con_text10 = "Contato exclu�do com sucesso.";
$con_text11 = "Abrir para todos";
$con_text12 = "Figura";
$con_text13 = "Por favor, selecione um vcard (*.vcf)";
$con_text14 = "Cria um Vcard";
$con_text15 = "Importar cat�logo de endere�os";
$con_text16 = "Por favor selecione um arquivo (*.csv)";
$con_text17 = "Dica: Abra seu cat�logo de endere�os do Outlook Express e selecione 'arquivo'/'exportar'/'outros endere�os'<br>
Nomeie o arquivo, selecione todos os campos na pr�xima tela e 'fim'";
$con_text18 = "Abra o Outlook em  'arquivo/importar/exportar no arquivo',<br>
escolha 'v�rgula separando valores (Win)', ent�o selecione 'contatos' no pr�ximo formul�rio,<br>
d� o nome de exporta��o e arquivo e acabou!";
$con_text19 = "Por favor escolha um arquivo para exportar (*.csv)";
$con_text20 = "Por favor exporte seu cat�logo de endere�os com uma v�rgula separando o nome do arquivo (.csv),<br>
e modifique as colunas da tabela com a extens�o SHEET para esse formato: <br>
T�tulo, Nome, Empresa, E-mail, E-mail 2, Fone 1, Fone 2, Fax, Celular, Rua, CEP, Cidade, Pa�s, Estado, Categoria, Coment�rio, Endere�o eletr�nico.<br>
Colunas apagados em seu arquivo n�o ser�o mostrados aqui e crie colunas vazias para campos que n�o existem em seu arquivo";
$con_text21 = "Por favor insira seu nome";
$con_text22 = "A importa��o dos Registros falhou devido a exist�ncia de um campo errado";
$con_text23 = "Importar para autorizar";
$con_text24 = "Importar lista";
$con_text25 = "A lista foi importada.";
$con_text26 = "A lista foi reijeitada.";
$con_text27 = "Perfis";
$con_text28 = "Objeto de origem";
$con_text29 = "Nova Enquete";
$con_text30 = "O Contato foi modificado com sucesso";

// apresentacoes.php/manuais.php/marketing.php.formularios.php
$datei_text1 = "Selecione um arquivo";
$datei_text2 = "J� existe um arquivo com este nome";
$datei_text3 = "Nome";
$datei_text4 = "Coment�rio";
$datei_text4_1 = "Observa��es";
$datei_text5 = "Tipo";
$datei_text6 = "Data";
$datei_text7 = "Enviar ";
$datei_text8 = "Arquivo";
$datei_text8b = "Texto";
$datei_text8a = "T�tulo";
$datei_text9 = "Apaga arquivo";
$datei_text10 = "Sobrescreve";
$datei_text11 = "Acesso";
$datei_text11a = "Privado";
$datei_text11b = "P�blico";
$datei_text11c = "Grupos";
$datei_text11d = "Alguns s�o como diret�rios";
$datei_text11e = "Usu�rios";
$datei_text12 = "Voc� n�o est� autorizado a sobrescrever este arquivo quando algu�m estiver enviando";
$datei_text13 = "Pessoal";
$datei_text14 = "Buscar";
$datei_text15 = "Link";
$datei_text16 = "Endere�o";
$datei_text17 = "Com novos valores";
$datei_text18 = "Todos os arquivos neste diret�rio ser�o removidos! Continua?";
$datei_text19 = "Este nome j� existe";
$datei_text20 = "Tamanho m�ximo de arquivo";
$datei_text20b = "Tamanho";
$datei_text21 = "Link para";
$datei_text22 = "Objetos";
$datei_text23 = "A��es em alguns diret�rios n�o s�o poss�veis";
$datei_text24 = "Alterar item";
$datei_text25 = "O arquivo est� protegido. Digite a senha para acess�-lo: ";
$datei_text26 = "Senha";
$datei_text27 = "Repetir";
$datei_text28 = "Senhas diferentes!";
$datei_text29 = "Download do arquivo protegido por senha";
$datei_text30 = "Notificar todos os usu�rios com acesso";
$datei_text31 = "Escrever acesso";
$datei_text32 = "Vers�o";
$datei_text33 = "Vers�o Gerente";
$datei_text34a = "Travar";
$datei_text34b = "Destravar";
$datei_text34c = "Travado por";
$datei_text35 = "Download alternativo";

// forum1.php/forum2.php
$forum_text1 = "Por favor insira um titulo ao tema. Voltar...";
$forum_text2 = "Novo t�pico";
$forum_text3 = "Respostas";
$forum_text5 = "T�tulo";
$forum_text6 = "Texto";
$forum_text7 = "Submeter";
$forum_text7b = "Autor";
$forum_text8 = "Autor";
$forum_text9 = "Data";
$forum_text10 = "Aberto";
$forum_text11 = "Fechado";
$forum_text12 = "Enviar notifica��o por e-mail para os usu�rios da extranet";


$forum_text131 = "Prezado usu�rio, <br><br> 
				Um novo t�pico no F�rum de Discuss�es foi publicado na extranet $nome_extranet. <br><br>
				
				<b>T�tulo:</b> ";
$forum_text131b = "<br>
				<b>Enviado por:</b> ";
$forum_text131c = "<br><br>
				
				Atenciosamente, <br><br>
				FRANQUIA $nome_extranet <br>
				<a href='$link_extranet'>$link_extranet</a>
				";

if ( !isset($row[0]) ) $row[0] = (string) "";

$forum_text13a = "Responder sua mensagem no f�rum";
$forum_text13b = "Voc� teve uma resposta para sua mensagem \n '$row[0]' \n De
$user_name com o t�tulo: \n $titel\n";

// index.php
$index_text1a = "Nome";
$index_text1b = "Senha";
$index_text1c = "Login";
$index_text2 = "N�o est� autorizado a entrar";
$index_text3 = "Por favor execute-o";
$index_text4 = "Relembrar";
$index_text5 = "Sess�o expirada, por favor efetue seu Login novamente";

// info.php
$info_text1 = "Resultado da busca";
$info_text2 = "Data";
$info_text3 = "Texto";
$info_text4 = "Nenhum resultado encontrado";
$info_text5 = "T�tulo";
$info_text6 = "Nome do arquivo";
$info_text7 = "Coment�rio";
$info_text8 = "Categoria";
$info_text8b = "Situa��o";
$info_text9 = "Telefone";
$info_text10 = "Nome";
$info_text11 = "Empresa";
$info_text12 = "Endere�o";
$info_text13 = "Cidade";
$info_text14 = "Pa�s";
$info_text15 = "Por favor selecione os m�dulos onde a palavra-chave efetuar� a busca";
$info_text16 = "Insira sua(s) palavra(s)-chave . Se voc� inserir duas palavras, <b>ambas</b> devem aparecer no registro";
$info_text17 = "T�tulo";
$info_text18 = "Estado";

// setup.php
$inst_text1 = "Bem vindo � configura��o do PHProjekt!<br>";
$inst_text1a = "Por favor anote:<ul>
<li>Uma base de Dados em branco, deve estar dispon�vel
<li>Por favor tenha certeza que o seu Servidor Web sej� capaz de escrever o arquivo  'config.inc.php'<br> (e.g. 'chmod 777')";
$inst_text1b = "<li>se voc� encontrar qualquer erro durante a instala��o, por favor olhe em  <a href='help/faq_install.html' target=_blank>install faq</a>
ou visite o <a href='http://www.PHProjekt.com/forum.html' target=_blank>� de Instala��o</a></i>";
$inst_text2 = "Preencha os seguintes campos";
$inst_text3 = "(Em alguns casos o programa jamais responder�.<br>
Nesse caso cancele o programa, feche o browser e tente novamente).<br>";
$inst_text3a = "Tipo de Banco de Dados";
$inst_text4 = "Hostname";
$inst_text5 = "Username";
$inst_text6 = "Password";
$inst_text7 = "Nome do Banco de Dados existente";
$inst_text7a = "N�o foi encontrado config.inc.php! Deseja mesmo atualizar? Leia INSTALL ...";
$inst_text7b = "N�o foi encontrado config.inc.php! Talvez prefira atualizar o PHProjekt? Leia INSTALL ...";
$inst_text7c = "Por favor escolha  'Instala��o' ou 'Atualiza��o'! voltar ...";
$inst_text8 = "N�o funciona! Verifique as Permiss�es do Banco de Dados  <br>Repare e reinstale.";
$inst_text8b = "Desculpe, N�o est� funcionando! <br> Por favor configure DBDATE para 'Y4MD-' ou deixe o Phprojekt mudar est� vari�vel de sistema (php.ini)!";
$inst_text9 = "Parab�ns! Disp�e de um Database para liga��o!";
$inst_text10 = "Selecione os m�dulos que pretende usar.<br> (Pode desativ�-los mais tarde no config.inc.php)<br>";
$inst_text12 = "Instalar componente: inserir '1', ou deixe a caixa vazia";
$inst_text13 = "Grupos mostrados";
$inst_text14 = "Listas de �tens a fazer";
$inst_text15 = "F�rum";
$inst_text16 = "Sistema de vota��o";
$inst_text17 = "Favoritos";
$inst_text18 = "Recursos";
$inst_text19 = "Cidade";
$inst_text19a = "Nome do usu�rio definido no campo";
$inst_text19b = "Usu�rio definido";
$inst_text19c = "Perfis para contatos";
$inst_text20 = "Correio";
$inst_text20a = "1: Enviar Correio";
$inst_text20b = " Somente,<br> &nbsp; &nbsp; 2: Cliente de correio estiver cheio";
$inst_text21 = "Chat";
$inst_text22 = "Projetos";
$inst_text22a = "Lembretes";
$inst_text22b = "'1' para mostrar o apontamento na lista em uma janela separada,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'2' para alertas adicionais.";
$inst_text22c = "Alarme";
$inst_text22d = "M�ximo de minutos antes do evento";
$inst_text22e = "Lembrador de Correio";
$inst_text22f = "Lembrar via E-mail";
$inst_text23 = "'1'= Criar projetos,<br>
&nbsp; &nbsp; '2'= Relacionar worktime a projetos somente com a entrada do timecard<br>
&nbsp; &nbsp; '3'= Relacionar worktime a projetos sem a entrada do timecard<br> ";
$inst_text24 = "NOVO ITEM";
$inst_text24m = "Manuais";
$inst_text24mk = "Marketing";
$inst_text24f = "Formul�rios";
$inst_text24a = "Apresenta��es";
$inst_text25 = "Nome do diret�rio onde os arquivos ser�o guardados<br>( Sem administra��o de arquivos: campo vazio)";
$inst_text26 = "Localiza��o completa do diret�rio(Sem Arquivos = campo vazio)";
$inst_text26a = "Cart�o de Ponto";
$inst_text26b = "'1' Sistema de cart�o de ponto,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '2' Inser��o manual de Registros com c�pia para o Chefe";
$inst_text26c = "Notas";
$inst_text26d = "Mudar Senha";
$inst_text26e = "Novas senhas pelo usu�rio - 0: nenhum - 1: Somente senha rand�micas - 2: Escolha a pr�pria";
$inst_text26h = "Senhas encriptadas";
$inst_text26i = "Login via ";
$inst_text26j = "Pagina para login via SSL";
$inst_text26k = "Grupo";
$inst_text26l = "Fun��es de usu�rios e m�dulos s�o atribuidas para os grupos<br>
&nbsp;&nbsp;&nbsp;&nbsp;(recomendada para os n�meros de usu�rios > 40)";
$inst_text26m = "Fun��es de usu�rios e m�dulos s�o atribuidas para os grupos";
$inst_text26n = "Suporte (RT)";
$inst_text26o = "Gerenciador do Suporte / Problema com o sistema";
$inst_text26p = "RT Op��o: Cliente pode estabelecer uma data devida";
$inst_text26q = "RT Op��o: Autentica��o do Cliente";
$inst_text26r = "0: Aberto para todos, e-mail � suficiente, 1: cliente precisa estar na lista de contatos e inserir seu nome";
$inst_text26s = "RT Op��o: Inserir Solicita��o";
$inst_text26t = "0: Para todos, 1: Para todas as pessoas com status de  'chefe'";
$inst_text26u = "E-mail do suporte";
$inst_text26v = "Mistura nomes de arquivos";
$inst_text26w = "Cria nomes de arquivos misturados no servidor<br>
Assume o nome anterior no download";
$inst_text26x = "Nome de Login";
$inst_text26y = "0: Ultimo nome, 1: Login, 2: login nome";
$inst_text27 = "Alerta: N�o pode criar arquivo 'config.inc.php'!<br>
Instalala��o do diret�rio (chmod 777(unix) directorio e config.inc.php) para obter acesso.";
$inst_text28 = "Localiza��o do Database";
$inst_text28a = "Tipo de database sistema";
$inst_text29 = "Username para acesso";
$inst_text30 = "Password para acesso";
$inst_text31 = "Nome do Database";
$inst_text32 = "Primeira cor de fundo";
$inst_text33 = "Segunda cor de fundo";
$inst_text34 = "Terceira cor de fundo";
$inst_text35 = "Cor do evento nas tabelas";
$inst_text36 = "�cone da companhia yes = inserir nome da imagem";
$inst_text37 = "URL da homepage da companhia";
$inst_text38 = "no = deixe vazio";
$inst_text39 = "Primeira hora do dia";
$inst_text40 = "�ltima hora do dia";
$inst_text4a = "Ocorreu o seguinte erro ao criar a tabela: ";
$inst_text41 = "Tabela para manuseamento do ficheiro criada";
$inst_text42 = "Administra��o de ficheiros no = deixe vazio";
$inst_text43 = "yes = inserir localiza��o completa";
$inst_text44 = "e adicionalmente a localiza��o relativa para root";
$inst_text45 = "Tabela 'profile' (para perfis de membros) criada";
$inst_text46 = "Perfis yes = 1, no = 0";
$inst_text47 = "Tabela 'todo' (para itens a fazer) criada";
$inst_text48 = "Lista de itens a fazer yes = 1, no = 0";
$inst_text49 = "Tabela 'forum' (discuss�es,etc.) criada";
$inst_text50 = "Forum yes = 1, no = 0";
$inst_text51 = "Tabela 'votum' (para vota��es) criada";
$inst_text52 = "Sistema de vota��o yes = 1, no = 0";
$inst_text53 = "Tabela 'lesezeichen' (para favoritos) criada";
$inst_text54 = "Favoritos yes = 1, no = 0";
$inst_text55 = "Tabela 'ressourcen' (para administra��o de recursos adicionais) criada";
$inst_text56 = "Recursos yes = 1, no = 0";
$inst_text57 = "Tabela 'projekte' (para administra��o de projetos) Criada!";
$inst_text57a = "Tabela contatos (para contatos externos) Criada!";
$inst_text57b = "Tabela notas (para notas) Criada!";
$inst_text57c = "Tabela timecard (para sistema de time sheet) Criada!";
$inst_text57d = "Tabela grupos (para gerenciamento de grupos) Criada!";
$inst_text57e = "Tabela timeproj (relacionado tempo de trabalho a projetos) Criada!";
$inst_text57f = "Tabela rts and rts_cat (para o Suporte) Criada!";
$inst_text57g = "Tabela Conta_E-mail, E-mail_Anexo, Cliente_E-mail e Regras_E-mail (para a leitura de e-mails) Criada!";
$inst_text57h = "Tabela logs (para o usu�rio fazer login/logout) Criada!";
$inst_text57i = "Tabelas Perfis_contatos e Perfis_contatos_rel Criada!";
$inst_text58 = "Administra��o de projetos yes = 1, no = 0";
$inst_text59 = "Adicionalmente inserir recursos nos eventos";
$inst_text60 = "Endere�os  = 1, nein = 0";
$inst_text61 = "Correio expresso yes = 1, no = 0";
$inst_text62 = "Chat yes = 1, no = 0";
$inst_text63 = "'users' (para autentica��o e administra��o de endere�os)";
$inst_text64 = "Tabela 'termine' (para eventos) criada";
$inst_text65 = "Os seguintes membros foram inseridos com sucesso na tabela 'user':<br>
'root' - (Administrador geral)<br>
'test' - (membros regulares, com acesso restrito)";
$inst_text65b = "O grupo 'default' Foi criado";
$inst_text66 = "N�o altere nada al�m desta linha!";
$inst_text67 = " Erro da Database";
$inst_text68 = "Terminado";
$inst_text68a = "Houveram erros, por favor olhe a mensagem acima";
$inst_text69 = "Todas as tabelas necess�rias foram instaladas <br>
o arquivo de configura��o 'config.inc.php' foi re-escrito<br>
� uma boa id�ia fazer backup deste arquivo.<br>
Feche todas as janelas do seu Navegador agora.<br>";
$inst_text70 = "O administrador 'root' tem a senha 'root'. Por favor mude esta senha:";
$inst_text71 = "O usu�rio 'test' � membro do grupo 'default'.<br>
Agora voc� pode criar grupos novos e pode adicionar os novos usu�rios para o grupo";
$inst_text72 = "Para executar o programa PHProjekt no seu Browser v� a <b>index.php</b><br>

Teste a configura��o, em particular os modulos 'Correio expresso' e 'Arquivos'.";
$inst_text73a = "Lembretes";
$inst_text73b = "Alarme x minutos antes do evento";
$inst_text74 = "Caixa de alarme adicional";
$inst_text75 = "E-mail para o chefe";
$inst_text76 = "Sair/Voltar conta como : 1: Pausa - 0: Tempo de servi�o";
$inst_text77 = "As senhas ser�o encriptadas agora";
$inst_text77b = "Nomes de arquivos ser�o criptografados agora ...";
$inst_text78 = "Voc� quer fazer um backup do banco de dados agora? (E compacte junto com o  config.inc.php ...)<br>
Claro que voc� vai esperar!";
$inst_text79 = "Pr�ximo";
$inst_text80 = "Notifica��o de um novo evento em outros calend�rios";
$inst_text81 = "Caminho para o Envia FAX";
$inst_text81a = "Sem fax: Deixe em branco";
$inst_text82 = "Por favor leia a FAQ para instala��o com postgres";
$inst_text83 = "Tamanho dos Logins<br> (Numero de Letras: 3-6)";
$inst_text84 = "Se voc� quer instalar o PHProjekt manualmente, veja em
<a href='http://www.phprojekt.com/files/sql_dump.tar.gz' target=_blank>here</a> a mysql dump and a default config.inc.php";
$inst_text85 = "O servidor precisa de privil�gios para 'escrever' no diret�rio";
$inst_text86 = "Cabe�alho groupviews";
$inst_text86a = "nome, F.";
$inst_text86b = "Nome curto";
$inst_text86c = "Nome Login";
$inst_text87 = "Por favor crie o diret�rio";
$inst_text88 = "Mode padr�o para a estrutura do F�rum: 1 - open, 0 - closed";
$inst_text89 = "S�mbolo de moeda";
$inst_text89b = "Atual";
$inst_text90 = "Use LDAP";
$inst_text92 = "Permitir eventos paralelos";
$inst_text93 = "Diferen�a de fuso hor�rio [h] Servidor - usu�rio";
$inst_text93b = "Fuso Hor�rio";
$inst_text94 = "M�ximo de resultados exibidos na busca";
$inst_text95 = "N�o h� link para contatos em outros m�dulos";
$inst_text96 = "Registros da lista Highlight com 'mouseover'";
$inst_text97 = "Usu�rio deve fazer login/logout";

// l.php
$name_month = array("", "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");
$l_text1 = "Lista de recursos";
$l_text2 = "Lista de eventos";
$l_text3 = "Grupos mostrados";
$l_text4 = "Recursos";
$l_text4a = "Personal";
$l_text5 = "Criar &amp; Excluir eventos";
$l_text6 = "Dia";
$l_text7 = "Das";
$l_text8 = "at�";
$l_text9 = "Texto";
$l_text10 = "Nota";
$l_text11 = "Projeto";
$l_text12 = "Recurso";
$l_text13 = "Uma";
$l_text14 = "Diariamente";
$l_text15 = "1x/Semana";
$l_text16 = "1x/M�s";
$l_text17 = "1x/Ano";
$l_text18 = "at�";
$l_text19 = "Criar";
$l_text20 = "Excluir";
$l_text21a = "In�cio";
$l_text21b = "Sa�da do Escrit�rio";
$l_text21c = "Volta para o Escrit�rio";
$l_text21d = "Final";
$l_text22 = "@work";
$l_text23 = "Semana";
$l_text24 = "Eventos de Grupo";
$l_text25 = "ou Perfil";
$l_text26 = "Evento para o dia inteiro";
$l_text30 = "Tempo-axis:";
$l_text30a = "Vertical";
$l_text30b = "Horizontal";
$l_text30c = "Hor. seta";
$l_text31 = "-intervalo:";
$l_text31a = array("padr�o", "15 min.", "30 min.", " 1 horas", " 2 horas", " 4 horas", " 1 dia");
$l_text31b = array(0, 15, 30, 60, 120, 240, 1440);
$l_text32 = "Pr�prio";
$l_text33 = "Grupo";
$l_text34 = "...escrever";

//login.php
$log_text1 = "Por favor execute login.php!";

// m1.php
$name_day = array("Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado");
$name_day2 = array("Seg", "Ter", "Qua", "Qui", "Sex", "Sab","Dom");
$m1_text1 = "Existem outros eventos!<br>o principal apontamento �: ";
$m1_text2 = "Este recurso j� se encontra ocupado: ";
$m1_text3 = " Este evento n�o existe.<br> <br> Verifique a data e a hora. ";
$m1_text4 = "Verifique o formato da data e hora! ";
$m1_text5 = "Verifique a data!";
$m1_text6 = "Verifique a hora de inicio! ";
$m1_text7 = "Verifique a hora a que termina! ";
$m1_text8 = "Introduza texto ou nota!";
$m1_text9 = "Verifique hora de inicio e fim! ";
$m1_text10 = "Verifique o formato do fim da data! ";
$m1_text11 = "Verifique o fim da data! ";
$m1_text12 = "Lista de eventos";
$m1_text1a = "Lista de recursos";
$m1_text13 = "Dia";

$m1_text14 = "In�cio";
$m1_text15 = "T�rmino";
$m1_text16 = "Recurso";
$m1_text17 = "Membro";
$m1_text18 = "Texto";
$m1_text19 = "Excluir evento";
$m1_text24 = "Cidade";
$m1_text25 = "Nome";
$m1_text26 = "Nome";
$m1_text26b = "Contato";
$m1_text26b1 = "Participante";
$m1_text27 = "Login";
$m1_text28 = "Empresa";
$m1_text281 = "Grupo";
$m1_text29 = "Telefone";
$m1_text30 = "Fax";
$m1_text30b = "Observa��es";
$m1_text30c = "Categoria";
$m1_text31 = "Endere�o";
$m1_text32 = "Cidade";
$m1_text33 = "Pa�s";
$m1_text34 = "Favoritos";
$m1_text35 = "Descri��o";
$m1_text36 = "Coment�rio";
$m1_text37 = "Dentro da lista";
$m1_text38 = "Grupo";
$m1_text39 = "Novo evento";
$m1_text40 = "Criado por";
$m1_text41 = "Bot�o vermelho -> apaga um evento di�rio. ";
$m1_text42 = "M�ltiplos eventos";
$m1_text43 = "Ver ano";
$m1_text44 = "Calend�rio semana";
$m1_text45 = "+ Info";
$m1_text46 = "�rea";
$m1_text47 = "Empresas";
$m1_text47b = "Receber notifica��es por e-mail";

//m2.php
$m2_text1 = "Criar &amp; Excluir eventos";
$m2_text2 = "Normal";
$m2_text3 = "Privado";
$m2_text4 = "P�blico";
$m2_text5 = "Vis�vel";

// o.php
$o_adresses = "CONTATOS";
$o_chat = "CHAT";
$o_comunicacao = "COMUNICA��O";
$o_conhecimento = "CONHECIMENTO";
$o_colaboracao = "COLABORA��O";
$o_ferramentas = "FERRAMENTAS";
$o_forum1 = "LI��ES APRENDIDAS";
$o_forum2 = "F�RUM DISCUSS�ES";
$o_manuais = "MANUAIS";
$o_marketing = "MARKETING";
$o_formularios = "FORMUL�RIOS";
$o_apresentacoes = "APRESENTA��ES";
$o_imprensa = "IMPRENSA";
$o_comunicados = "COMUNICADOS";
$o_novasfranquias = "NOVAS FRANQUIAS";
$o_projects = "PROJETOS";

$mail_modules["imprensa"]="IMPRENSA";
$mail_modules["comunicacao"]="COMUNICA��O";
$mail_modules["conhecimento"]="CONHECIMENTO";
$mail_modules["manuais"]="MANUAIS";
$mail_modules["marketing"]="MARKETING";
$mail_modules["formularios"]="FORMUL�RIOS";
$mail_modules["apresentacoes"]="APRESENTA��ES";
$mail_modules["comunicados"]="COMUNICADOS";
$mail_modules["novasfranquias"]="NOVAS FRANQUIAS";


$o_opt = "Op��es";
$o_opt2 = "Excluir";
$o_rts = "SUPORTE";
$o_admin = "ADMIN";
$o_email = "E-MAIL";
$o_logout = "SAIR";
$o_summary = "SUM�RIO";

// options.php
$opt_bm1 = "Descri��o:";
$opt_bm2 = "Coment�rio:";
$opt_bm3 = "Insira um E-mail v�lido! ";
$opt_bm4 = "Especifique uma descri��o!";
$opt_bm5 = "Este endere�o j� existe com uma descri��o diferente";
$opt_bm6 = " j� existe. ";
$opt_bm7 = "Est� inserido na lista de favoritos.";
$opt_bm8 = " foi alterado.";
$opt_bm9 = " foi eliminado.";
$opt_pro1 = "Especifique uma descri��o! ";
$opt_pro2 = "Selecione pelo menos um nome! ";
$opt_pro3 = " foi criado como perfil.<br> Assim que o calend�rio for atualizado o perfil ficar� ativo.";
$opt_pro4 = "foi alterado.<br> Assim que o calend�rio for atualizado o perfil fica activo.";
$opt_pro5 = "O perfil foi eliminado.";
$opt_vote1 = "Especifique uma quest�o para votar.";
$opt_vote2 = "Complete todas as respostas da enquete.";
$opt_vote3 = "Enquete publicada.";
$opt_vote4 = "Selecione pelo menos um usu�rio.";
$opt_vote5 = "Selecione pelo menos um grupo.";
$opt_bm_1 = "<h2>Favoritos</h2>Aqui pode criar, modificar e excluir favoritos:";
$opt_2 = "Criar";
$opt_3 = "Modificar";
$opt_4 = "Excluir";
$opt_pro_1 = "<h2>Perfis</h2>Aqui voc� pode criar, modificar e excluir perfis:";
$opt_vote_1 = "<h2>Formul�rio da enquete</h2>";
$opt_vote_2 = "Aqui voc� pode deixar uma enquete direcionada a outros membros.";
$opt_vote_3 = "Quest�o:";
$opt_vote_4 = "�nica escolha";
$opt_vote_5 = "M�ltipla escolha";
$opt_vote_6 = "Op��o";
$opt_vote_7 = "Participantes:";
$opt_text1 = " ou ";
$opt_text2 = "<h3>Mudan�a de senha</h3> Nesta se��o voc� escolhe uma senha aleat�ria gerada para voc�.";
$opt_text3 = "Senha Antiga";
$opt_text4 = "Cria uma nova senha";
$opt_text5 = "Salva senha";
$opt_text6 = "Sua nova senha foi armazenada";
$opt_text7 = "Senha Errada !";
$opt_text8 = "Apaga Voto";
$opt_for1 = "<h4>Exclui assuntos do F�rum</h4> Aqui voc� pode excluir
seus assuntos <br> Somente assuntos sem coment�rios aparecer�o.";
$opt_text10 = "Troca de Senha";
$opt_text11 = "Senha Antiga";
$opt_text12 = "Nova senha";
$opt_text13 = "Redigite a senha";
$opt_text14 = "A nova senha precisa ter no m�nimo 5 letras";
$opt_text15 = "Voc� n�o redigitou a nova senha corretamente";
$opt_text16 = "Descri��o";
$opt_text17 = "Mostra bookings";
$opt_text18 = "Caracteres V�lidos";
$opt_text19 = "Sugest�o";
$opt_text20 = 'Coloque a palavra "AND" entre as palavras, caso deseje buscar mais de uma'; // n�o modifique a palavra AND
$opt_text21 = "Escrever acesso para calend�rio";
$opt_text22 = "Escrever acesso para outros usu�rios para seu calend�rio";
$opt_text23 = "Usu�rio como Administrador ainda tem acesso de escrita";
$opt_vote_24 = "Tipo";

// projects
$proj_list = "Lista de Projetos";
$proj_name = "Descri��o do projeto";
$proj_start = "In�cio";
$proj_end = "T�rmino";
$proj_pers = "Participantes";
$proj_prio = "Prioridade";
$proj_prio1 = "Alta";
$proj_prio2 = "M�dia";
$proj_prio3 = "Baixa";
$proj_stat = "Completo";
$proj_status = "Situa��o";
$proj_chan = "�ltima <br>Altera��o";
$proj_chef = "Respons�vel";
$proj_stat2 = "Estat�sticas";
$proj_stat3 = "Minha Estat�stica";
$proj_name2 = "Nome do projeto";
$proj_lider_name = "Selecione o l�der do projeto.";
$proj_pers2 = "Pessoa";
$proj_time2 = "Horas";
$proj_sum = "Sum�rio do projeto";
$proj_text2 = " Escolha a combina��o Projeto/Pessoa";
$proj_text3 = "(sele��o m�ltipla com a tecla 'Ctrl')";
$proj_text4 = "Projetos";
$proj_text5 = "Pessoas";
$proj_text6 = "In�cio:";
$proj_text7 = "T�rmino:";
$proj_text8 = "Todos";
$proj_text9 = "Tempo de trabalho anotado em";
$proj_text10 = "Subprojeto de";
$proj_text11 = "Objetivo";
$proj_text12 = "Contato";
$proj_text13 = "Taxa de horas";
$proj_text14 = "Custo Calculado";
$proj_text15 = "Novo subprojeto";
$proj_text16 = "Anotado at� agora";
$proj_text17 = "Or�amento";
$proj_text18 = "Lista detalhada";
$proj_text19 = "Linha do tempo";
$proj_text20 = "Iniciado";
$proj_text21 = "Ordenado";
$proj_text23 = "Trabalhando";
$proj_text24 = "Conclu�do";
$proj_text25 = "Parado";
$proj_text26 = "Re-aberto";
$proj_text27 = "Aguardando";
$proj_text28 = "Somente projetos principais";
$proj_text28a = "Somente este projeto";
$proj_text29 = "Come�ar > Terminar";
$proj_text30 = "Formato ISO: dd/mm/aaaa";
$proj_text31 = "O per�odo do sub-projeto deve coincidir com o per�odo do projeto principal.";
$proj_text32 = "Escolha pelo menos uma pessoa";
$proj_text33 = "Escolha pelo menos um projeto";
$proj_text34 = "Depend�ncia";
$proj_text35a = "Anterior";
$proj_text35b = "Pr�ximo";
$proj_text36 = "N�o � poss�vel come�ar antes do fim do projeto";
$proj_text37 = "N�o � poss�vel come�ar antes do in�cio do projeto";
$proj_text38 = "N�o � poss�vel terminar antes do in�cio do projeto";
$proj_text39 = "N�o � poss�vel terminar antes do fim do projeto";
$proj_text40 = "Aten��o, viola��o de depend�ncia";
$proj_text41 = "Cofre";
$proj_text42 = "Projeto externo";
$proj_text43 = "Escolha autom�tica";
$proj_text44 = "Legenda";
$proj_text45 = "Sem valor";
$proj_text46 = "A data de inicio deve ser anterior � data final";
$proj_text47 = "Incluir novo projeto";

// r.php
$r_status = "Verifique status!";
$r_search = "Busca";
$r_search21 = "Li��es Aprendidas";
$r_search22 = "F�rum Discuss�es";
$r_search23 = "An�ncios";
$r_search3m = "Manuais";
$r_search3mk = "Marketing";
$r_search3f = "Formul�rios";
$r_search3a = "Apresenta��es";
$r_search3i = "Imprensa";
$r_search3c = "Comunicados";
$r_search3nf = "Novas Franquias";
$r_search4 = "Endere�os";
$r_search5 = "Extendido";
$r_search6 = "Todos os m�dulos";
$r_bmlist = "Lista";
$r_projtitle = "Projetos:";
$r_proj1 = "Nome";
$r_proj2 = "Data final";
$r_proj3 = "Status";
$r_votetitle = "Enquetes";
$r_votelist = "Lista";
$r_vote1 = "Enquete criada em ";
$r_notes1 = "Notas";

// helpdesk.php
$rts_0 = "Solicita��o";
$rts_1 = "Solicitac�o de Suporte";
$rts_2 = "Solicita��es";
$rts_3 = "Fila de Espera";
$rts_4 = "Pesquisar no Banco de Dados";
$rts_5 = "Palavra-Chave";
$rts_6 = "Resultados";
$rts_7 = "Formulario de Solicita��o";
$rts_8 = "Digite a palavra-chave";
$rts_9 = "Digite seu E-mail";
$rts_10 = "Digite o titulo da sua solicita��o";
$rts_11 = "Descreva seu problema";
$rts_12 = "Categoria";
$rts_13 = "Data da Solicita��o";
$rts_14 = "Dias";
$rts_15 = "Desculpe, voc� n�o est� na lista";
$rts_16 = "O numero do seu chamado �";
$rts_17 = "Usu�rio";
$rts_18 = "Titulo";
$rts_19 = "Data";
$rts_20 = "Status";
$rts_21 = "Busca";
$rts_22 = "em";
$rts_23 = "Todos os campos";
$rts_24 = "Texto";
$rts_25 = "Solicita��o";
$rts_26 = "Resposta";
$rts_27 = "e";
$rts_28 = "Situa��o";
$rts_29 = "Pendente";
$rts_30 = "No Aguardo";
$rts_31 = "Transferido";
$rts_32 = "Resolvido";
$rts_33 = "Enviado";
$rts_34 = "Assinado";
$rts_35 = "Prioridade";
$rts_36 = "Acesso";
$rts_37 = "Respons�vel";
$rts_38 = "Prioridade";
$rts_39 = "Atualizar arquivo";
$rts_40 = "Atualizar";
$rts_41 = "Resolver";
$rts_42 = "No Aguardo";
$rts_43 = "Cancelar";
$rts_44 = "Transferir para Solicita��o";
$rts_45 = "Prezado usu�rio,<br><br>
Recebemos sua solicita��o com sucesso. <br>
Ela ser� respondida em at� 02 (dois) dias �teis.<br>
Para acompanh�-la, acesse o link www.starpoint.com.br/extranet.<br><br>
Atenciosamente,<br>";
$rts_46 = "Sua Solicita��o foi registrada em nosso sistema.<br>
Voc� receber� um e-mail de confirma��o em alguns instantes.";
$rts_47 = "n/d";
$rts_48 = "Interno";
$rts_49 = "Aberto";
$rts_50 = "O seguinte chamado foi redirecionado";
$rts_51 = "Criar nova Solicita��o";
$rts_52 = "Hor�rio de Trabalho";
$rts_53 = "Solicita��o";
$rts_53 = "Para: ";
$rts_54 = "Prioridade";
$rts_55 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$rts_56 = "Resposta para seu pedido, N� ";
$rts_57 = "Buscar novo pedido por E-mail";
$rts_58 = "Seu pedido foi respondido por ";
$rts_59 = "Resposta para seu pedido, N� ";
$rts_60 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$rts_61 = "Voltar";
$rts_62 = "Respostas anteriores";
$rts_63 = "Adicionar resposta";
$rts_64 = "T�tulo da Solicita��o";
$rts_65 = "Respons�vel";
$rts_66 = "responde";
$rts_67 = "Email";
$rts_67 = "Redirecionar para";
$rts_68 = "Alta";
$rts_69 = "M�dia";
$rts_70 = "Baixa";
$rts_71 = "N�";
$rts_72 = "Atualiza��o";
$rts_73 = "Solicitante";
$rts_74 = "Excluir �ltima resposta";
$rts_75 = "Extranet Starpoint - Envio de Arquivo";

$rts_100 = "N�";
$rts_101 = "Assunto";
$rts_102 = "Enviado em";
$rts_103 = "Data Limite";
$rts_104 = "Situa��o";
$rts_105 = "Respostas";
$rts_106 = "Sem resposta";


$rts_110 = "Destinat�rio";
$rts_111 = "Data de Envio";
$rts_112 = "Lido em";
$rts_113 = "Respondido em";
$rts_114 = "Situa��o";

$rts_120 = "N�";
$rts_121 = "Solicitante";
$rts_122 = "Assunto";
$rts_123 = "Data envio";
$rts_124 = "Prioridade";
$rts_125 = "�ltima resposta";
$rts_126 = "Situa��o";

$rts_130 = "Solicita��es/P�gina";

$rts_140 = "Nenhuma solicita��o encontrada";

$rts_150 = "Assunto";
$rts_151 = "Destinat�rios"; 


// chamados.php
$chamados_0 = "Solicita��o";
$chamados_1 = "Solicitac�o de Suporte";
$chamados_2 = "Solicita��es Pendentes";
$chamados_3 = "Fila de Espera";
$chamados_4 = "Pesquisar no Banco de Dados";
$chamados_5 = "Palavra-Chave";
$chamados_6 = "Resultados";
$chamados_7 = "Registro de Ocorr�ncia";
$chamados_8 = "Digite a palavra-chave";
$chamados_9 = "Digite seu E-mail";
$chamados_10 = "Digite o titulo da sua solicita��o";
$chamados_11 = "Descreva seu problema";
$chamados_12 = "Categoria";
$chamados_13 = "Data da Solicita��o";
$chamados_14 = "Dias";
$chamados_15 = "Desculpe, voc� n�o est� na lista";
$chamados_16 = "O numero do seu chamado �";
$chamados_17 = "Remetente";
$chamados_18 = "Titulo";
$chamados_19 = "Data";
$chamados_20 = "Situa��o";
$chamados_21 = "Busca";
$chamados_22 = "em";
$chamados_23 = "Todos os campos";
$chamados_24 = "Texto";
$chamados_25 = "Solicita��o";
$chamados_26 = "Resposta";
$chamados_27 = "e";
$chamados_28 = "Situa��o";
$chamados_29 = "Pendente";
$chamados_30 = "Em andamento";
$chamados_31 = "Resolvido";
$chamados_32 = "N�o-resolvido";
$chamados_33 = "Enviado";
$chamados_34 = "Assinado";
$chamados_35 = "Prioridade";
$chamados_36 = "Acesso";
$chamados_37 = "Respons�vel";
$chamados_38 = "Prioridade";
$chamados_39 = "Atualizar dados da parte superior da tela";
$chamados_40 = "Atualizar";
$chamados_41 = "Resolver";
$chamados_42 = "No Aguardo";
$chamados_43 = "Cancelar";
$chamados_44 = "Transferir para Solicita��o";
$chamados_45 = "Prezado usu�rio,<br><br>
Recebemos sua solicita��o com sucesso. <br>
Ela ser� respondida em at� 02 (dois) dias �teis.<br>
Para acompanh�-la, acesse o link www.quiosquechoppdabrahma.com.br/extranet.<br><br>
Atenciosamente,<br>";
$chamados_46 = "Sua Solicita��o foi registrada em nosso sistema.<br>
Voc� receber� um e-mail de confirma��o em alguns instantes.";
$chamados_47 = "n/d";
$chamados_48 = "Interno";
$chamados_49 = "Aberto";
$chamados_50 = "O seguinte chamado foi redirecionado";
$chamados_51 = "Nova ocorr�ncia";
$chamados_52 = "Hor�rio de Trabalho";
$chamados_53 = "Solicita��o";
$chamados_53 = "Para: ";
$chamados_54 = "Prioridade";
$chamados_55 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamados_56 = "Resposta para seu pedido, N� ";
$chamados_57 = "Buscar novo pedido por E-mail";
$chamados_58 = "Seu pedido foi respondido por ";
$chamados_59 = "Resposta para seu pedido, N� ";
$chamados_60 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamados_61 = "Voltar";
$chamados_62 = "Respostas anteriores";
$chamados_63 = "Adicionar resposta";
$chamados_64 = "T�tulo da Solicita��o";
$chamados_65 = "Respons�vel";
$chamados_66 = "responde";
$chamados_67 = "Email";
$chamados_67 = "Redirecionar para";
$chamados_68 = "Alta";
$chamados_69 = "M�dia";
$chamados_70 = "Baixa";
$chamados_71 = "N�";
$chamados_72 = "Atualiza��o";
$chamados_73 = "Remetente";
$chamados_74 = "Excluir �ltima resposta";
$chamados_75 = "Ocorr�ncia";
$chamados_76 = "Unidade";
$chamados_77 = "Dura��o";
$chamados_78 = "Data de ocorr�ncia";
$chamados_79 = "SLA";
$chamados_80 = "Unidade";
$chamados_81 = "Descri��o";
$chamados_82 = "Previs�o Atendimento";
$chamados_83 = "Anexo";
$chamados_84 = "Ocorr�ncia";
$chamados_85 = "Copiados";
$chamados_86 = "Gerenciar ocorr�ncias";
$chamados_87 = "Setor";





// chamadosx.php
$chamadosx_0 = "Solicita��o";
$chamadosx_1 = "Solicitac�o de Suporte";
$chamadosx_2 = "Solicita��es Pendentes";
$chamadosx_3 = "Fila de Espera";
$chamadosx_4 = "Pesquisar no Banco de Dados";
$chamadosx_5 = "Palavra-Chave";
$chamadosx_6 = "Resultados";
$chamadosx_7 = "Registro de Ocorr�ncia";
$chamadosx_8 = "Digite a palavra-chave";
$chamadosx_9 = "Digite seu E-mail";
$chamadosx_10 = "Digite o titulo da sua solicita��o";
$chamadosx_11 = "Descreva seu problema";
$chamadosx_12 = "Categoria";
$chamadosx_13 = "Data da Solicita��o";
$chamadosx_14 = "Dias";
$chamadosx_15 = "Desculpe, voc� n�o est� na lista";
$chamadosx_16 = "O numero do seu chamado �";
$chamadosx_17 = "Remetente";
$chamadosx_18 = "Titulo";
$chamadosx_19 = "Data";
$chamadosx_20 = "Situa��o";
$chamadosx_21 = "Busca";
$chamadosx_22 = "em";
$chamadosx_23 = "Todos os campos";
$chamadosx_24 = "Texto";
$chamadosx_25 = "Solicita��o";
$chamadosx_26 = "Resposta";
$chamadosx_27 = "e";
$chamadosx_28 = "Situa��o";
$chamadosx_29 = "Pendente";
$chamadosx_30 = "Em andamento";
$chamadosx_31 = "Resolvido";
$chamadosx_32 = "N�o-resolvido";
$chamadosx_33 = "Enviado";
$chamadosx_34 = "Assinado";
$chamadosx_35 = "Prioridade";
$chamadosx_36 = "Acesso";
$chamadosx_37 = "Respons�vel";
$chamadosx_38 = "Prioridade";
$chamadosx_39 = "Atualizar dados da parte superior da tela";
$chamadosx_40 = "Atualizar";
$chamadosx_41 = "Resolver";
$chamadosx_42 = "No Aguardo";
$chamadosx_43 = "Cancelar";
$chamadosx_44 = "Transferir para Solicita��o";
$chamadosx_45 = "Prezado usu�rio,<br><br>
Recebemos sua solicita��o com sucesso. <br>
Ela ser� respondida em at� 02 (dois) dias �teis.<br>
Para acompanh�-la, acesse o link www.quiosquechoppdabrahma.com.br/extranet.<br><br>
Atenciosamente,<br>";
$chamadosx_46 = "Sua Solicita��o foi registrada em nosso sistema.<br>
Voc� receber� um e-mail de confirma��o em alguns instantes.";
$chamadosx_47 = "n/d";
$chamadosx_48 = "Interno";
$chamadosx_49 = "Aberto";
$chamadosx_50 = "O seguinte chamado foi redirecionado";
$chamadosx_51 = "Nova ocorr�ncia";
$chamadosx_52 = "Hor�rio de Trabalho";
$chamadosx_53 = "Solicita��o";
$chamadosx_53 = "Para: ";
$chamadosx_54 = "Prioridade";
$chamadosx_55 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamadosx_56 = "Resposta para seu pedido, N� ";
$chamadosx_57 = "Buscar novo pedido por E-mail";
$chamadosx_58 = "Seu pedido foi respondido por ";
$chamadosx_59 = "Resposta para seu pedido, N� ";
$chamadosx_60 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamadosx_61 = "Voltar";
$chamadosx_62 = "Respostas anteriores";
$chamadosx_63 = "Adicionar resposta";
$chamadosx_64 = "T�tulo da Solicita��o";
$chamadosx_65 = "Respons�vel";
$chamadosx_66 = "responde";
$chamadosx_67 = "Email";
$chamadosx_67 = "Redirecionar para";
$chamadosx_68 = "Alta";
$chamadosx_69 = "M�dia";
$chamadosx_70 = "Baixa";
$chamadosx_71 = "N�";
$chamadosx_72 = "Atualiza��o";
$chamadosx_73 = "Remetente";
$chamadosx_74 = "Excluir �ltima resposta";
$chamadosx_75 = "Ocorr�ncia";
$chamadosx_76 = "Unidade";
$chamadosx_77 = "Dura��o";
$chamadosx_78 = "Data de ocorr�ncia";
$chamadosx_79 = "SLA";
$chamadosx_80 = "Unidade";
$chamadosx_81 = "Descri��o";
$chamadosx_82 = "Previs�o Atendimento";
$chamadosx_83 = "Anexo";
$chamadosx_84 = "Ocorr�ncia";
$chamadosx_85 = "Copiados";
$chamadosx_86 = "Gerenciar ocorr�ncias";
$chamadosx_87 = "Setor";



// chamados.php
$chamados2_0 = "Solicita��o";
$chamados2_1 = "Solicitac�o de Suporte";
$chamados2_2 = "Solicita��es Pendentes";
$chamados2_3 = "Fila de Espera";
$chamados2_4 = "Pesquisar no Banco de Dados";
$chamados2_5 = "Palavra-Chave";
$chamados2_6 = "Resultados";
$chamados2_7 = "Registro de Ocorr�ncia";
$chamados2_8 = "Digite a palavra-chave";
$chamados2_9 = "Digite seu E-mail";
$chamados2_10 = "Digite o titulo da sua solicita��o";
$chamados2_11 = "Descreva seu problema";
$chamados2_12 = "Categoria";
$chamados2_13 = "Data da Solicita��o";
$chamados2_14 = "Dias";
$chamados2_15 = "Desculpe, voc� n�o est� na lista";
$chamados2_16 = "O numero do seu chamado �";
$chamados2_17 = "Remetente";
$chamados2_18 = "Titulo";
$chamados2_19 = "Data";
$chamados2_20 = "Situa��o";
$chamados2_21 = "Busca";
$chamados2_22 = "em";
$chamados2_23 = "Todos os campos";
$chamados2_24 = "Texto";
$chamados2_25 = "Solicita��o";
$chamados2_26 = "Resposta";
$chamados2_27 = "e";
$chamados2_28 = "Situa��o";
$chamados2_29 = "Pendente";
$chamados2_30 = "Em andamento";
$chamados2_31 = "Resolvido";
$chamados2_32 = "N�o-resolvido";
$chamados2_33 = "Enviado";
$chamados2_34 = "Assinado";
$chamados2_35 = "Prioridade";
$chamados2_36 = "Acesso";
$chamados2_37 = "Respons�vel";
$chamados2_38 = "Prioridade";
$chamados2_39 = "Atualizar dados da parte superior da tela";
$chamados2_40 = "Atualizar";
$chamados2_41 = "Resolver";
$chamados2_42 = "No Aguardo";
$chamados2_43 = "Cancelar";
$chamados2_44 = "Transferir para Solicita��o";
$chamados2_45 = "Prezado usu�rio,<br><br>
Recebemos sua solicita��o com sucesso. <br>
Ela ser� respondida em at� 02 (dois) dias �teis.<br>
Para acompanh�-la, acesse o link www.quiosquechoppdabrahma.com.br/extranet.<br><br>
Atenciosamente,<br>";
$chamados2_46 = "Sua Solicita��o foi registrada em nosso sistema.<br>
Voc� receber� um e-mail de confirma��o em alguns instantes.";
$chamados2_47 = "n/d";
$chamados2_48 = "Interno";
$chamados2_49 = "Aberto";
$chamados2_50 = "O seguinte chamado foi redirecionado";
$chamados2_51 = "Nova ocorr�ncia";
$chamados2_52 = "Hor�rio de Trabalho";
$chamados2_53 = "Solicita��o";
$chamados2_53 = "Para: ";
$chamados2_54 = "Prioridade";
$chamados2_55 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamados2_56 = "Resposta para seu pedido, N� ";
$chamados2_57 = "Buscar novo pedido por E-mail";
$chamados2_58 = "Seu pedido foi respondido por ";
$chamados2_59 = "Resposta para seu pedido, N� ";
$chamados2_60 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$chamados2_61 = "Voltar";
$chamados2_62 = "Respostas anteriores";
$chamados2_63 = "Adicionar resposta";
$chamados2_64 = "T�tulo da Solicita��o";
$chamados2_65 = "Respons�vel";
$chamados2_66 = "responde";
$chamados2_67 = "Email";
$chamados2_67 = "Redirecionar para";
$chamados2_68 = "Alta";
$chamados2_69 = "M�dia";
$chamados2_70 = "Baixa";
$chamados2_71 = "N�";
$chamados2_72 = "Atualiza��o";
$chamados2_73 = "Remetente";
$chamados2_74 = "Excluir �ltima resposta";
$chamados2_75 = "Ocorr�ncia";
$chamados2_76 = "Unidade";
$chamados2_77 = "Dura��o";
$chamados2_78 = "Data de ocorr�ncia";
$chamados2_79 = "SLA";
$chamados2_80 = "Unidade";
$chamados2_81 = "Descri��o";
$chamados2_82 = "Previs�o Atendimento";
$chamados2_83 = "Anexo";
$chamados2_84 = "Ocorr�ncia";
$chamados2_85 = "Copiados";
$chamados2_86 = "Gerenciar ocorr�ncias";
$chamados2_87 = "Setor";




// pedidos.php
$pedidos_0 = "Solicita��o";
$pedidos_1 = "Solicitac�o de Suporte";
$pedidos_2 = "Solicita��es Pendentes";
$pedidos_3 = "Fila de Espera";
$pedidos_4 = "Pesquisar no Banco de Dados";
$pedidos_5 = "Palavra-Chave";
$pedidos_6 = "Resultados";
$pedidos_7 = "Formul�rio de Solicita��o";
$pedidos_8 = "Digite a palavra-chave";
$pedidos_9 = "Digite seu E-mail";
$pedidos_10 = "Digite o titulo da sua solicita��o";
$pedidos_11 = "Descreva seu problema";
$pedidos_12 = "Categoria";
$pedidos_13 = "Data da Solicita��o";
$pedidos_14 = "Dias";
$pedidos_15 = "Desculpe, voc� n�o est� na lista";
$pedidos_16 = "O numero do seu chamado �";
$pedidos_17 = "Usu�rio";
$pedidos_18 = "Titulo";
$pedidos_19 = "Data";
$pedidos_20 = "Situa��o";
$pedidos_21 = "Busca";
$pedidos_22 = "em";
$pedidos_23 = "Todos os campos";
$pedidos_24 = "Texto";
$pedidos_25 = "Solicita��o";
$pedidos_26 = "Resposta";
$pedidos_27 = "e";
$pedidos_28 = "Situa��o";
$pedidos_29 = "Pendente";
$pedidos_30 = "No�Aguardo";
$pedidos_31 = "Transferido";
$pedidos_32 = "Resolvido";
$pedidos_33 = "Enviado";
$pedidos_34 = "Assinado";
$pedidos_35 = "Prioridade";
$pedidos_36 = "Acesso";
$pedidos_37 = "Respons�vel";
$pedidos_38 = "Prioridade";
$pedidos_39 = "Atualizar dados da parte superior da tela";
$pedidos_40 = "Atualizar";
$pedidos_41 = "Resolver";
$pedidos_42 = "No Aguardo";
$pedidos_43 = "Cancelar";
$pedidos_44 = "Transferir para Solicita��o";
$pedidos_45 = "Prezado usu�rio,<br><br>
Recebemos sua solicita��o com sucesso. <br>
Ela ser� respondida em at� 02 (dois) dias �teis.<br>
Para acompanh�-la, acesse o link www.quiosquechoppdabrahma.com.br/extranet.<br><br>
Atenciosamente,<br>";
$pedidos_46 = "Sua Solicita��o foi registrada em nosso sistema.<br>
Voc� receber� um e-mail de confirma��o em alguns instantes.";
$pedidos_47 = "n/d";
$pedidos_48 = "Interno";
$pedidos_49 = "Aberto";
$pedidos_50 = "O seguinte chamado foi redirecionado";
$pedidos_51 = "Nova Solicita��o";
$pedidos_52 = "Hor�rio de Trabalho";
$pedidos_53 = "Solicita��o";
$pedidos_53 = "Para: ";
$pedidos_54 = "Prioridade";
$pedidos_55 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$pedidos_56 = "Resposta para seu pedido, N� ";
$pedidos_57 = "Buscar novo pedido por E-mail";
$pedidos_58 = "Seu pedido foi respondido por ";
$pedidos_59 = "Resposta para seu pedido, N� ";
$pedidos_60 = "Sua solu��o foi enviada para o cliente e escrito no Banco de Dados.";
$pedidos_61 = "Voltar";
$pedidos_62 = "Respostas anteriores";
$pedidos_63 = "Adicionar resposta";
$pedidos_64 = "T�tulo da Solicita��o";
$pedidos_65 = "Respons�vel";
$pedidos_66 = "responde";
$pedidos_67 = "Email";
$pedidos_67 = "Redirecionar para";
$pedidos_68 = "Alta";
$pedidos_69 = "M�dia";
$pedidos_70 = "Baixa";
$pedidos_71 = "N�";
$pedidos_72 = "Atualiza��o";
$pedidos_73 = "Solicitante";
$pedidos_74 = "Excluir �ltima resposta";
$pedidos_75 = "Tipo";
$pedidos_75b = "Produto";
$pedidos_76 = "Grupo";
$pedidos_77 = "Dura��o";
$pedidos_78 = "Data Solicita��o";
$pedidos_79 = "SLA";
$pedidos_80 = "Grupo Solucionador";
$pedidos_81 = "Mensagem";
$pedidos_82 = "Previs�o Atendimento";
$pedidos_83 = "Anexo";
$pedidos_84 = "Tipo da Solicita��o";
$pedidos_85 = "Copiados";



// Resumo
$sum_forum2 = "F�rum de Discuss�es";
$sum_filesm = "Novos manuais";
$sum_filesmk = "Novos materiais de marketing";
$sum_filesf = "Novos formul�rios";
$sum_filesa = "Novas apresenta��es";
$sum_votes = "Enquetes";
$sum_projects = "Projetos correntes";
$sum_helpdesk = "D�vidas no Suporte";

// votum.php
$votum_title = "Resultados da vota��o: ";
$votum_text1 = "Quest�o para vota��o: ";
$votum_text2 = "S�o poss�veis v�rias respostas";
$votum_text3 = "Resposta ";
$votum_text4 = "Branco: ";
$votum_text5 = "Dos";
$votum_text6 = "Participantes j� votaram";
$votum_text7 = "Enquetes em Aberto";
$votum_text8 = "Enquetes j� votadas";


//faq.php
$faq_name = "Perguntas mais frequentes";
$post_by = "enviado por";
$new_question = "Nova pergunta";
$question = "Pergunta";
$answer = "Resposta";
$poster = "Enviado por";
$all = "Todos";
$new_question = "Nova pergunta adicionada";

// summary.php
$summary01 = "Imprensa";
$summary02 = "Comunicados";
$summary03 = "Busca";
$summary04 = "Manuais";
$summary05 = "Formul�rios";
$summary06 = "Projetos";
$summary07 = "Calend�rio";

$selected_directory = "Filtrar diret�rio";
$items_directory = "Itens por diret�rio";

$question_text = "Quest�o";
$answer_text = "Resposta";

$arquivos = "FILTRO";
$arquivos1 = "BUSCA";
$arquivos2 = "Palavra-chave";
$arquivos3 = "Em";

$vars_situacao['R'] = "Resolvida";
$vars_situacao['P'] = "N�o lida";
$vars_situacao['E'] = "Encaminhada";
$vars_situacao['A'] = "Pendente";
$vars_situacao['B'] = "Reaberta";
$vars_situacao['D'] = "N�o-resolvida";

$calendario_situacao['C'] = "Confirmado";
$calendario_situacao['D'] = "Cancelado";
$calendario_situacao['A'] = "A Confirmar";

$calendariox_situacao['C'] = "Confirmado";
$calendariox_situacao['D'] = "Cancelado";
$calendariox_situacao['A'] = "A Confirmar";

$calendario_tipo[0] = "Videoconferencia";
$calendario_tipo[1] = "Congresso";
$calendario_tipo[2] = "Reuni�o em sala";
$calendario_tipo[3] = "Confraterniza��o";
$calendario_tipo[4] = "Outros";


$lang_prioridade['A'] = "Alta";
$lang_prioridade['M'] = "M�dia";
$lang_prioridade['B'] = "Baixa";


$mes_array[1] = "Janeiro";
$mes_array[2] = "Fevereiro";
$mes_array[3] = "Mar�o";
$mes_array[4] = "Abril";
$mes_array[5] = "Maio";
$mes_array[6] = "Junho";
$mes_array[7] = "Julho";
$mes_array[8] = "Agosto";
$mes_array[9] = "Setembro";
$mes_array[10] = "Outubro";
$mes_array[11] = "Novembro";
$mes_array[12] = "Dezembro";

$mes_ingles_array[1] = "January";
$mes_ingles_array[2] = "February";
$mes_ingles_array[3] = "March";
$mes_ingles_array[4] = "April";
$mes_ingles_array[5] = "May";
$mes_ingles_array[6] = "June";
$mes_ingles_array[7] = "July";
$mes_ingles_array[8] = "August";
$mes_ingles_array[9] = "September";
$mes_ingles_array[10] = "October";
$mes_ingles_array[11] = "November";
$mes_ingles_array[12] = "December";

$semana_array[1] = "Domingo";
$semana_array[2] = "Segunda";
$semana_array[3] = "Ter�a";
$semana_array[4] = "Quarta";
$semana_array[5] = "Quinta";
$semana_array[6] = "Sexta";
$semana_array[7] = "S�bado";

$semana_ingles_array[1] = "Sunday";
$semana_ingles_array[2] = "Monday";
$semana_ingles_array[3] = "Tuesday";
$semana_ingles_array[4] = "Wednesday";
$semana_ingles_array[5] = "Thursday";
$semana_ingles_array[6] = "Friday";
$semana_ingles_array[7] = "Saturday";

$tipo_item["d"] = "Diret�rio";
$tipo_item["f"] = "Arquivo";
$tipo_item["l"] = "Link";
$tipo_item["t"] = "Texto";
$tipo_item["v"] = "V�deo";

?>