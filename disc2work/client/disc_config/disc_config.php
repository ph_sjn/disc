<?php

// Configuracao do cliente do Disc2Work

//$CONFIGLIB_DISCX = "/home/users/__discx/";
$CONFIGLIB_DISCX = "D:/Projetos/Source/disc/__discx/";

// Funciona somente se estiver no AWS-Master
//require("/home/users/__discx/disc-lib/libDiscXLocal.php");
require($CONFIGLIB_DISCX . "disc-lib/libDiscXLocal.php");

// Se nao estiver tem que copiar a pasta dwc-lib para esta pasta
// e usar o include abaixo
// 
// include_once("disc_config/disc-lib/libDiscXLocal.php");

// Consulta os dados do cliente usando o login do usu�rio como c�digo do cliente
$DISC_objCliente = DISCX_consultarCliente("", $user_kurz);
$DISC_objClienteDecoded = json_decode($DISC_objCliente);

// Atribui o c�digo e o ID do cliente
$DEFWEB_cd_cliente = $DISC_objClienteDecoded->Codigo;
$DEFWEB_id_cliente = $DISC_objClienteDecoded->Id;

// Indica se o login tem acesso administrativo (suporte)
// Neste caso ser� necess�rio escolher qual o cliente nas p�ginas do client
$DEFWEB_master_user = checaPermissao($module,"A",$user_access);
