<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

// Biblioteca de classes e fun��es do Disc
include_once("../survey/lib/libdisc.php");

$DW_id_projeto = $_REQUEST['id_projeto'];
$DW_filtroCliente = $_REQUEST['filtroCliente'];

// Se veio o id do projeto
if ($DW_id_projeto != "") {
    //
    $sql = "SELECT ";
    $sql .= " id_cliente,";
    $sql .= " id_projeto,";
    $sql .= " nm_projeto, cd_projeto, dt_inicio, dt_fim";
    $sql .= " FROM tbdw_projeto ";
    $sql .= " WHERE id_projeto = " . $DW_id_projeto;
    $resultSet = db_query($sql) or db_die();
    $rowDados = db_fetch_array($resultSet);
    //
    if($rowDados) {
        $DW_id_cliente = $rowDados["id_cliente"];
    }
}
?>

<form method=post enctype='multipart/form-data' action='index.php' name='frm'>
    <input type=hidden name='module' value='<?= $_REQUEST['module'] ?>'>
    <input type=hidden name='mode' value='data'>


    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">CADASTRO DE PROJETO</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Cliente:</td>
                        <td style="font-weight: bold;">
                            <select name="id_cliente" class="combo">
                                <option value="">(Selecione o cliente)</option>
                                <?
                                $objDEFWEBmaster = new DEFWEBmaster();
                                //
                                $objDEFWEBmaster->obterClientes();
                                //
                                foreach ($objDEFWEBmaster->colClientes AS $objItem) {
                                    //
                                    if ($DW_id_cliente === $objItem->Id) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<OPTION VALUE='";
                                    $sHTML .= $objItem->Id;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objItem->Nome . "</OPTION>";
                                    //
                                    echo($sHTML);
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Nome do Projeto:</td>
                        <td style="font-weight: bold;"><input type="text" name="nm_projeto" value="<?php echo($rowDados['nm_projeto']);  ?>" class="texto" size="50" maxlength="150"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">C�digo:</td>
                        <td style="font-weight: bold;"><input type="text" name="cd_projeto" value="<?php echo($rowDados['cd_projeto']); ?>" class="texto" size="45" maxlength="45"></td>
                    </tr>

                </table>
                <input type=hidden name='id_projeto' value="<?php echo($rowDados['id_projeto']); ?>">
                <input type=hidden id="filtroCliente" name="filtroCliente" value="<?php echo($DW_filtroCliente); ?>" >

            </td>
        </tr>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td>
                            <input type="button" onclick="document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&filtroCliente=<?= $DW_filtroCliente?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <? if ($DW_id_projeto != "") { ?>
                                <input type="submit" name="salvar" value="Salvar"  class="botao">
                                <input type="submit" name="excluir" value="Excluir"  class="botao" onclick="return confirm('Tem certeza que deseja excluir permanentemente este registro?')">
                            <? } else { ?>
                                <input type="submit" name="incluir" value="Incluir" class="botao">
                            <? } ?>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>
