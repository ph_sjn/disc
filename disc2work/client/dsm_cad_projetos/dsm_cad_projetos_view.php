<?
$acesso_administrativo = checaPermissao($module,"A",$user_access); 
?>

<?php
$DEFWEB_cliente = $_REQUEST['filtroCliente'];
?>

<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<tr>
	<td style="vertical-align:middle;">
		<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms"><span style="font-weight:bold;">CADASTRAR PROJETO</span></a>
	</td>
</tr>
<tr>
	<td style="text-align:right">
	
		<form action='index.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
		  
				Cliente: 
				<select name='filtroCliente' id='filtroCliente' class="combo">
				<option value=''>Todos</option>
				<?
				$sql = "SELECT id_cliente, ds_cliente FROM tbdw_cliente ORDER BY ds_cliente ASC";
				$resultCliente = db_query($sql) or db_die();
				while ($rowCliente = db_fetch_array($resultCliente) ){
					if ($DEFWEB_cliente == $rowCliente['id_cliente'])
						$selected = "selected";
					else
						$selected = "";
					echo "<option value='". $rowCliente['id_cliente'] ."' $selected>". $rowCliente['ds_cliente'] ."</option> " ;
				}
				?>
				</select>
			</td>
			<td style="vertical-align:middle;">
				<input type='Submit' name="filtrarCliente" value='Filtrar' class="botao">
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>

<br>

<table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
<tr align='center' bgcolor="<?= $bgcolor4 ?>">
	<td>

		<table border=0 cellpadding=4 cellspacing=0 width='100%' bgcolor="<?= $bgcolor5 ?>" align='center'>
		<tr bgcolor="<?= $bgcolor4 ?>">
			<td style="font-weight: bold;">CLIENTE</td>
			<td style="font-weight: bold;">PROJETO</td>
			<td style="font-weight: bold;">C�DIGO</td>
			<!-- <td style="font-weight: bold;">IN�CIO</td> -->
			<!-- <td style="font-weight: bold;">T�RMINO</td> -->
		</tr>
		<?
		$sql = "SELECT";
                $sql .= " pj.id_projeto AS id_projeto,";
                $sql .= " cl.ds_cliente AS ds_cliente,";
                $sql .= " pj.nm_projeto AS nm_projeto,";
                $sql .= " pj.cd_projeto AS cd_projeto,";
                $sql .= " pj.dt_inicio AS dt_inicio, pj.dt_fim AS dt_fim";
                $sql .= " FROM tbdw_projeto AS pj";
                $sql .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = pj.id_cliente";
		if ( $DEFWEB_cliente ) {
			$sql .= " WHERE pj.id_cliente = ". $DEFWEB_cliente;	
		}
                /*
		if ( $_POST['checkPontosSemGeolocalizacao'] ) {
			$sql .= " AND ( ";
			$sql .= " TRIM(p.geo_latitude) = '' OR p.geo_latitude is null OR p.geo_latitude = 0 ";
			$sql .= " OR TRIM(p.geo_longitude) = '' OR p.geo_longitude is null OR p.geo_longitude = 0 ";	
			$sql .= " ) ";
		}
                */
		$sql .= " ORDER BY cl.ds_cliente, pj.nm_projeto";
		//echo $sql . "<br>";
		$resultSet = db_query($sql) or db_die();
		$total_avaliacoes = db_num_rows($resultSet);
		while ( $rowDados = db_fetch_array($resultSet) ) {
			$tr_hover = " onmouseover=\"this.style.cursor='pointer'; this.style.backgroundColor='$bgcolor4'\"";
			$tr_mouseout = " onmouseout=\"this.style.backgroundColor='". $bgcolor6 ."'\""; 
			$onclick = " onclick=\"document.location.href='index.php?module=". $module ."&mode=forms&filtroCliente=". $DEFWEB_cliente . "&id_projeto=". $rowDados['id_projeto']  ."'\" ";
			?>
			<tr <?= $tr_hover . $tr_mouseout . $onclick ?> bgcolor="<?= $bgcolor6 ?>">
				<td><?php echo($rowDados["ds_cliente"]); ?></td>
				<td><?php echo($rowDados["nm_projeto"]); ?></td>
				<td><?php echo($rowDados["cd_projeto"]); ?></td>
				<!-- <td><?php // echo($rowDados["dt_inicio"]); ?></td> -->
				<!-- <td><?php // echo($rowDados["dt_fim"]); ?></td> -->
			</tr><?
		}
		?>
			
		</table>
		
	</td>
</tr>
</table>

<br><br>

	<div style="text-align: center; font-weight: bold;">Total de projetos encontrados: <span style="color:#FF0000"><?= $total_avaliacoes ?></span> </div>

<br><br>

