<?
// Redirecionamento temporario
//header("Location: ../site/");
//die();
#DEBUG

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);


// Carregar cabe�alho do sistema
include_once "lib/cabecalho.php"; 

// Verifica a sess�o
if ( isset($_SESSION) && session_name() != "ExtraNet_disc2work_client" ) { 
	session_destroy();
 	header("Location:".$link_extranet ); 
}

	// Se a sess�o do usu�rio estiver ativa, permitir acesso interno do sistema
	// Sen�o, pedir para fazer login
	//if ($user_kurz) {
        if (isset($_SESSION['user_kurz'])) {   

		// Carregar o menu
		include_once "lib/menu.php";
		
		// Verificar tipo de m�dulo a ser carregado
		// @pasta: m�dulos gen�ricos
		// @module: m�dulos espec�ficos com pastas de nomes pr�prios
		if ($pasta) {
			$inc_modulo = $pasta;
		}
		else {
			if (!$module) {
				$module = "summary";
			}
			$inc_modulo = $module;
		}
		
		// In�cio da div de delimita��o do miolo
		include_once "lib/inc/topo_conteudo.php";
		
		// Bloqueia o acesso as p�ginas do tipo "sem acesso" para o usu�rio - fun��o se encontra na lib.inc.php
//		if ((checaAcessoRestrito($module, $user_access, $inc_modulo) == true) || ($user_kurz == "suporte")) { 	
		if ((checaAcessoRestrito($module, $user_access, $inc_modulo) == true) || ($user_kurz == "suporte")) { 	
		
			// Carregar m�dulo
			include_once $inc_modulo ."/" .$inc_modulo .".php";
			
		} else { ?>

			<br><br><br><br>
			<div style="text-align:center; font-size: 14px; color: blue">P�gina indispon�vel para seu usu�rio.
				<br><br>
				Favor entrar em contato com o administrador caso precise de acesso no endere�o indicado.
				<br><br>
				Suporte Extra|Net 
			</div>
			<br><br><br><br> <?
		}

		// Fim da delimita��o do miolo
		include_once "lib/inc/rodape_conteudo.php";
		
	} else {

		// Setar m�dulo de login
		$module = "login";
		
		// Carregar p�gina de login
		include_once "login/login.php";
		
	}

// Carregar rodap� do sistema
include_once "lib/rodape.php";

?>