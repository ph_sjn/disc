<?php

/*
 * libCorreios.php
 * 
 * Biblioteca para obter as informacoes de prazo e valor diretamente dos
 * Correios atraves do seu webservice publico.
 * 
 * Limites e dimensoes de embalagens:
 * http://www.correios.com.br/encomendas/prazo/Formato.cfm
 * 
 * Pacote e caixa
 * +------------------------------------------+
 * | Dimensao         |   Minimo  |   Maximo  |
 * |------------------+-----------+-----------|
 * | Comprimento(C)   |   16 cm   |   105 cm  |
 * | Largura(L)       |   11 cm   |   105 cm  |
 * | Altura(A)        |   2 cm    |   105 cm  |
 * |------------------+-----------+-----------|
 * | Soma(C+L+A)      |   29 cm   |   200 cm  |
 * +------------------------------------------+ 
 * 
 * Rolo/Prisma
 * +------------------------------------------+
 * | Dimensao         |   Minimo  |   Maximo  |
 * |------------------+-----------+-----------|
 * | Comprimento(C)   |   18 cm   |   105 cm  |
 * | Diametro(D)      |   5 cm    |   91 cm   |
 * |------------------+-----------+-----------|
 * | Soma(C+2D)       |   28 cm   |   200 cm  |
 * +------------------------------------------+ 
 * 
 * Envelope
 * +------------------------------------------+
 * | Dimensao         |   Minimo  |   Maximo  |
 * |------------------+-----------+-----------|
 * | Comprimento(C)   |   16 cm   |   60 cm   |
 * | Largura(L)       |   11 cm   |   60 cm   |
 * |------------------+-----------+-----------|
 * | Soma(C+L)        |   27 cm   |   120 cm  |
 * +------------------------------------------+ 
 * 
 * Referencia:
 *      http://www.correios.com.br/webservices/
 * 
 * Historico:
 *      2013-11-15  MPOSSANI    Versao inicial
 * 
 */

    class clsOpcaoEnvio {
        
        public
                $Codigo,
                $DescricaoServico,
                $ImagemServico,
                $Valor,
                $PrazoEntrega,
                $DescricaoPrazo,
                $Erro,
                $MensagemErro;
        
    }

    class clsCorreios {
        
        // Atributos da classe que armazenam os parametros para consulta
        public
                // Parametros
                $nCdServico,
                $sCepOrigem,        //8 digitos, sem hifem
                $sCepDestino,       //8 digitos, sem hifem
                $nVlPeso,           //Em Kg, com ponto decimal
                $nCdFormato,        //1=Caixa/Pacote, 2=Rolo/Prima, 3=Envelope
                $nVlComprimento,    //Em Cm, com ponto decimal
                $nVlAltura,         //Em Cm, com ponto decimal
                $nVlLargura,        //Em Cm, com ponto decimal
                $nVlDiametro,       //Em Cm, com ponto decimal
                // Parametros com padroes
                $sCdMaoPropria = "N",
                $nVlValorDeclarado = "0",
                $sCdAvisoRecebimento = "N",
                // Retornos
                $colOpcoesFrete,
                $URLchamada,
                $XMLretorno;
        
        //  obterDescricaoPrazo(<Dias>)
        //  
        //  Retorna o texto "dia" ou "dias" conforme a quantidade de dias
        //  informada como parametro.
        //
        public function obterDescricaoPrazo($PARM_dias) {
            
            $sDescricao = "dia";
            
            if($PARM_dias > 1) {
                $sDescricao .= "s";
            }
            
            return($sDescricao);
            
        }
        
        //  obterDescricaoServico(<Servico>)
        //
        //  Retorna a descricao do servico conforme o codigo informado,
        //  para uso na pagina que exibe as opcoes de envio disponiveis.
        //
        public function obterDescricaoServico($PARM_codigoServico) {
            
            $sDescricao = "";
            
            switch($PARM_codigoServico) {
                case "40010" :
                    $sDescricao = "SEDEX";
                    break;
                case "40215" :
                    $sDescricao = "SEDEX 10";
                    break;
                case "40290" :
                    $sDescricao = "SEDEX Hoje";
                    break;
                case "41106" :
                    $sDescricao = "PAC";
            }
            
            return($sDescricao);
        }
        
        
        
        //  calcularPrecoPrazo()
        //
        //  Envia os parametros ao webservice dos Correios que retorna as opcoes
        //  de envio com valores e prazos de acordo com os parametros da
        //  encomenda (tamanho, peso, etc).
        //
        //  O resultado e armazenado nos seguintes atributos da classe:
        //  
        //      colOpcoesFrete
        //          Array tratado com as opcoes retornadas pelo webservice
        //              Cada elemento possui os seguinte itens:
        //                  Codigo
        //                      Codigo do servico
        //                  DescricaoServico
        //                      Descricao do servico
        //                  Valor
        //                      Valor do frete
        //                  PrazoEntrega
        //                      Prazo em dias para a entrega
        //                  DescricaoPrazo
        //                      Sufixo do prazo (dia, dias)
        //                  Erro
        //                      Codigo do erro (0 se tudo OK)
        //                  MsgErro
        //                      Descricao do erro (Vazio se tudo OK)
        //      URLchamada
        //          URL utilizada para a chamada do webservice, util para
        //          depuracao em caso de problemas.
        //      XMLretorno
        //          XML com o retorno completo do webservice, util para
        //          depuracao em caso de problemas.
        //
        public function calcularPrecoPrazo() {
            
            // Inicializa o XML retorno
            $this->XMLretorno = NULL;
            
            // Inicializa o JSON retorno
            $this->colOpcoesFrete = NULL;
            
            // URL do webservice
            $webserviceURL = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/CalcPrecoPrazo?";
            // Parametros obrigatorios, fixos
            $webserviceURL .= "nCdEmpresa=&sDsSenha=";
            // Parametros obrigatorios, informados nos atributos da classe
            $webserviceURL .= "&nCdServico=%s";
            $webserviceURL .= "&sCepOrigem=%s";
            $webserviceURL .= "&sCepDestino=%s";
            $webserviceURL .= "&nVlPeso=%s";
            $webserviceURL .= "&nCdFormato=%s";
            $webserviceURL .= "&nVlComprimento=%s";
            $webserviceURL .= "&nVlAltura=%s";
            $webserviceURL .= "&nVlLargura=%s";
            $webserviceURL .= "&nVlDiametro=%s";
            $webserviceURL .= "&sCdMaoPropria=%s";
            $webserviceURL .= "&nVlValorDeclarado=%s";
            $webserviceURL .= "&sCdAvisoRecebimento=%s";
            
            // Zera os parametros nao utilizados conforme o
            // tipo da embalagem
            
            switch($this->nCdFormato) {
                // Caixa/Pacote
                case "1" : 
                    $this->nVlDiametro = "0";
                    break;
                // Rolo/Prisma
                case "2" :
                    $this->nVlLargura = "0";
                    $this->nVlAltura = "0";
                    break;
                // Envelope
                case "3" :
                    $this->nVlDiametro = "0";
                    $this->nVlAltura = "0";
                    break;
            }
            
            
            // Monta a URL da chamada com os respectivos parametros
            $this->URLchamada = sprintf(
                    $webserviceURL,
                    $this->nCdServico,
                    $this->sCepOrigem,
                    $this->sCepDestino,
                    $this->nVlPeso,
                    $this->nCdFormato,
                    $this->nVlComprimento,
                    $this->nVlAltura,
                    $this->nVlLargura,
                    $this->nVlDiametro,
                    $this->sCdMaoPropria,
                    $this->nVlValorDeclarado,
                    $this->sCdAvisoRecebimento
            );

            // Inicializa o cURL
            $cURL = curl_init($this->URLchamada);
            
            // Ajusta os paramentos do cURL
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            
            // Faz a chamada e salva o resultado
            $sRetorno = curl_exec($cURL);
            
            // Fecha o cURL
            curl_close($cURL);
            
            // Extrai o XML retornado
            $this->XMLretorno = simplexml_load_string($sRetorno);
            
            // Armazena as opcoes de envio que serao extraidas do XML
            $colOpcoes = Array();
            // Ponteiro no array das opcoes de envio
            $iPos = 0;
            
            // Nome base do arquivo de imagem que representa o servico
            $sPastaBase = $_SERVER['PHP_SELF'];
            $sArquivoImgBase = substr($sPastaBase, 0, strripos($sPastaBase, "/")) . "/img/imgCorreios";
            
            // Para cada servico retornado no XML pelo webservice...
            foreach ($this->XMLretorno->Servicos->cServico as $objServico) {
                
                $objOpcaoEnvio = new clsOpcaoEnvio();
                
                // Extrai o codigo do servico
                $objOpcaoEnvio->Codigo = sprintf("%s", $objServico->Codigo);
                // Completa com a descricao do servico
                $objOpcaoEnvio->DescricaoServico = $this->obterDescricaoServico($objServico->Codigo);
                // URL para o icone do servico
                $objOpcaoEnvio->ImagemServico =  $sArquivoImgBase . $objServico->Codigo . ".gif";
                // Extrai o valor (vem formatado com virgula decimal)
                $tempValorVirgulaDecimal = sprintf("%s", $objServico->Valor);
                // Troca a virgula por ponto (para facilitar no calculo pela pagina)
                $tempValorPontoDecimal = substr_replace($tempValorVirgulaDecimal, ".", strpos($tempValorVirgulaDecimal, ","), 1);
                // Atribui o valor
                $objOpcaoEnvio->Valor = $tempValorPontoDecimal;
                // Extrai o prazo de entrega estimado (em dias)
                $objOpcaoEnvio->PrazoEntrega = sprintf("%s", $objServico->PrazoEntrega);
                // Completa com o sufixo do prazo (dia, dias)
                $objOpcaoEnvio->DescricaoPrazo = $this->obterDescricaoPrazo($objServico->PrazoEntrega);
                // Extrai o codigo do erro retornado pelo webservice
                $objOpcaoEnvio->Erro = sprintf("%s", $objServico->Erro);
                // Extrai a descricao do erro retornado pelo webservice
                $objOpcaoEnvio->MensagemErro = sprintf("%s", $objServico->MsgErro);
                
                $colOpcoes[$iPos] = $objOpcaoEnvio;
                
                // Incrementa o ponteiro
                $iPos++;
            } 
            
            // Se retornou algum servico
            if($iPos > 0) {
                // Atribui o array
                $this->colOpcoesFrete = $colOpcoes;
            }
            
        }
        
    }


