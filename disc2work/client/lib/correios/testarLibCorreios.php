<?php

//ini_set('default_charset', 'UTF-8');

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);

include_once("libCorreios.php");

$PARM_CEPorigem = $_REQUEST["txtCEPorigem"];
$PARM_CEPdestino = $_REQUEST["txtCEPdestino"];

$PARM_Peso = $_REQUEST["txtPeso"];
$PARM_Formato = $_REQUEST["cboFormato"];
$PARM_Comprimento = $_REQUEST["txtComprimento"];
$PARM_Altura = $_REQUEST["txtAltura"];
$PARM_Largura = $_REQUEST["txtLargura"];
$PARM_Diametro = $_REQUEST["txtDiametro"];

if($PARM_Formato == "") {
    $PARM_Formato = "3";
}

?>

<html>
    <head>
        <script language="javascript">
    
            function exibirCampos() {
                var objFormato = document.getElementById('cboFormato');
                var objDIVlargura = document.getElementById('divLargura');
                var objDIVdiametro = document.getElementById('divDiametro');
                var objDIValtura = document.getElementById('divAltura');

                var vlFormato = objFormato.value;
                
                objDIVlargura.style.visibility = (((vlFormato == 1)||(vlFormato == 3)) ? "visible" : "hidden");
                objDIVlargura.style.display = (((vlFormato == 1)||(vlFormato == 3)) ? "block" : "none");
                objDIVdiametro.style.visibility = ((vlFormato == 2) ? "visible" : "hidden");
                objDIVdiametro.style.display = ((vlFormato == 2) ? "block" : "none");
                objDIValtura.style.visibility = ((vlFormato == 1) ? "visible" : "hidden");
                objDIValtura.style.display = ((vlFormato == 1) ? "block" : "none");
            }

        </script>

    </head>
    <body onload="javascript:exibirCampos();">
        <p>Pagina para teste da biblioteca de calculo de prazo e frete dos Correios:</p>
        <p>
            Parametros para consulta:
        </p>
        <form id="formCEP" name="formCEP" action="<?php echo($_SERVER["PHP_SELF"]); ?>" method="POST">
            <p>
                Dados para entrega
            </p>
            <div style="display: block;">
                CEP de origem: <input type="text" id="txtCEPorigem" name="txtCEPorigem" value="<?php echo($PARM_CEPorigem); ?>" size="10" maxlength="8"><br />
                <br />
                CEP de destino: <input type="text" id="txtCEPdestino" name="txtCEPdestino" value="<?php echo($PARM_CEPdestino); ?>" size="10" maxlength="8"><br />
            </div>
            <p>
                Informacoes sobre o pacote
            </p>
            <div style="display: block;">
                Peso: <input type="text" id="txtPeso" name="txtPeso" value="<?php echo($PARM_Peso); ?>" size="10" maxlength="8"> kg<br />
                <br />
            </div>
            <div style="display: block;">
                Formato: <select id="cboFormato" name="cboFormato" onchange="javascript:exibirCampos();">
                            <option value="1" <?php if($PARM_Formato == "1"){ echo(" SELECTED");} ?>>
                                Caixa/Pacote
                            </option>
                            <option value="2" <?php if($PARM_Formato == "2"){ echo(" SELECTED");} ?>>
                                Rolo/Prisma
                            </option>
                            <option value="3" <?php if($PARM_Formato == "3"){ echo(" SELECTED");} ?>>
                                Envelope
                            </option>
                        </select>               
                <br />
                <br />
            </div>
            <div id="divComprimento" style="visibility: visible; display: block;">
                Comprimento: <input type="text" id="txtComprimento" name="txtComprimento" value="<?php echo($PARM_Comprimento); ?>" size="10" maxlength="8"> cm<br/>
                <br />
            </div>
            <div id="divAltura" style="visibility: hidden; display: none;">
                Altura: <input type="text" id="txtAltura" name="txtAltura" value="<?php echo($PARM_Altura); ?>" size="10" maxlength="8"> cm<br />
                <br />
            </div>
            <div id="divLargura" style="visibility: hidden; display: none;">
                Largura: <input type="text" id="txtLargura" name="txtLargura" value="<?php echo($PARM_Largura); ?>" size="10" maxlength="8"> cm<br />
                <br />
            </div>
            <div id="divDiametro" style="visibility: hidden; display: none;">
                Diametro: <input type="text" id="txtDiametro" name="txtDiametro" value="<?php echo($PARM_Diametro); ?>" size="10" maxlength="8"> cm<br />
                <br />
            </div>
        </form>
        <p>
            <input type="submit" name="btnCalcular" id="btnCalcular" value="Calcular prazo e frete" form="formCEP">
        </p>

<?php

//echo("<br>".date("Y-m-d H:i:s")."<br>");

//echo("<br>PARM_CEPorigem=".$PARM_CEPorigem."<br>");
//echo("<br>PARM_CEPdestino=".$PARM_CEPdestino."<br>");

if($PARM_CEPorigem != "" && $PARM_CEPdestino != "") {
    
    $objCorreios = new clsCorreios();

    //$objCorreios->nCdServico = "40010";
    $objCorreios->nCdServico = "40010,40215,40290,41106";
    $objCorreios->sCepOrigem = $PARM_CEPorigem; //"11025200";
    $objCorreios->sCepDestino = $PARM_CEPdestino; //"04089001";
    $objCorreios->nVlPeso = $PARM_Peso; //"0.300";
    $objCorreios->nCdFormato = $PARM_Formato; //"1";
    $objCorreios->nVlComprimento = $PARM_Comprimento; //"30";
    $objCorreios->nVlAltura = $PARM_Altura; //"10";
    $objCorreios->nVlLargura = $PARM_Largura; //"20";
    $objCorreios->nVlDiametro = $PARM_Diametro; //"0";

    $objCorreios->calcularPrecoPrazo();
    
    if($objCorreios->colOpcoesFrete) {
 
        ?>
        <table>
        
        <?php
        $sPrefixoMoeda = "R";
        
        foreach ($objCorreios->colOpcoesFrete as $objOpcao) {

            ?>
            <tr>
                <td colspan="2">
                    <img src="<?php echo(sprintf("%s", $objOpcao->ImagemServico)); ?>" alt="">
                </td>
            </tr>
            <tr>
                <td>Servico:</td>
                <td><?php echo(sprintf("%s", $objOpcao->DescricaoServico)); ?></td>
            </tr>
            <tr>
                <td>Valor:</td>
                <td><?php echo(number_format(sprintf("%s", $objOpcao->Valor), 2, ",", ".")); ?></td>
            </tr>
            <tr>
                <td>Prazo estimado de entrega:</td>
                <td><?php echo(sprintf("%s %s", $objOpcao->PrazoEntrega, $objOpcao->DescricaoPrazo)); ?></td>
            </tr>
            <?php
            
        }
        
        ?>
        </table>
        <?php
        
    } else {
        echo("<br>NAO FOI POSSIVEL CALCULAR O VALOR DO FRETE E O PRAZO ESTIMADO PARA ENVIO!<br>");
    }
}

?>
    </body>
</html>
