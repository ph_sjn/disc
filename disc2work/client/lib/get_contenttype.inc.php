<?php

// Envia o cabe�alho geral
if (eregi("MSIE",$HTTP_USER_AGENT)) { 
	$msie5 = 1;
}

if (strpos($HTTP_USER_AGENT,"MSIE") > 0) {
	$msie5 = 1;
}

header ("Expires: Mon, 10 Dec 2001 08:00:00 GMT");
header ("Last-Modified: " . gmdate ("D, d M Y H:i:s") . " GMT");
if ($HTTP_SERVER_VARS['HTTPS'] != 'on') {
  if($msie5){
     // O IE n�o pode fazer download das sess�es sem um cache
    header ('Cache-Control: public');
  } else {
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Pragma: no-cache");
  }
} else {
   // Para conec��es SSL voc� deve recolocar as duas linhas anteriores com
   header ("Cache-Control: must-revalidate, post-check=0,pre-check=0");
   header ("Pragma: public");
}

// Volta se nenhum tipo de download for indicado
if (!$file_download_type) { $file_download_type = "attachment"; }
// Modo de download alternativo
if ($alt_down) { $file_download_type = $alt_down; }

$contenttype = content_type($name);
if ($file_download_type == "inline") {
   // Checa o �ndice e relaciona o MIME e o tipo do arquivo
  header ("Content-type: $contenttype");
  header ("Content-disposition: inline; filename=\"$name\"");
}
else {
	if($msie5){
		header("Content-type: application/force-download");
		header ("Content-disposition: attachment; filename=\"$name\";");
	}
	else{ 
		header ("Content-type: $contenttype");
		header ("Content-disposition: attachment; filename=\"$name\";");
	}
}

function content_type($name) {
   // Define o tipo de �ndice baseado na extens�o do arquivo
   $contenttype = "application/octet-stream";
   $contenttypes = array("html" => "text/html",
                      "htm" => "text/html",
                      "txt" => "text/plain",
                      "gif" => "image/gif",
                      "jpg" => "image/jpeg",
                      "png" => "image/png",
                      "sxw" => "application/vnd.sun.xml.writer",
                      "sxg" => "application/vnd.sun.xml.writer.global",
                      "sxd" => "application/vnd.sun.xml.draw",
                      "sxc" => "application/vnd.sun.xml.calc",
                      "sxi" => "application/vnd.sun.xml.impress",
                      "xls" => "application/vnd.ms-excel",
					  "xlsx" => "application/vnd.ms-excel",
                      "ppt" => "application/vnd.ms-powerpoint",
					  "pptx" => "application/vnd.ms-powerpoint",
                      "doc" => "application/msword",
					  "docx" => "application/msword",
                      "rtf" => "text/rtf",
                      "zip" => "application/zip",
                      "mp3" => "audio/mpeg",
                      "pdf" => "application/pdf",
                      "tgz" => "application/x-gzip",
                      "gz"  => "application/x-gzip",
                      "vcf" => "text/vcf");

   $name = str_replace("�"," ",$name);
   foreach ($contenttypes as $type_ext => $type_name) {
     if (preg_match ("/$type_ext$/i", $name)) { $contenttype = $type_name; }
   }
   return $contenttype;
}

?>
