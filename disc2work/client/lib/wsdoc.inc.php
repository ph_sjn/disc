<?php
/*
 * Debug

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);
 */
 
 
 /*
  * Debug echo
	Habilitar/desabilitar echos de debug
 */
$debug_habilitado = true;



/*
wsDoc.inc.php
Include com funcoes para acesso ao WebService de manipulacao de arquivos
*/

/*
masterIP
   Parameter
 */
function masterIP($external_url) {
    global $wsdoc_url;


    $ws_url = $external_url."/ws/ws_masterIP.php";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == Null) {
        return "0.0.0.0";
    }

    return $ret;
        
}

/*
masterName
   Parameter
 */
function masterName($external_url) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $external_url."/ws/ws_masterName.php";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == Null) {
        return "";
    }
    return $ret;
        
}



/*
docExist
   Parameter
   filename: nome do arquivo incluindo o path completo no servidor remoto
*/
function docExist($filename) {
    global $wsdoc_url, $debug_habilitado;
    
    $ws_url = $wsdoc_url ."/ws_docExist.php";

    $params = array(
        'doc'=>"$filename"
    );  

	if ($debug_habilitado) { 
		echo "<br>DEBUG docExist: curl...";
		print_r($params);
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch)) {
		error_log("Erro do CURL docExist(): ". curl_errno($ch) .". URL: ". $ws_url . ". filename: ". $params['doc']);
		if ($debug_habilitado) { 
			echo "<br>DEBUG docExist: error:".curl_errno($ch);
		}
        $ret = False;
    }else {
		//error_log("CURL docExist() executado com sucesso.");
	}
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docExist: response:".$ret;
		echo "<br>DEBUG docExist: end<br>";
	}
    return $ret;

    
}

/*
docDelete
   Parameter
   filename: nome do arquivo incluindo o path completo no servidor remoto
   is_dir: especifica true se o arquivo passado é um diretório 
*/
function docDelete($filename,$is_dir=false) {
    global $wsdoc_url, $debug_habilitado;
    
    $ws_url = $wsdoc_url."/ws_docDelete.php";

    $params = array(
        'doc'=>"$filename",
		'is_dir'=>"$is_dir"
    );  

	if ($debug_habilitado) { 
		echo "<br>DEBUG docDelete: curl...";
		print_r($params);
	}


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch)) {
		if ($debug_habilitado) { 
			echo "<br>DEBUG docDelete: error:".curl_errno($ch);
		}
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docDelete: response:".$ret;
		echo "<br>DEBUG docDelete: end<br>";
	}
    return $ret;
    
}

/*
docRename
   Parameter
	nome_original: nome original do diretório/arquivo
	nome_novo: nome novo do diretório/arquivo
*/
function docRename($nome_original,$nome_novo) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docRename.php";

    $params = array(
        'nome_original'=>"$nome_original",
		'nome_novo'=>"$nome_novo"
    ); 

    if ($debug_habilitado) { 
		echo "<br>DEBUG docRename: curl...";
		print_r($params);
	}


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch)) {
		if ($debug_habilitado) { 
			echo "<br>DEBUG docRename: error:".curl_errno($ch);
		}
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docRename: response:".$ret;
		echo "<br>DEBUG docRename: end<br>";
	}
    return $ret;
}

/*
docPutDir
   Parameter
   dir: nome do novo diretório a criar 
   perm: nível de permissão do novo diretório - 0000 a 0777
*/
function docPutDir($dir, $perm) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docPutDir.php";

    $params = array(
        'diretorio'=>"$dir",
        'permissao'=>"$perm"
    );  

	if ($debug_habilitado) { 
		echo "<br>DEBUG docPutDir: curl...";
		print_r($params);
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == False) {
		error_log("Erro do CURL docPutDir() FALHA: ". curl_errno($ch) .". URL: ". $ws_url . ". diretorio: ". $params['diretorio']);
		if ($debug_habilitado) { 
			echo "<br>DEBUG docPutDir: error:".curl_errno($ch);
		}
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docPutDir: response:".$ret;
		echo "<br>DEBUG docPutDir: end<br>";
	}
    return $ret;
        
}


/*
docPut
   Parameter
   localfile: nome do arquivo local incluindo o path completo mormalmente no diretorio tmp 
   remotefile: nome do arquivo remoto incluindo o path completo
*/
//BACKUP DA FUNCAO 09082018
function docPut($localfile, $remotefile) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docPut.php";

    $params = array(
        'localdoc'=>"@$localfile",
        'remotedoc'=>"$remotefile"
    );  

	if ($debug_habilitado) { 
		echo "<br>DEBUG docPut: curl...";
		print_r($params);
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, False);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
	//error_log("Debug: objRet1st-> " . curl_errno($ch) ." objRet1st-> ". $ret. " ws_url->" . $ws_url . " ");
	//$ret = False;
    if (curl_errno($ch) || $ret == False) {
		
		error_log("Erro do CURL docPut() FALHA: ". curl_errno($ch) .". URL: ". $ws_url . ". localdoc: ". $params['localdoc'] . ". remotedoc: ". $params['remotedoc']);

		if ($debug_habilitado) { 
			echo "<br>DEBUG docPut: error:".curl_errno($ch);
		}
        $ret = False;
		
		error_log("Nova tentativa do docPut e aguardando 3 segundos... ");
		
		curl_close($ch);
		
		sleep(3);
		
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ws_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, False);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		
		$ret = curl_exec($ch);
		//error_log("Debug: objRet1st-> " . curl_errno($ch) ." objRet1st-> ". $ret. " ws_url->" . $ws_url . " ");
		if (curl_errno($ch) || $ret == False) {
			error_log("Erro do CURL docPut() FALHA DA SEGUNDA TENTATIVA: ". curl_errno($ch) .". URL: ". $ws_url . ". localdoc: ". $params['localdoc'] . ". remotedoc: ". $params['remotedoc']);
		}

    }
	//error_log("Erro do CURL docPut() SUCESSO: ". curl_errno($ch) .". URL: ". $ws_url . ". localdoc: ". $params['localdoc'] . ". remotedoc: ". $params['remotedoc']);		
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docPut: response:".$ret;
		echo "<br>DEBUG docPut: end<br>";
	}
    return $ret;
        
}

/*
class docPutObj_parm {

    public $ws_url = null;
    public $params = null;

}

class docPutObj_ret {

    public $ch = null;
    public $ret = null;

}

function internal_docPut($objParms) {
    //
    $objRet = new docPutObj_ret();
    //
    $objRet->ch = curl_init();
    //
    curl_setopt($objRet->ch, CURLOPT_URL, $objParms->ws_url);
    curl_setopt($objRet->ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($objRet->ch, CURLOPT_POSTFIELDS, $objParms->params);
    //
    $objRet->ret = curl_exec($objRet->ch);
    //
    return($objRet);
}

function docPut($localfile, $remotefile) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url . "/ws_docPut.php";

    $params = array(
        'localdoc' => "@$localfile",
        'remotedoc' => "$remotefile"
    );

    if ($debug_habilitado) {
        echo "<br>DEBUG docPut: curl...";
        print_r($params);
    }
    //
    try {
        //
        $objParms1st = new docPutObj_parm();
        //
        $objParms1st->params = $params;
        $objParms1st->ws_url = $ws_url;
        //
        $objRet1st = internal_docPut($objParms1st);
		$ret = $objRet1st;
        //
        // Se retornou algum erro do cURL ou false...

     error_log("Debug: objRet1st-> " . curl_errno($objRet1st->ch) ." objRet1st-> ". $objRet1st->ret . " ws_url->" . $ws_url . " ");
		
        if (curl_errno($objRet1st->ch) || $objRet1st->ret == False) {
        //if (curl_errno($objRet1st->ch)) {
			error_log(
					"Erro do CURL docPut() FALHA NA PRIMEIRA TENTATIVA:" 
				.   curl_errno($objRet1st->ch) 
				.   ". URL: " . $ws_url 
				. ". localdoc: " . $params['localdoc'] 
				. ". remotedoc: " . $params['remotedoc']
				);
			
            if ($debug_habilitado) {
                echo "<br>DEBUG docPut: error:" . curl_errno($objRet1st->ch);
            }
            curl_close($objRet1st->ch);
			
            //
            throw new Exception("Falhou na primeira - docPut().");
        }
		error_log(
					"Erro do CURL docPut() SUCESSO NA PRIMEIRA TENTATIVA:" 
				.   curl_errno($objRet2st->ch) 
				.   ". URL: " . $ws_url 
				. ". localdoc: " . $params['localdoc'] 
				. ". remotedoc: " . $params['remotedoc']
				);
        curl_close($objRet1st->ch);
        if ($debug_habilitado) {
            echo "<br>DEBUG docPut: response:" . $objRet1st->ret;
            echo "<br>DEBUG docPut: end<br>";
        }
        
    } catch (Exception $e) {
        
        try {
			
            //
            $objParms2st = new docPutObj_parm();
            //
            $objParms2st->params = $params;
            $objParms2st->ws_url = $ws_url;
            //
            $objRet2st = internal_docPut($objParms2st);
			$ret = $objRet2st;
            //
            // Se retornou algum erro do cURL ou false...
            if (curl_errno($objRet2st->ch) || $objRet2st->ret == False) {
                if ($debug_habilitado) {
                    echo "<br>DEBUG docPut: error:" . curl_errno($objRet2st->ch);
                }
				curl_close($objRet2st->ch);
                //
                throw new Exception("Falhou na segunda - docPut().");
            }
			error_log(
						"Erro do CURL docPut() SUCESSO NA SEGUNDA TENTATIVA: " 
					.   curl_errno($objRet2st->ch) 
					.   ". URL: " . $ws_url 
					. ". localdoc: " . $params['localdoc'] 
					. ". remotedoc: " . $params['remotedoc']
					);
            curl_close($objRet2st->ch);
            if ($debug_habilitado) {
                echo "<br>DEBUG docPut: response:" . $objRet2st->ret;
                echo "<br>DEBUG docPut: end<br>";
            }

        } catch (Exception $e) {

            try {
                error_log(
                            "Erro do CURL docPut()  " . $ws_url . " TERCEIRA TENTATIVA UTILIZANDO O copy():" 
                        . ". localfile: " . $localfile
                        . ". remotefile: " . $remotefile
                        );
               
			   copy($localfile, $remotefile);
                
            } catch (Exception $e) {
                //
                // Falhou em todas as tentativas, retorna False
                $ret = False;
                // Registra a falha na ultima tentativa
                error_log(
                            "Erro do CURL docPut() ULTIMA TENTATIVA: copy()" 
                        . ". localfile: " . $localfile
                        . ". remotefile: " . $remotefile
                        );

            }
            
        }
    }
    // Retorno da funcao
    return $ret;
}
*/

/*
docGetThumb
   Parameter
   doc: nome do arquivo remoto incluindo o path completo
   height: altura do thumb
   width: largura do thumb
 */
//function docGetThumb($doc, $height, $width) {
//    global $wsdoc_url;
//
//    $ws_url = $wsdoc_url."/ws_docGetThumb.php";
//   
//    $key = "RP extra|Net";
//    $docenc = encode($doc, $key); 
//    $src_url = $wsdoc_url."/ws_docGetThumb.php?doc=".$docenc."&h=".$height."&w=".$width;
//    
//    return $src_url;
//        
//}

/*
docGetThumb
   Parameter
   doc: nome do arquivo remoto incluindo o path completo
   height: altura do thumb
   width: largura do thumb
   f: formato de saída do arquivo (jpg, png, fig, etc)
   q: qualidade da imagem
 */
function docGetThumb($doc, $height, $width, $f="", $q="") {
    global $wsdoc_url, $debug_habilitado;

    $src_url = $wsdoc_url."/../phpThumb/phpThumb.php?src=".$doc."&h=".$height."&w=".$width;
    if ($f) {
        $src_url = $src_url . "&f=". $f;
    }
    if ($q) {
        $src_url = $src_url . "&q=". $q;
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $src_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_HEADER, False);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == False) {
        error_log("Erro do CURL docGetThumb(): ". curl_errno($ch) .". URL: ". $ws_url . ". localdoc: ". $params['localdoc'] . ". remotedoc: ". $params['remotedoc']);		
		
		return "Erro: Imagem nao encontrada";
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docPut: response:".$ret;
		echo "<br>DEBUG docPut: end<br>";
	}
    $ret = base64_encode($ret);
    return "data:image/jpeg;base64,".$ret;
        
}

/*
docGet
   Parameter
   doc: nome do arquivo remoto incluindo o path completo
 */
function docGet($doc, $type) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docGet.php";
    
    $params = array(
        'doc'=>"$doc"
    );  

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, True);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == False) {
        return "Erro: Imagem nao encontrada";
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docPut: response:".$ret;
		echo "<br>DEBUG docPut: end<br>";
	}
    $sMIMEType = "application/octet-stream";
    switch ($type) {
        case ($type == "jpeg" || $type == "jpg"): 
            $sMIMEType = "image/jpeg";
            break;
        case "gif": 
            $sMIMEType = "image/gif";
            break;
        case "png": 
            $sMIMEType = "image/png";
            break;
        case "pdf": 
            $sMIMEType = "application/pdf";
            break;
        default:
            $sMIMEType = "application/octet-stream";
            break;
    }

    if ($type != "nh") { //nh = No Header
        //header( "Content-type: " . $sMIMEType );
        header( "Content-type: " . $sMIMEType ."; charset=ISO-8859-1");
    }
    return $ret;
        
}


/*
docGetSize
   Parameter
   remotefile: nome do arquivo remoto incluindo o path completo
*/
function docGetSize($remotefile) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docGetSize.php";

    $params = array(
        'remotedoc'=>"$remotefile"
    );  
	
	if ($debug_habilitado) { 
		echo "<br>DEBUG docGetSize: curl...";
		print_r($params);
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == False) {
		if ($debug_habilitado) { 
			echo "<br>DEBUG docGetSize: error:".curl_errno($ch);
		}
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docGetSize: response:".$ret;
		echo "<br>DEBUG docGetSize: end<br>";
	}
    return $ret;
        
}


/*
docGetImageDimensions
   Parameter
   remotefile: nome do arquivo remoto incluindo o path completo
*/
function docGetImageDimensions($remotefile) {
    global $wsdoc_url, $debug_habilitado;

    $ws_url = $wsdoc_url."/ws_docGetImageDimensions.php";

    $params = array(
        'remotedoc'=>"$remotefile"
    );  

    if ($debug_habilitado) { 
		echo "<br>DEBUG docGetImageDimensions: curl...";
		print_r($params);
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ws_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $ret = curl_exec($ch);
    if (curl_errno($ch) || $ret == False) {
		if ($debug_habilitado) { 
			echo "<br>DEBUG docGetImageDimensions: error:".curl_errno($ch);
		}
        $ret = False;
    }
    curl_close($ch);
	if ($debug_habilitado) { 
		echo "<br>DEBUG docGetImageDimensions: response:".$ret;
		echo "<br>DEBUG docGetImageDimensions: end<br>";
	}
    return $ret;
        
}


/* 
 * Funcoes Auxiliares de Codificacao e Decodificacao
 */
function encode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0; $hash = "";
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}

function decode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0; $hash = "";
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

?>