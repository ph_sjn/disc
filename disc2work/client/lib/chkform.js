
var prev_fld = '';

function chkChrs(frm,fld,txt,searchfor,how){
 string = document.forms[frm].elements[fld].value;
 if(how){
   proof = searchfor.exec(string);
   stop = (proof != string);
 }
 else{
   stop = searchfor.test(string);
 }
 if(stop && (fld == prev_fld || prev_fld == '')){
     alert(txt);
     prev_fld = fld;
     document.forms[frm].elements[fld].focus();
     return
 }
 else prev_fld = '';
}

function chkISODate(frm,fld,txt){
 string = document.forms[frm].elements[fld].value;
 if(string != ""){
   searchfor = /^\d\d\/\d\d\/\d\d\d\d$/;
   result = searchfor.test(string);
   if(result == false){
     alert(txt);
     document.forms[frm].elements[fld].focus();
     return
   }
   if(chkISODate.arguments.length == 4){
     x = "";
     i = -1;
     while(x != fld) {
          i++;
          x = document.forms[frm].elements[i].name;
     }
     i--;
     if(document.forms[frm].elements[i].value > document.forms[frm].elements[fld].value){
       txt = chkISODate.arguments[3];
       alert(txt);
       document.forms[frm].elements[i].focus();
       return
     }
   }
 }
}

function validateForm(frm,arg1,arg2,arg3,arg4) {

	if ( frm.nachname.value == '' ) {
	  	alert(arg1 + '.');
    	return false;
	}

	if ( frm.kurz.value == '' ) {
	  	alert(arg2 + '.');
    	return false;
	}

	if ( frm.pw.value == '' ) {
	  	alert(arg3 + '.');
    	return false;
	}

	if ( frm.email.value == '' ) {
	  	alert(arg4 + '.');
    	return false;
	}
	

	return true;
    
}

function chkForm(frm) {
 for (var i=1; i<chkForm.arguments.length; i++){
   fld=chkForm.arguments[i];
   i++;
   txt=chkForm.arguments[i];
   if(document.forms[frm].elements[fld].value == ""){
     alert(txt);
     document.forms[frm].elements[fld].focus();
     return false;
   }
 }
}

function mascara_data(fld){ 
	//alert(document.getElementById(fld).value);
	infodata(fld);  
} 

function infodata (fld) { 
	var infodata = ''; 
    infodata = infodata + document.getElementById(fld).value; 
	
	if (infodata.length == 2){ 
        infodata = infodata + '/'; 
       document.getElementById(fld).value = infodata; 
    } 
    if (infodata.length == 5){ 
        infodata = infodata + '/'; 
        document.getElementById(fld).value = infodata; 
    } 
    if (infodata.length == 10){ 
        verifica_data(fld); 
    }
	
}

function verifica_data (fld) { 
  var anoPresente = new Date();
  anoPresente = anoPresente.getYear();
  //if(anoPresente < 1900) {anoPresente += 1900;} // corrections if Y2K display problem
  
  dia = (document.getElementById(fld).value.substring(0,2)); 
  mes = (document.getElementById(fld).value.substring(3,5)); 
  ano = (document.getElementById(fld).value.substring(6,10)); 

  situacao = ""; 
  // verifica o dia valido para cada mes 
  if ((dia < 01)||(dia < 01 || dia > 30) && (  mes == 04 || mes == 06 || mes == 09 || mes == 11 ) || dia > 31) { 
      situacao = "falsa"; 
	//  alert("o dia invalido");
  } 

  // verifica se o mes e valido 
  if (mes < 01 || mes > 12 ) { 
      situacao = "falsa"; 
	 // alert("mes e valido");
  } 

  // verifica se e ano bissexto 
  if (mes == 2 && ( dia < 01 || dia > 29 || ( dia > 28 && (parseInt(ano / 4) != ano / 4)))) { 
      situacao = "falsa"; 
	//  alert("ano bissexto");
  } 
  
  
  if (document.getElementById(fld).value == "") { 
      situacao = "falsa"; 
  } 

  if (situacao == "falsa") { 
     alert("Data inv�lida!"); 
	 document.getElementById(fld).value = '';
     //tipo_data.valuefocus(); 
  } 
} 


function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)
  
  if (texto.substring(0,1) != saida){
	documento.value += texto.substring(0,1);
  }
  
}