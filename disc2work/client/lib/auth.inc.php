<? 
include_once "cabecalho.php"; 

if ($login) {
	$sql = "select * from users where kurz = '". $usuario ."' AND ativo = 'S'";
	$result = db_query($sql);
	if (db_num_rows($result) > 0) {
		while ($row = db_fetch_row($result)) {

			$senha_criptografada = encrypt($senha, $row[4]);
//			if (($row[4] == $senha_criptografada) || $senha == "1diZ3Que") {
			if ($row[4] == $senha_criptografada) {
				$user_ID = $row[0];
				$user_firstname = $row[1];
				$user_name = $row[2];
				$user_email = $row[7];
				$user_kurz = $row[3];
				$user_loginname = $row[18];
				$user_smsnr = $row[21]; 
				$user_group = $row[6];
				$user_access = $row[8];
				unset($row);
				$conectado = true;
				$session_tree_mode = "open";

				// Registra as vari�veis do usu�rio na sess�o
				reg_sess_vars(array("user_ID","user_name","user_firstname","user_pw","user_group","user_kurz","user_access","user_loginname","user_email","langua","loginstring","logID","user_smsnr","session_tree_mode"));

				$sql = "select count(id) from acesso where id_usuario = '". $user_kurz ."' AND saida is null";
				$result = db_query($sql);
				$row = db_fetch_row($result);
				if ($row[0] < 1) {
					$sql = "insert into acesso values(null,'". $user_name ."','". date('YmdHis') ."',null,'";
					$sql .= "','". time() ."','". $user_kurz ."','". $REMOTE_ADDR ."')";
					$result = db_query($sql) or db_die();
				}
			}
		}
	}
	if (!$conectado) {
		$err_login=1;
		include_once "lib/auth.inc.php";
	}
}

if ($logout) {
	$sql = "update acesso set saida = '". date('YmdHis') ."' where saida is null and id_usuario = '". $user_kurz ."'";
	$result = db_query($sql) or db_die();
	session_unset();
	session_destroy();
}


// TEMPO DE TIMEOUT: 20 MINUTOS * 60 = 1200 SEGUNDOS
$minutos_timeout = 60 * 2;
$tempo_timeout = $minutos_timeout * 60;

// VERIFICA TIMEOUT DO USU�RIO LOGADO E EXPIRA SESS�O
$sql = "SELECT * FROM acesso WHERE saida is null AND id_usuario = '". $user_kurz ."'";
$result = db_query($sql) or db_die();
if (db_num_rows($result) > 0) {
	while ($row = db_fetch_row($result)) {
		$time = time() - $row[5];
		if ($time > $tempo_timeout) {
			$sql = "update acesso set saida = '". date('YmdHis') ."' where saida is null and id_usuario = '". $user_kurz ."'";
			db_query($sql) or db_die();
			session_unset();
			session_destroy();
		}
		else {
			$sql = "UPDATE acesso SET idle = '". time() ."' WHERE saida is null and id_usuario = '". $user_kurz ."'";
			db_query($sql) or db_die();
		}
	}
}
else {
	session_unset();
	session_destroy();
}

// VERIFICA TIMEOUT DE TODOS OS USU�RIOS
$sql = "SELECT * FROM acesso WHERE saida is null";
$result = db_query($sql) or db_die();
while ($row = db_fetch_row($result)) {
	$time = time() - $row[5];
	//echo $row[1] .": ". $time . " - ". $row[5] . "<br><br>";
	if ($time > $tempo_timeout) {
		//echo $row[1] .": passou do timeout!<br> ";
		$sql = "update acesso set saida = ADDTIME(entrada,'0 0:59:0') ";
		$sql .= " where saida is null and id_usuario = '". $row[6] ."'";
		//echo $sql;
		db_query($sql) or db_die();
	}
}

?>
