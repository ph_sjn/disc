<?php

// gpcs_vars.inc.php - PHProjekt Vers�o 4.0
// copyright � 2000-2003 Albrecht Guenther  ag@phprojekt.com
// www.phprojekt.com
// Autor: Albrecht Guenther
// Adaptado por: RPconsultoria - info@RPconsultoria.com.br

function arr_addsl(&$item,$key){$item = addslashes($item);}

foreach($_FILES as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}

foreach($_SESSION as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}

foreach($_COOKIE as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}


foreach($_REQUEST as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}

/*
foreach($_POST as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}

foreach($_GET as $key => $value) {
	if(!is_array($value)){
	 ${$key} = trim(rawurldecode($value));
	}
	else{
		${$key} = $value;
	}
}
*/
$userfile = "";

if(isset($_FILES['userfile'])){
  $userfile_name = $_FILES['userfile']['name'];
  $userfile_type = $_FILES['userfile']['type'];
  $userfile_size = $_FILES['userfile']['size'];
  $userfile = $_FILES['userfile']['tmp_name'];
}

$HTTP_USER_AGENT = getenv("HTTP_USER_AGENT");
$PHP_SELF = $_SERVER["PHP_SELF"];

function quote_runtime($x){
   if(!get_magic_quotes_runtime()) {
      if(is_array($x)) array_walk($x,'arr_addsl');
      else $x = addslashes($x);
   }
   return $x;
}

function reg_sess_vars($sess_vars){
   global $_SESSION;
   if(ini_get('register_globals')) session_register($sess_vars);
   else foreach($sess_vars as $varname)$_SESSION[$varname] = $GLOBALS[$varname];
}

function unreg_sess_var($varname){
   global $_SESSION;
   if(ini_get('register_globals')) session_unregister($varname);
   else unset($_SESSION[$varname]);
}

?>
