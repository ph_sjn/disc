<?
setlocale (LC_ALL, 'pt_BR.iso-8859-1');

function checaPermissao($modulo, $tipo_funcao, $user_access) {
	$sql = "SELECT p.nivel FROM perfil_acesso_padrao p, modulos m WHERE ";
	$sql .= " m.id = p.id_modulo AND p.id_perfil_acesso = ". $user_access ." AND m.identificador = '". $modulo ."'";
	$consulta = db_query($sql);
	if (db_num_rows($consulta) > 0) {
		$item = db_fetch_row($consulta);
		switch ($item[0]) {
			case "S":
				return true;
				break;

			case "A":
				if ($tipo_funcao == "A" || $tipo_funcao == "N" || $tipo_funcao == "R")
					return true;
				else
					return false;
				break;

			case "N":
				if ($tipo_funcao == "N" || $tipo_funcao == "R")
					return true;
				else
					return false;
				break;

			case "R":
				if ($tipo_funcao == "R")
					return true;
				else
					return false;
				break;

			default:
				return false;
				break;
		}
	}
	else {
		return false;
	}
}

// FUN��O UTILIZADA PARA REMO��O DE ACENTOS A FIM DE PERMITIR A GRAVA��O DE ARQUIVOS, LOGINS SEM ACENTOS, ETC
function LimparString($string) {
 	$string = implode('_',explode(' ',strtolower($string)));
 	//$string = ereg_replace("[^a-z0-9_]", "", strtr($string, "��������������������������", "aaaaeeiooouucAAAAEEIOOOUUC"));
	$string = preg_replace("[^a-z0-9_]", "", strtr($string, "��������������������������", "aaaaeeiooouucAAAAEEIOOOUUC"));
	
	$chars = '()|:;\/\'"*&%$#@����?�`,.+-!�';
    for ($i=0;$i<strlen($chars);$i++) {
        $string = str_replace($chars[$i],'',$string);
    }
    return $string;
}



function dataMesPortugues($data) {
	GLOBAL $mes_array;
	$data = explode("/", $data);
	return $data[0] ." de ". $mes_array[1 * $data[1]];
}

function dataMesAnoPortugues($data) {
	GLOBAL $mes_array;
	$data = explode("/", $data);
	return $data[0] ." de ". $mes_array[1 * $data[1]] . " de ". $data[2];
}

?>