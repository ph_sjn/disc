<?
$msg_ajuda[1] = "Imagem base \n\n� a imagem usada como base para personaliza��es da pe�a, sem nenhuma edi��o previamente aplicada.\n\nFormatos compat�veis: PNG, JPG e GIF. Esquema de cores 24bits RGB.\n\nUtilize o formato PNG caso a imagem tenha fundo transparente.";
$msg_ajuda[2] = "Imagem modelo  \n\nCaso a imagem base seja maior que 15Mb, envie uma imagem menor para simples visualiza��o. Ou uma imagem j� com as personaliza��es feitas para servir como exemplo.\n\nFormatos compat�veis: PNG, JPG e GIF. Esquema de cores 24bits RGB.";
$msg_ajuda[3] = "Imagem demo \n\nEsta imagem � usada para demonstra��o nas pe�as personaliz�veis. Exibe as vari�veis configuradas a fim de servir como exemplo para o usu�rio fazer as pr�prias personaliza��es na pe�a.";


$msg_ajuda[4] = "Capa/Miolo, Unica, 1a Via, 2a Via,..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Tipo lamina
$msg_ajuda[5] = "Couche, Alta Alvura, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Papel
$msg_ajuda[6] = "120, 180, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Gramatura
$msg_ajuda[7] = "21x29,7 - 42x30, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Formato do trabalho aberto
$msg_ajuda[8] = "21x29,7 - 21x30, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Formato do trabalho fechado
$msg_ajuda[9] = "4x4, 4x0, 1x0, 1x1, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Cores
$msg_ajuda[10] = "2, 4, 8, 16, ..."; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Paginas
$msg_ajuda[11] = "Lamina��o ( Fosca, Brilho )
Verniz UV ( Total, Localizado )
Corte e Vinco
Espiral
Wire-O
Grampeado
Dobrado
Colado
HotMelt
Intercala��o"; // Pagina: marketing_personalizadox_forms_peca.php. Campo: Acabamento

?>