<!DOCTYPE html>

<html>
<head>

	<? // EXIBE O NOME DA EXTRANET ESPECIFICADO NO ARQUIVO DE IDIOMAS, NA PASTA /LANG ?>
	<title>Extranet - <?= $nome_extranet ?></title>			

	<?//FAVICON PARA PC?>
	<link rel="shortcut icon" href="img-icons/favicon.ico" />

	<?//FAVICON PARA DISPOSITIVOS M�VEIS?>
	<link rel="apple-touch-icon" sizes="144x144" href="img-icons/touch-icon-ipad-retina.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="img-icons/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="img-icons/touch-icon-ipad.png" />
	<link rel="apple-touch-icon" href="img-icons/touch-icon-iphone.png" />		

	<? // INICIALIZA O CSS ESPECIFICADO NO ARQUIVO CONFIG.PHP ?>
	<link href="lib/css/padrao.php" type="text/css" rel="stylesheet" >

	<? // CHARSET PADR�O DA EXTRANET: ISO 8859-1 (LATIN1) ?>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

	<? // METATAG VIEWPORT RECEBE "width=device-width" PARA A RESOLU��O SE ADEQUAR AO TAMANHO DA TELA DO DISPOSITIVO ?>
	<!-- <meta name="viewport" content="width=device-width, user-scalable=no"> -->
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=4">


	<?// CARREGAR BIBLIOTECA JQUERY, UTILIZADA PELO CHAT E OUTROS DEMAIS COMPONENTES DO SISTEMA ?>
	<script src="lib/js/jquery/2.0.3/jquery-2.0.3.min.js"></script>
	<script src="lib/js/jquery/ui.1.10.1/jquery-ui-1.10.3.custom.min.js"></script>
	
</head>

<body <? if (!isset($_SESSION['user_kurz'])) { ?> style="background: #DBDBDB;  background-image: url(img/fundo_gradiente_login.png);  background-repeat:repeat-x; " <?}?>>

<!-- Google Analytics inicio -->
  <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38250366-1']);
  _gaq.push(['_setDomainName', '<?=$web_host?>.franquiaextranet.com.br']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Google Analytics Fim --> 


<? // CONT�INER PRINCIPAL DO SISTEMA ?>
<div id="div_conteiner">
	<? if (!isset($_SESSION['user_kurz'])) {?>		
		<div style="position:fixed; width:100%; height:40px; background:#f0f0f0;">
                    <?php
                    /*
			<img src="img/logo.png" style="float:left" height="40px">		
                    */
                    ?>
		</div>
	<?}?>

<? // SE O USU�RIO ESTIVER CONECTADO NO SISTEMA, CARREGA O TOPO DAS P�GINAS INTERNAS ?>
<? if (isset($_SESSION['user_kurz'])) { ?>
	<? include_once("lib/inc/topo_internas_".$movel_tipo.".php"); ?>
<? } /*else { ?>
	<? include_once("lib/inc/topo_capa.php"); ?>
<? } */?>