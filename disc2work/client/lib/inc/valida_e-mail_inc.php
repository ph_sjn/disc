<?
/*
***********************************************************************
Valida Email

Par�metros de entrada: 
email_to (email do destinat�rio)
email_from (email do remetente)

Retorno:

status (retorna o resultado da valida��o) 
0: OK  email v�lido; 
1: NOK email inv�lido
2: email n�o verificado; 

msg_rcpt (retorna uma mensagem de erro dependendo da situa��o decorrente do mesmo)

Funcionamento:
Checa formata��o do email, em seguida procura pelo dom�nio na tabela mx_dominio. Se o dom�nio existe cria uma conex�o com o servidor mx e verifica se o email existe no servidor.
Caso o dom�nio n�o esteja na tabela, o mx � obtido atrav�s da fun��o getmxrr(), nesse caso dependendo da situa��o atualiza-se a tabela com 0 ou 1.
A valida��o final do email consiste no tratamento do c�digo de 3 digitos enviado pelo servidor.

Referencias:
-Email address validation script v2.1.2 Written by Mark 'Tarquin' Wilton-Jones 
-http://php.net/
-http://www.ietf.org/rfc/rfc2821.txt
-http://www.greenend.org.uk/rjk/tech/smtpreplies.html
-http://www.logicalpackets.com/EmailDossier/emailhelo.asp

�LTIMA ATUALIZA��O: 04/04/2013 ----- T�rcio Garcia e �lvaro Salles
***********************************************************************

*/

//para conex�ees de JavaScript
if($cntrl_email ==1) {
session_name("ExtraNet_disc2work_client");
session_start();
$path_pre = "../../";
include_once("../../config.inc.php"); 
include_once("../db/mysql.inc.php");
$email_to = $_GET['email'] ; 
$retorno = valida_email($email_to, "suporte@franquiaextranet.com.br");
echo $retorno[0];	
}

function valida_email($email_to,$email_from){

//list($usec, $sec) = explode(' ', microtime()); $comps_0 = ((int) $sec + ((float) $usec )); 

global $HTTP_HOST, $db_name; 
$port = "25";
$email_to = trim($email_to);	
$email_to = strtolower($email_to);	
$status = 2; $msg_rcpt = "E-mail n�o informado" ; 
	if ($email_to) { 
		$status = 2; $msg_rcpt = "Formata��o do e-mail inv�lida" ;
		// verifica a formata��o
		if (preg_match ("/^[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*@[A-Za-z0-9]+([_.-][A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$/", $email_to)) {
			$status = 2; $msg_rcpt = "E-mail do remetente n�o inserido" ;
				if ($email_from) {
					$dominio = explode('@',$email_to);
					$domain = $dominio[1];
					$domain = strtolower($domain);


				//	$status = 1; $msg_rcpt = "Dom�nio n�o verificado" ;
				//	if ($domain!='bol.com.br' && $domain!='uol.com.br' && $domain!='yahoo.com.br' && $domain!='yahoo.com'){
 						$sql_val = "SELECT status, mx from extranet_admin.mx_dominio WHERE dominio ='$domain'";
						$result_val = mysql_query($sql_val );
						$row_val = mysql_fetch_row($result_val) ;

						$status = 2 ;  $msg_rcpt = "Dom�nio inexistente";
						if ( mysql_num_rows($result_val) > 0){
						//achou
							$mx_status  = $row_val[0];
							$mx_dominio = $row_val[1];
						}
						else {
							// nao encontrou dominio
							getmxrr( $domain, $mxHost, $weightings ) ;
							if ( strlen($mxHost[0]) > 0 ){ 
								$mx_status = 1;
								$mx_dominio = $mxHost[0];
							}
							else{ $mx_status = 0;
								  $mx_dominio = "";
								  $msg_rcpt .= " Erro ao obter MX de dom�nio: ".$domain ; 
								}
							$sql_insert = "INSERT INTO extranet_admin.mx_dominio (dominio, status, mx) VALUES ('".$domain."',".$mx_status.",'".$mx_dominio."')";
							mysql_query($sql_insert );	
							
						}
						
						// verificar o status_mx
						if ($mx_status == 1) {

 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec  + ((float) $usec )); echo  $comps - $comps_0. " trava 1<br>" ;

							$connection = fsockopen ( $mx_dominio, $port );

 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec  + ((float) $usec )); echo  $comps - $comps_0. " trava 2<br>" ;
 	

							if (!$connection){
								getmxrr( $domain, $mxHost, $weightings ) ;
								foreach ($mxHost as $mx_dominio){
									$connection = fsockopen ( $mx_dominio, $port );
									if ($connection){
										$sql_upt_val= "UPDATE extranet_admin.mx_dominio SET status ='1', mx = '".$mx_dominio."' WHERE dominio ='".$domain."'";
										mysql_query($sql_upt_val );
										break;
									}
								}			
							}
// list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec   + ((float) $usec )); echo  $comps - $comps_0 . " trava 3<br>" ;
 		
							$status = 1 ; $msg_rcpt = "Erro ao estabelecer conex�o com o dom�nio: ".$domain."  " ;

							if ($cn = fgets( $connection, 1024 )) {
 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec   + ((float) $usec )); echo  $comps - $comps_0. " trava 4<br>" ;								
											$msg_rcpt .= $cn ; 
											$cod = substr($cn , 0, 3);
										    //echo "<br>".$cod."<br>".$cn."<br><br>";

 								// 220 The server is ready
								if ( $cod == '220' ) { 

									// while( preg_match( "/^2\d\d-/", $cn ) ) { $cn = fgets( $connection, 1024 ); } 

											//$msg_rcpt = $cn ; 
											//$cod = substr($msg_rcpt, 0, 3);
											//echo "<br>---- >".$cod."<br>".$cn."<br><br>";

										$status = 1; $msg_rcpt = "Dom�nio n�o verificado" ;

										if( !$_SERVER ) { global $HTTP_SERVER_VARS; $_SERVER = $HTTP_SERVER_VARS; }
										//attempt to send an email from the user to themselves (not <> as some misconfigured servers reject it)
										$localHostIP = gethostbyname(preg_replace("/^.*@|:.*$/",'',$_SERVER['HTTP_HOST']));
										$localHostName = gethostbyaddr($localHostIP);
										fputs( $connection, 'EHLO '.($localHostName?$localHostName:('['.$localHostIP.']'))."\r\n" );

 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec  + ((float) $usec )); echo  $comps- $comps_0. " trava 5a<br>" ;

										$status = 1 ; $msg_rcpt = "Erro de comunica��o [MAIL FROM]" ;
										if( $hl = fgets( $connection, 1024 )  ) {

 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec  + ((float) $usec )); echo  $comps- $comps_0. " trava 5b<br>" ;	

											$txt = "MAIL FROM:<".$email_from.">\r\n";			
											fputs( $connection, $txt  );

											
											$status = 1 ; $msg_rcpt = "Erro de comunica��o [CONNECTION]" ;
											
											if( $cn = fgets( $connection, 1024 ) ) {

												while( preg_match( "/^2\d\d-/", $cn ) ) { $cn = fgets( $connection, 1024 ) ;} 



												
												$txt ="RCPT TO:<".$email_to.">\r\n";
												fputs( $connection,  $txt );
												 	
												$cn1 = fgets( $connection, 1024 ) ; 

												$msg_rcpt .= $cn1 ; 
												$cod = substr($cn1 , 0, 3);




												$msg_rcpt = fgets( $connection, 1024 ) ; 
												$cod = substr($msg_rcpt, 0, 3);
												






												$txt ="RSET";
												fputs( $connection,  $txt );
												
												$txt ="QUIT";
												fputs( $connection,  $txt );
												
												switch ($cod){
													case 250; //250	Requested mail action okay, completed
													case 251; //251	User not local; will forward to <forward-path>
														$status = 0; $msg_rcpt = "E-mail v�lido" ;
														break;
													case 421; //421	<domain> Service not available, closing transmission channel
														$status = 1; //E-mail n�o pode ser verificado 
														$msg_rcpt = "Erro de comunica��o " .$cod  ;
														break;

													case 452; //452	Requested action not taken: insufficient system storage
													case 500; //500	Syntax error, command unrecognised
													case 501; //501	Syntax error in parameters or arguments
													case 503; //503	Bad sequence of commands
													case 521; //521	<domain> does not accept mail [rfc1846]
													case 552; //552	Requested mail action aborted: exceeded storage allocation
													 	$status = 1; //E-mail n�o pode ser verificado
														break;
														
		 												
													case 450; //450	Requested mail action not taken: mailbox unavailable
													case 451; //451	Requested action aborted: local error in processing
														$status = 1; //E-mail n�o pode ser verificado 
														break;
													case 550; //550	Requested action not taken: mailbox unavailable
														$status = 1; //E-mail n�o pode ser verificado
														if     ( strpos($msg_rcpt, "Recipient address rejected: temporarily blocked"        ) > -1   )  { $status = 1; } //E-mail n�o pode ser verificado
														elseif ( strpos($msg_rcpt, "Recipient address rejected: Mail appeared to be SPAM"   ) > -1   )  { $status = 1; } //E-mail n�o pode ser verificado
													  //elseif ( strpos($msg_rcpt, "Recipient address rejected: Server configuration error" ) > -1   )  { $status = 1; } //E-mail n�o pode ser verificado

														elseif ( strpos($msg_rcpt, "Recipient address rejected: User unknown"               ) > -1   )  { $status = 2; } // E-mail  invalido
														elseif ( strpos($msg_rcpt, "Recipient address rejected"                             ) > -1   )  { $status = 2; } // E-mail  invalido
														elseif ( strpos($msg_rcpt, "mailbox unavailable"                                    ) > -1   )  { $status = 2; }  
														elseif ( strpos($msg_rcpt, "does not exist"                                         ) > -1   )  { $status = 2; }  
														elseif ( strpos($msg_rcpt, "No Such User Here"                                      ) > -1   )  { $status = 2; } 
														elseif ( strpos($msg_rcpt, "User unknown"                                           ) > -1   )  { $status = 2; } 
														elseif ( strpos($msg_rcpt, "Mailbox disabled for this recipient"                    ) > -1   )  { $status = 2; } 
														elseif ( strpos($msg_rcpt, "Address rejected"                                       ) > -1   )  { $status = 2; }
													



														break;
													case 551; //551	User not local; please try <forward-path>
													case 553; //553	Requested action not taken: mailbox name not allowed
														$status = 2; //E-mail inv�lido
														//$msg_rcpt = "Erro de comunica��o []  " .$cod;
														break;

													case 554; //554 Transition failed
		 												$status = 1; //E-mail n�o pode ser verificado
		 												if     ( strpos($msg_rcpt, "Recipient address rejected"         ) > -1   )  { $status = 2; } //E-mail n�o pode ser verificado
														break;



													default;
														$status = 1 ; //erro n�o identificado
														$msg_rcpt = "Erro de comunica��o " .$cod  ;
													break;
												}					
												//fim do Switch
											}
									}	
								} //else { $msg_rcpt .= fgets( $connection, 1024 ) ;    } 
							} 
						// Fechar o socket
						fclose($connection);
					//}
				}
			}
		}
	}

 //list($usec, $sec) = explode(' ', microtime()); $comps = ((int) $sec   + ((float) $usec )); echo  $comps - $comps_0. " trava 6<br>" ;	
	return array ($status, $msg_rcpt) ;
}
?>