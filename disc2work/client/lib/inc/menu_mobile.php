<script type="text/javascript">
	function abreSubmenu(id){
		if (document.getElementById(id).style.display == "none"){
			document.getElementById(id).style.display = "block";
		}else{
			document.getElementById(id).style.display = "none"
		}
	}
</script>

<style type="text/css">

#div_menu {
    margin: 0 auto; 
    width: 100%;
    display: none;
    text-align: left;    
}

.item{
	padding: 15px;
	font-weight: bold;
	color: #ffffff;
}


.item.nivel1{
	background: <?=$cor2?>;
	border-bottom: 1px dotted #ededed;	

}

.item.nivel2{
	text-indent:20px;
	background: #426ca9;
	border-bottom: 1px dotted #ededed;	

}

.item.nivel3{
	text-indent:40px;
	background: #6886b8;
	border-bottom: 1px dotted #ededed;


}

</style>

<div id="div_menu">
	<?$sql = "select m.id, m.descricao, m.identificador, m.icone, m.pasta from modulos m, perfil_acesso_padrao p ";
	$sql .= " where m.nivel_pai = 0 AND p.id_modulo = m.id AND m.identificador <> 'admin'  ";
	$sql .= "AND (tipo <> 'sistema' OR tipo = '' OR tipo IS NULL) AND p.id_perfil_acesso = ". $user_access;
	$sql .= " AND m.situacao = 'A'  ";
	$sql .= " ORDER BY ordem asc";
	$result = db_query($sql) or db_error();
	while ($item = db_fetch_row($result)) { 	
		if ($module == $item[2]) {
			$pasta = $item[4];
		}	
		$sql_nivelpai = "SELECT id, descricao, identificador, nivel_pai, icone, pasta FROM modulos ";
		$sql_nivelpai .= "WHERE identificador = '". $module . "'";
		$result_nivelpai = db_query($sql_nivelpai) or db_die();
		$row_nivelpai = db_fetch_row($result_nivelpai) or db_die();
	
		if ($row_nivelpai[3] == 0) {
			$sql_modulo_inicial = "SELECT id, descricao, identificador, nivel_pai, icone, pasta FROM modulos ";
			$sql_modulo_inicial .= "WHERE nivel_pai = '". $row_nivelpai[0] . "'  ";
			$sql_modulo_inicial .= " AND situacao = 'A'  ";
			$sql_modulo_inicial .= " ORDER BY ordem asc LIMIT 1";
			$result_modulo_inicial = db_query($sql_modulo_inicial) or db_die();
			if (db_num_rows($result_modulo_inicial) > 0) {
				$row_modulo_inicial = db_fetch_row($result_modulo_inicial);
				if ($row_modulo_inicial[2] != "") {
					$module = $row_modulo_inicial[2];
				}
			}
		}
		if ($row_nivelpai[3] == 0) { //SE O M�DULO SELECIONADO FOR MODULO DO PRIMEIRO NIVEL
			$nivel_pai = $row_nivelpai[0];
		}
		else { //SE N�O FOR, PEGA O ID DO N�VEL PAI
			$nivel_pai = $row_nivelpai[3];
		}?>
		<div>				
			<a href="index.php?module=<?=$item[2]?>" id="a_<?=$item[2]?>" name="a_<?= $item[2] ?>">			
				<div class="item nivel1" onclick="abreSubmenu(<?=$item[0]?>)">				
					<?=$item[1]?>				
				</div>
			</a>
			<?$modulo_inicial = "";
			$sql = "select m.id, m.descricao, m.identificador, m.pasta, m.nivel  from modulos m, perfil_acesso_padrao p ";
			$sql .= " where m.nivel_pai = ". $item[0] ." AND p.id_modulo = m.id AND m.situacao = 'A' ";
			$sql .= " AND p.id_perfil_acesso = ". $user_access . " ORDER BY ordem asc";
			$consulta_submodulo = db_query($sql) or db_error();
			if (db_num_rows($consulta_submodulo) > 0) { ?>
				
				<!-- INICIO
				SCRIPT PARA DESATIVAR O CLIQUE NO M�DULO PRINCIPAL NO MENU DA EXTRANET, 
				EM RAZ�O DOS DISPOSITIVOS TOUCH SCREEN FUNCIONAREM DE MANEIRA DIFERENTE COM O "MOUSEOVER": 
				O TOQUE � INTERPRETADO COMO EVENTO MOUSEOVER E AO MESMO TEMPO UM EVENTO ONCLICK
				-->
				<script language="javascript">
					if ( document.getElementById("a_<?= $item[2] ?>") != null ) {
						document.getElementById("a_<?= $item[2] ?>").href="javascript:void(0)"; 
					}
				</script>
				<!-- FIM -->		
				<div id="<?=$item[0]?>" style="display:none">
					<?while ($item_submodulo = db_fetch_row($consulta_submodulo)) { 
						if ($item_submodulo[2] == $module) {
							$pasta = $item_submodulo[3];
						}
						if ($item_submodulo[4] == 1) { 								
							//if ($module == $item_submodulo[2]) {
							
							/*
								NOVO IF COM TRATAMENTO ESPECIAL PARA SUBM�DULOS DO MDONLINE
								- O MDOnLine possui apenas uma pasta, mas v�rias telas de configura��o
								- Controlar a navega��o das telas consultando o par�metro $mode passado na identifica��o do m�dulo
							*/?>
							<a href="index.php?module=<?= $item_submodulo[2] ?>">
						       	<div class="item nivel2">					       		
									<?= $item_submodulo[1] ?>								
								</div>
							</a>
						<?}else {?>
							<div>
								<div class="item nivel2" onclick="abreSubmenu(<?=$item_submodulo[0]?>)">
									<?=$item_submodulo[1]?>									
								</div>
								<div id="<?=$item_submodulo[0]?>" style="display:none">
									<?$sql_subniveis = "select m.id, m.descricao, m.identificador, m.pasta, m.nivel from modulos m, perfil_acesso_padrao p ";
									$sql_subniveis .= " where m.nivel_pai = ". $item_submodulo[0] ." AND p.id_modulo = m.id AND m.situacao = 'A' ";
									$sql_subniveis .= " AND p.id_perfil_acesso = ". $user_access . " ORDER BY m.ordem asc";									
									$consulta_submodulo2 = db_query($sql_subniveis) or db_error();
									while ($item_submodulo2 = db_fetch_row($consulta_submodulo2)) {?>	
										<a href="index.php?module=<?= $item_submodulo2[2] ?>">
										    <div class="item nivel3"> 								       		
												<?= $item_submodulo2[1]?>											
											</div>
										</a>
									<?}?>
								</div>
						   </div>
						<?}?>	
					<?}?>
				</div>			
			<?}?>
		</div>
	<?}?>
</div>
<br>




