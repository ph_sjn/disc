<?

// gpcs_vars.inc.php - PHProjekt Vers�o 4.0
// copyright � 2000-2003 Albrecht Guenther  ag@phprojekt.com
// www.phprojekt.com
// Autor: Albrecht Guenther
// Adaptado por: RPconsultoria - info@RPconsultoria.com.br

function arr_addsl(&$item,$key){$item = addslashes($item);}

if(isset($_GET)) {
  foreach($_GET as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else{
           if(is_array($value)) {array_walk($value,'arr_addsl'); $$key=$value;}
         else $$key=addslashes($value);
  }
}
}

if(isset($_POST)) {
  foreach($_POST as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else{
           if(is_array($value)) {array_walk($value,'arr_addsl'); $$key=$value;}
           else $$key=addslashes($value);
         }
  } 
}

if(isset($_COOKIE)) {
  foreach($_COOKIE as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else $$key=addslashes($value);
  }
}

if(isset($_SESSION)) {
  foreach($_SESSION as $key=>$value) {
         $$key=$value;
  }
}

$userfile = "";

if(isset($_FILES['userfile'])){
  $userfile_name = $_FILES['userfile']['name'];
  $userfile_type = $_FILES['userfile']['type'];
  $userfile_size = $_FILES['userfile']['size'];
  $userfile = $_FILES['userfile']['tmp_name'];
}

$HTTP_USER_AGENT = getenv("HTTP_USER_AGENT");
$PHP_SELF = $_SERVER["PHP_SELF"];

function quote_runtime($x){
   if(!get_magic_quotes_runtime()) {
      if(is_array($x)) array_walk($x,'arr_addsl');
      else $x = addslashes($x);
   }
   return $x;
}

function reg_sess_vars($sess_vars){
   global $_SESSION;
   if(ini_get('register_globals')) session_register($sess_vars);
   else foreach($sess_vars as $varname)$_SESSION[$varname] = $GLOBALS[$varname];
}

function unreg_sess_var($varname){
   global $_SESSION;
   if(ini_get('register_globals')) session_unregister($varname);
   else unset($_SESSION[$varname]);
}

/*
// gpcs_vars.inc.php - PHProjekt Vers�o 4.0
// copyright � 2000-2003 Albrecht Guenther  ag@phprojekt.com
// www.phprojekt.com
// Autor: Albrecht Guenther
// Adaptado por: RPconsultoria - info@RPconsultoria.com.br

function arr_addsl(&$item,$key){$item = addslashes($item);}

if(isset($HTTP_GET_VARS)) {
  reset($HTTP_GET_VARS);
  foreach($HTTP_GET_VARS as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else{
           if(is_array($value)) {array_walk($value,'arr_addsl'); $$key=$value;}
         else $$key=addslashes($value);
  }
}
}

if(isset($HTTP_POST_VARS)) {
  reset($HTTP_POST_VARS);
  foreach($HTTP_POST_VARS as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else{
           if(is_array($value)) {array_walk($value,'arr_addsl'); $$key=$value;}
           else $$key=addslashes($value);
         }
  } 
}

if(isset($HTTP_COOKIE_VARS)) {
  reset($HTTP_COOKIE_VARS);
  foreach($HTTP_COOKIE_VARS as $key=>$value) {
         if(get_magic_quotes_gpc()) $$key=$value;
         else $$key=addslashes($value);
  }
}

if(isset($HTTP_SESSION_VARS)) {
  reset($HTTP_SESSION_VARS);
  foreach($HTTP_SESSION_VARS as $key=>$value) {
         $$key=$value;
  }
}

$userfile = "";

if(isset($HTTP_POST_FILES['userfile'])){
  $userfile_name = $HTTP_POST_FILES['userfile']['name'];
  $userfile_type = $HTTP_POST_FILES['userfile']['type'];
  $userfile_size = $HTTP_POST_FILES['userfile']['size'];
  $userfile = $HTTP_POST_FILES['userfile']['tmp_name'];
}

$HTTP_USER_AGENT = getenv("HTTP_USER_AGENT");
$PHP_SELF = $HTTP_SERVER_VARS["PHP_SELF"];

function quote_runtime($x){
   if(!get_magic_quotes_runtime()) {
      if(is_array($x)) array_walk($x,'arr_addsl');
      else $x = addslashes($x);
   }
   return $x;
}

function reg_sess_vars($sess_vars){
   global $HTTP_SESSION_VARS;
   if(ini_get('register_globals')) session_register($sess_vars);
   else foreach($sess_vars as $varname)$HTTP_SESSION_VARS[$varname] = $GLOBALS[$varname];
}

function unreg_sess_var($varname){
   global $HTTP_SESSION_VARS;
   if(ini_get('register_globals')) session_unregister($varname);
   else unset($HTTP_SESSION_VARS[$varname]);
}
*/
?>
