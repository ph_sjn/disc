// Inicializar timeout de verifica��o de novas mensagens
var timeout=0; 
var vMsgChat=jQuery.noConflict();
var jChat=jQuery.noConflict();
var inicializado=false;

// Ini - Verificar novas mensagens do chat
vMsgChat(document).ready(function() {
	verificaMensagensChat();
});
// Fim - Verificar novas mensagens do chat


// Ini - Inicializar componente draggable na janela do chat, usando noConflict para evitar conflitos com outros componentes jQuery 
function arrastaDiv(){
	if(!inicializado){
		jChat("#div_chat").draggable();
		inicializado=!inicializado;
	}else jChat("#div_chat").draggable("enable");
}

function paraDeArrastarADiv(){
	jChat("#div_chat").draggable("disable");
}

function verEnvio(e){
	if(e.keyCode==13){
		document.getElementById("btn_envia_mensagem_chat").click();
		return false;
	}
}
// Fim - Inicializar componente draggable na janela do chat, usando noConflict para evitar conflitos com outros componentes jQuery 



/* Ini - Fun��es do menu que aparece ao passar o ponteiro sobre o nome do usu�rio no canto superior direito */
function hideMenu(){ 
    document.getElementById('menu').setAttribute('style', 'display:none'); 
} 
 
 function showMenu(){ 
    clearTimeout(timeout); 
    document.getElementById('menu').setAttribute('style', 'display:block'); 
} 
 
function requestHide() { 
    if (timeout > 0) clearTimeout(timeout); 
    timeout = 0; 
    timeout = setTimeout(" hideMenu( )", 0); 
} 
 /* Fim - Fun��es do menu que aparece ao passar o ponteiro sobre o nome do usu�rio no canto superior direito */
 