<?
//
// Fun��o rp_mail usando PHPMailer v5.2.21
// 06-02-2017
// ARLYSON
//

//require("/home/users/extranet/PHPMailer-5.2.23/PHPMailerAutoload.php");
//require($CONFIGENET_EXTRANET . "PHPMailer-5.2.23/PHPMailerAutoload.php");
// email_to = "array,string";  $subject  = "titulo do email", $body = " corpo do email", "anexo = array com caminho e nome de 1 arquivo";

function rp_mail($email_to, $subject, $body, $anexo = false) {


    global  $mail_username, 
            $mail_password,
            $mail_smtp,
            $mail_port ,
            $mail_sender ,
            $mail_from_name ,
            $mail_from ,
            $mail_reply_to ,
            $mail_reply_to_name ;
 
//	$body .="<br><br><font color='#898989'>Por favor, n�o retorne este e-mail. Clique no link e responda diretamente na extranet.</font><br>";
    setlocale(LC_ALL, 'pt_BR');


    //Create a new PHPMailer instance
    $mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    // 3 = exibe todas as informa��es
    $mail->SMTPDebug = 0;

    //Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';

    //Set the hostname of the mail server
    $mail->Host = $mail_smtp;

    //Set the SMTP port number - likely to be 25, 465 or 587
    $mail->Port = $mail_port ;

    //Set the encryption system to use - ssl (deprecated) or tls
    if  ($mail_smtp =='smtp.gmail.com'  ) {$mail->SMTPSecure = 'tls';}
    else{ $mail->SMTPSecure = ''; }

    //Whether to use SMTP authentication
    if ($mail_smtp =='rp-srv5.franquiaextranet.com.br') { $mail->SMTPAuth = false; }
    else{ $mail->SMTPAuth = true; }   

    $mail->AuthType ="LOGIN";

    //Username to use for SMTP authentication
    $mail->Username = $mail_username ;

    //Password to use for SMTP authentication
    $mail->Password = $mail_password;

    //Set who the message is to be sent from
    $mail->setFrom($mail_from, $mail_from_name);

    //Set an alternative reply-to address
    $mail->addReplyTo($mail_reply_to, $mail_reply_to_name);
    
    // set word wrap
    $mail->WordWrap = 70; 

    //Set the subject line
    $mail->Subject = $subject;

    //$mail->isHTML(true);
    
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
    $mail->msgHTML($body);

    //Replace the plain text body with one created manually
    $mail->AltBody = $body;
    //Attach an image file
    //$anexo[0] =  caminho do arquivo
    //$anexo[1] = nome do arquivo
    if($anexo) { 
        foreach ($anexo as $key => $value) {
            $mail->AddAttachment($anexo[$key][0], $anexo[$key][1]) ;
         }   
    }

    $retorno_email[0] = 1; 
    $retorno_email[1] = "Mensagem enviada"; 
    $retorno_email[2] = "" ;

    if(is_array($email_to)){

        $retorno_email = array();
        $retorno_email[0] = 1; 
        $retorno_email[1] = "Mensagem enviada"; 
        $retorno_email[2] = "" ;

    //  N�mero de emails a serem enviados por bloco
        $num_email_bloco = 23; // 101
               
    //  N�mero de emails a serem enviados
        $num_email = count($email_to);

    //  Separando a lista de endere�os por blocos de envio
        $email_to = array_chunk($email_to, $num_email_bloco);

    //  N�mero de envios 
        $num_envios = count($email_to);

        for ($i = 0; $i < $num_envios ; $i++) { //00            
            foreach ($email_to[ $i ] as $key => $value) { //01
                list ( $mail_to_name_aux, $email_to_aux ) = NameAddress ($email_to[ $i ][$key]);
                if (strpos($email_to_aux,"#") === false )  { $mail->AddBCC($email_to_aux, $mail_to_name_aux);}  
            }   //01 
       //    Envia e-mail 
            if( ! $mail->Send()) { 
                $retorno_email[0]    = 0 ; 
                $retorno_email[1]   .= " Mailer Error: " . $mail->ErrorInfo; 
            }
          $mail->ClearBCCs();        
        } //00
    }
    else{
        if (strpos($email_to,"#") !== false )  {  
            $retorno_email[0]  = 0 ; 
            $retorno_email[1]  = "Email invalido: ". $email_to ; 
            $retorno_email[2]  = "";
            return  $retorno_email;
        }
        //Set who the message is to be sent to
        list ( $mail_to_name_aux, $email_to_aux ) = NameAddress ($email_to);
        $mail->addAddress($email_to_aux, $mail_to_name_aux  );         
        if( ! $mail->Send()) { 
            $retorno_email[0]   = 0 ; 
            $retorno_email[1]   = "Mailer Error: " . $mail->ErrorInfo; 
            $retorno_email[2]   = "Username: ".$mail_username. "<BR>";
            $retorno_email[2]  .= "Host: ".$mail_smtp. "<BR>";
            $retorno_email[2]  .= "Sender: ".$mail_sender. "<BR>";
            $retorno_email[2]  .= "From: ".$mail_from. "<BR>";
            $retorno_email[2]  .= "Reply_to". $mail_reply_to. "<BR>";
            $retorno_email[2]  .= "SMTPSecure: ".$mail->SMTPSecure . "<BR>";
            $retorno_email[2]  .= "SMTPAuth: ".$mail->SMTPAuth . "<BR>"; 
            $retorno_email[2]  .= "AuthType: ".$mail->AuthType . "<BR>"; 
            $retorno_email[2]  .= "Host: ".$mail->Host . "<BR>"; 
            $retorno_email[2]  .= "Port: ".$mail->Port . "<BR>"; 
            $retorno_email[2]  .= "Debugoutput: ".$mail->Debugoutput . "<BR>";
            $retorno_email[2]  .= "SMTPDebug: ".$mail->SMTPDebug . "<BR>";

        }
    }
   
    $mail->ClearBCCs();
    $mail->ClearAddresses();
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();

    return  $retorno_email;
}

function NameAddress ($email_to){
    $retorno_NameAddress = array(); 
    if (strpos($email_to,"<")) {
        $pos1 = strpos($email_to,"<") + 1  ;
        $pos2 = strpos($email_to,">") ;
        $retorno_NameAddress[0] = substr($email_to, 0, $pos1-1);
        $retorno_NameAddress[1] = substr($email_to,  $pos1, $pos2-$pos1 );   
    }
    else {
            $retorno_NameAddress[0] = "";
            $retorno_NameAddress[1] = $email_to ;    
    }
    return $retorno_NameAddress;
}

?>