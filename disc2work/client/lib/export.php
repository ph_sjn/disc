<?

$path_pre="../";
$include_path = "lib.inc.php";
include_once $include_path;

include_once("db/$db_type.inc.php"); 

//SE A EXPORTA��O FOR DE ACESSO
$fields = array("usuario","entrada","saida","downloads");
$f_lang = array("Usu�rio","Entrada","Saida","Downloads");
$sql_fields = implode(",",$fields);
$query = "select usuario, entrada, saida, downloads from acesso where aux_data >= $data1 and aux_data <= $data2";
$export_array = make_export_array($query);
$file = "acessos";

// NOME DO ARQUIVO
$name = $file .".". $medium;

// Impress�o e HTML devem ser mostrados inline
if (ereg("html|print",$medium)) {
	$file_download_type = "inline";
}
else {
	$file_download_type = "attachment";
}
$include_path = "get_contenttype.inc.php";
include_once $include_path; 


switch ($medium) {
  // RTF
  case "rtf":
    $rtfstring = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang2055{\\fonttbl{\\f0\fnil\\fcharset0 Helvetica;}}\n";
    $rtfstring .= "\\viewkind4\\uc1\\pard\\f0\\fs20";
    for ($i=0; $i<count($f_lang)-1;$i++) { 
      $rtfstring .= "\\b $f_lang[$i]\\b0\\tab";      
    }
    $i++;
    $rtfstring .= "\\b $f_lang[$i]\\b0\\par\n";    
    foreach ($export_array as $line) {
      for ($i=0;$i < count($line)-1; $i++) {  
        $rtfstring .= " $line[$i] \\tab";
      }
      $i++;
      $rtfstring .= " $line[$i]\\par\n";
    }
    $rtfstring .="}";
    echo $rtfstring;
  break; 

  // DOC
  case "doc":
    $rtfstring = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang2055{\\fonttbl{\\f0\fnil\\fcharset0 Helvetica;}}\n";
    $rtfstring .= "\\viewkind4\\uc1\\pard\\f0\\fs20";
    for ($i=0; $i<count($f_lang)-1;$i++) { 
      $rtfstring .= "\\b $f_lang[$i]\\b0\\tab";      
    }
    $i++;
    $rtfstring .= "\\b $f_lang[$i]\\b0\\par\n";    
    foreach ($export_array as $line) {
      for ($i=0;$i < count($line)-1; $i++) {     
        if (!$line[$i]) $line[$i] = " ";
        $rtfstring .= " $line[$i]\\tab";
      }
      $i++; 
      if (!$line[$i]) $line[$i] = " ";
      $rtfstring .= " $line[$i]\\par\n";
    }
    $rtfstring .="}";
    echo $rtfstring;
  break;   

  // P�gina de imprens�o normal
  case "print":
  echo "<html><body bgcolor=ffffff onLoad='self.print()'>";
  echo "<table border=1 cellpadding=1 cellspacing=0>\n";
  echo "<tr>";
	for ($i=0; $i<count($f_lang);$i++) { 
		echo "<td>$f_lang[$i]</td>\n";
	}
  echo "</tr>\n";
  foreach ($export_array as $line) {
    echo "<tr>\n";
    foreach ($line as $element) {
      echo "<td>$element&nbsp;</td>";
    }
    echo "</tr>\n";
  }
  echo "</table></body></html>";
  break;

  // Exportar XLS
  case "xls":
    $xlsstring = pack( "ssssss", 0x809, 0x08, 0x00,0x10, 0x0, 0x0 );
    for ($i=0; $i<count($f_lang);$i++) { 
      $xlsstring .= pack( "s*", 0x0204, 8+strlen($f_lang[$i]), 0, $i, 0x00, strlen($f_lang[$i]) );
      $xlsstring .= $f_lang[$i];      
    }
    $a=0;
    foreach ($export_array as $line) {
      for ($i=0;$i < count($line); $i++) {
        $line[$i] = str_replace("\r", "", $line[$i]);
        $xlsstring .= pack( "s*", 0x0204, 8+strlen($line[$i]), $a+1, $i, 0x00, strlen($line[$i]) );
        $xlsstring .= $line[$i];
      }
      $a++;
    }
    $xlsstring .= pack("ss", 0x0A, 0x00);
    echo $xlsstring;
    break;  

  // HTML
  case "html":
    echo "<html><body bgcolor=$bgcolor3>";
	echo "<table border=0 cellpadding=10 cellspacing=0>\n";
    echo "<tr bgcolor=$bgcolor2 align='center'>";
    for ($i=0; $i<count($f_lang);$i++) {
		echo "<td width=25%><b>$f_lang[$i]</b></td>\n";
	}
    echo "</tr>\n";
    if ($export_array) { 
      foreach ($export_array as $line) {
        if (($cnr/2) == round($cnr/2)) { $color = "$bgcolor1"; $cnr++;}
        else { $color = "$bgcolor2"; $cnr++; }
        echo "<tr bgcolor=$color>\n";
        foreach ($line as $element) {
          echo "<td>$element&nbsp;</td>";
        }
        echo "</tr>\n";
      }  
    }
    echo "</table></body></html>";
    break;
}

function make_export_array($query) {
	$result = db_query($query) or db_die();
	while ($row = db_fetch_row($result)) { 
		$line = array();
		foreach($row as $element) { $line[] = $element; }
		$export_array[] = $line;
	}
	return $export_array;
}

?>
