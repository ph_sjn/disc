<?

include_once($diretorio_base ."lib/classes/VO/UsuariosVO.php");

class UsuariosDAO {

	function consultar() {
		$sql = "SELECT id, vorname, nachname, kurz, pw, firma, gruppe, email, acc, tel1, tel2, fax, strasse, stadt,";
		$sql .= " plz, land, sprache, mobil, loginname, ldap_name, anrede, sms, role, proxy, settings, acesso, notificacoes, corte_geografico FROM users where nachname <> 'Root'";
		$consulta = db_query($sql) or db_die("ERRO DE CONSULTA");

		$ponteiro = 0;
		while ($tupla = db_fetch_row($consulta) ) {
			$usuariosVO = new UsuariosVO();
			$usuariosVO->setID($tupla[0]);
			$usuariosVO->setVorname($tupla[1]);
			$usuariosVO->setNachname($tupla[2]);
			$usuariosVO->setKurz($tupla[3]);
			$usuariosVO->setPw($tupla[4]);
			$usuariosVO->setFirma($tupla[5]);
			$usuariosVO->setGruppe($tupla[6]);
			$usuariosVO->setEmail($tupla[7]);
			$usuariosVO->setAcc($tupla[8]);
			$usuariosVO->setTel1($tupla[9]);
			$usuariosVO->setTel2($tupla[10]);
			$usuariosVO->setFax($tupla[11]);
			$usuariosVO->setStrasse($tupla[12]);
			$usuariosVO->setStadt($tupla[13]);
			$usuariosVO->setPlz($tupla[14]);
			$usuariosVO->setLand($tupla[15]);
			$usuariosVO->setSprache($tupla[16]);
			$usuariosVO->setMobil($tupla[17]);
			$usuariosVO->setLoginname($tupla[18]);
			$usuariosVO->setLdap_name($tupla[19]);
			$usuariosVO->setAnrede($tupla[20]);
			$usuariosVO->setSms($tupla[21]);
			$usuariosVO->setRole($tupla[22]);
			$usuariosVO->setProxy($tupla[23]);
			$usuariosVO->setSettings($tupla[24]);
			$usuariosVO->setAcesso($tupla[25]);
			$usuariosVO->setNotificacoes($tupla[26]);
			$usuariosVO->setCorte_geografico($tupla[27]);
			
			$listUsuarios[$ponteiro] = $usuariosVO;
			$ponteiro++;
		}
		return $listUsuarios;
	}

}


?> 
