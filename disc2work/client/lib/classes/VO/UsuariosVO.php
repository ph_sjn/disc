<?

class UsuariosVO {

	var $ID; //(int)
	var $vorname; //(string)
	var $nachname; //(string)
	var $kurz; //(string)
	var $pw; //(string)
	var $firma; //(string)
	var $gruppe; //(int)
	var $email; //(string)
	var $acc; //(string)
	var $tel1; //(string)
	var $tel2; //(string)
	var $fax; //(string)
	var $strasse; //(string)
	var $stadt; //(string)
	var $plz; //(string)
	var $land; //(string)
	var $sprache; //(string)
	var $mobil; //(string)
	var $loginname; //(string)
	var $ldap_name; //(string)
	var $anrede; //(string)
	var $sms; //(string)
	var $role; //(int)
	var $proxy; //(string)
	var $settings; //(string)
	var $acesso; //(int)
	var $notificacoes; //(string)
	var $corte_geografico; //(string)

	
	///////////////////////////////////////
	//M�todos SET
	///////////////////////////////////////


	
	function setID($ID) {

		$this->ID = (int) $ID;

	}

	
	function setVorname($vorname) {

		$this->vorname = (string) $vorname;

	}

	
	function setNachname($nachname) {

		$this->nachname = (string) $nachname;

	}

	
	function setKurz($kurz) {

		$this->kurz = (string) $kurz;

	}

	
	function setPw($pw) {

		$this->pw = (string) $pw;

	}

	
	function setFirma($firma) {

		$this->firma = (string) $firma;

	}

	
	function setGruppe($gruppe) {

		$this->gruppe = (int) $gruppe;

	}

	
	function setEmail($email) {

		$this->email = (string) $email;

	}

	
	function setAcc($acc) {

		$this->acc = (string) $acc;

	}

	
	function setTel1($tel1) {

		$this->tel1 = (string) $tel1;

	}

	
	function setTel2($tel2) {

		$this->tel2 = (string) $tel2;

	}

	
	function setFax($fax) {

		$this->fax = (string) $fax;

	}

	
	function setStrasse($strasse) {

		$this->strasse = (string) $strasse;

	}

	
	function setStadt($stadt) {

		$this->stadt = (string) $stadt;

	}

	
	function setPlz($plz) {

		$this->plz = (string) $plz;

	}

	
	function setLand($land) {

		$this->land = (string) $land;

	}

	
	function setSprache($sprache) {

		$this->sprache = (string) $sprache;

	}

	
	function setMobil($mobil) {

		$this->mobil = (string) $mobil;

	}

	
	function setLoginname($loginname) {

		$this->loginname = (string) $loginname;

	}

	
	function setLdap_name($ldap_name) {

		$this->ldap_name = (string) $ldap_name;

	}

	
	function setAnrede($anrede) {

		$this->anrede = (string) $anrede;

	}

	
	function setSms($sms) {

		$this->sms = (string) $sms;

	}

	
	function setRole($role) {

		$this->role = (int) $role;

	}

	
	function setProxy($proxy) {

		$this->proxy = (string) $proxy;

	}

	
	function setSettings($settings) {

		$this->settings = (string) $settings;

	}

	
	function setAcesso($acesso) {

		$this->acesso = (int) $acesso;

	}

	
	function setNotificacoes($notificacoes) {

		$this->notificacoes = (string) $notificacoes;

	}
	
	function setCorte_geografico($corte_geografico) {

		$this->corte_geografico = (string) $corte_geografico;

	}

	///////////////////////////////////////
	//M�todos GET
	///////////////////////////////////////


		
	function getID() {

		return (int) $this->ID;

	}
	
	function getVorname() {

		return (string) $this->vorname;

	}
	
	function getNachname() {

		return (string) $this->nachname;

	}
	
	function getKurz() {

		return (string) $this->kurz;

	}
	
	function getPw() {

		return (string) $this->pw;

	}
	
	function getFirma() {

		return (string) $this->firma;

	}
	
	function getGruppe() {

		return (int) $this->gruppe;

	}
	
	function getEmail() {

		return (string) $this->email;

	}
	
	function getAcc() {

		return (string) $this->acc;

	}
	
	function getTel1() {

		return (string) $this->tel1;

	}
	
	function getTel2() {

		return (string) $this->tel2;

	}
	
	function getFax() {

		return (string) $this->fax;

	}
	
	function getStrasse() {

		return (string) $this->strasse;

	}
	
	function getStadt() {

		return (string) $this->stadt;

	}
	
	function getPlz() {

		return (string) $this->plz;

	}
	
	function getLand() {

		return (string) $this->land;

	}
	
	function getSprache() {

		return (string) $this->sprache;

	}
	
	function getMobil() {

		return (string) $this->mobil;

	}
	
	function getLoginname() {

		return (string) $this->loginname;

	}
	
	function getLdap_name() {

		return (string) $this->ldap_name;

	}
	
	function getAnrede() {

		return (string) $this->anrede;

	}
	
	function getSms() {

		return (string) $this->sms;

	}
	
	function getRole() {

		return (int) $this->role;

	}
	
	function getProxy() {

		return (string) $this->proxy;

	}
	
	function getSettings() {

		return (string) $this->settings;

	}
	
	function getAcesso() {

		return (int) $this->acesso;

	}
	
	function getNotificacoes() {

		return (string) $this->notificacoes;

	}
	
	function getCorte_geografico() {

		return (string) $this->corte_geografico;

	}
	
}