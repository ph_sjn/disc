<?

class GruposVO {

	var $ID; //(int)
	var $firma; //(string)
	var $descricao; //(string)
	var $estado; //(string)

	
	///////////////////////////////////////
	//M�todos SET
	///////////////////////////////////////

	function setID($ID) {
		$this->ID = (int) $ID;
	}

	function setFirma($firma) {
		$this->firma = (string) $firma;
	}

	function setDescricao($descricao) {
		$this->descricao = (string) $descricao;
	}

	function setEstado($estado) {
		$this->estado = (string) $estado;
	}


	///////////////////////////////////////
	//M�todos GET
	///////////////////////////////////////

	function getID() {
		return (int) $this->ID;
	}

	function getFirma() {
		return (string) $this->firma;
	}

	function getDescricao() {
		return (string) $this->descricao;
	}

	function getEstado() {
		return (string) $this->estado;
	}

}
