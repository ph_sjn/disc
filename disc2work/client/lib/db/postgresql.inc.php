<?

// Conecta
  $dbIDnull = "pgnull";
  $link = pg_connect((($db_host == "" or $db_host == "localhost") ? "" : "host=$db_host ").(($db_pass == "") ? "" : "password=$db_pass ")."dbname=$db_name user=$db_user") or pg_errormessage();
  if(!$link) die("<b>Erro ao se conectar com o banco de dados!</b><br>Entre em contato com o Administrador.");

// Executa o query do SQL
function db_query($query) {
  global $link, $f_row;
  $dbIDnull = "";
  if    (preg_match("/INTO\s*(\w*)\s*/",$query,$matches)){$dbIDnull="nextval('" . $matches[1] . "_id_seq')"; }
  elseif(preg_match("/into\s*(\w*)\s*/",$query,$matches)){$dbIDnull="nextval('" . $matches[1] . "_id_seq')"; }
  $query = str_replace("pgnull",$dbIDnull,$query);
  $query = str_replace("''","NULL",$query); 
  if (substr(phpversion(),0,1) == "4" and substr(phpversion(),2,1) <= "2") { $tmp = pg_exec($link, $query); }
  else { $tmp = pg_query($link, $query); }
  if (!$tmp) {
    print "Sem resultados em: $query<br>";
  }
  $f_row[$tmp]=0;
  return($tmp);
}  

// Procura indica��o da coluna
function db_fetch_row($result) {
    return pg_fetch_row($result);
}

// Mensagens de erro
function db_die() {
  if (substr(phpversion(),0,1) == "4" and substr(phpversion(),2,1) < "2") { echo pg_errormessage($link); }
  else { echo pg_last_error($link); }  
  die("</body></html>");
}

?>
