<?

// Conecta
  $dbIDnull = "null";
  $link = OCILogon($db_user, $db_pass, $db_name);
  $datestmt = OCIParse($link, "alter session set NLS_DATE_FORMAT='YYYY-MM-DD HH:MI:SS'");
  OCIExecute($datestmt);
  if(!$link) die("<b>Erro ao se conectar com o banco de dados!</b><br>Entre em contato com o Administrador.");

// Executa o query do SQL
function db_query($query) {
  global $link;
  $stmt = OCIParse($link, $query);
  OCIExecute($stmt);
  if (eregi('insert|update|delete|create',$query)) {
    $commit_stmt = OCIParse($link, 'commit');
    OCIExecute($commit_stmt);
  }
  return $stmt;
}

// Procura indica��o da coluna
function db_fetch_row ($result) {
  OCIFetchInto($result, $row, OCI_RETURN_NULLS+OCI_RETURN_LOBS);
  return $row;
}

// Mensagens de erro
function db_die() {
  echo OCIError($stmt);
  die("</body></html>");
}

?>