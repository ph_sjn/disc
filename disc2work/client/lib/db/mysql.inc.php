<?

include_once($path_pre ."config.inc.php");

//Fun��o para retornar Mensagem formatada 
function retornaMensagemMySQL($mensagem) {
	GLOBAL $bgcolor4, $bgcolor5;
	$out = "<div align='center'></div>";
	$out .= "<table border='0' cellpadding='10' width='300' cellspacing='1' align='center' bgcolor='$bgcolor5'>";
	$out .= "<tr bgcolor='$bgcolor4'><td align='center' nowrap>";
	$out .= "<font color='red'><b>";
	$out .= $mensagem;
	$out .= "</b></font>";
	$out .= "</td></tr></table><br>";
	return $out;
}

// Conecta
$dbIDnull = "null";
$link = mysql_connect($db_host, $db_user, $db_pass) or mysql_error();
$conn = mysql_select_db($db_name);
if(!$link or (isset($conn) and !$conn)) {
	die(retornaMensagemMySQL("Erro de conex�o do banco de dados.<br>Entre em contato com o administrador, enviando a seguinte mensagem de erro: <br><br>". mysql_error() ));
}

// Executa o query do SQL
function db_query($query) {
  return mysql_query($query);
}

// Procura indica��o da coluna
function db_fetch_row($result) {
	if($result)
  		return mysql_fetch_row($result);
  	else 
  		return false;
}

// Procura indica��o da coluna
function db_fetch_array($result) {
	if($result)
		return mysql_fetch_array($result);
	else 
		return false;
}


// Error-Messages
function db_die() {
  echo mysql_error(); 
  die("</body></html>");
}

// Error-Messages
function db_num_rows($result) {
	if($result){
		return mysql_num_rows($result);
	} else return 0;
}

// Retorna mensagem de erro
function db_error($link) {
	return mysql_error($link);
}

// Retira da mem�ria e termina o processo de uma consulta SQL
function db_free_result($consulta) {
	return mysql_free_result($consulta);
}

// Retira a conex�o da mem�ria e termina a conex�o no banco de dados
function db_close($link) {
	return mysql_close($link);
}

?>