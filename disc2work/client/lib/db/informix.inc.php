<?

// Conecta
  $dbIDnull = "0";
  if ($db_host == "") $db = $db_name; else $db = $db_name."@".$db_host;
  $link = ifx_connect($db, $db_user, $db_pass);
  if(!$link) die("<b>Erro ao se conectar com o banco de dados!</b><br>Entre em contato com o Administrador.");

// Executa o query do SQL
function db_query($query) {
  $rid = ifx_prepare($query, $link);
  if (!ifx_do($rid)) {
    ifx_error();
  }
  return $rid;
}

// Procura indica��o da coluna
function db_fetch_row ($result) {
  if($row = ifx_fetch_row($result)) {
    $types = ifx_fieldtypes($result);
    $row = array_values($row);
  }
  return $row;
}

// Mensagens de erro
function db_die() {
  echo ifx_error();
  die("</body></html>");
}

?>