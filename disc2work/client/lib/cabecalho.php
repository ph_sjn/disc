<?
include_once "lib/lib.inc.php";

//flag inicializado na config.ini.php
if($user_kurz=='suporte' && $fl_debug == 'A'){
	ini_set("display_errors",'on');
	error_reporting(E_ALL);
}

// VERIFICA SE O USUARIO ESTA� ACESSANDO O SISTEMA POR UM DISPOSITIVO MOVEL */
$movel = false;
$movel_tipo = "";
if(preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipod|j2me|java|midp|mini|mmp|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|zte)/i', $_SERVER['HTTP_USER_AGENT'])) {

	// SE FOR UM ANDROID, VERIFICA SE � UM TABLET OU SMARTPHONE
	if(preg_match('/(android)/i', $_SERVER['HTTP_USER_AGENT'])) {
	
		// SE � UM SMARTPHONE ANDROID, CARREGA PAGINA COM TELA REDUZIDA
		if(preg_match('/(mobile)/i', $_SERVER['HTTP_USER_AGENT'])) {
			$movel = true;
			$movel_tipo = "mobile";
		} else {
			// MANTER PAGINA NORMAL PARA TABLETS ANDROID
		}
		
	}
	// SE NAO � UM ANDROID, CARREGA PAGINA DE TELA REDUZIDA PARA OS DEMAIS DISPOSITIVOS MOVEIS
	else { 
		$movel = true;
		$movel_tipo = "mobile";
	}
}

// // AMBIENTE PARA MOBILE TEMPORARIAMENTE DESATIVADO
// $movel = false;
// $movel_tipo = "pc";


$acesso_administrativo = checaPermissao("admin","A",$user_access);

if ($movel == true)  { 
	include_once "lib/inc/cabecalho_". $movel_tipo .".php";
} else {
	include_once "lib/inc/cabecalho_pc.php";
}
