<?
session_name("ExtraNet_disc2work_client");
session_start();

if ( !isset($perpage) ) {
	$tempo = time();
	$_SESSION['time'] = $tempo;
} else {
	$tempo = time() - $_SESSION['time'];
}
$PHPSESSID=session_id();

//setlocale(LC_ALL, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
//setlocale(LC_ALL, 'pt_BR');
setlocale (LC_ALL, 'pt_BR.iso-8859-1');

// Idiomas
$languages = array("br");
$perpage_values = array("10","20","30","50","100");

// Valores por p�gina
if ( !isset($perpage) ) {
	if ( !isset($start_perpage) ) {
		$perpage = "30"; 
		$start_perpage = $perpage;
	}
	else { 
		$perpage = $start_perpage;
	}
}

if ( !isset( $path_pre ) ) {
	$path_pre = (string) "";
}

include_once($path_pre ."config.inc.php");

include_once("lib/inc/mensagens_ajuda.php");

if ( !isset( $timezone ) ) {
	$timezone = (string) "";
}

// Vari�veis e constantes
$var_ini_set = ini_set('include_path','');
$include_path = $path_pre."lib/gpcs_vars.inc.php";
include_once $include_path;
$img_path = $path_pre."img";
$dbTSnull = date("YmdHis", mktime(date("H")+$timezone,date("i"),date("s"),date("m"),date("d"),date("Y")));

// Erros e seguran�a
if (!isset($error_reporting_level) or !$error_reporting_level) { error_reporting(0); }
else { error_reporting( E_ALL & ~E_NOTICE); }
$var_ini_set = ini_set("session.bug_compat_42", 1);
$var_ini_set = ini_set("session.bug_compat_warn", 0);

// Banco de dados
include_once("lib/db/$db_type.inc.php"); 

// Sa�da HTML
function html_out($outstr){
  if ($outstr <> "") return str_replace("'","&#39;",htmlspecialchars($outstr));
}

// Cores
function tr_tag($dblclick,$parent="") {
  global $cnr, $bgcolor1, $bgcolor2, $tr_hover;
  if (($cnr/2) == round($cnr/2)) { $color = "$bgcolor1"; $cnr++;}
  else { $color = "$bgcolor2"; $cnr++; }
  if ($tr_hover) { $tr_hover_on = "onmouseover=\"this.style.backgroundColor = '#ffffff'\" onmouseout=\"this.style.backgroundColor = '$color'\""; }
  else { $tr_hover_on = ""; }
  echo "<tr bgcolor=$color $tr_hover_on>\n";
}

//if ($login || $logout) {
	include_once "auth.inc.php";
//}

// Idiomas
include_once("lang/". $idioma .".inc.php");

// Skin padr�o
include($path_pre."layout/default/default.php");

// Determina o estilo
$css_style = def_style(); 

// Retorna o n�vel de um sub-elemento
function show_elements_of_tree($table,$name,$query,$acc,$order,$selected,$parent,$parent_ID) {
  global $indent, $user_kurz, $user_access, $subdirs, $user_ID;

  $sql = " SELECT  m.id, $acc, $name FROM ". $table ." m WHERE ";
  $sql .= " typ = 'd' AND $parent = '$parent_ID' ";
  $sql .= " ORDER BY m.datum DESC, m.typ, m.filename ASC";

  $result = db_query($sql) or db_die();  
  while ($row = db_fetch_row($result)) {

      echo "<option value='$row[0]'";
      if ($row[0] == $selected) { echo " selected"; }
      echo ">";
      for ($i = 1; $i <= $indent; $i++) { echo "&nbsp;&nbsp;"; }
      echo "$row[2]</option>\n";

    // Procura sub-elementos
    $indent++;
    show_elements_of_tree($table,$name,$query,$acc,$order,$selected,$parent,$row[0]);
    $indent--;
  }
}

// Formul�rio de sele��o simples para op��es de exporta��o
function show_export_form2($file,$data1,$data2) { 
  global $img_path, $print, $exp1, $keyword, $filter, $sort, $up, $month, $year, $anfang, $ende, $pdf_support, $PHPSESSID;
  $hidden = array('file'=>$file,'PHPSESSID'=>$PHPSESSID,'filter'=>$filter,'keyword'=>$keyword,'up'=>$up,'sort'=>$sort,'month'=>$month,'year'=>$year);
  echo "<form action='lib/export.php' method=post>\n";
  echo "<input type=hidden name=data1 value=$data1>\n";
  echo "<input type=hidden name=data2 value=$data2>\n";
  echo "$exp1: <select name=medium>\n";   
  echo "<option value=html>HTML\n";
  echo "<option value=xls>XLS\n";
  echo "<option value=rtf>RTF\n";
  echo "<option value=doc>DOC\n";
  echo "<option value=print>$print\n";
  echo "</select> <input type=submit value='Exportar' border=0></form>&nbsp;";
}

// Checa o sistema operacional e escolhe o arquivo css apropriado
function def_style() {
	global $css_style;
	return $path_pre."lib/css/". $css_style;
}

function str_to_upper($str){
   return strtr($str,
   "abcdefghijklmnopqrstuvwxyz".
   "\x9C\x9A\xE0\xE1\xE2\xE3".
   "\xE4\xE5\xE6\xE7\xE8\xE9".
   "\xEA\xEB\xEC\xED\xEE\xEF".
   "\xF0\xF1\xF2\xF3\xF4\xF5".
   "\xF6\xF8\xF9\xFA\xFB\xFC".
   "\xFD\xFE\xFF",
   "ABCDEFGHIJKLMNOPQRSTUVWXYZ".
   "\x8C\x8A\xC0\xC1\xC2\xC3\xC4".
   "\xC5\xC6\xC7\xC8\xC9\xCA\xCB".
   "\xCC\xCD\xCE\xCF\xD0\xD1\xD2".
   "\xD3\xD4\xD5\xD6\xD8\xD9\xDA".
   "\xDB\xDC\xDD\xDE\x9F");
}




function Token($pont,$token,$string) {
	$cont = 0;
	$arrcont = 0;
	while ( $cont <= strlen($string) ) {
	    $str = substr($string, $cont , 1);
		if ($str == $token) {
			$array[$arrcont] = substr($string,0 ,$cont);
			$string = substr($string,$cont + 1, strlen($string));
			$arrcont++;
			$cont = 0;
		}
		$cont = $cont + 1;
	}
	if ($pont <> "0") { return $array[$pont - 1]; }
	else { return $array; }
}


function dataMesAnoPortuguesAbreviado($data) {
	GLOBAL $mes_array;
	$data = explode("/", $data);
	return $data[0] ."/". $mes_array[1 * $data[1]] . "/". substr($data[2],2,4);
}



//Fun��o para retornar mensagem formatada 
//SEGUNDO PARAMETRO RECEBE VAZIO POR DEFAULT EM CASO DE N�O RECEBER
function retornaMensagem($mensagem, $tipo=""){
	GLOBAL $bgcolor4, $bgcolor5;
	// 1 = ERRO e 0 = SEM ERRO
	if ($tipo == 1) $cor_fonte = "#FF0000";
	else $cor_fonte = "#0000FF";
	$out = "<table border='0' cellpadding='10' cellspacing='1' style=\"text-align:center; background: ". $bgcolor5 ."; width: 300px; margin: 0 auto;\">";
	$out .= "<tr bgcolor='$bgcolor4'><td style=\"text-align:center; color: ". $cor_fonte ."; font-weight: bold; \" nowrap>";
	$out .= $mensagem;
	$out .= "</td></tr></table><br>";
	return $out;
}

function verificaCor($module,$current_module) {
  if ($current_module == $module) {
	return "class='selecionado'";
  }
  else {
    return "class='naoselecionado'";
  }
}


function conta_lidas($antwort) {
 global $user_ID;
  $sql = " SELECT ID, (SELECT count(id_usuario) FROM forum2_lidas where id_usuario = $user_ID and id_mensagem = ID ) ";
  $sql .= " FROM forum2 WHERE antwort = '$antwort'";
  $result = db_query($sql) or db_die();
  $contador = 0;
  while ($row = db_fetch_row($result)) {
	if ($row[1] == "0")
	  	$contador++;
  }
  return $contador;
}

function conta_lidas2($antwort, $contador) {
 global $user_ID;
  $sql = " SELECT ID, (SELECT count(id_usuario) FROM forum2_lidas where id_usuario = $user_ID and id_mensagem = ID ) ";
  $sql .= " FROM forum2 WHERE antwort = '$antwort' and flag_autorizado = '1'";
  $result = db_query($sql) or db_die();
  while ($row = db_fetch_row($result)) {
	  if ($row[1] == 0) {
	  	$contador++;
	  }
      $contador = conta_lidas($row[0], $contador);
  }
  return $contador;
}

function encrypt($password, $saltstring) {
  $salt = substr($saltstring, 0, 2);
  $enc_pw = crypt($password, $salt);
  return $enc_pw;
}

include_once("checapermissao.php");


//Usada no Modulo Calendario
function mes_portugues($a){
	switch($a) {
		case 1: case 01: $res="Janeiro";
		break;
		case 2: case 02: $res="Fevereiro";
		break;
		case 3: case 03: $res="Mar�o";
		break;
		case 4: case 04: $res="Abril";
		break;
		case 5: 
		case 05: $res="Maio"; 
		break;
		case 6: case 06: $res="Junho"; 
		break;
		case 7: case 07: $res="Julho";  
		break;
		case 8: case 08: $res="Agosto"; 
		break;
		case 9: case 09: $res="Setembro";    
		break;
		case 10: $res="Outubro";    
		break;
		case 11: $res="Novembro";    
		break;
		case 12: $res="Dezembro";    
		break;    
	}    
	
	$res=strtoupper($res);
	return $res;
}


// Bloqueia o acesso as p�ginas do tipo "sem acesso" para o usu�rio
function checaAcessoRestrito($module, $user_access, $inc_modulo){	
	$sql_sistema = "SELECT identificador FROM modulos WHERE tipo = 'sistema'";
	$result_sistema = db_query($sql_sistema);

	$sql_restrito = "SELECT m.id, p.id_perfil_acesso, p.nivel from perfil_acesso_padrao p, modulos m WHERE m.situacao = 'A' AND p.id_perfil_acesso = $user_access and m.identificador = ";
	$i=0;
	while ($row = db_fetch_row($result_sistema)) {
		$row1[$i]=$row[0];
		$i++;
	}
	if(in_array($module, $row1)){
			$sql_restrito .= "'admin' and p.id_modulo = m.id";
		}else{
			$sql_restrito .= "'".$module."' and p.id_modulo = m.id";	
		}
	$result_restrito = db_query($sql_restrito);

	if ((db_num_rows($result_restrito) > 0) || ($inc_modulo == "login")) {
		return true;
	}else{
		return false;
	}
}




	
function subnivelTipoSolicitacao($id, $espaco, $id_selecionado) {
 	$result_tipos = db_query("select id, nome from tb_tipos_solicitacoes where nivel_pai = $id order by id") or db_die(mysql_error());
	$espaco .= " |����";
 	while ($row_tipos = db_fetch_row($result_tipos) ) {
	    if ($id_selecionado == $row_tipos[0])
			$selected = "selected";
		else
			$selected = "";
		echo "<option value='". $row_tipos[0] ."' $selected>$espaco ".  $row_tipos[1] . "</option>";
		subnivelTipoSolicitacao($row_tipos[0],$espaco,$id_selecionado);
	}
	$espaco = substr($espaco,strlen($espaco)-2);
}

// Gera senha com caracteres aleat�rios
function scramble() {
  srand((double)microtime()*1000000);
  $char = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMANOPQRSTUVWXYZ";
  while (strlen($str) < 12) { $str .= substr($char,(rand()%(strlen($char))),1); }
  return $str;
}

function ChecaListaUsuarios($module,$acesso){
	$ids_perfis_liberados="";
	$nm_users_liberados = "";
	
	//MODULO ELEARNING N�O SEGUE PADR�O
	if ($module == 'elearning_curso') $module = 'elearning';
	
	$sql="SELECT id FROM modulos WHERE identificador='".$module."'";
	$result=db_query($sql);
	$row=db_fetch_row($result);
	$id_modulo_corrente=$row[0];
	
	//CHECA PERFIS QUE POSSUEM ACESSO AO M�DULO
	$sql="SELECT id_perfil_acesso FROM perfil_acesso_padrao WHERE id_modulo=".$id_modulo_corrente;
	$result=db_query($sql);
	while($row=db_fetch_row($result)){
		if(!empty($ids_perfis_liberados)) $ids_perfis_liberados .= ", ";
		$ids_perfis_liberados .= $row[0];
	}

	//CHECA USU�RIOS QUE EST�O ATIVOS E EST�O RELACIONADOS AO PERFIS QUE TEM PERMISS�O AO M�DULO
	$sql_grupos="SELECT id, kurz FROM users WHERE acc IN(".$ids_perfis_liberados.") AND ativo='S' ORDER BY nachname ASC";
	$result_grupo=db_query($sql_grupos);
	while($row_grupo=db_fetch_row($result_grupo)){
		if(!empty($nm_users_liberados)) $nm_users_liberados .= "', '";
		$nm_users_liberados .= $row_grupo[1];
		$ids_users_liberados[]= $row_grupo[0];
	}
	if ($acesso == 'G'){
		return $nm_users_liberados;
	}else if ($acesso == 'U'  || $acesso == 'group'){
		return $ids_perfis_liberados;
	}else if ($acesso == 'todos'){
		return $ids_users_liberados;
	}
}


//LISTA OS GRUPOS E USU�RIOS DA FORMS
function ChecaExibiUsuarios($module,$acesso, $ID){
	
	if ($acesso == 'G'){
	
		$nm_users_liberados = ChecaListaUsuarios($module ,$acesso);
		
		$sql = "SELECT distinct f.ID, f.firma FROM firm AS f, user_firm AS u ";
		$sql .= "WHERE f.ID = u.id_firm AND u.id_user IN('".$nm_users_liberados."') ORDER BY f.firma ASC";
		$ret_firma = db_query($sql);
		while($result_firma = db_fetch_row($ret_firma)){
			$id_firma[]=$result_firma[0];
			$nm_firma[]=$result_firma[1];	
		}
		if (!$ID){
			for ($y =0; $y < count($id_firma); $y++) {?>
				<option value = "<?=$id_firma[$y]?>" ><?=$nm_firma[$y]?></option>
			<?}
		}else{
			$sql_selecao = "SELECT id_users FROM ".$module."_item_acesso ";
			$sql_selecao.= "WHERE id_file=".$ID." AND tipo_acesso = 'G'";
			$result_selecao=db_query($sql_selecao);
			while($row_selecao_firma = db_fetch_row($result_selecao)){
				$selecao_firma[]=$row_selecao_firma[0];
			}
			
			for ($y =0; $y < count($id_firma); $y++) {?>
				<option value = "<?=$id_firma[$y]?>" <?if(in_array($id_firma[$y], $selecao_firma)){echo 'selected';}?> ><?=$nm_firma[$y]?></option>
			<?}	
		}

	}else if ($acesso == 'U'){
		$ids_perfis_liberados = ChecaListaUsuarios($module ,$acesso);
		
		$sql_users = "SELECT id, nachname FROM users WHERE acc IN(".$ids_perfis_liberados.") AND ativo='S' ORDER BY nachname ASC";
		$ret_users = db_query($sql_users);
		while($result_users = db_fetch_array($ret_users)){ 
			$id_user[]=$result_users[0];
			$nm_user[]=$result_users[1];	
		}
		if (!$ID){
			for ($y =0; $y < count($id_user); $y++) {?>
				<option value = "<?=$id_user[$y]?>" ><?=$nm_user[$y]?></option>
			<?}
		}else{
			$sql_user= "SELECT id_users FROM ".$module."_item_acesso ";
			$sql_user.= "WHERE id_file=".$ID." AND tipo_acesso = 'U' ";
			$result_user=db_query($sql_user);
			while($row_selecao_user = db_fetch_row($result_user)){
				$selecao_user[]=$row_selecao_user[0];
			}
			
			for ($y =0; $y < count($id_user); $y++) {?>
				<option value = "<?=$id_user[$y]?>" <?if(in_array($id_user[$y], $selecao_user)){echo 'selected';}?> ><?=$nm_user[$y]?></option>
			<?}
		}	
	}
}

//INSERE NA TABELA DE ACESSO OS USU�RIO QUE TER�O PERMISS�O DE ACESSO AO M�DULO
function ChecaInsereUsuarios($module, $ID, $firmas, $acc){

	if ($acc == "todos"){
	
		$ids_users_liberados = ChecaListaUsuarios($module ,$acc);
		
		$sql_todos = "INSERT INTO ". $module ."_item_acesso (id_file, id_users, mensagem_pendente, tipo_acesso) VALUES ";
		for ($y =0; $y < count($ids_users_liberados); $y++) {
			if ($y > 0) $sql_todos.= ", ";
			$sql_todos .= "(".$ID.", ".$ids_users_liberados[$y].", 'N', 'U')";
		}
		$result = db_query($sql_todos) or db_die();

	}else if ($acc == "group"){
		$ids_perfis_liberados = ChecaListaUsuarios($module ,$acc);
		
		$sql_user_grupos = "SELECT DISTINCT u.id FROM user_firm AS f, users AS u ";
		$sql_user_grupos .= "WHERE u.kurz = f.id_user AND u.ativo='S' AND id_firm IN (".$firmas.") AND acc IN(".$ids_perfis_liberados.")";
		$result_user_grupos = db_query($sql_user_grupos);
		while($row_user_grupos=db_fetch_row($result_user_grupos)){
			$ids_users_grupos_liberados[]=$row_user_grupos[0];
		}
		
		$sql_grupo = "insert into ". $module ."_item_acesso (id_file, id_users, mensagem_pendente, tipo_acesso) VALUES ";
		for ($i =0; $i < count($ids_users_grupos_liberados); $i++) {
			if ($i > 0) $sql_grupo.= ", ";
			$sql_grupo .=  "(".$ID.", ".$ids_users_grupos_liberados[$i].", 'N', 'U')";
		}
		$result = db_query($sql_grupo) or db_die();
	}
}
	
?>

