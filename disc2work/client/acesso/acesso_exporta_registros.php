<?
$path_pre="../";

//INCLUDES NECESS�RIOS
session_name("ExtraNet_disc2work_client");
session_start();
ini_set("display_errors", "on");

//DEFININDO CONSTANTES DE COR
define("COR_CABECALHO", "004A8F");
define("COR_FONTE_CABECALHO", "FFFFFF");

$dir_absoluto= $dir_base ."../../extranet/";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
require_once($dir_absoluto."excel_xlsx/PHPExcel.php");
require_once($dir_absoluto."excel_xlsx/PHPExcel/Writer/Excel2007.php");
require_once($dir_absoluto."excel_xlsx/PHPExcel/IOFactory.php");

//DEFININDO A LINHA INICIAL DA PLANILHA
$linha=(int)1;
$cor1=(string)"CD0000";

function makeCabecalho($cabecalho, $colunas, $linha){
	global $planilha;
	foreach($cabecalho as $keys => $valor){
		$planilha->setCellValue($colunas[$keys].$linha,utf8_encode($valor));
		$planilha->getStyle($colunas[$keys].$linha)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB(COR_CABECALHO);
		$planilha->getStyle($colunas[$keys].$linha)->getFont()->getColor()->setARGB(COR_FONTE_CABECALHO);
		$planilha->getStyle($colunas[$keys].$linha)->getFont()->setBold(true);
		$planilha->getStyle($colunas[$keys].$linha)->getAlignment()->setHorizontal('left');
	}
	return ++$linha;
}

function makeInfo($cabecalho, $colunas, $row, $linha){
	global $planilha;

	//CONSIDERA��ES:
	//Os elementos do cabe�alho, das colunas e da row tem que estar dispostos na ordem correta, como devem aparecer

	foreach($cabecalho as $key => $valor){

		if($row[$key]=="") $row[$key]=" ----- ";
			
		//CONDI��ES ESPEC�FICAS DA FUN��O, QUE PODEM SER PERSONALIZADAS, SEGUINDO ESTE PADR�O
			
		//FIM

		$planilha->setCellValue($colunas[$key].$linha, utf8_encode($row[$key]));
	}
	return ++$linha;
}

function makeFooter($colunas, $linha, $info, $align){
	global $planilha;
	if(!$align){
		$align="right";
	}
	$linha++;
	$string_merge = $colunas[0]. "$linha:" . $colunas[count($colunas)-1]."$linha";
	$planilha->mergeCells($string_merge);
	$planilha->getStyle($colunas[0].$linha)->getFont()->setBold(true);
	$planilha->getStyle($colunas[0].$linha)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB(COR_CABECALHO);
	$planilha->getStyle($colunas[0].$linha)->getFont()->getColor()->setARGB(COR_FONTE_CABECALHO);
	$planilha->getStyle($colunas[0].$linha)->getAlignment()->setHorizontal($align);
	if(!empty($info)){
		$planilha->setCellValue($colunas[0].$linha, utf8_encode($info));
	}
	return ++$linha;
}

$name ="Relat�rio de Usu�rios Cadastrados|Exclu�dos|Ativados|Desativados Extra|Net - ".date("d_m_Y_-_H:i:s");

//CRIANDO O ARQUIVO EXCEL
$excel = new PHPExcel();

//SETANDO O NOME DO ARQUIVO
$excel->getProperties()->setTitle(utf8_encode($name));

//ESCOLHENDO A PASTA DE TRABALHO
$excel->setActiveSheetIndex(0);

//Para facilitar o trabalho atribuimostudo a essa vari�vel
$planilha = $excel->getActiveSheet();

// OBTENDO E ESCREVENDO DADOS DA PLANILHA - USUARIOS CADASTRADOS
	
	//cabe�alho dos pedidos
	$cabecalho=array("Data do registro", "Id usu�rio Adm", "Nome usu�rio Adm", "Id usu�rio registrado", "Nome usu�rio registrado", "A��o");
	$colunas = range("A", "F");
	//fim

	// //faz um rodap� personalizado, de acordo com o numero de colunas.
	$linha=makeFooter($colunas, $linha, "Registro de usu�rios", "center");
	$linha++;

	$sql = "SELECT DATE_FORMAT(data, '%d/%m/%Y %H:%i:%s'), id_user_adm, nm_user_adm, id_user, nm_user, ";
	$sql .= " CASE nm_acao ";
	$sql .= " WHEN 'S' THEN 'Ativo' ";
	$sql .= " WHEN 'N' THEN 'Desativado' ";
	$sql .= " WHEN 'E' THEN 'Exclu�do' ";
	$sql .= " WHEN 'C' THEN 'Cadastrado' "; 
	$sql .= " END ";
	$sql .= " FROM tb_registro_user ";
	$sql .= " WHERE DATE_FORMAT(data,'%Y%m%d') >= '". $aux_data1 ."' AND DATE_FORMAT(data,'%Y%m%d') <= '". $aux_data2 ."' ";
	if ($filtro_acao && $filtro_acao <> ''){
		$sql .= " AND nm_acao = '".$filtro_acao."' ";
	}
	$sql .= " ORDER BY data";

	$usuarios = db_query($sql);
	if(db_num_rows($usuarios)>0){
	
		$linha=makeCabecalho($cabecalho, $colunas, $linha);
		while($result=db_fetch_row($usuarios)){
			$linha=makeInfo($cabecalho, $colunas, $result, $linha);
		}
		foreach($colunas as $key => $value){
			$planilha->getStyle($value)->getAlignment()->setWrapText(true);
			$planilha->getColumnDimension($value)->setAutoSize(true);
			$planilha->getStyle($value)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		}
	} else {
		$string_merge = $letras[0]. "$linha:" . $letras[count($letras)-1]."$linha";
		$planilha->mergeCells($string_merge);
		$planilha->getStyle($letras[0].$linha)->getAlignment()->setHorizontal("center");
		$planilha->setCellValue($letras[0].$linha, utf8_encode("Nenhuma informa��o encontrada."));
	}
//FIM

try{

	//INSERINDO O ARQUIVO GERADO NO ARQUIVO EXCEL
	$objWriter = PHPExcel_IOFactory::createWriter ($excel,'Excel2007');
	
	//SALVANDO O ARQUIVO NO DIRET�RIO SELECIONADO
	$objWriter->save("/tmp/". $name .".xlsx");
	
	// CARREGAR ARQUIVO PARA DOWNLOAD NO CLIENTE
	header("Content-type: application/force-download");
	header("Content-disposition: attachment; filename=\"". $name .".xlsx\";");
	readfile("/tmp/".$name .".xlsx");

	//APAGANDO O ARQUIVO DO DIRET�RIO SELECIONADO, PARA N�O GERAR LIXO
	unlink("/tmp/". $name .".xlsx");

}catch (exception $e){
	//echo $e;
}?>