<?
session_name("ExtraNet_disc2work_client");
session_start();
$path_pre = "../";
include_once($path_pre ."config.inc.php"); 
include_once($path_pre ."lang/br.inc.php"); 
include_once($path_pre ."lib/db/mysql.inc.php"); 
include_once($path_pre ."lib/checapermissao.php");
require_once($path_pre ."../extranet/excel_xlsx/PHPExcel.php");
require_once($path_pre ."../extranet/excel_xlsx/PHPExcel/Writer/Excel2007.php");
require_once($path_pre ."../extranet/excel_xlsx/PHPExcel/IOFactory.php");


// DEFININDO O NOME DO ARQUIVO
$nome_arquivo_xls = "Acessos.xlsx";

// CRIANDO O ARQUIVO EXCEL
$excel = new PHPExcel();

// SETANDO O NOME DO ARQUIVO
$excel->getProperties()->setTitle("Acessos");
	
// ESCOLHENDO A PASTA DE TRABALHO
$excel->setActiveSheetIndex(0);

// SETANDO NA VARI�VEL A ABA ATIVA DA PLANILHA
$planilha = $excel->getActiveSheet();

// SETANDO TAMANHO DAS COLUNAS
$planilha->getColumnDimension('A')->setWidth('40');
$planilha->getColumnDimension('B')->setWidth('20');
$planilha->getColumnDimension('C')->setWidth('20');
$planilha->getColumnDimension('D')->setWidth('60');

// DEFININDO CABE�ALHO DA PLANILHA
$planilha->setCellValue("A1", utf8_encode( "USUARIO" ) );
$planilha->setCellValue("B1", utf8_encode( "ENTRADA" ) );
$planilha->setCellValue("C1", utf8_encode( "SA�DA" ) );
$planilha->setCellValue("D1", utf8_encode( "DOWNLOADS" ) );

// CONSULTAR DADOS DE ACESSO CONFORME PAR�METROS DE DATA INICIAL E DATA FINAL
$sql = " SELECT id, usuario, DATE_FORMAT(entrada,'%d/%m/%Y - %H:%i'), DATE_FORMAT(saida,'%d/%m/%Y - %H:%i'),downloads ";
$sql .= " FROM acesso WHERE ";
$sql .= " DATE_FORMAT(entrada,'%Y%m%d') >= '". $aux_data1 ."' and DATE_FORMAT(entrada,'%Y%m%d') <= '". $aux_data2 ."' ";
$sql .= " ORDER BY entrada desc";

// INICIALIZA O PONTEIRO DE LINHAS DA PLANILHA
$linha = 2;

// EXECUTA A QUERY
$consulta = db_query($sql) or db_die();
while ($row = db_fetch_row($consulta) ) {

	// INSERINDO AS C�LULAS COM OS DADOS CONSULTADOS DO BANCO DE DADOS
	$planilha->setCellValue("A".$linha, utf8_encode( $row[1] ) );
	$planilha->setCellValue("B".$linha, utf8_encode( $row[2] ) );
	$planilha->setCellValue("D".$linha, utf8_encode( $row[4] ) );
	
	// SE O CAMPO "SA�DA" ESTIVER NULO/VAZIO, SIGNIFICA QUE O USU�RIO AINDA EST� CONECTADO. NESTE CASO, INSERIR "CONECTADO" NA C�LULA
	if ( trim($row[3]) == "") 
		$planilha->setCellValue("C".$linha, utf8_encode( "Conectado" ) );
	else
		$planilha->setCellValue("C".$linha, utf8_encode( $row[3] ) );
	
	$linha++;
}

//INSERINDO NO WRITER A PLANILHA GERADA
$objWriter = PHPExcel_IOFactory::createWriter ($excel,'Excel2007');

try{
	
	//SALVANDO O ARQUIVO NO TEMP
	$objWriter->save("/tmp/". $nome_arquivo_xls);

	// GERANDO CABE�ALHO DE RETORNO
	header("Content-type: application/force-download");
	header("Content-disposition: attachment; filename=\"$nome_arquivo_xls\";");
	
	// OBTER CONTE�DO DO ARQUIVO PARA OUTPUT NO DOWNLOAD
	readfile("/tmp/". $nome_arquivo_xls);

	//APAGANDO O ARQUIVO DO DIRET�RIO SELECIONADO, PARA N�O GERAR LIXO
	unlink("/tmp/". $nome_arquivo_xls);

}catch (exception $e){
	// TRATAR ERROS
}

?>
