<?

$acesso_administrativo = checaPermissao($module, "A", $user_access);

// Biblioteca de classes e fun��es do DefWebExpress
//include_once("discx/libdisc.php");

$DW_id_avaliacao = $_REQUEST['id'];
$REQ_idc = $_REQUEST["idc"];

// Se veio o id da avali��o...
if ($DW_id_avaliacao != "") {
    //
    $objPesquisa_temp = DWX_obterPesquisa(1, $DW_id_avaliacao);
    $objPesquisa = json_decode($objPesquisa_temp, false);
//    //
//    if(strtolower($_SESSION['user_kurz']) == "rp-marcio") {
//        echo("<hr>");
//        echo("<pre>");
//        print_r($objPesquisa);
//        echo("</pre>");
//        echo("<hr>");
//        
//    }
}
?>

<form method=post enctype='multipart/form-data' action='index.php' name='frm'>
    <input type=hidden name='module' value='<?= $_REQUEST['module'] ?>'>
    <input type=hidden name='mode' value='modelo_data'>
    <input type=hidden name='idc' value="<?= $REQ_idc ?>">

    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">MODELO DE E-MAIL PARA OS USU�RIOS</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">
                <br/>
                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Assunto do e-mail:</td>
                        <td style="font-weight: bold;"><input type="text" style="width:450px;" name="ds_email_assunto" value="<?php echo($objPesquisa->EmailModeloAssunto);  ?>" class="texto" size="50" maxlength="60"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Corpo do e-mail:</td>
                        <td style="font-weight: bold;">
                            <textarea 
                                class="texto" rows=12 cols=45 maxlength="1024" name="ds_email_txt_link"
                                style="width:450px; height:250px"
                            ><?php echo($objPesquisa->EmailModeloCorpo); ?></textarea>
                        </td>
                    </tr>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">
                            Dicas:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td style="vertical-align: middle; text-align: right;">
                                        {NOME}
                                    </td>
                                    <td>
                                        Substitui pelo nome do entrevistado no e-mail.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; text-align: right;">
                                        {UNIDADE}
                                    </td>
                                    <td>
                                        Substitui pela unidade do entrevistado no e-mail.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; text-align: right;">
                                        {LINK}
                                    </td>
                                    <td>
                                        Substitui pelo link para a pesquisa no e-mail.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; text-align: right;">
                                        (*)
                                    </td>
                                    <td>
                                        Voc� pode usar marca��es HTML para incrementar o texto.
                                    </td>
                                </tr>
                            </table>
                            <br/>
                        </td>
                    </tr>

                </table>
                <input type=hidden name='id' value="<?php echo($DW_id_avaliacao); ?>">

            </td>
        </tr>
        <?php
        if($objPesquisa->EnviarEmailConclusao == 1) {
        ?>
        <tr align='center' bgcolor="<?= $bgcolor6 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">MODELO DE E-MAIL PARA CONCLUS�O</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">
                <br/>
                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Assunto do e-mail:</td>
                        <td style="font-weight: bold;"><input type="text" style="width:450px;" name="ds_email_subj_conclusao" value="<?php echo($objPesquisa->EmailConclusaoAssunto);  ?>" class="texto" size="50" maxlength="60"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Corpo do e-mail:</td>
                        <td style="font-weight: bold;">
                            <textarea 
                                class="texto" rows=12 cols=45 maxlength="1024" name="ds_email_body_conclusao"
                                style="width:450px; height:250px"
                            ><?php echo($objPesquisa->EmailConclusaoCorpo); ?></textarea>
                        </td>
                    </tr>
                </table>
                <br/>
            </td>
        </tr>
        <?php
        } else {
        ?>
        <tr>
            <td>
                <input type="hidden" name="ds_email_subj_conclusao" value="<?php echo($objPesquisa->EmailConclusaoAssunto);  ?>" >
                <input type="hidden" name="ds_email_body_conclusao" value="<?php echo($objPesquisa->EmailConclusaoCorpo);  ?>" >
            </td>
        </tr>
        <?php
        }
        ?>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td>
                            <input type="button" onclick="document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&idc=<?= $REQ_idc ?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <input type="submit" name="salvar" value="Salvar"  class="botao">
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>
