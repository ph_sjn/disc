<?php

if ($salvar) {

    if (!$msg_erro) {
        
        $email_assunto = $_POST['ds_email_assunto'];
        $email_texto = $_POST['ds_email_txt_link'];
        $email_assuntoconcl = $_POST['ds_email_subj_conclusao'];
        $email_textoconcl = $_POST['ds_email_body_conclusao'];
        //
        $objRetorno_temp = DWX_gravarModeloEmail(1, $id, $email_assunto, $email_texto, $email_assuntoconcl, $email_textoconcl);
        $objRetorno = json_decode($objRetorno_temp, false);
        //
        if($objRetorno->ERRO == 0) {
            echo retornaMensagem("Modelo do e-mail atualizado com sucesso.");
        } else {
//            echo("<pre>");
//            print_r($objRetorno);
//            echo("</pre>");
//            echo("<hr>");
            echo retornaMensagem("Ocorreu um erro ao atualizar os dados do modelo do e-mail. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        }
    }

    include_once("disc_projetos/disc_projetos_modelo.php");
}
