<?
session_name("ExtraNet_discmaster");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
include_once("../lang/br.inc.php"); 
include_once($path_pre. "lib/checapermissao.php");

$acesso_administrativo = checaPermissao($module,"A",$user_access);
$acesso_normal = checaPermissao($module,"N",$user_access);
$existe = false;

// DefwebExpress: Configurações e biblioteca de classes e funções LOCAIS
include_once("../disc_config.php");
//require("/home/users/__discx/disc-lib/libDiscXLocal.php");
require($CONFIGENET_DISCX . "disc-lib/libDiscXLocal.php");

$DW_id_avaliacao = $_REQUEST["id"];
$DW_id_franqueador = $_REQUEST["idfq"];

$objRespostasFranqueador_temp = DWX_obterRespostasFranqueador(1, $DW_id_avaliacao, $DW_id_franqueador);
$objRespostasFranqueador = json_decode($objRespostasFranqueador_temp, false);

//echo("<pre>");
//print_r($objRespostasFranqueador);
//echo("</pre>");

$sQuebra_CodigoTema = "";
$sQuebra_CodigoAgrupamento = "";

echo("<center>");
echo("<table width=80%>");
echo("<tr>");
echo("<td>");

$lngPos = 0;

foreach ($objRespostasFranqueador As $objItem) {
    
    if(($objItem->CodigoTema != $sQuebra_CodigoTema)||($objItem->CodigoAgrupamento != $sQuebra_CodigoAgrupamento)) {
        $sQuebra_CodigoTema = $objItem->CodigoTema;
        $sQuebra_CodigoAgrupamento = $objItem->CodigoAgrupamento;
        //
        $sTitulo = $objItem->DescricaoAgrupamento . "-" . $objItem->DescricaoTema;
        //
        echo("<br><b>".$sTitulo."</b><br>");
    }
    $lngPos++;
    //
    $sNumero = str_pad($lngPos, 3, "0", STR_PAD_LEFT);
    //
    $sLinkResponder = " onclick=\"window.open('http://aws.franquiaextranet.com.br/discmaster/discx/dwsurvey_redir.php?id_avaliacao=" . $DW_id_avaliacao . "&id_token=" . $DW_id_franqueador . "&id_item_formulario=" . $objItem->IdItemFormulario . "')\" ";
    //
    ?>
    <?php
    //
    $sItem = $objItem->DescricaoItem;
    //
    echo("<br />" . $sNumero . "-" . $sItem);
    //
    if($objItem->QuantidadeRespostas > 0 ) {
        //
        echo("<br><br>");
        echo("<table cellpadding=5px cellspacing=0>");
        echo("<tr>");
        echo("<td bgcolor='#EEEEEE' style='min-width:50px; text-align:left; border:solid 1px gray;'>");
?>
<table cellpadding="5px" cellspacing="0">
<?php
        foreach ($objItem->colRespostas As $objValor) {
?>
    <tr>
        <td><?= $objValor->DescricaoOpcaoItem ?></td>
        <td>[&nbsp;<?= $objValor->Resposta ?>&nbsp;]</td>
    </tr>
<?php
        }
?>
</table>
<?php
        echo("</td>");
        echo("</tr>");
        echo("</table>");
        ?>
        <br/>
        <input type='button' 
            name="cmdResponder" value='Revisar a resposta' 
            title="Revisar a resposta desta pergunta" 
            class="botao"
            <?= $sLinkResponder ?> onmouseover="this.style.cursor='pointer';"
        /><br />
        <?php
    } else {
        ?>
        <br/><br/>
        <input type='button' 
            name="cmdResponder" value='Responder a pergunta' 
            title="Responder esta pergunta" 
            class="botao"
            style="background-color: red;"
            <?= $sLinkResponder ?> onmouseover="this.style.cursor='pointer';"
        /><br />
        <?php
    }
}
echo("<br /><br />");
echo("</td>");
echo("</tr>");
echo("</table>");
echo("</center>");
