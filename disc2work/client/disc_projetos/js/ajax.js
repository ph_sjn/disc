function Ajax() {
  try {
    return new ActiveXObject("Microsoft.XMLHTTP");
  } catch(e) {
    try {
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch(ex) {
      try {
        return new XMLHttpRequest();
      } catch(exc) {
        return false;
      }
    }
  }
}

function montarDashboard(modulo,id,div,img,tr, mode, sub_pasta, chave) {
	browser = "NAVEGADOR" + navigator.userAgent;
	ajax2 = Ajax();
	var element = document.getElementById(tr);
	var ico = document.getElementById(img);
	
	if (element.style.display == 'none') {
	
                if (browser.indexOf("MSIE") > 0) element.style.display = "block"; 
                else element.style.display = 'table-row';
                ico.setAttribute('src','img/open.gif');
                var sLoading = "<div style=\"text-align: center; margin: 10px;\"><img src=\"disc_config/img/dwloading.gif\"></div>";
                document.getElementById(div).innerHTML = sLoading;
                
                var sIDC = document.getElementsByName("idc")[0].value;

                pagina = "disc_projetos/ajax_dashboard.php?module="+modulo+"&mode="+ mode +"&id="+id+"&id_pasta="+sub_pasta+"&idx="+chave+"&idc="+sIDC;
		//alert (pagina);
		ajax2.open("GET", pagina, true);
		ajax2.onreadystatechange = function() {
			if(ajax2.readyState == 4){
				if(ajax2.status == 200){
					document.getElementById(div).innerHTML = ajax2.responseText;
				}
			}
		 }
		 ajax2.send(null);
	 }else {
	 	//document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
     	ico.setAttribute('src','img/close.gif');
    }
}

function exibirRespostas(modulo, id, idfq, div, img, tr, mode) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);
    
    if (element.style.display == 'none') {

        if (browser.indexOf("MSIE") > 0)
            element.style.display = "block";
        else
            element.style.display = 'table-row';
        ico.setAttribute('src', 'img/open.gif');
        var sLoading = "<div style=\"text-align: center; margin: 10px;\"><img src=\"disc_config/img/dwloading.gif\"></div>";
        document.getElementById(div).innerHTML = sLoading;

        pagina = "disc_projetos/ajax_franqueador.php?module=" + modulo + "&mode=" + mode + "&id=" + id + "&idfq=" + idfq;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {
                    document.getElementById(div).innerHTML = ajax2.responseText;
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}

function confirmarReinicioDePesquisas(id, tokenid) {
    if(confirm('Tem certeza que deseja descartar todas as suas respostas?')) {
        document.location.href="index.php?module=disc_projetos&mode=&acao=reset&id="+id+"&id_token="+tokenid;
    }
}
