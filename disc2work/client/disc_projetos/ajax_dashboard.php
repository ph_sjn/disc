<?
// versao: ajax_dashboard_v2.php
session_name("ExtraNet_discmaster");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
include_once("../lang/br.inc.php"); 
include_once($path_pre. "lib/checapermissao.php");
//
$acesso_administrativo = checaPermissao($module,"A",$user_access);
$acesso_normal = checaPermissao($module,"N",$user_access);
$existe = false;
//
// DefwebExpress: Configura��es e biblioteca de classes e fun��es LOCAIS
//require("/home/users/__discx/inc/libDiscX.php");
require($CONFIGENET_DISCX . "inc/libDiscX.php");
//
$DW_id_avaliacao = $_REQUEST["id"];
$DW_idx = $_REQUEST["idx"];
$DW_idc = $_REQUEST["idc"];
//
$objDWC = new clsDWCmaster();
//
$colResumo = $objDWC->obterResumoPesquisa("", $DW_id_avaliacao, "");
//
?>

<table cellspacing=0 cellpadding=6 border=0 width=80% align='center'>
    <?php
    //
    $qtde_concluidos = 0;
    $qtde_naoiniciaram = 0;
    $qtde_respondendo = 0;
    //
    $qtde_total = 0;
    //
	$DW_dt_inicio_avaliacao = "";
	$DW_dt_fim_avaliacao    = "";
	//
    //
	foreach ($colResumo As $objItem){
		if ( $objItem->Situacao == "Concluiram" ){
			$qtde_concluidos = $objItem->QtdeEntrevistados;
		}
		if ( $objItem->Situacao == "Respondendo" ){
			$qtde_respondendo = $objItem->QtdeEntrevistados;
		}
		if ( $objItem->Situacao == "N�o iniciaram" ){
			$qtde_naoiniciaram = $objItem->QtdeEntrevistados;
		}
		$qtde_total += $objItem->QtdeEntrevistados;
		//
		if ( $DW_dt_inicio_avaliacao == "" ){
			$DW_dt_inicio_avaliacao = date("d/m/Y", strtotime($objItem->DtInicio));
		}
		if ( $DW_dt_fim_avaliacao == "" ){
			$DW_dt_fim_avaliacao = date("d/m/Y", strtotime($objItem->DtFim));
		}
	}
	//
	//
    $sLinkFranqueados = "onclick=\"javascript:document.location.href='index.php?module=disc_usuarios&mode=view&filtroAv=" . $DW_id_avaliacao . "'\" ";
    $sLinkModeloEmail = "onclick=\"javascript:document.location.href='index.php?module=disc_projetos&mode=modelo&id=" . $DW_id_avaliacao . "&idc=" . $DW_idc . "'\" ";
    //
    $perc_zero = 100 * $qtde_naoiniciaram / $qtde_total;
    $perc_resp = 100 * $qtde_respondendo / $qtde_total;
    $perc_conc = 100 - ($perc_zero + $perc_resp);
    //
    //
    if(($DW_dt_inicio_avaliacao!="")||($DW_dt_fim_avaliacao!="")) {
        // Esta pesquisa podera ser respondida
        $DW_periodo_aplicacao = "Esta pesquisa poder&aacute; ser respondida ";
        //
        if(($DW_dt_inicio_avaliacao!="")&&($DW_dt_fim_avaliacao!="")) {
            //  no periodo de XX ate XX.
            $DW_periodo_aplicacao .= "no per&iacute;odo";
            $DW_periodo_aplicacao .= " de <b>" . $DW_dt_inicio_avaliacao . "</b>";
            $DW_periodo_aplicacao .= " at&eacute; <b>" . $DW_dt_fim_avaliacao . "</b>";
            //
        } elseif($DW_dt_inicio_avaliacao!="") {
            // a partir de XX.
            $DW_periodo_aplicacao .= "a partir ";
            $DW_periodo_aplicacao .= "de <b>" . $DW_dt_inicio_avaliacao . "</b>";
            //
        } elseif($DW_dt_fim_avaliacao) {
            //ate XX.
            $DW_periodo_aplicacao .= "at&eacute; <b>" . $DW_dt_fim_avaliacao . "</b>";
        }
        //
        $DW_periodo_aplicacao .= ".";
        //
    } else {
        $DW_periodo_aplicacao = "";
    }
    ?>
    <tr>
        <td nowrap colspan="2">
            <?php echo($DW_periodo_aplicacao); ?>
        </td>
    </tr>
    <tr>
        <td nowrap colspan="2"><b>Pesquisa Usu�rios</b></td>
    </tr>
    <tr>
        <td width="50%" colspan="2">
            <input type='button' 
                name="cmdVerFranqueados" value='Ver usu�rios' 
                title="Exibir a lista de usu�rios." 
                class="botao"
                <?= $sLinkFranqueados ?> onmouseover="this.style.cursor='pointer';"
            />
            &nbsp;&nbsp;&nbsp;
            <input type='button' 
                name="cmdModelo" value='Modelo do e-mail' 
                title="Ajustar o modelo de e-mail do franqueado." 
                class="botao"
                <?= $sLinkModeloEmail ?> onmouseover="this.style.cursor='pointer';"
            />
            <br /><br />
            <table style="border-width: 1px; border-style: solid; border-color: darkblue;">
                <tr>
                    <td width="110" colspan="3" style="text-align: center; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: darkblue;"><b>Resumo</b></td>
                </tr>
                <?php
                if($qtde_total > 0) {
                ?>
                <tr>
                    <td width="110" style="text-align: center;">Conclu�ram</td>
                    <td width="110" style="text-align: center;">Respondendo</td>
                    <td width="110" style="text-align: center;">N�o iniciaram</td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_conc, 0, ",", ".")."% (".number_format($qtde_concluidos, 0, ",", ".").")"); ?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_resp, 0, ",", ".")."% (".number_format($qtde_respondendo, 0, ",", ".").")"); ?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_zero, 0, ",", ".")."% (".number_format($qtde_naoiniciaram, 0, ",", ".").")"); ?>
                    </td>
                </tr>
                <?php
                } else {
                    ?>
                <tr>
                    <td width="330" colspan="3" style="text-align: center; border-bottom-width: 0px; border-bottom-style: solid; border-bottom-color: darkblue;">N�o h� entrevistados cadastrados!</td>
                </tr>
                <?php
                }
                ?>
            </table>
        </td>
    </tr>
	<?php //PROJETOS ANALISE ?>
    <tr>
        <td colspan="3">
			<?php
			include_once("../dsm_projetos/inc/projetos_analise.php");
			?>
		</td>
	</tr>
</table>
