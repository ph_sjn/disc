<script src='disc_projetos/js/ajax.js' type='text/javascript'></script>
<script src='dsm_projetos/js/projetos_analise.js' type='text/javascript'></script>
<script src='../survey/js/dssurvey_graph.js' type='text/javascript'></script>

<?php
include_once "disc_config/dw_dlg_aguarde_msg.php";

$acesso_administrativo = checaPermissao($module,"A",$user_access); 

$REQ_id_cliente = $_REQUEST["idc"];

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
}
?>

<br>

<script language="javascript">
    function executarSubmitIDC() {
        var objForm = document.getElementById("frm");
        dw_dlg_exibirAguarde();
        objForm.submit();
    }
</script>

<form action='index.php' method='post' id="frm" name='frm'>
    <input type="hidden" name="module" value="<?= $module ?>">
    
    <table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
        <tr>
            <td style="text-align:right">

                <table align="right">
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 

                            Cliente: 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select name='idc' class="combo"  onchange="javascript:executarSubmitIDC();">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                                echo("<input type='hidden' id='idc' name='idc' value='" . $DEFWEB_cd_cliente . " '>");
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
<br/>

<table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
    <tr>
        <td>
<?

$REQ_acao = $_REQUEST["acao"];

if($REQ_acao == "reset") {
    //
    $REQ_avaliacao = $_REQUEST["id"];
    $REQ_token = $_REQUEST["id_token"];
    //
    $objToken = new clsDWClocalToken();
    //
    $objToken->Id = $REQ_token;
    $objToken->IdCliente = $DEFWEB_id_cliente;
    $objToken->IdAvaliacao = $REQ_avaliacao;
    //
    if($DEFWEB_master_user) {
        //
        $objToken->IdCliente = $REQ_id_cliente;
    }
    //
    $objRetorno_temp = DWX_reiniciarPesquisaToken(1, $objToken);
    $objRetorno = json_decode($objRetorno_temp, false);
    //
    if ($objRetorno->ERRO != 0) {
        //
        $sMensagem = "As respostas do entrevistado n�o foram descartadas!";
        $sCor = "red";
    } else {
        $sMensagem = "As respostas do entrevistado foram descartadas!";
    }
    //
    // Mensagem de retorno
    echo retornaMensagem("<font color='" . $sCor . "'>" . $sMensagem . "</font>");
    //
    unset($objToken);    
}

// Obtem a lista de pesquisas liberadas para o cliente
//
if($DEFWEB_master_user) {
    //
    $colPesquisas = DWX_obterPesquisas(1, $REQ_id_cliente, "");
} else {
    $colPesquisas = DWX_obterPesquisas(1, $DEFWEB_cd_cliente, "");
}
$colPesquisasDecoded = json_decode($colPesquisas, false);

// Contador para os IDs da lista
$iPos = 0;

// Para cada item da colecao de pesquisas...
foreach ($colPesquisasDecoded AS $objItem) {
    if($objItem) {
        //
        // Incrementa o contador dos IDs da lista
        $iPos++;
        // Sufixo do ID
        $sKey = str_pad($iPos, 4, "0", STR_PAD_LEFT);
?>
            <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
                <tr width="100%" onclick="javascript:montarDashboard('<?= $module ?>', '<?= $objItem->IdAvaliacao ?>', 'div_<?= $module ?>_<?= $sKey ?>', 'imagem_<?= $sKey ?>', 'tr_<?= $sKey ?>', '<?= $module ?>', '', '<?= $sKey ?>');" onmouseover="this.style.cursor = 'pointer'">
                    <td bgcolor="<?= $bgcolor4 ?>">
                        <table width="100%" cellpadding="0" cellspacing="0" border=0>
                            <tr>
                                <td><img id="imagem_<?= $sKey ?>" src="img/close.gif" border="0">&nbsp;
                                    <b><?= $objItem->NomeProjeto . " : " . $objItem->Titulo ?></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display:none; padding: 0px;" id="tr_<?= $sKey ?>" bgcolor="<?= $bgcolor6 ?>">
                    <td style="padding: 0px;"><div id="div_<?= $module ?>_<?= $sKey ?>"></div></td>
                </tr>
            </table>
            <br>
<?php
        //
    }
}           

// TOTALIZACAO

$sPlural = "";
$sQtde = "";
//
if($iPos == 0) {
    //
    $sQtde .= "<span style=\"color:#FF0000\">NENHUMA PESQUISA ENCONTRADA!</span>";
    //
} else {
    //
    $sQtde .= "<span style=\"color:#FF0000\">" . number_format($iPos, 0, ",", ".") . "</span>";
    //
    if($iPos > 1) {
        $sPlural .= "s";
    }
    //
    $sQtde .= " pesquisa" . $sPlural . " encontrada" . $sPlural . ".";
}
//
?>
        </td>
    </tr>
</table>

<br>

<div style="text-align: center; font-weight: bold;"><?= $sQtde ?></div>

<br><br>
