<?
// Configuracao do cliente do Disc2Work

// Funciona somente se estiver no AWS-Master
require("/home/users/__discx/disc-lib/libDiscXLocal.php");

// Se nao estiver tem que copiar a pasta dwc-lib para esta pasta
// e usar o include abaixo
// 
// include_once("disc_config/disc-lib/libDiscXLocal.php");

//
$DISC_id_cliente = $_REQUEST["idc"];
$DISC_id_pesquisa = $_REQUEST["idp"];

//$DISC_objCliente = DISCX_consultarCliente($DISC_id_cliente, "");
$DISC_objCliente = DISCX_consultarCliente("" , $DISC_id_cliente);

$sHTML = "";

if($DISC_objCliente) {

    $DISC_objClienteDecoded = json_decode($DISC_objCliente);

    $DISC_cd_cliente = $DISC_objClienteDecoded->Codigo;
    
    $DISC_objProjetos = DWX_obterPesquisas("1", $DISC_cd_cliente, $DISC_id_cliente);

    if($DISC_objProjetos) {
        
        //$DISC_objProjetos = DISCX_listarProjetos("", $DISC_cd_cliente);
        $DISC_objProjetosDecoded = json_decode($DISC_objProjetos);

        //
        //$sHTML .= "<OPTION VALUE=\"\">(Selecione a pesquisa)</OPTION>";
        //
        foreach ($DISC_objProjetosDecoded AS $objAvaliacaoItem) {
            //
            if($DISC_id_pesquisa == $objAvaliacaoItem->IdAvaliacao) {
                $sAttrSelected = " SELECTED";
            } else {
                $sAttrSelected = "";
            }
            //
            $sHTML .= "<OPTION VALUE='";
            $sHTML .= $objAvaliacaoItem->IdAvaliacao;
            $sHTML .= "'" . $sAttrSelected;
            $sHTML .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
            //
        }
    } else {
        //$sHTML .= "<OPTION VALUE=\"\">(Nenhuma pesquisa encontrada)</OPTION>";
    }
} else {
    //$sHTML .= "<OPTION VALUE=\"\">(Nenhuma pesquisa encontrada)</OPTION>";
}
echo($sHTML);
