<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

//include_once("/home/users/extranet/arquivos_comuns/lib/inc/rp_mail.inc.php");
include_once("lib/mail/rp_mail_disc.inc.php");

$REQ_filtroSituacao = $_REQUEST["filtroSituacao"];
$REQ_filtroAv = $_REQUEST['filtroAv'];
$REQ_id_cliente = $_REQUEST["idc"];

if (($REQ_filtroSituacao != "0") && ($REQ_filtroSituacao != "1") && ($REQ_filtroSituacao != "2")) {
    $REQ_filtroSituacao = "*";
}

if (($acaoEmail) || ($acaoReiniciar) || ($acaoExcluir)) {

    $arrayTokens = $_REQUEST['chkToken'];
    $sTokens = "";
    $sMensagemAdicional = "";
    
    $sCor = "navy";

    for ($i = 0; $i < count($arrayTokens); $i++) {
        if ($sTokens != "") {
            $sTokens .= ",";
        }
        $sTokens .= $arrayTokens[$i];
    }
    //echo retornaMensagem("[DEBUG] <font color='navy'>Tokens selecionados: " . $sTokens . "</font>");

    // Se algum franqueado foi selecionado...
    if (count($arrayTokens) > 0) {
        //
        $objDEFWEBavaliacao_temp = DWX_obterPesquisa(1, $REQ_filtroAv);
        $objDEFWEBavaliacao = json_decode($objDEFWEBavaliacao_temp, false);
        
        $colTokens_temp = DWX_obterTokens(1, $REQ_filtroAv);
        $colTokens = json_decode($colTokens_temp, false);
        
        // Zera a quantidade de tokens processados
        $iQtdeTokens = 0;
        //
//        $sEmailAssunto = html_entity_decode($objDEFWEBavaliacao->EmailModeloAssunto);
//        //
//        $sEmailModeloCorpo = $objDEFWEBavaliacao->EmailModeloCorpo;
        //
        // Percorre o array de Tokens
        for($iPos = 0; $iPos < count($arrayTokens); $iPos++) {

            // Indicador de falha no processamento
            $bFalhou = FALSE;
            
            // Extrai o token atual
            $sToken = $arrayTokens[$iPos];
            // Le os dados do token
            $objToken = DWX_obterTokenPeloID($colTokens, $sToken);
            // Obtem os dados do entrevistado
            $sNomeFranqueado = $objToken->Nome;
            $sEmailFranqueado = $objToken->Email;
            $sUnidadeFranqueado = $objToken->Unidade;
            
            if ($acaoEmail) {
                
                // Atribuir modelo de inicio de questionario
                $sEmailAssunto = html_entity_decode($objDEFWEBavaliacao->EmailModeloAssunto);
                $sEmailModeloCorpo = $objDEFWEBavaliacao->EmailModeloCorpo;
                
                // Se o indicador de envio automatico de e-mail na conclusao estiver ligado...
                if($objDEFWEBavaliacao->EnviarEmailConclusao == 1) {
                
                    // Se o questionario ja foi concluido
                    if($objToken->QtdePerguntasFaltam == 0) {
                
                        // Se o assunto do modelo de conclusao estiver preenchido...
                        if( !is_null($objDEFWEBavaliacao->EmailConclusaoAssunto) && $objDEFWEBavaliacao->EmailConclusaoAssunto != "") {
                            // Atribui o assunto do modelo de conclusao
                            $sEmailAssunto = html_entity_decode($objDEFWEBavaliacao->EmailConclusaoAssunto);
                        }
                
                        // Se o corpo do modelo de conclusao estiver preenchido...
                        if( !is_null($objDEFWEBavaliacao->EmailConclusaoCorpo) && $objDEFWEBavaliacao->EmailConclusaoCorpo != "") {
                            // Atribui o corpo do modelo de conclusao
                            $sEmailModeloCorpo = $objDEFWEBavaliacao->EmailConclusaoCorpo;
                        }
                    }
                }
                
                //
                // Monta o link para a pesquisa
                //$sLink = "http://aws.franquiaextranet.com.br/discmaster/discx/";
                $sLink = $CONFIGENET_DISC2WORK . "survey/";
                $sLink .= "dssurvey_redir.php";
                $sLink .= "?id_h=" . $objToken->Hash;
                //$sLink .= "?id_avaliacao=" . $REQ_filtroAv;
                //$sLink .= "&id_token=" . $sToken;
                
                //
                // Monta o corpo do e-mail
                //
                // Substitui as macros
                $sEmailCorpo = str_replace("{NOME}", $sNomeFranqueado, $sEmailModeloCorpo);
                $sEmailCorpo = str_replace("{UNIDADE}", $sUnidadeFranqueado, $sEmailCorpo);
                $sEmailCorpo = str_replace("{LINK}", $sLink, $sEmailCorpo);
                // Substitui as quebras de linha do php pelas do html
                $sEmailCorpo = str_replace("\n", "<br />", $sEmailCorpo);
                //
                // Envia o e-mail
                $RET_email = rp_mail($sEmailFranqueado, $sEmailAssunto, $sEmailCorpo);
                //
//                echo("<pre>RET_email<br>");
//                print_r($RET_email);
//                echo("</pre>");
                //
                if(!($RET_email[0] == 1)) {
                    //
                    $bFalhou = TRUE;
                    //
                    //echo("<pre>"); print_r($RET_email); echo("</pre>");
                }
                //
            } elseif ($acaoReiniciar) {
                //
                $objToken = new clsDWClocalToken();
                //
                $objToken->Id = $sToken;
                $objToken->IdCliente = $objDEFWEBavaliacao->IdCliente;
                $objToken->IdAvaliacao = $objDEFWEBavaliacao->Id;
                //
                $objRetorno_temp = DWX_reiniciarPesquisaToken(1, $objToken);
                $objRetorno = json_decode($objRetorno_temp, false);
                //
                if ($objRetorno->ERRO != 0) {
                    //
                    $bFalhou = TRUE;
                }
                //
                unset($objToken);
                //
            } elseif ($acaoExcluir) {
                //
                $objToken = new clsDWClocalToken();
                //
                $objToken->Id = $sToken;
                $objToken->IdCliente = $objDEFWEBavaliacao->IdCliente;
                $objToken->IdAvaliacao = $objDEFWEBavaliacao->Id;
                //
                $objRetorno_temp = DWX_excluirToken(1, $objToken);
                $objRetorno = json_decode($objRetorno_temp, false);
                //
                if ($objRetorno->ERRO != 0) {
                    //
                    $bFalhou = TRUE;
                }
                //
                unset($objToken);
            }

            if($bFalhou) {
                //
                if($sMensagemAdicional == "") {
                    //
                    $sMensagemAdicional = "Ocorreram problemas com os seguintes entrevistado(s):<br />";
                    $sMensagemAdicional .= "<br />";
                }
                //
                $sMensagemAdicional .= $sNomeFranqueado;
                $sMensagemAdicional .= " (" . $sEmailFranqueado . ")";
                $sMensagemAdicional .= "<br>";
                //
            } else {
                // Incrementa o contador de tokens
                $iQtdeTokens++;
            }
        }
        
        
        // Mensagem de retorno padrao
        if($acaoEmail) {
            $sMensagem = "Nenhum e-mail foi enviado!";
            //
        } elseif($acaoReiniciar) {
            $sMensagem = "Nenhuma pesquisa foi reiniciada!";
            //
        } elseif($acaoExcluir) {
            $sMensagem = "Nenhum franqueado foi exclu�do!";
            //
        }
        
        // Se algum token foi processado...
        if($iQtdeTokens > 0) {
            // Se foi enviado mais de um e-mail
            if($iQtdeTokens > 1) {
                $sVarios = "s";
            }
            // Monta a mensagem de retorno
            $sMensagem = sprintf("%d", $iQtdeTokens);
            //
            if($acaoEmail) {
                //
                $sMensagem .= " e-mail" . $sVarios;
                $sMensagem .= " enviado" . $sVarios . "!";
                //
            } elseif($acaoReiniciar) {
                //
                $sMensagem .= " pesquisa" . $sVarios;
                $sMensagem .= " reiniciada" . $sVarios . "!";
                //
            } elseif($acaoExcluir) {
                //
                $sMensagem .= " entrevistado" . $sVarios;
                $sMensagem .= " exclu�do" . $sVarios . "!";
            }
        }
    } else {
        //
        $sMensagem = "Selecione pelo menos um entrevistado para executar a a��o!";
        $sCor = "red";
        
    }

    // Mensagem de retorno
    echo retornaMensagem("<font color='" . $sCor . "'>" . $sMensagem . "</font>");
    
    // Se tiver mensagem adicional
    if($sMensagemAdicional != "") {
        //
        // Mensagem adicional de retorno
        echo retornaMensagem("<font color='red'>" . $sMensagemAdicional . "</font>");
    }

}
//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
}

//
// Obtem a lista de pesquisas liberadas para o cliente
//
$sHTMLavaliacoes = "";
//
if($REQ_id_cliente != "" || $DEFWEB_cd_cliente != "") {
    //
    if($DEFWEB_master_user) {
        //
        $colPesquisas = DWX_obterPesquisas(1, $REQ_id_cliente, "");
    } else {
        $colPesquisas = DWX_obterPesquisas(1, $DEFWEB_cd_cliente, "");
    }
    $colPesquisasDecoded = json_decode($colPesquisas, false);
    //
    foreach ($colPesquisasDecoded AS $objAvaliacaoItem) {
    //    //
    //    if(($REQ_filtroAv == "") && ($REQ_id_cliente != "")) {
    //        $REQ_filtroAv = $objAvaliacaoItem->IdAvaliacao;
    //    }
        //
        if ($REQ_filtroAv == $objAvaliacaoItem->IdAvaliacao || $REQ_filtroAv == "") {
            $REQ_filtroAv = $objAvaliacaoItem->IdAvaliacao;
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objAvaliacaoItem->IdAvaliacao;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
        //
        $sHTMLavaliacoes .= $sHTML;
    }
}

include_once "disc_config/dw_dlg_aguarde_msg.php";

?>




<script language="javascript">
    function inverterSelecao() {
        var objchkSelecao = document.getElementById('chkSelecao');
        if (objchkSelecao.checked) {
            selecionarTodos();
        } else {
            limparSelecao();
        }
    }

    function selecionarTodos() {
        var objlblSelecao = document.getElementById('lblSelecao');
        objlblSelecao.innerHTML = "Limpar sele��o";
        var objchkToken = document.getElementsByName('chkToken[]');
        for (iPos = 0; iPos < objchkToken.length; iPos++) {
            objchkToken[iPos].checked = true;
        }
    }

    function limparSelecao() {
        var objlblSelecao = document.getElementById('lblSelecao');
        objlblSelecao.innerHTML = "Selecionar todos";
        var objchkToken = document.getElementsByName('chkToken[]');
        for (iPos = 0; iPos < objchkToken.length; iPos++) {
            objchkToken[iPos].checked = false;
        }
    }
    
    function confirmarEnvioDeEmails() {
        if(confirm('Tem certeza que deseja reenviar o e-mail sobre o question�rio para os entrevistados selecionados?')) {
            dw_dlg_exibirAguarde();
            return(true);
        } else {
            return(false);
        }
    }
    
    function confirmarExclusaoDeFranqueados() {
        if(confirm('Tem certeza que deseja excluir os registros selecionados?')) {
            dw_dlg_exibirAguarde();
            return(true);
        } else {
            return(false);
        }
    }
    
    function confirmarReinicioDePesquisas() {
        if(confirm('Tem certeza que deseja reiniciar os question�rios dos registros selecionados?')) {
            dw_dlg_exibirAguarde();
            return(true);
        } else {
            return(false);
        }
    }

    function limparAv() {
        var objAv = document.getElementById('id_avaliacao');
        objAv.value = "";
        var objAv = document.getElementById('filtroAv');
        objAv.value = "";
    }

    function executarSubmit() {
        var objForm = document.getElementById("frm");
        dw_dlg_exibirAguarde();
        objForm.submit();
    }
    
	function copyToClipboard(sValue) {
	  var aux = document.createElement("input");
	  aux.setAttribute("value", sValue);
	  document.body.appendChild(aux);
	  aux.select();
	  document.execCommand("copy");
	  document.body.removeChild(aux);
	  alert("Link copiado para o clipboard. Cole onde quiser.");

	}
</script>

<form action='index.php' method='post' id="frm" name='frm'>
    <input type="hidden" name="module" value="<?= $module ?>">
    <input type="hidden" name="id_avaliacao" id="id_avaliacao" value="<?= $REQ_filtroAv ?>" >

    <table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
        <tr>
            <td style="vertical-align:middle;">
                <a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms&idc=<?=$REQ_id_cliente ?>&filtroAv=<?= $REQ_filtroAv ?>"><span style="font-weight:bold;">CADASTRAR USU�RIO</span></a>
                &nbsp; &nbsp; &nbsp; 
                <a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=importacao&idc=<?=$REQ_id_cliente ?>&filtroAv=<?= $REQ_filtroAv ?>"><span style="font-weight:bold;">IMPORTAR LISTA...</span></a>			
            </td>
        </tr>
        <tr>
            <td style="text-align:right">

                <table align="right">
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 

                            Cliente: 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select name='idc' class="combo"  onchange="javascript:limparAv();executarSubmit();">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>

                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 

                            Nome da Pesquisa: 
                            <select name='filtroAv' id='filtroAv' class="combo"  onchange="javascript:executarSubmit();">
                                <?php
                                echo($sHTMLavaliacoes);
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>

                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
                            Situa��o do question�rio: 
                            <select name='filtroSituacao' class="combo" onchange="javascript:executarSubmit();">
                                <option value='*' <?php echo(($REQ_filtroSituacao == "*" ? " SELECTED" : "")); ?>>Todos</option>
                                <option value='0' <?php echo(($REQ_filtroSituacao == "0" ? " SELECTED" : "")); ?>>N�o iniciado</option>
                                <option value='2' <?php echo(($REQ_filtroSituacao == "2" ? " SELECTED" : "")); ?>>Em andamento</option>
                                <option value='1' <?php echo(($REQ_filtroSituacao == "1" ? " SELECTED" : "")); ?>>Conclu�do</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
                            <input type='Submit' name="acaoAtualizar" value='Atualizar a lista' 
                                   title="Atualizar a listagem" 
                                   class="botao"
                                   onclick="javascript:dw_dlg_exibirAguarde()"
                                   >
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br>
    <table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr>
            <td>
                <div style="width:140px; display: inline-block;">
                    <input type="checkbox" name="chkSelecao" id="chkSelecao" onchange="javascript:inverterSelecao();">
                    &nbsp;<span id="lblSelecao">Selecionar todos</span>
                </div>    
                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;
                <input type='Submit' name="acaoEmail" value='Enviar e-mail' 
                       title="Enviar e-mail aos usu�rios selecionados." 
                       class="botao"
                       onclick="return confirmarEnvioDeEmails();"
                       >
                &nbsp;&nbsp;&nbsp;
                <input type='Submit' name="acaoReiniciar" value='Reiniciar' 
                       title="Reiniciar question�rios selecionados." 
                       class="botao"
                       onclick="return confirmarReinicioDePesquisas();"
                       >
                &nbsp;&nbsp;&nbsp;
                <input type='Submit' name="acaoExcluir" value='Excluir' 
                       title="Excluir registros selecionados." 
                       class="botao"
                       onclick="return confirmarExclusaoDeFranqueados();"
                       >
            </td>
        </tr>
    </table>
    <br>

    <table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td>
                <table border=0 cellpadding=4 cellspacing=0 width='100%' bgcolor="<?= $bgcolor5 ?>" align='center'>
                    <tr bgcolor="<?= $bgcolor4 ?>">
                        <td style="font-weight: bold;">UNIDADE</td>
                        <td style="font-weight: bold;">NOME</td>
                        <td style="font-weight: bold;">E-MAIL</td>
                        <td style="font-weight: bold;">SITUA��O</td>
                        <td style="font-weight: bold;">OP��ES</td>
                    </tr>
<?
//
$total_linhas = 0;
//
//if($REQ_filtroAv == "") {
if($sHTMLavaliacoes == "") {
    $colTokens_temp = DWX_obterTokens(1, "99999999");
} else {
    $colTokens_temp = DWX_obterTokens(1, $REQ_filtroAv);
}
//echo("<hr>colTokens_temp($REQ_filtroAv):<pre>"); print_r($colTokens_temp); echo("</pre><hr>");
//
$colTokens = json_decode($colTokens_temp, false);
//
//echo("<hr>colTokens($REQ_filtroAv):<pre>"); print_r($colTokens); echo("</pre><hr>");
//
foreach ($colTokens As $objToken) {
    //
    if ($objToken->CodigoPublicoAlvo == 2) {
        //
        if ($objToken->QtdeTotalPerguntas > 0) {
            $PERC_resp = 100 * $objToken->QtdePerguntasRespondidas / $objToken->QtdeTotalPerguntas;
        } else {
            $PERC_resp = 100;
        }                        //
        $QTDE_saldo = $objToken->QtdeTotalPerguntas - $objToken->QtdePerguntasRespondidas;
        //
        if (($objToken->QtdeTotalPerguntas > 0 && $objToken->QtdePerguntasRespondidas == 0)) {
            $sSituacao = "N�o iniciado";
        } else {
            //$sSituacao = "(Faltam ".$QTDE_saldo." perguntas, realizado " . number_format($PERC_resp, 0, ",", ".") . "%)";
            $sSituacao = number_format($PERC_resp, 0, ",", ".") . "% realizado (Faltam " . $QTDE_saldo . " perguntas)";
        }
        //
        if ($QTDE_saldo == 0){
        	$sSituacao = "Conclu�do";
        }        
        $tr_hover = " onmouseover=\"this.style.cursor='pointer'; this.style.backgroundColor='$bgcolor4'\"";
        $tr_mouseout = " onmouseout=\"this.style.backgroundColor='" . $bgcolor6 . "'\"";
        $sLinkModificar = "onclick=\"javascript:dw_dlg_exibirAguarde();document.location.href='index.php?module=" . $module . "&mode=forms&id_token=" . $objToken->Id . "&idc=" . $REQ_id_cliente . "&filtroAv=" . $REQ_filtroAv . "'\" ";
//        $sLinkTestar = " onclick=\"javascript:window.open('http://aws.franquiaextranet.com.br/discmaster/discx/dssurvey_redir.php?id_avaliacao=" . $objToken->IdAvaliacao . "&id_token=" . $objToken->Id . "');\" ";
//        $sLinkTestar = " onclick=\"javascript:window.open('http://www.disc2work.com.br/survey/dssurvey_redir.php?id_avaliacao=" . $objToken->IdAvaliacao . "&id_token=" . $objToken->Id . "');\" ";
        $sLinkTestar = " onclick=\"javascript:window.open('" . $CONFIGENET_DISC2WORK . "survey/dssurvey_redir.php?id_h=" . $objToken->Hash . "');\" ";
        $sLinkReal = $CONFIGENET_DISC2WORK . "survey/dssurvey_redir.php?id_h=" . $objToken->Hash;
        $sLinCopiar = " onclick=\"javascript:copyToClipboard('" . $CONFIGENET_DISC2WORK . "survey/dssurvey_redir.php?id_h=" . $objToken->Hash . "');\" ";
        if(     ($REQ_filtroSituacao == "*") 
            ||  ($REQ_filtroSituacao == "0" && $PERC_resp == 0) 
            ||  ($REQ_filtroSituacao == "1" && $PERC_resp == 100) 
            ||  ($REQ_filtroSituacao == "2" && $PERC_resp > 0 && $PERC_resp < 100)                 
            ) {
        
            $total_linhas++;
        //
        ?>
                            <tr <?= $tr_hover . $tr_mouseout . $onclick ?> bgcolor="<?= $bgcolor6 ?>">
                                <td><input type="checkbox" name="chkToken[]" id="chkToken[]" value="<?php echo($objToken->Id); ?>"> <?= $objToken->Unidade ?></td>
                                <td><?= $objToken->Nome ?></td>
                                <td><?= $objToken->Email ?></td>
                                <td><?php echo($sSituacao); ?></td>
        <?php
        /* <td><a <?= $sLinkModificar ?>>Modificar</a> | <a <?= $sLinkTestar ?>>Testar</a></td>
         */
        ?>
                                <td>
                                    <input type='button' name="cmdModificar" value='Modificar' 
                                           title="Alterar dados." 
                                           class="botao"
        <?= $sLinkModificar ?>
                                           >
                                    <?php
                                    if($acesso_administrativo) {
                                    ?>
                                    &nbsp;
                                    <input type='button' name="cmdTestar" value='Abrir question�rio' 
                                           title="Abrir question�rio." 
                                           class="botao"
        <?= $sLinkTestar ?>
                                           >
                                    <?php
                                    }
                                    ?>
                                    <!--&nbsp;<a href="<?php echo($sLinkReal); ?>" class="botao">Link</a> -->
									&nbsp;
                                    <input type='button' name="cmdCopiar" value='Copiar Link' 
                                           title="Copiar Link." 
                                           class="botao"
        <?= $sLinCopiar ?>
                                           >                                   
                                </td>
                            </tr>
        <?php
            }
    }
}
?>

                </table>

            </td>
        </tr>
    </table>

    <br><br>

    <div style="text-align: center; font-weight: bold;">Total de registros encontrados: <span style="color:#FF0000"><?= $total_linhas ?></span> </div>

    <br><br>

</form>

