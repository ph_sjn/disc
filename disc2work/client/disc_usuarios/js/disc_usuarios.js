function Ajax() {
    try {
        return new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (ex) {
            try {
                return new XMLHttpRequest();
            } catch (exc) {
                return false;
            }
        }
    }
}

function carregarProjetos() {
    //
    ajax2 = Ajax();
    var objDropDownCliente = document.getElementById("id_cliente");
    //
    url_pagina = "disc_usuarios/ajaxcli_obterpesquisas.php?idc=" + objDropDownCliente.value;
    //
    ajax2.open("GET", url_pagina, true);
    ajax2.onreadystatechange = function() {
        if (ajax2.readyState == 4) {
            if (ajax2.status == 200) {
                objDropDownProjeto = document.getElementById("filtroAv");
                objDropDownProjeto.innerHTML = ajax2.responseText;
            }
        }
    }
    ajax2.send(null);
}
