<?
include_once "disc_config/dw_dlg_aguarde_msg.php";

$acesso_administrativo = checaPermissao($module, "A", $user_access);

$REQ_idc = $_REQUEST["idc"];

$DW_id_avaliacao = $_REQUEST["filtroAv"];

// Se veio o id do franqueado
if ($_REQUEST['id_token']) {
    
    $objToken_ret = DWX_obterTokenPeloIDnoBD(1, $DW_id_avaliacao, $_REQUEST['id_token']);
    $objToken = json_decode($objToken_ret);
    
//    echo("$DW_id_avaliacao /" .  $_REQUEST["id_token"] . "<pre>");
//    print_r($objToken);
//    echo("</pre>");
//    echo("<hr>");
    
    /*
    // Obt�m a lista de tokens j� cadastrados
    $colTokens_temp = DWX_obterTokens(1, $DW_id_avaliacao);
    $colTokens = json_decode($colTokens_temp, false);
    //
    $objToken = DWX_obterTokenPeloID($colTokens, $_REQUEST['id_token']);
    */
    //
    $_REQUEST["filtroAv"] = $objToken->IdAvaliacao;
    //
    $DW_id_cliente = $objToken->IdCliente;
}

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if (($DW_id_cliente != "" && $_REQUEST['id_token'] && $objCliente->Id == $DW_id_cliente)) {
            //
            $REQ_idc = $objCliente->Codigo;
        }
//        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
        if ($REQ_idc == $objCliente->Codigo || $REQ_idc == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            $DEFWEB_cd_cliente = $REQ_id_cliente;
            $DW_id_cliente = $objCliente->Id;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
} else {
    if($REQ_idc == "") {
        $REQ_idc = $DEFWEB_cd_cliente;
    }
}
    
include_once "disc_config/dw_dlg_aguarde_msg.php";
?>

<script language="JavaScript" src="disc_usuarios/js/disc_usuarios.js"></script>

<?php
//}
?>

<form method=post enctype='multipart/form-data' action='index.php' id="frm" name='frm'>
    <input type=hidden name='id_token' value="<?= $_REQUEST['id_token'] ?>">
    <input type=hidden name='idc' value="<?= $REQ_idc ?>">
    <input type=hidden name='module' value='<?= $_REQUEST['module'] ?>'>
    <input type=hidden name='mode' value='data'>


    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">CADASTRO DO USU�RIO</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr>
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Cliente:</td>
                        <td style="font-weight: bold; vertical-align:middle;"> 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select id="id_cliente" name='id_cliente' class="combo"  onchange="javascript:carregarProjetos();">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                ?>
                                <input type=hidden name='id_cliente' value="<?= $DW_id_cliente ?>">
                                <?php
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                            }
                            ?>
                        </td>
                    </tr>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Nome da Pesquisa:</td>
                        <td style="font-weight: bold;">
                            <select id="filtroAv" name="filtroAv" class="combo">
                                <?
                                //
                                // Obtem a lista de pesquisas liberadas para o cliente
                                $colPesquisas = DWX_obterPesquisas(1, $DEFWEB_cd_cliente, "");
                                $colPesquisasDecoded = json_decode($colPesquisas, false);
                                //
                                foreach ($colPesquisasDecoded AS $objAvaliacaoItem) {
                                    //
                                    if ($_REQUEST["filtroAv"] === $objAvaliacaoItem->IdAvaliacao) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<OPTION VALUE='";
                                    $sHTML .= $objAvaliacaoItem->IdAvaliacao;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
                                    //
                                    echo($sHTML);
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">E-mail:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_email" value="<?php echo($objToken->Email);  ?>" class="texto" size="50" maxlength="50"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Nome:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_nome" value="<?php echo($objToken->Nome); ?>" class="texto" size="50" maxlength="60"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">C�digo:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_codigo" value="<?php echo($objToken->Codigo);  ?>" class="texto" size="45" maxlength="45"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Unidade:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_unidade" value="<?php echo($objToken->Unidade); ?>" class="texto" size="45" maxlength="45"></td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Observa��es:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_observacao" value="<?php echo($objToken->Observacoes); ?>" class="texto" size="50" maxlength="250"></td>
                    </tr>
                    
                </table>

            </td>
        </tr>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td>
                            <input type="button" onclick="javascript:dw_dlg_exibirAguarde();document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&idc=<?= $REQ_idc ?>&filtroAv=<?= $_REQUEST["filtroAv"] ?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <? if ($_REQUEST['id_token']) { ?>
                                <input type="submit" name="salvar" value="Salvar"  class="botao" onclick="javascript:dw_dlg_exibirAguarde();">
                                <input type="submit" name="excluir" value="Excluir"  class="botao" onclick="return confirm('Tem certeza que deseja excluir permanentemente este registro?')">
                            <? } else { ?>
                                <input type="submit" name="incluir" value="Incluir" class="botao" onclick="javascript:dw_dlg_exibirAguarde();">
                            <? } ?>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>
