<?

$REQ_id_token = $_POST["id_token"];
$REQ_id_avaliacao = $_REQUEST["filtroAv"];
$REQ_cd_cliente = $_REQUEST["id_cliente"];

//
if($DEFWEB_master_user) {
    //
    $DISC_objCliente = DISCX_consultarCliente("", $REQ_cd_cliente);
    $DISC_objClienteDecoded = json_decode($DISC_objCliente);

    // Atribui o c�digo e o ID do cliente
    $DEFWEB_cd_cliente = $DISC_objClienteDecoded->Codigo;
    $DEFWEB_id_cliente = $DISC_objClienteDecoded->Id;
}

if ($incluir) {

    if ($REQ_id_avaliacao == "") {
        $msg_erro = "Selecione a Avalia��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if (!$msg_erro) {

        $objToken = new clsDWClocalToken();
        //
        $objToken->IdCliente = $DEFWEB_id_cliente;
        $objToken->IdAvaliacao = $REQ_id_avaliacao;
        $objToken->Nome = $_POST['ds_nome'];
        $objToken->Codigo = $_POST['ds_codigo'];
        $objToken->Email = $_POST['ds_email'];
        $objToken->Unidade = $_POST['ds_unidade'];
        $objToken->Observacoes = $_POST['ds_observacao'];
        $objToken->CodigoPublicoAlvo = 2;
        
        $objRetorno_temp = DWX_criarToken(1, $objToken);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Registro inclu�do com sucesso.");
        } else {
//            echo("<pre>");
//            print_r($objRetorno);
//            echo("</pre>");
//            echo("<hr>");
            echo retornaMensagem("Falhou na inclus�o do registro.");
        }

        $_REQUEST["filtroAv"] = $REQ_id_avaliacao;

        include_once("disc_usuarios/disc_usuarios_view.php");
    } else {
        include_once("disc_usuarios/disc_usuarios_forms.php");
    }
}

if ($salvar) {

    if ($REQ_id_avaliacao == "") {
        $msg_erro = "Selecione a Avalia��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    if (!$msg_erro) {

        $objToken = new clsDWClocalToken();
        //
        $objToken->Id = $REQ_id_token;
        $objToken->IdCliente = $DEFWEB_id_cliente;
        $objToken->IdAvaliacao = $REQ_id_avaliacao;
        $objToken->Nome = $_POST['ds_nome'];
        $objToken->Codigo = $_POST['ds_codigo'];
        $objToken->Email = $_POST['ds_email'];
        $objToken->Unidade = $_POST['ds_unidade'];
        $objToken->Observacoes = $_POST['ds_observacao'];
        $objToken->CodigoPublicoAlvo = 2;
        
        $objRetorno_temp = DWX_gravarToken(1, $objToken);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Dados atualizados com sucesso.");
        } else {
//            echo("<pre>");
//            print_r($objRetorno);
//            echo("</pre>");
//            echo("<hr>");
            echo retornaMensagem("Ocorreu um erro ao atualizar o registro. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        }

        $_REQUEST["filtroAv"] = $REQ_id_avaliacao;
    }

    include_once("disc_usuarios/disc_usuarios_forms.php");

}

if ($excluir) {

    if (!$msg_erro) {

        $objToken = new clsDWClocalToken();
        //
        $objToken->Id = $REQ_id_token;
        $objToken->IdCliente = $DEFWEB_id_cliente;
        $objToken->IdAvaliacao = $REQ_id_avaliacao;
        
        $objRetorno_temp = DWX_excluirToken(1, $objToken);
        $objRetorno = json_decode($objRetorno_temp, false);
        
        if ($objRetorno->ERRO == 0) {
            echo retornaMensagem("Registro exclu�do com sucesso.");
        } else {
//            echo("<pre>");
//            print_r($objRetorno);
//            echo("</pre>");
//            echo("<hr>");
            echo retornaMensagem("Ocorreu um erro ao excluir o registro. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
        }

        $_REQUEST["filtroAv"] = $REQ_id_avaliacao;
    }

    include_once("disc_usuarios/disc_usuarios_view.php");
}
?>