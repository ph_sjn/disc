<?php
include_once "disc_config/dw_dlg_aguarde_msg.php";

// Indica se veio um arquivo para importa��o
$DW_POST = ($_POST['enviar_planilha']) && ($_FILES['arquivo']['tmp_name']) && ($_FILES['arquivo']['name']);
//$DW_id_cliente = 0;
$REQ_filtroAv = $_REQUEST['filtroAv'];
$REQ_idc = $_REQUEST["idc"];

//
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
//        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
        if ($REQ_idc == $objCliente->Codigo || $REQ_idc == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            $DEFWEB_cd_cliente = $REQ_id_cliente;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
?>
<script language="JavaScript" src="disc_usuarios/js/disc_usuarios.js"></script>

<?php
}


?>
<br>

<form method=post enctype='multipart/form-data' action='index.php' id="frm" name='frm'>
    <input type=hidden name='module' value='<?= $module ?>'>
    <input type=hidden name='mode' value='<?= $mode ?>'>
    <input type="hidden" name="filtroAv" value='$_REQUEST["filtroAv"]' >

    <a name="linkTOPO"></a>

<?php
// Se recebeu o POST do arquivo planilha para importa��o...
if ($DW_POST) {
    // Processa a importa��o e monta a p�gina com o resultado
?>   
<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0 width="70%">
    <tr bgcolor="<?= $bgcolor4 ?>">
        <td style="text-align:center; font-weight: bold;">RESULTADO DA IMPORTA��O DE USU�RIOS</td>
    </tr>
    <tr bgcolor="<?= $bgcolor6 ?>">
        <td>
            <br>
            <b>ARQUIVO RECEBIDO:</b> <?= $_FILES['arquivo']['name'] ?><br>
            <br>
<?php
               
    // Indica se deve ignorar a primeira linha
    $DW_ignorarPrimeiraLinha = strtoupper($_POST["chkIgnorarLinha1"] === "S");
    // ID da avalia��o
    $DW_id_avaliacao = $REQ_filtroAv;
    //
    $objDWavaliacao_temp = DWX_obterPesquisa(1, $DW_id_avaliacao);
    $objDWavaliacao = json_decode($objDWavaliacao_temp, false);
    
    // ID do cliente
    $DW_id_cliente = $objDWavaliacao->IdCliente;
    // Data e hora da importa��o
    $DW_dt_criacao = date("Y-m-d H:i:s");
    // Franqueado
    $DW_cd_publico_alvo = "2";
    
    ?>
            Nome da Pesquisa: <?php echo($objDWavaliacao->Titulo); ?><br>
            <br>
            Cliente: <?php echo($objDWavaliacao->NomeCliente); ?><br>
            <br>
            Data e hora do processamento: <?php echo(date("d/m/Y"). " �s ".date("H:i:s")); ?><br>
            <br>
    <?php
    // Avisa o usu�rio se estiver ignorando a primeira linha do arquivo...
    if($DW_ignorarPrimeiraLinha) {
        echo("A primeira linha do arquivo foi ignorada.<br><br>");
    }
    
    // Obt�m a lista de tokens j� cadastrados
    $colTokens_temp = DWX_obterTokens(1, $DW_id_avaliacao);
    $colTokens = json_decode($colTokens_temp, false);
    
    //
//    echo("<hr>colTokens:<pre>"); print_r($colTokens); echo("</pre><hr>");
    //
    
    // Biblioteca para a manipula��o de planilhas MS-Excel
    include_once "/home/users/extranet/excel_xlsx/PHPExcel/IOFactory.php";
    // Instancia o objeto da classe para MS-Excel 2007
    $objReader07 = new PHPExcel_Reader_Excel2007();
    $objReader07->setReadDataOnly(true);

    // Nome do arquivo original
    $arquivo = strtolower($_FILES['arquivo']['name']);
    // Nome do arquivo tempor�rio armazenado pelo upload
    $arquivo_temp = $_FILES['arquivo']['tmp_name'];
    
    // Armazena as linhas rejeitadas para informa��o do usu�rio
    $linhasRejeitadas = array();
    
    // Armazena as linhas que talvez precisem ser revisadas
    $linhasAlertas = array();
    
    // Zera os contadores
    $qtdeOk = 0;
    $qtdeRejeitadas = 0;
    $qtdeLidos = 0;
    $qtdeAlertas = 0;

    // Se for do tipo MS-Excel...
    if (strrchr($arquivo,'.') == '.xlsx') {

        // Carrega a planilha
        $objPHPExcel = $objReader07->load($arquivo_temp);
        // Obt�m os dados da planilha
        $objPlanilha = $objPHPExcel->getActiveSheet();
                
        // Obt�m a �ltima linha da planilha
        $posUltimaLinha = $objPlanilha->getHighestRow();
        
        // Percorre as linhas da planilha...
        for($posLinha = ($DW_ignorarPrimeiraLinha ? 2 : 1); $posLinha <= $posUltimaLinha; $posLinha++) {
            // Incrementa o contador de linhas lidas
            $qtdeLidos++;
            // Obt�m os valores dos campos da planilha
            $Coluna_A_email = addslashes(utf8_decode(trim($objPlanilha->getCellByColumnAndRow(0, $posLinha)->getValue())));
            $Coluna_B_nome = addslashes(utf8_decode(trim($objPlanilha->getCellByColumnAndRow(1, $posLinha)->getValue())));
            $Coluna_C_loja = addslashes(utf8_decode(trim($objPlanilha->getCellByColumnAndRow(2, $posLinha)->getValue())));
            $Coluna_D_codigo = addslashes(utf8_decode(trim($objPlanilha->getCellByColumnAndRow(3, $posLinha)->getValue())));

            // Se as colunas e-mail e nome estiverem preenchidas...
            if(($Coluna_A_email) && ($Coluna_B_nome)) {
                //
                // Obtem o token pelo e-mail
                $objTokenItem = DWX_obterTokenPeloEmail($colTokens, "2", $Coluna_A_email);
////                echo("<pre>"); print_r($objTokenItem); echo("</pre>");
                // Se o e-mail j� estiver cadastrado...
                if(isset($objTokenItem)) {
                    $qtdeRejeitadas++;
                    $linhasRejeitadas[] = sprintf("Linha nr. %04d | E-mail '%s' j� cadastrado! A linha foi ignorada.", $posLinha, $Coluna_A_email);
//                    $qtdeAlertas++;
//                    $linhasAlertas[] = sprintf("Linha nr. %04d | E-mail '%s' j� cadastrado! A linha n�o foi rejeitada. Revise a lista de usu�rios.", $posLinha, $Coluna_A_email);
                } else {
                    //
                    // Cadastrar a linha
                    //
                    $objToken = new clsDWClocalToken();
                    //
                    $objToken->IdCliente = $DW_id_cliente;
                    $objToken->IdAvaliacao = $DW_id_avaliacao;
                    $objToken->Nome = $Coluna_B_nome;
                    $objToken->Email = $Coluna_A_email;
                    $objToken->Codigo = $Coluna_D_codigo;
                    $objToken->Unidade = $Coluna_C_loja;
                    $objToken->Observacoes = "";
                    $objToken->CodigoPublicoAlvo = $DW_cd_publico_alvo;

                    $objRetorno_temp = DWX_criarToken(1, $objToken);
                    $objRetorno = json_decode($objRetorno_temp, false);

                    if ($objRetorno->ERRO == 0) {
                        $qtdeOk++;
                    } else {
                        $qtdeRejeitadas++;
                        $linhasRejeitadas[] = sprintf("Linha nr. %04d | Falhou na inclus�o!", $posLinha);
                    }

                    unset($objToken);
                }
            } else {
                $qtdeRejeitadas++;
                $linhasRejeitadas[] = sprintf("Linha nr. %04d | Conte�do inv�lido!", $posLinha);
            }
            
        }
        
        ?>
            Foram lidas <?php echo(number_format($qtdeLidos, 0, ",", ".")); ?> linhas da planilha.<br>
            <br>
            <?php
            if($qtdeOk > 0) {
                ?>
                Foram cadastradas <?php echo(number_format($qtdeOk, 0, ",", ".")); ?> linhas da planilha.<br>
                <?php
            } else {
                ?>
                Nenhuma linha foi cadastrada.<br>
                <?php
                echo("<br>");
            }
            if($qtdeAlertas > 0) {
                ?>
                Foram encontrado(s) <?php echo(number_format($qtdeAlertas, 0, ",", ".")); ?> linhas da planilha com e-mails j� cadastrados.<br>
                <br>
                Linhas que dever�o ser verificadas:<br>
                <br>
                <?php
                for($iPos=0;$iPos<=count($linhasAlertas);$iPos++) {
                    echo($linhasAlertas[$iPos]."<br>");
                }
                echo("<br>");
            }
            if($qtdeRejeitadas > 0) {
                ?>
                Foram rejeitadas <?php echo(number_format($qtdeRejeitadas, 0, ",", ".")); ?> linhas da planilha.<br>
                <br>
                Linhas rejeitadas:<br>
                <br>
                <?php
                for($iPos=0;$iPos<=count($linhasRejeitadas);$iPos++) {
                    echo($linhasRejeitadas[$iPos]."<br>");
                }
            } else {
                ?>
                Nenhuma linha foi rejeitada.<br>
                <?php
            }
            ?>
            
        <?php
    }
?>
        </td>
    </tr>
    <tr bgcolor="<?= $bgcolor6 ?>">
        <td>
                    <table border='0' width='100%'>
                    <tr>
                        <td>
                            <input type=button class="botao" value='Voltar' onclick="javascript:document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&idc=<?= $_REQUEST["idc"] ?>&filtroAv=<?= $_REQUEST["filtroAv"] ?>'">
                        </td>
                        <td style="text-align:right">
                            <input type=submit class="botao" name='enviar_planilha' value='Nova importa��o'><br>
                        </td>
                    </tr>
                </table>
            </td>
    </tr>
</table>
    <p style='text-align: center;'>
        <a href="#linkTOPO" style="color:darkgreen;">(&uarr; Topo da p�gina)</a>
    </p>
<?php
} else {
?>

    <table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0>
        <tr bgcolor="<?= $bgcolor4 ?>">
            <td style="text-align:center; font-weight: bold;">IMPORTA��O DE USU�RIOS</td>
        </tr>
        <tr bgcolor="<?= $bgcolor6 ?>">
            <td>

                <table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=0 cellpadding=3 border=0 width="100%">

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Cliente:</td>
                        <td style="font-weight: bold; vertical-align:middle;"> 
                            <?php
                            if($DEFWEB_master_user) {
                            ?>
                            <select id="id_cliente" name='id_cliente' class="combo"  onchange="javascript:carregarProjetos();">
                                <?php
                                echo($sHTMLidc);
                                ?>
                            </select>
                            <?php
                            } else {
                                echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
                            }
                            ?>
                        </td>
                    </tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">

                        <td style="font-weight: bold; vertical-align: middle; text-align: right; width: 20%;">Nome da Pesquisa:</td>

                        <td style="font-weight: bold;">
                            <select id="filtroAv" name='filtroAv' class="combo">
                                <?
                                //
                                // Obtem a lista de pesquisas liberadas para o cliente
                                $colPesquisas = DWX_obterPesquisas(1, $DEFWEB_cd_cliente, "");
                                $colPesquisasDecoded = json_decode($colPesquisas, false);
                                //
                                foreach($colPesquisasDecoded AS $objAvaliacaoItem) {
                                    //
                                    if($REQ_filtroAv == $objAvaliacaoItem->IdAvaliacao) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<OPTION VALUE='";
                                    $sHTML .= $objAvaliacaoItem->IdAvaliacao;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
                                    //
                                    echo($sHTML);
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="text-align:right; vertical-align:middle"><b>Arquivo:</b></td>
                        <td>
                            <input type="file" class="frmFiles" name="arquivo" id="arquivo">
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="text-align:right; vertical-align:middle"><b>Ignorar a primeira linha (cabe�alho):</b></td>
                        <td>
                            <input type="checkbox" class="simples" name="chkIgnorarLinha1" id="chkIgnorarLinha1" checked value="S">
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td align='right' style="vertical-align:middle"></td>
                        <td>

                            <?php
                            if($DW_POST) {
                                ?>
                            <a href="#linkIMPRESULT" style="color:darkgreen;"><b>IMPORTA��O CONCLU�DA!</b> Clique aqui para ver o resultado da importa��o.</a>
                            
                            <br>
                                <?php
                            }
                            ?>
                            
                            <br>

                            <span style="color:red; font-weight: bold;">Siga atentamente as instru��es abaixo:</span>

                            <br><br>

                            As colunas na planilha devem ser exatamente iguais ao apresentado no modelo, nesta mesma ordem: <br>
                            <br>
                            COLUNA A - E-mail (obrigat�rio)<br>
                            COLUNA B - Nome (obrigat�rio)<br>
                            COLUNA C - Identifica��o/C�digo da unidade/loja (opcional)<br>
                            COLUNA D - Identifica��o/C�digo (opcional)<br>
                            <br>
                            Indique se a primeira linha dever� ser ignorada na importa��o (cabe�alho/t�tulo das colunas).<br>
                            <br>
                            Caso o e-mail j� esteja cadastrado nesta pesquisa a linha ser� ignorada.<br>
                            <br>
                            Modelo de arquivo para importa��o: 
                            <a href="disc_usuarios/usuarios.xlsx"><span style="color:red; font-weight: bold;">usuarios.xlsx</span></a>

                            <br><br>
                        </td>
                    </tr>
                </table>


            </td>
        </tr>
        <tr bgcolor="<?= $bgcolor6 ?>">
            <td>

                <table border='0' width='100%'>
                    <tr>
                        <td>
                            <input type=button class="botao" value='Voltar' onclick="javascript:document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&idc=<?= $_REQUEST["idc"] ?>&filtroAv=<?= $_REQUEST["filtroAv"] ?>'">
                        </td>
                        <td style="text-align:right">
                            <input type=submit class="botao" name='enviar_planilha' value='Enviar'>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

<?php
}
?>

</form>

<br><br>

