<?php
/**
 * @author Arnaldo Beserra (RP Consultoria)
 * Data: 16/06/2017
 * Mecanismo de Busca - Cliente - Disc2Work
 *
 */
//---------------------------------------------------------------
//
// Configuracao do cliente do Disc2Work
//
// Funciona somente se estiver no AWS-Master
require("/home/users/__discx/disc-lib/libDiscXLocal.php");
//
//---------------------------------------------------------------
//
// enviar uma mensagem de excecao ocorrida
function throw_exception($message){
	$arr = array("ERRO" => 1, "MENSAGEM" => $message);
	$arr = utf8_encode_all($arr);
	echo $json_info = json_encode($arr);
	die();
}

// returns $dat encoded to UTF8
function utf8_encode_all($dat){ 
  if (is_string($dat)) return utf8_encode($dat); 
  if (!is_array($dat)) return $dat; 
  $ret = array(); 
  foreach($dat as $i=>$d) $ret[$i] = utf8_encode_all($d); 
  return $ret; 
}

// returns $dat decoded from UTF8
function utf8_decode_all($dat){ 
  if (is_string($dat)) return utf8_decode($dat); 
  if (!is_array($dat)) return $dat; 
  $ret = array(); 
  foreach($dat as $i=>$d) $ret[$i] = utf8_decode_all($d); 
  return $ret; 
}

// Variaveis
$DEFWEB_master_user = 0;
$DEFWEB_cd_cliente  = "";
$DEFWEB_id_cliente  = 0;
//
$TipoConteudo   = "";
$idCliente      = 0;
$selPesquisa	= array();
$selCampos		= array();
$itensPorPag	= 20;
$nrPage			= 1;
//
$Start_From		= 0;
//
//
//------------------------------------------------------------------------------
// Na chamada os parametros estao sendo enviados como json
$postdata = file_get_contents("php://input");
if($postdata){
    $request = json_decode($postdata);
	//
	foreach ($request as $field) {
		//
		switch ( $field->name ){
			case "tpReq":
				$TipoConteudo = $field->value;
				break;
		
			case "idCliente":
				$idCliente = $field->value;
				break;
				
			case "selPesquisa":
				$selPesquisa = $field->value;
				break;
				
			case "selCampos":
				$selCampos = $field->value;
				break;
			
			case "nrPg":
				$nrPage = $field->value;
				break;
		
			case "itensPorPg":
				$itensPorPag = $field->value;
				break;

			case "mstkrz":
				$DEFWEB_master_user = $field->value;
				break;

			case "cdc":
				$DEFWEB_cd_cliente = $field->value;
				break;

			case "idc":
				$DEFWEB_id_cliente = $field->value;
				break;
		}
	}
}
//
//------------------------------------------------------------------------------
// TRATA A REQUISICAO
if ( strlen($TipoConteudo) < 4 ){
	// PARA EVITAR ERRO NA FUNCAO SUBSTR
	$TipoConteudo .= "    ";
}
switch ( strtoupper(substr($TipoConteudo,2,strlen($TipoConteudo)-4)) ) {
    case "LSTCL":
		// LISTA CLIENTES
		$clientes = array();
		$clientes["ERROR"] = array("ERRO" => 0, "MENSAGEM" => "");
		$clientes["DATA"]  = array();
		//
		if ( $DEFWEB_master_user ){
			$colClientesTmp = DISCX_listarClientes();
		} else {
			$colClientesTmp = DISCX_consultarCliente("", $DEFWEB_cd_cliente);
		}
		//
		$colClientes = json_decode($colClientesTmp);
		//
		foreach ($colClientes AS $objCliente) {
			$clientes["DATA"][] = $objCliente;
		}
		//
		$clientes = utf8_encode_all($clientes);
		echo $json_info = json_encode($clientes);
		break;

    case "LSTAV":
		// LISTA AVALIACOES
		$avaliacao = array();
		if ( $idCliente == 0 ){
			$avaliacao["ERROR"] = array("ERRO" => 1, "MENSAGEM" => "Cliente n�o informado!");
		} else {
			$avaliacao["ERROR"] = array("ERRO" => 0, "MENSAGEM" => "");
			$avaliacao["DATA"]  = array();
			//
			$objPesquisasTmp = DWX_obterPesquisas("1", "", $idCliente);
			//
			$objPesquisas = json_decode($objPesquisasTmp);
			//
			foreach ($objPesquisas AS $objPesquisa) {
				$avaliacao["DATA"][] = $objPesquisa;
			}
		}
		//
		$avaliacao = utf8_encode_all($avaliacao);
		echo $json_info = json_encode($avaliacao);
		break;

    case "LSTCP":
		// LISTA OPCOES DE CAMPOS
		$campos = array();
		$campos["ERROR"] = array("ERRO" => 0, "MENSAGEM" => "");
		$campos["DATA"]  = array();
		//
		$objCamposTmp = DISCX_listarCamposEntrevistado($selPesquisa);
		//
		$objCampos = json_decode($objCamposTmp);
		//
		foreach ($objCampos AS $objCampo) {
			$campos["DATA"][] = $objCampo;
		}		
		//
		$campos = utf8_encode_all($campos);
		echo $json_info = json_encode($campos);
		break;

    case "LSTENTR":
		// BUSCA ENTREVISTADOS
		$entrevistados = array();
		//
		// Validando os parametros
		$paramOk = true;
		if ( $idCliente == 0 ){
			$entrevistados["ERROR"] = array("ERRO" => 1, "MENSAGEM" => "Cliente n�o informado!");
			$paramOk = false;
		}
		if ( !is_array($selPesquisa) ){
			$entrevistados["ERROR"] = array("ERRO" => 1, "MENSAGEM" => "Par�metro selPesquisa inv�lido!");
			$paramOk = false;
		} else {
			if ( count($selPesquisa) <= 0 ){
				$entrevistados["ERROR"] = array("ERRO" => 1, "MENSAGEM" => "Pesquisa n�o informada!");
				$paramOk = false;
			}
		}
		//
		if ( $paramOk ) {
			$entrevistados["ERROR"] = array("ERRO" => 0, "MENSAGEM" => "");
			$entrevistados["DATA"]  = array();
			//
			$objEntrevistadosTmp = DISCX_buscarEntrevistados($idCliente, $selPesquisa, $selCampos, $itensPorPag, $nrPage);
			//
			$objEntrevistados = json_decode($objEntrevistadosTmp);
			//
			foreach ($objEntrevistados AS $objEntrevistado) {
				$entrevistados["DATA"][] = $objEntrevistado;
			}
		}
		//
		$entrevistados = utf8_encode_all($entrevistados);
		echo $json_info = json_encode($entrevistados);
		break;
		
    default: 
			throw_exception("N�o foi poss�vel atender a solicita��o. Entre em contato com o suporte.");
			break;
}
?>