(function($){
	'use strict';
	//
	var obClientes  	= [];
	var obPesquisas 	= [];
	var obCampos    	= [];
	var obEntrevistados = [];
	//
	$( document ).ready( function(){
		//
		obterClientes();
		obterCampos();
		//
		$('button[id=btnIncCondicao]').bind('click', function (event) {
			event.preventDefault();
			incCondicao();
		});
		//
		$('button[id=btnLimpar]').bind('click', function (event) {
			event.preventDefault();
			$('#msg_cliente').empty();
			$('#msg_pesquisa').empty();
			$('div[id=tbCondicoes]').empty();
		});
		//
		$('button[id=btnVoltar]').bind('click', function (event) {
			event.preventDefault();
			$('input[name=nrPg]').val(1);
			$('div[id=dvCriterios]').show();
			$('div[id=dvEntrevistados]').hide();
		});
		//
		$('button[id=btnEnviar]').bind('click', function (event) {
			event.preventDefault();
			buscarEntrevistados();
		});
		//
		$('select[id=itensPorPagina]').on('change', function() {
			changeQtPage();
		});
	});
	//
	// OBTER A LISTA DE CLIENTES
	function obterClientes(){
		//
		var Local  = "div[id=dvCliente]";
		var Params = $("form[id=frm]").serializeArray();
		var sHtml  = "";
		//
		Params.push({name: "tpReq", value: "gtlstclin"});
		//
		Params = JSON.stringify(Params);
		//
		$(Local).empty().html(Loading());
		var request = $.ajax({
			url: "disc_bucar_ent/disc_bucar_ent_data.php",
			type: 'post',
			data: Params
		});
		//
		request.done(function (response, textStatus, jqXHR){
			//
			obClientes = JSON.parse(response);
			//
			if ( obClientes.ERROR.ERRO == 0 ){
				if ( obClientes.DATA.length > 0 ){
					sHtml = "<label for=\"selCliente\">Cliente:</label>";
					sHtml += "<div>";
					sHtml += "<select id=\"selCliente\" name=\"selCliente\" class=\"form-control\">";
					$.each(obClientes.DATA, function(i, item){
						sHtml += "<option value=\"" + item.Id + "\">" +  item.Nome + "</option>";
					});
					sHtml += "</select>";
					sHtml += "</div>";
				} else {
					sHtml = "<p class=\"text-danger\">N�o encontrou nenhum cliente!</p>";
				}
			
			} else if ( obClientes.ERROR.ERRO == 1 ){
				sHtml = "<p class=\"text-danger\">" + obClientes.MENSAGEM + "</p>";
			
			} else {
				sHtml = "<p class=\"text-danger\">N�o foi poss�vel a recupera��o do cliente.</p>";
			
			}
			//
			$(Local).empty().html(sHtml);
			//
			configPesquisa();
			//
			$('select[id=selCliente]').on('change', function() {
				configPesquisa();
				obterCampos();
			});
		});
		//
		request.fail(function (jqXHR, textStatus, errorThrown){
			//
			$(Local).empty().html("<p class=\"text-danger\">Falhou a recupera��o do cliente!</p>");
			//
		});
	}
	//
	// OBTER A LISTA DE PESQUISAS
	function obterPesquisas(clienteId){
		//
		var Local  = "div[id=dvPesquisa]";
		var Params = $("form[id=frm]").serializeArray();
		var sHtml  = "";
		//
		Params.push({name: "tpReq", value: "gtlstavin"});
		Params.push({name: "idCliente", value: clienteId});
		//
		Params = JSON.stringify(Params);
		//
		$(Local).empty().html(Loading());
		var request = $.ajax({
			url: "disc_bucar_ent/disc_bucar_ent_data.php",
			type: 'post',
			data: Params
		});
		//
		request.done(function (response, textStatus, jqXHR){
			//
			obPesquisas = JSON.parse(response);
			//
			if ( obPesquisas.ERROR.ERRO == 0 ){
				if ( obPesquisas.DATA.length > 0 ){
					sHtml = "";
					$.each(obPesquisas.DATA, function(i, item){
						sHtml += "<label for=\"selPesquisa_" + item.IdAvaliacao + "\">";
						sHtml += "<input type=\"checkbox\" name=\"selPesquisas[]\" id=\"selPesquisa_" + item.IdAvaliacao + "\" value=\"" + item.IdAvaliacao + "\">";
						sHtml += item.Titulo;
						sHtml += "</label>";
					});
				} else {
					sHtml = "<p class=\"text-danger\">N�o encontrou nenhuma pesquisas!</p>";
				}
			
			} else if ( obPesquisas.ERROR.ERRO == 1 ){
				sHtml = "<p class=\"text-danger\">" + obPesquisas.MENSAGEM + "</p>";
			
			} else {
				sHtml = "<p class=\"text-danger\">N�o foi poss�vel a recupera��o das pesquisas.</p>";
			
			}
			//
			$(Local).empty().html(sHtml);
			//
			$('input[id^=selPesquisa_]').on('change', function() {
				obterCampos();
			});
		});
		//
		request.fail(function (jqXHR, textStatus, errorThrown){
			//
			$(Local).empty().html("<p class=\"text-danger\">Falhou a recupera��o das pesquisas!</p>");
			//
		});
	}
	//
	function configPesquisa(){
		var clienteAtual = $('select[id=selCliente]');
		if ( clienteAtual.val() !== undefined ){
			obterPesquisas(clienteAtual.val());
		}
	}
	//
	// OBTER A LISTA DE CAMPOS
	function obterCampos(){
		//
		var Local  = "div[id=dvCampos]";
		var Params = $("form[id=frm]").serializeArray();
		var sHtml  = "";
		//
		// Obter as pesquisas
		var selPesquisa = $('.selectBox label input[id^=selPesquisa_]:checked');
		var selPesquisas = [];
		$(selPesquisa).filter( function(){
			if( $(this).is(":checked") ) {
				var id = $(this).val();
				$.each(obPesquisas.DATA, function( index, obPesquisa ) {
					if ( id == obPesquisa.IdAvaliacao ){
						selPesquisas.push(obPesquisa);
					}
				});
			}
		});
		//
		Params.push({name: "tpReq", value: "gtlstcpin"});
		if ( selPesquisas.length > 0 ){
			Params.push({name: "selPesquisa", value: selPesquisas});
		}
		//
		Params = JSON.stringify(Params);
		//
		var lstCampo = $('select[id=selCampo]').val();
		if ( lstCampo === undefined ){
			$(Local).empty().html(Loading());
		}
		//
		var request = $.ajax({
			url: "disc_bucar_ent/disc_bucar_ent_data.php",
			type: 'post',
			data: Params
		});
		//
		request.done(function (response, textStatus, jqXHR){
			//
			obCampos = JSON.parse(response);
			//
			if ( obCampos.ERROR.ERRO == 0 ){
				if ( obCampos.DATA.length > 0 ){
					sHtml = "<label for=\"selCampo\">Entrevistado:</label>";
					sHtml += "<div>";
					sHtml += "<select id=\"selCampo\" name=\"selCampo\" class=\"form-control\">";
					$.each(obCampos.DATA, function(i, item){
						sHtml += "<option value=\"" + item.Id + "\">" +  item.Campo + "</option>";
					});
					sHtml += "</select>";
					sHtml += "</div>";

				} else {
					sHtml = "<p class=\"text-danger\">N�o encontrou nenhum campo!</p>";

				}
			
			} else if ( obCampos.ERROR.ERRO == 1 ){
				sHtml = "<p class=\"text-danger\">" + obCampos.MENSAGEM + "</p>";

			} else {
				sHtml = "<p class=\"text-danger\">N�o foi poss�vel a recupera��o dos campos.</p>";

			}
			//
			$(Local).empty().html(sHtml);
			//
			configValorCampo();
			//
			$('select[id=selCampo]').on('change', function() {
				configValorCampo();
			});
		});
		//
		request.fail(function (jqXHR, textStatus, errorThrown){
			//
			$(Local).empty().html("<p class=\"text-danger\">Falhou a recupera��o dos campos!</p>");
			//
		});
	}
	//
	//
	function configValorCampo(){
		var campoAtual = $('select[id=selCampo]');
		if ( campoAtual.val() !== undefined ){
			if ( obCampos.DATA.length > 0 ){
				$.each(obCampos.DATA, function(i, item){
					if ( item.Id == campoAtual.val() ){
						$('input[id=valor]').attr("maxlength", item.Tamanho);
					}
				});
			}
		}
	}
	//
	function Loading(){
		var sLoaging = "<img src=\"disc_config/img/dwloading.gif\">";
		return sLoaging;
	}
	//
	// INCLUIR CONDICAO NA TABELA
	function incCondicao(){
		var campo = $('select[id=selCampo] option:selected');
		var cond  = $('select[id=selCondicao]');
		var valor = $('input[id=valor]');
		//
		var flDadosValidos = true;
		//
		$('#msg_campo').empty();
		//
		if ( campo.val() == "" || campo.val() == null ){
			$('#msg_campo').html("Campo n�o selecionado!");
			campo.focus();
			flDadosValidos = false;
		}
		else if ( cond.val() == "" || cond.val() == null ){
			$('#msg_campo').html("Condi��o n�o selecionada!");
			cond.focus();
			flDadosValidos = false;
		}
		else if ( valor.val() == "" ){
			$('#msg_campo').html("Valor n�o informado!");
			valor.focus();
			flDadosValidos = false;
		}
		//
		var campoAtual = $('select[id=selCampo]');
		if ( campoAtual.val() !== undefined ){
			if ( obCampos.DATA.length > 0 ){
				$.each(obCampos.DATA, function(i, item){
					if ( item.Id == campoAtual.val() && item.Validacao != "" ){
						var patt = new RegExp(item.Validacao);
						if ( !patt.test(valor.val()) ) {
							$('input[id=valor]').focus();
							$('#msg_campo').html("Valor inv�lido! " + item.Descricao);
							flDadosValidos = false;
						}
					}
				});
			}
		} else {
			flDadosValidos = false;
		}
		//
		if ( flDadosValidos ){
			var sHTML = "";
			//
			var tbCond = $('div[id=tbCondicoes]');
			var tbl = $('tbody[id=tb_data]');
			var idx = 0;
			//
			if ( tbl.length === 0 ){
				sHTML  = "<table class=\"table table-bordered table-striped\">";
				sHTML += "<thead><tr>";
				sHTML += "<th style=\"display:none;\">CampoID</th>";
				sHTML += "<th>Campo</th>";
				sHTML += "<th>Condi��o</th>";
				sHTML += "<th>Valor</th>";
				sHTML += "<th>Remover</th>";
				sHTML += "</tr></thead>";
				sHTML += "<tbody id=\"tb_data\">";
				//
				sHTML += "<tr id=\"trCond_" + idx + "\">";
				sHTML += "<td style=\"display:none;\">" + campo.val() + "</td>";
				sHTML += "<td>" + campo.text() + "</td>";
				sHTML += "<td>" + cond.val() + "</td>";
				sHTML += "<td>" + valor.val().toUpperCase() + "</td>";
				sHTML += "<td>"
				sHTML += "<button id=\"btnExclCondicao_" + idx + "\" class=\"btn btn-danger btn-sm\"><span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span></button>";
				sHTML += "</td>";
				sHTML += "</tr>";
				//
				sHTML += "</tbody></table>";
				tbCond.html(sHTML);
				//
			} else {
				//
				idx = $('tbody[id=tb_data] tr').length;
				//
				sHTML = "<tr id=\"trCond_" + idx + "\">";
				sHTML += "<td style=\"display:none;\">" + campo.val() + "</td>";
				sHTML += "<td>" + campo.text() + "</td>";
				sHTML += "<td>" + cond.val() + "</td>";
				sHTML += "<td>" + valor.val().toUpperCase() + "</td>";
				sHTML += "<td>";
				sHTML += "<button id=\"btnExclCondicao_" + idx + "\" class=\"btn btn-danger btn-sm\"><span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span></button>";
				sHTML += "</td>";
				sHTML += "</tr>";
				tbl.append(sHTML);
			}
			//
			// Binding event click to remove row
			$('button[id^=btnExclCondicao_]').on('click', function(){
				$(this).closest('tr').remove();
			});
			//
			orderTbl();
			//
			valor.val("");
		}
		//
		return flDadosValidos;
		//
	}
	//
	// ORDENAR A TABELA PELO ID DO CAMPO
	function orderTbl(){
		var rows = $('tbody[id=tb_data] > tr').get();
		//
		rows.sort(function(a, b) {
			var A = getVal(a);
			var B = getVal(b);
			if(A < B) {
				return -1;
			}
			if(A > B) {
				return 1;
			}
			return 0;
		});
		//
		function getVal(tr){
			var td  = $('td:eq(0)', tr);
			var tx  = td.text();
			return tx;
		}
		//
		$.each(rows, function(index, row) {
			$('tbody[id=tb_data]').append(row);
		});
	}
	//
	//
	// OBTER OS CAMPOS DA TABELA
	function obterCamposTbl(){
		var rows = $('tbody[id=tb_data] > tr').get();
		//
		var tblCampos = [];
		//
		$.each(rows, function(index, row) {
			var tdCampoId = $('td:eq(0)', row);
			var tdCond    = $('td:eq(2)', row);
			var tdValor   = $('td:eq(3)', row);
			//
			var campoId  = tdCampoId.text();
			var condicao = tdCond.text();
			var valor    = tdValor.text();
			//
			$.each(obCampos.DATA, function(i, obCampo) {
				if ( campoId == obCampo.Id ) {
					var campo = obCampo;
					//
					campo['Condicao'] = condicao;
					campo['Valor'] = valor;
					//
					tblCampos.push(campo);
				}
			});
			
		});
		//
		return tblCampos;
	}
	//
	// BUSCAR ENTREVISTADOS
	function buscarEntrevistados(){
		//
		var flDadosValidos = true;
		//
		$('#msg_cliente').empty();
		$('#msg_pesquisa').empty();
		//
		var clienteId   = $('select[id=selCliente]').val();
		if ( clienteId === undefined ){
			$('#msg_cliente').html("Cliente n�o selecionado!");
			flDadosValidos = false;
		}
		//
		if ( flDadosValidos ){
			var selPesquisa = $('.selectBox label input[id^=selPesquisa_]:checked');
			if ( selPesquisa.length == 0 ){
				$('#msg_pesquisa').html("Nenhuma pesquisa foi selecionada!");
				flDadosValidos = false;
			}
		}
		//
		if ( flDadosValidos ){
			$('div[id=dvCriterios]').hide();
			$('div[id=dvEntrevistados]').show();
			//
			//
			var Local  = "div[id=tbEntrevistados]";
			//
			// Quantidade de itens por pagina
			var itensPorPag = $('select[id=itensPorPagina]').val();
			// Pagina solicitada
			var nrPag = $('input[name=nrPg]').val();
			//
			// Obter as pesquisas
			var selPesquisas = [];
			$(selPesquisa).filter( function(){
				if( $(this).is(":checked") ) {
					var id = $(this).val();
					$.each(obPesquisas.DATA, function( index, obPesquisa ) {
						if ( id == obPesquisa.IdAvaliacao ){
							selPesquisas.push(obPesquisa);
						}
					});
				}
			});
			//
			// Obter os campos do entrevistados
			var selCampos = obterCamposTbl();
			//
			var Params = $("form[id=frm]").serializeArray();
			var sHtml  = "";
			//
			Params.push({name: "tpReq", value: "gtlstentrin"});
			Params.push({name: "idCliente", value: clienteId});
			Params.push({name: "selPesquisa", value: selPesquisas});
			Params.push({name: "selCampos", value: selCampos});
			Params.push({name: "itensPorPg", value: itensPorPag});
			Params.push({name: "nrPg", value: nrPag});
			//
			Params = JSON.stringify(Params);
			//
			$(Local).empty().html(Loading());
			var request = $.ajax({
				url: "disc_bucar_ent/disc_bucar_ent_data.php",
				type: 'post',
				data: Params
			});
			//
			request.done(function (response, textStatus, jqXHR){
				//
				var Total = 0;
				//
				obEntrevistados = JSON.parse(response);
				//
				if ( obEntrevistados.ERROR.ERRO == 0 ){
					if ( obEntrevistados.DATA.length > 0 ){
						//
						sHtml = "<table class=\"table table-bordered table-striped\">";
						sHtml += "<thead><tr>";
						sHtml += "<th>Pesquisa</th>";
						sHtml += "<th>Entrevistado</th>";
						sHtml += "<th>Resultado</th>";
						sHtml += "<th>T�rmino</th>";
						sHtml += "</tr></thead>";
						sHtml += "<tbody id=\"tb_datEntr\">";
						//
						$.each(obEntrevistados.DATA, function(i, item){
							if ( Total == 0 ){
								Total = item.Total;
							}
							sHtml += "<tr id=\"trEntr_" + item.IdToken + "\">";
							sHtml += "<td>" + item.Titulo + "</td>";
							sHtml += "<td>" + item.NomeEntrevistado + "</td>";
							sHtml += "<td>" + formatResultHTML(item.ResultadoFatorDisc) + "</td>";
							sHtml += "<td>" + item.DtTermino + "</td>";
							sHtml += "</tr>";
						});
						//
						sHtml += "</tbody></table>";
						//
						if ( Total > 0 ){
							if ( Total == 1 ){
								sHtml += "<p><b>Foi encontrado " + Total + " registro</b></p>";
							} else {
								sHtml += "<p><b>Foram encontrados " + Total + " registros</b></p>";
							}
						}
						//
					} else {
						sHtml = "<p class=\"text-danger\">N�o encontrou nenhum entrevistado!</p>";
					}
			
				} else if ( obEntrevistados.ERROR.ERRO == 1 ){
					sHtml = "<p class=\"text-danger\">" + obEntrevistados.MENSAGEM + "</p>";
			
				} else {
					sHtml = "<p class=\"text-danger\">N�o foi poss�vel a recupera��o dos entrevistados.</p>";
			
				}
				//
				$(Local).empty().html(sHtml);
				//
				if ( Total > 20 ){
					// Opcoes de tamanhos de pagina
					$('select[id=itensPorPagina] option').each( function(){
						if ( eval($(this).val()) <= Total ){
							$(this).show();
						} else {
							$(this).hide();
						}
					});
					//
					// Navegacao das paginas - maximo 10 botoes na barra de navegacao
					var pgs = Math.ceil(Total / itensPorPag);
					var nav = "";
					var navBar = ((pgs < 10) || (pgs > 10 && nrPag <= 10));
					var navBarCount = 0;
					var navBarFirst = 0;
					//
					nav  = "<li>"
					nav += "<a id=\"pp_1\" href=\"#\" aria-label=\"Previous\">";
					nav += "<span aria-hidden=\"true\">&laquo;</span>";
					nav += "</a>";
					nav += "</li>";
					//
					for (var p = 0; p < pgs; p++) {
						if ( navBarFirst == 0 ){
							navBarFirst = eval(p+1);
						}
						if ( (nrPag >= navBarFirst) && (nrPag <= eval(navBarFirst+9)) ){
							navBar = true;
						} else {
							navBar = false;
						}
						//
						if ( navBar && navBarFirst == eval(p+1) && navBarFirst > 10 ){
							nav += "<li>";
							nav += "<a id=\"pg_" + eval(p-9) + "\" href=\"#\">...</a>";
							nav += "</li>";
						}
						//
						nav += "<li";
						if ( eval(p+1) == nrPag ){
							nav += " class=\"active\"";
						}
						if ( !navBar ){
							nav += " style=\"display:none;\"";
						} else {
							navBarCount++;
						}
						nav += ">";
						nav += "<a id=\"pg_" + eval(p+1) + "\" href=\"#\">" + eval(p+1) + "</a>";
						nav += "</li>";
						if ( (navBar && (navBarCount == 10)) && (eval(p+2) < pgs) ){
							navBar = false;
							nav += "<li>";
							nav += "<a id=\"pg_" + eval(p+2) + "\" href=\"#\">...</a>";
							nav += "</li>";
						}
						if ( (eval(p+1) % 10) == 0 ){
							navBarFirst = 0;
						}
					}
					//
					nav += "<li>"
					nav += "<a id=\"pu_" + pgs + "\" href=\"#\" aria-label=\"Next\">";
					nav += "<span aria-hidden=\"true\">&raquo;</span>";
					nav += "</a>";
					nav += "</li>";
					//
					$('ul.pagination').empty().append(nav);
					//
					$('ul.pagination li a').on('click', function() {
						var refId = $(this).attr('id');
						var refSplit = refId.split('_');
						var pg = refSplit[1];
						//
						showPage(pg);
					});
					//
					$('nav[id=nvEntrevistados]').show();
				} else {
					$('nav[id=nvEntrevistados]').hide();
				}
			});
			//
			request.fail(function (jqXHR, textStatus, errorThrown){
				//
				$(Local).empty().html("<p class=\"text-danger\">Falhou a recupera��o dos entrevistados!</p>");
				//
			});
		}
	}
	//
	function formatResultHTML(sFatorDisc){
		var result_fmt = "";
		//var nFator = 0;
		for (var nFator = 0; nFator < sFatorDisc.length; nFator++) {
			result_fmt += "<span";
			if ( nFator == 0 ) {
				result_fmt += " class='disc_letter_main'";
			} else {
				result_fmt += " class='disc_letter_norm'";
			}
			result_fmt += ">";
			result_fmt += sFatorDisc.substr(nFator, 1);
			result_fmt += "</span>";
		}
		return result_fmt;
	}
	//
	//
	function changeQtPage(){
		$('input[name=nrPg]').val(1);
		buscarEntrevistados();
	}
	//
	function showPage(page){
		//
		$('input[name=nrPg]').val(page);
		//
		buscarEntrevistados();
		//
		return false;
		//
	}
	//
})(jQuery)