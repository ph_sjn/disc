<?php
$acesso_administrativo = checaPermissao($module, "A", $user_access);
//
$GRAFICO_EXIBIR = 1;
//
?>
		<?// Estilos ?>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/normalize.css" type="text/css" rel="stylesheet">
		<link href="css/disc2work.css" type="text/css" rel="stylesheet">
		
		<?// JQuery ?>
		<script src="js/jquery-3.2.0.min.js"></script>
				
		<?// Bootstrap - dependencia ?>
		<script src="js/bootstrap.min.js"></script>

		<div class="container-fluid">

			<form action='index.php' method='post' id="frm" name='frm'>

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8">
						<h2>Busca Entrevistado(s)</h2>
					</div>
				</div>
				<?php // CRITERIOS PARA A BUSCA ?>
				<div id="dvCriterios">
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8">
						
							<div class="panel panel-default center-block">
								<div class="panel-body">
								
									<div class="form-inline">
										<div id="dvCliente" class="form-group form-group-sm">
										</div>
										<p id="msg_cliente" class="text-danger"></p>
									</div>
									<br>
									<label for="selPesquisas">Pesquisas:</label>
									<div class="mutliSelect">
										<div id="dvPesquisa" class="selectBox">
										</div>
										<p id="msg_pesquisa" class="text-danger"></p>
									</div>
									<br>
									<div class="form-inline">
										<div id="dvCampos" class="form-group form-group-sm">
										</div>
										<div class="form-group form-group-sm">
											<label for="selCondicao">Condi��o:</label>
											<div>
												<select id="selCondicao" name="selCondicao" class="form-control">
													<option value="=">=</option>
													<option value=">">></option>
													<option value="<"><</option>
													<option value=">=">>=</option>
													<option value="<="><=</option>
													<option value="contem">Cont�m</option>
													<option value="ncontem">N�o cont�m</option>
												</select>
											</div>
										</div>
										<div class="form-group form-group-sm">
											<label for="valor">Valor:</label>
											<div>
												<input type="text" class="form-control" name="valor" id="valor" maxlength="4" placeholder="Valor" value="">
											</div>
										</div>
										<div class="form-group form-group-sm">
											<label>&nbsp;</label>
											<div>
												<button id="btnIncCondicao" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Incluir</button>
											</div>
										</div>
										<p id="msg_campo" class="text-danger"></p>
									</div>
								
								</div>
							</div>
						
						</div>
					</div>
					<div class="row">
						<div id="tbCondicoes" class="col-xs-12 col-sm-8 col-md-8">
						</div>
						<p id="msg_tbCondicoes" class="text-danger"></p>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8">
							<button id="btnEnviar" class="btn btn-primary btn-sm">Enviar</button>
							<button id="btnLimpar" class="btn btn-default btn-sm">Limpar</button>
						</div>
					</div>
				</div>
				<?php // RESULTADO DA BUSCA ?>
				<div id="dvEntrevistados" style="display:none;">
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8">
							<button id="btnVoltar" class="btn btn-primary btn-sm">Voltar</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8">
							<div id="tbEntrevistados">
							</div>
							<nav id="nvEntrevistados" aria-label="Page navigation" style="display: none;">
								<ul class="pagination pagination-sm">
								</ul>
								<ul class="nav navbar-nav navbar-right">
									<div class="form-group form-group-sm">
										<label for="itensPorPagina">Itens por p�gina:</label>
										<div>
											<select id="itensPorPagina" name="itensPorPagina" class="form-control">
												<option value="20">20</option>
												<option value="50" style="display: none;">50</option>
												<option value="100" style="display: none;">100</option>
												<option value="500" style="display: none;">500</option>
											</select>
										</div>
									</div>
								</ul>
							</nav>
							<p id="msg_Entrevistados" class="text-danger"></p>
						</div>
					</div>
				</div>
				
				<input type="hidden" name="nrPg"   value="1">
				<input type="hidden" name="module" value="<?= $module ?>">
				<input type="hidden" name="mode"   value="view">
				<input type="hidden" name="mstkrz" value="<?= $DEFWEB_master_user ?>">
				<input type="hidden" name="cdc"    value="<?= $DEFWEB_cd_cliente ?>">
				<input type="hidden" name="idc"    value="<?= $DEFWEB_id_cliente ?>">
				<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />
			</form>
			<script src="disc_bucar_ent/js/disc_bucar_ent.js"></script>
		</div>
