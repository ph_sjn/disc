<?
session_name("ExtraNet_disc2work_client");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
include_once("../lang/br.inc.php"); 
include_once($path_pre. "lib/checapermissao.php");

$acesso_administrativo = checaPermissao($module,"A",$user_access);
$acesso_normal = checaPermissao($module,"N",$user_access);
$existe = false;

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../discx/libdisc_franqueador.php");

$DW_id_cliente = 0;
$DW_id_avaliacao = $_REQUEST["id"];
$DW_id_franqueador = $_REQUEST["idfq"];

//echo("\$_REQUEST<pre>");
//print_r($_REQUEST);
//echo("</pre>");

$objDEFWEBmaster = new DEFWEBmasterFranqueador();

//echo("\$objDEFWEBmaster:<pre>");
//print_r($objDEFWEBmaster);
//echo("</pre>");

$objRespostasFranqueador = $objDEFWEBmaster->obterRespostasFranqueador($DW_id_avaliacao, $DW_id_franqueador);

//echo("<pre>");
//print_r($objRespostasFranqueador);
//echo("</pre>");

$objItem = new DEFWEBrespostaItem();
$objValor = new DEFWEBrespostaValor();

$sQuebra_CodigoTema = "";
$sQuebra_CodigoAgrupamento = "";

echo("<center>");
echo("<table width=80%>");
echo("<tr>");
echo("<td>");

$lngPos=0;

foreach ($objRespostasFranqueador As $objItem) {
    
    if(($objItem->CodigoTema != $sQuebra_CodigoTema)||($objItem->CodigoAgrupamento != $sQuebra_CodigoAgrupamento)) {
        $sQuebra_CodigoTema = $objItem->CodigoTema;
        $sQuebra_CodigoAgrupamento = $objItem->CodigoAgrupamento;
        //
        $sTitulo = $objItem->DescricaoAgrupamento . "-" . $objItem->DescricaoTema;
        //
        echo("<br><b>".$sTitulo."</b><br>");
    }
    //
    $lngPos++;
    //
    $sNumero = str_pad($lngPos, 3, "0", STR_PAD_LEFT);
    //
    $sItem = $objItem->DescricaoItem;
    //
    echo("<br />" . $sNumero . "-" . $sItem . "<br /><br />");
    //
    echo("<table cellpadding=5px cellspacing=0>");
    //
    echo("<tr>");
    //
    echo("<td bgcolor='#EEEEEE' style='min-width:50px; text-align:left; border:solid 1px gray;'>");
    if($objItem->QuantidadeRespostas > 0 ) {
//        $bPrimeiraLinha = TRUE;
//        foreach ($objItem->colRespostas As $objValor) {
//            if(!$bPrimeiraLinha) {
//                echo("<br>");
//            }
//            $bPrimeiraLinha = FALSE;
//            echo("<b>");
//            echo($objValor->Resposta . "-" . $objValor->DescricaoOpcaoItem);
//            echo("</b>");
//        }
        //
?>
<table cellpadding="5px" cellspacing="0">
<?php
        //
        foreach ($objItem->colRespostas As $objValor) {
?>
    <tr>
        <td><?= $objValor->DescricaoOpcaoItem ?></td>
        <td>[&nbsp;<?= $objValor->Resposta ?>&nbsp;]</td>
    </tr>
<?php
        }
?>
</table>
<?php
        //
    } else {
        echo("<font color=\"red\">Ainda n�o respondeu.</font>");
    }
    //
    echo("</td>");
    //
    echo("</tr>");
    //
    echo("</table>");
}
echo("</td>");
echo("</tr>");
echo("</table>");
echo("</center>");
