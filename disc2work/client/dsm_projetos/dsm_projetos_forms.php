<script language="JavaScript" src="dsm_projetos/js/ajax.js"></script>
<script language="JavaScript" src="lib/calendario.js"></script>

<?php
$acesso_administrativo = checaPermissao($module, "A", $user_access);

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../survey/lib/libdisc.php");

$DW_id_avaliacao = $_REQUEST['id'];
$DW_filtroCliente = $_REQUEST['filtroCliente'];
//
$objDEFWEBavaliacao = new DEFWEBavaliacao($DW_id_avaliacao);
$objDEFWEBavaliacao->obterFormularios();
//
    //
//    if(strtolower($_SESSION['user_kurz']) == "rp-marcio") {
//        echo("<hr>");
//        echo("<pre>");
//        print_r($objDEFWEBavaliacao);
//        echo("</pre>");
//        echo("<hr>");
//        
//    }
//
$DW_id_projeto = $objDEFWEBavaliacao->IdProjeto;
$DW_id_cliente = $objDEFWEBavaliacao->IdCliente;
$DW_cd_sit_avaliacao = $objDEFWEBavaliacao->CodigoSituacao;

//echo("<hr>");
//echo("DW_id_avaliacao=".$DW_id_avaliacao."<br>");
//echo("DW_id_cliente=".$DW_id_cliente."<br>");
//echo("DW_id_projeto=".$DW_id_projeto."<br>");
//echo("<hr>");
//echo("<pre>");
//print_r($_REQUEST);
//echo("</pre>");
//echo("<hr>");
//echo("<pre>");
//print_r($objDEFWEBavaliacao);
//echo("</pre>");
//echo("<hr>");
//
// Se n�o tem a situa��o da avalia��o...
if($DW_cd_sit_avaliacao == "") {
    // For�a com o c�digo para "N�o liberada"
    $DW_cd_sit_avaliacao = 2;
}
//
$objDSXadmin = new clsDSXmasterAdmin();
//
$objDEFWEBmaster = new DEFWEBmaster();
//
$objDEFWEBmaster->obterClientes();
$objDEFWEBmaster->obterProjetos($DW_id_cliente);
//
?>
<link rel="stylesheet" href="lib/js/jquery/themes/extranet/jquery.ui.all.css">

<form method=post enctype='multipart/form-data' action='index.php' name='frm' id="frm">
    <input type=hidden name='module' value='<?= $_REQUEST['module'] ?>'>
    <input type=hidden name='mode' value='data'>

    <table border=0 cellpadding=0 cellspacing=1 bgcolor="<?= $bgcolor5 ?>" align='center'>
        <tr align='center' bgcolor="<?= $bgcolor4 ?>">
            <td style="font-weight: bold; text-align: center; vertical-align: middle; height: 25px">CADASTRO DE PROJETO/PESQUISA</td>
        </tr>
        <tr>
            <td bgcolor="<?= $bgcolor6 ?>">

                <table style="padding:15px;" border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Cliente:</td>
                        <td style="font-weight: bold;">
                            <select id="id_cliente" name="id_cliente" class="combo" onchange="javascript:carregarProjetos();">
                            <?php
                            if ($DW_id_cliente == "") {
                            ?>
                                <option value="">(Selecione o cliente)</option>
                            <?php
                            }
                            ?>
                                <?
                                //
                                foreach ($objDEFWEBmaster->colClientes AS $objItem) {
                                    //
                                    if ($DW_id_cliente === $objItem->Id) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    //  Todas op��es do dropdown somente ser�o carregadas
                                    //  se for inclus�o (id_projeto vazio)
                                    //  sen�o carrega apenas o cliente selecionado na pesquisa j� existente.
                                    // 
                                    if (($DW_id_projeto == "") || ($DW_id_projeto != "" && $sAttrSelected != "")) {
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->Id;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->Nome . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Projeto:</td>
                        <td style="font-weight: bold;">
                            <select id="id_projeto" name="id_projeto" class="combo">
                            <?php
                            if ($DW_id_projeto == "") {
                            ?>
                                <option value="">(Selecione o projeto)</option>
                            <?php
                            }
                                //
                                foreach ($objDEFWEBmaster->colProjetos AS $objItem) {
                                    //
                                    if ($DW_id_projeto === $objItem->Id) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    //  Todas op��es do dropdown somente ser�o carregadas
                                    //  se for inclus�o (id_projeto vazio)
                                    //  sen�o carrega apenas o projeto selecionado na pesquisa j� existente.
                                    // 
                                    if (($DW_id_projeto == "") || ($DW_id_projeto != "" && $sAttrSelected != "")) {
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->Id;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->Nome . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Nome da Pesquisa:</td>
                        <td style="font-weight: bold;"><input type="text" name="ds_titulo" value="<?php echo($objDEFWEBavaliacao->Titulo);  ?>" class="texto" size="62" maxlength="150"></td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Data de in�cio:</td>
                        <?php
                        $sDataInicio = "";
                        if((!is_null($objDEFWEBavaliacao->DataInicio))&&($objDEFWEBavaliacao->DataInicio!="")) {
                            $sDataInicio = date("d/m/Y", strtotime($objDEFWEBavaliacao->DataInicio));
                        }
                        ?>
                        <td style="font-weight: bold;">
                            <input type="text" id="dt_inicio" name="dt_inicio" value="<?php echo($sDataInicio);  ?>" class="texto" size="9" readonly>
                            <a href='javascript:void(0)'>
                                    <img src='img/calendar.png' alt='Calend�rio' style="vertical-align:middle" title='Abrir Calend�rio' border='0' NAME='btncalDtInicio' Onclick="javascript:popdate('document.frm.dt_inicio','popcalDtInicio','150',document.frm.dt_inicio.value)">
                            </a>
                            <span id='popcalDtInicio' style='position:absolute'></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="font-weight: bold; vertical-align: middle; text-align: right; ">Data de t�rmino:</td>
                        <?php
                        $sDataFim = "";
                        if((!is_null($objDEFWEBavaliacao->DataFim))&&($objDEFWEBavaliacao->DataFim!="")) {
                            $sDataFim = date("d/m/Y", strtotime($objDEFWEBavaliacao->DataFim));
                        }
                        ?>
                        <td style="font-weight: bold;">
                            <input type="text" id="dt_fim" name="dt_fim" value="<?php echo($sDataFim);  ?>" class="texto" size="9" readonly>
                            <a href='javascript:void(0)'>
                                    <img src='img/calendar.png' alt='Calend�rio' style="vertical-align:middle" title='Abrir Calend�rio' border='0' NAME='btncalDtTermino' Onclick="javascript:popdate('document.frm.dt_fim','popcalDtTermino','150',document.frm.dt_fim.value)">
                            </a>
                            <span id='popcalDtTermino' style='position:absolute'></span>
                        </td>
                    </tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Situa��o da Pesquisa:</td>
                        <td style="font-weight: bold;">
                            <select id="cd_sit_avaliacao" name="cd_sit_avaliacao" class="combo">
                                <?
                                //
                                $arraySituacao = $objDEFWEBmaster->obterSituacoesPesquisa();
                                //
                                foreach ($arraySituacao AS $objItem) {
                                    //
                                    if ($DW_cd_sit_avaliacao == $objItem->Codigo) {
                                        $sAttrSelected = " SELECTED";
                                    } else {
                                        $sAttrSelected = "";
                                    }
                                    //
                                    $sHTML = "<OPTION VALUE='";
                                    $sHTML .= $objItem->Codigo;
                                    $sHTML .= "'" . $sAttrSelected;
                                    $sHTML .= ">" . $objItem->Situacao . "</OPTION>";
                                    //
                                    echo($sHTML);
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr style="height: 15px"><td colspan="2"></td></tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: top; text-align: right; ">Imagem do topo:</td>
                        <td style="color:navy;">
                            <?php
//
// Indicador se ha arquivo de logotipo armazenado
                            $bTemArquivoArmazenado = (!is_null($objDEFWEBavaliacao->IMGLogoID) && ($objDEFWEBavaliacao->IMGLogoID != ""));
// Se tiver arquivo de logotipo...
                            if ($bTemArquivoArmazenado) {
                            ?>
                            <?php //$CONFIGENET_DISC2WORK
                            //$LINK_download = "http://www.disc2work.com.br/client/dsm_projetos/dsm_obterarq.php?";
                            $LINK_download = $CONFIGENET_DISC2WORK . "/client/dsm_projetos/dsm_obterarq.php?";
                            $LINK_download .= "ida=" . $objDEFWEBavaliacao->Id;
                            ?>
                            <input type="button" id="btnBaixar" name="btnBaixar" value="Baixar" class="botao" onclick='window.open("<?php echo($LINK_download); ?>", "_blank");' />&nbsp;
                            <?php
                                // Exibe o nome do arquivo
                                echo($objDEFWEBavaliacao->IMGLogoName);
                                //
                            } else {
                                // Exibe mensagem para o usu�rio
                                echo("Nenhum arquivo armazenado.");
                            }
                            ?>
                            <br><br>
                            <input type=file name=file_topo_novo id="file_topo_novo" size=45>
                            <?php
                            if ($bTemArquivoArmazenado) {
                                ?>
                                &nbsp;&nbsp;<input type="submit" id="btnRemoverArq" name="btnRemoverArq" value="Remover" class="botao" onclick="return confirm('Deseja realmente remover o arquivo da imagem de topo?')">
                                <br>
                                <font style="font-size: x-small;">
                                Selecione um novo arquivo e clique sobre o bot�o <b>Enviar</b> para substitu�-lo.<br/>
                                <b>Aten��o:</b><br>
                                O arquivo deve estar no formato JPEG, GIF ou PNG, e com a dimens�o recomendada<br/>
                                de 600 pontos de largura por 100 pontos de altura (600x100).
                                <input type=hidden name=file_topo id="file_topo" value="<?php echo($objPedido->nm_arquivo); ?>">
                                <?php
                            } else {
                                ?>
                                <br>
                                <font style="font-size: x-small;">
                                Selecione um arquivo para armazen�-lo. Se j� houver um arquivo ele ser� substitu�do ao salvar a pesquisa.<br/>
                                <b>Aten��o:</b><br/>
                                O arquivo deve estar no formato JPEG, GIF ou PNG, e com a dimens�o recomendada<br/>
                                de 600 pontos de largura por 100 pontos de altura (600x100).
                                <input type=hidden name=file_topo id="file_topo" value="">
                                <?php
                            }
                            ?>
                            </font>
                        </td>
                    </tr>
                    
                    <tr style="height: 15px"><td colspan="2"></td></tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Formul�rio da pesquisa:</td>
                        <td style="font-weight: bold;">
                            <select id="id_form_franqueado" name="id_form_franqueado" class="combo" style="width:400px;">
                                <option value="">(Selecione o formul�rio)</option>
                                <?
                                $objDEFWEBmaster = new DEFWEBmaster();
                                //
                                $objDEFWEBmaster->obterFormularios("0");
                                // C�digo para p�blico alvo Franqueado
                                $CodigoPublicoAlvo = 2;
                                //
                                // Monta a lista de formul�rios que n�o est�o cadastrados
                                //
                                foreach ($objDEFWEBmaster->colFormularios AS $objItem) {
                                    //
                                    $objFormulario = $objDEFWEBavaliacao->colFormularios[$CodigoPublicoAlvo][$objItem->Id];
                                    //
                                    $sHTML = "<OPTION VALUE='";
                                    $sHTML .= $objItem->Id . "'";
                                    //
                                    // Se for o formulario selecionado...
                                    if($objFormulario->IdFormulario == $objItem->Id) {
                                        $sHTML .= " SELECTED";
                                    }
                                    $sHTML .= ">" . $objItem->Nome;
                                    $sHTML .= "</OPTION>";
                                    //
                                    echo($sHTML);
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr style="height: 15px"><td colspan="2"></td></tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: center; " colspan="2">Configura��o das p�ginas</td>
                    </tr>
                    
                    <tr style="height: 15px"><td colspan="2"></td></tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Confirma��o de cadastro:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_confcad" name="id_pag_confcad" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 1) {
                                        if ($objDEFWEBavaliacao->IdPaginaConfCad == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Instru��es:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_instrucoes" name="id_pag_instrucoes" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 2) {
                                        if ($objDEFWEBavaliacao->IdPaginaInstrucoes == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Question�rio:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_questionario" name="id_pag_questionario" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 3) {
                                        if ($objDEFWEBavaliacao->IdPaginaQuestionario == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Conclus�o:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_conclusao" name="id_pag_conclusao" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 4) {
                                        if ($objDEFWEBavaliacao->IdPaginaConclusao == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Relat�rio para o entrevistado:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_relquest" name="id_pag_relquest" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 5) {
                                        if ($objDEFWEBavaliacao->IdPaginaRelEntrevistado == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td></td>
                        <td style="font-weight: bold; vertical-align: middle;">
                            <input type="checkbox" id="fl_enviar_email" name="fl_enviar_email" <?php echo( ($objDEFWEBavaliacao->EnviarEmailConclusao == 1) ? "checked" : ""); ?> value="1"> Enviar automaticamente por e-mail.
                        </td>
                    </tr>

                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td style="font-weight: bold; vertical-align: middle; text-align: right;">Relat�rio para a administra��o:</td>
                        <td style="font-weight: bold;">
                            <select id="id_pag_reladmin" name="id_pag_reladmin" class="combo" style="width:400px;">
                                <option value="">(Selecione a p�gina)</option>
                                <?
                                //
                                $colPaginas = $objDSXadmin->listarPaginas(null);
                                //
                                foreach ($colPaginas AS $objItem) {
                                    //
                                    if($objItem->TipoPagina->CodigoTipoPagina == 0 || $objItem->TipoPagina->CodigoTipoPagina == 6) {
                                        if ($objDEFWEBavaliacao->IdPaginaRelAdmin == $objItem->IdPagina) {
                                            $sAttrSelected = " SELECTED";
                                        } else {
                                            $sAttrSelected = "";
                                        }
                                        //
                                        $sHTML = "<OPTION VALUE='";
                                        $sHTML .= $objItem->IdPagina;
                                        $sHTML .= "'" . $sAttrSelected;
                                        $sHTML .= ">" . $objItem->CodigoPagina . "</OPTION>";
                                        //
                                        echo($sHTML);
                                    }
                                }
                                //
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                </table>
                <input type=hidden id="id" name='id' value="<?php echo($DW_id_avaliacao); ?>">
                <input type=hidden id="filtroCliente" name="filtroCliente" value="<?php echo($DW_filtroCliente); ?>" >
            </td>
        </tr>
        <tr>
            <td>

                <table border=0 cellpadding=4 cellspacing=0 width='100%' align='center'>
                    <tr bgcolor="<?= $bgcolor6 ?>">
                        <td>
                            <input type="button" onclick="document.location.href = 'index.php?module=<?= $_REQUEST['module'] ?>&filtroCliente=<?= $DW_filtroCliente?>'" value="Voltar" class="botao">
                        </td>
                        <td style="text-align: right">
                            <? if ($DW_id_avaliacao != "") { ?>
                                <input type="submit" name="salvar" value="Salvar"  class="botao">
                                <input type="submit" name="excluir" value="Excluir"  class="botao" onclick="return confirm('Tem certeza que deseja excluir permanentemente este registro?')">
                            <? } else { ?>
                                <input type="submit" name="incluir" value="Incluir" class="botao">
                            <? } ?>

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

<br><br>

<?php
//echo("objDEFWEBavaliacao:<pre>");
//print_r($objDEFWEBavaliacao);
//echo("</pre>");
