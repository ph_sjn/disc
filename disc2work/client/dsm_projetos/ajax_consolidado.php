<?
session_name("ExtraNet_discmaster");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
include_once("../lang/br.inc.php"); 
include_once($path_pre. "lib/checapermissao.php");

$acesso_administrativo = checaPermissao($module,"A",$user_access);
$acesso_normal = checaPermissao($module,"N",$user_access);
$existe = false;

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../../survey/lib/libdisc_consolidado.php");
//include_once("../discx/libdisc_consolidado.php");

$DW_id_cliente = 0;
$DW_id_avaliacao = $_REQUEST["id"];

$objDEFWEBmasterConsolidado = new DEFWEBmasterConsolidado();

$objConsolidado = $objDEFWEBmasterConsolidado->obterConsolidado($DW_id_avaliacao);

$objItem = new DEFWEBconsolidadoItem();
$objOpcao = new DEFWEBconsolidadoItemOpcao();

$sQuebra_CodigoTema = "";
$sQuebra_CodigoAgrupamento = "";

$GRAFICO_EXIBIR = 1;

echo("<center>");
echo("<table width=80%>");
echo("<tr>");
echo("<td>");

foreach ($objConsolidado As $objItem) {
    
    if(($objItem->CodigoTema != $sQuebra_CodigoTema)||($objItem->CodigoAgrupamento != $sQuebra_CodigoAgrupamento)) {
        $sQuebra_CodigoTema = $objItem->CodigoTema;
        $sQuebra_CodigoAgrupamento = $objItem->CodigoAgrupamento;
        //
        $sTitulo = $objItem->DescricaoAgrupamento . "-" . $objItem->DescricaoTema;
        //
        echo("<br><b>".$sTitulo."</b><br>");
    }
    //
    $sItem = $objItem->DescricaoItem;
    //
    echo("<br>" . $sItem . "<br><br>");
    //
    echo("<div style='float:left;width:50%'>");

    $sData   = '[';
    $sLabels = '[';
    $sColors = '[';
    $c = 1;
    if($objItem->colOpcoes) {
        //
        echo("<table cellpadding=5px cellspacing=0>");
        for($iPos=1;$iPos<=3;$iPos++) {
            //
            echo("<tr>");
            foreach ($objItem->colOpcoes As $objOpcao) {
                
                if($iPos == 1) {
                    echo("<tr>");
                    //

                    echo("<td bgcolor='#EEEEEE' style='min-width:50px; text-align:center; border:solid 1px gray;'>");
                    echo("<b>" . $objOpcao->DescricaoOpcaoItem . "</b>");
                    echo("</td>");
                    
                    if($objItem->TipoOpcoes != "S"){
                        echo("<td style='min-width:50px; text-align:center; border:solid 1px gray;'>");
                        echo(number_format($objOpcao->Quantidade , 0, ",", "."));
                        echo("</td>");                    
                    }
                    
                    echo("<td style='min-width:50px; text-align:center; border:solid 1px gray;'>");
                    echo(number_format($objOpcao->Percentual, 2, ",", ".") . "%");
                    echo("</td>");
                    
                    switch ($c) {
                        case 1:
                            $cor = "#000080"; //
                            break;
                        case 2:
                            $cor = "#3366FF";//
                            break;
                        case 3:
                            $cor = "#9ACD32";//
                            break;
                        case 4:
                            $cor = "#006400"; //
                            break;
                        case 5:
                            $cor = "#C0C0C0"; //
                            break;
                        case 6:
                            $cor = "#CC0000";//
                            break;
                        case 7:
                            $cor = "#008000";
                            break;
                        case 8:
                            $cor = "#FFCC00";
                            break;
                        case 9:
                            $cor = "#FFCC00";
                            $c = 0;
                            break;
                    }
                    

                    $c = $c + 1;
                    $sData   = $sData   . number_format($objOpcao->Percentual, 2, ".", "") . ',';
                    $sLabels = $sLabels . '"' . $objOpcao->DescricaoOpcaoItem . ': ' . number_format($objOpcao->Percentual, 2, ",", ".") . '%",';
                    $sColors = $sColors . '"' . $cor . '",';
                    
                    echo("<td style='min-width:50px; text-align:center; border:solid 1px gray;'bgcolor=" . $cor  . " ;>");
                    echo("</td>");
                    
                    echo("</tr>");
                    

                }     
                
                
            }
            echo("</tr>");
        }
        //
        echo("</table>");
        echo("</div>");
        
?>
<?php

        if($GRAFICO_EXIBIR == 1) {
            $sData   = substr($sData,   0, -1) . ']';
            $sLabels = substr($sLabels, 0, -1) . ']';
            $sColors = substr($sColors, 0, -1) . ']';
            echo("<br>");
            echo("<input type='hidden' id='graf" . $objItem->CodigoItem . "' value='". $sData ."' >");
            echo("<input type='hidden' id='grfLabels" . $objItem->CodigoItem . "' value='". $sLabels ."' >");
            echo("<input type='hidden' id='grfColors" . $objItem->CodigoItem . "' value='". $sColors ."' >");
            echo("<div style='float:left;'>");
            echo("<canvas id='canv" . $objItem->CodigoItem . "' width='300' height='150'></canvas>");
            echo("</div>");            
        }   
        
    } else {
        echo("<font color=red>Nenhum franqueado respondeu esta pergunta ainda.</font><br>");
    }
    echo("<div style='clear: both;'>");
}
echo("</td>");
echo("</tr>");
echo("</table>");
echo("</center>");
echo("");
            echo("<div>");
            echo("<canvas id='canv99'></canvas>");
            echo("</div>");            
?>
<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />
