<!-- Chart.bundle.js versao 2.5.0-->
<script src="lib/js/chart/Chart.bundle.js"></script>
<link rel="stylesheet" href="lib/js/jquery/themes/extranet/jquery.ui.all.css">

<script src='dsm_projetos/js/ajax.js' type='text/javascript'></script>
<script src='dsm_projetos/js/projetos_analise.js' type='text/javascript'></script>
<script src='../../survey/js/dssurvey_graph.js' type='text/javascript'></script>

<?php
$acesso_administrativo = checaPermissao($module,"A",$user_access); 
?>

<?php
$DEFWEB_cliente = $_REQUEST['filtroCliente'];
?>

<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<tr>
	<td style="vertical-align:middle;">
		<a href="index.php?module=<?= $module ?>&mode=forms"><span style="font-weight:bold;">NOVA PESQUISA</span></a>
	</td>
</tr>
<tr>
	<td style="text-align:right">
	
		<form action='index.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
		  
				Cliente: 
				<select name='filtroCliente' id="filtroCliente" class="combo">
				<option value=''>Todos</option>
				<?
				$sql = "SELECT id_cliente, ds_cliente FROM tbdw_cliente ORDER BY ds_cliente ASC";
				$resultCliente = db_query($sql) or db_die();
				while ($rowCliente = db_fetch_array($resultCliente) ){
					if ($DEFWEB_cliente == $rowCliente['id_cliente'])
						$selected = "selected";
					else
						$selected = "";
					echo "<option value='". $rowCliente['id_cliente'] ."' $selected>". $rowCliente['ds_cliente'] ."</option> " ;
				}
				?>
				</select>
			</td>
			<td style="vertical-align:middle;">
				<input type='Submit' name="filtrarCliente" value='Filtrar' class="botao">
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>

<br>

<table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
    <tr >
        <td>


            <?
            $sql = "SELECT";
            $sql .= " av.id_avaliacao AS id_avaliacao,";
            $sql .= " pj.nm_projeto AS nm_projeto,";
            $sql .= " cl.ds_cliente AS ds_cliente,";
            $sql .= " av.ds_titulo AS ds_titulo,";
            $sql .= " sit.ds_sit_avaliacao AS ds_sit_avaliacao,";
            $sql .= " av.dt_inicio AS dt_inicio, av.dt_fim AS dt_fim";
            $sql .= " FROM tbdw_avaliacao AS av";
            $sql .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente";
            $sql .= " LEFT JOIN tbdw_sit_avaliacao AS sit ON sit.cd_sit_avaliacao = av.cd_sit_avaliacao";
            $sql .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
            if ($DEFWEB_cliente != "") {
                $sql .= " WHERE av.id_cliente = " . $DEFWEB_cliente;
            }
            $sql .= " ORDER BY ds_cliente, nm_projeto, ds_titulo";
            //echo("<hr>" . $sql . "<hr>");
//$sql .= " ORDER BY av.cd_sit_avaliacao DESC, av.id_avaliacao DESC";
            //echo $sql_acesso."<br />";
            $resultSet = db_query($sql);
            $total_pesquisas = db_num_rows($resultSet);
            $i = 1;
            while ($rsDados = db_fetch_array($resultSet)) {
                ?>
                <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
                    <tr width="100%" onclick="javascript:montarDashboard('<?= $module ?>', '<?= $rsDados["id_avaliacao"] ?>', 'div_<?= $module ?>_<?= $i ?>', 'imagem_<?= $i ?>', 'tr_<?= $i ?>', '<?= $module ?>', '<?= $item_unidade[2] ?>','<?= $i ?>');" onmouseover="this.style.cursor = 'pointer'">
                        <td bgcolor="<?= $bgcolor4 ?>">
                            <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                <tr>
                                    <td><img id="imagem_<?= $i ?>" src="img/close.gif" border="0">&nbsp;
                                        <b><?= $rsDados["nm_projeto"] . " : " . $rsDados["ds_titulo"] ?></b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="display:none; padding: 0px;" id="tr_<?= $i ?>" bgcolor="<?= $bgcolor6 ?>">
                        <td style="padding: 0px;"><div id="div_<?= $module ?>_<?= $i ?>"></div></td>
                    </tr>
                </table>
                <br>
                <? $i++; ?>	<? }
            ?>


        </td>
    </tr>
</table>
<br><br>

	<div style="text-align: center; font-weight: bold;">Total de pesquisas encontradas: <span style="color:#FF0000"><?= $total_pesquisas ?></span> </div>

<br><br>

