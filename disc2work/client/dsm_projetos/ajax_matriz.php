<?
// Backup desta versao: ajax_matriz.v5.php
session_name("ExtraNet_discmaster");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1", true);
$path_pre = "../";
include_once("../config.inc.php");
include_once("../lib/db/mysql.inc.php");
include_once("../lang/br.inc.php");
include_once($path_pre . "lib/checapermissao.php");

$acesso_administrativo = checaPermissao($module, "A", $user_access);
$acesso_normal = checaPermissao($module, "N", $user_access);
$existe = false;

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../../survey/lib/libdisc.php");
//include_once("../discx/libdisc.php");

// Biblioteca de classes e fun��es para aplica��o do question�rio
include_once("../../survey/lib/libdisc_survey.php");
//include_once("../discx/libdisc_survey.php");

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../../survey/lib/libdisc_analise.php");
//include_once("../discx/libdisc_analise.php");

$DW_id_cliente = 0;
$DW_id_avaliacao = $_REQUEST["id"];

$ID_AVALIACAO = 7;
$ID_TOKEN = 794;

$GRAFICO_EXIBIR = 1;

$objDEFWEBmasterAnalise = new DEFWEBmasterAnalise($DW_id_avaliacao);

$objDEFWEBmasterAnalise->obterFranqueados($DW_id_avaliacao);
?>
<center>
    <table width="80%">
		<tr>
            <td colspan="4">
                <br>
					<b>MATRIZ DISC DOS ENTREVISTADOS</b><br>
                <br>
            </td>
		</tr>
		<tr>
			<td width="50%">
				ordenar por: 
				<input type="radio" name="optOrder" value="0" checked> <b>Entrevistado</b>
				 ou por: 
				<input type="radio" name="optOrder" value="1"> <b>Resultado</b>
			</td>
			<td>
				<input type="text" id="entrFiltro" onkeyup="filtrarEntrevistados()" class="dsFiltro" placeholder="Filtrar resultado...">
			</td>
			<td>
				<input style="display:none;" type='button' name="cmdOpenRptAll" value='Abrir todos' title="Abrir todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
			<td>
				<input style="display:none;" type='button' name="cmdCloseRptAll" value='Fechar todos' title="Fechar todos." class="botao" onmouseover="this.style.cursor='pointer';">
			</td>
        </tr>
        <tr>
            <td colspan="4">

                <?php
				//
				// Cria matriz da pesquisa
				$discPesquisa = array(
					"D" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"I" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"S" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					),
					"C" => array(
						"M" => 0,
						"L" => 0,
						"A" => 0
					)
				);
				$nEntrevistados  = 0;
				//
                $bPrimeiraLinha = TRUE;
                foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
                    //
                    unset($objDWsurvey);
                    $objDWsurvey = new DEFWEBsurvey($DW_id_avaliacao, $objFranqueado->Id);
                    if ($objDWsurvey->qt_nao_respondidas == 0) {
						//
						$objDiscNormalizado = new DEFWEBdisc_normalizado();
						$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
						//
						// Contar os entrevistados
						$nEntrevistados++;
						//
						// Acumular os pontos M e L do disc dos entrevistados
						$discPesquisa["D"]["M"] += $objDWsurvey->colDISC["D"]["M"];
						$discPesquisa["D"]["L"] += $objDWsurvey->colDISC["D"]["L"];
						$discPesquisa["I"]["M"] += $objDWsurvey->colDISC["I"]["M"];
						$discPesquisa["I"]["L"] += $objDWsurvey->colDISC["I"]["L"];
						$discPesquisa["S"]["M"] += $objDWsurvey->colDISC["S"]["M"];
						$discPesquisa["S"]["L"] += $objDWsurvey->colDISC["S"]["L"];
						$discPesquisa["C"]["M"] += $objDWsurvey->colDISC["C"]["M"];
						$discPesquisa["C"]["L"] += $objDWsurvey->colDISC["C"]["L"];
						// Acumular os pontos A normalizados dos entrevistados
						$discPesquisa["D"]["A"] += $objDiscNormalizado->colDISC_convertido["D"];
						$discPesquisa["I"]["A"] += $objDiscNormalizado->colDISC_convertido["I"];
						$discPesquisa["S"]["A"] += $objDiscNormalizado->colDISC_convertido["S"];
						$discPesquisa["C"]["A"] += $objDiscNormalizado->colDISC_convertido["C"];
						//
						if ($bPrimeiraLinha) {
                            $bPrimeiraLinha = FALSE;
							//
							// Tabela incluida para a ordenacao dos entrevistados
							//
							echo "<table id=\"tblDadosEntrevistados\" width='100%'>";
						}
                        ?>
	<?php // table tblDadosEntrevistados ordenacao ?>
	<tr>
		<td>
	<?php // table tblDadosEntrevistados ordenacao ?>

            <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?php echo($bgcolor5); ?>' align='center'>
                <tr width="100%">
                    <td bgcolor="<?php echo($bgcolor4); ?>">
                        <table width="100%" cellpadding="0" cellspacing="0" border=0>
                            <tr>
                                <td width="50%" onclick="javascript:entrevistado('imgEntrv_<?= $objFranqueado->Id ?>', 'trEntrv_<?= $objFranqueado->Id ?>');" onmouseover="this.style.cursor = 'pointer'">
									<img id="imgEntrv_<?php echo($objFranqueado->Id);?>" src="img/close.gif" border="0">&nbsp;
                                    <span class="td_entrv_nome">
                                    <?php
                                        echo($objFranqueado->Nome);
                                        if ($objFranqueado->Unidade != "") {
                                            echo("<br>(" . $objFranqueado->Unidade . ")");
                                        }
                                    ?>
									</span>
                                </td>
								<td width="20%">
									<?php
										$sLetters   = $objDiscNormalizado->resultado;
										$result_fmt = "";
										for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
											if ( $nLetter == 0 ) {
												$result_fmt = "<span class=\"disc_letter_main\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											} else {
												$result_fmt .= "<span class=\"disc_letter_norm\">";
												$result_fmt .= substr($sLetters, $nLetter, 1);
												$result_fmt .= "</span>";
											}
										}
										if ( strlen($sLetters) > 0 ) {
											echo($result_fmt);
										}
									?>
								</td>
								<td width="10%">
									<?php
										// BOTAO DO RELATORIO DO ENTREVISTADO
										$sLink = " onclick=\"javascript:window.open('" . $CONFIGENET_DISC2WORK . "survey/dssurvey_redir.php?etapa=6&id_h=" . $objFranqueado->Hash . "');\" ";
									?>
									<input type='button' name="cmdOpenRpt_<?php echo($objFranqueado->Id);?>" value='Relat�rio' title="Abrir relat�rio." class="botao" onmouseover="this.style.cursor='pointer';" <?php echo($sLink);?>>
                                                                        <?php
                                                                        // BOTAO PARA IMPRIMIR O RELATORIO DO ENTREVISTADO
                                                                        $sLinkPrint = " onclick=\"javascript:window.open('" . $CONFIGENET_DISC2WORK . "survey/dssurvey_redir.php?etapa=6&rpt=1&id_h=" . $objFranqueado->Hash . "');\" ";
                                                                        ?>
									<br><br><input type='button' name="cmdOpenRptPrint_<?php echo($objFranqueado->Id);?>" value='Imprimir' title="Abrir relat�rio." class="botao" onmouseover="this.style.cursor='pointer';" <?php echo($sLinkPrint);?>>
                                                                </td>
								<td width="20%" class="td_entrv_dt_termino">
									<?php
										$dt_termino_token = new DateTime($objDWsurvey->dt_termino_token);
										echo($dt_termino_token->format('d/m/Y H:i:s'));
									?>
								</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display:none; padding: 0px;" id="trEntrv_<?php echo($objFranqueado->Id);?>" bgcolor="<?php echo($bgcolor6); ?>">
                    <td style="padding: 5px;">						
						
						
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
                                    <td>Resultado</td>
                                    <?php
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
									<td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">M</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">L</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">A</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["D"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["I"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["S"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($objDWsurvey->colDISC["C"]["A"]); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAn" . $objFranqueado->Id . "' value='". $objDiscNormalizado->serie ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAn" . $objFranqueado->Id . "' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
									<td style='vertical-align: middle'>
										<p><span class="disc_perc_fator_D"><?=$objDiscNormalizado->colDISC_convertido["D"]?>% </span><span class="disc_texto_fator_D">Domin�ncia</span></p>
										<p><span class="disc_perc_fator_I"><?=$objDiscNormalizado->colDISC_convertido["I"]?>% </span><span class="disc_texto_fator_I">Influ�ncia</span></p>
										<p><span class="disc_perc_fator_S"><?=$objDiscNormalizado->colDISC_convertido["S"]?>% </span><span class="disc_texto_fator_S">Estabilidade</span></p>
										<p><span class="disc_perc_fator_C"><?=$objDiscNormalizado->colDISC_convertido["C"]?>% </span><span class="disc_texto_fator_C">Conformidade</span></p>
									</td>
                                </tr>
							</tbody>
						</table>
						
					</td>
				</tr>
			</table>
	<?php // table tblDadosEntrevistados ordenacao ?>
		</td>
	</tr>
	<?php // table tblDadosEntrevistados ordenacao ?>
                <?php
                    }
                }
                if ($bPrimeiraLinha) {
                    echo("NENHUM ENTREVISTADO RESPONDEU AO QUESTION�RIO!");
                } else {
					//
					// Tabela incluida para a ordenacao dos entrevistados
					//
					echo "</table>";
				}
                ?>
            </td>
        </tr>
		<?php 
		//----------------------------------------------------------------------------------------------------------
		// MATRIZ DISC DA PESQUISA - INICIO
		//----------------------------------------------------------------------------------------------------------
		if ( $nEntrevistados > 0 ){
			//
			// Transformando o acumulado em media da pesquisa
			$discPesquisa["D"]["M"] = intval(round(($discPesquisa["D"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["L"] = intval(round(($discPesquisa["D"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["M"] = intval(round(($discPesquisa["I"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["L"] = intval(round(($discPesquisa["I"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["M"] = intval(round(($discPesquisa["S"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["L"] = intval(round(($discPesquisa["S"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["M"] = intval(round(($discPesquisa["C"]["M"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["L"] = intval(round(($discPesquisa["C"]["L"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["D"]["A"] = intval(round(($discPesquisa["D"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["I"]["A"] = intval(round(($discPesquisa["I"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["S"]["A"] = intval(round(($discPesquisa["S"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			$discPesquisa["C"]["A"] = intval(round(($discPesquisa["C"]["A"]/$nEntrevistados),0,PHP_ROUND_HALF_UP));
			// Monta a serie do grafico normalizado
			$discPesqSerieNormalizada = "[" . $discPesquisa["D"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["I"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["S"]["A"];
			$discPesqSerieNormalizada .= "," . $discPesquisa["C"]["A"];
			$discPesqSerieNormalizada .= "]";
			//
			// Calculando o A da pesquisa para obter o resultado
			$discPesquisa["D"]["A"] = ($discPesquisa["D"]["M"]-$discPesquisa["D"]["L"]);
			$discPesquisa["I"]["A"] = ($discPesquisa["I"]["M"]-$discPesquisa["I"]["L"]);
			$discPesquisa["S"]["A"] = ($discPesquisa["S"]["M"]-$discPesquisa["S"]["L"]);
			$discPesquisa["C"]["A"] = ($discPesquisa["C"]["M"]-$discPesquisa["C"]["L"]);
			// Cria o objeto Normalizador
			$discPesqNormalizado = new DEFWEBdisc_normalizado();
			$discPesqNormalizado->normalizar_disc($discPesquisa, "");
			// Obtem o resultado da pesquisa
			$sLetters   = $discPesqNormalizado->resultado;
			$result_fmt = "";
			for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
				if ( $nLetter == 0 ) {
					$result_fmt = "<span class=\"disc_letter_main\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				} else {
					$result_fmt .= "<span class=\"disc_letter_norm\">";
					$result_fmt .= substr($sLetters, $nLetter, 1);
					$result_fmt .= "</span>";
				}
			}
			?>
			<tr>
				<td>
					<br>
						<b>MATRIZ DISC DA PESQUISA</b><br>
					<br>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4">
			
                        <table style="line-height: 25px;" cellspacing="0">
                            <thead style="font-weight: bold;">
                                <tr>
									<?php
									if ( $result_fmt != "" ) {
										echo("<td>$result_fmt</td>");
                                    } else {
										echo("<td>Resultado</td>");
									}
                                    if($GRAFICO_EXIBIR == 1) {
                                      	echo("<td></td>");
                                    }
									?>
									<td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style='vertical-align: middle; padding:10px; min-width:50px; text-align:center; border:solid 1px gray;'>
                                        <table style="line-height: 15px; text-align:center; margin:0px; padding:0px;">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">M</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">L</td>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; min-width:30px; border:solid 1px lightgray; text-align: center;">A</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">D</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["D"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">I</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["I"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">S</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["S"]["A"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#EEEEEE' style="font-weight: bold; width:20px; border:solid 1px lightgray; text-align: center;">C</td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["M"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["L"]); ?></td>
                                                    <td style="width:20px; border:solid 1px lightgray; text-align: center;"><?php echo($discPesquisa["C"]["A"]); ?></td>
                                                </tr>
                                            </tbody>

                                        </table>
                                    </td>
                                    <?php
									if($GRAFICO_EXIBIR == 1) {
            							echo("<td>");
            							echo("<br>");
										echo("<input type='hidden' id='grfAnPesq' value='". $discPesqSerieNormalizada ."' >");
            							echo("<div style='float:left;'>");
            							echo("<canvas id='canvAnPesq' height='300'></canvas>");
            							echo("</div>");
            							echo("</td>");
									}
									?>
									<td style='vertical-align: middle'>
										<p><span class="disc_perc_fator_D"><?=$discPesqNormalizado->colDISC_convertido["D"]?>% </span><span class="disc_texto_fator_D">Domin�ncia</span></p>
										<p><span class="disc_perc_fator_I"><?=$discPesqNormalizado->colDISC_convertido["I"]?>% </span><span class="disc_texto_fator_I">Influ�ncia</span></p>
										<p><span class="disc_perc_fator_S"><?=$discPesqNormalizado->colDISC_convertido["S"]?>% </span><span class="disc_texto_fator_S">Estabilidade</span></p>
										<p><span class="disc_perc_fator_C"><?=$discPesqNormalizado->colDISC_convertido["C"]?>% </span><span class="disc_texto_fator_C">Conformidade</span></p>
									</td>
                                </tr>
							</tbody>
						</table>
			
			</td>
		</tr>
		<?php 
		}
		//----------------------------------------------------------------------------------------------------------
		// MATRIZ DISC DA PESQUISA - FINAL
		//----------------------------------------------------------------------------------------------------------
		?>
    </table>
    <br>
    <table width="80%">
        <tr>
            <td>
                <br>
                <b>ENTREVISTADOS QUE N�O RESPONDERAM AO QUESTION�RIO</b><br>
                <br>
            </td>   
        </tr>
        <tr>
            <td>
                <?php
                $bPrimeiraLinha = TRUE;
                //
                foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
                    //
                    unset($objDWsurvey);
                    $objDWsurvey = new DEFWEBsurvey($DW_id_avaliacao, $objFranqueado->Id);
                    //
                    // Se n�o respondeu nenhuma pergunta, ou,
                    // se ainda n�o respondeu todas as perguntas (em andamento)...
                    if(($objDWsurvey->qt_respondidas == 0)||($objDWsurvey->qt_respondidas > 0 && $objDWsurvey->qt_nao_respondidas > 0)) {
                        // Se for a primeira linha da tabela...
                        if ($bPrimeiraLinha) {
                            // Desliga o flag e monta
                            $bPrimeiraLinha = FALSE;
                            // Abre a tabela e monta o cabe�alho
                            ?>
				<table style="line-height: 25px;" cellspacing="0">
					<thead style="font-weight: bold;">
						<tr>
							<td>
								Entrevistado
							</td>
                            <td>
                                Situa��o
                            </td>
                        </tr>
                    </thead>
                    <tbody>
						<?php 
						}
						?>
                        <tr>
                            <td bgcolor='#EEEEEE' style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                <?php
                                echo($objFranqueado->Nome);
                                if ($objFranqueado->Unidade != "") {
                                    echo("<br>(" . $objFranqueado->Unidade . ")");
                                }
                                ?>
                            </td>
                    <?php
                        // Se n�o respondeu nenhuma pergunta...
                        if ($objDWsurvey->qt_respondidas == 0) {
                    ?>
                            <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                Ainda n�o respondeu ao question�rio.
                            </td>
                    <?php
                    } else {
                    ?>
                            <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                Entrevista em andamento.
                            </td>
                    <?php
                    }
                    ?>
                        </tr>
                <?php
					}
                }
                // Se j� desligou o indicador de primeira linha...
                if (!$bPrimeiraLinha) {
                    // Fecha a tabela
                ?>
                    </tbody>
                </table>
                    <?php
                } else {
                    echo("TODOS OS ENTREVISTADOS RESPONDERAM AO QUESTION�RIO!<br>");
                }
                ?>
                <br>
            </td>
        </tr>
    </table>

</center>
<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />
