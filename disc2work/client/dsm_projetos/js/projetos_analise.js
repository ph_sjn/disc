function montarAnalise(modulo, id, div, img, tr, mode) {
    browser = "NAVEGADOR" + navigator.userAgent;
    ajax2 = Ajax();
    var element = document.getElementById(tr);
    var ico = document.getElementById(img);

    if (element.style.display == 'none') {
		//
		if (browser.indexOf("MSIE") > 0){
			element.style.display = "block";
		} else {
			element.style.display = 'table-row';
		}
		ico.setAttribute('src', 'img/open.gif');
        var sLoading = "<div style=\"text-align: center; margin: 10px;\"><img src=\"disc_config/img/dwloading.gif\"></div>";
        document.getElementById(div).innerHTML = sLoading;
		//
        pagina = "dsm_projetos/ajax_matriz.php?module=" + modulo + "&mode=" + mode + "&id=" + id;
        //alert (pagina);
        ajax2.open("GET", pagina, true);
        ajax2.onreadystatechange = function() {
            if (ajax2.readyState == 4) {
                if (ajax2.status == 200) {

                    document.getElementById(div).innerHTML = ajax2.responseText;
					// Chama a funcao que configura os botoes dos entrevistados
					configBtnEntrevistas();
                    // Chama a funcao que desenha os graficos
                    montarGraficosAnalise();
					// Chama a funcao que configura os radios (opcao de ordenacao)
					configRadioOrder();
                }
            }
        }
        ajax2.send(null);
    } else {
        //document.getElementById(div).innerHTML = "";
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}
//
// Abre/Fecha o entrevistado selecionado
function entrevistado(img, tr){
    browser = "NAVEGADOR" + navigator.userAgent;
	var element = document.getElementById(tr);
    var ico = document.getElementById(img);
    if (element.style.display == 'none') {
        if (browser.indexOf("MSIE") > 0)
            element.style.display = "block";
        else
            element.style.display = 'table-row';
		ico.setAttribute('src','img/open.gif');
		montarGraficosAnalise();
    } else {
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}
//
//
function configBtnEntrevistas(){
	if ( jQuery("tr[id^='trEntrv_']").length > 0 ) {
		var btnOpen  = jQuery('input[name=cmdOpenRptAll]');
		var btnClose = jQuery('input[name=cmdCloseRptAll]');
		if ( btnOpen !== undefined ){
			btnOpen.bind( "click", function() {
				// Abre a analise de todos os entrevistados
				jQuery("tr[id^='trEntrv_']").show();
				jQuery("img[id^='imgEntrv_']").attr("src","img/open.gif");
				montarGraficosAnalise();
			});
			if ( btnOpen.css('display') == 'none' ){
				btnOpen.show();
			}
		}
		if ( btnClose !== undefined ){
			btnClose.bind( "click", function() {
				// Fecha a analise de todos os entrevistados
				jQuery("tr[id^='trEntrv_']").hide();
				jQuery("img[id^='imgEntrv_']").attr("src","img/close.gif");
			});
			if ( btnClose.css('display') == 'none' ){
				btnClose.show();
			}
		}
	}
}
//
//
function configRadioOrder(){
	jQuery('input[type=radio]').click(function(){
		var optOrder = jQuery('input[name=optOrder]:checked').val();
		if (/(0|1)/.test(optOrder)){
			sortTable(optOrder);
		}
	});
}
//
function sortTable(n) {
	var rows = jQuery('#tblDadosEntrevistados > tbody > tr').get();
	//
	rows.sort(function(a, b) {
		var A = getVal(a);
		var B = getVal(b);
		if(A < B) {
			return -1;
		}
		if(A > B) {
			return 1;
		}
		return 0;
	});
	//
	function getVal(tr){
		var tr2 = jQuery('table > tbody > tr:eq(0)', tr);
		var tr3 = jQuery('table > tbody > tr:eq(0)', tr2);
		var td  = jQuery('td:eq(' + n + ')', tr3);
		var spn = jQuery('span', td);
		var tx  = jQuery.trim(spn.text().toUpperCase());
		if ( n == 1 ){
			// Se for o resultado
			tx = tx.replace("C", "X");
		}
		return tx;
	}
	//
	jQuery.each(rows, function(index, row) {
		jQuery('#tblDadosEntrevistados').children('tbody').append(row);
	});
	//
}

function filtrarEntrevistados() {
	// Valor do filtro
	var filter = jQuery('#entrFiltro').val().toUpperCase();
	//
	// Dados dos entrevistados
	var rows = jQuery('#tblDadosEntrevistados > tbody > tr').get();
	//
	//
	rows.filter(function(row) {
		var rFilter = valFilter(row);
		if(rFilter) {
			row.style.display = "none";
		} else {
			row.style.display = "";
		}
	});
	//
	function valFilter(tr){
		var tr2 = jQuery('table > tbody > tr:eq(0)', tr);
		var tr3 = jQuery('table > tbody > tr:eq(0)', tr2);
		var td1  = jQuery('td:eq(0)', tr3);
		var td2  = jQuery('td:eq(1)', tr3);
		var spn1 = jQuery('span', td1);
		var spn2 = jQuery('span', td2);
		var tx1  = jQuery.trim(spn1.text().toUpperCase());
		var tx2  = jQuery.trim(spn2.text().toUpperCase());
		//console.log("tx1: [" + tx1 + "] - txt2: [" + tx2 + "]\n");
		//
		// caracter \ nao eh permitido
		filter = filter.replace(/\\/g, "");
		//
		// i: case-insensitive
		var pattern = new RegExp(filter, "i");
		// O filter devera ser feito apenas na coluna 2 (resultados) e nao nos entrevistados (coluna 1)
		//if ((pattern.test(tx1)) || (pattern.test(tx2))){
		if ( pattern.test(tx2) ){
			// valor correspondeu ... nao filtrar
			return false;
		} else {
			// nao correspondeu ao valor procurado ... filtrar
			return true;
		}
	}
	//
	jQuery.each(rows, function(index, row) {
		jQuery('#tblDadosEntrevistados').children('tbody').append(row);
	});
}
