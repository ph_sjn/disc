<?
session_name("ExtraNet_disc2work_client");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1",true);
$path_pre = "../";
include_once("../config.inc.php"); 
include_once("../lib/db/mysql.inc.php"); 
include_once("../lang/br.inc.php"); 
include_once($path_pre. "lib/checapermissao.php");

$module = $_REQUEST["module"];

$acesso_administrativo = checaPermissao($module,"A",$user_access);
$acesso_normal = checaPermissao($module,"N",$user_access);
$acesso_express = checaPermissao("disc_usuarios","N",$user_access);
$existe = false;

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../../survey/lib/libdisc.php");

include_once("../disc_config/disc_config.php");

//$DW_id_cliente = 0;
$DW_id_avaliacao = $_REQUEST["id"];
$DW_idx = $_REQUEST["idx"];
$DW_filtroCliente = $_REQUEST['filtroCliente'];

?>

<table cellspacing=0 cellpadding=6 border=0 width=80% align='center'>
    <?php
    $objDEFWEB = new DEFWEBavaliacao();
    //
    //$objDEFWEB->IdCliente = $DW_id_cliente;
    $objDEFWEB->Id = $DW_id_avaliacao;
    //
    $objDEFWEB->obterAvaliacao($DW_id_avaliacao);
    $objDEFWEB->obterTokens(TRUE);
    //
    $qtde_concluidos = 0;
    $qtde_naoiniciaram = 0;
    $qtde_respondendo = 0;
    //
    $qtde_total = 0;
    //
    foreach ($objDEFWEB->colTokens As $objToken) {
        //
        if ($objToken->CodigoPublicoAlvo == 2) {
            //
            $qtde_total++;
            //
            $QTDE_saldo = $objToken->QtdeTotalPerguntas - $objToken->QtdePerguntasRespondidas;
            //
            if (($objToken->QtdeTotalPerguntas > 0 && $objToken->QtdePerguntasRespondidas == 0)) {
                $qtde_naoiniciaram++;
            } else {
                if ($QTDE_saldo > 0) {
                    $qtde_respondendo++;
                } else {
                    $qtde_concluidos++;
                }
            }
        }
    }
    //
    $IdTokenFranqueador = $objDEFWEB->IdFranqueador;
    //
    $objTokenFranqueador = $objDEFWEB->obterTokenPeloId($IdTokenFranqueador);
    //
    if($objTokenFranqueador->QtdeTotalPerguntas > 0) {
        $PERC_F_resp = 100 * $objTokenFranqueador->QtdePerguntasRespondidas / $objTokenFranqueador->QtdeTotalPerguntas;
    } else {
        $PERC_F_resp = 100;
    }                        //
    $QTDE_F_saldo = $objTokenFranqueador->QtdePerguntasFaltam; //$objTokenFranqueador->QtdeTotalPerguntas - $objTokenFranqueador->QtdePerguntasRespondidas;
    //
    if(($objTokenFranqueador->QtdeTotalPerguntas > 0 && $objTokenFranqueador->QtdePerguntasRespondidas == 0)) {
        $sSituacaoFranqueador = "Ainda n�o come�ou a responder a pesquisa.";
    } else {
        $sSituacaoFranqueador = number_format($PERC_F_resp, 0, ",", ".") . "% realizado (Faltam ".$QTDE_F_saldo." perguntas).";
    }
    //
    $sLinkFranqueados = "onclick=\"javascript:document.location.href='index.php?module=dwc_franqueados&mode=view&filtroAv=" . $objDEFWEB->Id . "'\" ";
    $sLinkModificar = "onclick=\"javascript:document.location.href='index.php?module=".$module."&mode=forms&id=" . $objDEFWEB->Id . "&filtroCliente=" . $DW_filtroCliente . "'\" ";
    //
    //$PERC_conc = 100 * $qtde_concluidos / $qtde_total;
    $perc_zero = 100 * $qtde_naoiniciaram / $qtde_total;
    $perc_resp = 100 * $qtde_respondendo / $qtde_total;
    $perc_conc = 100 - ($perc_zero + $perc_resp);
    //
    $DW_dt_inicio_avaliacao = "";
    if((!is_null($objDEFWEB->DataInicio))&&($objDEFWEB->DataInicio!="")) {
        $DW_dt_inicio_avaliacao = date("d/m/Y", strtotime($objDEFWEB->DataInicio));
    }
    $DW_dt_fim_avaliacao = "";
    if((!is_null($objDEFWEB->DataFim))&&($objDEFWEB->DataFim!="")) {
        $DW_dt_fim_avaliacao = date("d/m/Y", strtotime($objDEFWEB->DataFim));
    }
    //
    if(($DW_dt_inicio_avaliacao!="")||($DW_dt_fim_avaliacao!="")) {
        // Esta pesquisa podera ser respondida
        $DW_periodo_aplicacao = "Esta pesquisa est&aacute; ajustada para ser respondida ";
        //
        if(($DW_dt_inicio_avaliacao!="")&&($DW_dt_fim_avaliacao!="")) {
            //  no periodo de XX ate XX.
            $DW_periodo_aplicacao .= "no per&iacute;odo";
            $DW_periodo_aplicacao .= " de <b>" . $DW_dt_inicio_avaliacao . "</b>";
            $DW_periodo_aplicacao .= " at&eacute; <b>" . $DW_dt_fim_avaliacao . "</b>";
            //
        } elseif($DW_dt_inicio_avaliacao!="") {
            // a partir de XX.
            $DW_periodo_aplicacao .= "a partir ";
            $DW_periodo_aplicacao .= "de <b>" . $DW_dt_inicio_avaliacao . "</b>";
            //
        } elseif($DW_dt_fim_avaliacao) {
            //ate XX.
            $DW_periodo_aplicacao .= "at&eacute; <b>" . $DW_dt_fim_avaliacao . "</b>";
        }
        //
        $DW_periodo_aplicacao .= ".";
        //
    } else {
        $DW_periodo_aplicacao = "";
    }
 ?>
    <tr>
        <td nowrap colspan="3">
            <?php echo($DW_periodo_aplicacao); ?>
        </td>
    </tr>
    <tr>
        <td nowrap><b>Pesquisa</b></td>
        <!--
        <td nowrap><b>Master</b></td>
        -->
        <td nowrap><b>Usu�rios</b></td>
    </tr>
    <tr>
        <td width="20%">
            <input type='button' 
                name="cmdModificar" value='Modificar' 
                title="Alterar dados desta pesquisa." 
                class="botao"
                <?= $sLinkModificar ?> onmouseover="this.style.cursor='pointer';"
            />
            <br>
        </td>
        <!--
        <td>
            <?php 
                echo($sSituacaoFranqueador); 
            ?>
        </td>
        -->
        <td width="50%">
            <input type='hidden' name='dbgIdCliente' value='<?= $objDEFWEB->IdCliente ?>' />
            <input type='hidden' name='dbgid_cliente' value='<?= $DEFWEB_id_cliente ?>' />
            <?php
            if(($objDEFWEB->IdCliente == $DEFWEB_id_cliente) && ($acesso_express || $acesso_administrativo)) {
            ?>
            <input type='button' 
                name="cmdVerFranqueados" value='Ver franqueados' 
                title="Exibir a lista de entrevistados." 
                class="botao"
                <?= $sLinkFranqueados ?> onmouseover="this.style.cursor='pointer';"
            />
            <br /><br />
            <?php
            }
            ?>
            <table style="border-width: 1px; border-style: solid; border-color: darkblue;">
                <tr>
                    <td width="110" colspan="3" style="text-align: center; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: darkblue;">Resumo</td>
                </tr>
                <?php
                if($qtde_total > 0) {
                ?>
                <tr>
                    <td width="110" style="text-align: center;">Conclu�ram</td>
                    <td width="110" style="text-align: center;">Respondendo</td>
                    <td width="110" style="text-align: center;">N�o iniciaram</td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_conc, 0, ",", ".")."% (".number_format($qtde_concluidos, 0, ",", ".").")"); ?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_resp, 0, ",", ".")."% (".number_format($qtde_respondendo, 0, ",", ".").")"); ?>
                    </td>
                    <td style="text-align: center;">
                        <?php echo(number_format($perc_zero, 0, ",", ".")."% (".number_format($qtde_naoiniciaram, 0, ",", ".").")"); ?>
                    </td>
                </tr>
                <?php
                } else {
                    ?>
                <tr>
                    <td width="330" colspan="3" style="text-align: center; border-bottom-width: 0px; border-bottom-style: solid; border-bottom-color: darkblue;">N�o h� entrevistados cadastrados!</td>
                </tr>
                <?php
                }
                ?>
            </table>
        </td>
    </tr>
	<!--
    <tr>
        <td colspan="3">
            <table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                <tr >
                    <td>
                        <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
                            <tr width="100%" onclick="javascript:exibirRespostas('<?= $module ?>', '<?= $DW_id_avaliacao ?>', '<?= $IdTokenFranqueador ?>', 'divfr_<?= $module ?>_<?= $DW_idx ?>', 'imagemfr_<?= $DW_idx ?>', 'trfr_<?= $DW_idx ?>', '<?= $module ?>');" onmouseover="this.style.cursor = 'pointer'">
                                <td bgcolor="<?= $bgcolor4 ?>">
                                    <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                        <tr>
                                            <td><img id="imagemfr_<?= $DW_idx ?>" src="img/close.gif" border="0">&nbsp;
                                                <b>Respostas Master</b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display:none; padding: 0px;" id="trfr_<?= $DW_idx ?>" bgcolor="<?= $bgcolor6 ?>">
                                <td style="padding: 0px;"><div id="divfr_<?= $module ?>_<?= $DW_idx ?>"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    -->
	<!--
    <tr>
        <td colspan="3">
            <table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                <tr >
                    <td>
                        <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
                            <tr width="100%" onclick="javascript:montarConsolidado('<?= $module ?>', '<?= $DW_id_avaliacao ?>', 'divcx_<?= $module ?>_<?= $DW_idx ?>', 'imagemcx_<?= $DW_idx ?>', 'trcx_<?= $DW_idx ?>', '<?= $module ?>');" onmouseover="this.style.cursor = 'pointer'">
                                <td bgcolor="<?= $bgcolor4 ?>">
                                    <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                        <tr>
                                            <td><img id="imagemcx_<?= $DW_idx ?>" src="img/close.gif" border="0">&nbsp;
                                                <b>Consolidado das respostas dos usu�rios</b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display:none; padding: 0px;" id="trcx_<?= $DW_idx ?>" bgcolor="<?= $bgcolor6 ?>">
                                <td style="padding: 0px;"><div id="divcx_<?= $module ?>_<?= $DW_idx ?>"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	-->
    <tr>
        <td colspan="3">
			<?php
			include_once("inc/projetos_analise.php");
			?>
			<!--
            <table width="98%" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
                <tr >
                    <td>
                        <table width='100%' border=0 cellpadding=10 cellspacing=1 bgcolor='<?= $bgcolor5 ?>' align='center'>
                            <tr width="100%" onclick="javascript:montarAnalise('<?= $module ?>', '<?= $DW_id_avaliacao ?>', 'divan_<?= $module ?>_<?= $DW_idx ?>', 'imageman_<?= $DW_idx ?>', 'tran_<?= $DW_idx ?>', '<?= $module ?>');" onmouseover="this.style.cursor = 'pointer'">
                                <td bgcolor="<?= $bgcolor4 ?>">
                                    <table width="100%" cellpadding="0" cellspacing="0" border=0>
                                        <tr>
                                            <td><img id="imageman_<?= $DW_idx ?>" src="img/close.gif" border="0">&nbsp;
                                                <b>An�lise</b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display:none; padding: 0px;" id="tran_<?= $DW_idx ?>" bgcolor="<?= $bgcolor6 ?>">
                                <td style="padding: 0px;"><div id="divan_<?= $module ?>_<?= $DW_idx ?>"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			-->
        </td>
    </tr>
</table>
