<?
session_name("ExtraNet_discmaster");
session_start();
header("Content-Type: text/html;  charset=ISO-8859-1", true);
$path_pre = "../";
include_once("../config.inc.php");
include_once("../lib/db/mysql.inc.php");
include_once("../lang/br.inc.php");
include_once($path_pre . "lib/checapermissao.php");

$acesso_administrativo = checaPermissao($module, "A", $user_access);
$acesso_normal = checaPermissao($module, "N", $user_access);
$existe = false;

// Biblioteca de classes e fun��es do DefWebExpress
include_once("../discx/libdisc_analise.php");

$DW_id_cliente = 0;
$DW_id_avaliacao = $_REQUEST["id"];

$objDEFWEBmasterAnalise = new DEFWEBmasterAnalise($DW_id_avaliacao);

$objDEFWEBmasterAnalise->obterFranqueados($DW_id_avaliacao);

$objDEFWEBmasterAnalise->carregarEngajamentoFranqueados();

//echo("<pre>");
//print_r($objDEFWEBmasterAnalise);
//echo("</pre><hr>");
?>
<center>
    <table width="80%">
        <tr>
            <td>
                <br>
                <b>ENGAJAMENTO DOS FRANQUEADOS</b><br>
                <br>
            </td>   
        </tr>
        <tr>
            <td>
 
                        <?php
                        $ssTmp = '{';
                        $ssTmpLb = '"labels":[';
                        $ssTmpDt = '"datasets":[{"fillColor": "rgba(220,220,220,0.0)", "strokeColor": "rgba(220,220,220,0)", "pointColor": "rgba(255,000,000,1)","data":[';
                        $numeral = 0;
                        $bPrimeiraLinha = TRUE;
                        foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) {
                            //
                            if (!is_null($objFranqueado->Engajamento)) {
                                if ($objFranqueado->Engajamento->ds_engajamento) {
                                    if($bPrimeiraLinha) {
                                        $bPrimeiraLinha = FALSE;
                        ?>
               <table style="line-height: 25px;" cellspacing="0">
                    <thead style="font-weight: bold;">


                        <tr>
                            <td>
                                Franqueado
                            </td>
                            <td>
                                Score
                            </td>
                            <td>
                                Engajamento
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                }
                        ?>
                            <tr>
                                <td bgcolor='#EEEEEE' style='min-width:50px; text-align:center; border:solid 1px gray;'>
                                    <?php echo($objFranqueado->Nome); 
                                    if($objFranqueado->Unidade != "") {
                                        echo("<br>(" . $objFranqueado->Unidade . ")");
                                    }
                                    ?>
                                </td>

                                <td style='min-width:50px; text-align:center; border:solid 1px gray;'>
                                    <?php echo(number_format($objFranqueado->Engajamento->vl_score, 2, ",", ".")); ?>
                                </td>

                                <td style='min-width:50px; text-align:center; border:solid 1px gray;'>
                                    <?php echo($objFranqueado->Engajamento->ds_engajamento); ?>
                                </td>

                                <?php
                                $numeral = $numeral + 1;
                                $ssTmpLb = $ssTmpLb . '"' . $numeral . '",';
                                $ssTmpDt = $ssTmpDt . number_format($objFranqueado->Engajamento->vl_score, 2, ".", "") . ',';
                                ?>
                            </tr>
                            <?php
                                }
                            }
                        }
                        if(!$bPrimeiraLinha) {
                        ?>
                    </tbody>
                </table>
                <?php
                        } else {
                            echo("NENHUM FRANQUEADO RESPONDEU � PESQUISA!");
                        }
                ?>
            </td>
        </tr>
    </table>


    <br>

    <?php
    if ((1 == 1)&&(!$bPrimeiraLinha)) {
        $ssTmpLb = substr($ssTmpLb, 0, -1) . "],";
        $ssTmpDt = substr($ssTmpDt, 0, -1) . "]}]";
        $ssTmp = $ssTmp . $ssTmpLb . $ssTmpDt . '}';
        echo ("<br>");
        ?>
        <div>
       <?php     
        echo ("<input type='hidden' id='dtGrafic' value='" . $ssTmp . "'>");
        echo ("<canvas id='cvGrafLine' width='700' height='400'></canvas>");
        ?>
        </div>
    <?php     
    }
    ?>

  
    <table width="80%">
        <tr>
            <td>
                <br>
                <b>FRANQUEADOS QUE N�O RESPONDERAM AO ENGAJAMENTO</b><br>
                <br>
            </td>   
        </tr>
        <tr>
            <td>

                <table style="line-height: 25px;" cellspacing="0">
                    <thead style="font-weight: bold;">


                        <tr>
                            <td>
                                Franqueado
                            </td>
                            <td>
                                Score
                            </td>

                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($objDEFWEBmasterAnalise->colFranqueados AS $objFranqueado) { ?>
                            <tr>
                                <?php
                                if (!is_null($objFranqueado->Engajamento)) {
                                    if ($objFranqueado->Engajamento->ds_engajamento) {
                                        
                                    } else {
                                        ?>

                                        <td bgcolor='#EEEEEE' style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                            <?php 
                                            echo($objFranqueado->Nome); 
                                            if($objFranqueado->Unidade != "") {
                                                echo("<br>(" . $objFranqueado->Unidade . ")");
                                            }
                                            ?>
                                        </td>

                                        <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                            Ainda n�o respondeu ao question�rio!
                                        </td>
                                        <?php
                                    }
                                } else {
                                    ?>

                                    <td bgcolor='#EEEEEE' style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                        <?php 
                                        echo($objFranqueado->Nome); 
                                        if($objFranqueado->Unidade != "") {
                                            echo("<br>(" . $objFranqueado->Unidade . ")");
                                        }
                                        ?>
                                    </td>

                                    <td style='min-width:50px; margin:5px; padding:5px; text-align:left; border:solid 1px gray;'>
                                        Ainda n�o respondeu ao question�rio!
                                    </td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table>
            </td>
        </tr>
    </table>

</center>

<?php
/*
<script src="../lib/js/jquery/2.0.3/jquery-2.0.3.min.js"></script>
<script src="../lib/js/jquery/ui.1.10.1/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../lib/js/chart/Chart.min.js"></script>
<script>

    var ssChartOptions = {
        //responsive: true
        
    };

    $(document).ready(function () {
        var data = $("input[id^='dtGrafic']").val();
        //$("input[id^='dtGrafic']").each(function () {
        //console.log("Datasets: " + $(this).val());
        //console.log($(this).attr("id").replace("cvGrafLine","dtGrafic"));
        var ssChartDataSets = JSON.parse(data);
        //console.log(ssChartDataSets[0]);
        //console.log(ssChartDataSets[1]);
        //});
        var ctx = document.getElementById("cvGrafLine").getContext("2d");
        var myLineChart = new Chart(ctx).Line(ssChartDataSets, ssChartOptions);
        //console.log(data);
    });
</script>
*/
?>
