<?php

// Biblioteca de classes e fun��es
include_once("../survey/lib/libdisc.php");

// FUN��O PARA REMOVER CARACTERES ESPECIAIS NO NOME DE ARQUIVO E
// DEFINIR NOMENCLATURA PADR�O: <ID_DA_AVALIACAO>_<TIMESTAMP>_<NOME_DO_ARQUIVO_SEM_CARACTERES ESPECIAIS>
function montarNomeInternoArquivo($PARM_id, $PARM_nomeArquivo) {
    $arquivo_partes_array = pathinfo($PARM_nomeArquivo);
    $PARM_nomeArquivo = ereg_replace("[^A-z0-9_]", "", strtr($arquivo_partes_array['filename'], "��������������������������", "aaaaeeiooouucAAAAEEIOOOUUC"));
    $PARM_nomeArquivo = preg_replace("/[^a-zA-Z0-9]/", "", $PARM_nomeArquivo);
    $PARM_nomeArquivo = $PARM_nomeArquivo . "." . $arquivo_partes_array['extension'];
    $RET_nomeArquivo = $PARM_id . "_" . date('YmdHis') . "_" . $PARM_nomeArquivo;
    return $RET_nomeArquivo;
}

function delete_file($filename) {
    global $upload_dir;
    $path = $upload_dir . "/" . $filename;
    docDelete($path);
}

function copy_file($oldfilename, $olddir, $newfilename, $newdir) {
    if ($olddir <> "") {
        $old_path = $olddir . "/" . $oldfilename;
    } else {
        $old_path = $oldfilename;
    }
    if ($newdir <> "") {
        $new_path = $newdir . "/" . $newfilename;
    } else {
        $new_path = $newfilename;
    }
    docPut($old_path, $new_path);
}

$ARQ_nomeTemp = "";
$ARQ_nomeInterno = "";
$ARQ_nomeOriginal = "";
$ARQ_tipo = "";

// Se veio algum arquivo pelo upload...
if ($_FILES["file_topo_novo"]["tmp_name"] != "") {
    // Obtem o nome do arquivo temporario
    $ARQ_nomeTemp = $_FILES["file_topo_novo"]["tmp_name"];
    // Obtem o nome original
    $ARQ_nomeOriginal = $_FILES["file_topo_novo"]["name"];
    // Obtem o formato do arquivo
    $ARQ_tipo = exif_imagetype($ARQ_nomeTemp);
    // Se nao for PNG(3), JPEG(2) ou GIF(1)...
    if ($ARQ_tipo != 3 && $ARQ_tipo != 2 && $ARQ_tipo != 1) {
        // Mensagem para o usu�rio
        $msg_erro = "O arquivo para a imagem do topo deve estar no formato PNG, JPEG(JPG) ou GIF.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
}

// Se for para excluir a imagem do topo...
if ($btnRemoverArq && $id && !$msg_erro) {
    //
    // Carrega a avalia��o da base de dados
    $DW_objAval = new DEFWEBavaliacao($id);

    // Se obteve a avaliacao...
    if($DW_objAval) {
        // Remove o arquivo temporario
        delete_file($DW_objAval->IMGLogoID);
        // Limpa os campos
        $DW_objAval->atualizarImagemTopo("", "");
        //
        $msg_erro = "Arquivo \"" . $DW_objAval->IMGLogoName . "\" removido com sucesso!";
        //
    } else {
        // Mensagem para o usu�rio
        $msg_erro = "Falhou na leitura dos dados.";
    }
    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    
    // Exibe o formul�rio
    include_once("dsm_projetos/dsm_projetos_forms.php");
}

// Se for inclus�o de projeto/pesquisa...
if ($incluir) {

    // Se o cliente n�o foi selecionado...
    if ($id_cliente == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione o cliente.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se o projeto n�o foi selecionado...
    if ($id_projeto == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione o projeto.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de confirma��o de cadastro n�o foi selecionada...
    if ($id_pag_confcad == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina para confirma��o de cadastro.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de instru��es n�o foi selecionada...
    if ($id_pag_instrucoes == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de instru��es.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de question�rio n�o foi selecionada...
    if ($id_pag_questionario == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de question�rio.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de conclus�o n�o foi selecionada...
    if ($id_pag_conclusao == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina para conclus�o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de relat�rio para o entrevistado n�o foi selecionada...
    if ($id_pag_relquest == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de relat�rio para o entrevistado.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de relat�rio para a administra��o n�o foi selecionada...
    if ($id_pag_relquest == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de relat�rio para a administra��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    
    // Se n�o encontrou nenhum erro at� aqui...
    if (!$msg_erro) {

        // Instancia objeto da classe avaliacao (armazena o projeto/pesquisa)
        $DW_objAval = new DEFWEBavaliacao();
        //
        // Preenche com os dados do formul�rio preenchido pelo usu�rio
        $DW_objAval->IdCliente = $id_cliente;
        $DW_objAval->IdProjeto = $id_projeto;
        $DW_objAval->Titulo = $ds_titulo;
        $DW_objAval->CodigoSituacao = $cd_sit_avaliacao;
        $DW_objAval->IdPaginaConfCad = $id_pag_confcad;
        $DW_objAval->IdPaginaInstrucoes = $id_pag_instrucoes;
        $DW_objAval->IdPaginaQuestionario = $id_pag_questionario;
        $DW_objAval->IdPaginaConclusao = $id_pag_conclusao;
        $DW_objAval->IdPaginaRelEntrevistado = $id_pag_relquest;
        $DW_objAval->IdPaginaRelAdmin = $id_pag_reladmin;
        $DW_objAval->EnviarEmailConclusao = $fl_enviar_email;
        //
        if($dt_inicio != "") {
            //
            $array_dt_inicio = split("/", $dt_inicio);
            //
            $dt_inicio = $array_dt_inicio[2] . "-" . $array_dt_inicio[1] . "-" . $array_dt_inicio[0];
            //
            $DW_objAval->DataInicio = $dt_inicio;
        }
        //
        if($dt_fim != "") {
            //
            $array_dt_fim = split("/", $dt_fim);
            //
            $dt_fim = $array_dt_fim[2] . "-" . $array_dt_fim[1] . "-" . $array_dt_fim[0];
            //
            $DW_objAval->DataFim = $dt_fim;
        }
        // Preenche na cole��o de formul�rios do franqueado no objeto
        // do projeto/pesquisa
        $DW_objAval->colFormularios[2][$id_form_franqueado] = $id_form_franqueado;
        //
        /////
        //
        //  MODELO PADRAO PARA OS E-MAILS
        //  
        //  2018-05-29  MARCIO  Inserido no codigo emergencialmente
        //  
        /////
        //
        if(!($DW_objAval->EmailModeloAssunto)||($DW_objAval->EmailModeloAssunto == "")) {
            $DW_objAval->EmailModeloAssunto = "Acesso � sua avalia��o DISC";
        }
        if(!($DW_objAval->EmailModeloCorpo)||($DW_objAval->EmailModeloCorpo == "")) {
            //
            $DISC_MODELO_EMAIL = "Ol� {NOME}!\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Convidamos voc� a participar de uma avalia��o, atrav�s da metodologia DISC.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "O DISC � uma ferramenta que tra�a o perfil comportamental e assim pode ajudar a potencializar os talentos de cada um, auxiliando no autoconhecimento e na satisfa��o pessoal e profissional.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Para cada quest�o n�o existe uma resposta certa ou errada, por isso pode ser respondido com tranquilidade.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Segue o link de acesso � pesquisa {LINK}.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Obrigado!\n";
            //
            $DW_objAval->EmailModeloCorpo = $DISC_MODELO_EMAIL;
        }
        //
        /////
        //
        //  MODELO PADRAO PARA OS E-MAILS DE CONCLUSAO
        //  
        //  2018-05-29  MARCIO  Inserido no codigo emergencialmente
        //  
        /////
        //
        if(!($DW_objAval->EmailConclusaoAssunto)||($DW_objAval->EmailConclusaoAssunto == "")) {
            $DW_objAval->EmailConclusaoAssunto = "Relat�rio de Perfil Comportamental DISC";
        }
        if(!($DW_objAval->EmailConclusaoCorpo)||($DW_objAval->EmailConclusaoCorpo == "")) {
            //
            $DISC_MODELO_EMAIL = "Ol� {NOME}, tudo bem?\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Agradecemos o tempo que dedicou para participar desta avalia��o DISC.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Voc� poder� acessar o relat�rio com seu Perfil Comportamental (DISC) atrav�s deste {LINK}.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Esperamos que este relat�rio o ajude a ser mais feliz na realiza��o da sua atividade profissional.\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Abra�os,\n";
            $DISC_MODELO_EMAIL .= "\n";
            $DISC_MODELO_EMAIL .= "Equipe disc2work\n";
            $DISC_MODELO_EMAIL .= "- Voc� mais produtivo e feliz no trabalho -\n";
            //
            $DW_objAval->EmailConclusaoCorpo = $DISC_MODELO_EMAIL;
        }
        //
        // Chama o m�todo que cria o projeto/pesquisa e obt�m o ID do PxP (Projeto/Pesquisa)
        $DW_id = $DW_objAval->criarProjeto();
        //
        // Se conseguiu criar o PxP...
        if ($DW_id) {
            //
            // Atualiza os formul�rios do franqueado
            $DW_objAval->atualizarFormularios(2, $DW_objAval->colFormularios[2]);
            //
            // Se falhou...
            if($DW_objAval->ERRO == 1) {
                //
                $msg_erro = "Ocorreu um erro ao atualizar o formul�rio do question�rio da pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
            } else {
                
                // Se veio um arquivo de imagem...
                if($ARQ_nomeOriginal) {
                    // Gera um nome interno para o arquivo
                    $ARQ_nomeInterno = montarNomeInternoArquivo($DW_id, $ARQ_nomeOriginal);
                    // 
                    // Se tiver arquivo atual...
                    if(!is_null($DW_objAval->IMGLogoID) && $DW_objAval->IMGLogoID != "") {
                        // Remove o arquivo atual
                        delete_file($upload_dir.$DW_objAval->IMGLogoID);
                    }
                    //
                    // Copia o arquivo temporario para a area definitiva de armazenamento
                    copy_file($ARQ_nomeTemp, "", $ARQ_nomeInterno, $upload_dir);
                    // Atualiza os campos
                    $DW_objAval->atualizarImagemTopo($ARQ_nomeOriginal, $ARQ_nomeInterno);
                    //
                    if($DW_objAval->ERRO == 1) {
                        //
                        $msg_erro = "Ocorreu um erro ao armazenar a imagem do topo. Caso o problema persista, entre em contato com o administrador.";
                        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                    }
                } else {
                    echo retornaMensagem("Projeto inclu�do com sucesso.");
                }
            }
        } else {
            $msg_erro = "Ocorreu um erro na inclus�o do projeto!<br>";
            if($DW_objval->ERRO_mensagem != "") {
                $msg_erro .= "(" . $DW_objAval->ERRO_mensagem . ")<br>";
            }
            $msg_erro .= "Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
            echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
        }
        // Exibe a lista de PxP
        include_once("dsm_projetos/dsm_projetos_view.php");
        //
    } else {
        // Exibe o formul�rio de PxP
        include_once("dsm_projetos/dsm_projetos_forms.php");
    }
}

// Se for altera��o de PxP (Projeto/Pesquisa)...
if ($salvar) {

    // Se o cliente n�o foi selecionado...
    if ($id_cliente == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione o cliente.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se o projeto n�o foi selecionado...
    if ($id_projeto == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione o projeto.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de confirma��o de cadastro n�o foi selecionada...
    if ($id_pag_confcad == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina para confirma��o de cadastro.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de instru��es n�o foi selecionada...
    if ($id_pag_instrucoes == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de instru��es.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de question�rio n�o foi selecionada...
    if ($id_pag_questionario == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de question�rio.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de conclus�o n�o foi selecionada...
    if ($id_pag_conclusao == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina para conclus�o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de relat�rio para o entrevistado n�o foi selecionada...
    if ($id_pag_relquest == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de relat�rio para o entrevistado.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }
    // Se a p�gina de relat�rio para a administra��o n�o foi selecionada...
    if ($id_pag_relquest == "") {
        // Mensagem para o usu�rio
        $msg_erro = "Selecione a p�gina de relat�rio para a administra��o.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
    }

    // Se n�o encontrou nenhum erro at� aqui...
    if (!$msg_erro) {

        // Carrega a avalia��o da base de dados
        $DW_objAvalAtual = new DEFWEBavaliacao($id);
        // Carrega os formularios da avaliacao
        $DW_objAvalAtual->obterFormularios();
        
        // Indica se algum usuario j� come�ou a responder
        $DW_franqueadoIniciou = $DW_objAvalAtual->pesquisaEmAndamento("*");
        
        //
        // Instancia objeto da classe avaliacao (armazena o projeto/pesquisa)
        $DW_objAval = new DEFWEBavaliacao();
        //
        // Preenche com os dados do formul�rio preenchido pelo usu�rio
        $DW_objAval->IdCliente = $id_cliente;
        $DW_objAval->IdProjeto = $id_projeto;
        $DW_objAval->Titulo = $ds_titulo;
        $DW_objAval->CodigoSituacao = $cd_sit_avaliacao;
        $DW_objAval->IdPaginaConfCad = $id_pag_confcad;
        $DW_objAval->IdPaginaInstrucoes = $id_pag_instrucoes;
        $DW_objAval->IdPaginaQuestionario = $id_pag_questionario;
        $DW_objAval->IdPaginaConclusao = $id_pag_conclusao;
        $DW_objAval->IdPaginaRelEntrevistado = $id_pag_relquest;
        $DW_objAval->IdPaginaRelAdmin = $id_pag_reladmin;
        //
        if(!($fl_enviar_email) || ($fl_enviar_email == "") || is_null($fl_enviar_email) ) {
            //
            $fl_enviar_email = "0";
        }
        //
        $DW_objAval->EnviarEmailConclusao = $fl_enviar_email;
        //
        if($dt_inicio != "") {
            //
            $array_dt_inicio = split("/", $dt_inicio);
            //
            $dt_inicio = $array_dt_inicio[2] . "-" . $array_dt_inicio[1] . "-" . $array_dt_inicio[0];
            //
            $DW_objAval->DataInicio = $dt_inicio;
        }
        //
        if($dt_fim != "") {
            //
            $array_dt_fim = split("/", $dt_fim);
            //
            $dt_fim = $array_dt_fim[2] . "-" . $array_dt_fim[1] . "-" . $array_dt_fim[0];
            //
            $DW_objAval->DataFim = $dt_fim;
        }
        $ids_forms_franqueado = $id_form_franqueado;
        //
        // Preenche na cole��o de formul�rios do franqueado no objeto
        // do projeto/pesquisa
        $DW_objAval->colFormularios[2][$id_form_franqueado] = $id_form_franqueado;
        //
        $id_forms = "";
        // Para cada ID do array de forms do franqueado que est�o no BD...
        foreach ($DW_objAvalAtual->colFormularios[2] AS $objItem) {
            //
            if($id_forms != "") {
                $id_forms .= ",";
            }
            $id_forms .= $objItem->IdFormulario;
        }
        //
        $DW_id_forms_franqueado = $id_forms;
        
//        echo("<hr>DW_objAval:<pre>");
//        print_r($DW_objAval);
//        echo("</pre><hr>");
//        
//        echo("<hr>DW_objAvalAtual(PRE):<pre>");
//        print_r($DW_objAvalAtual);
//        echo("</pre><hr>");

        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro) {
            // Se houve altera��o nas p�ginas da pesquisa...
            if( 
                ($DW_objAval->IdPaginaConfCad != $DW_objAvalAtual->IdPaginaConfCad)
                ||
                ($DW_objAval->IdPaginaInstrucoes != $DW_objAvalAtual->IdPaginaInstrucoes)
                ||
                ($DW_objAval->IdPaginaQuestionario != $DW_objAvalAtual->IdPaginaQuestionario)
                ||
                ($DW_objAval->IdPaginaConclusao != $DW_objAvalAtual->IdPaginaConclusao)
                ||
                ($DW_objAval->IdPaginaRelEntrevistado != $DW_objAvalAtual->IdPaginaRelEntrevistado)
                ||
                ($DW_objAval->IdPaginaRelAdmin != $DW_objAvalAtual->IdPaginaRelAdmin)
                ||
                ($DW_objAval->EnviarEmailConclusao != $DW_objAvalAtual->EnviarEmailConclusao)
                ) {
                // Alterar as paginas da pesquisa
                $DW_objAvalAtual->atualizarPaginas($DW_objAval);
                //
                // Se falhou...
                if($DW_objAvalAtual->ERRO == 1) {
                    //
                    $msg_erro = "Ocorreu um erro ao atualizar as p�ginas da pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                }
            }
            // Se veio um arquivo de imagem...
            if($ARQ_nomeOriginal) {
                // Gera um nome interno para o arquivo
                $ARQ_nomeInterno = montarNomeInternoArquivo($id, $ARQ_nomeOriginal);
                // 
                // Se tiver arquivo atual...
                if(!is_null($DW_objAvalAtual->IMGLogoID) && $DW_objAvalAtual->IMGLogoID != "") {
                    // Remove o arquivo atual
                    delete_file($upload_dir.$DW_objAvalAtual->IMGLogoID);
                }
                //
                // Copia o arquivo temporario para a area definitiva de armazenamento
                copy_file($ARQ_nomeTemp, "", $ARQ_nomeInterno, $upload_dir);
                // Atualiza os campos
                $DW_objAvalAtual->atualizarImagemTopo($ARQ_nomeOriginal, $ARQ_nomeInterno);
                //
                if($DW_objAval->ERRO == 1) {
                    //
                    $msg_erro = "Ocorreu um erro ao armazenar a imagem do topo. Caso o problema persista, entre em contato com o administrador.";
                    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                } else {
                    // Informa o sucesso da operacao
                    $msg_erro = "Arquivo \"" . $ARQ_nomeOriginal . "\" armazenado com sucesso!";
                    echo retornaMensagem($msg_erro);
                }
            }
        }

        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro) {
            // Se houve altera��o nos textos de introducao da pesquisa...
            if( 
                ($DW_objAval->DataInicio != $DW_objAvalAtual->DataInicio)
                ||
                ($DW_objAval->DataFim != $DW_objAvalAtual->DataFim)
                ) {
                // Alterar o texto de introducao da pesquisa
                //
                $DW_objAvalAtual->atualizarDatas($DW_objAval->DataInicio, $DW_objAval->DataFim);
                //
                // Se falhou...
                if($DW_objAvalAtual->ERRO == 1) {
                    //
                    $msg_erro = "Ocorreu um erro ao atualizar as datas da pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                }
            }
        }
        
        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro) {
            // Se houve altera��o no NOME DA PESQUISA...
            if($DW_objAval->Titulo != $DW_objAvalAtual->Titulo) {
                // Alterar o t�tulo da pesquisa
                //
                $DW_objAvalAtual->atualizarTitulo($DW_objAval->Titulo);
                //
                // Se falhou...
                if($DW_objAvalAtual->ERRO == 1) {
                    //
                    $msg_erro = "Ocorreu um erro ao atualizar o t�tulo da pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                }
            }
        }
        
        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro) {
            // Se houve altera��o na SITUA��O DA PESQUISA...
            if($DW_objAval->CodigoSituacao != $DW_objAvalAtual->CodigoSituacao) {
                // Alterar a situa��o da pesquisa
                //
                $DW_objAvalAtual->atualizarSituacao($DW_objAval->CodigoSituacao);
                //
                // Se falhou...
                if($DW_objAvalAtual->ERRO == 1) {
                    //
                    $msg_erro = "Ocorreu um erro ao atualizar a situa��o da pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                    echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                }
            }
        }

        //
        //  Estes podem ser alterados apenas se nenhum franqueado come�ou
        //  a responder a pesquisa:
        //  
        //      7. FORMUL�RIOS DO QUESTIONARIO
        //  

        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro
            &&
               ($ids_forms_franqueado != $DW_id_forms_franqueado) 
            ) {
            if($DW_franqueadoIniciou == TRUE) {
                // Avisa o usu�rio que a pesquisa n�o pode ser alterada
                $msg_erro = "Os formul�rios do entrevistado n�o podem ser alterados porque algum entrevistado j� come�ou a responder a pesquisa!";
                echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
            } else {

                // 7. Formul�rios do franqueado

                // Se houve altera��o nos formul�rios do franqueador...
                if ($ids_forms_franqueado != $DW_id_forms_franqueado) {
                    // Atualiza os formul�rios do franqueado
                    $DW_objAvalAtual->atualizarFormularios(2, $DW_objAval->colFormularios[2]);
                    //
                    // Se falhou...
                    if($DW_objAvalAtual->ERRO == 1) {
                        //
                        $msg_erro = "Ocorreu um erro ao atualizar os formul�rios para os entrevistados. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                    }                    
                }
            }
        }

        // Se n�o encontrou nenhum erro at� aqui...
        if(!$msg_erro) {
            echo retornaMensagem("Dados da pesquisa atualizados com sucesso.");
        }        
    }

    include_once("dsm_projetos/dsm_projetos_forms.php");
}

// Se for exclus�o de PxP (Projeto/Pesquisa)...
if ($excluir) {
    //
    // Carrega a avalia��o da base de dados
    $DW_objAval = new DEFWEBavaliacao($id);
    //
    if($DW_objAval && ($DW_objAval->ERRO == 0)) {
        //
        $DW_objAval->obterTokens(FALSE);
        //
        $DW_remover = TRUE;
        //
        if($DW_objAval->colTokens) {
           //
           if(count($DW_objAval->colTokens) > 0) {
                //
                $DW_remover = FALSE;
                $msg_erro = "N�o � permitido remover uma pesquisa que ainda tenha entrevistados cadastrados.";
                echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
                include_once("dsm_projetos/dsm_projetos_forms.php");
            }
        }
        //
        if($DW_remover) {
            //
            if(!is_null($DW_objAval->IMGLogoID) && $DW_objAval->IMGLogoID != "") {
                // Remove o arquivo temporario
                delete_file($DW_objAval->IMGLogoID);
                // Limpa os campos
                $DW_objAval->atualizarImagemTopo("", "");
            }
            //
            $DW_objAval->removerFormularios(1);
            $DW_objAval->removerFormularios(2);
            $DW_objAval->removerAvaliacao($id);
            //
            // Se falhou...
            if($DW_objAvalAtual->ERRO == 1) {
                //
                $msg_erro = "Ocorreu um erro ao remover a pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
                echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
            } else {
                echo retornaMensagem("Pesquisa removida com sucesso.");
            }
            include_once("dsm_projetos/dsm_projetos_view.php");
        }
    } else {
        $msg_erro = "Ocorreu um erro ao remover a pesquisa. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.";
        echo retornaMensagem("<font color='red'>" . $msg_erro . "</font>");
        include_once("dsm_projetos/dsm_projetos_forms.php");
    }

}

//echo("DW_objAvalAtual:<pre>");
//print_r($DW_objAvalAtual);
//echo("</pre>");
//
//echo("DW_objAval:<pre>");
//print_r($DW_objAval);
//echo("</pre>");