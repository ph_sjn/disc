<br>

<form method=post enctype='multipart/form-data' action='index.php' name='frm'>
<input type=hidden name='module' value='<?= $module ?>'>
<input type=hidden name='mode' value='<?= $mode ?>'>

<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0 width=980>
<tr bgcolor="<?= $bgcolor4 ?>">
	<td style="text-align:center; font-weight: bold;">IMPORTA��O DE USU�RIOS</td>
</tr>
<tr bgcolor="<?= $bgcolor6 ?>">
	<td>

		<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=0 cellpadding=3 border=0 width="100%">
		<tr bgcolor="<?= $bgcolor6 ?>">
			<td style="text-align:right; vertical-align:middle"><b>Arquivo:</b></td>
			<td>
				<input type="file" class="frmFiles" name="arquivo" id="arquivo">
			</td>
		</tr>
		<tr bgcolor="<?= $bgcolor6 ?>">
			<td align='right' style="vertical-align:middle"></td>
			<td>
				
				<br>
				
				<span style="color:red; font-weight: bold;">Siga atentamente as instru��es abaixo:</span>
				
				<br><br>
				
				As colunas na planilha devem ser exatamente iguais ao apresentado no modelo, nesta mesma ordem: <br>
				Coluna A - Nome<br>
				Coluna B - Email<br>
				Coluna C - Login<br>
				Coluna D - Senha<br>
				Coluna E - Perfil<br>
				Coluna F - Grupo
				<br><br>
				Todas as informa��es acima s�o obrigat�rias na planilha.
				<br><br>
				A primeira linha s�o dos t�tulos. Somente a partir da segunda linha s�o considerados os dados v�lidos para importa��o.
				<br><br>
				A senha deve ter at� 8 caracteres, no m�ximo.
				<br><br>
				O login deve ter at� 60 caracteres, no m�ximo.
				<br><br>
				Quando n�o existirem na extranet os grupos ou perfis indicados na planilha, o sistema de importa��o ir� inclu�-los.
				<br><br>
				Para associar os usu�rios na planilha aos grupos ou perfis j� cadastrados na extranet, os nomes t�m que ser exatamente iguais, sem diferen�as de espa�os ou caracteres extras.
				<br><br>
				Para associar um login a mais de um grupo, na coluna Grupo separe-os por barras /, conforme indicado no modelo.
				<br><br>
				Quando um determinado login j� estiver cadastrado na extranet, o mesmo n�o ser� importado.
				<br><br>

				Modelo de arquivo para importa��o: 
				<a href="admin/usuarios.xlsx"><span style="color:red; font-weight: bold;">usuarios.xlsx</span></a>
				
				<br><br>
			</td>
		</tr>
		</table>
		

	</td>
</tr>
<tr bgcolor="<?= $bgcolor6 ?>">
	<td>
		  
		<table border='0' width='100%'>
		<tr>
			<td>
				<input type=button class="botao" value='Voltar' onclick="javascript:document.location.href='index.php?module=usuarios'">
			</td>
			<td style="text-align:right">
				<input type=submit class="botao" name='enviar_planilha' value='Enviar e importar usu�rios'>
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>

</form>

<br><br>



<? if ($_POST['enviar_planilha']) { ?>

	<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0 width=980>
	<tr bgcolor="<?= $bgcolor6 ?>">
		<td>
			<b>ARQUIVO ENVIADO:</b> <?= $_FILES['arquivo']['name'] ?><br><br>
	
			<?
			/*
			$_FILES['arquivo']['tmp_name'] = "admin/Recepcao.xlsx";
			$_FILES['arquivo']['name'] = "Recepcao.xlsx";
			
			$_FILES['arquivo']['tmp_name'] = "admin/dentistasparceiros.xlsx";
			$_FILES['arquivo']['name'] = "dentistasparceiros.xlsx";
			*/
			
			if ($_FILES['arquivo']['tmp_name']) {
				include_once "/home/users/extranet/excel_xlsx/PHPExcel/IOFactory.php";
				
				$objReader07 = new PHPExcel_Reader_Excel2007();
				$objReader07->setReadDataOnly(true);
				
				$arquivo = $_FILES['arquivo']['name'];
				$arquivo_temp = $_FILES['arquivo']['tmp_name'];

				if (strrchr($arquivo,'.') == '.xlsx'){
					$objPHPExcel = $objReader07->load($arquivo_temp);
					$sheet = $objPHPExcel->getActiveSheet();
				}
				
				$formato_valido = true;
				
				echo "Verificando formato da planilha...<br><blockquote>";
				if ( $sheet->getCell('A1')->getValue() == "Nome" ) { echo "Coluna Nome OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Nome n�o encontrada.</span><br>"; $formato_valido = false; }
				if ( $sheet->getCell('B1')->getValue() == "Email" ) { echo "Coluna Email OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Email n�o encontrada.</span><br>"; $formato_valido = false; }
				if ( $sheet->getCell('C1')->getValue() == "Login" ) { echo "Coluna Login OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Login n�o encontrada.</span><br>"; $formato_valido = false; }
				if ( $sheet->getCell('D1')->getValue() == "Senha" ) { echo "Coluna Senha OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Senha n�o encontrada.</span><br>"; $formato_valido = false; }
				if ( $sheet->getCell('E1')->getValue() == "Perfil" ) { echo "Coluna Perfil OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Perfil n�o encontrada.</span><br>"; $formato_valido = false; }
				if ( $sheet->getCell('F1')->getValue() == "Grupo" ) { echo "Coluna Grupo OK<br>"; } else { echo "<span style=\"color: #FF0000\">Coluna Grupo n�o encontrada.</span><br>"; $formato_valido = false; }
				echo "</blockquote>";
				
				// Se o formato da planilha estiver OK, iniciar importa��o dos dados na extranet
				if ( $formato_valido ) { 
				
					echo "Formato da planilha OK.<br><br>Iniciando importa��o de dados...<br><br>";
				
					// Obter grupos e perfis e colocar em uma matriz
					for($j = 2 ; $j < 5000;$j++){

						if ( stristr( utf8_decode( trim( $sheet->getCell('F'. $j)->getValue() ) ) , "/" ) ) {
							$arrayMultiplosGrupos = explode( "/" ,utf8_decode( trim( $sheet->getCell('F'. $j)->getValue() ) ));
							foreach ( $arrayMultiplosGrupos as $grupo ) {
								$arrayGrupos[] = trim($grupo);
							}
						} else {
							$arrayGrupos[] = utf8_decode( trim( $sheet->getCell('F'. $j)->getValue() ) );
						}
						
						$arrayPerfis[] = utf8_decode( trim( $sheet->getCell('E'. $j)->getValue() ) );
						
					}
					
					// Agrupar perfis e grupos encontrados removendo as duplicatas
					$arrayPerfis = array_unique($arrayPerfis);
					$arrayGrupos = array_unique($arrayGrupos);
					
					// Perfis encontrados
					echo "Perfis encontrados: <br><blockquote>";
					foreach ($arrayPerfis as $perfil) {
						echo $perfil . "<br>";
					}
					echo "</blockquote>";
					
					// Grupos encontrados
					echo "Grupos encontrados: <br><blockquote>";
					foreach ($arrayGrupos as $grupo) {
						echo $grupo . "<br>";
					}
					echo "</blockquote>";

					// Cadastrar grupos n�o existentes na extranet
					echo "Importando grupos n�o cadastrados na extranet...<br><blockquote>";
					
					foreach ($arrayGrupos as $grupo) {
						if ( trim($grupo) != "") {

							$sql = "SELECT id FROM firm WHERE firma = \"". $grupo ."\" ";
							$consultaGrupo = db_query($sql);
							if ( db_num_rows( $consultaGrupo ) > 0 ) {
								echo "Grupo <span style=\"color: #0000FF\">". $grupo ."</span> j� existe.";

								$rowIdGrupo = db_fetch_array( $consultaGrupo );

								$idGrupo[$grupo] = $rowIdGrupo['id'];

							} else {
								echo "Grupo <span style=\"color: #FF0000\">". $grupo ."</span> n�o existe. Cadastrando...";

								$sqlInserirGrupo = "INSERT INTO firm (firma, estado) ";
								$sqlInserirGrupo .= "VALUES (\"". $grupo ."\",\"UNIDADE\") ";
								//echo $sqlInserirGrupo . "<br>";
								$resultInserirGrupo = db_query($sqlInserirGrupo);
								if ( $resultInserirGrupo ) {
									$sql = "SELECT id FROM firm WHERE firma = \"". $grupo ."\" ";
									$consultaGrupo = db_query($sql);
									$rowIdGrupo = db_fetch_array( $consultaGrupo );

									$idGrupo[$grupo] = $rowIdGrupo['id'];
									
									echo "OK.";
								} else {
									echo "Ocorreu um erro ao incluir o grupo.";
								}

							}
							echo "<br>";
						}
					}
					echo "</blockquote>";
					
					// Cadastrar perfis n�o existentes na extranet
					echo "Importando perfis n�o cadastrados na extranet...<br><blockquote>";
					foreach ($arrayPerfis as $perfil) {
						if ( trim($perfil) != "") {
						
							$sql = "SELECT id FROM perfil_acesso WHERE descricao = \"". $perfil ."\" ";
							$consultaPerfil = db_query($sql);
							if ( db_num_rows( $consultaPerfil ) > 0 ) {
								echo "Perfil <span style=\"color: #0000FF\">". $perfil ."</span> j� existe.";

								$rowIdPerfil = db_fetch_array( $consultaPerfil );
								$idPerfil[$perfil] = $rowIdPerfil['id'];

							} else {
								echo "Perfil <span style=\"color: #FF0000\">". $perfil ."</span> n�o existe. Cadastrando perfil...";

								$sqlInserirPerfil = "INSERT INTO perfil_acesso (descricao) ";
								$sqlInserirPerfil .= "VALUES ( \"". $perfil ."\" ) ";
								//echo $sqlInserirPerfil . "<br>";
								$resultInserirPerfil = db_query($sqlInserirPerfil);
								if ( $resultInserirPerfil ) {

									$sql = "SELECT id FROM perfil_acesso WHERE descricao = \"". $perfil ."\" ";
									$consultaPerfil = db_query($sql);
									$rowIdPerfil = db_fetch_array( $consultaPerfil );

									$idPerfil[$perfil] = $rowIdPerfil['id'];
									
									echo "OK.";
								} else {
									echo "Ocorreu um erro ao incluir o perfil.";
								}

							}
							echo "<br>";
						}
					}
					echo "</blockquote>";

					
					// Cadastrar usu�rios n�o existentes na extranet
					echo "Importando usu�rios n�o cadastrados na extranet...<br><blockquote>";

					for($j = 2 ; $j < 5000;$j++){
					
						if ( trim($sheet->getCell('A'. $j)->getValue()) ) {
						
							$sql = "SELECT id FROM users WHERE kurz = \"". str_replace("\'","",$sheet->getCell('C'. $j)->getValue()) ."\" ";
							$consultaUsuario = db_query($sql); 
							//echo $sql . "<br>";
							if ( db_num_rows($consultaUsuario) > 0 ) {
								echo " <span style=\"color: blue\">J� existe o usu�rio ". str_replace("\'","",$sheet->getCell('C'. $j)->getValue()) . ".</span><br>";
							} else { 
								echo "<span style=\"color: red\">N�o existe o usu�rio ". str_replace("\'","",$sheet->getCell('C'. $j)->getValue()) .". Inserindo...</span> ";

								// Decodificar dados em UTF8 da planilha, tirar espa�amentos em branco e guardar em vari�veis auxiliares para o SQL
								$nome = trim( str_replace("\'","", utf8_decode($sheet->getCell('A'. $j)->getValue()) ) );
								$email = trim( str_replace("\'","", utf8_decode($sheet->getCell('B'. $j)->getValue()) ) );
								$login = trim( str_replace("\'","", utf8_decode($sheet->getCell('C'. $j)->getValue()) ) );
								$senha = encrypt( utf8_decode($sheet->getCell('D'. $j)->getValue()) , utf8_decode($sheet->getCell('D'. $j)->getValue()) );
								$id_perfil = $idPerfil[ utf8_decode(trim($sheet->getCell('E'. $j)->getValue())) ];
								
								// Inserir os dados do usu�rio no banco de dados
								$sql  = " INSERT INTO users ";
								$sql .= " (vorname, nachname, kurz, pw, ";
								$sql .= " firma, gruppe, email, acc, tel1, tel2, fax, strasse, stadt, plz, land, ";
								$sql .= " sprache, mobil, loginname, ldap_name, anrede, sms, role, proxy, settings, acesso, notificacoes, ";
								$sql .= " corte_geografico, empresa, escritorio, departamento, cargo, radio, disponivel_chat, data_nascimento, ativo) ";
								$sql .= " VALUES ('franqueado', '". $nome ."', '". $login ."', '". $senha ."', ";
								$sql .= " '', 5, '". $email ."', '". $id_perfil ."', '', '', '', '', '', '', '', ";
								$sql .= " '', '', '". $login ."', '', '', '', 0, '', '', '', 'V', ";
								$sql .= " '', '', '', '', '', '', 'S', null, 'S'); ";
								//echo $sql . "<br>";
								$inserirUsuario = db_query($sql);
								
								// Se a inclus�o do usu�rio foi efetuada com sucesso, associ�-lo aos perfis e grupos
								if ( $inserirUsuario ) { 

									echo "<span style=\"color: blue; font-weight: bold;\">OK!</span><br>";
								
									// Se foram especificados mais de um grupo, separ�-los e associ�-los ao usu�rio
									if ( stristr( utf8_decode( trim( $sheet->getCell('F'. $j)->getValue() ) ) , "/" ) ) {
										
										// Obter grupos separados por barras /
										$arrayMultiplosGrupos = explode( "/" ,utf8_decode( trim( $sheet->getCell('F'. $j)->getValue() ) ));
										
										// Iterar grupos obtidos para associar aos usu�rios
										foreach ( $arrayMultiplosGrupos as $grupo ) {
											
											// Associar o grupo ao usu�rio cadastrado
											$sql = "INSERT INTO user_firm (id_user, id_firm) ";
											$sql .= " VALUES ('". str_replace("\'","", utf8_decode($sheet->getCell('C'. $j)->getValue())) ."', '". $idGrupo[ trim($grupo) ] ."' ); ";
											//echo $sql ."<br>";
											$inserirGrupo = db_query($sql);
											
											if ( $inserirGrupo ) {
												echo "<span style=\"color: blue; font-weight: bold;\">Associado ao grupo ". trim($grupo) ."</span><br>";
											} else {
												echo "<span style=\"color: red; font-weight: bold;\">Ocorreu um erro ao inserir o grupo ". trim($grupo) ."</span><br>";
											}
											
										}
										
									} else {
								
										// Associar o grupo ao usu�rio cadastrado
										$sql = "INSERT INTO user_firm (id_user, id_firm) ";
										$sql .= " VALUES ('". str_replace("\'","", utf8_decode($sheet->getCell('C'. $j)->getValue())) ."', '". $idGrupo[ utf8_decode(trim($sheet->getCell('F'. $j)->getValue())) ] ."' ); ";
										//echo $sql ."<br>";
										$inserirGrupo = db_query($sql);
										
										if ( $inserirGrupo ) {
											echo "<span style=\"color: blue; font-weight: bold;\">Associado ao grupo ". utf8_decode(trim($sheet->getCell('F'. $j)->getValue())) ."</span><br>";
										} else {
											echo "<span style=\"color: red; font-weight: bold;\">Ocorreu um erro ao inserir o grupo ". utf8_decode(trim($sheet->getCell('F'. $j)->getValue())) ."</span><br>";
										}

									}
									
								} else {
									echo "<span style=\"color: red; font-weight: bold;\">Ocorreu um erro ao incluir o usu�rio.</span>";
								}
								echo "<br>";

							}
							
						}
					}
					echo "</blockquote>";

				} else {
					echo retornaMensagem("<span style=\"color: red; font-weight: bold;\">Por favor corrija o formato da planilha seguindo os mesmos padr�es conforme o modelo <a href=\"admin/usuarios.xlsx\"><span style=\"color:blue; font-weight: bold;\">usuarios.xlsx</span></a>.</span>");
				}
		
			} else {
				echo retornaMensagem("<font color='red'><b>Envie um arquivo</b></font>") ."<br>";
			}
			?>
		</td>
	</tr>
	</table><?
}
?>

<br><br>
