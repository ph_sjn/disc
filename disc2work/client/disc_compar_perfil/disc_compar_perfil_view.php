<?
$acesso_administrativo = checaPermissao($module, "A", $user_access);

$REQ_id_cliente 	= $_REQUEST["idc"];
$REQ_filtroAvA  	= $_REQUEST['filtroAvA'];
$REQ_filtroAvB  	= $_REQUEST['filtroAvB'];
$REQ_selIDPerfilA  	= $_REQUEST['selIDPerfilA'];
$REQ_OptPerfilA 	= $_REQUEST['optPerfilA'];
$REQ_OptPerfilB 	= $_REQUEST['optPerfilB'];
$REQ_comparar		= $_REQUEST['comparar'];

if (!isset($REQ_OptPerfilA)){
	$REQ_OptPerfilA = 0;
}
if (!isset($REQ_OptPerfilB)){
	$REQ_OptPerfilB = 1;
}

$GRAFICO_EXIBIR = 1;
//
include_once("/home/users/__discx/inc/libDiscX.php");
//
//-------------------------------------------------------------------------------------------------
// Listar Clientes
//-------------------------------------------------------------------------------------------------
if($DEFWEB_master_user) {
    //
    $colClientes = DISCX_listarClientes();
    $colClientesDecoded = json_decode($colClientes);
    //
    $sHTMLidc = "";
    //
    foreach ($colClientesDecoded AS $objCliente) {
        //
        if ($REQ_id_cliente == $objCliente->Codigo || $REQ_id_cliente == "") {
            //
            $REQ_id_cliente = $objCliente->Codigo;
            //
            $sAttrSelected = " SELECTED";
        } else {
            $sAttrSelected = "";
        }
        //
        $sHTML = "<OPTION VALUE='";
        $sHTML .= $objCliente->Codigo;
        $sHTML .= "'" . $sAttrSelected;
        $sHTML .= ">" . $objCliente->Nome . "</OPTION>";
        //
        $sHTMLidc .= $sHTML;
    }
}
//-------------------------------------------------------------------------------------------------
// Listar Pesquisas
//-------------------------------------------------------------------------------------------------
$sHTMLavaliacoesA = "";
$sHTMLavaliacoesB = "";
if($REQ_id_cliente != "" || $DEFWEB_cd_cliente != "") {
    //
    if($DEFWEB_master_user) {
        //
        $colPesquisas = DWX_obterPesquisas(1, $REQ_id_cliente, "");
    } else {
        $colPesquisas = DWX_obterPesquisas(1, $DEFWEB_cd_cliente, "");
    }
    $colPesquisasDecoded = json_decode($colPesquisas, false);
    //
    foreach ($colPesquisasDecoded AS $objAvaliacaoItem) {
        //
        if ($REQ_filtroAvA == $objAvaliacaoItem->IdAvaliacao || $REQ_filtroAvA == "") {
            $REQ_filtroAvA = $objAvaliacaoItem->IdAvaliacao;
            $sAttrSelectedA = " SELECTED";
        } else {
            $sAttrSelectedA = "";
        }
		//
        if ($REQ_filtroAvB == $objAvaliacaoItem->IdAvaliacao || $REQ_filtroAvB == "") {
            $REQ_filtroAvB = $objAvaliacaoItem->IdAvaliacao;
            $sAttrSelectedB = " SELECTED";
        } else {
            $sAttrSelectedB = "";
        }
        //
        $sHTMLA = "<OPTION VALUE='";
        $sHTMLA .= $objAvaliacaoItem->IdAvaliacao;
        $sHTMLA .= "'" . $sAttrSelectedA;
        $sHTMLA .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
		//
        $sHTMLB = "<OPTION VALUE='";
        $sHTMLB .= $objAvaliacaoItem->IdAvaliacao;
        $sHTMLB .= "'" . $sAttrSelectedB;
        $sHTMLB .= ">" . $objAvaliacaoItem->Titulo . "</OPTION>";
        //
        $sHTMLavaliacoesA .= $sHTMLA;
		$sHTMLavaliacoesB .= $sHTMLB;
    }
}
//-------------------------------------------------------------------------------------------------
// Listar Funcoes
//-------------------------------------------------------------------------------------------------
$colFuncoes = DISCX_listarPerfisDeFuncao(1, "", $REQ_id_cliente, "");
$colFuncoesDecoded = json_decode($colFuncoes, false);
//
$sHTMLselPerfilA = "";
//
foreach ($colFuncoesDecoded AS $objFuncoes) {
	//
    if ($REQ_selIDPerfilA == $objFuncoes->IdPerfilFuncao || $REQ_selIDPerfilA == "") {
        $REQ_selIDPerfilA = $objFuncoes->IdPerfilFuncao;
        $sAttrSelectedA = " SELECTED";
    } else {
        $sAttrSelectedA = "";
    }
    //
    $sHTMLA = "<OPTION VALUE='";
    $sHTMLA .= $objFuncoes->IdPerfilFuncao;
    $sHTMLA .= "'" . $sAttrSelectedA;
    $sHTMLA .= ">" . $objFuncoes->Nome . "</OPTION>";
	//
    $sHTMLselPerfilA .= $sHTMLA;
}
//
//
$objDWC = new clsDWCmaster();
//
$sLabelPerfilA = "Fun��es:";
if ( $REQ_OptPerfilA == 1 ) {
	$sLabelPerfilA = "Entrevistado:";
	//-------------------------------------------------------------------------------------------------
	// Listar Entrevistados
	//-------------------------------------------------------------------------------------------------
	$colEntrevistadosA = $objDWC->obterTokens($REQ_filtroAvA);
	$sHTMLselPerfilA = "";
	//
	foreach ($colEntrevistadosA AS $objEntrevistado) {
		//
		if ($REQ_selIDPerfilA == $objEntrevistado->Id || $REQ_selIDPerfilA == "") {
			$REQ_selIDPerfilA = $objEntrevistado->Id;
			$sAttrSelectedA = " SELECTED";
		} else {
			$sAttrSelectedA = "";
		}
		//
		$sHTMLA = "<OPTION VALUE='";
		$sHTMLA .= $objEntrevistado->Id;
		$sHTMLA .= "'" . $sAttrSelectedA;
		$sHTMLA .= ">" . $objEntrevistado->Nome . "</OPTION>";
		//
		$sHTMLselPerfilA .= $sHTMLA;
	}
}

include_once("disc_config/dw_dlg_aguarde_msg.php");

?>

<script src="lib/js/chart/Chart.bundle.js"></script>
<script src='disc_compar_perfil/js/compar_perfil.js' type='text/javascript'></script>
<script src='../../survey/js/dssurvey_graph_v1.js' type='text/javascript'></script>

<form action='index.php' method='post' id="frm" name='frm'>
    <input type="hidden" name="module" value="<?= $module ?>">
	<input type="hidden" name="comparar" value="">
    <input type="hidden" name="mode" value="view">
	<input type="hidden" name="hdnGraficoCfg" id="hdnGraficoCfg" value="<?php echo($GRAFICO_EXIBIR); ?>" />

	<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
		<tr>
			<td style="text-align:right">
			
				<table align="right">
					<tr>
						<td style="text-align: right; font-weight: bold; vertical-align:middle;">

							Cliente: 
							<?php
							if($DEFWEB_master_user) {
							?>
								<select name='idc' class="combo" onchange="javascript:executarSubmit();">
									<?php
									echo($sHTMLidc);
									?>
								</select>
							<?php
							} else {
								echo(" [<font style=\"font-weight: normal;\">" . $DEFWEB_cd_cliente . "</font>]");
							}
							?>

						</td>
                        <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
                            <input type='Submit' name="acaoAtualizar" value='Atualizar a lista' 
                                   title="Atualizar a listagem" 
                                   class="botao"
                                   onclick="javascript:dw_dlg_exibirAguarde()"
                                   >
                        </td>
					</tr>
				</table>
	
			</td>
		</tr>
	</table>
    <br>
    <table border=0 cellpadding=0 cellspacing=1 width='98%' align='center'>
        <tr align='center'>
            <td>
			
				<table width="100%">
					<tr>
						<td>
						
							<table width="100%" cellpadding=5 cellspacing=0 style='border:solid 1px gray; line-height: 22px;'>
								<thead style="font-weight: bold;">
									<tr>
										<td bgcolor="<?php echo($bgcolor4); ?>" style='padding:5px;' colspan="2">
											Perfil A Selecionado
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="text-align: center;" colspan="2">
											<input type="radio" name="optPerfilA" value="0" <?php if ( $REQ_OptPerfilA == 0) echo("checked") ?>><b>Fun��o</b>
											<input type="radio" name="optPerfilA" value="1" <?php if ( $REQ_OptPerfilA == 1) echo("checked") ?>><b>Entrevistado</b>
										</td>
									</tr>
									<tr>
										<td style="text-align: right; font-weight: bold; vertical-align:middle;">
											Pesquisa:
										</td>
										<td style="text-align: left; font-weight: bold; vertical-align:middle;">
											<select name='filtroAvA' class="combo" onchange="javascript:executarSubmit();" <?php if ( $REQ_OptPerfilA == 0) echo("disabled") ?>>
											<?php
												echo($sHTMLavaliacoesA);
											?>
											</select>
										</td>
									</tr>
									<tr>
										<td style="text-align: right; font-weight: bold; vertical-align:middle;">
											<?=$sLabelPerfilA;?>
										</td>
										<td style="text-align: left; font-weight: bold; vertical-align:middle;">
											<select name='selIDPerfilA' class="combo">
											<?php
												echo($sHTMLselPerfilA);
											?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							
						</td>
						<td>
						</td>
						<td>
							
							<table width="100%" cellpadding=5 cellspacing=0 style='border:solid 1px gray; line-height: 22px;'>
								<thead style="font-weight: bold;">
									<tr>
										<td bgcolor="<?php echo($bgcolor4); ?>" style='padding:5px;' colspan="2">
											Perfil B Comparar
										</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="text-align: center;" colspan="2">
											<input type="radio" name="optPerfilB" value="0" <?php if ( $REQ_OptPerfilB == 0) echo("checked") ?>><b>Fun��o</b>
											<input type="radio" name="optPerfilB" value="1" <?php if ( $REQ_OptPerfilB == 1) echo("checked") ?>><b>Entrevistado</b>
										</td>
									</tr>
									<tr>
										<td style="text-align: right; font-weight: bold; vertical-align:middle;">
											Pesquisa:
										</td>
										<td style="text-align: left; font-weight: bold; vertical-align:middle;">
											<select name='filtroAvB' class="combo" onchange="javascript:executarSubmit();">
											<?php
												echo($sHTMLavaliacoesB);
											?>
											</select>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<input type='button' name="cmdComparar" value='Comparar' 
												title="Comparar Perfis" 
												class="botao" 
											>
										</td>
									</tr>
								</tbody>
							</table>
							
						</td>
					</tr>
					<tr>
						<td colspan="3">
                <?php
				if ( $REQ_comparar == 1 ) {
					//
					$flagPerfilA = false;
					//
					$objDWCPerfil = new clsDWCPerfilDisc();
					//
					// Dados do Perfil A
					if ( $REQ_OptPerfilA == 0 ){
						// Perfil de Funcao
						$objPerfilDeFuncaoTmp = DISCX_consultarPerfilDeFuncao("1", $REQ_selIDPerfilA);
						$objPerfilDeFuncao = json_decode($objPerfilDeFuncaoTmp, false);
						//
						$objDWCPerfil->setPerfilX($objPerfilDeFuncao->Nome, intval($objPerfilDeFuncao->DISC_D), intval($objPerfilDeFuncao->DISC_I),  intval($objPerfilDeFuncao->DISC_S), intval($objPerfilDeFuncao->DISC_C));
						if ( $objDWCPerfil->Erro == 0 ){
							$flagPerfilA = true;
						}
						//
					} else {
						// Perfil de Entrevistado
						$objDWsurvey = new DEFWEBsurvey($REQ_filtroAvA, $REQ_selIDPerfilA);
						if ($objDWsurvey->qt_nao_respondidas == 0) {
							//
							$objDiscNormalizado = new DEFWEBdisc_normalizado();
							$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
							//
							$objDWCPerfil->setPerfilX($objDWsurvey->ds_nome_token, $objDiscNormalizado->colDISC_convertido["D"], $objDiscNormalizado->colDISC_convertido["I"],  $objDiscNormalizado->colDISC_convertido["S"], $objDiscNormalizado->colDISC_convertido["C"]);
							if ( $objDWCPerfil->Erro == 0 ){
								$flagPerfilA = true;
							}
						}
					}
					//
					// Dados do Perfil B
					if ( $flagPerfilA ) {
						$flagPerfilB = false;
						//
						if ( $REQ_OptPerfilB == 0 ){
							// Perfil de Funcao
							//
							if($DEFWEB_master_user) {
								$Cliente = $REQ_id_cliente;
							} else {
								$Cliente = $DEFWEB_cd_cliente;
							}
							//
							$objPerfilDeFuncaoTmp = DISCX_listarPerfisDeFuncao("1", "", $Cliente, "");
							$objPerfilDeFuncao = json_decode($objPerfilDeFuncaoTmp, false);
							//
							foreach ($objPerfilDeFuncao As $objItem) {
								$flagPerfilB = false;
								$objDWCPerfil->setPerfilY($objItem->Nome, intval($objItem->DISC_D), intval($objItem->DISC_I),  intval($objItem->DISC_S), intval($objItem->DISC_C));
								if ( $objDWCPerfil->Erro == 0 ){
									$objDWCPerfil->correlacionar();
									$flagPerfilB = true;
								}
							}
							//
						} else {
							// Perfil de Entrevistado
							$colEntrevistadosB = $objDWC->obterTokens($REQ_filtroAvB);
							foreach ($colEntrevistadosB AS $objEntrevistado) {
								$flagPerfilB = false;
								unset($objDWsurvey);
								$objDWsurvey = new DEFWEBsurvey($REQ_filtroAvB, $objEntrevistado->Id);
								if ($objDWsurvey->qt_nao_respondidas == 0) {
									//
									unset($objDiscNormalizado);
									$objDiscNormalizado = new DEFWEBdisc_normalizado();
									$objDiscNormalizado->normalizar_disc($objDWsurvey->colDISC, $objDWsurvey->dv_sexo);
									//
									$objDWCPerfil->setPerfilY($objDWsurvey->ds_nome_token, $objDiscNormalizado->colDISC_convertido["D"], $objDiscNormalizado->colDISC_convertido["I"],  $objDiscNormalizado->colDISC_convertido["S"], $objDiscNormalizado->colDISC_convertido["C"]);
									if ( $objDWCPerfil->Erro == 0 ){
										$objDWCPerfil->correlacionar();
										$flagPerfilB = true;
									}
								}
							}
						}
					}
					//
					if ( $objDWCPerfil->Erro == 0 && count($objDWCPerfil->colPerfisComparado) > 0 ){
						//
						$tr_hover = " onmouseover=\"this.style.cursor='pointer';\"";
						//
						$total_linhas   = 0;
						$bPrimeiraLinha = TRUE;
						//
						foreach ($objDWCPerfil->colPerfisComparado AS $objItem) {
								//
								if ( $objItem->correlacao > 0 ){
									//
									$total_linhas++;
									//
									/*
									$sLetters = $objItem->resultadoY;
									$result_fmt = "";
									for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
										if ( $nLetter == 0 ) {
											$result_fmt = "<span class=\"disc_letter_main\">";
											$result_fmt .= substr($sLetters, $nLetter, 1);
											$result_fmt .= "</span>";
										} else {
											$result_fmt .= "<span class=\"disc_letter_norm\">";
											$result_fmt .= substr($sLetters, $nLetter, 1);
											$result_fmt .= "</span>";
										}
									}
									*/
									//
									if ($bPrimeiraLinha) {
										$bPrimeiraLinha = false;
									?>
									<table border=0 cellpadding=5 cellspacing=0>
										<tr>
											<td>
												<input type='button' name="cmdOpenAll" value='Abrir todos' title="Abrir todos." class="botao" onmouseover="this.style.cursor='pointer';">
											</td>
											<td>
												<input type='button' name="cmdCloseAll" value='Fechar todos' title="Fechar todos." class="botao" onmouseover="this.style.cursor='pointer';">
											</td>
										</tr>
									</table>
									<table width='100%' border=0 cellpadding=5 cellspacing=1 bgcolor='<?php echo($bgcolor5); ?>' align='center'>
										<thead style="font-weight: bold;">
											<tr bgcolor="<?php echo($bgcolor4); ?>">
												<td style='padding:5px;'>
													Nome
												</td>
												<td style='padding:5px;'>
													Perfil
												</td>
												<td style='padding:5px;'>
													Correla��o
												</td>
											</tr>
										</thead>
										<tbody>
									<?php
									}
									?>
											<tr bgcolor="<?php echo($bgcolor6); ?>" <?= $tr_hover ?>>
												<td onclick="javascript:perfil('imgPerfil_<?= $total_linhas ?>', 'trPerfil_<?= $total_linhas ?>');">
													<img id="imgPerfil_<?php echo($total_linhas);?>" src="img/close.gif" border="0">&nbsp;
													<?php echo($objItem->nomeY);?>
												</td>
												<td>
													<?php echo($objItem->resulYHtml);?>
												</td>
												<td>
													<?php echo(number_format($objItem->correlacao * 100) . "%");?>
												</td>
											</tr>
											<?php if($GRAFICO_EXIBIR == 1) {?>
											<tr style="display:none; padding: 0px;" id="trPerfil_<?php echo($total_linhas);?>" bgcolor="<?php echo($bgcolor6); ?>">
												<td colspan="3">
													
													<table border=0 cellpadding=5 cellspacing=0 align="center">
														<thead style="font-weight: bold;">
															<tr>
																<td>
																	<?php echo($objItem->nomeX);?>
																</td>
																<td>
																	<?php echo($objItem->nomeY);?>
																</td>
																<td>
																</td>
															<tr>
														</thead>
														<tbody>
															<tr>
																<td style="text-align: center;">
																	<?php 
																	/*
																	$nLetter = $objItem->resultadoX;
																	$result_fmt = "";
																	for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
																		if ( $nLetter == 0 ) {
																			$result_fmt = "<span class=\"disc_letter_main\">";
																			$result_fmt .= substr($sLetters, $nLetter, 1);
																			$result_fmt .= "</span>";
																		} else {
																			$result_fmt .= "<span class=\"disc_letter_norm\">";
																			$result_fmt .= substr($sLetters, $nLetter, 1);
																			$result_fmt .= "</span>";
																		}
																	}
																	*/
																	echo($objItem->resulXHtml);
																	?>
																</td>
																<td style="text-align: center;">
																	<?php 
																	/*
																	$nLetter = $objItem->resultadoY;
																	$result_fmt = "";
																	for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
																		if ( $nLetter == 0 ) {
																			$result_fmt = "<span class=\"disc_letter_main\">";
																			$result_fmt .= substr($sLetters, $nLetter, 1);
																			$result_fmt .= "</span>";
																		} else {
																			$result_fmt .= "<span class=\"disc_letter_norm\">";
																			$result_fmt .= substr($sLetters, $nLetter, 1);
																			$result_fmt .= "</span>";
																		}
																	}
																	*/
																	echo($objItem->resulYHtml);
																	?>
																</td>
																<td>
																</td>
															</tr>
															<tr>
																<td>
																	<?php
																	echo("<input type='hidden' id='grfAnX" . $total_linhas . "' value='". $objItem->serieX ."' >");
																	echo("<canvas id='canvAnX" . $total_linhas . "' height='300'></canvas>");
																	?>
																</td>
																<td>
																	<?php
																	echo("<input type='hidden' id='grfAnY" . $total_linhas . "' value='". $objItem->serieY ."' >");
																	echo("<canvas id='canvAnY" . $total_linhas . "' height='300'></canvas>");
																	?>
																</td>
																<td>
																	<div>
																	<?php
																	echo("<input type='hidden' id='grfCompar" . $total_linhas . "' value='". $objItem->serieXY ."' >");
																	echo("<canvas id='canvCompar" . $total_linhas . "' height='300'></canvas>");
																	?>
																	</div>
																</td>
															</tr>
															<tr>
																<td colspan="2">
																	<table border=0 cellpadding=5 cellspacing=0 style='border:solid 1px gray;' align="center">
																		<thead style="font-weight: bold;">
																			<tr>
																				<td colspan="2" style="text-align: center;" bgcolor="<?php echo($bgcolor4); ?>">
																					Compara��o dos Fatores DISC
																				</td>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td style="font-weight: bold; text-align: right;">
																					Domin�ncia
																				</td>
																				<td>
																					<?php echo($objItem->colPerfilX["D"] - $objItem->colPerfilY["D"]);?>
																				</td>
																			</tr>
																			<tr>
																				<td style="font-weight: bold; text-align: right;">
																					Influ�ncia
																				</td>
																				<td>
																					<?php echo($objItem->colPerfilX["I"] - $objItem->colPerfilY["I"]);?>
																				</td>
																			</tr>
																			<tr>
																				<td style="font-weight: bold; text-align: right;">
																					Estabilidade
																				</td>
																				<td>
																					<?php echo($objItem->colPerfilX["S"] - $objItem->colPerfilY["S"]);?>
																				</td>
																			</tr>
																			<tr>
																				<td style="font-weight: bold; text-align: right;">
																					Conformidade
																				</td>
																				<td>
																					<?php echo($objItem->colPerfilX["C"] - $objItem->colPerfilY["C"]);?>
																				</td>
																			<tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
													
												</td>
											</tr>
										<?php
											} // if grafico exibir
								} // if correlacao
								//
						} // foreach perfis
					}
					//
					if ( $total_linhas == 0 ) {
					?>
						<table width='100%' border=0 cellpadding=10 cellspacing=0>
							<tr>
								<td style="font-weight: bold; text-align: center;">
									N�O FOI ENCONTRADO NENHUM PERFIL!
								</td>
							</tr>
						</table>
					<?php
					} else {
					?>
							</tbody>
						</table>
						<br><br>
						<div style="text-align: center; font-weight: bold;">Total de registros encontrados: <span style="color:#FF0000"><?= $total_linhas ?></span> </div>
						<br><br>
					<?php
					}
				} // REQ_comparar
			?>

						</td>
					</tr>
				</table>
            </td>
        </tr>
    </table>
</form>