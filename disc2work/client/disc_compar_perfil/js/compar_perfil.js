//
jQuery('document').ready( function(){
	configBtnPerfis();
	configRadioPerfilA();
	configRadioPerfilB();
	configBtnComparar();
});
//
//
// Abre/Fecha o entrevistado selecionado
function perfil(img, tr){
    browser = "NAVEGADOR" + navigator.userAgent;
	var element = document.getElementById(tr);
    var ico = document.getElementById(img);
    if (element.style.display == 'none') {
        if (browser.indexOf("MSIE") > 0)
            element.style.display = "block";
        else
            element.style.display = 'table-row';
		ico.setAttribute('src','img/open.gif');
		montarGraficosAnalise();
		montarGraficosComparacao();
    } else {
        element.style.display = 'none';
        ico.setAttribute('src', 'img/close.gif');
    }
}
//
function configBtnComparar(){
	var btn     = jQuery('input[name=cmdComparar]');
	var compar  = jQuery('input[name=comparar]');
	compar.val("");
	if ( btn !== undefined ){
		btn.bind( "click", function() {
			compar.val("1");
			executarSubmit();
		});
	}
}
//
function configBtnPerfis(){
	if ( jQuery("tr[id^='trPerfil_']").length > 0 ) {
		var btnOpen  = jQuery('input[name=cmdOpenAll]');
		var btnClose = jQuery('input[name=cmdCloseAll]');
		if ( btnOpen !== undefined ){
			btnOpen.bind( "click", function() {
				// Abre a analise de todos os entrevistados
				jQuery("tr[id^='trPerfil_']").show();
				jQuery("img[id^='imgPerfil_']").attr("src","img/open.gif");
				montarGraficosAnalise();
				montarGraficosComparacao();
			});
			if ( btnOpen.css('display') == 'none' ){
				btnOpen.show();
			}
		}
		if ( btnClose !== undefined ){
			btnClose.bind( "click", function() {
				// Fecha a analise de todos os entrevistados
				jQuery("tr[id^='trPerfil_']").hide();
				jQuery("img[id^='imgPerfil_']").attr("src","img/close.gif");
			});
			if ( btnClose.css('display') == 'none' ){
				btnClose.show();
			}
		}
	}
}
//
function configRadioPerfilA(){
	jQuery('input[name=optPerfilA]').click(function(){
		var optPerfil = jQuery('input[name=optPerfilA]:checked').val();
		if (optPerfil == 0){
			// Perfil de Funcoes
			jQuery('select[name=filtroAvA]').prop("disabled", true);
		} 
		else if (optPerfil == 1){
			// Perfil de Entrevistados
			jQuery('select[name=filtroAvA]').prop("disabled", false);
		}
		executarSubmit();
	});
}
function configRadioPerfilB(){
	jQuery('input[name=optPerfilB]').click(function(){
		var optPerfil = jQuery('input[name=optPerfilB]:checked').val();
		if (optPerfil == 0){
			// Perfil de Funcoes
			jQuery('select[name=filtroAvB]').prop("disabled", true);
		} 
		else if (optPerfil == 1){
			// Perfil de Entrevistados
			jQuery('select[name=filtroAvB]').prop("disabled", false);
		}
	});
}
//
function executarSubmit() {
	var objForm = document.getElementById("frm");
	dw_dlg_exibirAguarde();
	objForm.submit();
}
//