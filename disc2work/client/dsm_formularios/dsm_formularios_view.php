<?
$acesso_administrativo = checaPermissao($module,"A",$user_access); 
?>


<table border=0 cellpadding=0 cellspacing=0 width='98%' align='center'>
<?
/*
<tr>
	<td style="vertical-align:middle;">
		<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=forms"><span style="font-weight:bold;">CADASTRAR PONTOS</span></a>
		&nbsp; &nbsp; &nbsp; 
		<a href="index.php?module=<?= $_REQUEST['module'] ?>&mode=importacao"><span style="font-weight:bold;">IMPORTAR PONTOS...</span></a>			
	</td>
</tr>
*/
?>
<tr>
	<td style="text-align:right">
	
		<form action='index.php' method='post' name='frm'>
		<input type="hidden" name="module" value="<?= $module ?>">	
	
		<table align="right">
		<tr>

		  <td style="text-align: right; font-weight: bold; vertical-align:middle;"> 
		  
				Agrupamento: 
				<select name='filtroAgrupamento' class="combo">
				<option value=''>Todos</option>
				<?
				$sql = "SELECT cd_agrupamento, ds_agrupamento FROM tbdw_agrupamento ORDER BY ds_agrupamento ASC";
				$resultAg = db_query($sql) or db_die();
				while ($rowAg = db_fetch_array($resultAg) ){
					if ($_POST['filtroAgrupamento'] == $rowAg['cd_agrupamento'])
						$selected = "selected";
					else
						$selected = "";
					echo "<option value='". $rowAg['cd_agrupamento'] ."' $selected>". $rowAg['ds_agrupamento'] ."</option> " ;
				}
				?>
				</select>
			</td>
                        <?php
                        /*
			<td style="vertical-align:middle;">
				<input type="checkbox" name="checkPontosSemGeolocalizacao" value="S" <? if ( $_POST['checkPontosSemGeolocalizacao'] ) { ?> checked <? } ?>> 
			</td>
			<td style="vertical-align:middle;">
				Pontos sem geolocalização 
			</td>
                        */
                        ?>
			<td style="vertical-align:middle;">
				<input type='Submit' name="filtrarFormularios" value='Filtrar' class="botao">
			</td>
		</tr>
		</table>
		
		</form>
	
	</td>
</tr>
</table>

</form>

<br>



<table border=0 cellpadding=0 cellspacing=1 width='98%' bgcolor="<?= $bgcolor5 ?>" align='center'>
<tr align='center' bgcolor="<?= $bgcolor4 ?>">
	<td>

		<table border=0 cellpadding=4 cellspacing=0 width='100%' bgcolor="<?= $bgcolor5 ?>" align='center'>
		<tr bgcolor="<?= $bgcolor4 ?>">
			<td style="font-weight: bold;">FORMULÁRIO</td>
			<td style="font-weight: bold;">SITUAÇÃO</td>
			<td style="font-weight: bold;">AGRUPAMENTO</td>
			<td style="font-weight: bold;">TEMA</td>
			<td style="font-weight: bold;">ITEM</td>
                        <td style="font-weight: bold;">ID.ITEM</td>
                        <td style="font-weight: bold;">NOTA</td>
		</tr>
		<?
		$sql = "SELECT";
                $sql .= " form.id_formulario,";
                $sql .= " form.ds_formulario,";
                $sql .= " sit.ds_sit_formulario,";
                $sql .= " ag.ds_agrupamento,";
                $sql .= " itemform.id_item_formulario,";
                $sql .= " tema.ds_tema,";
                $sql .= " item.ds_item,";
                $sql .= " item.cd_item,";
                $sql .= " item.vl_item_peso,";
                $sql .= " itemform.nr_posicao";
                $sql .= " FROM tbdw_item_formulario AS itemform";
                $sql .= " LEFT JOIN tbdw_formulario AS form ON form.id_formulario = itemform.id_formulario";
                $sql .= " LEFT JOIN tbdw_sit_formulario AS sit ON sit.cd_sit_formulario = form.cd_sit_formulario";
                $sql .= " LEFT JOIN tbdw_item AS item ON item.cd_item = itemform.cd_item AND item.cd_agrupamento = itemform.cd_agrupamento AND item.cd_tema = itemform.cd_tema AND item.id_versao = itemform.id_versao";
                $sql .= " LEFT JOIN tbdw_tema AS tema ON tema.cd_tema = item.cd_tema";
                $sql .= " LEFT JOIN tbdw_agrupamento AS ag ON ag.cd_agrupamento = tema.cd_agrupamento";
		if ( $_POST['filtroAgrupamento']) {
			$sql .= " WHERE itemform.cd_agrupamento = ". $_POST['filtroAgrupamento'];	
		}
 		//$sql .= " ORDER BY form.id_formulario, ag.ds_agrupamento, tema.ds_tema, itemform.nr_posicao";
 		$sql .= " ORDER BY form.id_formulario, ag.ds_agrupamento, itemform.id_item_formulario";
                //echo("<hr>".$sql."<hr>");
		$resultForms = db_query($sql) or db_die();
		$total_itens = db_num_rows($resultForms);
		while ( $rowItem = db_fetch_array($resultForms) ) {
			$tr_hover = " onmouseover=\"this.style.cursor='pointer'; this.style.backgroundColor='$bgcolor4'\"";
			$tr_mouseout = " onmouseout=\"this.style.backgroundColor='". $bgcolor6 ."'\""; 
			//$onclick = " onclick=\"document.location.href='index.php?module=". $module ."&mode=forms&id=". $rowAvaliacao['id_avaliacao']  ."'\" ";
			?>
			<tr <?= $tr_hover . $tr_mouseout . $onclick ?> bgcolor="<?= $bgcolor6 ?>">
				<td><?= $rowItem["ds_formulario"] ?></td>
				<td><?= $rowItem["ds_sit_formulario"] ?></td>
				<td><?= $rowItem["ds_agrupamento"] ?></td>
				<td><?= $rowItem["ds_tema"] ?></td>
				<td><?= $rowItem["ds_item"] ?></td>
				<td><?= $rowItem["cd_item"] ?></td>
                                <td>
                                    <?php
                                        if(!is_null($rowItem["vl_item_peso"])) {
                                            echo(number_format($rowItem["vl_item_peso"] , 2, ",", "."));
                                            //echo($rowItem["vl_item_peso"]);
                                        }
                                    ?>
                                </td>
			</tr><?
		}
		?>
			
		</table>
		
	</td>
</tr>
</table>

<br><br>

	<div style="text-align: center; font-weight: bold;">Total de itens encontrados: <span style="color:#FF0000"><?= $total_itens ?></span> </div>

<br><br>

