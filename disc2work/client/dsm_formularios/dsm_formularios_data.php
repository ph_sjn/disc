<?
exit;
if ($incluir) {

	if ($id_conjunto == "") {
		$msg_erro = "Selecione o Conjunto do Ponto.";
		echo retornaMensagem("<font color='red'>". $msg_erro ."</font>");
	}

	if (!$msg_erro) {
	
		if ( trim($_POST['geo_latitude']) == "" or !is_numeric( $_POST['geo_latitude'] ) ) { $_POST['geo_latitude'] = 0; }
		if ( trim($_POST['geo_longitude']) == "" or !is_numeric( $_POST['geo_longitude'] ) ) { $_POST['geo_longitude'] = 0; }
		if ( trim($_POST['tel_ddi_1']) == "" or !is_numeric( $_POST['tel_ddi_1'] ) ) { $_POST['tel_ddi_1'] = 0; }
		if ( trim($_POST['tel_ddd_1']) == "" or !is_numeric( $_POST['tel_ddd_1'] ) ) { $_POST['tel_ddd_1'] = 0; }
		if ( trim($_POST['tel_numero_1']) == "" or !is_numeric( $_POST['tel_numero_1'] ) ) { $_POST['tel_numero_1'] = 0; }
		if ( trim($_POST['tel_compl_1']) == "" or !is_numeric( $_POST['tel_compl_1'] ) ) { $_POST['tel_compl_1'] = 0; }
		if ( trim($_POST['tel_ddi_2']) == "" or !is_numeric( $_POST['tel_ddi_2'] ) ) { $_POST['tel_ddi_2'] = 0; }
		if ( trim($_POST['tel_ddd_2']) == "" or !is_numeric( $_POST['tel_ddd_2'] ) ) { $_POST['tel_ddd_2'] = 0; }
		if ( trim($_POST['tel_numero_2']) == "" or !is_numeric( $_POST['tel_numero_2'] ) ) { $_POST['tel_numero_2'] = 0; }
		if ( trim($_POST['vl_valor_1']) == "" or !is_numeric( $_POST['vl_valor_1'] ) ) { $_POST['vl_valor_1'] = 0; }
		if ( trim($_POST['vl_valor_2']) == "" or !is_numeric( $_POST['vl_valor_2'] ) ) { $_POST['vl_valor_2'] = 0; }
		if ( trim($_POST['vl_valor_3']) == "" or !is_numeric( $_POST['vl_valor_3'] ) ) { $_POST['vl_valor_3'] = 0; }

		$sql = "INSERT INTO tb_geo_ponto ";
		$sql .= " (id_conjunto, cd_ponto, nm_ponto, end_tipo_logradouro, end_nome_logradouro, end_numero, end_complemento, end_bairro, end_cep, end_cidade, end_uf, geo_latitude, geo_longitude, tel_ddi_1, tel_ddd_1, tel_numero_1, tel_tipo_1, tel_compl_1, tel_ddi_2, tel_ddd_2, tel_numero_2, tel_tipo_2, tel_compl_2, vl_valor_1, vl_valor_2, vl_valor_3)  ";
		$sql .= " VALUES (". $_POST['id_conjunto'] .", \"". $_POST['cd_ponto'] ."\", \"". $_POST['nm_ponto'] ."\", \"". $_POST['end_tipo_logradouro'] ."\", ";
		$sql .= " \"". $_POST['end_nome_logradouro'] ."\", \"". $_POST['end_numero'] ."\", \"". $_POST['end_complemento'] ."\", ";
		$sql .= " \"". $_POST['end_bairro'] ."\", \"". $_POST['end_cep'] ."\", \"". $_POST['end_cidade'] ."\", \"". $_POST['end_uf'] ."\", ";
		$sql .= 		 $_POST['geo_latitude'] .", ". $_POST['geo_longitude'] .",". $_POST['tel_ddi_1'] .",". $_POST['tel_ddd_1'] .",". $_POST['tel_numero_1'] .", ";
		$sql .= " \"". $_POST['tel_tipo_1'] ."\" , ". $_POST['tel_compl_1'] .", ". $_POST['tel_ddi_2'] .", ". $_POST['tel_ddd_2'] .", ". $_POST['tel_numero_2'] .", ";
		$sql .= " \"". $_POST['tel_tipo_2'] ."\" , \"". $_POST['tel_compl_2'] ."\", ". $_POST['vl_valor_1'] .", ". $_POST['vl_valor_2'] .", ". $_POST['vl_valor_3'] ." )";
		//echo $sql;
		$result = db_query($sql) or db_die();
		if ($result) { 
			echo retornaMensagem("Ponto inclu�do com sucesso.");
		} else {
			echo retornaMensagem("Ponto inclu�do com sucesso.");
		}
		
		include_once("geo_pontos/geo_pontos_view.php");
	}
	else {
		include_once("geo_pontos/geo_pontos_forms.php");
	}

}

if ($salvar) {

	if ($id_conjunto == "") {
		$msg_erro = "Selecione o Conjunto do Ponto.";
		echo retornaMensagem("<font color='red'>". $msg_erro ."</font>");
	}

	if (!$msg_erro) {
	
		if ( trim($_POST['geo_latitude']) == "" or !is_numeric( $_POST['geo_latitude'] ) ) { $_POST['geo_latitude'] = 0; }
		if ( trim($_POST['geo_longitude']) == "" or !is_numeric( $_POST['geo_longitude'] ) ) { $_POST['geo_longitude'] = 0; }
		if ( trim($_POST['tel_ddi_1']) == "" or !is_numeric( $_POST['tel_ddi_1'] ) ) { $_POST['tel_ddi_1'] = 0; }
		if ( trim($_POST['tel_ddd_1']) == "" or !is_numeric( $_POST['tel_ddd_1'] ) ) { $_POST['tel_ddd_1'] = 0; }
		if ( trim($_POST['tel_numero_1']) == "" or !is_numeric( $_POST['tel_numero_1'] ) ) { $_POST['tel_numero_1'] = 0; }
		if ( trim($_POST['tel_compl_1']) == "" or !is_numeric( $_POST['tel_compl_1'] ) ) { $_POST['tel_compl_1'] = 0; }
		if ( trim($_POST['tel_ddi_2']) == "" or !is_numeric( $_POST['tel_ddi_2'] ) ) { $_POST['tel_ddi_2'] = 0; }
		if ( trim($_POST['tel_ddd_2']) == "" or !is_numeric( $_POST['tel_ddd_2'] ) ) { $_POST['tel_ddd_2'] = 0; }
		if ( trim($_POST['tel_numero_2']) == "" or !is_numeric( $_POST['tel_numero_2'] ) ) { $_POST['tel_numero_2'] = 0; }
		if ( trim($_POST['vl_valor_1']) == "" or !is_numeric( $_POST['vl_valor_1'] ) ) { $_POST['vl_valor_1'] = 0; }
		if ( trim($_POST['vl_valor_2']) == "" or !is_numeric( $_POST['vl_valor_2'] ) ) { $_POST['vl_valor_2'] = 0; }
		if ( trim($_POST['vl_valor_3']) == "" or !is_numeric( $_POST['vl_valor_3'] ) ) { $_POST['vl_valor_3'] = 0; }

		$sql = "UPDATE tb_geo_ponto ";
		$sql .= " SET id_conjunto = ". $_POST['id_conjunto'] .", cd_ponto = \"". $_POST['cd_ponto'] ."\", nm_ponto  = \"". $_POST['nm_ponto'] ."\", ";
		$sql .= " end_tipo_logradouro  = \"". $_POST['end_tipo_logradouro'] ."\", ";
		$sql .= " end_nome_logradouro  = \"". $_POST['end_nome_logradouro'] ."\", end_numero  = \"". $_POST['end_numero'] ."\", ";
		$sql .= " end_complemento  = \"". $_POST['end_complemento'] ."\",  ";
		$sql .= " end_bairro  = \"". $_POST['end_bairro'] ."\", end_cep  = \"". $_POST['end_cep'] ."\", end_cidade  = \"". $_POST['end_cidade'] ."\", ";
		$sql .= " end_uf  = \"". $_POST['end_uf'] ."\", geo_latitude = ". $_POST['geo_latitude'] .",  ";
		$sql .= " geo_longitude = ". $_POST['geo_longitude'] .", tel_ddi_1 = ". $_POST['tel_ddi_1'] .", tel_ddd_1 = ". $_POST['tel_ddd_1'] .", ";
		$sql .= " tel_numero_1 = ". $_POST['tel_numero_1'] .",  ";
		$sql .= " tel_tipo_1  = \"". $_POST['tel_tipo_1'] ."\", tel_compl_1  = \"". $_POST['tel_compl_1'] ."\", tel_ddi_2 = ". $_POST['tel_ddi_2'] .", ";
		$sql .= " tel_ddd_2 = ". $_POST['tel_ddd_2'] .",  ";
		$sql .= " tel_numero_2 = ". $_POST['tel_numero_2'] .", tel_tipo_2  = \"". $_POST['tel_tipo_2'] ."\", tel_compl_2  = \"". $_POST['tel_compl_2'] ."\", ";
		$sql .= " vl_valor_1 = ". $_POST['vl_valor_1'] .",  ";
		$sql .= " vl_valor_2 = ". $_POST['vl_valor_2'] .", vl_valor_3 = ". $_POST['vl_valor_3'] ." WHERE id_ponto =". $id;
		//echo $sql;
		$result = db_query($sql) or db_die();
		if ( $result ) {
			echo retornaMensagem("Dados do ponto atualizados com sucesso.");
		} else { 
			echo retornaMensagem("Ocorreu um erro ao atualizar os dados do ponto. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
		}
		
		
	}

	include_once("geo_pontos/geo_pontos_forms.php");
}

if ($excluir) {
	$sql = "DELETE FROM tb_geo_ponto WHERE id_ponto = ". $_POST['id'];
	db_query($sql) or db_die();

	if ( $result ) {
		echo retornaMensagem("Ponto exclu�do com sucesso.");
	} else {
		echo retornaMensagem("Ocorreu um erro ao excluir o ponto. Por favor tente novamente. Caso o problema persista, entre em contato com o administrador.");
	}

	include_once("geo_pontos/geo_pontos_view.php");
}

?>