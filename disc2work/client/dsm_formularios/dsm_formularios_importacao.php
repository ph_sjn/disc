<?php
exit
?>

<br>

<form method=post enctype='multipart/form-data' action='index.php' name='frm'>
<input type=hidden name='module' value='<?= $module ?>'>
<input type=hidden name='mode' value='<?= $mode ?>'>

<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0>
<tr bgcolor="<?= $bgcolor4 ?>">
	<td style="text-align:center; font-weight: bold;">IMPORTA��O DE PONTOS</td>
</tr>
<tr bgcolor="<?= $bgcolor6 ?>">
	<td>

		<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=0 cellpadding=3 border=0 width="100%">
		
		<tr bgcolor="<?= $bgcolor6 ?>">
			<td style="font-weight: bold; vertical-align: middle; text-align: right; width: 20%;">Conjunto:</td>
			<td style="font-weight: bold;">
				<select name="id_conjunto" class="combo">
				<?
				// Query do SQL
				$sql = "SELECT id_conjunto, cd_conjunto, nm_conjunto, ds_conjunto FROM tb_geo_conjunto ";
				$sql .= " ORDER BY nm_conjunto ASC";
				$result = db_query($sql) or db_die();
				while ( $rowConjuntos = db_fetch_array($result) ) {?>
					<option value="<?= $rowConjuntos['id_conjunto'] ?>"><?= $rowConjuntos['nm_conjunto'] ?></option>
				<? } ?>
				</select>
			</td>
		</tr>
		
		<tr bgcolor="<?= $bgcolor6 ?>">
			<td style="text-align:right; vertical-align:middle"><b>Arquivo:</b></td>
			<td>
				<input type="file" class="frmFiles" name="arquivo" id="arquivo">
			</td>
		</tr>
		<tr bgcolor="<?= $bgcolor6 ?>">
			<td align='right' style="vertical-align:middle"></td>
			<td>
				
				<br>
				
				<span style="color:red; font-weight: bold;">Siga atentamente as instru��es abaixo:</span>
				
				<br><br>
				
				As colunas na planilha devem ser exatamente iguais ao apresentado no modelo, nesta mesma ordem: <br>
				COLUNA A - cd_ponto<br>
				COLUNA B - nm_ponto<br>
				COLUNA C - end_tipo_logradouro<br>
				COLUNA D - end_nome_logradouro<br>
				COLUNA E - end_numero<br>
				COLUNA F - end_complemento<br>
				COLUNA G - end_bairro<br>
				COLUNA H - end_cep<br>
				COLUNA I - end_cidade<br>
				COLUNA J - end_uf<br>
				COLUNA K - geo_lat<br>
				COLUNA L - geo_long<br>
				COLUNA M - ddi1<br>
				COLUNA N - ddd1<br>
				COLUNA O - tel1<br>
				COLUNA P - tipo1<br>
				COLUNA Q - compl1<br>
				COLUNA R - ddi2<br>
				COLUNA S - ddd2<br>
				COLUNA T - tel2<br>
				COLUNA U - tipo2<br>
				COLUNA V - compl2<br>
				COLUNA W - valor1<br>
				COLUNA X - valor2<br>
				COLUNA Y - valor3
				<br><br>
				Os t�tulo s�o obrigat�rios na primeira linha.
				<br><br>
				A primeira linha s�o somente dos t�tulos. A partir da segunda linha s�o considerados os dados v�lidos para importa��o.
				<br><br>
				Modelo de arquivo para importa��o: 
				<a href="geo_pontos/pontos.xlsx"><span style="color:red; font-weight: bold;">pontos.xlsx</span></a>
				
				<br><br>
			</td>
		</tr>
		</table>
		

	</td>
</tr>
<tr bgcolor="<?= $bgcolor6 ?>">
	<td>
		  
		<table border='0' width='100%'>
		<tr>
			<td>
				<input type=button class="botao" value='Voltar' onclick="javascript:document.location.href='index.php?module=<?= $_REQUEST['module'] ?>'">
			</td>
			<td style="text-align:right">
				<input type=submit class="botao" name='enviar_planilha' value='Enviar'>
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>

</form>

<br><br>



<? if ($_POST['enviar_planilha']) { ?>

	<table align='center' bgcolor="<?= $bgcolor5 ?>" cellspacing=1 cellpadding=6 border=0>
	<tr bgcolor="<?= $bgcolor6 ?>">
		<td>
			<b>ARQUIVO ENVIADO:</b> <?= $_FILES['arquivo']['name'] ?><br><br>
	
			<?
		
			if ($_FILES['arquivo']['tmp_name']) {
				include_once "/home/users/extranet/excel_xlsx/PHPExcel/IOFactory.php";
				
				$objReader07 = new PHPExcel_Reader_Excel2007();
				$objReader07->setReadDataOnly(true);
				
				$arquivo = $_FILES['arquivo']['name'];
				$arquivo_temp = $_FILES['arquivo']['tmp_name'];

				if (strrchr($arquivo,'.') == '.xlsx'){
				
					$objPHPExcel = $objReader07->load($arquivo_temp);
					$sheet = $objPHPExcel->getActiveSheet();
					
					$sql  = " INSERT INTO tb_geo_ponto ";
					$sql .= " (id_conjunto, cd_ponto, nm_ponto, end_tipo_logradouro, end_nome_logradouro, ";
					$sql .= " end_numero, end_complemento, end_bairro, end_cep, end_cidade, end_uf, geo_latitude, ";
					$sql .= " geo_longitude, tel_ddi_1, tel_ddd_1, tel_numero_1, tel_tipo_1, tel_compl_1, tel_ddi_2, ";
					$sql .= " tel_ddd_2, tel_numero_2, tel_tipo_2, tel_compl_2, vl_valor_1, vl_valor_2, vl_valor_3) ";
					$sql .= " VALUES ";
					$linhas_sql = 0;

					for($j = 2 ; $j < 1000;$j++){
						// EXIBIR AS 1000 LINHAS DA PLANILHA
						if ( trim( $sheet->getCell('A'. $j)->getValue() ) != "" ) {
							echo $sheet->getCell('A'. $j)->getValue() . " / ";
							echo $sheet->getCell('B'. $j)->getValue() . "<br>";
							
							$cd_ponto = addslashes(utf8_decode( trim($sheet->getCell('A'. $j)->getValue() ) ) );
							$nm_ponto = addslashes(utf8_decode( trim($sheet->getCell('B'. $j)->getValue() ) ));
							$end_tipo_logradouro = addslashes( utf8_decode( trim($sheet->getCell('C'. $j)->getValue() ) ) );
							$end_nome_logradouro = addslashes( utf8_decode( trim($sheet->getCell('D'. $j)->getValue() ) ) );
							$end_numero = utf8_decode( trim($sheet->getCell('E'. $j)->getValue() ) );
							$end_complemento = addslashes( utf8_decode( trim($sheet->getCell('F'. $j)->getValue() ) ) );
							$end_bairro = addslashes( utf8_decode( trim($sheet->getCell('G'. $j)->getValue() ) ) );
							$end_cep = utf8_decode( trim($sheet->getCell('H'. $j)->getValue() ) );
							$end_cidade = addslashes(utf8_decode( trim($sheet->getCell('I'. $j)->getValue() ) ) );
							$end_uf = utf8_decode( trim($sheet->getCell('J'. $j)->getValue() ) );
							
							$geo_lat = utf8_decode( trim($sheet->getCell('K'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('K'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('K'. $j)->getValue() ) ) ) ) $geo_lat = "null";
							
							$geo_long = utf8_decode( trim($sheet->getCell('L'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('L'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('L'. $j)->getValue() ) ) ) ) $geo_long = "null";
							
							$ddi1 = utf8_decode( trim($sheet->getCell('M'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('M'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('M'. $j)->getValue() ) ) ) ) $ddi1 = "null";
							
							$ddd1 = utf8_decode( trim($sheet->getCell('N'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('N'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('N'. $j)->getValue() ) ) ) ) $ddd1 = "null";

							$tel1 = utf8_decode( trim($sheet->getCell('O'. $j)->getValue() ) );

							$tipo1 = utf8_decode( trim($sheet->getCell('P'. $j)->getValue() ) );
							$compl1 = addslashes( utf8_decode( trim($sheet->getCell('Q'. $j)->getValue() ) ) );
							
							$ddi2 = utf8_decode( trim($sheet->getCell('R'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('R'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('R'. $j)->getValue() ) ) ) ) $ddi2 = "null";
							
							$ddd2 = utf8_decode( trim($sheet->getCell('S'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('S'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('S'. $j)->getValue() ) ) ) ) $ddd2 = "null";

							$tel2 = utf8_decode( trim($sheet->getCell('T'. $j)->getValue() ) );

							$tipo2 = utf8_decode( trim($sheet->getCell('U'. $j)->getValue() ) );
							$compl2 = addslashes( utf8_decode( trim($sheet->getCell('V'. $j)->getValue() ) ) );
						
							$valor1 = utf8_decode( trim($sheet->getCell('W'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('W'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('W'. $j)->getValue() ) ) ) ) $valor1 = "null";

							$valor2 = utf8_decode( trim($sheet->getCell('X'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('X'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('X'. $j)->getValue() ) ) ) ) $valor2 = "null";

							$valor3 = utf8_decode( trim($sheet->getCell('Y'. $j)->getValue() ) );
							if ( utf8_decode( trim($sheet->getCell('Y'. $j)->getValue() ) ) == "" || !is_numeric( utf8_decode( trim($sheet->getCell('Y'. $j)->getValue() ) ) ) ) $valor3 = "null";
							
							if ( $linhas_sql > 0 ) {
								$sql .= ",";
							}
							$sql .= "(". $_POST['id_conjunto'] .", '". $cd_ponto ."', '". $nm_ponto ."', '". $end_tipo_logradouro ."', '". $end_nome_logradouro ."', ";
							$sql .= " '". $end_numero ."', '". $end_complemento ."', '". $end_bairro ."', '". $end_cep ."', '". $end_cidade ."', '". $end_uf ."', ". $geo_lat .", ";
							$sql .= $geo_long .", ". $ddi1 .", ". $ddd1 .", '". $tel1 ."', '". $tipo1 ."', '". $compl1 ."', ";
							$sql .= $ddi2 .", ". $ddd2 .", '". $tel2 ."', '". $tipo2 ."', '". $compl2 ."',";
							$sql .= $valor1 .", ". $valor2 .", ". $valor3 .")";
							
							$linhas_sql++;
							
						}
					}
					if ( $linhas_sql > 0 ) {
						//echo $sql . "<br>";
						if ( db_query($sql) ) {
							echo retornaMensagem("Pontos importados com sucesso.");
							
							$sqlErro = "INSERT INTO tb_geo_ponto_log_importacao ";
							$sqlErro .= " (hora_importacao, mensagem, id_usuario) VALUES (now(), 'Pontos importados com sucesso.\n\nSQL: \n\n". addslashes($sql) ."',". $_SESSION['user_ID'] .") ";
							//echo $sqlErro;
							db_query($sqlErro);
							
						} else {
							echo retornaMensagem("Ocorreu um erro ao importar os pontos. <br><br>Entre em contato com o administrador enviando a mensagem de erro abaixo.<br><br><span style=\"font-style: italic\">". db_error($link) ."</span>.",1);
							
							$sqlErro = "INSERT INTO tb_geo_ponto_log_importacao ";
							$sqlErro .= " (hora_importacao, mensagem, id_usuario) VALUES (now(), 'Erro ao carregar o SQL:\n\n". addslashes(db_error($link)) ."\n\nSQL: \n\n". addslashes($sql) ."',". $_SESSION['user_ID'] .") ";
							//echo $sqlErro;
							db_query($sqlErro);
							
						}
					}

				}

			}
			?>
		</td>
	</tr>
	</table><?
}
?>

<br><br>
