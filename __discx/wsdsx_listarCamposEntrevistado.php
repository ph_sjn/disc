<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_listarCamposEntrevistado.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_listarCamposEntrevistado.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_listarCamposEntrevistado";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXbusca.php";

$PARM_tipoRetorno  = $_REQUEST["tipo"];
$PARM_selPesquisas = $_REQUEST["pesq"];

// Instancia a classe principal
$objDSXbusca = new clsDSXbusca();

$colCamposEntrevistado = $objDSXbusca->listarCamposEntrevistado($PARM_selPesquisas);

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($colCamposEntrevistado AS $objItem) {
    if($objItem) {
        $_JSON_retorno[$_JSON_linha]['Id'] = $objItem->Id;
        $_JSON_retorno[$_JSON_linha]['Campo'] = $objItem->Campo;
        $_JSON_retorno[$_JSON_linha]['Tipo'] = $objItem->Tipo;
		$_JSON_retorno[$_JSON_linha]['flBasico'] = $objItem->flBasico;
		$_JSON_retorno[$_JSON_linha]['Tamanho'] = $objItem->Tamanho;
		$_JSON_retorno[$_JSON_linha]['Validacao'] = $objItem->Validacao;
		$_JSON_retorno[$_JSON_linha]['Descricao'] = $objItem->Descricao;
		$_JSON_retorno[$_JSON_linha]['Condicao'] = $objItem->Condicao;
		$_JSON_retorno[$_JSON_linha]['Valor'] = $objItem->Valor;
        //
        $_JSON_linha++;
    }
}           

// Permite cross-domain Ajax
header('Access-Control-Allow-Origin: *');
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
