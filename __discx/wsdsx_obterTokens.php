<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterTokens.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterTokens.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterTokens";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id = $_REQUEST["id"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$RET_array = $objDWC->obterTokens($PARM_id);

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($RET_array AS $objItem) {
    if($objItem) {
        //
        $_JSON_linha = $objItem->Id;
        //$_JSON_linha = $objItem->CodigoPublicoAlvo . strtolower($objItem->Email);
        //$_JSON_linha = $objItem->Id . "_" . strtolower($objItem->Email);
        //
        $_JSON_retorno[$_JSON_linha]['Id'] = $objItem->Id;
        $_JSON_retorno[$_JSON_linha]['DataCriacao'] = $objItem->DataCriacao;
        $_JSON_retorno[$_JSON_linha]['Codigo'] = $objItem->Codigo;
        $_JSON_retorno[$_JSON_linha]['Nome'] = $objItem->Nome;
        $_JSON_retorno[$_JSON_linha]['Email'] = $objItem->Email;
        $_JSON_retorno[$_JSON_linha]['Observacoes'] = $objItem->Observacoes;
        $_JSON_retorno[$_JSON_linha]['Unidade'] = $objItem->Unidade;
        $_JSON_retorno[$_JSON_linha]['CodigoPublicoAlvo'] = $objItem->CodigoPublicoAlvo;
        $_JSON_retorno[$_JSON_linha]['PublicoAlvo'] = $objItem->PublicoAlvo;
        $_JSON_retorno[$_JSON_linha]['IdAvaliacao'] = $objItem->IdAvaliacao;
        $_JSON_retorno[$_JSON_linha]['IdCliente'] = $objItem->IdCliente;
        $_JSON_retorno[$_JSON_linha]['Hash'] = $objItem->Hash;
        $_JSON_retorno[$_JSON_linha]['QtdeTotalPerguntas'] = $objItem->QtdeTotalPerguntas;
        $_JSON_retorno[$_JSON_linha]['QtdePerguntasFaltam'] = $objItem->QtdePerguntasFaltam;
        $_JSON_retorno[$_JSON_linha]['QtdePerguntasRespondidas'] = $objItem->QtdePerguntasRespondidas;
        //$_JSON_linha++;
    }
}           
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
//echo("<br>SQL=".$objDWC->ERRO_sql);
