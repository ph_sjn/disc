<?php
/******
 * 
 * libDiscX.php
 * 
 ******/

//$CONFIGLIB_DISCX = "/home/users/__discx/";
$CONFIGLIB_DISCX = "D:/Projetos/Source/disc/__discx/";
//$CONFIGLIB_DISCSURVEY = "/home/users/disc2work/survey/";
$CONFIGLIB_DISCSURVEY = "D:/Projetos/Source/disc/disc2work/survey/";

include_once $CONFIGLIB_DISCX . "inc/incConfig.php";
//include_once("/home/users/disc2work/survey/lib/libdisc_survey.php");
include_once $CONFIGLIB_DISCSURVEY . "lib/libdisc_survey.php";

class clsDWCPerfisComparado{
	// Correlacao entre X e Y
	public $correlacao = 0;
	//
	// Serie DISC X
	public $colPerfilX = null;
	public $serieX     = "";
	public $resultadoX = "";
	public $resulXHtml = "";
	public $nomeX 	   = "";
	//
	// Serie DISC Y
	public $colPerfilY = null;
	public $serieY     = "";
	public $resultadoY = "";
	public $resulYHtml = "";
	public $nomeY 	   = "";
	//
	// Serie X contra serie Y
	public $serieXY	   = "";
}

class clsDWCPerfilDisc{
	//--------------------------------------------------------
	// ATRIBUTOS DA CLASSE
	//--------------------------------------------------------
	// Correlacao entre X e Y ( -100% a +100%)
	//    Onde: 
	//			0%   : Perfis sem correlacao
	//			100% : Perfis totalmente correlacionados
	//	  Sinal:
	//			(-)  : Perfis contrarios. Um oposto ao outro.
	//			(+)  : Perfis cominham juntos.
	//
	public $correlacao = 0;
	//
	// Serie DISC X
	public $colPerfilX = null;
	public $serieX     = "";
	public $resultadoX = "";
	public $resulXHtml = "";
	public $nomeX 	   = "";
	//
	// Serie DISC Y
	public $colPerfilY = null;
	public $serieY     = "";
	public $resultadoY = "";
	public $resulYHtml = "";
	public $nomeY 	   = "";
	//
	// Serie X contra serie Y
	public $serieXY	   = "";
	//
	// Colecao de Perfis Comparado
	public $colPerfisComparado = null;
	//
	// Erro
	public $Erro	   = 0;
	public $ErroMsg    = "";
	//
	//--------------------------------------------------------
	// CONSTRUTOR DA CLASSE
	//--------------------------------------------------------
    function __construct() {
    	//
		$this->clear();
		//
	}
	//
	//--------------------------------------------------------
	// METODOS PUBLICOS
	//--------------------------------------------------------
	public function setPerfilX($nome, $fatorD, $fatorI, $fatorS, $fatorC){
		//
		$this->clearPerfilX();
		//
		// Validacao dos parametros
		if ( $this->chkFatorParametros($fatorD, $fatorI, $fatorS, $fatorC) ){
			//
			$this->nomeX = $nome;
			//
			$this->colPerfilX["D"] = $fatorD;
			$this->colPerfilX["I"] = $fatorI;
			$this->colPerfilX["S"] = $fatorS;
			$this->colPerfilX["C"] = $fatorC;
			//
			$this->serieX = "[" . $fatorD . "," . $fatorI . "," . $fatorS . "," . $fatorC . "]";
			//
			$resultados = array();
			foreach($this->colPerfilX as $key=>$value) {
				if ( preg_match("/(D|I|S|C)/", $key) ){
					array_push($resultados, array($key => $value));
				}
			}
			$resultados = $this->ordenarResultados($resultados);
			//
			$this->resultadoX = $this->getResultado($resultados);
			$this->resulXHtml = $this->getResultHTML($this->resultadoX);
		}
	}
	//
	public function getResultXHtml(){
		return getResultHTML($this->resultadoX);
	}
	//
	public function setPerfilY($nome, $fatorD, $fatorI, $fatorS, $fatorC){
		//
		$this->clearPerfilY();
		//
		// Validacao dos parametros
		if ( $this->chkFatorParametros($fatorD, $fatorI, $fatorS, $fatorC) ){
			//
			$this->nomeY = $nome;
			//
			$this->colPerfilY["D"] = $fatorD;
			$this->colPerfilY["I"] = $fatorI;
			$this->colPerfilY["S"] = $fatorS;
			$this->colPerfilY["C"] = $fatorC;
			//
			$this->serieY = "[" . $fatorD . "," . $fatorI . "," . $fatorS . "," . $fatorC . "]";
			//
			$resultados = array();
			foreach($this->colPerfilY as $key=>$value) {
				if ( preg_match("/(D|I|S|C)/", $key) ){
					array_push($resultados, array($key => $value));
				}
			}
			$resultados = $this->ordenarResultados($resultados);
			//
			$this->resultadoY = $this->getResultado($resultados);
			$this->resulYHtml = $this->getResultHTML($this->resultadoY);
		}
	}
	//
	public function getResultYHtml(){
		return getResultHTML($this->resultadoY);
	}
	//
	public function correlacionar(){
		// Se houver erro nao permitir a correlacao
		if ( $this->Erro != 0 ) return;
		//
		$sumX  = 0;
		$sumX2 = 0;
		$sumY  = 0;
		$sumY2 = 0;
		$sumXY = 0;
		//
		// Somatoria do Perfil X e X^2
		foreach($this->colPerfilX as $key=>$value) {
			if ( preg_match("/(D|I|S|C)/", $key) ){
				$sumX  += $value;
				$sumX2 += pow($value, 2);
			}
		}
		//
		// Somatoria do Perfil Y e Y^2
		foreach($this->colPerfilY as $key=>$value) {
			if ( preg_match("/(D|I|S|C)/", $key) ){
				$sumY  += $value;
				$sumY2 += pow($value, 2);
			}
		}
		//
		// Somatoria X * Y
		$sumXY  = $this->colPerfilX["D"] * $this->colPerfilY["D"];
		$sumXY += $this->colPerfilX["I"] * $this->colPerfilY["I"];
		$sumXY += $this->colPerfilX["S"] * $this->colPerfilY["S"];
		$sumXY += $this->colPerfilX["C"] * $this->colPerfilY["C"];
		//
		// Correlacao
		$this->correlacao = ((4*$sumXY) - ($sumX * $sumY))/(sqrt((4*$sumX2)-pow($sumX, 2))*sqrt((4*$sumY2)-pow($sumY, 2)));
		//
		// Serie X contra serie Y
		$this->serieXY  = "[" . intval($this->colPerfilX["D"] - $this->colPerfilY["D"]);
		$this->serieXY .= "," . intval($this->colPerfilX["I"] - $this->colPerfilY["I"]);
		$this->serieXY .= "," . intval($this->colPerfilX["S"] - $this->colPerfilY["S"]);
		$this->serieXY .= "," . intval($this->colPerfilX["C"] - $this->colPerfilY["C"]) . "]";
		//
		// Objeto que representa os Perfis Comparado
		$PerfilComparado = new clsDWCPerfisComparado();
		//
		// Mapeamento do objeto
		$PerfilComparado->correlacao = $this->correlacao;
		$PerfilComparado->colPerfilX = $this->colPerfilX;
		$PerfilComparado->serieX     = $this->serieX;
		$PerfilComparado->resultadoX = $this->resultadoX;
		$PerfilComparado->resulXHtml = $this->resulXHtml;
		$PerfilComparado->nomeX 	 = $this->nomeX;
		$PerfilComparado->colPerfilY = $this->colPerfilY;
		$PerfilComparado->serieY     = $this->serieY;
		$PerfilComparado->resultadoY = $this->resultadoY;
		$PerfilComparado->resulYHtml = $this->resulYHtml;
		$PerfilComparado->nomeY 	 = $this->nomeY;
		$PerfilComparado->serieXY	 = $this->serieXY;
		//
		// Adicionar a colecao
		$this->colPerfisComparado[] = $PerfilComparado;
		//
		if ( count($this->colPerfisComparado) > 1 ){
			usort ( $this->colPerfisComparado , function ($a, $b) {
				if($a->correlacao == $b->correlacao) {
					return 0;
				}
				return ($a->correlacao > $b->correlacao) ? -1 : 1;
			});
		}
	}
	//
	public function clear(){
    	//
		$this->Erro    = 0;
		$this->ErroMsg = "";
		//
		$this->clearPerfilX();
		$this->clearPerfilY();
		//
		$this->colPerfisComparado = array();
		//
	}
	//
	//--------------------------------------------------------
	// METODOS INTERNOS
	//--------------------------------------------------------
	private function chkFatorParametros($fatorD, $fatorI, $fatorS, $fatorC){
		$parametrosValidos = true;
		//
		if ( preg_match ("/\,/", $fatorD) ){
			$fatorD = str_replace(",",".",$fatorD);
		}
		if ( preg_match ("/\,/", $fatorI) ){
			$fatorI = str_replace(",",".",$fatorI);
		}
		if ( preg_match ("/\,/", $fatorS) ){
			$fatorS = str_replace(",",".",$fatorS);
		}
		if ( preg_match ("/\,/", $fatorC) ){
			$fatorC = str_replace(",",".",$fatorC);
		}
		//
		if ( !preg_match ("/(\d+|\d+\.|\d+\.\d+)/", $fatorD) || 
			 !preg_match ("/(\d+|\d+\.|\d+\.\d+)/", $fatorI) || 
			 !preg_match ("/(\d+|\d+\.|\d+\.\d+)/", $fatorS) || 
		     !preg_match ("/(\d+|\d+\.|\d+\.\d+)/", $fatorC) ){
				//
				$parametrosValidos = false;
				//
				$this->Erro = 1;
				$this->ErroMsg = "Par�metros inv�lidos! Os par�metros devem ser do tipo n�mericos sem sinal.";
		} else {
			$fatorD = intval($fatorD);
			$fatorI = intval($fatorI);
			$fatorS = intval($fatorS);
			$fatorC = intval($fatorC);
			if ( ( $fatorD < 0 && $fatorD > 100 ) || 
			     ( $fatorI < 0 && $fatorI > 100 ) ||
				 ( $fatorS < 0 && $fatorS > 100 ) ||
				 ( $fatorC < 0 && $fatorC > 100 ) ) {
				//
				$parametrosValidos = false;
				//
				$this->Erro = 1;
				$this->ErroMsg = "Par�metros fora do intervalo! Os par�metros devem estar no intervalo de 0 a 100.";
			}
		}
		//
		return $parametrosValidos;
	}
	//
	private function ordenarResultados($resultados){
		usort ( $resultados , function ($a, $b) {
				$keyA = key($a);
				$keyB = key($b);
				if($a[$keyA] == $b[$keyB]) {
					if ($keyA == "C" && preg_match("/(D|I|S)/", $keyB)){
						return 1;
					} elseif ($keyA == "S" && preg_match("/(D|I)/", $keyB)){
						return 1;
					} elseif ($keyA == "I" && preg_match("/D/", $keyB)){
						return 1;
					} else {
						return 0;
					}
				}
				return ($a[$keyA] > $b[$keyB]) ? -1 : 1;
			}
		);
		return $resultados;
	}
	//
	private function getResultado($resultados){
		$letterDisc = "";
		foreach($resultados as $disc) {
			foreach($disc as $key=>$value) {
				if ( $value > 50 ) {
					$letterDisc .= $key;
				}
			}
		}
		//
		if ( strlen($letterDisc) == 3 ) {
			switch(substr($letterDisc,1,2)) {
				case "SI":
					$letterDisc = substr($letterDisc,0,1) . "IS";
					break;
				case "CS":
					$letterDisc = substr($letterDisc,0,1) . "SC";
					break;
				case "CI":
					$letterDisc = substr($letterDisc,0,1) . "IC";
					break;
				case "SD":
					$letterDisc = substr($letterDisc,0,1) . "DS";
					break;
				case "CD":
					$letterDisc = substr($letterDisc,0,1) . "DC";
					break;
				case "ID":
					$letterDisc = substr($letterDisc,0,1) . "DI";
					break;
			}
		}
		return $letterDisc;
	}
	//
	public function getResultHTML($sLetters){
		$result_fmt = "";
		for ($nLetter = 0; $nLetter < strlen($sLetters); $nLetter++) {
			if ( $nLetter == 0 ) {
				$result_fmt = "<span class=\"disc_letter_main\">";
				$result_fmt .= substr($sLetters, $nLetter, 1);
				$result_fmt .= "</span>";
			} else {
				$result_fmt .= "<span class=\"disc_letter_norm\">";
				$result_fmt .= substr($sLetters, $nLetter, 1);
				$result_fmt .= "</span>";
			}
		}
		return $result_fmt;
	}
	//
	private function clearPerfilX(){
		//
		$this->Correlacao = 0;
		$this->serieXY	  = "";
		//
		unset($this->colPerfilX);
		//
		$this->colPerfilX = array(
			 "D" => 0
			,"I" => 0
			,"S" => 0
			,"C" => 0
		);
		//
		$this->serieX 		= "[0,0,0,0]";
		//
		$this->resultadoX 	= "";
		$this->resulXHtml	= "";
		//
		$this->nomeX 	    = "";
		//
	}
	//
	private function clearPerfilY(){
		//
		$this->Correlacao = 0;
		$this->serieXY	  = "";
		//
		unset($this->colPerfilY);
		//
		$this->colPerfilY = array(
			 "D" => 0
			,"I" => 0
			,"S" => 0
			,"C" => 0
		);
		//
		$this->serieY 		= "[0,0,0,0]";
		//
		$this->resultadoY 	= "";
		$this->resulYHtml	= "";
		//
		$this->nomeY 	    = "";
		//
	}
}

class clsDWCitemPesquisa {
    //
    public $IdAvaliacao; // id_avaliacao
    public $IdCliente; // id_cliente
    public $Titulo; // ds_titulo
    public $CodigoSituacao; // cd_sit_avaliacao;
    public $DataInicio; // dt_inicio
    public $DataFim; // dt_fim
    //
    public $CodigoProjeto; // cd_projeto
    public $NomeProjeto; // nm_projeto
    //
    public $CodigoCliente; // cd_cliente
    public $NomeCliente; // ds_cliente
    //
    public $SituacaoAvaliacao; // ds_sit_avaliacao
}

class clsDWCpesquisa {
    //
    // id_avaliacao
    public $Id = null;
    // dt_inicio
    public $DataInicio = null;
    // dt_fim
    public $DataFim = null;
    // objeto DEFWEBsituacaoAvaliacao(cd_sit_avaliacao)
    //public $Situacao = null;
    // ds_titulo
    public $Titulo = null;
    
    // ds_franqueador_intro
    public $TextoIntroducaoFranqueador = null;
    // ds_franqueado_intro
    public $TextoIntroducaoFranqueado = null;

    // id_token_franqueador
    public $IdFranqueador = null;
    // ds_nome
    public $NomeFranqueador = null;
    // ds_email
    public $EmailFranqueador = null;
    // ds_email_assunto
    public $EmailModeloAssunto = null;
    // ds_email_txt_link
    public $EmailModeloCorpo = null;
    // ds_email_subj_conclusao
    public $EmailConclusaoAssunto = null;
    // ds_email_body_conclusao
    public $EmailConclusaoCorpo = null;
    // fl_enviar_email_conclusao
    public $EnviarEmailConclusao = 0;
    
        
    // cd_sit_avaliacao
    public $CodigoSituacao = null;
    // ds_sit_avaliacao
    public $Situacao = null;

    // id_cliente
    public $IdCliente = null;
    // ds_cliente
    public $NomeCliente = null;
    
    // id_projeto
    public $IdProjeto = null;
    // ds_projeto
    public $NomeProjeto = null;
	
	public $IMGLogoID = null;
	public $IMGLogoName = null;
}


class clsDWCtoken {
    //
    // id_token
    public $Id = null;
    // cd_hash
    public $Hash = null;
    // dt_criacao
    public $DataCriacao = null;
    // ds_codigo
    public $Codigo = null;
    // ds_nome
    public $Nome = null;
    // ds_email
    public $Email = null;
    // ds_observacao
    public $Observacoes = null;
    // ds_unidade
    public $Unidade = null;
    // cd_publico_alvo
    public $CodigoPublicoAlvo = null;
    // ds_publico_alvo
    public $PublicoAlvo = null;
    // id_avaliacao
    public $IdAvaliacao = null;
    // id_cliente
    public $IdCliente = null;
    // dv_sexo
    public $Sexo = null;
    // dt_nascimento
    public $DataNascimento = null;
    // nr_cep
    public $CEP = null;
    // nr_telefone
    public $Telefone = null;
    //
    public $QtdeTotalPerguntas = null;
    public $QtdePerguntasFaltam = null;
    public $QtdePerguntasRespondidas = null;
}

class clsDWCfranqueadorRespostaItem {
    //
    public $CodigoTema = null; // cd_tema
    public $DescricaoTema = null; // ds_tema
    public $CodigoItem = null; // cd_item
    public $DescricaoItem = null; // ds_item
    public $IdItemFormulario = null; // id_item_formulario
    public $IdFormulario = null; // id_formulario
    public $CodigoAgrupamento = null; // cd_agrupamento
    public $DescricaoAgrupamento = null; // ds_agrupamento
    public $TipoOpcoes = null; // dv_tipo_opcoes
    // array DEFWEBrespostaItemOpcao()
    public $colOpcoes = null;
    // array DEFWEBrespostaValor()
    public $colRespostas = null;
    //
    public $QuantidadeRespostas = null;
}

class clsDWCfranqueadorRespostaValor {
    //
    public $Resposta = null; // ds_resposta
    public $DescricaoOpcaoItem = null; // ds_opcao_item
    public $CodigoOpcao = null; // cd_opcao_item
}

class clsDWCfranqueadorRespostaItemOpcao {
    //
    public $Resposta = null; // ds_resposta
    public $DescricaoOpcaoItem; // ds_opcao_item
    //
    public $CodigoOpcao = null; // cd_opcao_item
    public $CodigoItem = null; // cd_item
    public $CodigoAgrupamento = null; // cd_agrupamento
    public $CodigoTema = null; // cd_tema
    public $Versao = null; // id_versao
    public $ValorItem = null; // vl_opcao_item
    public $ValorOpcao = null; // vl_opcao_valor
    public $TipoCampo = null; // dv_tipo_campo
}

class clsDWCresumoPesquisa{
	public $IdAvaliacao = null;
    public $DsTitulo = null;
	public $DtInicio = null;
	public $DtFim = null;
	public $CdSitAvaliacao = null;
	public $Ordem = null;
	public $Situacao = null;
	public $QtdeEntrevistados = null;
}

class clsDWCmaster {
    //
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_sql = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    //
    
    /***
     * 
     * obterResumoDiarioPesquisa( <id-cliente>, <id-avaliacao>, <dt_inicio> ) : array de clsDWCresumoPesquisa
     * 
     * Devolve o resumo diario da pesquisa. Qtde de entrevistados que responderam
     * 
     ***/
	public function obterResumoDiarioPesquisa($PARM_id_cliente, $PARM_id_avaliacao, $PARM_dt_inicio){
       //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_avaliacao";
                $sSQL .= ", ds_titulo";
                $sSQL .= ", dt_inicio";
				$sSQL .= ", dt_fim";
                $sSQL .= ", cd_sit_avaliacao";
                $sSQL .= ", ordem";
                $sSQL .= ", situacao";
                $sSQL .= ", COUNT(QtdeEntrevistados) AS QtdeEntrevistados";
                //
				$sSQL .= " FROM (";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 0 AS 'ordem'";
				$sSQL .= ", 'Concluiram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND af.id_avaliacao = " . $PARM_id_avaliacao;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " AND avtk.dt_termino IS NOT NULL";
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = COUNT(1)";
				//
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 1 AS 'ordem'";
				$sSQL .= ", 'Respondendo' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND af.id_avaliacao = " . $PARM_id_avaliacao;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) < COUNT(1)";
                //
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 2 AS 'ordem'";
				$sSQL .= ", 'N�o iniciaram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND af.id_avaliacao = " . $PARM_id_avaliacao;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = 0";
                //
				$sSQL .= ") AS resumo";
				$sSQL .= " GROUP BY id_avaliacao, ds_titulo, dt_fim, cd_sit_avaliacao, ordem, situacao";
				$sSQL .= " ORDER BY dt_fim, ordem ASC";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
					$index = 0;
                    while ($rsDados) {
                        //
                        $objLinha = new clsDWCresumoPesquisa();
                        //
						// Mudando o formato de yyyy/MM/dd para dd/MM/yyyy
						$dt_inicio_aux = $rsDados["dt_inicio"];
						$dt_fim_aux	   = $rsDados["dt_fim"];
						if ( preg_match ("/\d{4}\/\d{2}\/\d{2}/", $dt_inicio_aux) ){
							$dtSplitInicio = explode("/", $dt_inicio_aux);
							$dt_inicio_aux = $dtSplitInicio[2] . "/" . $dtSplitInicio[1] . "/" . $dtSplitInicio[0];
						}
						if ( preg_match ("/\d{4}\/\d{2}\/\d{2}/", $dt_fim_aux) ){
							$dtSplitFim = explode("/", $dt_fim_aux);
							$dt_fim_aux = $dtSplitFim[2] . "/" . $dtSplitFim[1] . "/" . $dtSplitFim[0];
						}                        
                        //
                        $objLinha->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objLinha->DsTitulo = $rsDados["ds_titulo"];
                        $objLinha->DtInicio = $dt_inicio_aux;
                        $objLinha->DtFim = $dt_fim_aux;
                        $objLinha->CdSitAvaliacao = $rsDados["cd_sit_avaliacao"];
                        $objLinha->Ordem = $rsDados["ordem"];
                        $objLinha->Situacao = $rsDados["situacao"];
                        $objLinha->QtdeEntrevistados = $rsDados["QtdeEntrevistados"];
                        //
                        $RET_array[$index] = $objLinha;
						$index++;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
	}
	
    /***
     * 
     * obterResumoDiarioGeral( <id-cliente>, <id_projeto>, <dt_inicio> ) : array de clsDWCresumoPesquisa
     * 
     * Devolve o resumo diario das pesquisas do projeto. Qtde de entrevistados que responderam
     * 
     ***/
	public function obterResumoDiarioGeral($PARM_id_cliente, $PARM_id_projeto, $PARM_dt_inicio){
       //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_avaliacao";
                $sSQL .= ", ds_titulo";
                $sSQL .= ", dt_inicio";
				$sSQL .= ", dt_fim";
                $sSQL .= ", cd_sit_avaliacao";
                $sSQL .= ", ordem";
                $sSQL .= ", situacao";
                $sSQL .= ", COUNT(QtdeEntrevistados) AS QtdeEntrevistados";
                //
				$sSQL .= " FROM (";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", REPLACE(av.ds_titulo, 'hidden','') AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 0 AS 'ordem'";
				$sSQL .= ", 'Concluiram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND av.id_projeto = " . $PARM_id_projeto;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " AND avtk.dt_termino IS NOT NULL";
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = COUNT(1)";
				//
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", REPLACE(av.ds_titulo, 'hidden','') AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 1 AS 'ordem'";
				$sSQL .= ", 'Respondendo' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND av.id_projeto = " . $PARM_id_projeto;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) < COUNT(1)";
                //
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", REPLACE(av.ds_titulo, 'hidden','') AS 'ds_titulo'";
                $sSQL .= ", DATE_FORMAT(avtk.dt_inicio, '%Y/%m/%d') AS 'dt_inicio'";
				$sSQL .= ", DATE_FORMAT(avtk.dt_termino, '%Y/%m/%d') AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 2 AS 'ordem'";
				$sSQL .= ", 'N�o iniciaram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE af.id_cliente = " . $PARM_id_cliente;
				$sSQL .= " AND av.id_projeto = " . $PARM_id_projeto;
				if ( $PARM_dt_inicio != "" ){
					$sSQL .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = 0";
                //
				$sSQL .= ") AS resumo";
				$sSQL .= " GROUP BY id_avaliacao, ds_titulo, dt_fim, cd_sit_avaliacao, ordem, situacao";
				$sSQL .= " ORDER BY id_avaliacao, dt_fim, ordem ASC";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
					$index = 0;
                    while ($rsDados) {
                        //
                        $objLinha = new clsDWCresumoPesquisa();
                        //
						// Mudando o formato de yyyy/MM/dd para dd/MM/yyyy
						$dt_inicio_aux = $rsDados["dt_inicio"];
						$dt_fim_aux	   = $rsDados["dt_fim"];
						if ( preg_match ("/\d{4}\/\d{2}\/\d{2}/", $dt_inicio_aux) ){
							$dtSplitInicio = explode("/", $dt_inicio_aux);
							$dt_inicio_aux = $dtSplitInicio[2] . "/" . $dtSplitInicio[1] . "/" . $dtSplitInicio[0];
						}
						if ( preg_match ("/\d{4}\/\d{2}\/\d{2}/", $dt_fim_aux) ){
							$dtSplitFim = explode("/", $dt_fim_aux);
							$dt_fim_aux = $dtSplitFim[2] . "/" . $dtSplitFim[1] . "/" . $dtSplitFim[0];
						}                        
                        //
                        $objLinha->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objLinha->DsTitulo = $rsDados["ds_titulo"];
                        $objLinha->DtInicio = $dt_inicio_aux;
                        $objLinha->DtFim = $dt_fim_aux;
                        $objLinha->CdSitAvaliacao = $rsDados["cd_sit_avaliacao"];
                        $objLinha->Ordem = $rsDados["ordem"];
                        $objLinha->Situacao = $rsDados["situacao"];
                        $objLinha->QtdeEntrevistados = $rsDados["QtdeEntrevistados"];
                        //
                        $RET_array[$index] = $objLinha;
						$index++;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
	}
	
    /***
     * 
     * obterResumoPesquisa( <id-cliente>, <id-avaliacao>, <dt_inicio> ) : array de clsDWCresumoPesquisa
     * 
     * Devolve o resumo da pesquisa. Qtde de entrevistados que responderam, estao respondendo e nao responderam
     * 
     ***/
	public function obterResumoPesquisa($PARM_id_cliente, $PARM_id_avaliacao, $PARM_dt_inicio){
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
		//
	if ( preg_match("/[0-9]+/", $PARM_id_cliente) || preg_match("/[0-9]+/", $PARM_id_avaliacao) ){
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Filtro
				$sFilter = "";
				if ( preg_match("/[0-9]+/", $PARM_id_cliente) ){
					$sFilter .= "af.id_cliente = " . $PARM_id_cliente;
				}
				if ( preg_match("/[0-9]+/", $PARM_id_avaliacao) ){
					if ( $sFilter != "" ){
						$sFilter .= " AND ";
					}
					$sFilter .= "af.id_avaliacao = " . $PARM_id_avaliacao;
				}
				if ( preg_match("/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/", $PARM_dt_inicio) ){
					$sFilter .= " AND avtk.dt_inicio >= '" . $PARM_dt_inicio . " 00:00:00'";
				}
				//
				// Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " id_avaliacao";
                $sSQL .= ", ds_titulo";
                $sSQL .= ", dt_inicio";
                $sSQL .= ", dt_fim";
                $sSQL .= ", cd_sit_avaliacao";
                $sSQL .= ", ordem";
                $sSQL .= ", situacao";
                $sSQL .= ", COUNT(QtdeEntrevistados) AS QtdeEntrevistados";
                //
				$sSQL .= " FROM (";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", av.dt_inicio AS 'dt_inicio'";
                $sSQL .= ", av.dt_fim AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 0 AS 'ordem'";
				$sSQL .= ", 'Concluiram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE " . $sFilter;
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = COUNT(1)";
				//
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", av.dt_inicio AS 'dt_inicio'";
                $sSQL .= ", av.dt_fim AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 1 AS 'ordem'";
				$sSQL .= ", 'Respondendo' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE " . $sFilter;
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) > 0 AND SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) < COUNT(1)";
                //
				$sSQL .= " UNION ALL ";
				//
                $sSQL .= "SELECT";
                $sSQL .= " av.id_avaliacao AS 'id_avaliacao'";
                $sSQL .= ", av.ds_titulo AS 'ds_titulo'";
                $sSQL .= ", av.dt_inicio AS 'dt_inicio'";
                $sSQL .= ", av.dt_fim AS 'dt_fim'";
                $sSQL .= ", av.cd_sit_avaliacao AS 'cd_sit_avaliacao'";
                $sSQL .= ", 2 AS 'ordem'";
				$sSQL .= ", 'N�o iniciaram' AS 'situacao'";
				$sSQL .= ", COUNT(DISTINCT 1) AS 'QtdeEntrevistados'";
				$sSQL .= " FROM tbdw_avaliacao AS av";
				$sSQL .= " LEFT JOIN tbdw_avaliacao_formulario AS af ON (av.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_item_formulario AS itemf ON (itemf.id_formulario = af.id_formulario)";
				$sSQL .= " LEFT JOIN tbdw_aval_token AS token ON (token.id_avaliacao = af.id_avaliacao)";
				$sSQL .= " LEFT JOIN tbdw_af_token_item AS resp ON (resp.id_item_formulario = itemf.id_item_formulario AND resp.id_token = token.id_token)";
				$sSQL .= " LEFT JOIN tbdw_aval_form_token AS avtk ON (token.id_avaliacao = avtk.id_avaliacao AND token.id_token = avtk.id_token)";
				$sSQL .= " WHERE " . $sFilter;
                                $sSQL .= " AND token.id_token IS NOT NULL";
				$sSQL .= " GROUP BY token.id_token";
				$sSQL .= " HAVING SUM(CASE WHEN ISNULL(resp.dt_resposta) THEN 0 ELSE 1 END) = 0";
                //
				$sSQL .= ") AS resumo";
				$sSQL .= " GROUP BY id_avaliacao, ds_titulo, dt_inicio, dt_fim, cd_sit_avaliacao, ordem, situacao";
				$sSQL .= " ORDER BY ordem ASC";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDWCresumoPesquisa();
                        //
                        $objLinha->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objLinha->DsTitulo = $rsDados["ds_titulo"];
                        $objLinha->DtInicio = $rsDados["dt_inicio"];
                        $objLinha->DtFim = $rsDados["dt_fim"];
                        $objLinha->CdSitAvaliacao = $rsDados["cd_sit_avaliacao"];
                        $objLinha->Ordem = $rsDados["ordem"];
                        $objLinha->Situacao = $rsDados["situacao"];
                        $objLinha->QtdeEntrevistados = $rsDados["QtdeEntrevistados"];
                        //
                        $RET_array[$rsDados["ordem"]] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
	}
        //
        return($RET_array);	
	}
	
	
    /***
     * 
     * obterPesquisas( <id-cliente>, <cd-cliente> ) : array de clsDWCitemPesquisa
     * 
     * Devolve a lista de pesquisas liberadas para o cliente especificado
     * pelo ID ou pelo C�DIGO.
     * 
     ***/
    public function obterPesquisas($PARM_cd_cliente, $PARM_id_cliente) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " av.id_avaliacao AS id_avaliacao";
                $sSQL .= " ,av.id_cliente AS id_cliente";
                $sSQL .= " ,pj.cd_projeto AS cd_projeto";
                $sSQL .= " ,pj.nm_projeto AS nm_projeto";
                $sSQL .= " ,cl.cd_cliente AS cd_cliente";
                $sSQL .= " ,cl.ds_cliente AS ds_cliente";
                $sSQL .= " ,av.ds_titulo AS ds_titulo";
                $sSQL .= " ,av.cd_sit_avaliacao AS cd_sit_avaliacao";
                $sSQL .= " ,sit.ds_sit_avaliacao AS ds_sit_avaliacao";
                $sSQL .= " ,av.dt_inicio AS dt_inicio";
                $sSQL .= " ,av.dt_fim AS dt_fim";
                $sSQL .= " FROM tbdw_avaliacao AS av";
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente";
                $sSQL .= " LEFT JOIN tbdw_sit_avaliacao AS sit ON sit.cd_sit_avaliacao = av.cd_sit_avaliacao";
                $sSQL .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
                $sSQL .= " WHERE ";
                //
                // Se veio o codigo do cliente...
                if($PARM_cd_cliente) {
                    // Pesquisa pelo codigo do cliente (Ex. migraws)
                    $sSQL .= " cl.cd_cliente = '" . mysqli_escape_string($objLink, $PARM_cd_cliente) . "'";
                } else {
                    // Se veio o ID do cliente...
                    if($PARM_id_cliente) {
                        // Pesquisa pelo ID do cliente (Ex. 5)
                        $sSQL .= " av.id_cliente = " . mysqli_escape_string($objLink, $PARM_id_cliente);
                    } else {
                        // Nao vieram os parametros, forca o retorno vazio
                        $sSQL .= "1 = 2";
                    }
                }
                //
                //$sSQL .= " AND av.cd_sit_avaliacao <> 2";
                $sSQL .= " ORDER BY ds_cliente, nm_projeto, ds_titulo";
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDWCitemPesquisa();
                        //
                        $objLinha->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objLinha->IdCliente = $rsDados["id_cliente"];
                        $objLinha->Titulo = $rsDados["ds_titulo"];
                        $objLinha->CodigoSituacao = $rsDados["cd_sit_avaliacao"];
                        $objLinha->DataInicio = $rsDados["dt_inicio"];
                        $objLinha->DataFim = $rsDados["dt_fim"];
                        $objLinha->CodigoProjeto = $rsDados["cd_projeto"];
                        $objLinha->NomeProjeto = $rsDados["nm_projeto"];
                        $objLinha->CodigoCliente = $rsDados["cd_cliente"];
                        $objLinha->NomeCliente = $rsDados["ds_cliente"];
                        $objLinha->SituacaoAvaliacao = $rsDados["ds_sit_avaliacao"];
                        //
                        $RET_array[$rsDados["id_avaliacao"]] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    /***
     * 
     * obterPesquisa( <id-pesquisa> ) : clsDWCpesquisa
     * 
     * Devolve informacoes sobre a pesquisa especificada pelo ID.
     * 
     ***/
    public function obterPesquisa($PARM_id_pesquisa) {
        //
        $RET_obj = new clsDWCpesquisa();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " av.id_avaliacao AS id_avaliacao";
                $sSQL .= ", av.id_cliente AS id_cliente";
                $sSQL .= ", cl.ds_cliente AS ds_cliente";
                $sSQL .= ", av.ds_titulo AS ds_titulo";
                $sSQL .= ", av.cd_sit_avaliacao AS cd_sit_avaliacao";
                $sSQL .= ", sit.ds_sit_avaliacao AS ds_sit_avaliacao";
                $sSQL .= ", av.id_projeto AS id_projeto";
                $sSQL .= ", pj.nm_projeto AS nm_projeto";
                $sSQL .= ", av.dt_inicio AS dt_inicio, av.dt_fim AS dt_fim";
                $sSQL .= ", av.id_token_franqueador AS id_token_franqueador";
                $sSQL .= ", tk.ds_nome AS ds_nome_franqueador";
                $sSQL .= ", tk.ds_email AS ds_email_franqueador";
                $sSQL .= ", av.ds_email_assunto AS ds_email_assunto";
                $sSQL .= ", av.ds_email_txt_link AS ds_email_txt_link";
                $sSQL .= ", av.ds_email_subj_conclusao AS ds_email_subj_conclusao";
                $sSQL .= ", av.ds_email_body_conclusao AS ds_email_body_conclusao";
                $sSQL .= ", av.fl_enviar_email_conclusao AS fl_enviar_email_conclusao";
                $sSQL .= ", av.ds_franqueador_intro AS ds_franqueador_intro";
                $sSQL .= ", av.ds_franqueado_intro AS ds_franqueado_intro";
                $sSQL .= ", av.dt_inicio AS dt_inicio";
                $sSQL .= ", av.dt_fim AS dt_fim";
				$sSQL .= ", av.nmarq_topo_interno AS IMGLogoID";
				$sSQL .= ", av.nmarq_topo_original AS IMGLogoName";
                $sSQL .= " FROM tbdw_avaliacao AS av";
                $sSQL .= " LEFT JOIN tbdw_cliente AS cl ON cl.id_cliente = av.id_cliente";
                $sSQL .= " LEFT JOIN tbdw_sit_avaliacao AS sit ON sit.cd_sit_avaliacao = av.cd_sit_avaliacao";
                $sSQL .= " LEFT JOIN tbdw_projeto AS pj ON pj.id_projeto = av.id_projeto";
                $sSQL .= " LEFT JOIN tbdw_aval_token AS tk ON tk.id_token = av.id_token_franqueador";
                $sSQL .= " WHERE av.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_pesquisa);
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    // Se o result set estiver vazio...
                    if(!$rsDados) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Dados n�o encontrados no banco de dados!";
                    // Senao, obteve o result set...
                    } else {
                        $RET_obj->Id = $rsDados["id_avaliacao"];
                        $RET_obj->IdCliente = $rsDados["id_cliente"];
                        $RET_obj->NomeCliente = $rsDados["ds_cliente"];
                        $RET_obj->Titulo = $rsDados["ds_titulo"];
                        $RET_obj->CodigoSituacao = $rsDados["cd_sit_avaliacao"];
                        $RET_obj->Situacao = $rsDados["ds_sit_avaliacao"];
                        $RET_obj->IdProjeto = $rsDados["id_projeto"];
                        $RET_obj->NomeProjeto = $rsDados["nm_projeto"];
                        $RET_obj->IdFranqueador = $rsDados["id_token_franqueador"];
                        $RET_obj->NomeFranqueador = $rsDados["ds_nome_franqueador"];
                        $RET_obj->EmailFranqueador = $rsDados["ds_email_franqueador"];
                        $RET_obj->EmailModeloAssunto = $rsDados["ds_email_assunto"];
                        $RET_obj->EmailModeloCorpo = $rsDados["ds_email_txt_link"];
                        $RET_obj->EmailConclusaoAssunto = $rsDados["ds_email_subj_conclusao"];
                        $RET_obj->EmailConclusaoCorpo = $rsDados["ds_email_body_conclusao"];
                        $RET_obj->EnviarEmailConclusao = $rsDados["fl_enviar_email_conclusao"];
                        $RET_obj->TextoIntroducaoFranqueador = $rsDados["ds_franqueador_intro"];
                        $RET_obj->TextoIntroducaoFranqueado = $rsDados["ds_franqueado_intro"];
                        $RET_obj->DataInicio = $rsDados["dt_inicio"];
                        $RET_obj->DataFim = $rsDados["dt_fim"];
						$RET_obj->IMGLogoID = $rsDados["IMGLogoID"];
						$RET_obj->IMGLogoName = $rsDados["IMGLogoName"];
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_obj);
    }

    /***
     * 
     * 
     ***/
    public function obterTokenPeloHash($PARM_hash) {
        //
        $objToken = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.dt_criacao AS dt_criacao,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.cd_publico_alvo AS cd_publico_alvo,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.id_avaliacao AS id_avaliacao,";
                $sSQL .= " tk.cd_hash AS cd_hash";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " WHERE tk.cd_hash = '" . mysqli_escape_string($objLink, strtolower($PARM_hash)) . "'";
                //$this->ERRO_sql = $sSQL;
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    if($rsDados) {
                        //
                        $objToken = new clsDWCtoken();
                        //
                        $objToken->Id = $rsDados["id_token"];
                        $objToken->DataCriacao = $rsDados["dt_criacao"];
                        $objToken->Codigo = $rsDados["ds_codigo"];
                        $objToken->Nome = $rsDados["ds_nome"];
                        $objToken->Email = $rsDados["ds_email"];
                        $objToken->Observacoes = $rsDados["ds_observacao"];
                        $objToken->Unidade = $rsDados["ds_unidade"];
                        $objToken->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objToken->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objToken->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objToken->IdCliente = $rsDados["id_cliente"];
                        $objToken->Hash =  $rsDados["cd_hash"];
                        //
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($objToken);
        //
    }

    /***
     * 
     * obterTokenPeloId( <id-pesquisa>, <id-token> ) : clsDWCtoken
     * 
     * Devolve o token encontrado pelos IDs fornecidos como parametro.
     * 
     */
    public function obterTokenPeloId($PARM_id_pesquisa, $PARM_id_token) {
        //
        $RET_obj = null;
        //
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.dt_criacao AS dt_criacao,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.cd_publico_alvo AS cd_publico_alvo,";
                $sSQL .= " pa.ds_publico_alvo AS ds_publico_alvo,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.cd_hash AS cd_hash,";
                $sSQL .= " tk.id_avaliacao AS id_avaliacao";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = tk.cd_publico_alvo";
                $sSQL .= " WHERE tk.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_pesquisa);
                $sSQL .= " AND tk.id_token = " . mysqli_escape_string($objLink, $PARM_id_token);
                //
                //$this->ERRO_sql = $sSQL;
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    if($rsDados) {
                        //
                        $objToken = new clsDWCtoken();
                        //
                        $objToken->Id = $rsDados["id_token"];
                        $objToken->DataCriacao = $rsDados["dt_criacao"];
                        $objToken->Codigo = $rsDados["ds_codigo"];
                        $objToken->Nome = $rsDados["ds_nome"];
                        $objToken->Email = $rsDados["ds_email"];
                        $objToken->Observacoes = $rsDados["ds_observacao"];
                        $objToken->Unidade = $rsDados["ds_unidade"];
                        $objToken->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objToken->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objToken->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objToken->IdCliente = $rsDados["id_cliente"];
                        $objToken->Hash = $rsDados["cd_hash"];
                        //
//                        $objToken->QtdeTotalPerguntas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 0, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
//                        $objToken->QtdePerguntasRespondidas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 1, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
//                        $objToken->QtdePerguntasFaltam = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 2, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        //
//                        $objQtdes = new DWXsumarioQtde();
                        $objQtdes = DWS_obterQtdeNovo($objToken->IdAvaliacao, $objToken->Id, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        //
                        $objToken->QtdeTotalPerguntas = $objQtdes->qt_total;
                        $objToken->QtdePerguntasRespondidas = $objQtdes->qt_respondidas;
                        $objToken->QtdePerguntasFaltam = $objQtdes->qt_nao_respondidas;
                        //
                        $RET_obj = $objToken;
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_obj);
    }
    
    
    /***
     * 
     * obterTokens( <id-pesquisa> ) : array de clsDWCtoken
     * 
     * Devolve a lista de tokens da pesquisa identificada pelo ID.
     * 
     ***/
    public function obterTokens($PARM_id_pesquisa) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT ";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.dt_criacao AS dt_criacao,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.cd_publico_alvo AS cd_publico_alvo,";
                $sSQL .= " pa.ds_publico_alvo AS ds_publico_alvo,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.cd_hash AS cd_hash,";
                $sSQL .= " tk.id_avaliacao AS id_avaliacao";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = tk.cd_publico_alvo";
                if($PARM_id_pesquisa > 0) {
                    $sSQL .= " WHERE tk.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_pesquisa);
                }
                $sSQL .= " ORDER BY ds_unidade, ds_nome";
                //$this->ERRO_sql = $sSQL;
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    while($rsDados) {
                        //
                        $objToken = new clsDWCtoken();
                        //
                        $objToken->Id = $rsDados["id_token"];
                        $objToken->DataCriacao = $rsDados["dt_criacao"];
                        $objToken->Codigo = $rsDados["ds_codigo"];
                        $objToken->Nome = $rsDados["ds_nome"];
                        $objToken->Email = $rsDados["ds_email"];
                        $objToken->Observacoes = $rsDados["ds_observacao"];
                        $objToken->Unidade = $rsDados["ds_unidade"];
                        $objToken->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objToken->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objToken->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objToken->IdCliente = $rsDados["id_cliente"];
                        $objToken->Hash = $rsDados["cd_hash"];
                        //
                      //  $objToken->QtdeTotalPerguntas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 0, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                      //  $objToken->QtdePerguntasRespondidas = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 1, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                      //  $objToken->QtdePerguntasFaltam = DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 2, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        //
//                        $objQtdes = new DWXsumarioQtde();
                        $objQtdes = DWS_obterQtdeNovo($objToken->IdAvaliacao, $objToken->Id, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        //
                        $objToken->QtdeTotalPerguntas = $objQtdes->qt_total;
                        $objToken->QtdePerguntasRespondidas = $objQtdes->qt_respondidas;
                        $objToken->QtdePerguntasFaltam = $objQtdes->qt_nao_respondidas;
                        //
                        //$RET_array[$objToken->CodigoPublicoAlvo . strtolower($objToken->Email)] = $objToken;
                        //$RET_array[$objToken->Id . "_" . strtolower($objToken->Email)] = $objToken;
                        $RET_array[$objToken->Id] = $objToken;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
        //
    }

    /***
     * 
     * obterTokensLight( <id-pesquisa> ) : array de clsDWCtoken
     * 
     * Devolve a lista de tokens da pesquisa identificada pelo ID.
     * 
     * Nao obtem a quantidade de perguntas respondidas ou nao.
     * 
     ***/
    public function obterTokensLight($PARM_id_pesquisa) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT ";
                $sSQL .= " tk.id_token AS id_token,";
                $sSQL .= " tk.ds_codigo AS ds_codigo,";
                $sSQL .= " tk.dt_criacao AS dt_criacao,";
                $sSQL .= " tk.ds_nome AS ds_nome,";
                $sSQL .= " tk.ds_email AS ds_email,";
                $sSQL .= " tk.ds_observacao AS ds_observacao,";
                $sSQL .= " tk.ds_unidade AS ds_unidade,";
                $sSQL .= " tk.cd_publico_alvo AS cd_publico_alvo,";
                $sSQL .= " pa.ds_publico_alvo AS ds_publico_alvo,";
                $sSQL .= " tk.id_cliente AS id_cliente,";
                $sSQL .= " tk.cd_hash AS cd_hash,";
                $sSQL .= " tk.id_avaliacao AS id_avaliacao";
                $sSQL .= " FROM tbdw_aval_token AS tk";
                $sSQL .= " LEFT JOIN tbdw_publico_alvo AS pa ON pa.cd_publico_alvo = tk.cd_publico_alvo";
                if($PARM_id_pesquisa > 0) {
                    $sSQL .= " WHERE tk.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_pesquisa);
                }
                $sSQL .= " ORDER BY ds_unidade, ds_nome";
                //$this->ERRO_sql = $sSQL;
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    while($rsDados) {
                        //
                        $objToken = new clsDWCtoken();
                        //
                        $objToken->Id = $rsDados["id_token"];
                        $objToken->DataCriacao = $rsDados["dt_criacao"];
                        $objToken->Codigo = $rsDados["ds_codigo"];
                        $objToken->Nome = $rsDados["ds_nome"];
                        $objToken->Email = $rsDados["ds_email"];
                        $objToken->Observacoes = $rsDados["ds_observacao"];
                        $objToken->Unidade = $rsDados["ds_unidade"];
                        $objToken->CodigoPublicoAlvo = $rsDados["cd_publico_alvo"];
                        $objToken->PublicoAlvo = $rsDados["ds_publico_alvo"];
                        $objToken->IdAvaliacao = $rsDados["id_avaliacao"];
                        $objToken->IdCliente = $rsDados["id_cliente"];
                        $objToken->Hash = $rsDados["cd_hash"];
                        //
                        $objToken->QtdeTotalPerguntas = 0; //DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 0, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        $objToken->QtdePerguntasRespondidas = 0; //DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 1, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        $objToken->QtdePerguntasFaltam = 0; //DWS_obterQtde($objToken->IdAvaliacao, $objToken->Id, 2, LIB_db_host, LIB_db_user, LIB_db_pass, LIB_db_name);
                        //
                        //$RET_array[$objToken->CodigoPublicoAlvo . strtolower($objToken->Email)] = $objToken;
                        //$RET_array[$objToken->Id . "_" . strtolower($objToken->Email)] = $objToken;
                        $RET_array[$objToken->Id] = $objToken;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
        //
    }
    
    /***
     * 
     * gravarModeloEmail( <id-pesquisa>, <assunto>, <texto>, <assunto_concl>, <texto_concl> ) : 0 = Ok, 1 = Falhou
     * 
     * Atualiza o assunto e o texto do corpo do e-mail que compoe o modelo
     * para ser enviado aos franqueados.
     * 
     * Devolve 0 se OK, 1 se falhou. Verificar o erro nos atributos da classe
     * master.
     * 
     ***/
    public function gravarModeloEmail($PARM_id_pesquisa, $PARM_assunto, $PARM_texto, $PARM_assuntoconcl, $PARM_textoconcl) {
        //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta o UPDATE
                $sSQL  = " UPDATE tbdw_avaliacao ";
                $sSQL .= " SET ";
                $sSQL .= " ds_email_assunto = '". mysqli_escape_string($objLink, $PARM_assunto) . "'";
                $sSQL .= ", ds_email_txt_link = '". mysqli_escape_string($objLink, $PARM_texto) . "'";
                $sSQL .= ", ds_email_subj_conclusao = '". mysqli_escape_string($objLink, $PARM_assuntoconcl) . "'";
                $sSQL .= ", ds_email_body_conclusao = '". mysqli_escape_string($objLink, $PARM_textoconcl) . "'";
                $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $PARM_id_pesquisa);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }

    // Recebe os parametros: ID do cliente, ID da avaliacao e ID do token, 
    // monta no formato:
    //      CCCCCCCCCCCCCCCC-D-AAAAAAAAAAAAAAAA-D-TTTTTTTTTTTT-D
    //  *   CCCCCCCCCCC-D-AAAAAAAAAAAAAAAA-D-TTTTTTTTTTTT-D
    //  *   CCCDAAAAAADTTTTTTTD
    // calcula e retorna o hash.
    //
    public function gerarHashToken($PARM_id_cliente, $PARM_id_avaliacao, $PARM_id_token) {
        //
        $PARM_id_cliente = $this->extrairNumeros($PARM_id_cliente);
        $PARM_id_cliente = sprintf("%016s", $PARM_id_cliente);
        //
        $PARM_id_avaliacao = $this->extrairNumeros($PARM_id_avaliacao);
        $PARM_id_avaliacao = sprintf("%016s", $PARM_id_avaliacao);
        //
        $PARM_id_token = $this->extrairNumeros($PARM_id_token);
        $PARM_id_token = sprintf("%016s", $PARM_id_token);
        //
        // Calcula o DV1
        $TRN_token_temp = $PARM_id_cliente;
        $TRN_dv = $this->calcularMOD11($TRN_token_temp);
        // Calcula o DV2
        $TRN_token_temp .= "-" . $TRN_dv . "-" . $PARM_id_avaliacao;
        $TRN_dv = $this->calcularMOD11($TRN_token_temp);
        // Calcula o DV3
        $TRN_token_temp .= "-" . $TRN_dv . "-" . $PARM_id_token;
        $TRN_dv = $this->calcularMOD11($TRN_token_temp);
        //
        $TRN_token_temp .= "-" . $TRN_dv;
        //
        $TRN_token_temp = strtolower(sha1($TRN_token_temp));
        //
        return($TRN_token_temp);
    }
    
    //
    public function verificarHashToken($PARM_hash, $PARM_id_cliente, $PARM_id_avaliacao, $PARM_id_token) {
        // Inicia o retorno da funcao
        $RET_valido = 0;
        // Se nao tiver o tamanho correto...
        if(strlen($PARM_hash) == 40) {
            //
            $TRN_token_temp = $this->gerarHashToken($PARM_id_cliente, $PARM_id_avaliacao, $PARM_id_token);
            //
            if(strtolower($PARM_hash) == $TRN_token_temp) {
                $RET_valido = 1;
            }
        }
        // Retorno da funcao
        return($RET_valido);
    }
    
    // Calcula o DV utilizando o algoritmo de modulo 11
    //
    public function calcularMOD11($PARM_numeros) {
        //
        // Extrai apenas os numeros do parametro recebido
        $PARM_numeros = $this->extrairNumeros($PARM_numeros);
        // Inicia o totalizador com zero
        $vlSoma = 0;
        // Inicia o fator em 2, para o ultimo digito do numero
        $vlFatorAtual = 2;
        //
        // Percorre cada digito do numero multiplicando-o
        // pelo fator da posicao e acumulando o total
        for($iPos=strlen($PARM_numeros)-1;$iPos>=0;$iPos--) {
            // Extrai o digito da posicao
            $vlDigito = intval(substr($PARM_numeros,$iPos,1));
            // Multiplica pelo fator da posicao e acumulado no total
            $vlSoma += ($vlDigito*$vlFatorAtual);
            // Incrementa o fator da posicao
            $vlFatorAtual++;
        }
        // Obtem o resto
        $vlResto = fmod($vlSoma, 11);
        // Calcula do DV
        $vlDV = 11-$vlResto;
        // Se for maior que 9...
        if($vlDV > 9) {
            // Atribui 1
            $vlDV = 1;
        }
        // Converte para string
        $RET_mod11 = strval($vlDV);
        // Retorno da funcao
        return($RET_mod11);
    }

    // Recebe uma string e devolve apenas os numeros
    // que ela contem. Util para CPF, CNPJ e CEP, por exemplo.
    //
    public function extrairNumeros($PARM_string) {
        // Mantem apenas os numeros na string
        $RET_string = ereg_replace("[^0-9]", "", $PARM_string);
        // Retorno da funcao
        return $RET_string;
    }
    
    /***
     * 
     * incluirToken( <objToken> ) : ID do novo token
     * 
     * Cadastra o token e devolve o ID do novo token.
     * 
     ***/
    public function incluirToken($PARM_objToken) {
        //
        $RET_id_token = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_aval_token ";
                $sSQL .= " (id_avaliacao, id_cliente, dt_criacao, cd_publico_alvo, ";
                $sSQL .= " ds_observacao, ds_email, ds_nome, ds_codigo, ds_unidade, ";
                $sSQL .= " dv_sexo, dt_nascimento, nr_cep, nr_telefone ";
                $sSQL .= " ) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                $sSQL .= ", now()";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->CodigoPublicoAlvo);
                //
                if($PARM_objToken->Observacoes) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Observacoes) . "'";
                } else {
                    $sSQL .= ", null";
                };
                //
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Email) . "'";
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Nome) . "'";
                //
                if($PARM_objToken->Codigo) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Codigo) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->Unidade) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Unidade) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->Sexo) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Sexo) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->DataNascimento) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->DataNascimento) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->CEP) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->CEP) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                if($PARM_objToken->Telefone) {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->Telefone) . "'";
                } else {
                    $sSQL .= ", null";
                }
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id_token = mysqli_insert_id($objLink);
                    //
                    $PARM_objToken->Id = $RET_id_token;
                    // Gera o hash
                    $PARM_objToken->Hash = $this->gerarHashToken($PARM_objToken->IdCliente, $PARM_objToken->IdAvaliacao, $PARM_objToken->Id);
                    // Grava na tabela
                    $this->gravarToken($PARM_objToken);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_id_token);
    }
     
    /***
     * 
     * gravarToken( <objToken> ) : 0 = Ok, 1 = Falhou
     * 
     * Atualiza os dados do token.
     * 
     * Devolve 0 se OK, 1 se falhou. Verificar o erro nos atributos da classe
     * master.
     * 
     ***/
    public function gravarToken($PARM_objToken) {
        //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta o UPDATE
                $sSQL = "UPDATE tbdw_aval_token  ";
		$sSQL .= " SET ";
                $sSQL .= " ds_nome  = '". mysqli_escape_string($objLink, $PARM_objToken->Nome) . "'";
		$sSQL .= ", ds_email  = '". mysqli_escape_string($objLink, $PARM_objToken->Email) . "'";
                if($PARM_objToken->Hash) {
                    $sSQL .= ", cd_hash = '". mysqli_escape_string($objLink, $PARM_objToken->Hash) . "'";
                }
                if($PARM_objToken->Codigo) {
                    $sSQL .= ", ds_codigo = '". mysqli_escape_string($objLink, $PARM_objToken->Codigo) . "'";
                }
                if($PARM_objToken->Observacoes) {
                    $sSQL .= ", ds_observacao  = '". mysqli_escape_string($objLink, $PARM_objToken->Observacoes) . "'";
                }
                if($PARM_objToken->Unidade) {
                    $sSQL .= ", ds_unidade  = '". mysqli_escape_string($objLink, $PARM_objToken->Unidade) . "'";
                }
                if($PARM_objToken->Sexo) {
                    $sSQL .= ", dv_sexo  = '". mysqli_escape_string($objLink, $PARM_objToken->Sexo) . "'";
                }
                if($PARM_objToken->DataNascimento) {
                    $sSQL .= ", dt_nascimento  = '". mysqli_escape_string($objLink, $PARM_objToken->DataNascimento) . "'";
                }
                if($PARM_objToken->CEP) {
                    $sSQL .= ", nr_cep  = '". mysqli_escape_string($objLink, $PARM_objToken->CEP) . "'";
                }
                if($PARM_objToken->Telefone) {
                    $sSQL .= ", nr_telefone  = '". mysqli_escape_string($objLink, $PARM_objToken->Telefone) . "'";
                }
		$sSQL .= " WHERE id_token = " . mysqli_escape_string($objLink, $PARM_objToken->Id);
                $sSQL .= " AND id_cliente = " . mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                $sSQL .= " AND id_avaliacao = " . mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                
                //$sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objToken->DataCriacao) . "'";
                //$sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objToken->CodigoPublicoAlvo);
                
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }

    /***
     * 
     * excluirToken( <objToken> ) : 0 = Ok, 1 = Falhou
     * 
     * Remove o token especificado.
     * 
     * Devolve 0 se OK, 1 se falhou. Verificar o erro nos atributos da classe
     * master.
     * 
     ***/
    public function excluirToken($PARM_objToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // BRADESCO
                    // --------
                    // Monta o DELETE dos setores
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_setor ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Monta o DELETE das marcas
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_marca ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Monta o DELETE do token do Bradesco
                    $sSQL  = " DELETE FROM tbdw_braabf17_token ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_aval_token ";
                    $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                    $sSQL .= " AND id_cliente = ". mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                    $sSQL .= " AND id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
    
    /***
     * 
     * reiniciarPesquisaToken( <objToken> ) : 0 = Ok, 1 = Falhou
     * 
     * Reinicia a pesquisa do token especificado.
     * 
     * Devolve 0 se OK, 1 se falhou. Verificar o erro nos atributos da classe
     * master.
     * 
     ***/
    public function reiniciarPesquisaToken($PARM_objToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o DELETE
                $sSQL  = " DELETE FROM tbdw_af_token_item ";
                $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!";
                    //
                } else {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_aval_form_token ";
                    $sSQL .= " WHERE id_avaliacao = ". mysqli_escape_string($objLink, $PARM_objToken->IdAvaliacao);
                    $sSQL .= " AND id_cliente = ". mysqli_escape_string($objLink, $PARM_objToken->IdCliente);
                    $sSQL .= " AND id_token = ". mysqli_escape_string($objLink, $PARM_objToken->Id);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    //
                    // Se falhou...
                    if(mysqli_errno($objLink) != 0) {
                        //
                        $RET_ok = 1;
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                    }
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
    /***
     * 
     * obterRespostasFranqueador( <id-pesquisa>, <id-token-franqueador> ) : array de clsDWC
     * 
     * Devolve a matriz com as perguntas e respostas dos questionarios aplicados
     * ao franqueador.
     * 
     ***/
    public function obterRespostasFranqueador($PARM_id_pesquisa, $PARM_id_franqueador) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL = "SELECT";
                $sSQL .= " itemform.cd_tema AS cd_tema";
                $sSQL .= ", tema.ds_tema AS ds_tema";
                $sSQL .= ", item.cd_item AS cd_item";
                $sSQL .= ", item.ds_item AS ds_item";
                $sSQL .= ", item.dv_tipo_opcoes AS dv_tipo_opcoes";
                $sSQL .= ", itemform.id_item_formulario AS id_item_formulario";
                $sSQL .= ", itemform.id_formulario AS id_formulario";
                $sSQL .= ", itemform.cd_agrupamento AS cd_agrupamento";
                $sSQL .= ", agrup.ds_agrupamento";
                $sSQL .= ", resp.ds_resposta AS ds_resposta";
                $sSQL .= ", opcao.ds_opcao_item AS ds_opcao_item";
                $sSQL .= ", opcao.cd_opcao_item AS cd_opcao_item";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_avaliacao_formulario AS af";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_item_formulario AS itemform ON itemform.id_formulario = af.id_formulario";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_item AS item ON item.cd_item = itemform.cd_item AND item.cd_agrupamento = itemform.cd_agrupamento AND item.cd_tema = itemform.cd_tema";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_agrupamento AS agrup ON agrup.cd_agrupamento = item.cd_agrupamento";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_tema AS tema ON tema.cd_tema = itemform.cd_tema AND tema.cd_agrupamento = item.cd_agrupamento";
                $sSQL .= " LEFT JOIN ";
                $sSQL .= " tbdw_aval_token AS token ON token.id_avaliacao = af.id_avaliacao AND token.cd_publico_alvo = af.cd_publico_alvo AND token.id_token = " . mysqli_escape_string($objLink, $PARM_id_franqueador);
                $sSQL .= " LEFT JOIN ";
                $sSQL .= " tbdw_af_token_item AS resp ON resp.id_item_formulario = itemform.id_item_formulario AND resp.id_token = token.id_token";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_opcao_item AS opcao ON opcao.cd_opcao_item = resp.cd_opcao_item AND opcao.cd_agrupamento = item.cd_agrupamento AND opcao.cd_tema = item.cd_tema";
                $sSQL .= " WHERE af.id_avaliacao = " . mysqli_escape_string($objLink, $PARM_id_pesquisa);
                $sSQL .= " AND af.cd_publico_alvo = 1";
                $sSQL .= " ORDER";
                $sSQL .= " BY";
                $sSQL .= " af.id_avaliacao_formulario";
                $sSQL .= ", itemform.cd_tema";
                $sSQL .= ", tema.ds_tema";
                $sSQL .= ", item.cd_item";
                $sSQL .= ", item.ds_item";
                $sSQL .= ", item.dv_tipo_opcoes";
                $sSQL .= ", itemform.nr_posicao";
                $sSQL .= ", itemform.id_item_formulario";
                $sSQL .= ", itemform.cd_agrupamento";
                $sSQL .= ", agrup.ds_agrupamento";
                $sSQL .= ", opcao.cd_opcao_item";
                $sSQL .= ", resp.ds_resposta";
                $sSQL .= ", opcao.ds_opcao_item";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados! ";
                    //
                // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    $iPos = 0;
                    //
                    while($rsDados) {
                        //
                        unset($objItem);
                        $objItem = new clsDWCfranqueadorRespostaItem();
                        //
                        $qtdeTotal = 0;
                        //
                        $objItem->CodigoItem = $rsDados["cd_item"];
                        $objItem->CodigoTema = $rsDados["cd_tema"];
                        $objItem->DescricaoTema = $rsDados["ds_tema"];
                        $objItem->DescricaoItem = $rsDados["ds_item"];
                        $objItem->IdItemFormulario = $rsDados["id_item_formulario"];
                        $objItem->IdFormulario = $rsDados["id_formulario"];
                        $objItem->CodigoAgrupamento = $rsDados["cd_agrupamento"];
                        $objItem->DescricaoAgrupamento = $rsDados["ds_agrupamento"];
                        $objItem->TipoOpcoes = $rsDados["dv_tipo_opcoes"];
                        $objItem->QuantidadeRespostas = 0;
                        //
                        unset($arrayTemp_colOpcoes);
                        //$arrayTemp_colOpcoes = $this->obterOpcoesDeRespostas($objItem->CodigoAgrupamento, $objItem->CodigoTema, $objItem->CodigoItem);
                        //
                        unset($arrayTemp_colRespostas);
                        $arrayTemp_colRespostas = array();
                        //
                        while( 
                                    ($rsDados) 
                                &&  $rsDados["cd_tema"] == $objItem->CodigoTema
                                &&  $rsDados["id_item_formulario"] == $objItem->IdItemFormulario
                                &&  $rsDados["id_formulario"] == $objItem->IdFormulario
                                &&  $rsDados["cd_agrupamento"] == $objItem->CodigoAgrupamento
                                ) {
                            //
                            unset($objValor);
                            $objValor = new clsDWCfranqueadorRespostaValor();
                            //
                            $objValor->Resposta = $rsDados["ds_resposta"];
                            if($rsDados["ds_opcao_item"]) {
                                $objValor->DescricaoOpcaoItem = $rsDados["ds_opcao_item"];
                            } else {
                                $objValor->DescricaoOpcaoItem = $rsDados["ds_resposta"];
                            }
                            //
                            if($objValor->DescricaoOpcaoItem) {
                                //
                                $objItem->QuantidadeRespostas++;
                                //
                                $objValor->CodigoOpcao = $rsDados["cd_opcao_item"];
                                //
                                $arrayTemp_colRespostas[$objValor->CodigoOpcao] = $objValor;
                            }
                            //
                            $rsDados = mysqli_fetch_assoc($objResult);
                        }
                        //
                        $objItem->colRespostas = $arrayTemp_colRespostas;
                        //
                        $RET_array[$iPos] = $objItem;
                        $iPos++;
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }    
}

//  obterTokenPeloID(<ID-Token>)
//  
//  Retorna o objeto token pelo ID fornecido
//  For�a a carga da cole��o de tokens, caso ela n�o esteja carregada
//
function DWS_obterTokenPeloID($PARM_colTokens, $PARM_id_token) {
    //
    // Inicializa o retorno
    $RET_objToken = null;
    // Percorre cada token da cole��o
    foreach ($PARM_colTokens AS $objItem) {
        // Se encontrou o ID desejado...
        if($objItem->Id == $PARM_id_token) {
            // Atribui para a vari�vel de retorno
            $RET_objToken = $objItem;
            // Quebra o la�o "foreach"
            break;
        }
    }
    //
    return($RET_objToken);
}


// 0 = Qtde. total de perguntas
// 1 = Qtde. de perguntas respondidas
// 2 = Qtde. de perguntas que faltam ser respondidas
function DWS_obterQtde($PARM_id_avaliacao, $PARM_id_token, $PARM_tipo, $db_host, $db_user, $db_pass, $db_name) {
    //
    $RET_qtde = 0;
    //
    $objDWsurvey = new DEFWEBsurvey($PARM_id_avaliacao, $PARM_id_token);
    //
    if($objDWsurvey) {
        //
        switch($PARM_tipo) {
            case 1:
                $RET_qtde = $objDWsurvey->qt_respondidas;
                break;
            case 2:
                $RET_qtde = $objDWsurvey->qt_nao_respondidas;
                break;
            default:
                $RET_qtde = $objDWsurvey->qt_total;
        }
    }
    //
    return($RET_qtde);
}

class DWXsumarioQtde {
    public $qt_respondidas = 0;
    public $qt_nao_respondidas = 0;
    public $qt_total = 0;
}

// 0 = Qtde. total de perguntas
// 1 = Qtde. de perguntas respondidas
// 2 = Qtde. de perguntas que faltam ser respondidas
function DWS_obterQtdeNovo($PARM_id_avaliacao, $PARM_id_token, $db_host, $db_user, $db_pass, $db_name) {
    //
    $RET_obj = new DWXsumarioQtde();
    //
    $objDWsurvey = new DEFWEBsurvey($PARM_id_avaliacao, $PARM_id_token);
    //
    if($objDWsurvey) {
        //
        $RET_obj->qt_respondidas = $objDWsurvey->qt_respondidas;
        $RET_obj->qt_nao_respondidas = $objDWsurvey->qt_nao_respondidas;
        $RET_obj->qt_total = $objDWsurvey->qt_total;
    }
    //
    return($RET_obj);
}

