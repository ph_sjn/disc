<?php

function utf8json($inArray) {

    static $depth = 0;

    /* our return object */
    $newArray = array();

    /* safety recursion limit */
    $depth ++;
//    if($depth >= '30') {
//        return false;
//    }

    /* step through inArray */
    foreach($inArray as $key=>$val) {
        if(is_array($val)) {
            /* recurse on array elements */
            $newArray[$key] = utf8json($val);
        } else {
            /* encode string values */
            $newArray[$key] = utf8_encode($val);
        }
    }

    /* return utf8 encoded array */
    return $newArray;
}

function htmljson($inArray) {

    static $depth = 0;

    /* our return object */
    $newArray = array();

    /* safety recursion limit */
    $depth ++;
//    if($depth >= '30') {
//        return false;
//    }

    /* step through inArray */
    foreach($inArray as $key=>$val) {
        if(is_array($val)) {
            /* recurse on array elements */
            $newArray[$key] = htmljson($val);
        } else {
            /* encode string values */
            $newArray[$key] = htmlentities($val);
        }
    }

    /* return utf8 encoded array */
    return $newArray;
}

if (!function_exists('json_last_error_msg')) {
	function json_last_error_msg() {
		static $ERRORS = array(
		JSON_ERROR_NONE => 'No error',
		JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
		JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
		JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
		JSON_ERROR_SYNTAX => 'Syntax error',
		JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
		);

		$error = json_last_error();
		return isset($ERRORS[$error]) ? $ERRORS[$error] : 'Unknown error';
	}
}

function safe_json_encode($value){
    if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
        $encoded = json_encode($value, JSON_PRETTY_PRINT);
    } else {
        $encoded = json_encode($value);
    }
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            return $encoded;
        case JSON_ERROR_DEPTH:
            return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_STATE_MISMATCH:
            return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_CTRL_CHAR:
            return 'Unexpected control character found';
        case JSON_ERROR_SYNTAX:
            return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
        case JSON_ERROR_UTF8:
            $clean = utf8ize($value);
            return safe_json_encode($clean);
        default:
            return 'Unknown error'; // or trigger_error() or throw new Exception()

    }
}

function utf8ize($mixed) {
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } else if (is_string ($mixed)) {
        return utf8_encode($mixed);
    }
    return $mixed;
}

/**
 * Passa a URL e os par�metros pela fun��o
 * Para retorno do corpo de resposta, use o par�metro de refer�ncia $body
 * Para retorno de poss�vel erro, use o par�metro de refer�ncia $errormsg
 * A fun��o retorna true em caso de sucesso e false em caso de falha
 * A falha ocorre se as fun��es do CURL retornarem valores que signifiquem falha
 * A falha tamb�m ocorre de acordo com as suas necessidades na valida��o da resposta
 * Por exemplo (Aplique de acordo com as suas necessidades):
 * 
 * Se a sa�da for vazia
 * Se a sa�da tiver um padr�o e atrav�s de express�o regular retornar falso
 * Se a sa�da estiver em JSON e obrigatoriamente precisa que exista um par�metro "status"
 * Se a sa�da estiver sm JSON e o par�metro "status" seja diferente de "SUCCESS"
 * 
 * A fun��o abaixo � apenas uma demonstra��o de como saber se algo deu certo ou n�o
 * Modifique de acordo com as suas necessidades
 */

function curl_post_42252( $url, $params = array(), &$body = null, &$errormsg = null ) {

    $fields = http_build_query( $params, '', '&' );

    set_time_limit(60);

    $ch = curl_init();

    $options = array(
        CURLOPT_CONNECTTIMEOUT => 30,
        CURLOPT_ENCODING       => '',
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HEADER         => true,
        CURLOPT_NOPROGRESS     => false,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $fields,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_URL            => $url,
        CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0',
        CURLOPT_VERBOSE        => true,
    );

    curl_setopt_array( $ch, $options );

    $exec  = curl_exec( $ch );
    $info  = curl_getinfo( $ch );
    $head  = substr( $exec, 0, $info['header_size'] );
    $body  = substr( $exec, $info['header_size'] );
    $error = curl_error( $ch );
    $errno = curl_errno( $ch );

    curl_close( $ch );

    /**
     * In�cio das verifica��es para saber se a requisi��o foi feita corretamente
     */

    /**
     * Exec retornou falso?
     */

    if ( $exec === false ) {
        $errormsg = 'curl_exec';
        return false;
    }

    /**
     * Tem algum erro?
     */

    if ( $error !== '' ) {
        $errormsg = 'curl_error';
        return false;
    }

    /**
     * Tem algum erro? (Redundante)
     */

    if ( $errno ) {
        $errormsg = 'curl_errno';
        return false;
    }

    /**
     * A parte acima verifica se n�o houve erros nas fun��es do CURL (HTTPS, Timeout, etc...)
     * Daqui para baixo est�o os exemplos de como validar e saber se realmente a resposta de sa�da � v�lida
     * Usando express�o regular ou JSON
     * Adapte de acordo com as suas necessidades
     */

    return true;

    /**
     * C�digo HTTP de resposta � diferente de 200? Geralmente o c�digo HTTP de resposta � 200
     */

    if ( $info['http_code'] !== 200 ) {
        $errormsg = 'curl_http_code';
        return false;
    }

    /**
     * Corpo (resposta) (html/json/xml) � vazio? Geralmente h� uma sa�da em JSON ou com algum valor simples informando falha ou sucesso
     */

    if ( trim( $body ) === '' ) {
        $errormsg = 'curl_body_empty';
        return false;
    }

    /**
     * Valida��o por express�o regular (Exemplo fict�cio)
     */

    $pattern = '/ID\:/';

    if ( !preg_match( $pattern, $body ) ) {
        $errormsg = 'pattern_dont_match';
        return false;
    }

    /**
     * Valida��o por valor JSON, supondo que a resposta esteja formatada em JSON e possua a chave "status" com o valor "SUCCESS" ou "ERROR" para Sucesso ou Erro
     */

    if ( ( $json = json_decode( $body ) ) === false || $json === null ) {
        $errormsg = 'json_decode_error';
        return false;
    }

    if ( !isset( $json->status ) ) {
        $errormsg = 'json_status_undefined';
        return false;
    }

    if ( $json->status !== 'SUCCESS' ) {
        $errormsg = 'json_status_fail';
        return false;
    }

    return true;

}
