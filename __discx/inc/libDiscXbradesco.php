<?php
/******
 * 
 * libDiscXbradesco.php
 * 
 ******/

include_once "/home/users/__discx/inc/incConfig.php";

// BD: discmaster..tbdw_braabf17_marca
//
// ::consultarMarca(<id>, <codigo>))
// ::listarMarcas()
class clsDSXbradescoMarca {
    //
    Public $Id = null;      // id_marca
    Public $Codigo = null;  // cd_marca
    Public $Nome = null;    // nm_marca
}

// BD: discmaster..tbdw_braabf17_setor
//
// ::consultarSetor(<id>, <codigo>))
// ::listarSetores()
class clsDSXbradescoSetor {
    //
    Public $Id = null;      // id_setor
    Public $Codigo = null;  // cd_setor
    Public $Nome = null;    // nm_setor
}

class clsDSXtokenBradesco {
    public $Id = null;
    // tbdw_braabf17_token.fl_intencao_investir
    public $PretendeInvestir = null;
    // tbdw_braabf17_token.nr_meses
    public $InvestirMeses = null;
    // tbdw_braabf17_token.nr_cpf
    public $CPF = null;
    // tbdw_braabf17_token.fl_autoriza_comunic
    public $AutorizaContato = null;
    // tbdw_braabf17_token_setor
    public $SetoresInteresse = null;
    // tbdw_braabf17_token_marca
    public $MarcasInteresseLista = null;
    // tbdw_braabf17_token.ds_marcas_interesse
    public $MarcasInteresse = null;
    // tbdw_braabf17_token.dv_faixa_valor_invest
    public $FaixaValorInvestimento = null;
    // tbdw_braabf17_token.ds_cidade_interesse
    public $CidadeInteresse = null;
    // tbdw_braabf17_token.ds_cidade_residencia
    public $CidadeResidencia = null;
    // tbdw_braabf17_token.ds_estado_residencia
    public $EstadoResidencia = null;
}

// BD: discmaster
//
// ::consultarAvaliacaoDoProjeto(<id_projeto>, <id_avaliacao>))
// ::listarAvaliacoesDoProjeto(<id_projeto>)
class clsDSXbradescoAvaliacao {
    //
	Public $IdAvaliacao = null;						// id_avaliacao             
	Public $IdCliente = null;                       	// id_cliente               
	Public $DataInicio = null;                       // dt_inicio                
	Public $DataFim = null;                          // dt_fim                   
	Public $CodigoSituacao = null;                 	// cd_sit_avaliacao         
	Public $Titulo = null;                        	// ds_titulo                
	Public $IdProjeto = null;                       	// id_projeto               
	Public $DsEmailAssunto = null;                	// ds_email_assunto         
	Public $DsEmailTxtInicial = null;             	// ds_email_txt_inicial     
	Public $DsEmailTxtLink = null;                	// ds_email_txt_link        
	Public $DsEmailTxtFinal = null;               	// ds_email_txt_final       
	Public $IdTokenFranqueador = null;            	// id_token_franqueador     
	Public $DsFranqueadorIntro = null;            	// ds_franqueador_intro     
	Public $DsFranqueadoIntro = null;             	// ds_franqueado_intro      
	Public $NmArqTopoOriginal = null;             	// nmarq_topo_original      
	Public $NmArqTopoInterno = null;              	// nmarq_topo_interno       
	Public $IdPaginaConfCad = null;               	// id_pagina_confcad        
	Public $IdPaginaInstrucoes = null;            	// id_pagina_instrucoes     
	Public $IdPaginaQuestionario = null;          	// id_pagina_questionario   
	Public $IdPaginaConclusao = null;             	// id_pagina_conclusao      
	Public $IdPaginaRelatorio = null;             	// id_pagina_relatorio      
	Public $IdPaginaRelAdmin = null;              	// id_pagina_reladmin       
	Public $DsEmailSubjConclusao = null;          	// ds_email_subj_conclusao  
	Public $DsEmailBodyConclusao = null;          	// ds_email_body_conclusao  
	Public $FlEnviarEmailConclusao = null;        	// fl_enviar_email_conclusao
}

class clsDSXbradesco {
    //
    
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_complemento = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function consultarSetor($PARM_id, $PARM_codigo) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "" || $PARM_codigo != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_setor, cd_setor, nm_setor";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_setor";
                    $sSQL .= " WHERE";
                    //
                    if($PARM_id != "") {
                        $sSQL .= " id_setor = " . mysqli_escape_string($objLink, $PARM_id);
                    } else {
                        $sSQL .= " cd_setor = '" . mysqli_escape_string($objLink, $PARM_codigo) . "'";
                    }
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXbradescoSetor();
                            //
                            $RET_obj->Id = $rsDados["id_setor"];
                            $RET_obj->Codigo = $rsDados["cd_setor"];
                            $RET_obj->Nome = $rsDados["nm_setor"];
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
        
    public function listarSetores() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " id_setor, cd_setor, nm_setor";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_setor";
                $sSQL .= " ORDER BY nm_setor;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXbradescoSetor();
                        //
                        $objLinha->Id = $rsDados["id_setor"];
                        $objLinha->Codigo = $rsDados["cd_setor"];
                        $objLinha->Nome = $rsDados["nm_setor"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarMarca($PARM_id, $PARM_codigo) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "" || $PARM_codigo != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_marca, cd_marca, nm_marca";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_marca";
                    $sSQL .= " WHERE";
                    //
                    if($PARM_id != "") {
                        $sSQL .= " id_marca = " . mysqli_escape_string($objLink, $PARM_id);
                    } else {
                        $sSQL .= " cd_marca = '" . mysqli_escape_string($objLink, $PARM_codigo) . "'";
                    }
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXbradescoSetor();
                            //
                            $RET_obj->Id = $rsDados["id_marca"];
                            $RET_obj->Codigo = $rsDados["cd_marca"];
                            $RET_obj->Nome = $rsDados["nm_marca"];
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
        
    public function listarMarcas() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " id_marca, cd_marca, nm_marca";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_marca";
                $sSQL .= " ORDER BY nm_marca;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXbradescoSetor();
                        //
                        $objLinha->Id = $rsDados["id_marca"];
                        $objLinha->Codigo = $rsDados["cd_marca"];
                        $objLinha->Nome = $rsDados["nm_marca"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarMarcasToken($PARM_id) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " mt.id_token_marca, mt.id_token, mt.id_marca, cm.id_marca, cm.cd_marca, cm.nm_marca";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_token_marca AS mt";
                $sSQL .= " LEFT JOIN tbdw_braabf17_marca AS cm";
                $sSQL .= " ON cm.id_marca = mt.id_marca";
                $sSQL .= " WHERE ";
                $sSQL .= " mt.id_token = " . mysqli_escape_string($objLink, $PARM_id);
                $sSQL .= " ORDER BY cm.nm_marca;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXbradescoMarca();
                        //
                        $objLinha->Id = $rsDados["id_marca"];
                        $objLinha->Codigo = $rsDados["cd_marca"];
                        $objLinha->Nome = $rsDados["nm_marca"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarSetoresToken($PARM_id) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " st.id_token_setor, st.id_token, st.id_setor, cs.id_setor, cs.cd_setor, cs.nm_setor";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_token_setor AS st";
                $sSQL .= " LEFT JOIN tbdw_braabf17_setor AS cs";
                $sSQL .= " ON cs.id_setor = st.id_setor";
                $sSQL .= " WHERE ";
                $sSQL .= " st.id_token = " . mysqli_escape_string($objLink, $PARM_id);
                $sSQL .= " ORDER BY cs.nm_setor;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXbradescoSetor();
                        //
                        $objLinha->Id = $rsDados["id_setor"];
                        $objLinha->Codigo = $rsDados["cd_setor"];
                        $objLinha->Nome = $rsDados["nm_setor"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarTokenBradesco($PARM_id) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_token, fl_intencao_investir, nr_meses, nr_cpf, fl_autoriza_comunic, ds_marcas_interesse, dv_faixa_valor_invest, ds_cidade_interesse, ds_cidade_residencia, ds_estado_residencia";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_token";
                    $sSQL .= " WHERE";
                    //
                    $sSQL .= " id_token = " . mysqli_escape_string($objLink, $PARM_id);
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXtokenBradesco();
                            //
                            $RET_obj->Id = $rsDados["id_token"];
                            $RET_obj->PretendeInvestir = $rsDados["fl_intencao_investir"];
                            $RET_obj->InvestirMeses = $rsDados["nr_meses"];
                            $RET_obj->CPF = $rsDados["nr_cpf"];
                            $RET_obj->AutorizaContato = $rsDados["fl_autoriza_comunic"];
                            $RET_obj->MarcasInteresse = $rsDados["ds_marcas_interesse"];
                            $RET_obj->FaixaValorInvestimento = $rsDados["dv_faixa_valor_invest"];
                            $RET_obj->CidadeInteresse = $rsDados["ds_cidade_interesse"];
                            $RET_obj->CidadeResidencia = $rsDados["ds_cidade_residencia"];
                            $RET_obj->EstadoResidencia = $rsDados["ds_estado_residencia"];
                            //
                            $RET_obj->MarcasInteresseLista = $this->consultarMarcasToken($RET_obj->Id);
                            $RET_obj->SetoresInteresse = $this->consultarSetoresToken($RET_obj->Id);
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
    
    public function gravarTokenBradesco($PARM_objToken, $PARM_objTokenBradesco) {
        //
        $this->removerMarcas($PARM_objToken->Id);
        $this->removerSetores($PARM_objToken->Id);
        $this->removerTokenBradesco($PARM_objToken->Id);
        //
        $this->incluirTokenBradesco($PARM_objToken, $PARM_objTokenBradesco);
        //
        if(!is_null($PARM_objTokenBradesco->MarcasInteresseLista)) {
            foreach($PARM_objTokenBradesco->MarcasInteresseLista AS $objItem) {
                //
                $this->incluirMarcaInteresse($PARM_objToken->Id, $objItem->Id);
            }
        }
        //
        if(!is_null($PARM_objTokenBradesco->SetoresInteresse)) {
            foreach($PARM_objTokenBradesco->SetoresInteresse AS $objItem) {
                //
                $this->incluirSetorInteresse($PARM_objToken->Id, $objItem->Id);
            }
        }
    }
    
    public function incluirMarcaInteresse($PARM_IdToken, $PARM_Id) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token_marca ";
                $sSQL .= " (id_token, id_marca) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_IdToken);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_Id);
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function incluirSetorInteresse($PARM_IdToken, $PARM_Id) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token_setor ";
                $sSQL .= " (id_token, id_setor) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_IdToken);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_Id);
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function incluirTokenBradesco($PARM_objToken, $PARM_objTokenBradesco) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token ";
                $sSQL .= " (id_token, fl_intencao_investir";
                $sSQL .= ", nr_meses, nr_cpf, fl_autoriza_comunic";
                $sSQL .= ", ds_marcas_interesse, dv_faixa_valor_invest, ds_cidade_interesse, ds_cidade_residencia, ds_estado_residencia";
                $sSQL .= ") ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_objToken->Id);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenBradesco->PretendeInvestir);
                //
                if(!is_null($PARM_objTokenBradesco->InvestirMeses) && $PARM_objTokenBradesco->InvestirMeses != "") {
                    $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenBradesco->InvestirMeses);
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objTokenBradesco->CPF) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenBradesco->AutorizaContato);
                //
                if(!is_null($PARM_objTokenBradesco->MarcasInteresse) && $PARM_objTokenBradesco->MarcasInteresse != "") {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objTokenBradesco->MarcasInteresse) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if(!is_null($PARM_objTokenBradesco->FaixaValorInvestimento) && $PARM_objTokenBradesco->FaixaValorInvestimento != "") {
                    $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenBradesco->FaixaValorInvestimento);
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if(!is_null($PARM_objTokenBradesco->CidadeInteresse) && $PARM_objTokenBradesco->CidadeInteresse != "") {
                    $sSQL .= ", '" . mysqli_escape_string($objLink, $PARM_objTokenBradesco->CidadeInteresse) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if(!is_null($PARM_objTokenBradesco->CidadeResidencia) && $PARM_objTokenBradesco->CidadeResidencia != "") {
                    $sSQL .= ", '" . mysqli_escape_string($objLink, $PARM_objTokenBradesco->CidadeResidencia) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if(!is_null($PARM_objTokenBradesco->EstadoResidencia) && $PARM_objTokenBradesco->EstadoResidencia != "") {
                    $sSQL .= ", '" . mysqli_escape_string($objLink, $PARM_objTokenBradesco->EstadoResidencia) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function removerSetores($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_setor ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
    public function removerMarcas($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_marca ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
        
    }
    
    public function removerTokenBradesco($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
    public function listarAvaliacoesDoProjeto($PARM_IdProjeto){
        //
        $RET_array = array();
        //
        // Se veio o ID...
        if($PARM_IdProjeto != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
					$sSQL .= "  id_avaliacao";
					$sSQL .= ", id_cliente";
					$sSQL .= ", dt_inicio";
					$sSQL .= ", dt_fim";
					$sSQL .= ", cd_sit_avaliacao";
					$sSQL .= ", ds_titulo";
					$sSQL .= ", id_projeto";
					$sSQL .= ", ds_email_assunto";
					$sSQL .= ", ds_email_txt_inicial";
					$sSQL .= ", ds_email_txt_link";
					$sSQL .= ", ds_email_txt_final";
					$sSQL .= ", id_token_franqueador";
					$sSQL .= ", ds_franqueador_intro";
					$sSQL .= ", ds_franqueado_intro";
					$sSQL .= ", nmarq_topo_original";
					$sSQL .= ", nmarq_topo_interno";
					$sSQL .= ", id_pagina_confcad";
					$sSQL .= ", id_pagina_instrucoes";
					$sSQL .= ", id_pagina_questionario";
					$sSQL .= ", id_pagina_conclusao";
					$sSQL .= ", id_pagina_relatorio";
					$sSQL .= ", id_pagina_reladmin";
					$sSQL .= ", ds_email_subj_conclusao";
					$sSQL .= ", ds_email_body_conclusao";
					$sSQL .= ", fl_enviar_email_conclusao";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_avaliacao";
                    $sSQL .= " WHERE";
                    //
                    $sSQL .= " id_projeto = " . mysqli_escape_string($objLink, $PARM_IdProjeto);
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        while ($rsDados) {
                            //
                            $RET_obj = new clsDSXbradescoAvaliacao();
                            //
							$RET_obj->IdAvaliacao=				$rsDados["id_avaliacao"];
							$RET_obj->IdCliente=                $rsDados["id_cliente"];
							$RET_obj->DataInicio=               $rsDados["dt_inicio"];
							$RET_obj->DataFim=                  $rsDados["dt_fim"];
							$RET_obj->CodigoSituacao=	        $rsDados["cd_sit_avaliacao"];
							$RET_obj->Titulo=	                $rsDados["ds_titulo"];
							$RET_obj->IdProjeto=                $rsDados["id_projeto"];
							$RET_obj->DsEmailAssunto=	        $rsDados["ds_email_assunto"];
							$RET_obj->DsEmailTxtInicial=	    $rsDados["ds_email_txt_inicial"];
							$RET_obj->DsEmailTxtLink=	        $rsDados["ds_email_txt_link"];
							$RET_obj->DsEmailTxtFinal=	        $rsDados["ds_email_txt_final"];
							$RET_obj->IdTokenFranqueador=	    $rsDados["id_token_franqueador"];
							$RET_obj->DsFranqueadorIntro=	    $rsDados["ds_franqueador_intro"];
							$RET_obj->DsFranqueadoIntro=	    $rsDados["ds_franqueado_intro"];
							$RET_obj->NmArqTopoOriginal=	    $rsDados["nmarq_topo_original"];
							$RET_obj->NmArqTopoInterno=	        $rsDados["nmarq_topo_interno"];
							$RET_obj->IdPaginaConfCad=	        $rsDados["id_pagina_confcad"];
							$RET_obj->IdPaginaInstrucoes=	    $rsDados["id_pagina_instrucoes"];
							$RET_obj->IdPaginaQuestionario=	    $rsDados["id_pagina_questionario"];
							$RET_obj->IdPaginaConclusao=	    $rsDados["id_pagina_conclusao"];
							$RET_obj->IdPaginaRelatorio=	    $rsDados["id_pagina_relatorio"];
							$RET_obj->IdPaginaRelAdmin=	        $rsDados["id_pagina_reladmin"];
							$RET_obj->DsEmailSubjConclusao=	    $rsDados["ds_email_subj_conclusao"];
							$RET_obj->DsEmailBodyConclusao=	    $rsDados["ds_email_body_conclusao"];
							$RET_obj->FlEnviarEmailConclusao=	$rsDados["fl_enviar_email_conclusao"];
							//
							if ( preg_match ("/hidden/", $RET_obj->Titulo) ){
								$RET_obj->Titulo = str_replace("hidden","", $RET_obj->Titulo);
							}
							//
							$RET_array[$RET_obj->IdAvaliacao] = $RET_obj;
							//
							$rsDados = mysqli_fetch_assoc($objResult);
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarAvaliacaoDoProjeto($PARM_IdProjeto, $PARM_IdAvaliacao){
        //
        $RET_obj = null;
        //
        // Se veio o ID...
        if($PARM_IdProjeto != "" && $PARM_IdAvaliacao != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
					$sSQL .= "  id_avaliacao";
					$sSQL .= ", id_cliente";
					$sSQL .= ", dt_inicio";
					$sSQL .= ", dt_fim";
					$sSQL .= ", cd_sit_avaliacao";
					$sSQL .= ", ds_titulo";
					$sSQL .= ", id_projeto";
					$sSQL .= ", ds_email_assunto";
					$sSQL .= ", ds_email_txt_inicial";
					$sSQL .= ", ds_email_txt_link";
					$sSQL .= ", ds_email_txt_final";
					$sSQL .= ", id_token_franqueador";
					$sSQL .= ", ds_franqueador_intro";
					$sSQL .= ", ds_franqueado_intro";
					$sSQL .= ", nmarq_topo_original";
					$sSQL .= ", nmarq_topo_interno";
					$sSQL .= ", id_pagina_confcad";
					$sSQL .= ", id_pagina_instrucoes";
					$sSQL .= ", id_pagina_questionario";
					$sSQL .= ", id_pagina_conclusao";
					$sSQL .= ", id_pagina_relatorio";
					$sSQL .= ", id_pagina_reladmin";
					$sSQL .= ", ds_email_subj_conclusao";
					$sSQL .= ", ds_email_body_conclusao";
					$sSQL .= ", fl_enviar_email_conclusao";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_avaliacao";
                    $sSQL .= " WHERE";
                    //
                    $sSQL .= " id_projeto = " . mysqli_escape_string($objLink, $PARM_IdProjeto);
                    $sSQL .= " AND id_avaliacao = " . mysqli_escape_string($objLink, $PARM_IdAvaliacao);
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXbradescoAvaliacao();
                            //
							$RET_obj->IdAvaliacao=				$rsDados["id_avaliacao"];
							$RET_obj->IdCliente=                $rsDados["id_cliente"];
							$RET_obj->DataInicio=               $rsDados["dt_inicio"];
							$RET_obj->DataFim=                  $rsDados["dt_fim"];
							$RET_obj->CodigoSituacao=	        $rsDados["cd_sit_avaliacao"];
							$RET_obj->Titulo=	                $rsDados["ds_titulo"];
							$RET_obj->IdProjeto=                $rsDados["id_projeto"];
							$RET_obj->DsEmailAssunto=	        $rsDados["ds_email_assunto"];
							$RET_obj->DsEmailTxtInicial=	    $rsDados["ds_email_txt_inicial"];
							$RET_obj->DsEmailTxtLink=	        $rsDados["ds_email_txt_link"];
							$RET_obj->DsEmailTxtFinal=	        $rsDados["ds_email_txt_final"];
							$RET_obj->IdTokenFranqueador=	    $rsDados["id_token_franqueador"];
							$RET_obj->DsFranqueadorIntro=	    $rsDados["ds_franqueador_intro"];
							$RET_obj->DsFranqueadoIntro=	    $rsDados["ds_franqueado_intro"];
							$RET_obj->NmArqTopoOriginal=	    $rsDados["nmarq_topo_original"];
							$RET_obj->NmArqTopoInterno=	        $rsDados["nmarq_topo_interno"];
							$RET_obj->IdPaginaConfCad=	        $rsDados["id_pagina_confcad"];
							$RET_obj->IdPaginaInstrucoes=	    $rsDados["id_pagina_instrucoes"];
							$RET_obj->IdPaginaQuestionario=	    $rsDados["id_pagina_questionario"];
							$RET_obj->IdPaginaConclusao=	    $rsDados["id_pagina_conclusao"];
							$RET_obj->IdPaginaRelatorio=	    $rsDados["id_pagina_relatorio"];
							$RET_obj->IdPaginaRelAdmin=	        $rsDados["id_pagina_reladmin"];
							$RET_obj->DsEmailSubjConclusao=	    $rsDados["ds_email_subj_conclusao"];
							$RET_obj->DsEmailBodyConclusao=	    $rsDados["ds_email_body_conclusao"];
							$RET_obj->FlEnviarEmailConclusao=	$rsDados["fl_enviar_email_conclusao"];
							//
							if ( preg_match ("/hidden/", $RET_obj->Titulo) ){
								$RET_obj->Titulo = str_replace("hidden","", $RET_obj->Titulo);
							}
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
}