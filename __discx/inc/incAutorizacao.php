<?php

/***
 * 
 * IPs validos para consumir cada servico
 * 
 * 2015-03-10   MARCIO      Incluido codigo que permite maior controle
 *                          sobre os IPs com acesso rejeitado para por exemplo
 *                          registrar a tentativa de acesso em log:
 * 
 *                          Ajustar $RPWS_SemBloqueio = TRUE antes do include
 *                          incAutorizacao.php no servico.
 * 
 *                          Analisar $RPWS_IP_SemAcesso e $RPWS_retornoNaoAutorizado
 *                          para registrar o acesso nao autorizado e interromper o servico.
 * 
 ***/

function verificarIP($parm_servico, $parm_ip) {
    //
    $RPWS_IP_Localhost = "127.0.0.1";
    $RPWS_IP_Localhost2 = "::1";
    //
    $RPWS_IPs_Geral = array(
                $RPWS_IP_Localhost => $RPWS_IP_Localhost
            ,   $RPWS_IP_Localhost2 => $RPWS_IP_Localhost2
        );
    //
    $RPWS_Servicos_IPs = array(
                strtolower("wsdsx_obterCliente") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterPerfilFuncao") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterPesquisa") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterPesquisas") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterTokens") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterTokenId") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_criarPerfilFuncao") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_criarToken") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_gravarModeloEmail") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_gravarPerfilFuncao") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_gravarToken") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_excluirPerfilFuncao") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_excluirToken") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_obterRespFranqueador") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_reiniciarPesquisaToken") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_listarTiposDePaginas") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_listarClientes") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_listarProjetos") => $RPWS_IPs_Geral
            ,   strtolower("wsdsx_listarPerfisDeFuncao") => $RPWS_IPs_Geral
			,   strtolower("wsdsx_listarCamposEntrevistado") => $RPWS_IPs_Geral
			,   strtolower("wsdsx_buscarEntrevistados") => $RPWS_IPs_Geral
        );
    //
    $parm_servico = strtolower($parm_servico);
    //
    return ($RPWS_Servicos_IPs[$parm_servico][$parm_ip] == $parm_ip);
}

// Obtem o IP de onde veio a chamada para o servico
$RPWS_IP_Origem = $_SERVER["REMOTE_ADDR"];

// Indica se eh acesso nao autorizado
$RPWS_IP_SemAcesso = (!(verificarIP(strtolower($RPWS_Servico), $RPWS_IP_Origem)));
//
if($RPWS_IP_SemAcesso) {
    //
    // Monta o retorno
    $RPWS_retornoNaoAutorizado = "-999";
    $RPWS_retornoNaoAutorizado .= "|" . $RPWS_IP_Origem . " n�o autorizado !";
    //
    // Se estiver configurado para nao terminar o php
    // quando o IP nao tiver acesso...
    if($RPWS_SemBloqueio) {
        //
        // Nao faz nada
        //
    // Senao...
    } else {
        // Retorna erro
        echo($RPWS_retornoNaoAutorizado);
        // Termina o php
        exit();
    }
}
