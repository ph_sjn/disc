<?php
/******
 * 
 * libDiscXadmin.php
 * 
 ******/

include_once "incConfig.php";

// BD: discmaster..tbdw_cliente
//
// ::consultarCliente(<id>, <codigo>))
// ::listarClientes()
class clsDSXcliente {
    //
    Public $Id = null;      // id_cliente
    Public $Codigo = null;  // cd_cliente
    Public $Nome = null;    // ds_cliente
}

// BD: discmaster..tbdw_projeto
//
// ::listarProjetos(<id cliente>, <codigo cliente>)
class clsDSXprojeto {
    //
    Public $Id = null;          // id_projeto
    Public $Codigo = null;      // cd_projeto
    Public $Nome = null;        // nm_projeto
    Public $DataInicio = null;  // dt_inicio
    Public $DataFim = null;     // dt_fim
    //
    Public $Cliente = null;     // clsDSXcliente (id_cliente)
}

// BD: discmaster..tbdw_tipo_pagina
//
// ::consultarTipoDePagina(<codigo>)
// ::listarTiposDePagina()
class clsDSXtipoPagina {
    //
    Public $CodigoTipoPagina = null;    // cd_tipo_pagina
    Public $NomeTipoPagina = null;      // nm_tipo_pagina
}

// BD: discmaster..tbdw_perfil_funcao
//
// ::incluirPerfilDeFuncao(<obj:clsDSXperfilDeFuncao>)
// ::excluirPerfilDeFuncao(<id>)
// ::alterarPerfilDeFuncao(<obj:clsDSXperfilDeFuncao>)
// ::consultarPerfilDeFuncao(<id>)
// ::listarPerfisDeFuncao(<array>)
class clsDSXperfilDeFuncao {
    //
    Public $IdPerfilFuncao = null;      // id_perfil_funcao
    Public $Cliente = null;             // clsDSXcliente (id_cliente)
    Public $Nome = null;                // nm_perfil_funcao
    Public $Descricao = null;           // ds_perfil_funcao
    Public $DISC_D = null;              // pc_disc_d
    Public $DISC_D_Min = null;          // pc_disc_d_min
    Public $DISC_D_Max = null;          // pc_disc_d_max
    Public $DISC_I = null;              // pc_disc_i
    Public $DISC_I_Min = null;          // pc_disc_i_min
    Public $DISC_I_Max = null;          // pc_disc_i_max
    Public $DISC_S = null;              // pc_disc_s
    Public $DISC_S_Min = null;          // pc_disc_s_min
    Public $DISC_S_Max = null;          // pc_disc_s_max
    Public $DISC_C = null;              // pc_disc_c
    Public $DISC_C_Min = null;          // pc_disc_c_min
    Public $DISC_C_Max = null;          // pc_disc_c_max
}

// BD: discmaster..tbdw_pagina
//
// ::incluirPagina(<obj:clsDSXpagina>)
// ::excluirPagina(<id>)
// ::alterarPagina(<obj:clsDSXpagina>)
// ::consultarPagina(<id>,<codigo>)
// ::listarPaginas(<array>)
class clsDSXpagina {
    //
    Public $IdPagina = null;        // id_pagina
    Public $CodigoPagina = null;    // cd_pagina
    Public $TipoPagina = null;      // clsDSXtipoPagina (cd_tipo_pagina)
    Public $Pagina = null;          // ds_pagina
    Public $Include = null;         // dv_include
    Public $Descricao = null;       // ds_descricao
}

class clsDSXmasterAdmin {
    //
    
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_complemento = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function consultarCliente($PARM_id, $PARM_codigo) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "" || $PARM_codigo != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_cliente, cd_cliente, ds_cliente";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_cliente";
                    $sSQL .= " WHERE";
                    //
                    if($PARM_id != "") {
                        $sSQL .= " id_cliente = " . mysqli_escape_string($objLink, $PARM_id);
                    } else {
                        $sSQL .= " cd_cliente = '" . mysqli_escape_string($objLink, $PARM_codigo) . "'";
                    }
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXcliente();
                            //
                            $RET_obj->Id = $rsDados["id_cliente"];
                            $RET_obj->Codigo = $rsDados["cd_cliente"];
                            $RET_obj->Nome = $rsDados["ds_cliente"];
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
    
    public function consultarTipoDePagina($PARM_codigo_tipo) {
        //
        $RET_obj = null;
        //
        // Se veio o codigo do tipo...
        if($PARM_codigo_tipo != "" && strval($PARM_codigo_tipo) >= 0) {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " cd_tipo_pagina, nm_tipo_pagina";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_tipo_pagina";
                    $sSQL .= " WHERE";
                    $sSQL .= " cd_tipo_pagina = " . mysqli_escape_string($objLink, $PARM_codigo_tipo);
                    $sSQL .= ";";
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXtipoPagina();
                            //
                            $RET_obj->CodigoTipoPagina = $rsDados["cd_tipo_pagina"];
                            $RET_obj->NomeTipoPagina = $rsDados["nm_tipo_pagina"];
                            //
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
    
    public function listarProjetos($PARM_id_cliente, $PARM_cd_cliente) {
        //
        $RET_array = array();
        //
        if( ($PARM_id_cliente != "" && !is_null($PARM_id_cliente) )
                || 
            ($PARM_cd_cliente != "" && !is_null($PARM_cd_cliente) )
            ) {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " pj.id_projeto AS id_projeto, pj.cd_projeto AS cd_projeto";
                    $sSQL .= ",pj.nm_projeto AS nm_projeto, pj.dt_inicio AS dt_inicio";
                    $sSQL .= ",pj.dt_fim AS dt_fim, pj.id_cliente AS id_cliente";
                    $sSQL .= ",cli.cd_cliente AS cd_cliente, cli.ds_cliente AS ds_cliente";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_projeto AS pj";
                    $sSQL .= " LEFT JOIN";
                    $sSQL .= " tbdw_cliente AS cli";
                    $sSQL .= " ON cli.id_cliente = pj.id_cliente";
                    $sSQL .= " WHERE ";
                    //
                    if($PARM_cd_cliente != "") {
                        //
                        $sSQL .= "cli.cd_cliente = '" . mysqli_escape_string($objLink, $PARM_cd_cliente) . "'";
                    } else {
                        if($PARM_id_cliente != "") {
                            //
                            $sSQL .= "pj.id_cliente = " . mysqli_escape_string($objLink, $PARM_id_cliente);
                        }
                    }
                    //
                    $sSQL .= " ORDER BY ds_cliente;";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        while ($rsDados) {
                            //
                            $objLinha = new clsDSXprojeto();
                            //
                            $objLinha->Id = $rsDados["id_projeto"];
                            $objLinha->Codigo = $rsDados["cd_projeto"];
                            $objLinha->Nome = $rsDados["nm_projeto"];
                            $objLinha->DataInicio = $rsDados["dt_inicio"];
                            $objLinha->DataFim = $rsDados["dt_fim"];
                            //
                            $TMP_obj = new clsDSXcliente();
                            //
                            $TMP_obj->Id = $rsDados["id_cliente"];
                            $TMP_obj->Codigo = $rsDados["cd_cliente"];
                            $TMP_obj->Nome = $rsDados["ds_cliente"];
                            //
                            $objLinha->Cliente = $TMP_obj;
                            //
                            $RET_array[$objLinha->Id] = $objLinha;
                            //
                            $rsDados = mysqli_fetch_assoc($objResult);
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_array);
    }
    
    public function listarClientes() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " id_cliente, cd_cliente, ds_cliente";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_cliente";
                $sSQL .= " ORDER BY ds_cliente;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXcliente();
                        //
                        $objLinha->Id = $rsDados["id_cliente"];
                        $objLinha->Codigo = $rsDados["cd_cliente"];
                        $objLinha->Nome = $rsDados["ds_cliente"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function listarTiposDePagina() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " cd_tipo_pagina, nm_tipo_pagina";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_tipo_pagina";
                $sSQL .= " ORDER BY nm_tipo_pagina;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXtipoPagina();
                        //
                        $objLinha->CodigoTipoPagina = $rsDados["cd_tipo_pagina"];
                        $objLinha->NomeTipoPagina = $rsDados["nm_tipo_pagina"];
                        //
                        $RET_array[$objLinha->CodigoTipoPagina] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarPagina($parm_id_pagina, $parm_cd_pagina) {
        //
        $RET_obj = null;
        //
        if( ($parm_id_pagina != "" && !is_null($parm_id_pagina) )
                || 
            ($parm_cd_pagina != "" && !is_null($parm_cd_pagina) )
            ) {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " pag.id_pagina, pag.cd_pagina";
                    $sSQL .= ",pag.cd_tipo_pagina, tip.nm_tipo_pagina";
                    $sSQL .= ",pag.ds_pagina, pag.dv_include, pag.ds_descricao";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_pagina AS pag";
                    $sSQL .= " LEFT JOIN";
                    $sSQL .= " tbdw_tipo_pagina AS tip";
                    $sSQL .= " ON tip.cd_tipo_pagina = pag.cd_tipo_pagina";
                    $sSQL .= " WHERE ";
                    //
                    if($parm_cd_pagina != "") {
                        //
                        $sSQL .= "pag.cd_pagina = '" . mysqli_escape_string($objLink, $parm_cd_pagina) . "'";
                    } else {
                        if($parm_id_pagina != "") {
                            //
                            $sSQL .= "pag.id_pagina = " . mysqli_escape_string($objLink, $parm_id_pagina);
                        }
                    }
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXpagina();
                            //
                            $RET_obj->IdPagina = $rsDados["id_pagina"];
                            $RET_obj->CodigoPagina = $rsDados["cd_pagina"];
                            $RET_obj->Pagina = $rsDados["ds_pagina"];
                            $RET_obj->Descricao = $rsDados["ds_descricao"];
                            $RET_obj->Include = $rsDados["dv_include"];
                            //
                            $TMP_obj = new clsDSXtipoPagina();
                            $TMP_obj->CodigoTipoPagina = $rsDados["cd_tipo_pagina"];
                            $TMP_obj->NomeTipoPagina = $rsDados["nm_tipo_pagina"];
                            //
                            $RET_obj->TipoPagina = $TMP_obj;
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }

    public function incluirPerfilDeFuncao($PARM_obj) {
        //
        $RET_id = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_perfil_funcao ";
                $sSQL .= " (";
                $sSQL .= " id_cliente";
                $sSQL .= ", nm_perfil_funcao";
                $sSQL .= ", ds_perfil_funcao";
                $sSQL .= ", pc_disc_d";
                $sSQL .= ", pc_disc_d_min";
                $sSQL .= ", pc_disc_d_max";
                $sSQL .= ", pc_disc_i";
                $sSQL .= ", pc_disc_i_min";
                $sSQL .= ", pc_disc_i_max";
                $sSQL .= ", pc_disc_s";
                $sSQL .= ", pc_disc_s_min";
                $sSQL .= ", pc_disc_s_max";
                $sSQL .= ", pc_disc_c";
                $sSQL .= ", pc_disc_c_min";
                $sSQL .= ", pc_disc_c_max";
                $sSQL .= " )";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->Cliente->Id);
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_obj->Nome) . "'";
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_obj->Descricao) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_D);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_D_Min);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_D_Max);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_I);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_I_Min);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_I_Max);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_S);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_S_Min);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_S_Max);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_C);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_C_Min);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->DISC_C_Max);
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id = mysqli_insert_id($objLink);
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_id);
    }
    
    public function atualizarPerfilDeFuncao($PARM_obj) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " UPDATE tbdw_perfil_funcao ";
                $sSQL .= " SET";
                $sSQL .= " nm_perfil_funcao = ";
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->Nome) . "'";
                $sSQL .= ", id_cliente = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->Cliente->Id);
                $sSQL .= ", ds_perfil_funcao = ";
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->Descricao) . "'";
                $sSQL .= ", pc_disc_d = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_D);
                $sSQL .= ", pc_disc_d_min = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_D_Min);
                $sSQL .= ", pc_disc_d_max = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_D_Max);
                $sSQL .= ", pc_disc_i = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_I);
                $sSQL .= ", pc_disc_i_min = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_I_Min);
                $sSQL .= ", pc_disc_i_max = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_I_Max);
                $sSQL .= ", pc_disc_s = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_S);
                $sSQL .= ", pc_disc_s_min = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_S_Min);
                $sSQL .= ", pc_disc_s_max = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_S_Max);
                $sSQL .= ", pc_disc_c = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_C);
                $sSQL .= ", pc_disc_c_min = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_C_Min);
                $sSQL .= ", pc_disc_c_max = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->DISC_C_Max);
                //
                //
                $sSQL .= " WHERE id_perfil_funcao = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->IdPerfilFuncao);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
    }
    
    public function excluirPerfilDeFuncao($PARM_id_perfil_funcao, $PARM_id_cliente) {
        //
        //
        $RET_id = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                $PARM_obj = new clsDSXperfilDeFuncao();
                //
                // Monta o INSERT
                $sSQL  = " DELETE FROM tbdw_perfil_funcao ";
                $sSQL .= " WHERE id_perfil_funcao = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_id_perfil_funcao);
                $sSQL .= " AND id_cliente = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_id_cliente);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function listarPerfisDeFuncao($parm_filtro) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " pf.id_perfil_funcao, pf.id_cliente";
                $sSQL .= ",pf.nm_perfil_funcao, pf.ds_perfil_funcao";
                $sSQL .= ",pf.pc_disc_d, pf.pc_disc_d_min, pf.pc_disc_d_max";
                $sSQL .= ",pf.pc_disc_i, pf.pc_disc_i_min, pf.pc_disc_i_max";
                $sSQL .= ",pf.pc_disc_s, pf.pc_disc_s_min, pf.pc_disc_s_max";
                $sSQL .= ",pf.pc_disc_c, pf.pc_disc_c_min, pf.pc_disc_c_max";
                $sSQL .= ",cli.cd_cliente, cli.ds_cliente";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_perfil_funcao AS pf";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_cliente AS cli";
                $sSQL .= " ON cli.id_cliente = pf.id_cliente";
                //
                if($parm_filtro && !is_null($parm_filtro)) {
                    //
                    $sWHERE = "";
                    //
                    if($parm_filtro["id_cliente"] != "") {
                        //
                        if($sWHERE != "") {
                            $sWHERE .= " AND ";
                        }
                        //
                        $sWHERE .= "pf.id_cliente = " . mysqli_escape_string($objLink, $parm_filtro["id_cliente"]);
                    }
                    //
                    if($parm_filtro["cd_cliente"] != "") {
                        //
                        if($sWHERE != "") {
                            $sWHERE .= " AND ";
                        }
                        //
                        $sWHERE .= "cli.cd_cliente = '" . mysqli_escape_string($objLink, $parm_filtro["cd_cliente"]) . "'";
                    }
                    //
                    if($parm_filtro["nm_perfil_funcao"] != "") {
                        //
                        if($sWHERE != "") {
                            $sWHERE .= " AND ";
                        }
                        //
                        $sWHERE .= "pf.nm_perfil_funcao LIKE '%" . mysqli_escape_string($objLink, $parm_filtro["nm_perfil_funcao"]) . "%'";
                    }
                    //
                    if($sWHERE != "") {
                        $sSQL .= " WHERE ". $sWHERE;
                    }
                }
                //
                $sSQL .= " ORDER BY pf.nm_perfil_funcao;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXperfilDeFuncao();
                        //
                        $objLinha->IdPerfilFuncao = $rsDados["id_perfil_funcao"];
                        $objLinha->Nome = $rsDados["nm_perfil_funcao"];
                        $objLinha->Descricao = $rsDados["ds_perfil_funcao"];
                        $objLinha->DISC_D = $rsDados["pc_disc_d"];
                        $objLinha->DISC_D_Min = $rsDados["pc_disc_d_min"];
                        $objLinha->DISC_D_Max = $rsDados["pc_disc_d_max"];
                        $objLinha->DISC_I = $rsDados["pc_disc_i"];
                        $objLinha->DISC_I_Min = $rsDados["pc_disc_i_min"];
                        $objLinha->DISC_I_Max = $rsDados["pc_disc_i_max"];
                        $objLinha->DISC_S = $rsDados["pc_disc_s"];
                        $objLinha->DISC_S_Min = $rsDados["pc_disc_s_min"];
                        $objLinha->DISC_S_Max = $rsDados["pc_disc_s_max"];
                        $objLinha->DISC_C = $rsDados["pc_disc_c"];
                        $objLinha->DISC_C_Min = $rsDados["pc_disc_c_min"];
                        $objLinha->DISC_C_Max = $rsDados["pc_disc_c_max"];
                        //
                        $TMP_obj = new clsDSXcliente();
                        //
                        $TMP_obj->Id = $rsDados["id_cliente"];
                        $TMP_obj->Codigo = $rsDados["cd_cliente"];
                        $TMP_obj->Nome = $rsDados["ds_cliente"];
                        //
                        $objLinha->Cliente = $TMP_obj;
                        //
                        $RET_array[$objLinha->IdPerfilFuncao] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }
    
    public function consultarPerfilDeFuncao($PARM_id) {
        //
        $RET_obj = null;
        //
        if ($PARM_id != "" && !is_null($PARM_id) ) {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " pf.id_perfil_funcao, pf.id_cliente";
                    $sSQL .= ",pf.nm_perfil_funcao, pf.ds_perfil_funcao";
                    $sSQL .= ",pf.pc_disc_d, pf.pc_disc_d_min, pf.pc_disc_d_max";
                    $sSQL .= ",pf.pc_disc_i, pf.pc_disc_i_min, pf.pc_disc_i_max";
                    $sSQL .= ",pf.pc_disc_s, pf.pc_disc_s_min, pf.pc_disc_s_max";
                    $sSQL .= ",pf.pc_disc_c, pf.pc_disc_c_min, pf.pc_disc_c_max";
                    $sSQL .= ",cli.cd_cliente, cli.ds_cliente";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_perfil_funcao AS pf";
                    $sSQL .= " LEFT JOIN";
                    $sSQL .= " tbdw_cliente AS cli";
                    $sSQL .= " ON cli.id_cliente = pf.id_cliente";
                    $sSQL .= " WHERE ";
                    $sSQL .= "pf.id_perfil_funcao = '" . mysqli_escape_string($objLink, $PARM_id) . "'";
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $objLinha = new clsDSXperfilDeFuncao();
                            //
                            $RET_obj->IdPerfilFuncao = $rsDados["id_perfil_funcao"];
                            $RET_obj->Nome = $rsDados["nm_perfil_funcao"];
                            $RET_obj->Descricao = $rsDados["ds_perfil_funcao"];
                            $RET_obj->DISC_D = $rsDados["pc_disc_d"];
                            $RET_obj->DISC_D_Min = $rsDados["pc_disc_d_min"];
                            $RET_obj->DISC_D_Max = $rsDados["pc_disc_d_max"];
                            $RET_obj->DISC_I = $rsDados["pc_disc_i"];
                            $RET_obj->DISC_I_Min = $rsDados["pc_disc_i_min"];
                            $RET_obj->DISC_I_Max = $rsDados["pc_disc_i_max"];
                            $RET_obj->DISC_S = $rsDados["pc_disc_s"];
                            $RET_obj->DISC_S_Min = $rsDados["pc_disc_s_min"];
                            $RET_obj->DISC_S_Max = $rsDados["pc_disc_s_max"];
                            $RET_obj->DISC_C = $rsDados["pc_disc_c"];
                            $RET_obj->DISC_C_Min = $rsDados["pc_disc_c_min"];
                            $RET_obj->DISC_C_Max = $rsDados["pc_disc_c_max"];
                            //
                            $TMP_obj = new clsDSXcliente();
                            //
                            $TMP_obj->Id = $rsDados["id_cliente"];
                            $TMP_obj->Codigo = $rsDados["cd_cliente"];
                            $TMP_obj->Nome = $rsDados["ds_cliente"];
                            //
                            $RET_obj->Cliente = $TMP_obj;
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
    
    public function incluirPagina($PARM_obj) {
        //
        $RET_id = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_pagina ";
                $sSQL .= " (";
                $sSQL .= " cd_pagina";
                $sSQL .= ", cd_tipo_pagina";
                $sSQL .= ", ds_pagina";
                $sSQL .= ", dv_include";
                $sSQL .= ", ds_descricao";
                $sSQL .= " )";
                $sSQL .= " VALUES (";
                //
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->CodigoPagina) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->TipoPagina->CodigoTipoPagina);
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_obj->Pagina) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_obj->Include);
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_obj->Descricao) . "'";
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                // Senao...
                } else {
                    // Obtem o novo ID
                    $RET_id = mysqli_insert_id($objLink);
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_id);
    }
    
    public function atualizarPagina($PARM_obj) {
        //
        $RET_id = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " UPDATE tbdw_pagina ";
                $sSQL .= " SET";
                $sSQL .= " cd_pagina = ";
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->CodigoPagina) . "'";
                $sSQL .= ", cd_tipo_pagina = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->TipoPagina->CodigoTipoPagina);
                $sSQL .= ", ds_pagina = ";
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->Pagina) . "'";
                $sSQL .= ", dv_include = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->Include);
                $sSQL .= ", ds_descricao = ";
                $sSQL .= "'" . mysqli_escape_string($objLink, $PARM_obj->Descricao) . "'";
                //
                //
                $sSQL .= " WHERE id_pagina = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_obj->IdPagina);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na atualiza��o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
    }
    
    public function excluirPagina($PARM_id_pagina) {
        //
        //
        $RET_id = null;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if(!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " DELETE FROM tbdw_pagina ";
                $sSQL .= " WHERE id_pagina = ";
                $sSQL .= mysqli_escape_string($objLink, $PARM_id_pagina);
                //
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o dos dados no banco de dados!";
                    //$this->ERRO_complemento = $sSQL;
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
    }
    
    public function listarPaginas($parm_filtro) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " pag.id_pagina, pag.cd_pagina";
                $sSQL .= ",pag.cd_tipo_pagina, tip.nm_tipo_pagina";
                $sSQL .= ",pag.ds_pagina, pag.dv_include, pag.ds_descricao";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_pagina AS pag";
                $sSQL .= " LEFT JOIN";
                $sSQL .= " tbdw_tipo_pagina AS tip";
                $sSQL .= " ON tip.cd_tipo_pagina = pag.cd_tipo_pagina";
                //
                if($parm_filtro && !is_null($parm_filtro)) {
                    //
                    $sWHERE = "";
                    //
                    if($parm_filtro["cd_tipo_pagina"] != "") {
                        //
                        if($sWHERE != "") {
                            $sWHERE .= " AND ";
                        }
                        //
                        $sWHERE .= "pag.cd_tipo_pagina = " . mysqli_escape_string($objLink, $parm_filtro["cd_tipo_pagina"]);
                    }
                    //
                    if($sWHERE != "") {
                        $sSQL .= " WHERE ". $sWHERE;
                    }
                }
                //
                $sSQL .= " ORDER BY pag.cd_pagina;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXpagina();
                        //
                        $objLinha->IdPagina = $rsDados["id_pagina"];
                        $objLinha->CodigoPagina = $rsDados["cd_pagina"];
                        $objLinha->Pagina = $rsDados["ds_pagina"];
                        $objLinha->Include = $rsDados["dv_include"];
                        $objLinha->Descricao = $rsDados["ds_descricao"];
                        //
                        $objLinha->TipoPagina = $this->consultarTipoDePagina($rsDados["cd_tipo_pagina"]);
                        //
                        $RET_array[$objLinha->IdPagina] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);        
    }
}