<?php
/******
 * 
 * libDiscXbusca.php
 * 
 ******/

include_once "/home/users/__discx/inc/incConfig.php";

// ::listarCamposEntrevistado()
class clsDSXcampoBusca{
    //
    public $Id = null;      	// Identifica o campo
    public $Campo = null;   	// Nome do campo para exibir
    public $Tipo = null;    	// Tipo do campo: texto, data e numero
	public $flBasico = null;   	// Flag que indica se o campo e do cadastro basico (=1), ou eh especifico (<>1)
	public $Tamanho = null; 	// Tamanho do campo
	public $Validacao = null; 	// Expressao para validar o campo
	public $Descricao = null; 	// Descricao do campo para exibir
	public $Condicao = null;  	// Condicao para localizar o campo: "=", ">", "<", ">=", "<=", "Cont�m", "N�o cont�m"
	public $Valor = null;    	// Valor ou conteudo do campo para buscar
}

// ::buscarEntrevistados
class clsDSXentrevistado{
	//
	public $Total = null;		// Total de entrevistados encontrados
	//
	public $IdCliente = null; 	// id_cliente
	public $IdAvaliacao = null; // id_avaliacao
	public $Titulo = null; 		// ds_titulo
	public $IdToken = null; 	// id_token
	public $NomeEntrevistado = null; // ds_nome
	public $ResultadoFatorDisc = null; // ds_result_fator_disc
	public $DtInicio = null; 	// dt_inicio
	public $DtTermino = null; 	// dt_termino
	public $Email = null; 		// ds_email
	public $Sexo = null; 		// dv_sexo
	public $DtNascimento = null; // dt_nascimento
	public $Cep = null; 		// nr_cep
	public $Telefone = null; 	// nr_telefone
	public $Cpf = null; 		// nr_cpf
	public $FlAutorizaComunic = null; // fl_autoriza_comunic
	public $FlIntencaoInvestir = null; // nr_meses (12 ou 24) = 1 se nao = 0
	public $QdoPretendeInvestirMeses = null; // nr_meses
	public $SetoresInteresse = null; // CONCAT(nm_setor)
	public $MarcasInteresse = null; // ds_marcas_interesse
	public $CdFaixaInvest = null; // dv_faixa_valor_invest
	public $FaixaInvest = null; // Lista
}

class clsDSXbusca {
    //
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_complemento = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function listarCamposEntrevistado($PARM_selPesquisas) {
        //
        $RET_array = array();
        //
		$this->ERRO = 0;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "1";
		$objLinha->Campo = "Fator DISC";
		$objLinha->Tipo = "texto";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "4";
		$objLinha->Validacao = "[discDISC]";
		$objLinha->Descricao = "Informe o resultado DISC.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
        //
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "2";
		$objLinha->Campo = "Nome";
		$objLinha->Tipo = "texto";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "60";
		$objLinha->Validacao = "w+";
		$objLinha->Descricao = "Informe o nome do entrevistado.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "3";
		$objLinha->Campo = "Email";
		$objLinha->Tipo = "texto";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "50";
		$objLinha->Validacao = "w+@w+";
		$objLinha->Descricao = "Informe o email.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "4";
		$objLinha->Campo = "Sexo";
		$objLinha->Tipo = "texto";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "1";
		$objLinha->Validacao = "([mM]|[fF])";
		$objLinha->Descricao = "Informe 'M' ou 'F'.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "5";
		$objLinha->Campo = "Data de Nascimento";
		$objLinha->Tipo = "data";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "10";
		$objLinha->Validacao = "[0-9]{2}/[0-9]{2}/[0-9]{4}";
		$objLinha->Descricao = "Informe dd/mm/aaaa.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "6";
		$objLinha->Campo = "CEP";
		$objLinha->Tipo = "numero";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "8";
		$objLinha->Validacao = "[0-9]+";
		$objLinha->Descricao = "Informe apenas n�meros.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		$objLinha = new clsDSXcampoBusca();
		$objLinha->Id = "7";
		$objLinha->Campo = "Telefone";
		$objLinha->Tipo = "numero";
		$objLinha->flBasico = "1";
		$objLinha->Tamanho = "11";
		$objLinha->Validacao = "[0-9]+";
		$objLinha->Descricao = "Informe apenas n�meros. Considere os dois primeiros d�gitos como c�digo de �rea.";
		$objLinha->Condicao = "";
		$objLinha->Valor = "";
		$RET_array[$objLinha->Id] = $objLinha;
		//
		// Verificar se tem pesquisa que utiliza cadastro especifico
		if ( is_array($PARM_selPesquisas) ){
			if ( count($PARM_selPesquisas) > 0 ) {
				//
				$flCadEspecifico = false;
				//
				if (LIB_debug) {
					$this->DBG_db_type = LIB_db_type;
					$this->DBG_db_host = LIB_db_host;
					$this->DBG_db_name = LIB_db_name;
					$this->DBG_db_user = LIB_db_user;
				}
				//
				$objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
				//
				if ($objLink) {
					//
					if (mysqli_select_db($objLink, LIB_db_name)) {
						// Monta a consulta
						$sSQL = "SELECT 1 AS flCadEspecifico FROM tbdw_avaliacao AS av";
						$sSQL .= " WHERE EXISTS(";
						$sSQL .= "SELECT 1 FROM tbdw_aval_token AS tk";
						$sSQL .= " INNER JOIN tbdw_braabf17_token AS tk2";
						$sSQL .= " ON (tk2.id_token = tk.id_token)";
						$sSQL .= " WHERE tk.id_avaliacao = av.id_avaliacao AND tk.id_cliente = av.id_cliente";
						$sSQL .= ") AND (";
						//
						$flgPrimeiro = true;
						foreach($PARM_selPesquisas as $record) {
							if ( $flgPrimeiro ){
								$flgPrimeiro = false;
							} else {
								$sSQL .= " OR ";
							}
							$sSQL .= "av.id_avaliacao=" . $record["IdAvaliacao"];
						}
						$sSQL .= ")";
						//
						//
						// Executa a consulta ao banco de dados
						$objResult = mysqli_query($objLink, $sSQL);
						//
						if ($objResult) {
							// Obtem o resultado da consulta
							$rsDados = mysqli_fetch_assoc($objResult);
							//
							if ($rsDados){
								if ( $rsDados["flCadEspecifico"] == 1 ){
									//
									$flCadEspecifico = true;
									//
								}
							}
						}
						//
						// Libera a memoria alocada
						mysqli_free_result($objResult);
					}
					// Fecha a conexao com o banco de dados
					mysqli_close($objLink);
				}
				//
				if ( $flCadEspecifico ) {
					$objLinha = new clsDSXcampoBusca();
					$objLinha->Id = "8";
					$objLinha->Campo = "CPF";
					$objLinha->Tipo = "numero";
					$objLinha->flBasico = "1";
					$objLinha->Tamanho = "11";
					$objLinha->Validacao = "[0-9]+";
					$objLinha->Descricao = "Informe apenas n�meros.";
					$objLinha->Condicao = "";
					$objLinha->Valor = "";
					$RET_array[$objLinha->Id] = $objLinha;
				}
			}
		}
		//
        return($RET_array);
    }
	//
	public function buscarEntrevistados($PARM_idCliente, $PARM_selPesquisas, $PARM_selCamposBusca, $PARM_itensPorPag, $PARM_nrPag) {
        //
        $RET_array = array();
        //
		$this->ERRO = 0;
		//
		// Validacao dos parametros
		$dadosValidos = true;
		if ( !preg_match("/[0-9]+/", $PARM_idCliente) && $dadosValidos ){
			$dadosValidos = false;
		}
		if ( !is_array($PARM_selPesquisas) && $dadosValidos ){
			$dadosValidos = false;
		} else {
			if ( count($PARM_selPesquisas) <= 0 && $dadosValidos ){
				$dadosValidos = false;
			}
		}
		//
		if ( $dadosValidos ){
			//
			// Tabelas
			//----------------------------------------------------------
			$sTabelas  = "";
			$sTabelas .= "tbdw_avaliacao AS av";
			$sTabelas .= " LEFT JOIN tbdw_aval_token AS tk";
			$sTabelas .= " ON (av.id_avaliacao = tk.id_avaliacao AND av.id_cliente = tk.id_cliente)";
			$sTabelas .= " LEFT JOIN tbdw_aval_form_token AS avtk";
			$sTabelas .= " ON (tk.id_avaliacao = avtk.id_avaliacao AND tk.id_token = avtk.id_token)";
			$sTabelas .= " LEFT JOIN tbdw_braabf17_token AS tk2";
			$sTabelas .= " ON (tk.id_token = tk2.id_token)";
			$sTabelas .= " LEFT JOIN tbdw_braabf17_token_setor AS tkset";
			$sTabelas .= " ON (tk.id_token = tkset.id_token)";
			$sTabelas .= " LEFT JOIN tbdw_braabf17_setor AS setor";
			$sTabelas .= " ON (tkset.id_setor = setor.id_setor)";
			//
			// Filtros
			//----------------------------------------------------------
			$sFiltro  = "";
			//
			// Cliente
			$sFiltro .= sprintf("av.id_cliente = %d", $PARM_idCliente);
			//
			// Pesquisa(s)
			if ( is_array($PARM_selPesquisas) ) {
				$sFiltro .= " AND (";
				$flgPrimeiro = true;
				foreach($PARM_selPesquisas as $record) {
					if ( $flgPrimeiro ){
						$flgPrimeiro = false;
					} else {
						$sFiltro .= " OR ";
					}
					$sFiltro .= sprintf("av.id_avaliacao = %d", $record["IdAvaliacao"]);
				}
				$sFiltro .= ")";
			}
			//
			$colCamposArray = array();
			foreach($PARM_selCamposBusca as $obj) {
				if ( is_object($obj) ) {
					$obj = get_object_vars($obj);
				}
				$colCamposArray[] = $obj;
			}
			$PARM_selCamposBusca = $colCamposArray;
			//
			if ( is_array($PARM_selCamposBusca) && count($PARM_selCamposBusca) > 0 ) {
				$sFiltro .= " AND (";
				//
				$flgPrimeiro = true;
				$flgCondIgual = true;
				$flgLike = false;
				$idCampoAnterior = 0;
				foreach($PARM_selCamposBusca as $record) {
					if ( $flgCondIgual && $record["Condicao"] != "=" ){
						$flgCondIgual = false;
					}
					if ( $flgPrimeiro ){
						$idCampoAnterior = $record["Id"];
						$flgPrimeiro = false;
					} else {
						//
						if ( $idCampoAnterior != $record["Id"] ){
							$idCampoAnterior = $record["Id"];
							$sFiltro .= ") AND (";
						} else {
							if ( $flgCondIgual ){
								$sFiltro .= " OR ";
							} else {
								$sFiltro .= " AND ";
							}
						}
					}
					//
					// Nomes dos campos na base de dados
					switch ( $record["Id"] ) {
						case "1": // Fator DISC
							$sFiltro .= "avtk.ds_result_fator_disc";
							break;
							
						case "2": // Nome
							$sFiltro .= "tk.ds_nome";
							break;

						case "3": // Email
							$sFiltro .= "tk.ds_email";
							break;

						case "4": // Sexo
							$sFiltro .= "tk.dv_sexo";
							break;

						case "5": // Data de Nascimento
							$sFiltro .= "tk.dt_nascimento";
							break;

						case "6": // CEP
							$sFiltro .= "tk.nr_cep";
							break;

						case "7": // Telefone
							$sFiltro .= "tk.nr_telefone";
							break;

						case "8": // CPF
							$sFiltro .= "tk2.nr_cpf";
							break;

						default:
							$sFiltro = "";
							break;
					}
					//
					if ( $sFiltro == "" ){
						break;
					}
					//
					// pattern para cont�m ou Cont�m
					if ( preg_match("/ont/", $record["Condicao"]) ){
						$flgLike = true;
						if ( preg_match("/^N/", $record["Condicao"]) ){
							$sFiltro .= "  NOT LIKE ";
						} else {
							$sFiltro .= " LIKE ";
						}
					} else {
						$flgLike = false;
						$sFiltro .= $record["Condicao"]; 
					}
					if ( $record["Tipo"] != "numero" || $flgLike ){
						$sFiltro .= "'";
						if ( $flgLike ){
							$sFiltro .= "%";
						}
					}
					//
					$Valor = $record["Valor"];
					if ( $record["Tipo"] == "data" ){
						//dd/mm/aaaa
						$dtSplit = explode("/", $Valor);
						$Valor = $dtSplit[2] . "-" . $dtSplit[1] . "-" . $dtSplit[0];
						if ( preg_match("/</", $record["Condicao"]) ){
							$Valor .= " 23:59:59";
						} elseif ( preg_match("/>=/", $record["Condicao"]) ){
							$Valor .= " 00:00:00";
						}
					}
					$sFiltro .= $Valor;
					//
					if ( $record["Tipo"] != "numero" || $flgLike ){
						if ( $flgLike ){
							$sFiltro .= "%";
						}
						$sFiltro .= "'";
					}
				}
				$sFiltro .= ")";
			}
			//
			//
			// Colunas
			//----------------------------------------------------------
			$sColunaTotal = "COUNT(DISTINCT tk.id_token) AS 'total_paginas'";
			//
			$sColunasEntrevistado  = "";
			$sColunasEntrevistado .= "av.id_cliente AS 'id_cliente'";
			$sColunasEntrevistado .= ", av.id_avaliacao AS 'id_avaliacao'";
			$sColunasEntrevistado .= ", av.ds_titulo AS 'ds_titulo'";
			$sColunasEntrevistado .= ", tk.id_token AS 'id_token'";
			$sColunasEntrevistado .= ", tk.ds_nome AS 'ds_nome'";
			$sColunasEntrevistado .= ", avtk.ds_result_fator_disc AS 'ds_result_fator_disc'";
			$sColunasEntrevistado .= ", DATE_FORMAT(avtk.dt_inicio, '%d/%m/%Y %H:%i:%s') AS 'dt_inicio'";
			$sColunasEntrevistado .= ", DATE_FORMAT(avtk.dt_termino, '%d/%m/%Y %H:%i:%s') AS 'dt_termino'";
			$sColunasEntrevistado .= ", tk.ds_email AS 'ds_email'";
			$sColunasEntrevistado .= ", tk.dv_sexo AS 'dv_sexo'";
			$sColunasEntrevistado .= ", DATE_FORMAT(tk.dt_nascimento, '%d/%m/%Y') AS 'dt_nascimento'";
			$sColunasEntrevistado .= ", tk.nr_cep AS 'nr_cep'";
			$sColunasEntrevistado .= ", IFNULL(tk.nr_telefone,'') AS 'nr_telefone'";
			$sColunasEntrevistado .= ", tk2.nr_cpf AS 'nr_cpf'";
			$sColunasEntrevistado .= ", tk2.fl_autoriza_comunic AS 'fl_autoriza_comunic'";
			$sColunasEntrevistado .= ", CASE ";
			$sColunasEntrevistado .= " WHEN tk2.nr_meses = 12 OR tk2.nr_meses = 24 THEN 1";
			$sColunasEntrevistado .= " ELSE 0 END  AS 'flag_intencao_investir'";
			$sColunasEntrevistado .= ", CASE ";
			$sColunasEntrevistado .= " WHEN tk2.nr_meses = 12 OR tk2.nr_meses = 24 THEN tk2.nr_meses";
			$sColunasEntrevistado .= " ELSE 0 END AS 'qdo_pretende_investir_meses'";
			$sColunasEntrevistado .= ", IFNULL(GROUP_CONCAT(setor.nm_setor),'') AS 'setores_interesse'";
			$sColunasEntrevistado .= ", IFNULL(tk2.ds_marcas_interesse,'') AS 'marcas_interesse'";
			$sColunasEntrevistado .= ", IFNULL(tk2.dv_faixa_valor_invest,0) AS 'cd_faixa_invest'";
			$sColunasEntrevistado .= ", CASE ";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 1 THEN 'At� R$ 25.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 2 THEN 'R$ 25.001 at� R$ 50.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 3 THEN 'R$ 50.001 at� R$ 100.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 4 THEN 'R$ 100.001 at� R$ 200.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 5 THEN 'R$ 200.001 at� R$ 300.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 6 THEN 'R$ 300.001 at� R$ 400.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 7 THEN 'R$ 400.001 at� R$ 500.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 8 THEN 'R$ 500.001 at� 750.000'";
			$sColunasEntrevistado .= " WHEN tk2.dv_faixa_valor_invest = 9 THEN 'Acima de R$ 750.000'";
			$sColunasEntrevistado .= " ELSE '' END AS 'ds_faixa_invest'";
			//----------------------------------------------------------
			//
			// Busca Entrevistados
			//
			//----------------------------------------------------------
			if (LIB_debug) {
				$this->DBG_db_type = LIB_db_type;
				$this->DBG_db_host = LIB_db_host;
				$this->DBG_db_name = LIB_db_name;
				$this->DBG_db_user = LIB_db_user;
			}
			//
			$objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
			//
			if (!$objLink) {
				//
				$this->ERRO = 1;
				$this->ERRO_bd_cod_erro = mysqli_connect_errno();
				$this->ERRO_bd_msg_erro = mysqli_connect_error();
				$this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
				//
			} else {
				// Se obteve sucesso na selecao do banco de dados...
				if (mysqli_select_db($objLink, LIB_db_name)) {
					// Monta a consulta
					// Busca Total
					$sSQL = "SELECT " . $sColunaTotal . " FROM " . $sTabelas . " WHERE " . $sFiltro;
					//
					$nrTotal = 0;
					//
					// Executa a consulta ao banco de dados
					$objResult = mysqli_query($objLink, $sSQL);
					// Se falhou...
					if (!$objResult) {
						//
						$this->ERRO = 1;
						$this->ERRO_bd_cod_erro = mysqli_errno($objLink);
						$this->ERRO_bd_msg_erro = mysqli_error($objLink);
						$this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
						//
						// Senao...
					} else {
						// Obtem o resultado da consulta
						$rsDados = mysqli_fetch_assoc($objResult);
						//
						$nrTotal = $rsDados["total_paginas"];
						//
						if ( $nrTotal > 0 ){
							// Monta a consulta
							// Busca Entrevistados
							$sSQL  = "SELECT " . $sColunasEntrevistado . " FROM " . $sTabelas . " WHERE " . $sFiltro;
							$sSQL .= " GROUP BY tk.id_token";
							$sSQL .= " ORDER BY av.id_cliente, tk.ds_nome, av.ds_titulo";
							// Itens por pagina
							if ( preg_match("/[0-9]+/", $PARM_itensPorPag) ) {
								// Pagina
								if ( !preg_match("/[0-9]+/", $PARM_nrPag) ){
									$PARM_nrPag = 1;
								}
								if ( $PARM_nrPag == 0 ){
									$PARM_nrPag = 1;
								}
								// Pagina inicial
								$PaginaInicial = ($PARM_nrPag-1) * $PARM_itensPorPag;
								$sSQL .= " LIMIT " . $PaginaInicial . ", " . $PARM_itensPorPag;
							}
							//
							// Executa a consulta ao banco de dados
							$objResult = mysqli_query($objLink, $sSQL);
							// Se falhou...
							if (!$objResult) {
								//
								$this->ERRO = 1;
								$this->ERRO_bd_cod_erro = mysqli_errno($objLink);
								$this->ERRO_bd_msg_erro = mysqli_error($objLink);
								$this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
								// Libera a memoria alocada
								mysqli_free_result($objResult);
								//
								// Senao...
							} else {
								// Obtem o resultado da consulta
								$rsDados = mysqli_fetch_assoc($objResult);
								//
								while ($rsDados) {
									//
									$objLinha = new clsDSXentrevistado();
									//
									$objLinha->Total = $nrTotal;
									$objLinha->IdCliente = $rsDados["id_cliente"];
									$objLinha->IdAvaliacao = $rsDados["id_avaliacao"];
									$objLinha->Titulo = $rsDados["ds_titulo"];
									$objLinha->IdToken = $rsDados["id_token"];
									$objLinha->NomeEntrevistado = $rsDados["ds_nome"];
									$objLinha->ResultadoFatorDisc = $rsDados["ds_result_fator_disc"];
									$objLinha->DtInicio = $rsDados["dt_inicio"];
									$objLinha->DtTermino = $rsDados["dt_termino"];
									$objLinha->Email = $rsDados["ds_email"];
									$objLinha->Sexo = $rsDados["dv_sexo"];
									$objLinha->DtNascimento = $rsDados["dt_nascimento"];
									$objLinha->Cep = $rsDados["nr_cep"];
									$objLinha->Telefone = $rsDados["nr_telefone"];
									$objLinha->Cpf = $rsDados["nr_cpf"];
									$objLinha->FlAutorizaComunic = $rsDados["fl_autoriza_comunic"];
									$objLinha->FlIntencaoInvestir = $rsDados["flag_intencao_investir"];
									$objLinha->QdoPretendeInvestirMeses = $rsDados["qdo_pretende_investir_meses"];
									$objLinha->SetoresInteresse = $rsDados["setores_interesse"];
									$objLinha->MarcasInteresse = $rsDados["marcas_interesse"];
									$objLinha->CdFaixaInvest = $rsDados["cd_faixa_invest"];
									$objLinha->FaixaInvest = $rsDados["ds_faixa_invest"];
									//
									$RET_array[$rsDados["id_token"]] = $objLinha;
									//
									$rsDados = mysqli_fetch_assoc($objResult);
								}
								//
								// Libera a memoria alocada
								mysqli_free_result($objResult);
							}
						} else {
							// Total = 0
							// Libera a memoria alocada
							mysqli_free_result($objResult);
						}
					}
				}
				// Fecha a conexao com o banco de dados
				mysqli_close($objLink);
			}
		}
		//
        return($RET_array);
	}
}