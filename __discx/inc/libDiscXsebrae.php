<?php
/******
 * 
 * libDiscXsebrae.php
 * 
 ******/

include_once "/home/users/__discx/inc/incConfig.php";

// BD: discmaster..tbdw_braabf17_marca
//
// ::consultarMarca(<id>, <codigo>))
// ::listarMarcas()
class clsDSXsebraeMarca {
    //
    Public $Id = null;      // id_marca
    Public $Codigo = null;  // cd_marca
    Public $Nome = null;    // nm_marca
}

// BD: discmaster..tbdw_braabf17_setor
//
// ::consultarSetor(<id>, <codigo>))
// ::listarSetores()
class clsDSXsebraeSetor {
    //
    Public $Id = null;      // id_setor
    Public $Codigo = null;  // cd_setor
    Public $Nome = null;    // nm_setor
}

class clsDSXtokenSebrae {
    public $Id = null;
    // tbdw_braabf17_token.fl_intencao_investir
    public $PretendeInvestir = null;
    // tbdw_braabf17_token.nr_meses
    public $InvestirMeses = null;
    // tbdw_braabf17_token.nr_cpf
    public $CPF = null;
    // tbdw_braabf17_token.fl_autoriza_comunic
    public $AutorizaContato = null;
    // tbdw_braabf17_token_setor
    public $SetoresInteresse = null;
    // tbdw_braabf17_token_marca
    public $MarcasInteresseLista = null;
    // tbdw_braabf17_token.ds_marcas_interesse
    public $MarcasInteresse = null;
    // tbdw_braabf17_token.dv_faixa_valor_invest
    public $FaixaValorInvestimento = null;
}

class clsDSXsebrae {
    //
    
    public $ERRO = 0;
    public $ERRO_bd_cod_erro = 0;
    public $ERRO_bd_msg_erro = "";
    public $ERRO_mensagem = "SEM ERRO";
    public $ERRO_complemento = "";
    public $DBG_db_type = "";
    public $DBG_db_host = "";
    public $DBG_db_name = "";
    public $DBG_db_user = "";
    public $DBG_db_pass = "";
    
    public function consultarSetor($PARM_id, $PARM_codigo) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "" || $PARM_codigo != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_setor, cd_setor, nm_setor";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_setor";
                    $sSQL .= " WHERE";
                    //
                    if($PARM_id != "") {
                        $sSQL .= " id_setor = " . mysqli_escape_string($objLink, $PARM_id);
                    } else {
                        $sSQL .= " cd_setor = '" . mysqli_escape_string($objLink, $PARM_codigo) . "'";
                    }
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXsebraeSetor();
                            //
                            $RET_obj->Id = $rsDados["id_setor"];
                            $RET_obj->Codigo = $rsDados["cd_setor"];
                            $RET_obj->Nome = $rsDados["nm_setor"];
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
        
    public function listarSetores() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " id_setor, cd_setor, nm_setor";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_setor";
                $sSQL .= " ORDER BY nm_setor;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXsebraeSetor();
                        //
                        $objLinha->Id = $rsDados["id_setor"];
                        $objLinha->Codigo = $rsDados["cd_setor"];
                        $objLinha->Nome = $rsDados["nm_setor"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarMarca($PARM_id, $PARM_codigo) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "" || $PARM_codigo != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_marca, cd_marca, nm_marca";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_marca";
                    $sSQL .= " WHERE";
                    //
                    if($PARM_id != "") {
                        $sSQL .= " id_marca = " . mysqli_escape_string($objLink, $PARM_id);
                    } else {
                        $sSQL .= " cd_marca = '" . mysqli_escape_string($objLink, $PARM_codigo) . "'";
                    }
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXsebraeSetor();
                            //
                            $RET_obj->Id = $rsDados["id_marca"];
                            $RET_obj->Codigo = $rsDados["cd_marca"];
                            $RET_obj->Nome = $rsDados["nm_marca"];
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
        
    public function listarMarcas() {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " id_marca, cd_marca, nm_marca";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_marca";
                $sSQL .= " ORDER BY nm_marca;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXsebraeSetor();
                        //
                        $objLinha->Id = $rsDados["id_marca"];
                        $objLinha->Codigo = $rsDados["cd_marca"];
                        $objLinha->Nome = $rsDados["nm_marca"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarMarcasToken($PARM_id) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " mt.id_token_marca, mt.id_token, mt.id_marca, cm.id_marca, cm.cd_marca, cm.nm_marca";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_token_marca AS mt";
                $sSQL .= " LEFT JOIN tbdw_braabf17_marca AS cm";
                $sSQL .= " ON cm.id_marca = mt.id_marca";
                $sSQL .= " WHERE ";
                $sSQL .= " mt.id_token = " . mysqli_escape_string($objLink, $PARM_id);
                $sSQL .= " ORDER BY cm.nm_marca;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXsebraeMarca();
                        //
                        $objLinha->Id = $rsDados["id_marca"];
                        $objLinha->Codigo = $rsDados["cd_marca"];
                        $objLinha->Nome = $rsDados["nm_marca"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarSetoresToken($PARM_id) {
        //
        $RET_array = array();
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                // Monta a consulta
                $sSQL =  "SELECT";
                $sSQL .= " st.id_token_setor, st.id_token, st.id_setor, cs.id_setor, cs.cd_setor, cs.nm_setor";
                $sSQL .= " FROM";
                $sSQL .= " tbdw_braabf17_token_setor AS st";
                $sSQL .= " LEFT JOIN tbdw_braabf17_setor AS cs";
                $sSQL .= " ON cs.id_setor = st.id_setor";
                $sSQL .= " WHERE ";
                $sSQL .= " st.id_token = " . mysqli_escape_string($objLink, $PARM_id);
                $sSQL .= " ORDER BY cs.nm_setor;";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if (!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                    //
                    // Senao...
                } else {
                    // Obtem o resultado da consulta
                    $rsDados = mysqli_fetch_assoc($objResult);
                    //
                    while ($rsDados) {
                        //
                        $objLinha = new clsDSXsebraeSetor();
                        //
                        $objLinha->Id = $rsDados["id_setor"];
                        $objLinha->Codigo = $rsDados["cd_setor"];
                        $objLinha->Nome = $rsDados["nm_setor"];
                        //
                        $RET_array[$objLinha->Id] = $objLinha;
                        //
                        $rsDados = mysqli_fetch_assoc($objResult);
                    }
                    // 
                    // Libera a memoria alocada
                    mysqli_free_result($objResult);
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_array);
    }
    
    public function consultarTokenSebrae($PARM_id) {
        //
        $RET_obj = null;
        //
        // Se veio o ID ou o Codigo...
        if($PARM_id != "") {
            //
            if (LIB_debug) {
                $this->DBG_db_type = LIB_db_type;
                $this->DBG_db_host = LIB_db_host;
                $this->DBG_db_name = LIB_db_name;
                $this->DBG_db_user = LIB_db_user;
            }
            //
            $this->ERRO = 0;
            //
            $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
            //
            if (!$objLink) {
                //
                $this->ERRO = 1;
                $this->ERRO_bd_cod_erro = mysqli_connect_errno();
                $this->ERRO_bd_msg_erro = mysqli_connect_error();
                $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
                //
            } else {
                // Se obteve sucesso na selecao do banco de dados...
                if (mysqli_select_db($objLink, LIB_db_name)) {
                    // Monta a consulta
                    $sSQL =  "SELECT";
                    $sSQL .= " id_token, fl_intencao_investir, nr_meses, nr_cpf, fl_autoriza_comunic, ds_marcas_interesse, dv_faixa_valor_invest";
                    $sSQL .= " FROM";
                    $sSQL .= " tbdw_braabf17_token";
                    $sSQL .= " WHERE";
                    //
                    $sSQL .= " id_token = " . mysqli_escape_string($objLink, $PARM_id);
                    $sSQL .= ";";
                    //
                    //$this->ERRO_complemento = $sSQL;
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                    // Se falhou...
                    if (!$objResult) {
                        //
                        $this->ERRO = 1;
                        $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                        $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                        $this->ERRO_mensagem = "Falhou na consulta ao banco de dados!";
                        //
                        // Senao...
                    } else {
                        // Obtem o resultado da consulta
                        $rsDados = mysqli_fetch_assoc($objResult);
                        //
                        if ($rsDados) {
                            //
                            $RET_obj = new clsDSXtokenSebrae();
                            //
                            $RET_obj->Id = $rsDados["id_token"];
                            $RET_obj->PretendeInvestir = $rsDados["fl_intencao_investir"];
                            $RET_obj->InvestirMeses = $rsDados["nr_meses"];
                            $RET_obj->CPF = $rsDados["nr_cpf"];
                            $RET_obj->AutorizaContato = $rsDados["fl_autoriza_comunic"];
                            $RET_obj->MarcasInteresse = $rsDados["ds_marcas_interesse"];
                            $RET_obj->FaixaValorInvestimento = $rsDados["dv_faixa_valor_invest"];
                            //
                            $RET_obj->MarcasInteresseLista = $this->consultarMarcasToken($RET_obj->Id);
                            $RET_obj->SetoresInteresse = $this->consultarSetoresToken($RET_obj->Id);
                        }
                        // 
                        // Libera a memoria alocada
                        mysqli_free_result($objResult);
                    }
                    // Fecha a conexao com o banco de dados
                    mysqli_close($objLink);
                }
            }
        }
        //
        return($RET_obj);
    }
    
    public function gravarTokenSebrae($PARM_objToken, $PARM_objTokenSebrae) {
        //
        $this->removerMarcas($PARM_objToken->Id);
        $this->removerSetores($PARM_objToken->Id);
        $this->removerTokenSebrae($PARM_objToken->Id);
        //
        $this->incluirTokenSebrae($PARM_objToken, $PARM_objTokenSebrae);
        //
        if(!is_null($PARM_objTokenSebrae->MarcasInteresseLista)) {
            foreach($PARM_objTokenSebrae->MarcasInteresseLista AS $objItem) {
                //
                $this->incluirMarcaInteresse($PARM_objToken->Id, $objItem->Id);
            }
        }
        //
        if(!is_null($PARM_objTokenSebrae->SetoresInteresse)) {
            foreach($PARM_objTokenSebrae->SetoresInteresse AS $objItem) {
                //
                $this->incluirSetorInteresse($PARM_objToken->Id, $objItem->Id);
            }
        }
    }
    
    public function incluirMarcaInteresse($PARM_IdToken, $PARM_Id) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token_marca ";
                $sSQL .= " (id_token, id_marca) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_IdToken);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_Id);
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function incluirSetorInteresse($PARM_IdToken, $PARM_Id) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token_setor ";
                $sSQL .= " (id_token, id_setor) ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_IdToken);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_Id);
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function incluirTokenSebrae($PARM_objToken, $PARM_objTokenSebrae) {
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if(mysqli_select_db($objLink, LIB_db_name)) {
                //
                // Monta o INSERT
                $sSQL  = " INSERT INTO tbdw_braabf17_token ";
                $sSQL .= " (id_token, fl_intencao_investir";
                $sSQL .= ", nr_meses, nr_cpf, fl_autoriza_comunic";
                $sSQL .= ", ds_marcas_interesse, dv_faixa_valor_invest";
                $sSQL .= ") ";
                $sSQL .= " VALUES (";
                //
                $sSQL .= mysqli_escape_string($objLink, $PARM_objToken->Id);
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenSebrae->PretendeInvestir);
                //
                if(!is_null($PARM_objTokenSebrae->InvestirMeses) && $PARM_objTokenSebrae->InvestirMeses != "") {
                    $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenSebrae->InvestirMeses);
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objTokenSebrae->CPF) . "'";
                $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenSebrae->AutorizaContato);
                //
                if(!is_null($PARM_objTokenSebrae->MarcasInteresse) && $PARM_objTokenSebrae->MarcasInteresse != "") {
                    $sSQL .= ", '". mysqli_escape_string($objLink, $PARM_objTokenSebrae->MarcasInteresse) . "'";
                } else {
                    $sSQL .= ", NULL";
                }
                //
                if(!is_null($PARM_objTokenSebrae->FaixaValorInvestimento) && $PARM_objTokenSebrae->FaixaValorInvestimento != "") {
                    $sSQL .= ", ". mysqli_escape_string($objLink, $PARM_objTokenSebrae->FaixaValorInvestimento);
                } else {
                    $sSQL .= ", NULL";
                }
                //
                $sSQL .= ")";
                // Executa a consulta ao banco de dados
                $objResult = mysqli_query($objLink, $sSQL);
                // Se falhou...
                if(!$objResult) {
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na inser��o dos dados no banco de dados!";
                    //
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($this->ERRO);
    }
    
    public function removerSetores($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_setor ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
    public function removerMarcas($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token_marca ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
        
    }
    
    public function removerTokenSebrae($PARM_IdToken) {
         //
        $RET_ok = 0;
        //
        if (LIB_debug) {
            $this->DBG_db_type = LIB_db_type;
            $this->DBG_db_host = LIB_db_host;
            $this->DBG_db_name = LIB_db_name;
            $this->DBG_db_user = LIB_db_user;
        }
        //
        $this->ERRO = 0;
        $this->ERRO_bd_cod_erro = "";
        $this->ERRO_bd_msg_erro = "";
        $this->ERRO_mensagem = "";
        //
        $objLink = mysqli_connect(LIB_db_host, LIB_db_user, LIB_db_pass);
        //
        if (!$objLink) {
            //
            $RET_ok = 1;
            //
            $this->ERRO = 1;
            $this->ERRO_bd_cod_erro = mysqli_connect_errno();
            $this->ERRO_bd_msg_erro = mysqli_connect_error();
            $this->ERRO_mensagem = "Falhou na conexao com o banco de dados!";
            //
        } else {
            // Se obteve sucesso na selecao do banco de dados...
            if (mysqli_select_db($objLink, LIB_db_name)) {
                    //
                    // Monta o DELETE
                    $sSQL  = " DELETE FROM tbdw_braabf17_token ";
                    $sSQL .= " WHERE id_token = ". mysqli_escape_string($objLink, $PARM_IdToken);
                    //
                    // Executa a consulta ao banco de dados
                    $objResult = mysqli_query($objLink, $sSQL);
                //
                // Se falhou...
                if(mysqli_errno($objLink) != 0) {
                    //
                    $RET_ok = 1;
                    //
                    $this->ERRO = 1;
                    $this->ERRO_bd_cod_erro = mysqli_errno($objLink);
                    $this->ERRO_bd_msg_erro = mysqli_error($objLink);
                    $this->ERRO_mensagem = "Falhou na exclus�o de informa��es do banco de dados!|".$sSQL;
                }
                // Fecha a conexao com o banco de dados
                mysqli_close($objLink);
            }
        }
        //
        return($RET_ok);
    }
    
}