<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterTokenId.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterTokenId.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterTokenId";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id_pesquisa = $_REQUEST["id"];
$PARM_id_token = $_REQUEST["idt"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$objItem = $objDWC->obterTokenPeloId($PARM_id_pesquisa, $PARM_id_token);

$_JSON_linha = 0;
$_JSON_retorno = Array();

if($objItem) {
    //
    $_JSON_retorno['Id'] = $objItem->Id;
    $_JSON_retorno['DataCriacao'] = $objItem->DataCriacao;
    $_JSON_retorno['Codigo'] = $objItem->Codigo;
    $_JSON_retorno['Nome'] = $objItem->Nome;
    $_JSON_retorno['Email'] = $objItem->Email;
    $_JSON_retorno['Observacoes'] = $objItem->Observacoes;
    $_JSON_retorno['Unidade'] = $objItem->Unidade;
    $_JSON_retorno['CodigoPublicoAlvo'] = $objItem->CodigoPublicoAlvo;
    $_JSON_retorno['PublicoAlvo'] = $objItem->PublicoAlvo;
    $_JSON_retorno['IdAvaliacao'] = $objItem->IdAvaliacao;
    $_JSON_retorno['IdCliente'] = $objItem->IdCliente;
    $_JSON_retorno['Hash'] = $objItem->Hash;
    $_JSON_retorno['QtdeTotalPerguntas'] = $objItem->QtdeTotalPerguntas;
    $_JSON_retorno['QtdePerguntasFaltam'] = $objItem->QtdePerguntasFaltam;
    $_JSON_retorno['QtdePerguntasRespondidas'] = $objItem->QtdePerguntasRespondidas;
}
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
//echo("<br>SQL=".$objDWC->ERRO_sql);
