<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterPerfilFuncao.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterPerfilFuncao.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterPerfilFuncao";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscXadmin.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id = $_REQUEST["idpf"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDSXmasterAdmin();

$objItem = $objDWC->consultarPerfilDeFuncao($PARM_id);

$_JSON_retorno = Array();
//
$_JSON_retorno['IdPerfilFuncao'] = $objItem->IdPerfilFuncao;
$_JSON_retorno['IdCliente'] = $objItem->Cliente->Id;
$_JSON_retorno['CodigoCliente'] = $objItem->Cliente->Codigo;
$_JSON_retorno['NomeCliente'] = $objItem->Cliente->Nome;
$_JSON_retorno['Nome'] = $objItem->Nome;
$_JSON_retorno['Descricao'] = $objItem->Descricao;
$_JSON_retorno['DISC_D'] = $objItem->DISC_D;
$_JSON_retorno['DISC_D_Min'] = $objItem->DISC_D_Min;
$_JSON_retorno['DISC_D_Max'] = $objItem->DISC_D_Max;
$_JSON_retorno['DISC_I'] = $objItem->DISC_I;
$_JSON_retorno['DISC_I_Min'] = $objItem->DISC_I_Min;
$_JSON_retorno['DISC_I_Max'] = $objItem->DISC_I_Max;
$_JSON_retorno['DISC_S'] = $objItem->DISC_S;
$_JSON_retorno['DISC_S_Min'] = $objItem->DISC_S_Min;
$_JSON_retorno['DISC_S_Max'] = $objItem->DISC_S_Max;
$_JSON_retorno['DISC_C'] = $objItem->DISC_C;
$_JSON_retorno['DISC_C_Min'] = $objItem->DISC_C_Min;
$_JSON_retorno['DISC_C_Max'] = $objItem->DISC_C_Max;
//
$_JSON_retorno["ERRO"] = $objDWC->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDWC->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDWC->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDWC->ERRO_mensagem;
$_JSON_retorno["ERRO_complemento"] = $objDWC->ERRO_complemento;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
