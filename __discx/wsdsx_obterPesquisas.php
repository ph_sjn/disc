<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterPesquisas.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterPesquisas.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterPesquisas";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id_cliente = $_REQUEST["id"];
$PARM_cd_cliente = $_REQUEST["cd"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$RET_array = $objDWC->obterPesquisas($PARM_cd_cliente, $PARM_id_cliente);

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($RET_array AS $objItem) {
    if($objItem) {
        //
        $_JSON_retorno[$_JSON_linha]['IdAvaliacao'] = $objItem->IdAvaliacao;
        $_JSON_retorno[$_JSON_linha]['IdCliente'] = $objItem->IdCliente;
        $_JSON_retorno[$_JSON_linha]['Titulo'] = $objItem->Titulo;
        $_JSON_retorno[$_JSON_linha]['CodigoSituacao'] = $objItem->CodigoSituacao;
        $_JSON_retorno[$_JSON_linha]['DataInicio'] = $objItem->DataInicio;
        $_JSON_retorno[$_JSON_linha]['DataFim'] = $objItem->DataFim;
        $_JSON_retorno[$_JSON_linha]['CodigoProjeto'] = $objItem->CodigoProjeto;
        $_JSON_retorno[$_JSON_linha]['NomeProjeto'] = $objItem->NomeProjeto;
        $_JSON_retorno[$_JSON_linha]['CodigoCliente'] = $objItem->CodigoCliente;
        $_JSON_retorno[$_JSON_linha]['NomeCliente'] = $objItem->NomeCliente;
        $_JSON_retorno[$_JSON_linha]['SituacaoAvaliacao'] = $objItem->SituacaoAvaliacao;
        $_JSON_linha++;
    }
}           
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
