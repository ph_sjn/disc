<?php

//$CONFIGLIB_DISCX = "/home/users/__discx/";
$CONFIGLIB_DISCX = "D:/Projetos/Source/disc/__discx/";

include_once($CONFIGLIB_DISCX . "inc/libCurlClass.php");

function DISCX_obterENDPOINT() {
    //
//    return("http://aws.franquiaextranet.com.br/__discx/");
    return("http://localhost/disc/__discx/");
}

function DWX_obterENDPOINT() {
    //
//    return("http://aws.franquiaextranet.com.br/__discx/");
    return("http://localhost/disc/__discx/");
}
class clsDSXlocalPerfilDeFuncao {
    //
    Public $IdPerfilFuncao = null;      // id_perfil_funcao
    Public $IdCliente = null;           // id_cliente
    Public $Nome = null;                // nm_perfil_funcao
    Public $Descricao = null;           // ds_perfil_funcao
    Public $DISC_D = null;              // pc_disc_d
    Public $DISC_D_Min = null;          // pc_disc_d_min
    Public $DISC_D_Max = null;          // pc_disc_d_max
    Public $DISC_I = null;              // pc_disc_i
    Public $DISC_I_Min = null;          // pc_disc_i_min
    Public $DISC_I_Max = null;          // pc_disc_i_max
    Public $DISC_S = null;              // pc_disc_s
    Public $DISC_S_Min = null;          // pc_disc_s_min
    Public $DISC_S_Max = null;          // pc_disc_s_max
    Public $DISC_C = null;              // pc_disc_c
    Public $DISC_C_Min = null;          // pc_disc_c_min
    Public $DISC_C_Max = null;          // pc_disc_c_max
}

function DISCX_criarPerfilDeFuncao($PARM_tipo, $PARM_obj) {
    //
    // Endpoint para o webservice
    $sURL = DISCX_obterENDPOINT() . "wsdsx_criarPerfilFuncao.php";
    // Parametros
    $aParms = array(
        'tipo' => $PARM_tipo
    ,   'idc' => $PARM_obj->IdCliente
    ,   'nm' => $PARM_obj->Nome
    ,   'ds' => $PARM_obj->Descricao
    ,   'd' => $PARM_obj->DISC_D
    ,   'dl' => $PARM_obj->DISC_D_Min
    ,   'dm' => $PARM_obj->DISC_D_Max
    ,   'i' => $PARM_obj->DISC_I
    ,   'il' => $PARM_obj->DISC_I_Min
    ,   'im' => $PARM_obj->DISC_I_Max
    ,   's' => $PARM_obj->DISC_S
    ,   'sl' => $PARM_obj->DISC_S_Min
    ,   'sm' => $PARM_obj->DISC_S_Max
    ,   'c' => $PARM_obj->DISC_C
    ,   'cl' => $PARM_obj->DISC_C_Min
    ,   'cm' => $PARM_obj->DISC_C_Max
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_gravarPerfilDeFuncao($PARM_tipo, $PARM_obj) {
    //
    // Endpoint para o webservice
    $sURL = DISCX_obterENDPOINT() . "wsdsx_gravarPerfilFuncao.php";
    // Parametros
    $aParms = array(
        'tipo' => $PARM_tipo
    ,   'idpf' => $PARM_obj->IdPerfilFuncao
    ,   'idc' => $PARM_obj->IdCliente
    ,   'nm' => $PARM_obj->Nome
    ,   'ds' => $PARM_obj->Descricao
    ,   'd' => $PARM_obj->DISC_D
    ,   'dl' => $PARM_obj->DISC_D_Min
    ,   'dm' => $PARM_obj->DISC_D_Max
    ,   'i' => $PARM_obj->DISC_I
    ,   'il' => $PARM_obj->DISC_I_Min
    ,   'im' => $PARM_obj->DISC_I_Max
    ,   's' => $PARM_obj->DISC_S
    ,   'sl' => $PARM_obj->DISC_S_Min
    ,   'sm' => $PARM_obj->DISC_S_Max
    ,   'c' => $PARM_obj->DISC_C
    ,   'cl' => $PARM_obj->DISC_C_Min
    ,   'cm' => $PARM_obj->DISC_C_Max
    );
//    //
//    echo("<br>URL=" . $sURL . "<br>");
//    echo("PARM_obj<pre>");
//    print_r($PARM_obj);
//    echo("</pre>");
    //
    $objCurl = new clsRPcurl();
//    //
//    echo("objCurl-Antes<pre>");
//    print_r($objCurl);
//    echo("</pre>");
//    //
    $objCurl->post($sURL, $aParms);
//    //
//    echo("objCurl-Depois<pre>");
//    print_r($objCurl);
//    echo("</pre>;");
    //
    return($objCurl->RESULTS);
}

function DISCX_excluirPerfilDeFuncao($PARM_tipo, $PARM_obj) {
    //
    // Endpoint para o webservice
    $sURL = DISCX_obterENDPOINT() . "wsdsx_excluirPerfilFuncao.php";
    // Parametros
    $aParms = array(
        'tipo' => $PARM_tipo
    ,   'idpf' => $PARM_obj->IdPerfilFuncao
    ,   'idc' => $PARM_obj->IdCliente
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);    
}

function DISCX_consultarPerfilDeFuncao($PARM_tipo, $PARM_id) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_obterPerfilFuncao.php";
    // Parametros
    $aParms = array(
        'tipo' => $PARM_tipo
    ,   'idpf' => $PARM_id
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_listarPerfisDeFuncao($PARM_tipo, $PARM_id_cliente, $PARM_cd_cliente, $PARM_nm_perfil_funcao) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_listarPerfisDeFuncao.php";
    // Parametros
    $aParms = array(
        'tipo' => $PARM_tipo
    ,   'idc' => $PARM_id_cliente
    ,   'cdc' => $PARM_cd_cliente
    ,   'nmpf' => $PARM_nm_perfil_funcao
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_consultarCliente($PARM_id, $PARM_codigo) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_obterCliente.php";
    // Parametros
    $aParms = array(
        'tipo' => "1"
    ,   'id' => $PARM_id
    ,   'cd' => $PARM_codigo
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_listarProjetos($PARM_id, $PARM_codigo) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_listarProjetos.php";
    // Parametros
    $aParms = array(
        'tipo' => "1"
    ,   'idc' => $PARM_id
    ,   'cd' => $PARM_codigo
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_listarClientes() {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_listarClientes.php";
    // Parametros
    $aParms = array(
        'tipo' => "1"
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

function DISCX_listarTiposDePaginas() {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_listarTiposDePaginas.php";
    // Parametros
    $aParms = array(
        'tipo' => "1"
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

// Parametro: $PARM_selPesquisas : array() - avaliacoes/pesquisas selecionadas
function DISCX_listarCamposEntrevistado($PARM_selPesquisas) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_listarCamposEntrevistado.php";
    // Parametros
    $aParms = array(
        'tipo' => "2"
		, 'pesq' => $PARM_selPesquisas
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

// Parametro: 
//		$PARM_idCliente   	: integer - identificacao do cliente
//		$PARM_selPesquisas 	: array() - avaliacoes/pesquisas selecionadas
//		$PARM_selCampos		: array() - campos do entrevistado selecionados
//		$PARM_itensPorPag	: integer - qtde de itens por pagina
//		$PARM_nrPag			: integer - nr da pagina
// 
function DISCX_buscarEntrevistados($PARM_idCliente, $PARM_selPesquisas, $PARM_selCampos, $PARM_itensPorPag, $PARM_nrPag) {
    //
    $sURL = DISCX_obterENDPOINT() . "wsdsx_buscarEntrevistados.php";
    // Parametros
    $aParms = array(
        'tipo' => "2"
		, 'idc' => $PARM_idCliente
		, 'pesq' => $PARM_selPesquisas
		, 'cmps' => $PARM_selCampos
		, 'ippg' => $PARM_itensPorPag
		, 'pg' => $PARM_nrPag
    );
    //
    $objCurl = new clsRPcurl();
    //
    $objCurl->post($sURL, $aParms);
    //
    return($objCurl->RESULTS);
}

////////////////////////////////////////////////////////////////////////////////



function DWX_obterPesquisas($PARM_tipo, $PARM_cd_cliente, $PARM_id_cliente ) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_obterPesquisas.php";
    $cURL_url .= "?tipo=" . $PARM_tipo . "&id=" . $PARM_id_cliente . "&cd=" . $PARM_cd_cliente;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Decodifica o JSON retornado
    //$cURL_decoded = json_decode($cURL_response);
    //// Obtem o http status code
    //$cURL_http_status = curl_getinfo($cURL_sessao, CURLINFO_HTTP_CODE);
    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    
    return($cURL_response);
}

function DWX_obterPesquisa($PARM_tipo, $PARM_id ) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_obterPesquisa.php";
    $cURL_url .= "?tipo=" . $PARM_tipo . "&id=" . $PARM_id;
    
    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Decodifica o JSON retornado
    //$cURL_decoded = json_decode($cURL_response);
    //// Obtem o http status code
    //$cURL_http_status = curl_getinfo($cURL_sessao, CURLINFO_HTTP_CODE);
    // Fecha o cURL
    curl_close($cURL_sessao);

    // echo($cURL_response);
    // echo($cURL_decoded);
    
    return($cURL_response);
}

function DWX_obterTokens($PARM_tipo, $PARM_id ) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_obterTokens.php";
    $cURL_url .= "?tipo=" . $PARM_tipo . "&id=" . $PARM_id;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Decodifica o JSON retornado
    //$cURL_decoded = json_decode($cURL_response);
    //// Obtem o http status code
    //$cURL_http_status = curl_getinfo($cURL_sessao, CURLINFO_HTTP_CODE);
    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    
    return($cURL_response);
}

//  obterPerfilDeFuncaoPeloID(<Colecao-de-perfis>, <ID-Perfil>)
//
//  Retorna o objeto Perfil de Funcao pelo ID fornecido
function DISCX_obterPerfilDeFuncaoPeloID($PARM_colItens, $PARM_id_item) {
    //
    // Inicializa o retorno
    $RET_obj = null;
    // Percorre cada token da cole��o
    foreach ($PARM_colItens AS $objItem) {
        // Se encontrou o ID desejado...
        if($objItem->IdPerfilFuncao == $PARM_id_item) {
            // Atribui para a vari�vel de retorno
            $RET_obj = $objItem;
            // Quebra o la�o "foreach"
            break;
        }
    }
    //
    return($RET_obj);
}

function DWX_obterTokenPeloIDnoBD($PARM_tipo, $PARM_id_pesquisa, $PARM_id_token) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_obterTokenId.php";
    $cURL_url .= "?tipo=" . $PARM_tipo . "&id=" . $PARM_id_pesquisa . "&idt=" . $PARM_id_token;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //
    return($cURL_response);  
}

//  obterTokenPeloID(<Colecao-de-tokens>, <ID-Token>)
//  
//  Retorna o objeto token pelo ID fornecido
//
function DWX_obterTokenPeloID($PARM_colTokens, $PARM_id_token) {
    //
    // Inicializa o retorno
    $RET_objToken = null;
    // Percorre cada token da cole��o
    foreach ($PARM_colTokens AS $objItem) {
        // Se encontrou o ID desejado...
        if($objItem->Id == $PARM_id_token) {
            // Atribui para a vari�vel de retorno
            $RET_objToken = $objItem;
            // Quebra o la�o "foreach"
            break;
        }
    }
    //
    return($RET_objToken);
}

//  obterTokenPeloEmail(<Colecao-de-tokens>, <Tipo>, <Email>)
//  
//  Tipo: 1=Franqueador, 2=Franqueado
//  
//  Retorna o objeto token pelos parametros fornecidos
//
function DWX_obterTokenPeloEmail($PARM_colTokens, $PARM_tipo, $PARM_email) {
    //
    // Inicializa o retorno
    $RET_objToken = null;
    // Percorre cada token da cole��o
    foreach ($PARM_colTokens AS $objItem) {
        // Se encontrou o ID desejado...
        if(($objItem->CodigoPublicoAlvo == $PARM_tipo) && (strtolower($objItem->Email) == strtolower($PARM_email))) {
            // Atribui para a vari�vel de retorno
            $RET_objToken = $objItem;
            // Quebra o la�o "foreach"
            break;
        }
    }
    //
    return($RET_objToken);
}

class clsDWClocalToken {
    //
    // id_token
    public $Id = null;
    // dt_criacao
    public $DataCriacao = null;
    // ds_codigo
    public $Codigo = null;
    // ds_nome
    public $Nome = null;
    // ds_email
    public $Email = null;
    // ds_observacao
    public $Observacoes = null;
    // ds_unidade
    public $Unidade = null;
    // cd_publico_alvo
    public $CodigoPublicoAlvo = null;
    // ds_publico_alvo
    public $PublicoAlvo = null;
    // id_avaliacao
    public $IdAvaliacao = null;
    // id_cliente
    public $IdCliente = null;
    //
    public $QtdeTotalPerguntas = null;
    public $QtdePerguntasFaltam = null;
    public $QtdePerguntasRespondidas = null;
}

function DWX_criarToken($PARM_tipo, $PARM_objToken) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_criarToken.php";
    $cURL_url .= "?tipo=" . $PARM_tipo;
    //
    $cURL_url .= "&idc=" . $PARM_objToken->IdCliente;
    $cURL_url .= "&ida=" . $PARM_objToken->IdAvaliacao;
    $cURL_url .= "&cpa=" . $PARM_objToken->CodigoPublicoAlvo;
    $cURL_url .= "&nm=" . urlencode($PARM_objToken->Nome);
    $cURL_url .= "&em=" . urlencode($PARM_objToken->Email);
    $cURL_url .= "&cd=" . urlencode($PARM_objToken->Codigo);
    $cURL_url .= "&un=" . urlencode($PARM_objToken->Unidade);
    $cURL_url .= "&obs=" . urlencode($PARM_objToken->Observacoes);

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    // Devolve o retorno da chamada
    return($cURL_response);
}

function DWX_gravarToken($PARM_tipo, $PARM_objToken) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_gravarToken.php";
    $cURL_url .= "?tipo=" . $PARM_tipo;
    //
    $cURL_url .= "&idt=" . $PARM_objToken->Id;
    $cURL_url .= "&idc=" . $PARM_objToken->IdCliente;
    $cURL_url .= "&ida=" . $PARM_objToken->IdAvaliacao;
    $cURL_url .= "&cpa=" . $PARM_objToken->CodigoPublicoAlvo;
    $cURL_url .= "&nm=" . urlencode($PARM_objToken->Nome);
    $cURL_url .= "&em=" . urlencode($PARM_objToken->Email);
    $cURL_url .= "&cd=" . urlencode($PARM_objToken->Codigo);
    $cURL_url .= "&un=" . urlencode($PARM_objToken->Unidade);
    $cURL_url .= "&obs=" . urlencode($PARM_objToken->Observacoes);

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    // Devolve o retorno da chamada
    return($cURL_response);
    
}

function DWX_excluirToken($PARM_tipo, $PARM_objToken) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_excluirToken.php";
    $cURL_url .= "?tipo=" . $PARM_tipo;
    //
    $cURL_url .= "&idt=" . $PARM_objToken->Id;
    $cURL_url .= "&idc=" . $PARM_objToken->IdCliente;
    $cURL_url .= "&ida=" . $PARM_objToken->IdAvaliacao;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    // Devolve o retorno da chamada
    return($cURL_response);
    
}

function DWX_reiniciarPesquisaToken($PARM_tipo, $PARM_objToken) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_reiniciarPesquisaToken.php";
    $cURL_url .= "?tipo=" . $PARM_tipo;
    //
    $cURL_url .= "&idt=" . $PARM_objToken->Id;
    $cURL_url .= "&idc=" . $PARM_objToken->IdCliente;
    $cURL_url .= "&ida=" . $PARM_objToken->IdAvaliacao;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    // Devolve o retorno da chamada
    return($cURL_response);
    
}

function DWX_gravarModeloEmail($PARM_tipo, $PARM_id, $PARM_assunto, $PARM_texto, $PARM_assunto_conclusao, $PARM_texto_conclusao) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_gravarModeloEmail.php";
    $cURL_url .= "?tipo=" . $PARM_tipo;
    //
    $cURL_url .= "&id=" . $PARM_id;
    $cURL_url .= "&assunto=" . urlencode($PARM_assunto);
    $cURL_url .= "&texto=" . urlencode($PARM_texto);
    $cURL_url .= "&assuntoconcl=" . urlencode($PARM_assunto_conclusao);
    $cURL_url .= "&textoconcl=" . urlencode($PARM_texto_conclusao);

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    // Devolve o retorno da chamada
    return($cURL_response);
}

function DWX_obterRespostasFranqueador($PARM_tipo, $PARM_id_pesquisa, $PARM_id_franqueador ) {
    
    // Endpoint para o webservice
    $cURL_url = DWX_obterENDPOINT() . "wsdsx_obterRespFranqueador.php";
    $cURL_url .= "?tipo=" . $PARM_tipo . "&idp=" . $PARM_id_pesquisa . "&idf=" . $PARM_id_franqueador;

    // Inicializa a sessao cURL
    $cURL_sessao = curl_init($cURL_url);

    // Configura o cURL antes do envio do request
    curl_setopt($cURL_sessao, CURLOPT_RETURNTRANSFER, true);                                                                      

    // Faz a chamada e obtem o retorno
    $cURL_response = curl_exec($cURL_sessao);

    // Decodifica o JSON retornado
    //$cURL_decoded = json_decode($cURL_response);
    //// Obtem o http status code
    //$cURL_http_status = curl_getinfo($cURL_sessao, CURLINFO_HTTP_CODE);
    // Fecha o cURL
    curl_close($cURL_sessao);

    //echo($cURL_response);
    //echo($cURL_decoded);
    
    return($cURL_response);
}
