<?php

/***
 * 
 * IPs validos para consumir cada servico
 * 
 * 2015-03-10   MARCIO      Incluido codigo que permite maior controle
 *                          sobre os IPs com acesso rejeitado para por exemplo
 *                          registrar a tentativa de acesso em log:
 * 
 *                          Ajustar $RPWS_SemBloqueio = TRUE antes do include
 *                          incAutorizacao.php no servico.
 * 
 *                          Analisar $RPWS_IP_SemAcesso e $RPWS_retornoNaoAutorizado
 *                          para registrar o acesso nao autorizado e interromper o servico.
 * 
 ***/

function verificarIP($parm_servico, $parm_ip) {
    //
    $RPWS_IP_Localhost = "127.0.0.1";
    $RPWS_IP_MarcioPossani = "201.13.174.171";
    $RPWS_IP_TabeliaoDigital = "201.6.246.87";
    $RPWS_IP_JF_2a_Rede = "200.243.67.123";
    $RPWS_IP_JF_1 = "187.16.179.19";
    $RPWS_IP_JF_2 = "187.16.179.20";
    $RPWS_IP_JF_3 = "187.16.179.21";
    $RPWS_IP_JF_4 = "187.16.179.22";
    $RPWS_IP_JF_5 = "187.16.179.23";
    $RPWS_IP_AWS_Master = "177.71.182.94";
    $RPWS_IP_AWS_RPSRV1 = "177.71.182.22";
    $RPWS_IP_AWS_Possani = "177.71.252.77";
    //
    $RPWS_IPs_Geral = array(
                $RPWS_IP_Localhost => $RPWS_IP_Localhost
            ,   $RPWS_IP_MarcioPossani => $RPWS_IP_MarcioPossani
            ,   $RPWS_IP_TabeliaoDigital => $RPWS_IP_TabeliaoDigital
            ,   $RPWS_IP_JF_2a_Rede => $RPWS_IP_JF_2a_Rede
            ,   $RPWS_IP_JF_1 => $RPWS_IP_JF_1
            ,   $RPWS_IP_JF_2 => $RPWS_IP_JF_2
            ,   $RPWS_IP_JF_3 => $RPWS_IP_JF_3
            ,   $RPWS_IP_JF_4 => $RPWS_IP_JF_4
            ,   $RPWS_IP_JF_5 => $RPWS_IP_JF_5
            ,   $RPWS_IP_AWS_Master => $RPWS_IP_AWS_Master
            ,   $RPWS_IP_AWS_RPSRV1 => $RPWS_IP_AWS_RPSRV1
            ,   $RPWS_IP_AWS_Possani => $RPWS_IP_AWS_Possani
        );
    //
    $RPWS_Servicos_IPs = array(
                strtolower("wsdsx_listarEntrevistas") => $RPWS_IPs_Geral
//            ,   strtolower("wsdsx") => $RPWS_IPs_Geral
        );
    //
    $parm_servico = strtolower($parm_servico);
    //
    return ($RPWS_Servicos_IPs[$parm_servico][$parm_ip] == $parm_ip);
}

// Obtem o IP de onde veio a chamada para o servico
$RPWS_IP_Origem = $_SERVER["REMOTE_ADDR"];

// Indica se eh acesso nao autorizado
$RPWS_IP_SemAcesso = (!(verificarIP(strtolower($RPWS_Servico), $RPWS_IP_Origem)));
//
if($RPWS_IP_SemAcesso) {
    //
    // Monta o retorno
    $RPWS_retornoNaoAutorizado = "-999";
    $RPWS_retornoNaoAutorizado .= "|" . $RPWS_IP_Origem . " n�o autorizado !";
    //
    // Se estiver configurado para nao terminar o php
    // quando o IP nao tiver acesso...
    if($RPWS_SemBloqueio) {
        //
        // Nao faz nada
        //
    // Senao...
    } else {
        // Retorna erro
        echo($RPWS_retornoNaoAutorizado);
        // Termina o php
        exit();
    }
}
