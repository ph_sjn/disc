<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_listarPerfisDeFuncao.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_listarPerfisDeFuncao.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_listarPerfisDeFuncao";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXadmin.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id_cliente = $_REQUEST["idc"];
$PARM_cd_cliente = $_REQUEST["cdc"];
$PARM_nm_perfil_funcao = $_REQUEST["nmpf"];

// Instancia a classe principal
$objDSXadmin = new clsDSXmasterAdmin();

$parmFiltro = null;

if($PARM_id_cliente != "" && $PARM_cd_cliente == "") {
    $parmFiltro = array("id_cliente" => $PARM_id_cliente);
}

if($PARM_cd_cliente != "") {
    if(!is_null($parmFiltro)) {
        $tempArray = array("cd_cliente" => $PARM_cd_cliente);
        //
        $parmFiltro = array_merge($parmFiltro, $tempArray);
    } else {
        $parmFiltro = array("cd_cliente" => $PARM_cd_cliente);
    }
}


$colColecao = $objDSXadmin->listarPerfisDeFuncao($parmFiltro);

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($colColecao AS $objItem) {
    if($objItem) {
        $_JSON_retorno[$_JSON_linha]['IdPerfilFuncao'] = $objItem->IdPerfilFuncao;
        $_JSON_retorno[$_JSON_linha]['IdCliente'] = $objItem->Cliente->Id;
        $_JSON_retorno[$_JSON_linha]['CodigoCliente'] = $objItem->Cliente->Codigo;
        $_JSON_retorno[$_JSON_linha]['NomeCliente'] = $objItem->Cliente->Nome;
        $_JSON_retorno[$_JSON_linha]['Nome'] = $objItem->Nome;
        $_JSON_retorno[$_JSON_linha]['Descricao'] = $objItem->Descricao;
        $_JSON_retorno[$_JSON_linha]['DISC_D'] = $objItem->DISC_D;
        $_JSON_retorno[$_JSON_linha]['DISC_D_Min'] = $objItem->DISC_D_Min;
        $_JSON_retorno[$_JSON_linha]['DISC_D_Max'] = $objItem->DISC_D_Max;
        $_JSON_retorno[$_JSON_linha]['DISC_I'] = $objItem->DISC_I;
        $_JSON_retorno[$_JSON_linha]['DISC_I_Min'] = $objItem->DISC_I_Min;
        $_JSON_retorno[$_JSON_linha]['DISC_I_Max'] = $objItem->DISC_I_Max;
        $_JSON_retorno[$_JSON_linha]['DISC_S'] = $objItem->DISC_S;
        $_JSON_retorno[$_JSON_linha]['DISC_S_Min'] = $objItem->DISC_S_Min;
        $_JSON_retorno[$_JSON_linha]['DISC_S_Max'] = $objItem->DISC_S_Max;
        $_JSON_retorno[$_JSON_linha]['DISC_C'] = $objItem->DISC_C;
        $_JSON_retorno[$_JSON_linha]['DISC_C_Min'] = $objItem->DISC_C_Min;
        $_JSON_retorno[$_JSON_linha]['DISC_C_Max'] = $objItem->DISC_C_Max;
        //
        $_JSON_linha++;
    }
}           

// Permite cross-domain Ajax
header('Access-Control-Allow-Origin: *');
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
