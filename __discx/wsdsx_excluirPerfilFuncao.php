<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_excluirPerfilFuncao.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_excluirPerfilFuncao.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_excluirPerfilFuncao";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXadmin.php";

// Obtem os parametros
$PARM_IdPerfilFuncao = $_REQUEST["idpf"];
$PARM_IdCliente = $_REQUEST["idc"];

// Instancia a classe principal
$objDSXadmin = new clsDSXmasterAdmin();

$objDSXadmin->excluirPerfilDeFuncao($PARM_IdPerfilFuncao, $PARM_IdCliente);

$_JSON_retorno = Array();

$_JSON_retorno["ERRO"] = $objDSXadmin->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDSXadmin->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDSXadmin->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDSXadmin->ERRO_mensagem;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
