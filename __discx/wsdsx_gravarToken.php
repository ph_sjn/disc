<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_gravarToken.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_gravarToken.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_gravarToken";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

// Obtem os parametros
$PARM_IdToken = $_REQUEST["idt"];
$PARM_IdCliente = $_REQUEST["idc"];
$PARM_IdAvaliacao = $_REQUEST["ida"];
$PARM_CodigoPublicoAlvo = $_REQUEST["cpa"];
$PARM_Nome = urldecode($_REQUEST["nm"]);
$PARM_Email = urldecode($_REQUEST["em"]);
$PARM_Codigo = urldecode($_REQUEST["cd"]);
$PARM_Unidade = urldecode($_REQUEST["un"]);
$PARM_Observacoes = urldecode($_REQUEST["obs"]);
$PARM_Hash = urldecode($_REQUEST["hsn"]);

// Cria um novo objeto token
$objToken = new clsDWCtoken();

// Preenche os parametros
$objToken->Id = $PARM_IdToken;
$objToken->IdCliente = $PARM_IdCliente;
$objToken->IdAvaliacao = $PARM_IdAvaliacao;
$objToken->Nome = $PARM_Nome;
$objToken->Email = $PARM_Email;
$objToken->Codigo = $PARM_Codigo;
$objToken->Unidade = $PARM_Unidade;
$objToken->Observacoes = $PARM_Observacoes;
$objToken->Hash = $PARM_Hash;

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

If($objToken->Hash != "") {
    // TO DO: Verificar se o hash
} else {
    $objToken->Hash = $objDWC->gerarHashToken($objToken->IdCliente, $objToken->IdAvaliacao, $objToken->Id);
}

$objDWC->gravarToken($objToken);

$_JSON_retorno = Array();

$_JSON_retorno["ERRO"] = $objDWC->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDWC->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDWC->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDWC->ERRO_mensagem;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
