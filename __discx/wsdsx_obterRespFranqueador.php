<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterRespFranqueador.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterRespFranqueador.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterRespFranqueador";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id_pesquisa = $_REQUEST["idp"];
$PARM_id_franqueador = $_REQUEST["idf"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$RET_array = $objDWC->obterRespostasFranqueador($PARM_id_pesquisa, $PARM_id_franqueador);

//$_JSON_retorno = (array) $RET_array;

//echo("<pre>");
//print_r($RET_array);
//echo("</pre>");

//$_JSON_retorno = $RET_array;

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($RET_array AS $objItem) {
    //
    if($objItem) {
        //
        $_JSON_retorno[$_JSON_linha]["CodigoTema"] = $objItem->CodigoTema;
        $_JSON_retorno[$_JSON_linha]["DescricaoTema"] = $objItem->DescricaoTema;
        $_JSON_retorno[$_JSON_linha]["CodigoItem"] = $objItem->CodigoItem;
        $_JSON_retorno[$_JSON_linha]["DescricaoItem"] = $objItem->DescricaoItem;
        $_JSON_retorno[$_JSON_linha]["IdItemFormulario"] = $objItem->IdItemFormulario;
        $_JSON_retorno[$_JSON_linha]["IdFormulario"] = $objItem->IdFormulario;
        $_JSON_retorno[$_JSON_linha]["CodigoAgrupamento"] = $objItem->CodigoAgrupamento;
        $_JSON_retorno[$_JSON_linha]["DescricaoAgrupamento"] = $objItem->DescricaoAgrupamento;
        $_JSON_retorno[$_JSON_linha]["TipoOpcoes"] = $objItem->TipoOpcoes;
//        $_JSON_retorno[$_JSON_linha]["colOpcoes"] = (array) $objItem->colOpcoes;
        //
        foreach ($objItem->colRespostas AS $objSubItem) {
            //
            if($objSubItem) {
                $_JSON_retorno[$_JSON_linha]["colRespostas"][$objSubItem->CodigoOpcao]["Resposta"] = $objSubItem->Resposta;
                $_JSON_retorno[$_JSON_linha]["colRespostas"][$objSubItem->CodigoOpcao]["DescricaoOpcaoItem"] = $objSubItem->DescricaoOpcaoItem;
                $_JSON_retorno[$_JSON_linha]["colRespostas"][$objSubItem->CodigoOpcao]["CodigoOpcao"] = $objSubItem->CodigoOpcao;
            }
        }
        //
        $_JSON_retorno[$_JSON_linha]["QuantidadeRespostas"] = $objItem->QuantidadeRespostas;
        //
        // $_JSON_retorno[$_JSON_linha] = (array) $objItem;
        //
        $_JSON_linha++;
    }
}           

//echo("<pre>");
//print_r($_JSON_retorno);
//echo("</pre>");
//exit;

// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
