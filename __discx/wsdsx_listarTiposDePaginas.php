<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_listarTiposDePaginas.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_listarTiposDePaginas.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_listarTiposDePaginas";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXadmin.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];

// Instancia a classe principal
$objDSXadmin = new clsDSXmasterAdmin();

$colTiposDePagina = $objDSXadmin->listarTiposDePagina();

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($colTiposDePagina AS $objItem) {
    if($objItem) {
        $_JSON_retorno[$_JSON_linha]['CodigoTipoPagina'] = $objItem->CodigoTipoPagina;
        $_JSON_retorno[$_JSON_linha]['NomeTipoPagina'] = $objItem->NomeTipoPagina;
        //
        $_JSON_linha++;
    }
}           

// Permite cross-domain Ajax
header('Access-Control-Allow-Origin: *');
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
