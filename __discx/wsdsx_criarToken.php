<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_criarToken.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_criarToken.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_criarToken";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe 
include_once "inc/libDiscX.php";

// Obtem os parametros
$PARM_IdCliente = $_REQUEST["idc"];
$PARM_IdAvaliacao = $_REQUEST["ida"];
$PARM_CodigoPublicoAlvo = $_REQUEST["cpa"];
$PARM_Nome = urldecode($_REQUEST["nm"]);
$PARM_Email = urldecode($_REQUEST["em"]);
$PARM_Codigo = urldecode($_REQUEST["cd"]);
$PARM_Unidade = urldecode($_REQUEST["un"]);
$PARM_Observacoes = urldecode($_REQUEST["obs"]);
//$PARM_DataCriacao = urldecode($_REQUEST["dtc"]);
$PARM_PublicoAlvo = $_REQUEST["pa"];

// Cria um novo objeto token
$objToken = new clsDWCtoken();

// Preenche os parametros
$objToken->IdCliente = $PARM_IdCliente;
$objToken->IdAvaliacao = $PARM_IdAvaliacao;
$objToken->CodigoPublicoAlvo = $PARM_CodigoPublicoAlvo;
$objToken->Nome = $PARM_Nome;
$objToken->Email = $PARM_Email;
$objToken->Codigo = $PARM_Codigo;
$objToken->Unidade = $PARM_Unidade;
$objToken->Observacoes = $PARM_Observacoes;
//$objToken->DataCriacao = $PARM_DataCriacao;

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$RET_Id_Token = $objDWC->incluirToken($objToken);

$_JSON_retorno = Array();

$_JSON_retorno["IdToken"] = $RET_Id_Token;
$_JSON_retorno["ERRO"] = $objDWC->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDWC->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDWC->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDWC->ERRO_mensagem;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
