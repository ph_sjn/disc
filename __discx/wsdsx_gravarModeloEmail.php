<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_gravarModeloEmail.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_gravarModeloEmail.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_gravarModeloEmail";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_id = $_REQUEST["id"];
$PARM_assunto = urldecode($_REQUEST["assunto"]);
$PARM_texto = urldecode($_REQUEST["texto"]);
$PARM_assuntoconcl = urldecode($_REQUEST["assuntoconcl"]);
$PARM_textoconcl = urldecode($_REQUEST["textoconcl"]);

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$objRetorno = $objDWC->gravarModeloEmail($PARM_id, $PARM_assunto, $PARM_texto, $PARM_assuntoconcl, $PARM_textoconcl);

$_JSON_retorno = Array();

$_JSON_retorno["ERRO"] = $objDWC->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDWC->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDWC->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDWC->ERRO_mensagem;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
