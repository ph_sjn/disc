<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_buscarEntrevistados.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_buscarEntrevistados.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_buscarEntrevistados";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXbusca.php";

$PARM_tipoRetorno  	= $_REQUEST["tipo"];
$PARM_idCliente    	= $_REQUEST["idc"];
$PARM_selPesquisas 	= $_REQUEST["pesq"];
$PARM_selCampos 	= $_REQUEST["cmps"];
$PARM_itensPorPag  	= $_REQUEST["ippg"];
$PARM_nrPag    		= $_REQUEST["pg"];

// Instancia a classe principal
$objDSXbusca = new clsDSXbusca();

$colEntrevistados = $objDSXbusca->buscarEntrevistados($PARM_idCliente, $PARM_selPesquisas, $PARM_selCampos, $PARM_itensPorPag, $PARM_nrPag);

$_JSON_linha = 0;
$_JSON_retorno = Array();

foreach ($colEntrevistados AS $objItem) {
    if($objItem) {
		$_JSON_retorno[$_JSON_linha]['Total'] = $objItem->Total;
		$_JSON_retorno[$_JSON_linha]['IdCliente'] = $objItem->IdCliente;
		$_JSON_retorno[$_JSON_linha]['IdAvaliacao'] = $objItem->IdAvaliacao;
		$_JSON_retorno[$_JSON_linha]['Titulo'] = $objItem->Titulo;
		$_JSON_retorno[$_JSON_linha]['IdToken'] = $objItem->IdToken;
		$_JSON_retorno[$_JSON_linha]['NomeEntrevistado'] = $objItem->NomeEntrevistado;
		$_JSON_retorno[$_JSON_linha]['ResultadoFatorDisc'] = $objItem->ResultadoFatorDisc;
		$_JSON_retorno[$_JSON_linha]['DtInicio'] = $objItem->DtInicio;
		$_JSON_retorno[$_JSON_linha]['DtTermino'] = $objItem->DtTermino;
		$_JSON_retorno[$_JSON_linha]['Email'] = $objItem->Email;
		$_JSON_retorno[$_JSON_linha]['Sexo'] = $objItem->Sexo;
		$_JSON_retorno[$_JSON_linha]['DtNascimento'] = $objItem->DtNascimento;
		$_JSON_retorno[$_JSON_linha]['Cep'] = $objItem->Cep;
		$_JSON_retorno[$_JSON_linha]['Telefone'] = $objItem->Telefone;
		$_JSON_retorno[$_JSON_linha]['Cpf'] = $objItem->Cpf;
		$_JSON_retorno[$_JSON_linha]['FlAutorizaComunic'] = $objItem->FlAutorizaComunic;
		$_JSON_retorno[$_JSON_linha]['FlIntencaoInvestir'] = $objItem->FlIntencaoInvestir;
		$_JSON_retorno[$_JSON_linha]['QdoPretendeInvestirMeses'] = $objItem->QdoPretendeInvestirMeses;
		$_JSON_retorno[$_JSON_linha]['SetoresInteresse'] = $objItem->SetoresInteresse;
		$_JSON_retorno[$_JSON_linha]['MarcasInteresse'] = $objItem->MarcasInteresse;
		$_JSON_retorno[$_JSON_linha]['CdFaixaInvest'] = $objItem->CdFaixaInvest;
		$_JSON_retorno[$_JSON_linha]['FaixaInvest'] = $objItem->FaixaInvest;
        //
        $_JSON_linha++;
    }
}           

// Permite cross-domain Ajax
header('Access-Control-Allow-Origin: *');
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
