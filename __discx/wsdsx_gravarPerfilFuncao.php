<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_gravarPerfilFuncao.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_gravarPerfilFuncao.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_gravarPerfilFuncao";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe principal
include_once "inc/libDiscXadmin.php";

// Obtem os parametros
$PARM_tipo = $_REQUEST["tipo"];
$PARM_IdPerfilFuncao = $_REQUEST["idpf"];
$PARM_IdCliente = $_REQUEST["idc"];
$PARM_Nome = $_REQUEST["nm"];
$PARM_Descricao = $_REQUEST["ds"];
$PARM_DISC_D = $_REQUEST["d"];
$PARM_DISC_D_Min = $_REQUEST["dl"];
$PARM_DISC_D_Max = $_REQUEST["dm"];
$PARM_DISC_I = $_REQUEST["i"];
$PARM_DISC_I_Min = $_REQUEST["il"];
$PARM_DISC_I_Max = $_REQUEST["im"];
$PARM_DISC_S = $_REQUEST["s"];
$PARM_DISC_S_Min = $_REQUEST["sl"];
$PARM_DISC_S_Max = $_REQUEST["sm"];
$PARM_DISC_C = $_REQUEST["c"];
$PARM_DISC_C_Min = $_REQUEST["cl"];
$PARM_DISC_C_Max = $_REQUEST["cm"];

// Cria um novo objeto
$PARM_obj = new clsDSXperfilDeFuncao();

// Preenche os parametros
$PARM_obj->IdPerfilFuncao = $PARM_IdPerfilFuncao;
$PARM_obj->Cliente->Id = $PARM_IdCliente;
$PARM_obj->Nome = $PARM_Nome;
$PARM_obj->Descricao = $PARM_Descricao;
$PARM_obj->DISC_D = $PARM_DISC_D;
$PARM_obj->DISC_D_Min = $PARM_DISC_D_Min;
$PARM_obj->DISC_D_Max = $PARM_DISC_D_Max;
$PARM_obj->DISC_I = $PARM_DISC_I;
$PARM_obj->DISC_I_Min = $PARM_DISC_I_Min;
$PARM_obj->DISC_I_Max = $PARM_DISC_I_Max;
$PARM_obj->DISC_S = $PARM_DISC_S;
$PARM_obj->DISC_S_Min = $PARM_DISC_S_Min;
$PARM_obj->DISC_S_Max = $PARM_DISC_S_Max;
$PARM_obj->DISC_C = $PARM_DISC_C;
$PARM_obj->DISC_C_Min = $PARM_DISC_C_Min;
$PARM_obj->DISC_C_Max = $PARM_DISC_C_Max;

// Instancia a classe principal
$objDSXadmin = new clsDSXmasterAdmin();

$objDSXadmin->atualizarPerfilDeFuncao($PARM_obj);

$_JSON_retorno = Array();

$_JSON_retorno["ERRO"] = $objDSXadmin->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDSXadmin->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDSXadmin->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDSXadmin->ERRO_mensagem;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
