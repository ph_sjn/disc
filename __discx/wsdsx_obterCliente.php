<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterCliente.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterCliente.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterCliente";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscXadmin.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id = $_REQUEST["id"];
$PARM_codigo = $_REQUEST["cd"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDSXmasterAdmin();

$objItem = $objDWC->consultarCliente($PARM_id, $PARM_codigo);

$_JSON_retorno = Array();
//
$_JSON_retorno["Id"] = $objItem->Id;
$_JSON_retorno["Codigo"] = $objItem->Codigo;
$_JSON_retorno["Nome"] = $objItem->Nome;
$_JSON_retorno["ERRO"] = $objDWC->ERRO;
$_JSON_retorno["ERRO_bd_cod_erro"] = $objDWC->ERRO_bd_cod_erro;
$_JSON_retorno["ERRO_bd_msg_erro"] = $objDWC->ERRO_bd_msg_erro;
$_JSON_retorno["ERRO_mensagem"] = $objDWC->ERRO_mensagem;
$_JSON_retorno["ERRO_complemento"] = $objDWC->ERRO_complemento;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
