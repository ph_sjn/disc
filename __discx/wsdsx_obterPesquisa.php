<?php

//function LOG_debug($texto){
//    $LOCAL_nome="wsdsx_obterPesquisa.php";
//    try {
//        //echo(date("Y-m-d H:m:s") . '|' . $texto . "<br>");
//        $handle = fopen($LOCAL_nome.".log", "a+");
//        if($handle){
//            fwrite($handle, date("Y-m-d H:m:s") . '|' . $texto . "\r\n");
//            fclose($handle);
//        }
//    } catch (Exception $exc) {
//        echo $exc->getMessage();
//    }
//}

/******
 * 
 * wsdsx_obterPesquisa.php
 * 
 ******/

// Nome do servico
$RPWS_Servico = "wsdsx_obterPesquisa";
$RPWS_SemBloqueio = FALSE;
$RPWS_retornoNaoAutorizado = "";

// Verifica se o cliente eh autorizado
include_once "inc/incAutorizacao.php";

/*
 * CORPO DO SERVICO
 */

include_once "inc/libUtils.php";

// Biblioteca da classe Geofusion
include_once "inc/libDiscX.php";

$PARM_tipoRetorno = $_REQUEST["tipo"];
$PARM_id = $_REQUEST["id"];

// Instancia objeto da classe Geofusion
$objDWC = new clsDWCmaster();

$objItem = $objDWC->obterPesquisa($PARM_id);

$_JSON_retorno = Array();
//
$_JSON_retorno["Id"] = $objItem->Id;
$_JSON_retorno["DataInicio"] = $objItem->DataInicio;
$_JSON_retorno["DataFim"] = $objItem->DataFim;
$_JSON_retorno["Titulo"] = $objItem->Titulo;
$_JSON_retorno["TextoIntroducaoFranqueador"] = $objItem->TextoIntroducaoFranqueador;
$_JSON_retorno["TextoIntroducaoFranqueado"] = $objItem->TextoIntroducaoFranqueado;
$_JSON_retorno["IdFranqueador"] = $objItem->IdFranqueador;
$_JSON_retorno["NomeFranqueador"] = $objItem->NomeFranqueador;
$_JSON_retorno["EmailFranqueador"] = $objItem->EmailFranqueador;
$_JSON_retorno["EmailModeloAssunto"] = $objItem->EmailModeloAssunto;
$_JSON_retorno["EmailModeloCorpo"] = $objItem->EmailModeloCorpo;
$_JSON_retorno["EmailConclusaoAssunto"] = $objItem->EmailConclusaoAssunto;
$_JSON_retorno["EmailConclusaoCorpo"] = $objItem->EmailConclusaoCorpo;
$_JSON_retorno["EnviarEmailConclusao"] = $objItem->EnviarEmailConclusao;
$_JSON_retorno["CodigoSituacao"] = $objItem->CodigoSituacao;
$_JSON_retorno["Situacao"] = $objItem->Situacao;
$_JSON_retorno["IdCliente"] = $objItem->IdCliente;
$_JSON_retorno["NomeCliente"] = $objItem->NomeCliente;
$_JSON_retorno["IdProjeto"] = $objItem->IdProjeto;
$_JSON_retorno["NomeProjeto"] = $objItem->NomeProjeto;
//
// Responde com o resultado em JSON
header('Content-type: application/json;');
if($PARM_tipoRetorno == "1") {
    echo json_encode(htmljson($_JSON_retorno));
} else {
    echo json_encode(utf8json($_JSON_retorno));
}
