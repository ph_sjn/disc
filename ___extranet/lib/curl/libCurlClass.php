<?php

class clsRPcurl {
	
	public $tempoLimite = 60; // Padrao 60 segundos
	
	public $ERRO_sucesso = false;
	public $ERRO_codigo = "";
	public $ERRO_mensagem = "";
	public $ERRO_curl_error = "";
	public $ERRO_curl_errno = 0;
	
	public $CURL_http_code = "";
	
	public $RESULTS = null;
	
	public $curlOPT_CONNECTTIMEOUT = 30;
	public $curlOPT_ENCODING = "";
	public $curlOPT_FOLLOWLOCATION = false;
	public $curlOPT_HEADER = true;
	public $curlOPT_NOPROGRESS = false;
	public $curlOPT_POST = true;
	public $curlOPT_RETURNTRANSFER = true;
	public $curlOPT_SSL_VERIFYHOST = false;
	public $curlOPT_SSL_VERIFYPEER = false;
	public $curlOPT_TIMEOUT = 60;
	public $curlOPT_USERAGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0";
	public $curlOPT_VERBOSE = false;
	
	public $CURL_exec = "";
	public $CURL_info = "";
	public $CURL_header = "";
	
	public function post( $parm_URL, $parm_Parametros = array() ) {
		
		// Limpa o resultado da requisicao
		$this->RESULTS = null;
	
	    // Monta os parametros para a requisicao
	    $objCamposPOST = http_build_query( $parm_Parametros, '', '&' );
	    
	    // Ajusta o tempo limite da requisicao
	    set_time_limit($this->tempoLimite);
	
		// Inicia a sessao e obtem o manipulador (handle)
	    $CURhnd = curl_init();
	    
		// Monta a matriz de opcoes
	    $CURLops = array(
	        CURLOPT_CONNECTTIMEOUT => $this->curlOPT_CONNECTTIMEOUT,
	        CURLOPT_ENCODING       => $this->curlOPT_ENCODING,
	        CURLOPT_FOLLOWLOCATION => $this->curlOPT_FOLLOWLOCATION,
	        CURLOPT_HEADER         => $this->curlOPT_HEADER,
	        CURLOPT_NOPROGRESS     => $this->curlOPT_NOPROGRESS,
	        CURLOPT_POST           => $this->curlOPT_POST,
	        CURLOPT_POSTFIELDS     => $objCamposPOST,
	        CURLOPT_RETURNTRANSFER => $this->curlOPT_RETURNTRANSFER,
	        CURLOPT_SSL_VERIFYHOST => $this->curlOPT_SSL_VERIFYHOST,
	        CURLOPT_SSL_VERIFYPEER => $this->curlOPT_SSL_VERIFYPEER,
	        CURLOPT_TIMEOUT        => $this->curlOPT_TIMEOUT,
	        CURLOPT_URL            => $parm_URL,
	        CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0',
	        CURLOPT_VERBOSE        => $this->curlOPT_VERBOSE,
	    );
	    
	    // Atribui as opcoes para a requisicao
	    curl_setopt_array( $CURhnd, $CURLops );
	    
	    // Executa a requisicao e obtem o retorno (Header + Body)
	    $this->CURL_exec  = curl_exec( $CURhnd );
	    
	    // Obtem as informacoes da requisicao
	    $this->CURL_info  = curl_getinfo( $CURhnd );
	
		// Obtem as informacoes de erro, se disponivel
	    $this->ERRO_curl_error = curl_error( $CURhnd );
	    $this->ERRO_curl_errno = curl_errno( $CURhnd );

		$lngTamanhoHeader = strval($this->CURL_info['header_size']);

	    // Obtem o HTTP RETURN CODE
	    $this->CURL_http_code = $this->CURL_info["http_code"];
	    // Extrai o cabecalho (HEADER)
	    $this->CURL_header  = substr( $this->CURL_exec, 0, $lngTamanhoHeader );
	    // Obtem o corpo do resultado (BODY)
	    $this->RESULTS = substr( $this->CURL_exec, $lngTamanhoHeader );

		// Fecha a sessao
	    curl_close( $CURhnd );
	    
	    // Se falhou no CURL_exec...
	    if ( $this->CURL_exec === false ) {
	    	// Atribui o codigo de erro
	        $this->ERRO_codigo = 'curl_exec';
	        // Retorna false
	        return false;
	    }
		// Se houve alguma outra falha...
	    if ( $this->ERRO_curl_error !== "" ) {
	    	// Atribui o codigo generico de erro
	        $this->ERRO_codigo = 'curl_error';
	        // Retorna false
	        return false;
	    }
		// Retorna sucesso
		$this->ERRO_sucesso = true;
		// 
		return($this->ERRO_sucesso);

	}

	public function get( $parm_URL, $parm_USERAGENT = "" ) {
		
		// Limpa o resultado da requisicao
		$this->RESULTS = null;
	
	    // Ajusta o tempo limite da requisicao
	    set_time_limit($this->tempoLimite);
	
		// Inicia a sessao e obtem o manipulador (handle)
	    $CURhnd = curl_init();
	    
		// Monta a matriz de opcoes
	    $CURLops = array(
	        CURLOPT_CONNECTTIMEOUT => $this->curlOPT_CONNECTTIMEOUT,
	        CURLOPT_ENCODING       => $this->curlOPT_ENCODING,
	        CURLOPT_FOLLOWLOCATION => $this->curlOPT_FOLLOWLOCATION,
	        CURLOPT_HEADER         => $this->curlOPT_HEADER,
	        CURLOPT_NOPROGRESS     => $this->curlOPT_NOPROGRESS,
	        CURLOPT_RETURNTRANSFER => $this->curlOPT_RETURNTRANSFER,
	        CURLOPT_SSL_VERIFYHOST => $this->curlOPT_SSL_VERIFYHOST,
	        CURLOPT_SSL_VERIFYPEER => $this->curlOPT_SSL_VERIFYPEER,
	        CURLOPT_TIMEOUT        => $this->curlOPT_TIMEOUT,
	        CURLOPT_URL            => $parm_URL,
	        CURLOPT_USERAGENT      => $this->curlOPT_USERAGENT,
	        CURLOPT_VERBOSE        => $this->curlOPT_VERBOSE
	    );
	    
	    // Atribui as opcoes para a requisicao
	    curl_setopt_array( $CURhnd, $CURLops );
	    
	    // Executa a requisicao e obtem o retorno (Header + Body)
	    $this->CURL_exec  = curl_exec( $CURhnd );
	    
	    // Obtem as informacoes da requisicao
	    $this->CURL_info  = curl_getinfo( $CURhnd );
	
		// Obtem as informacoes de erro, se disponivel
	    $this->ERRO_curl_error = curl_error( $CURhnd );
	    $this->ERRO_curl_errno = curl_errno( $CURhnd );

		$lngTamanhoHeader = strval($this->CURL_info['header_size']);

	    // Obtem o HTTP RETURN CODE
	    $this->CURL_http_code = $this->CURL_info["http_code"];
	    // Extrai o cabecalho (HEADER)
	    $this->CURL_header  = substr( $this->CURL_exec, 0, $lngTamanhoHeader );
	    // Obtem o corpo do resultado (BODY)
	    $this->RESULTS = substr( $this->CURL_exec, $lngTamanhoHeader );

		// Fecha a sessao
	    curl_close( $CURhnd );
	    
	    // Se falhou no CURL_exec...
	    if ( $this->CURL_exec === false ) {
	    	// Atribui o codigo de erro
	        $this->ERRO_codigo = 'curl_exec';
	        // Retorna false
	        return false;
	    }
		// Se houve alguma outra falha...
	    if ( $this->ERRO_curl_error !== "" ) {
	    	// Atribui o codigo generico de erro
	        $this->ERRO_codigo = 'curl_error';
	        // Retorna false
	        return false;
	    }
		// Retorna sucesso
		$this->ERRO_sucesso = true;
		// 
		return($this->ERRO_sucesso);

	}         
}
