<?php
/*
 *  Parametros:
 *    doc: nome do arquivo incluindo o path completo 
 */

//Definicao do nome do arquivo e diretorio (nome da extranet)
if(isset($_POST['doc'])) {
    
    $doc = $_POST['doc'];

    //Verifica se o arquivo existe
    if (!file_exists($doc)) {
        echo "File $doc not found";
        return;
    } 

    $fp = fopen( $doc, "r" );

    //Le e envia em blocos de 2048 bytes
    while( !feof( $fp ) ){
        $buffer = fread( $fp, 2048 );
        echo( $buffer );
        ob_flush();
        flush();
    }

    //Fecha o arquivo
    fclose( $fp );    
}
else {
    echo "File $doc not found";
}
?>
