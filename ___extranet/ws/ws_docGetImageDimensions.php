<?php
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors',1);
/*
 *  Parametros:
 *    remotedoc: nome do arquivo incluindo o path completo 
 */

//Definicao do nome do arquivo e diretorio (nome da extranet)
if(isset($_POST['remotedoc'])) {
//if(isset($_REQUEST['remotedoc'])) {
    
    $doc = $_POST['remotedoc'];
//    $doc = $_REQUEST['remotedoc'];

	// Obter as dimensões da imagem, no formato 9999x9999
	$dimensoes = getimagesize( $doc );
	
	// LARGURA X ALTURA X FORMATO RGB/CMYK
	//echo $dimensoes[0] ."x". $dimensoes[1] ."x". $dimensoes[2];
	
	$doc_path_array = explode("/",$doc);

	if ( ($doc_path_array[ count($doc_path_array)-1 ] != "") && file_exists($doc) )  {
//		echo("<hr>identify -format \"%wx%h;%xx%y;%[colorspace];%f\" ". $doc);
//		exec("identify -format \"%wx%h;%xx%y;%[colorspace];%f\" ". $doc, $saida);
		exec("/opt/local/bin/identify -format \"%wx%h;%xx%y;%[colorspace];%f\" ". $doc, $saida);
//		echo("<hr>saida:<pre>");
//		print_r($saida);
//		echo("</pre><hr>");
		$info = explode(";",$saida[0]);
	} else {
		$info[] = "0";
		$info[] = "0";
		$info[] = "0";
	}
	
	$resolucao = explode(";",$info[0]);
	
	// Resolucao
	echo $info[0] . "x";
	
	// Esquema de cores
	echo $info[2] . "x";
	
	// dpi
	if ( stristr($doc,".png") ) {
		$dpi = explode("x",$info[1]);
		$dpi_value = round($dpi[0] * 2.54);
	} else {
		$dpi = explode("x",$info[1]);
		$dpi_value = $dpi[0];
	}
	echo $dpi_value;
	
}
else {
	echo 0;
}
?>
