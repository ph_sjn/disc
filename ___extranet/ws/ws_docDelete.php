<?php
# Parametros:
#   doc: nome do arquivo incluindo o path completo 
#   tipo: especifica se é arquivo ou diretório
$filename = $_POST['doc'];
$is_dir = $_POST['is_dir'];

$ret = False;
if ( $is_dir ) { 
	if (rmdir($filename)) {
		$ret = true;
	}
} else { 
	if (unlink($filename)) {
		$ret = True;
	} 
}

echo $ret;
?>
